<?

$pagetitle = 'Grand Canyon tours';
$metadesc = 'Grand Canyon tour from Las Vegas';
include_once('2_header.php');


$tours = array();
$tourids = array();

//GET TOUR INFO
if(!isset($_REQUEST['ez']) || $_REQUEST['ez'] != 'y'){

echo '<DIV STYLE="font-family:Arial; font-size:20pt; font-weight:bold; text-align:center; padding-bottom:10px;"><I>'.gettrans('Our Top Grand Canyon Tours!').'</I></DIV>'."\n\n";
$query = 'SELECT DISTINCT tours.`id`, tours.`alias`, tours.`title`, tours.`eztitle`, tours.`short_desc`, tours.`toptitle`, tours.`topdesc`';
	if($_SESSION['lang'] != "1"): $query .= ', tours_translations.`title` AS `trans_title`, tours_translations.`eztitle` AS `trans_eztitle`, tours_translations.`short_desc` AS `trans_short_desc`, tours_translations.`toptitle` AS `trans_toptitle`, tours_translations.`topdesc` AS `trans_topdesc`'; endif;
	$query .= ' FROM `tours`';
	if($_SESSION['lang'] != "1"): $query .= ' LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid` AND tours_translations.`lang` = "'.$_SESSION['lang'].'"'; endif;
	$query .= ' WHERE tours.`toptour` = "1" AND tours.`archived` = 0 AND tours.`hidden` != "1"';
	$query .= ' ORDER BY tours.`toporder` ASC, tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';

} else {

if(!isset($_REQUEST['ez1']) || $_REQUEST['ez1'] == "" || !is_numeric($_REQUEST['ez1'])){ $_REQUEST['ez1'] = '*'; }
if(!isset($_REQUEST['ez2']) || $_REQUEST['ez2'] == "" || !is_numeric($_REQUEST['ez2'])){ $_REQUEST['ez2'] = '*'; }
if(!isset($_REQUEST['ez3']) || $_REQUEST['ez3'] == ""){
	$_REQUEST['ez3'] = array(
		'start' => $time,
		'end' => mktime(0,0,0,date("n",$time),date("j",$time),(date("Y",$time)+1))
		);
	} else {
	$_REQUEST['ez3'] = explode('-',$_REQUEST['ez3']);
		if(isset($_REQUEST['ez3'][0])){
			$_REQUEST['ez3'][0] = explode('/',$_REQUEST['ez3'][0]);
				if(!isset($_REQUEST['ez3'][0][0]) || $_REQUEST['ez3'][0][0] == ""){ $_REQUEST['ez3'][0][0] = date("n",$time); }
				if(!isset($_REQUEST['ez3'][0][1]) || $_REQUEST['ez3'][0][1] == ""){ $_REQUEST['ez3'][0][1] = date("j",$time); }
				if(!isset($_REQUEST['ez3'][0][2]) || $_REQUEST['ez3'][0][2] == ""){ $_REQUEST['ez3'][0][2] = date("Y",$time); }
				$_REQUEST['ez3']['start'] = mktime(0,0,0,$_REQUEST['ez3'][0][0],$_REQUEST['ez3'][0][1],$_REQUEST['ez3'][0][2]);
			} else {
			$_REQUEST['ez3']['start'] = $time;
			}
		if(isset($_REQUEST['ez3'][1])){
			$_REQUEST['ez3'][1] = explode('/',$_REQUEST['ez3'][1]);
				if(!isset($_REQUEST['ez3'][1][0]) || $_REQUEST['ez3'][1][0] == ""){ $_REQUEST['ez3'][1][0] = date("n",$time); }
				if(!isset($_REQUEST['ez3'][1][1]) || $_REQUEST['ez3'][1][1] == ""){ $_REQUEST['ez3'][1][1] = date("j",$time); }
				if(!isset($_REQUEST['ez3'][1][2]) || $_REQUEST['ez3'][1][2] == ""){ $_REQUEST['ez3'][1][2] = (date("Y",$time)+1); }
				$_REQUEST['ez3']['end'] = mktime(0,0,0,$_REQUEST['ez3'][1][0],$_REQUEST['ez3'][1][1],$_REQUEST['ez3'][1][2]);
			} else {
			$_REQUEST['ez3']['end'] = mktime(0,0,0,date("n",$_REQUEST['ez3']['start']),date("j",$_REQUEST['ez3']['start']),(date("Y",$_REQUEST['ez3']['start'])+1));
			}
	}
if(!isset($_REQUEST['ez4']) || $_REQUEST['ez4'] == "" || !is_numeric($_REQUEST['ez4'])){ $_REQUEST['ez4'] = '*'; }

$query = 'SELECT DISTINCT tours.`id`, tours.`alias`, tours.`title`, tours.`numdays`, tours.`short_desc`';
		$query .= ', ( SELECT routes.`dep_loc` FROM `tours_assoc`,`routes` WHERE tours_assoc.`tourid` = tours.`id` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = routes.`id` ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC LIMIT 1 ) AS `scity`';
		$query .= ', tours.`perguest`, tours.`mincharge`, tours.`maxcharge`, tours.`volstart`, tours.`volrate`, tours.`ext_pricing`';
		if($_SESSION['lang'] != "1"){ $query .= ', tours_translations.`title` AS `trans_title`, tours_translations.`short_desc` AS `trans_short_desc`'; }
		if($_REQUEST['ez4'] != '*'){ $query .= ', tours_transtypes_assoc.`transid`'; }
	$query .= ' FROM';
		if($_SESSION['lang'] == "1"){ $query .= ' `tours`'; } else { $query .= ' (`tours` LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid` AND tours_translations.`lang` = "'.$_SESSION['lang'].'")'; }
		if($_REQUEST['ez2'] != '*'){ $query .= ', `tours_assoc`, `routes`'; }
		if($_REQUEST['ez4'] != '*'){ $query .= ', `tours_transtypes_assoc`'; }
	$query .= ' WHERE tours.`archived` = 0 AND tours.`hidden` != "1"';
	$query .= ' AND EXISTS (SELECT * FROM `tours_dates` WHERE tours_dates.`tourid` = tours.`id` AND tours_dates.`date` >= "'.$_REQUEST['ez3']['start'].'" AND tours_dates.`date` <= "'.$_REQUEST['ez3']['end'].'")';
	if($_REQUEST['ez1'] != '*'){ $query .= ' AND `scity` = "'.$_REQUEST['ez1'].'"'; }
	if($_REQUEST['ez2'] != '*'){ $query .= ' AND (tours.`id` = tours_assoc.`tourid` AND tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`arr_loc` = "'.$_REQUEST['ez2'].'")'; }
	if($_REQUEST['ez4'] != '*'){ $query .= ' AND (tours.`id` = tours_transtypes_assoc.`tourid` AND tours_transtypes_assoc.`transid` = "'.$_REQUEST['ez4'].'")'; }
	$query .= ' ORDER BY tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
	//echo $query.'<BR>';
}

$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['images'] = array();
	$tours['t'.$row['id']] = $row;
	array_push($tourids,$row['id']);
	}

if($num_results > 0){

//GET IMAGES
$query = 'SELECT DISTINCT images_assoc_tours.`tourid` as `id`, images.`filename`, images_assoc_tours.`order` FROM `images_assoc_tours`,`images` WHERE images_assoc_tours.`imgid` = images.`id`';
	$query .= ' AND (images_assoc_tours.`tourid` = "'.implode('" OR images_assoc_tours.`tourid` = "',$tourids).'")';
	$query .= ' ORDER BY images_assoc_tours.`order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours['t'.$row['id']]['images'],$row['filename']);
	}

//PRINT TOUR LISTING
$usedimgs = array();
foreach($tours as $row){
	//echo '<PRE>'; print_r($row); echo '</PRE>'."\n\n";

	if(isset($row['trans_toptitle']) && $row['trans_toptitle'] != ""){
		$row['title'] = $row['trans_toptitle'];
		} elseif(isset($row['trans_eztitle']) && $row['trans_eztitle'] != ""){
		$row['title'] = $row['trans_eztitle'];
		} elseif(isset($row['trans_title']) && $row['trans_title'] != ""){
		$row['title'] = $row['trans_title'];
		} elseif(isset($row['toptitle']) && trim($row['toptitle']) != ""){
		$row['title'] = $row['toptitle'];
		} elseif(isset($row['eztitle']) && trim($row['eztitle']) != ""){
		$row['title'] = $row['eztitle'];
		}

	if(isset($row['trans_topdesc']) && $row['trans_topdesc'] != ""){
		$row['short_desc'] = $row['trans_topdesc'];
		} elseif(isset($row['trans_short_desc']) && $row['trans_short_desc'] != ""){
		$row['short_desc'] = $row['trans_short_desc'];
		} elseif(isset($row['topdesc']) && $row['topdesc'] != ""){
		$row['short_desc'] = $row['topdesc'];
		}

	$img = '';
	foreach($row['images'] as $thisimg){
		if(!in_array($thisimg,$usedimgs)){
			$img = $thisimg;
			array_push($usedimgs,$thisimg);
			break;
			}
		}
	if($img == '' && count($row['images']) > 0){ $img = $row['images'][0]; }
	if($img != ''){ $img = imgform($img,120,200); }

	echo "\t".'<DIV STYLE="background:#FFFFFF url(\'/img/toplinksback2.jpg\') center -22px repeat-x; padding-left:4px; padding-bottom:10px; font-family:Arial; font-size:12pt; clear:both;">'; // onClick="window.location=\'/tour.php?id='.urlencode($row['alias']).'\';"
		if(is_array($img) && $img['filename'] != ""){ echo '<DIV STYLE="padding-left:8px; padding-bottom:10px; float:right;"><A HREF="/2_tour.php?id='.urlencode($row['alias']).'"><IMG SRC="images/'.$img['filename'].'" WIDTH="'.$img['w'].'" HEIGHT="'.$img['h'].'" BORDER="0"></A></DIV>'; }
		echo '<DIV STYLE="font-weight:bold; padding-top:4px; padding-bottom:2px;"><A HREF="/2_tour.php?id='.urlencode($row['alias']).'">'.$row['title'].'</A></DIV>';
		echo '<SPAN STYLE="font-size:11pt;">'.$row['short_desc'].'</SPAN>';
		echo '</DIV>'."\n";
	}

} else { //else $num_results if statement


echo '<BR><BR><CENTER><SPAN STYLE="font-family:Arial; font-size:12pt;">No tours were found that match your criteria.<BR>Please try expanding your search using the EZ Tour Finder at the very top of this page.</SPAN></CENTER>';


} //end $num_results if statement


include('footer.php'); ?>