<?

$pagetitle = 'Resell Bundu Bashers tours';
$metadesc = 'Grand Canyon tours, as well as Monument Valley tours and Bryce Canyon tour service. Yellowstone tours from Salt Lake City, by bus to Grand Canyon, Monument Valley, tours, Yellowstone, Bryce Canyon tour';
include_once('header.php');

echo '<CENTER><BR>

<FONT FACE="Arial" SIZE="5" COLOR="#000080"><B>'.gettrans('Resell Bundu Bashers tours').'</B></FONT></CENTER><BR>

<p align="justify"><font face="Arial"><font color="#000080" style="font-size: 9pt">

	'.gettrans('Bundu Bashers is an independent tour company offering a variety of tours, primarily from Las Vegas and Salt Lake City.').'&nbsp;
	'.gettrans('Our products can be seen in this site.').'&nbsp;

	'.gettrans('Bundu Bashers specializes in smaller, more personal, multi days tours that the bigger companies feel are not cost effective to undertake.').'<BR><BR>

	'.gettrans('We invite travel professionals and other interested parties who may be interested in reselling our products to contact us.').'&nbsp;

	'.gettrans('Bundu Bashers offers competitive wholesale rates.').'<BR><BR>

	'.gettrans('Amanda McCabe, our sales director, is the person to contact.').'&nbsp;
	'.gettrans('She can be reached by').' <a href="mailto:amanda@utahtransportation.com?subject=We are interested in reselling your products">'.gettrans('email').'</a>'.gettrans(', or at 435 658 2227, 801 366 5466 or 800 724 7767.').'<BR><BR>

	'.gettrans('Thank you for your interest!').'</font></p>'."\n\n";

include('footer.php'); ?>