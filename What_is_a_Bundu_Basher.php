<?

$pagetitle = 'What is a Bundu Basher?';
$metadesc = 'Grand Canyon tours, as well as Monument Valley tours and Bryce Canyon tour service. Yellowstone tours from Salt Lake City, by bus to Grand Canyon, Monument Valley, tours, Yellowstone, Bryce Canyon tour';
include_once('header.php');

echo '<center>

            <table border="0" cellpadding="3" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2">

              <tr>

                <td width="100%">

                <p align="center">

                <img border="0" src="images/Bundu-Bashers-logo-letters-.gif" width="308" height="200"></td>

              </tr>

              <tr>

                <td width="100%">

                <h5 align="center"><font size="4" face="Tahoma" color="#000080">

                '.gettrans('Well ... just what is a Bundu Basher?????').'</font></h5>

                <p align="justify"><font face="Tahoma" size="2" color="#000080">

                '.gettrans('Bundu Basher is an old African term.').'&nbsp;
		'.gettrans('The bundu is out in the middle of nowhere, far from anything.').'&nbsp;
		'.gettrans('A basher is someone who goes beating about in the bundu.').'&nbsp;
		'.gettrans('In Africa he would take an old landrover, throw a couple of containers of gas in the back, next to the six packs of Castle lager, and head out for the veld - hundreds, if not thousands, of square miles of open country, uncontaminated by civilization.').'</font></p>
                <p align="justify"><font face="Tahoma" size="2" color="#000080">
                '.gettrans("This is what we hope to do in the American southwest, take you bashing about in our bundu, as you travel across virtually deserted canyonlands and deserts to some of the world's most spectacular sights!").'</font></td>

              </tr>

            </table>

            </center>'."\n\n";

include('footer.php'); ?>