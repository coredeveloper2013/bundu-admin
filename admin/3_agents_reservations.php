<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_agents_reservations";
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){ $pageid .= '_edit'; }
require("validate.php");
require("header.php");

if(!isset($_SESSION['agents_reservations']['p'])): $_SESSION['agents_reservations']['p'] = 1; endif;
if(!isset($_SESSION['agents_reservations']['view'])): $_SESSION['agents_reservations']['view'] = 'unconfirmed'; endif;
if(!isset($_SESSION['agents_reservations']['agent'])): $_SESSION['agents_reservations']['agent'] = '*'; endif;
if(!isset($_SESSION['agents_reservations']['limit'])): $_SESSION['agents_reservations']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['agents_reservations']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""): $_SESSION['agents_reservations']['view'] = $_REQUEST['view']; endif;
if(isset($_REQUEST['agent']) && $_REQUEST['agent'] != ""): $_SESSION['agents_reservations']['agent'] = $_REQUEST['agent']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['agents_reservations']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['agents_reservations']['p'] = '1';
	}

$viewopts = array(
	'unconfirmed' => 'Unconfirmed',
	'confirmed' => 'Confirmed',
	'*' => 'All'
	);

//GET AGENTS
$agents = array();
$query = 'SELECT * FROM `agents` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$agents['a'.$row['id']] = $row;
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Agent Reservations</U></FONT><BR>'."\n\n";


//GET RESERVATIONS
$reservations = array();
$resids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS reservations.`id`, reservations.`date_booked`, reservations.`name`,
				reservations.`agent`, agents.`name` AS `agentname`, reservations.`agent_conf`,
				subagents.name AS subagentname
			FROM `reservations`
			JOIN `agents` ON agents.`id` = reservations.`agent`
			LEFT JOIN subagents ON subagents.id = reservations.id_subagent';
	$query .= ' WHERE reservations.`canceled` = 0';
		if($_SESSION['agents_reservations']['agent'] != '*'){
			$query .= ' AND agents.`id` = '.$_SESSION['agents_reservations']['agent'];
		}
		if($_SESSION['agents_reservations']['view'] == 'unconfirmed'){
			$query .= ' AND reservations.`agent_conf` = ""';
		} else if($_SESSION['agents_reservations']['view'] == 'confirmed'){
			$query .= ' AND reservations.`agent_conf` != ""';
		}
	$query .= ' GROUP BY reservations.`id`';
	$query .= ' ORDER BY reservations.`id` DESC';
	$query .= ' LIMIT '.(($_SESSION['agents_reservations']['p']-1)*$_SESSION['agents_reservations']['limit']).','.$_SESSION['agents_reservations']['limit'];
$result = mysql_query($query);
//echo $query.'<BR>'."\n";
//echo mysql_error().'<BR>';
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($reservations,$row);
	array_push($resids,$row['id']);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['agents_reservations']['limit']);
if($numpages > 0 && $_SESSION['agents_reservations']['p'] > $numpages): $_SESSION['agents_reservations']['p'] = $numpages; endif;

$link = '';

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">View:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="view" STYLE="font-size:9pt;">';
		foreach($viewopts as $key => $val){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['agents_reservations']['view'] == $key): echo " SELECTED"; endif;
			echo '>'.$val.'</OPTION>'."\n";
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Vendor:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="agent" STYLE="font-size:9pt; width:200px;">';
			echo '<OPTION VALUE="*">All agents</OPTION>';
		foreach($agents as $row){
			echo '<OPTION VALUE="'.$row['id'].'"';
			if($_SESSION['agents_reservations']['agent'] == $row['id']): echo " SELECTED"; endif;
			echo '>'.$row['name'].'</OPTION>'."\n";
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['agents_reservations']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="View" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents_reservations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents_reservations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['agents_reservations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents_reservations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents_reservations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Conf#</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Booked</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Agent</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Subagent</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:10px;">Confirmation</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($reservations as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:5px; padding-right:10px; font-family:Arial; font-size:10pt; font-weight:bold;"><A HREF="3_reservations.php?view='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.date("m/d/y",$row['date_booked']).'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['name'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['agentname'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['subagentname'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['agent_conf'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="3_reservations.php?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents_reservations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents_reservations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['agents_reservations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['agents_reservations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['agents_reservations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing




require("footer.php");

?>