<? //error_reporting('E_ALL'); ini_set('display_errors','1');

// This custom utility created by Dominick Bernal - www.bernalwebservices.com

$pageid = "backup";
require("validate.php");

if(!isset($_REQUEST['file']) || $_REQUEST['file'] == "" || !file_exists('/home/sessel/db_back/'.$_REQUEST['file'])){

header("Location: 3_backup.php");

} else {

header('Content-Disposition: attachment; filename="'.$_REQUEST['file'].'"');
header('Content-Length: '.filesize('/home/sessel/db_back/'.$_REQUEST['file']));
header('Content-Type: application/octet-stream;');
header('Cache-Control: no-cache, must-revalidate');
header('Cache-Control: private');
header('Pragma: private');
readfile('/home/sessel/db_back/'.$_REQUEST['file']);

}

?>