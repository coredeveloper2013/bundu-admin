<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_bashersmenu";
require("validate.php");
require("header.php");

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "updatesectitles" && isset($_POST['sectionid']) && isset($_POST['sectiontitle'])){

	foreach($_POST['sectionid'] as $key => $title){
	if($_POST['sectionid'][$key] != "" && trim($_POST['sectiontitle'][$key]) != "")
	$query = 'UPDATE `menu_sections` SET `title` = "'.trim($_POST['sectiontitle'][$key]).'" WHERE `id` = "'.$_POST['sectionid'][$key].'" LIMIT 1';
	@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Updated menu section "'.trim($_POST['sectiontitle'][$key]).'" ('.$_POST['sectionid'][$key].').'); else: array_push($errormsg,$thiserror); endif;
	}

} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "updateitem" && isset($_POST['id']) && $_POST['id'] != ""){

	if(!isset($_POST['ar_scity']) || $_POST['ar_scity'] != "y"): $_POST['ar_scity'] = "n"; endif;
	if(!isset($_POST['ar_trans']) || $_POST['ar_trans'] != "y"): $_POST['ar_trans'] = "n"; endif;

	//INSERT NEW ITEM---------
	if($_POST['id'] == "new"){

	$query = 'INSERT INTO `menu_items` ( `section` , `title` , `tourid` , `url` , `contains` , `scity` , `destination` , `trans_type` , `cdays` , `days` , `desc` , `ar_scity` , `ar_trans` , `showone` , `order` )';
		$query .= ' VALUES ("'.$_POST['section'].'","'.$_POST['title'].'","'.$_POST['tourid'].'","'.$_POST['url'].'","'.$_POST['contains'].'","'.$_POST['scity'].'","'.$_POST['destination'].'","'.$_POST['trans_type'].'","'.$_POST['cdays'].'","'.$_POST['days'].'","'.$_POST['desc'].'","'.$_POST['ar_scity'].'","'.$_POST['ar_trans'].'","'.$_POST['showone'].'","'.$_POST['order'].'")';
	@mysql_query($query);
	$_POST['id'] = mysql_insert_id();
	if($_POST['id'] < 1): $_POST['id'] = "new"; endif;
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved menu item "'.$_POST['title'].'" ('.$_POST['id'].').'); else: array_push($errormsg,$thiserror); endif;

	//UPDATE ITEM-------------
	} else {
	$query = 'UPDATE `menu_items` SET `section` = "'.$_POST['section'].'", `title` = "'.$_POST['title'].'", `tourid` = "'.$_POST['tourid'].'", `url` = "'.$_POST['url'].'", `contains` = "'.$_POST['contains'].'", `scity` = "'.$_POST['scity'].'", `destination` = "'.$_POST['destination'].'", `trans_type` = "'.$_POST['trans_type'].'", `cdays` = "'.$_POST['cdays'].'", `days` = "'.$_POST['days'].'", `desc` = "'.$_POST['desc'].'", `ar_scity` = "'.$_POST['ar_scity'].'", `ar_trans` = "'.$_POST['ar_trans'].'", `showone` = "'.$_POST['showone'].'", `order` = "'.$_POST['order'].'"';
		$query .= ' WHERE `id` = "'.$_POST['id'].'" LIMIT 1';
	@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Updated menu item "'.$_POST['title'].'" ('.$_POST['id'].').'); else: array_push($errormsg,$thiserror); endif;

	} //End Id If Statement
	$_REQUEST['id'] = $_POST['id'];

} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "delitem" && isset($_POST['id']) && $_POST['id'] != ""){

	$query = 'DELETE FROM `menu_items` WHERE `id` = "'.$_POST['id'].'" LIMIT 1';
	@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Deleted menu item ('.$_POST['id'].').'); $_REQUEST['id'] = ''; else: array_push($errormsg,$thiserror); endif;

} //END FUNCTIONAL CODE




echo '<CENTER>
<IMG SRC="spacer.gif" BORDER="0" HEIGHT="12"><BR><FONT FACE="Arial" SIZE="5"><U>Bundu Bashers Menu</U></FONT><BR><BR>'."\n\n";

printmsgs($successmsg,$errormsg);


//GET MENU SECTIONS
$menu = array();
$query = 'SELECT * FROM `menu_sections` ORDER BY `order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($menu,$row);
	$menu['m'.$row['id']] = $row;
	$menu['m'.$row['id']]['items'] = array();
	}


if(isset($_REQUEST['id']) && $_REQUEST['id'] != ""){

$fillform = array();
if(count($errormsg) > 0 && count($_POST) > 1){
	$fillform = $_POST;
	} elseif($_REQUEST['id'] != "new") {
	$query = 'SELECT * FROM `menu_items` WHERE `id` = "'.$_REQUEST['id'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	} else {
	$fillform['id'] = $_REQUEST['id'];
	}

//GET STARTING CITIES
$scity = array();
$whereto = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($scity,$row);
	array_push($whereto,$row);
	}

//GET MEANS OF TRAVEL
$means = array();
$query = 'SELECT * FROM `tours_transtypes` ORDER BY `name` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($means,$row);
	}



echo '<FORM NAME="itemform" ID="itemform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">
<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="updateitem">
<INPUT TYPE="hidden" NAME="id" VALUE="'.$fillform['id'].'">

<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="0" WIDTH="600">'."\n\n";

//echo '<TR><TD ALIGN="right" CLASS="fields">ID&nbsp;</TD><TD><FONT FACE="Arial" SIZE="2">'.$fillform['id'].'</FONT></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Section&nbsp;</TD><TD><SELECT ID="section" NAME="section">';
	foreach($menu as $val){
	echo '<OPTION VALUE="'.$val['id'].'"';
	if($val['id'] == getval('section')): echo ' SELECTED'; endif;
	echo '>'.$val['title'].'</OPTION>';
	}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Title&nbsp;</TD><TD><INPUT TYPE="text" SIZE="50" NAME="title" VALUE="'.getval('title').'"></TD></TR>'."\n";

echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">&nbsp;</FONT></TD></TR>'."\n";

//GET TOUR LIST
$listtours = array();
$query = 'SELECT `id`,`title` FROM `tours`';
	$query .= ' ORDER BY `id` DESC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['title'] = str_replace("'",'',$row['title']);
	if(strlen($row['title']) > 50): $row['title'] = substr($row['title'],0,48).'...'; endif;
	array_push($listtours,$row);
	}

echo '<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="left" CLASS="fields">&nbsp;&nbsp;<B>This menu item will link directly to the following tour, if chosen:</B></TD></TR>'."\n";

echo '<TR><TD COLSPAN="2" ALIGN="center" CLASS="fields"><SELECT NAME="tourid"><OPTION VALUE="0">- None -</OPTION>';
		foreach($listtours as $thistour){
		echo '<OPTION VALUE="'.$thistour['id'].'"';
			if($thistour['id'] == getval('tourid')): echo ' SELECTED'; endif;
			echo '>'.$thistour['id'].' : '.$thistour['title'].'</OPTION>';
		}
		echo '</SELECT><BR></TD></TR>'."\n";

//echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">&nbsp;</FONT></TD></TR>'."\n";
echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2"><B><I>- OR, if no tour is chosen...</I></B></FONT></TD></TR>'."\n";
echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">&nbsp;</FONT></TD></TR>'."\n";

echo '<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="left" CLASS="fields">&nbsp;&nbsp;<B>This menu item will link directly to the following URL, if entered:</B></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">URL&nbsp;</TD><TD><INPUT TYPE="text" SIZE="50" NAME="url" VALUE="'.getval('url').'"></TD></TR>'."\n";

echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2"><B><I>- OR, if URL is left blank...</I></B></FONT></TD></TR>'."\n";
echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">&nbsp;</FONT></TD></TR>'."\n";

echo '<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="left" CLASS="fields">&nbsp;&nbsp;<B>This menu item will generate a listing of tours based on the following criteria:</B></TD></TR>'."\n";

echo '<TR><TD ALIGN="right" CLASS="fields">Tour title contains this word&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" NAME="contains" VALUE="'.getval('contains').'"></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Starts in this city&nbsp;</TD><TD><SELECT ID="scity" NAME="scity">';
	echo '<OPTION VALUE="0">- All cities -</OPTION>';
	foreach($scity as $val){
	echo '<OPTION VALUE="'.$val['id'].'"';
	if($val['id'] == getval('scity')): echo ' SELECTED'; endif;
	echo '>'.$val['name'].'</OPTION>';
	}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Includes destination&nbsp;</TD><TD><SELECT ID="destination" NAME="destination">';
	echo '<OPTION VALUE="0">- All destinations -</OPTION>';
	foreach($whereto as $val){
	echo '<OPTION VALUE="'.$val['id'].'"';
	if($val['id'] == getval('destination')): echo ' SELECTED'; endif;
	echo '>'.$val['name'].'</OPTION>';
	}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Includes transportation type&nbsp;</TD><TD><SELECT ID="trans_type" NAME="trans_type">';
	echo '<OPTION VALUE="0">- All transportation types -</OPTION>';
	foreach($means as $val){
	echo '<OPTION VALUE="'.$val['id'].'"';
	if($val['id'] == getval('trans_type')): echo ' SELECTED'; endif;
	echo '>'.$val['name'].'</OPTION>';
	}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Number of days is&nbsp;</TD><TD><SELECT ID="cdays" NAME="cdays">';
	$cdaysopts = array(
		'>' => 'greater than',
		'<' => 'less than',
		'=' => 'equal to'
		);
	foreach($cdaysopts as $key => $val){
	echo "<OPTION VALUE='".$key."'";
	if($key == getval('cdays')): echo ' SELECTED'; endif;
	echo '>'.$val.'</OPTION>';
	}
	echo '</SELECT>&nbsp;';
	echo '<INPUT TYPE="text" SIZE="3" NAME="days" VALUE="'.getval('days').'">';
	echo '</TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields">Description&nbsp;</TD><TD><TEXTAREA COLS="42" ROWS="6">'.getval('desc').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields"><INPUT TYPE="checkbox" NAME="ar_scity" ID="ar_scity" VALUE="y"';
	if(getval('ar_scity') == "y"): echo ' CHECKED'; endif;
	echo '></TD><TD CLASS="fields"><LABEL FOR="ar_scity">Divide and arrange tours by <B>Starting City</B></LABEL></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields"><INPUT TYPE="checkbox" NAME="ar_trans" ID="ar_trans" VALUE="y"';
	if(getval('ar_trans') == "y"): echo ' CHECKED'; endif;
	echo '></TD><TD CLASS="fields"><LABEL FOR="ar_trans">Divide and arrange tours by <B>Transportation Type</B></LABEL></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields"><INPUT TYPE="checkbox" NAME="showone" ID="showone" VALUE="y"';
	if(getval('showone') == "y"): echo ' CHECKED'; endif;
	echo '></TD><TD CLASS="fields"><LABEL FOR="showone">If divided and arranged, create radio buttons for each Starting City and/or Transportation Type</LABEL></TD></TR>'."\n";
echo '<TR><TD ALIGN="right" CLASS="fields"><I>Order value</I>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="3" NAME="order" VALUE="'.getval('order').'"></TD></TR>'."\n";

echo '<TR><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">&nbsp;</FONT></TD></TR>'."\n";

echo '</TABLE>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save Changes">'."\n\n";

echo '</FORM>'."\n\n";

echo '<FONT FACE="Arial" SIZE="3"><B><A HREF="'.$_SERVER['PHP_SELF'].'">Back to Menu Index</A></B></FONT><BR><BR>'."\n\n";


if($fillform['id'] != "new"){
echo '<SCRIPT>
<!--

function delitem(){
	var conf = confirm("Permanently delete this menu item?");
	if(conf){
	document.goodbyeitem.submit();
	}
}

// -->
</SCRIPT>

<FORM METHOD="post" NAME="goodbyeitem" ACTION="'.$_SERVER['PHP_SELF'].'">
<INPUT TYPE="hidden" NAME="utaction" VALUE="delitem">
<INPUT TYPE="hidden" NAME="id" VALUE="'.$fillform['id'].'">
<INPUT TYPE="button" VALUE="Delete this menu item" onClick="delitem()">
</FORM>'."\n\n";
}


} else {

echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">
<INPUT TYPE="hidden" NAME="utaction" VALUE="updatesectitles">'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="628">'."\n";

echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" COLSPAN="3"><FONT FACE="Arial" SIZE="1" COLOR="#FFFFFF"><B>Title</B></FONT></TD>';
	//echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="1" COLOR="#FFFFFF"><B>Edit Title</B></FONT></TD>';
	echo '</TR>'."\n";

echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="left" COLSPAN="3" STYLE="padding-left:10px;"><FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?id=new">Create new menu item...</A></B></FONT></TD></TR>'."\n\n";

//GET MENU ITEMS
$query = 'SELECT * FROM `menu_items` ORDER BY `section` ASC, `order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($menu['m'.$row['section']]['items'],$row);
	}


if(count($menu) > 0){
	bgcolor('');
	foreach($menu as $row){
	$bgcolor = bgcolor('');
	echo '<TR BGCOLOR="#'.$bgcolor.'">';
	echo '<TD ALIGN="left" COLSPAN="3"><FONT FACE="Arial" SIZE="3"><B>'.$row['title'].'</B></FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<FONT FACE="Arial" SIZE="1">Edit title:</FONT>&nbsp;<INPUT TYPE="hidden" NAME="sectionid[]" VALUE="'.$row['id'].'"><INPUT TYPE="text" NAME="sectiontitle[]" VALUE="'.$row['title'].'" SIZE="34"></TD>';
	//echo '<TD ALIGN="right" COLSPAN="2"></TD>';
	echo '</TR>'."\n";
		for($i=0; $i<count($row['items']); $i++){
		$item = $row['items'][$i];
		echo '<TR BGCOLOR="#'.$bgcolor.'">';
		echo '<TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>&#149;</B></FONT></TD>';
		echo '<TD ALIGN="left"><FONT FACE="Arial" SIZE="2"><A HREF="'.$_SERVER['PHP_SELF'].'?id='.$item['id'].'" CLASS="blacklink">'.$item['title'].'</A></FONT></TD>';
		/*echo '<TD ALIGN="right"><INPUT TYPE="hidden" NAME="itemid[]" VALUE="'.$item['id'].'"><INPUT TYPE="text" NAME="itemtitle[]" VALUE="'.$item['title'].'" CLASS="smalltxt" SIZE="32"></TD>';
			echo '<TD ALIGN="center">';
			if($i != 0): echo '<INPUT TYPE="button" VALUE="&nbsp;/\&nbsp;" CLASS="smalltxt" TITLE="Move Up"> '; endif;
			if($i < (count($row['items'])-1)): echo ' <INPUT TYPE="button" VALUE="&nbsp;\/&nbsp;" CLASS="smalltxt" TITLE="Move Down">'; endif;
			echo '</TD>';*/
		echo '<TD ALIGN="left"><SPAN CLASS="sched1">[&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?id='.$item['id'].'">Edit</A>&nbsp;]</SPAN></TD>';
		echo '</TR>'."\n";
		}
	}

} else {
	echo '<TR BGCOLOR="#FFFFFF"><TD ALIGN="center" COLSPAN="8"><FONT FACE="Arial" SIZE="3">Error or no menu items to list.</FONT></TD></TR>'."\n";
}
echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="left" COLSPAN="3" STYLE="padding-left:10px;"><FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?id=new">Create new menu item...</A></B></FONT></TD></TR>'."\n\n";

echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Update Section Titles">'."\n\n";

echo '</FORM>'."\n\n";



} //END $_REQUEST['id'] IF STATEMENT


require("footer.php");

?>