<?  // This custom utility created by Dominick Bernal - www.bernalwebservices.com

$pageid = "3_images";
//require("validate.php");

//echo '<PRE>'; print_r($_FILES); echo '</PRE>';

if(!isset($_REQUEST['choose'])): $_REQUEST['choose'] = 'm'; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "findimg"){
	$_SESSION['images']['find_filename'] = $_REQUEST['find_filename'];
	$_SESSION['images']['find_caption'] = $_REQUEST['find_caption'];
	$_SESSION['images']['limit'] = $_REQUEST['limit'];
	if($_REQUEST['limit'] > 0): $_SESSION['images']['limit'] = $_REQUEST['limit']; else: $_SESSION['images']['limit'] = 50; endif;
	$_SESSION['images']['p'] = 1;
} elseif(!isset($_SESSION['images'])){
	$_SESSION['images']['find_filename'] = '';
	$_SESSION['images']['find_caption'] = '';
	$_SESSION['images']['limit'] = "50";
	$_SESSION['images']['p'] = 1;
}
	if(!isset($_SESSION['images']['p'])): $_SESSION['images']['p'] = 1; endif;
	if(isset($_REQUEST['p'])): $_SESSION['images']['p'] = $_REQUEST['p']; endif;


function createthumb($filename,$maxw,$maxh){
	global $_SERVER;

	$newname = explode('.',$filename);
	$ext = pathinfo($filename,  PATHINFO_EXTENSION);

	if(strtolower($ext) == "gif"){
		$src_img = imagecreatefromgif($_SERVER['DOCUMENT_ROOT'].'/images/'.$filename);
	} elseif(strtolower($ext) == "png"){
		$src_img = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].'/images/'.$filename);
	} else {
		$src_img = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'].'/images/'.$filename);
	}



	$imgsize = array( imageSX($src_img) , imageSY($src_img) );
	$oldsize = $imgsize;

	if($imgsize[0] > $maxw){
		$newsize[0] = $maxw;
		$d = ($newsize[0] / $imgsize[0]);
		$newsize[1] = ceil($imgsize[1] * $d);
		$imgsize = $newsize;
	}
	if($imgsize[1] > $maxh){
		$newsize[1] = $maxh;
		$d = ($newsize[1] / $imgsize[1]);
		$newsize[0] = ceil($imgsize[0] * $d);
		$imgsize = $newsize;
	}

	$thumb = ImageCreateTrueColor($imgsize[0],$imgsize[1]);

	imagecopyresampled($thumb,$src_img,0,0,0,0,$imgsize[0],$imgsize[1],$oldsize[0],$oldsize[1]);
	if(strtolower($ext) == "gif"){
		$newFilename = $newname[0].'_small.'.$ext;
		imagegif($thumb,$_SERVER['DOCUMENT_ROOT'].'/images/'.$newFilename);
	} elseif(strtolower(end($newname)) == "png"){
		$newFilename = $newname[0].'_small.'.$ext;
		imagepng($thumb,$_SERVER['DOCUMENT_ROOT'].'/images/'.$newFilename);
	} else {
		$newFilename = $newname[0].'_small.'.$ext;
		imagejpeg($thumb,$_SERVER['DOCUMENT_ROOT'].'/images/'.$newFilename);
	}
	@chmod($_SERVER['DOCUMENT_ROOT'].'/images/'.$newFilename,0777);

	imagedestroy($thumb);
	imagedestroy($src_img);
}

$successmsg = array();
$errormsg = array();
if(isset($_FILES['newimg']) && $_FILES['newimg'] != ""){
	if($_FILES['newimg']['error'] != 4 && $_FILES['newimg']['tmp_name'] != ""){
	$filename = $_FILES['newimg']['name'];
	$filename = str_replace(' ','_',$filename);
	$filename = str_replace('%20','_',$filename);

	@mysql_query('INSERT INTO `images`(`filename`,`caption`,`added`) VALUES("'.$filename.'","'.$_REQUEST['caption'].'","'.$time.'")');
	$newid = mysql_insert_id();
	$thiserror = mysql_error();
	if($thiserror == ""){
		array_push($successmsg,'Added image to database.');
		if(move_uploaded_file($_FILES['newimg']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/images/'.$filename)){
			@chmod($_SERVER['DOCUMENT_ROOT'].'/images/'.$filename,0777);
			array_push($successmsg,'Uploaded image "'.$_FILES['newimg']['name'].'".');
			createthumb($filename,240,200);
			array_push($successmsg,'Created thumbnail for "'.$filename.'".');
			} else {
			array_push($errormsg,'There was an error attempting to upload image "'.$_FILES['newimg']['name'].'".');
			}
		$_REQUEST['editimage'] = $newid;
		$_SESSION['images']['find_filename'] = $filename;
		$_SESSION['images']['find_caption'] = '';
	} else {
		array_push($errormsg,$thiserror);
	}

	} elseif($_FILES['newimg']['error'] != 0 && $_FILES['newimg']['error'] != 4){
	array_push($errormsg,'Error for file '.$_FILES['newimg']['name'].': '.$uploaderrors[$_FILES['newimg']['error']]);
	}
}


if(isset($_POST['utaction']) && $_POST['utaction'] == "editimg" && isset($_REQUEST['editimage']) && $_REQUEST['editimage'] != "0" && $_REQUEST['editimage'] != ""){

$query = 'SELECT * FROM `images` WHERE `id` = "'.$_REQUEST['editimage'].'" LIMIT 1';
$result = @mysql_query($query);
$image = mysql_fetch_assoc($result);
$forsmll = explode(".",$image['filename']);
$thisthumb = reset($forsmll).'_small.'.end($forsmll);

if(isset($_POST['imgdel']) && $_POST['imgdel'] != "" && $_POST['imgdel'] != "no"){
$query = 'DELETE FROM `images` WHERE `id` = "'.$_REQUEST['editimage'].'" LIMIT 1'; @mysql_query($query);
$thiserror = mysql_error();
if($thiserror != ""){
	array_push($errormsg,'Error: '.$thiserror);
	} else {
	array_push($successmsg,'Removed image (id '.$_REQUEST['editimage'].') from database.');
	if($_POST['imgdel'] == "both"){
	unlink($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisthumb);
	if(unlink($_SERVER['DOCUMENT_ROOT'].'/images/'.$image['filename'])){
		array_push($successmsg,'Removed image (id '.$_REQUEST['editimage'].') from server.');
		}
	} //End Both If Statement
	} //END THISERROR IF STATEMENT
	$_REQUEST['editimage'] = '';
} else {

$query = 'UPDATE `images` SET `caption` = "'.$_POST['caption'].'" WHERE `id` = "'.$_REQUEST['editimage'].'" LIMIT 1'; @mysql_query($query);
$thiserror = mysql_error();
if($thiserror != ""): array_push($errormsg,'Error: '.$thiserror); else: array_push($successmsg,'Updated caption for image (id '.$_REQUEST['editimage'].').'); endif;

if(isset($_POST['imgnewname']) && trim($_POST['imgnewname']) != ""){
	$query = 'UPDATE `images` SET `filename` = "'.$_POST['imgnewname'].'" WHERE `id` = "'.$_REQUEST['editimage'].'" LIMIT 1'; @mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror != ""){
		array_push($errormsg,'Error: '.$thiserror);
	} else {
		array_push($successmsg,'Renamed file for image (id '.$_REQUEST['editimage'].').');
		$forsmll = explode(".",$_POST['imgnewname']);
		$newsmll = reset($forsmll).'_small.'.end($forsmll);
		//Rename Thumbnail
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisthumb)){
			rename($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisthumb,$_SERVER['DOCUMENT_ROOT'].'/images/'.$newsmll);
		}
		$thisthumb = $newsmll;
		//Rename Large
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.$image['filename'])){
			rename($_SERVER['DOCUMENT_ROOT'].'/images/'.$image['filename'],$_SERVER['DOCUMENT_ROOT'].'/images/'.$_POST['imgnewname']);
		}
		$image['filename'] = $_POST['imgnewname'];
	}
}

//REPLACE THUMBNAIL
if(isset($_FILES['imgthumb']) && $_FILES['imgthumb'] != ""){
	if($_FILES['imgthumb']['error'] != 4 && $_FILES['imgthumb']['tmp_name'] != ""){
	$thisfile = $thisthumb;
	if(move_uploaded_file($_FILES['imgthumb']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/images/'.$thisfile)){
		@chmod($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisfile,0777);
		array_push($successmsg,'Uploaded image "'.$_FILES['imgthumb']['name'].'" to "'.$thisfile.'".');
		} else {
		array_push($errormsg,'There was an error attempting to upload image "'.$_FILES['imgthumb']['name'].'".');
		}
	} elseif($_FILES['imgthumb']['error'] != 0 && $_FILES['imgthumb']['error'] != 4){
	array_push($errormsg,'Error for file '.$_FILES['imgthumb']['name'].': '.$uploaderrors[$_FILES['imgthumb']['error']]);
	}
}

//GENERATE THUMBNAIL
if(isset($_REQUEST['genthumb']) && $_REQUEST['genthumb'] == "y"){
	createthumb($image['filename'],240,200);
}

//REPLACE LARGE
if(isset($_FILES['imglarge']) && $_FILES['imglarge'] != ""){
	if($_FILES['imglarge']['error'] != 4 && $_FILES['imglarge']['tmp_name'] != ""){
	$thisfile = $image['filename'];
	if(move_uploaded_file($_FILES['imglarge']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/images/'.$thisfile)){
		@chmod($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisfile,0777);
		array_push($successmsg,'Uploaded image "'.$_FILES['imglarge']['name'].'".');
		createthumb($filename,240,200);
		} else {
		array_push($errormsg,'There was an error attempting to upload image "'.$_FILES['imglarge']['name'].'".');
		}
	} elseif($_FILES['imglarge']['error'] != 0 && $_FILES['imglarge']['error'] != 4){
	array_push($errormsg,'Error for file '.$_FILES['imglarge']['name'].': '.$uploaderrors[$_FILES['imglarge']['error']]);
	}
}

} //End Delete If Statement

} //END EDIT IMAGE IF STATEMENT


$maxitems = (8 * 4); //rows * 4

$link = 'choose='.$_REQUEST['choose'].'&func='.$_REQUEST['func'].'&fid='.$_REQUEST['fid'].'&';


if(isset($_REQUEST['editimage']) && $_REQUEST['editimage'] != "0" && $_REQUEST['editimage'] != ""){


echo '<FORM NAME="imgform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="MAX_FILE_SIZE" VALUE="10485760">
<INPUT TYPE="hidden" NAME="editimage" VALUE="'.$_REQUEST['editimage'].'">
<INPUT TYPE="hidden" NAME="utaction" VALUE="editimg">
<INPUT TYPE="hidden" NAME="choose" VALUE="'.$_REQUEST['choose'].'">
<INPUT TYPE="hidden" NAME="func" VALUE="'.$_REQUEST['func'].'">
<INPUT TYPE="hidden" NAME="fid" VALUE="'.$_REQUEST['fid'].'">'."\n\n";

if($_REQUEST['editimage'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['editimage'] == "*new*"){
	$fillform = array('id' => '*new*');
	} else {
	$query = 'SELECT * FROM `images` WHERE `id` = "'.$_REQUEST['editimage'].'" LIMIT 1';
	$result = @mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}

printmsgs($successmsg,$errormsg);

	$checksmll = explode(".",getval('filename'));
	if( file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.reset($checksmll).'_small.'.end($checksmll)) ){
		$checksmll = 'y';
		} else {
		$checksmll = 'n';
		}

	$img = imgformat(getval('filename'),680,700);

if($checksmll == "n"): echo '<FONT FACE="Arial" SIZE="2" COLOR="#FF0000"><B>This image has no thumbnail file!</B></FONT><BR>'; endif;

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">File name</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('filename').'<INPUT TYPE="hidden" NAME="imgoldname" VALUE="'.getval('filename').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Rename file</TD><TD STYLE="font-family:Arial; font-size:10px; color:#333333;"><INPUT TYPE="text" NAME="imgnewname" STYLE="width:300px;" VALUE="'.getval('filename').'"><BR><I>Please be sure to include the correct extention (such as ".jpg" or ".gif").</I></TD></TR>'."\n";
	$thumbbg = bgcolor('');
	echo '<TR STYLE="background:#'.$thumbbg.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Replace thumbnail</TD><TD STYLE="font-family:Arial; font-size:10px; color:#333333;"><INPUT TYPE="file" NAME="imgthumb" STYLE="width:300px;"><BR><I>Must be the same image type.</I></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.$thumbbg.'"><TD STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="genthumb" ID="genthumb" VALUE="y"></TD><TD STYLE="font-family:Arial; font-size:10pt;"><I><LABEL FOR="genthumb">Generate a new thumbnail on "Save Changes"</LABEL></I></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Replace image</TD><TD><INPUT TYPE="file" NAME="imglarge" STYLE="width:300px;"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Caption</TD><TD><TEXTAREA NAME="caption" STYLE="width:300px; height:100px;">'.getval('caption').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Delete image</TD><TD STYLE="font-family:Arial; font-size:10px;"><INPUT TYPE="radio" NAME="imgdel" ID="imgdel1" VALUE="no" CHECKED>&nbsp;<LABEL FOR="imgdel1">No</LABEL><BR><INPUT TYPE="radio" NAME="imgdel" ID="imgdel2" VALUE="dbonly">&nbsp;<LABEL FOR="imgdel2">From DB, but not from server</LABEL><BR><INPUT TYPE="radio" NAME="imgdel" ID="imgdel3" VALUE="both">&nbsp;<LABEL FOR="imgdel3">From both DB and server</LABEL></TD></TR>'."\n";
	echo '</TABLE>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save Changes"><BR><BR>'."\n\n";

echo '<A NAME="view_image"></A><IMG SRC="../images/'.getval('filename').'?'.rand(0,1000).'" BORDER="0" STYLE="width:'.$img['w'].'; height:'.$img['h'].'; border:1px solid #666666;"><BR><BR>'."\n\n";

if($_REQUEST['choose'] != "mng"){

	if(isset($_REQUEST['func']) && $_REQUEST['func'] == "chgmap"){
		$thisimage = imgform(getval('filename'),120,180);
		} elseif(isset($_REQUEST['func']) && $_REQUEST['func'] == "addimg"){
		$thisimage = imgform(getval('filename'),180,90);
		} else {
		$thisimage = imgform(getval('filename'),90,90);
		}

	echo '<INPUT TYPE="button" VALUE="';
		if($_REQUEST['choose'] == "m"){
			echo 'Add Image';
			} else {
			echo 'Choose';
			}
		echo '" onClick=\'javascript:opener.'.$_REQUEST['func'].'("'.str_replace('"','\"',getval('filename')).'","'.str_replace('"','\"',$thisimage['filename']).'","'.$thisimage['w'].'","'.$thisimage['h'].'","'.$image['id'].'"';
		if(isset($_REQUEST['fid']) && $_REQUEST['fid'] != ""): echo ',"'.$_REQUEST['fid'].'"'; endif;
		echo ');';
		if($_REQUEST['choose'] != "m"): echo ' window.close();'; endif;
		echo '\'><BR><BR>'."\n\n";
	}

echo '<FONT FACE="Arial" SIZE="2"><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.$_SESSION['images']['p'].'">Return to image listing</A></FONT>'."\n\n";

echo '</FORM>'."\n\n";


} else {


//CREATE FILENAME ARRAY
$find_filename = $_SESSION['images']['find_filename'];
if(strstr($find_filename,"|")){
	$find_filename = explode('|',$find_filename);
	} elseif(strstr($find_filename,"\n")){
	$find_filename = explode("\n",$find_filename);
	} else {
	$find_filename = array($find_filename);
	}
//TRIM FILENAME ENTIRES
	for($i=0; $i<count($find_filename); $i++){
	$find_filename[$i] = trim($find_filename[$i]);
	if(strstr($find_filename[$i],"/")){
		$find_filename[$i] = explode("/",$find_filename[$i]);
		$find_filename[$i] = end($find_filename[$i]);
		}
	$find_filename[$i] = str_replace('%20',' ',$find_filename[$i]);
	}

//GET IMAGES
$images = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `images` WHERE 1=1';
		$query .= ' AND (`filename` LIKE "%'.implode('%" OR `filename` LIKE "%',$find_filename).'%")';
	if(isset($_SESSION['images']['find_caption']) && $_SESSION['images']['find_caption'] != ""): $query .= ' AND `caption` LIKE "%'.$_SESSION['images']['find_caption'].'%"'; endif;
	$query .= ' ORDER BY `filename` ASC LIMIT '.(($_SESSION['images']['p']-1)*$maxitems).','.$maxitems;
$result = @mysql_query($query);
//echo $query;
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($images,$row);
	//$dbimages[$row['filename']] = $row;
	}
/*
if(is_array($find_filename) && count($find_filename) > 1){
$images = array();
for($i=0; $i<count($_REQUEST['find_filename']); $i++){
	if(isset($dbimages[$_REQUEST['find_filename'][$i]])){
	array_push($images,[$_REQUEST['find_filename'][$i]]);
	}
	}
}
*/


echo '<FORM METHOD="post" NAME="upform" ACTION="'.$_SERVER['PHP_SELF'].'" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="utaction" VALUE="findimg">
<INPUT TYPE="hidden" NAME="choose" VALUE="'.$_REQUEST['choose'].'">
<INPUT TYPE="hidden" NAME="func" VALUE="'.$_REQUEST['func'].'">
<INPUT TYPE="hidden" NAME="fid" VALUE="'.$_REQUEST['fid'].'">'."\n\n";

printmsgs($successmsg,$errormsg);

echo '<TABLE BORDER="0" WIDTH="93%" BGCOLOR="#CCCCCC">
<TR><TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;">Find images where <B>file name</B> contains <INPUT TYPE="text" NAME="find_filename" VALUE="'.stripslashes($_SESSION['images']['find_filename']).'" STYLE="font-size:9pt; width:100px;"> and <B>caption</B> contains <INPUT TYPE="text" NAME="find_caption" VALUE="'.stripslashes($_SESSION['images']['find_caption']).'" STYLE="font-size:9pt; width:100px;"> <INPUT TYPE="submit" STYLE="font-size:9pt;" VALUE="Find">';
	if($_SESSION['images']['find_filename'] != "" || $_SESSION['images']['find_caption'] != ""): echo '&nbsp;&nbsp;<INPUT TYPE="button" STYLE="font-size:9pt;" VALUE="View ALL" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$link.'utaction=findimg&find_filename=&find_caption=\';">'; endif;
	echo '</TD></TR>
</TABLE>'."\n";

echo '<TABLE BORDER="0"><TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2">Add a new image:</FONT></TD><TD><INPUT TYPE="file" SIZE="40" NAME="newimg" ID="newimg"> <INPUT TYPE="submit" VALUE="Upload"></TD></TR></TABLE>'."\n";

if(count($images) > 0){

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $maxitems);
if($numpages> 0 && $_SESSION['images']['p'] > $numpages): $_SESSION['images']['p'] = $numpages; endif;

echo '<script language="javascript"><!--

function go(url){ window.location=url; }

var imgs = new Array();'."\n";

	for($onimg=0; $onimg<count($images); $onimg++){
	if(isset($_REQUEST['func']) && $_REQUEST['func'] == "chgmap"){
		$thisimage = imgform($images[$onimg]['filename'],120,180);
		} elseif(isset($_REQUEST['func']) && $_REQUEST['func'] == "addimg"){
		$thisimage = imgform($images[$onimg]['filename'],180,90);
		} else {
		$thisimage = imgform($images[$onimg]['filename'],90,90);
		}
	echo 'imgs['.$onimg.'] = new Array("'.str_replace('"','\"',$images[$onimg]['filename']).'","'.str_replace('"','\"',$thisimage['filename']).'","'.$thisimage['w'].'","'.$thisimage['h'].'","'.$images[$onimg]['id'].'");'."\n";   //'."'".'<A HREF="../images/'.$images[$onimg]['filename'].'" TARGET="_blank"><IMG SRC="../images/'.$thisimage.'" BORDER="0" WIDTH="'.$newsize[0].'" HEIGHT="'.$newsize[1].'" CLASS="imgbord" ALT="'.addslashes($images[$onimg]['caption']).'"></A><INPUT TYPE="hidden" NAME="newimg[]" VALUE="'.$images[$onimg]['id'].'"> '."'".';'."\n";
	}
	echo "\n";

echo 'function tosource(i){
	opener.'.$_REQUEST['func'].'(imgs[i][0],imgs[i][1],imgs[i][2],imgs[i][3],imgs[i][4]';
	if(isset($_REQUEST['fid']) && $_REQUEST['fid'] != ""): echo ',"'.$_REQUEST['fid'].'"'; endif;
	echo ');
	//tosource(imgs[i][0],imgs[i][1],imgs[i][2],imgs[i][3],imgs[i][4]);
	//alert(imgs[i][0]+" "+imgs[i][1]+" "+imgs[i][2]+" "+imgs[i][3]+" "+imgs[i][4]);'."\n";
	if($_REQUEST['choose'] != "m"): echo '	window.close()'."\n"; endif;
	echo '	}'."\n\n";

echo 'function addpage(){
	for(i=0; i<imgs.length; i++){
	opener.'.$_REQUEST['func'].'(imgs[i][0],imgs[i][1],imgs[i][2],imgs[i][3],imgs[i][4]';
	if(isset($_REQUEST['fid']) && $_REQUEST['fid'] != ""): echo ',"'.$_REQUEST['fid'].'"'; endif;
	echo ');
	//tosource(imgs[i][0],imgs[i][1],imgs[i][2],imgs[i][3],imgs[i][4]);
	}
	window.close();
	//clwin();
	}'."\n\n";

echo '//--></script>'."\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLPADDING="3" CELLSPACING="0">'."\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left">';
	if($_SESSION['images']['p'] > 1): echo '<FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['images']['p']-1).'">&lt; Previous Page</A></B></FONT>'; endif;
	echo '</TD><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">'.$numitems.' images total - Viewing page <SELECT ID="p1" CLASS="smallsel" onChange="go('."'".$_SERVER['PHP_SELF'].'?'.$link.'p='."'+document.getElementById('p1').value".');">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['images']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD><TD ALIGN="right">';
	if($_SESSION['images']['p'] < $numpages): echo '<FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['images']['p']+1).'">Next Page &gt;</A></B></FONT>'; endif;
	echo '</TD></TR>'."\n\n";
	} //End Page Listing

	echo '<TR BGCOLOR="#'.bgcolor('').'">'."\n";
	$col=0;
	for($i=0; $i<count($images); $i++){
	$lrgsize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/'.$images[$i]['filename']);
	$checksmll = explode(".",$images[$i]['filename']);
	if( file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.reset($checksmll).'_small.'.end($checksmll)) ){
		$thisimage = reset($checksmll).'_small.'.end($checksmll);
		$checksmll = 'y';
		} else {
		$thisimage = $images[$i]['filename'];
		$checksmll = 'n';
		}
	$img = imgformat($thisimage,150,120);

	if($col > 3){
		echo "\t".'</TR><TR BGCOLOR="#'.bgcolor('').'">'."\n";
		$col = 0;
		}
	echo "\t".'<TD ALIGN="center" VALIGN="bottom" STYLE="width:25%; font-family:Arial; font-size:11px; color:#666666;">';
		echo wordwrap($images[$i]['filename'], 25, "<BR>", true).'<BR>';
		echo '<A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.$_SESSION['images']['p'].'&editimage='.$images[$i]['id'].'#view_image"><IMG SRC="../images/'.$img['filename'].'" BORDER="0" STYLE="width:'.$img['w'].'; height:'.$img['h'].'; border:1px solid #666666;" ALT="'.$images[$i]['caption'].'" TITLE="'.$images[$i]['caption'].'"></A><BR>';
		echo $lrgsize[0].' x '.$lrgsize[1].'<BR>';
		if($checksmll == "n"): echo '<SPAN STYLE="color:#FF0000"><I>No thumbnail file</I></SPAN><BR>'."\n"; endif;
		if($_REQUEST['choose'] == "mng"){
		// echo 'Id: '.$images[$i]['id'].'<BR>';
		echo '[ <A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.$_SESSION['images']['p'].'&editimage='.$images[$i]['id'].'">Edit</A> ]';
		} else {
		echo '<INPUT TYPE="button" VALUE="';
		if($_REQUEST['choose'] == "m"){
			echo 'Add Image';
			} else {
			echo 'Choose';
			}
		echo '" STYLE="font-size:9pt;" onClick="tosource('."'".$i."'".')">';
		echo '<BR>[ <A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.$_SESSION['images']['p'].'&editimage='.$images[$i]['id'].'">Edit</A> ]';
		}
		echo '</TD>'."\n";
	$col++;
	}
	echo "\t".'</TR>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left">';
	if($_SESSION['images']['p'] > 1): echo '<FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['images']['p']-1).'">&lt; Previous Page</A></B></FONT>'; endif;
	echo '</TD><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2">'.$numitems.' images total - Viewing page <SELECT ID="p2" CLASS="smallsel" onChange="go('."'".$_SERVER['PHP_SELF'].'?'.$link.'p='."'+document.getElementById('p2').value".');">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['images']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD><TD ALIGN="right">';
	if($_SESSION['images']['p'] < $numpages): echo '<FONT FACE="Arial" SIZE="2"><B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['images']['p']+1).'">Next Page &gt;</A></B></FONT>'; endif;
	echo '</TD></TR>'."\n\n";
	} //End Page Listing

echo '</TABLE>'."\n\n";

if(count($images) > 0 && $_REQUEST['choose'] == "m"): echo '<INPUT TYPE="button" VALUE="Add Page" onClick="addpage()"><BR>'."\n\n"; endif;

} else {

echo '<BR><BR><BR><BR><BR><FONT FACE="Arial" SIZE="3" COLOR="#666666">No images found.</FONT>'."\n\n";

} //END COUNT IMAGES IF STATEMENT

echo '</FORM>'."\n\n";

} //END $_REQUEST['editimage'] IF STATEMENT


/*
if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0){

	if(isset($successmsg) && count($successmsg) > 0){
	$logentry = implode("\n",$successmsg);
	$logentry = addslashes($logentry);
	@mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$thisuser['id'].'","'.$pageid.'","'.$logentry.'")');
	}
	if(isset($errormsg) && count($errormsg) > 0){
	$logentry = implode("\n",$errormsg);
	$logentry = addslashes($logentry);
	@mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$thisuser['id'].'","'.$pageid.'","Error: '.$logentry.'")');

	//$message = "Time: ".date("m/d/Y h:ia", $time)."\nUser: ".$thisuser."\nPage: ".$pageid."\n\n Log: ".stripslashes($logentry)."\n\n";
	//@mail("errors@bernalwebservices.com", "Park City Rental Error", $message, 'From: "Park City Rental" <errors@bernalwebservices.com>');
	}
}
*/

?>