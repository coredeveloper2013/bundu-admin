<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 7;  echo 'REMOVE THIS LINE!!!';

$working = 0;

$pageid = "3_itinerary";
require("validate.php");
require("header.php");

$today = mktime(0,0,0,date("j",$time),date("n",$time),date("Y",$time));


if(!isset($_SESSION['itinerary']['p'])): $_SESSION['itinerary']['p'] = 1; endif;
if(!isset($_SESSION['itinerary']['sortby'])): $_SESSION['itinerary']['sortby'] = 'itinerary.`nickname`'; endif;
	$sortby = array('itinerary.`id`'=>'ID','itinerary.`nickname`'=>'Nickname','itinerary.`title`'=>'Title');
if(!isset($_SESSION['itinerary']['sortdir'])): $_SESSION['itinerary']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['itinerary']['limit'])): $_SESSION['itinerary']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['itinerary']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['itinerary']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['itinerary']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['itinerary']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['itinerary']['p'] = '1';
	}

//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

if(!isset($_POST['delit'])){ $_POST['delit'] = array(); }
$numremoved = 0; $numupdated = 0;

foreach($_POST['id'] as $key => $id){
	$_POST['nickname'][$key] = encodeSQL(trim($_POST['nickname'][$key]));
	$_POST['title'][$key] = encodeSQL(trim($_POST['title'][$key]));
	$_POST['map'][$key] = encodeSQL(trim($_POST['map'][$key]));
	$_POST['ml'][$key] = encodeSQL(trim($_POST['ml'][$key]));
	$_POST['time'][$key] = encodeSQL(trim($_POST['time'][$key]));
	$_POST['comments'][$key] = encodeSQL(trim($_POST['comments'][$key]));
	$_POST['youtube'][$key] = encodeSQL(trim($_POST['youtube'][$key]));

	//INSERT NEW ---------------------
	if(substr($id,0,3) == 'new'){ if(trim($_POST['nickname'][$key]) != "" || trim($_POST['title'][$key]) != "" || trim($_POST['ml'][$key]) != "" || trim($_POST['time'][$key]) != "" || trim($_POST['comments'][$key]) != "" || trim($_POST['youtube'][$key]) != "" || $_POST['map'][$key] > 0 || count($_POST['imgs'.$id]) > 0){
	$query = 'INSERT INTO `itinerary`(`nickname`,`title`,`map`,`ml`,`time`,`comments`,`youtube`)';
		$query .= ' VALUES ("'.$_POST['nickname'][$key].'","'.$_POST['title'][$key].'","'.$_POST['map'][$key].'","'.$_POST['ml'][$key].'","'.$_POST['time'][$key].'","'.$_POST['comments'][$key].'","'.$_POST['youtube'][$key].'")';
		@mysql_query($query);
		$thiserror = mysql_error();
		$id = mysql_insert_id();
		$_REQUEST['edit'] = $id;
		if($thiserror == ""): $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;

	//UPDATE ESTABLISHED -------------
	}} else { if(trim($_POST['title'][$key]) == "" && trim($_POST['ml'][$key]) == "" && trim($_POST['time'][$key]) == "" && trim($_POST['comments'][$key]) == "" && trim($_POST['youtube'][$key]) == "" && $_POST['map'][$key] < 1 && count($_POST['imgs'.$id]) < 1){
	array_push($_POST['delit'],$id);
	} else {
	$query = 'UPDATE `itinerary` SET `nickname` = "'.$_POST['nickname'][$key].'", `title` = "'.$_POST['title'][$key].'", `map` = "'.$_POST['map'][$key].'", `ml` = "'.$_POST['ml'][$key].'", `time` = "'.$_POST['time'][$key].'", `comments` = "'.$_POST['comments'][$key].'", `youtube` = "'.$_POST['youtube'][$key].'"';
		$query .= ' WHERE `id` = "'.$id.'" LIMIT 1';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""): $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
	}}

	//IMAGES -------------------------
	@mysql_query('DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.$id.'"');
	if(isset($_POST['imgs'.$_POST['id'][$key]])){
	foreach($_POST['imgs'.$_POST['id'][$key]] as $ord => $imgid){
		$query = 'INSERT INTO `images_assoc_itinerary`(`itid`,`imgid`,`order`) VALUES("'.$id.'","'.$imgid.'","'.$ord.'") ON DUPLICATE KEY UPDATE `order` = "'.$ord.'"';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""): array_push($errormsg,$thiserror); endif; //$imgupdated=($imgupdated+mysql_affected_rows()); else:
		} //End img foreach
		} //End if statement

	} //End Ids foreach

	//REMOVE STEPS -------------------
	/*if(isset($_POST['delit']) && count($_POST['delit']) > 0){
		foreach($_POST['delit'] as $del){
			$query = 'DELETE FROM `itinerary` WHERE `id` = "'.$del.'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""): $numremoved=($numremoved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			@mysql_query('DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.$del.'"');
			} //End For Loop
	} //END delit IF STATEMENT*/

	if($numremoved > 0): array_push($successmsg,$numremoved.' itinerary entires were removed. ('.$_POST['edit'].')'); endif;
	if($numupdated > 0): array_push($successmsg,$numupdated.' itinerary entries were added/updated. ('.$_POST['edit'].')'); endif;

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){

	//Remove image associations
	$query = 'DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.implode('" OR `itid` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	//if($thiserror == ""){ array_push($successmsg,mysql_affected_rows().' translations were deleted.'); } else { array_push($errormsg,$thiserror); }

	//Delete Translations
	$query = 'DELETE FROM `itinerary_translations` WHERE `itid` = "'.implode('" OR `itid` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){ array_push($successmsg,mysql_affected_rows().' translations were deleted.'); } else { array_push($errormsg,$thiserror); }

	//Delete Tour Associations
	$query = 'DELETE FROM `tours_assoc` WHERE `type` = "i" AND (`typeid` = "'.implode('" OR `typeid` = "',$_POST['selitems']).'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){ array_push($successmsg,mysql_affected_rows().' itinerary associations were removed from tours.'); } else { array_push($errormsg,$thiserror); }

	//Delete itinerary items
	$query = 'DELETE FROM `itinerary` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){ array_push($successmsg,mysql_affected_rows().' itinerary items were deleted.'); } else { array_push($errormsg,$thiserror); }

}


//if(!isset($_REQUEST['edit'])): $_REQUEST['edit'] = "*new*"; endif;

echo '<CENTER>'."\n\n";

echo '<IMG SRC="spacer.gif" BORDER="0" HEIGHT="12"><BR><FONT FACE="Arial" SIZE="5"><U>Edit Itinerary Entry</U></FONT><BR><BR>'."\n\n";

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

//GET ITINERARY
$itinerary = array();
$it_ids = array();
if($_REQUEST['edit'] == '*new*'){
		$itinerary['inew'] = array(
		'id' => 'new',
		'nickname' => '',
		'routeid' => '0',
		'title' => '',
		'map' => '0',
		'ml' => '',
		'time' => '',
		'comments' => '',
		'youtube' => '',
		'images_assoc' => array()
		);
	array_push($it_ids,'new');
	} else {
	$query = 'SELECT * FROM `itinerary` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$itinerary['i'.$row['id']] = $row;
		$itinerary['i'.$row['id']]['images_assoc'] = array();
		array_push($it_ids,$row['id']);
		}
	}

//GET IMAGES
$query = 'SELECT images_assoc_itinerary.* FROM `images_assoc_itinerary`,`itinerary` WHERE images_assoc_itinerary.`itid` = itinerary.`id` AND itinerary.`id` = "'.$_REQUEST['edit'].'" ORDER BY itinerary.`id` ASC, images_assoc_itinerary.`order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($itinerary['i'.$row['itid']]['images_assoc'],$row['imgid']);
	}
	//echo '<PRE>'; print_r($it_images); echo '</PRE>';
$images = array();
$query = 'SELECT DISTINCT images.* FROM `images`,`images_assoc_itinerary`,`itinerary` WHERE (images.`id` = images_assoc_itinerary.`imgid` AND images_assoc_itinerary.`itid` = itinerary.`id` AND itinerary.`id` = "'.$_REQUEST['edit'].'") OR (images.`id` = itinerary.`map` AND itinerary.`id` = "'.$_REQUEST['edit'].'")';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$images['i'.$row['id']] = $row;
	$images['i'.$row['id']]['data'] = imgform($row['filename'],180,90);
	}


function build_it_entry($entry,$count){
	global $images;
	$code = '';

	$code .= '<INPUT TYPE="hidden" NAME="id[]" VALUE="'.$entry['id'].'">';
	$code .= '<DIV STYLE="padding:4px; font-family:Arial; font-size:9pt; font-weight:bold; text-align:center;">Nickname: <INPUT TYPE="text" NAME="nickname[]" ID="nickname_'.$entry['id'].'" VALUE="'.$entry['nickname'].'" STYLE="width:500px;"></DIV>'."\n";
	$code .= '<DIV STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:9pt; font-weight:bold; text-align:center; padding-bottom:4px;">';
		$code .= 'Title: <INPUT TYPE="text" NAME="title[]" ID="title_'.$entry['id'].'" VALUE="'.$entry['title'].'" STYLE="font-size:9pt; width:260px;">&nbsp;&nbsp;';
		$code .= 'Miles/Km: <INPUT TYPE="text" NAME="ml[]" ID="ml_'.$entry['id'].'" VALUE="'.$entry['ml'].'" onKeyUp="to_km(this.value,document.getElementById(\'km_'.$entry['id'].'\'));" STYLE="font-size:9pt; width:50px;">/<INPUT TYPE="text" ID="km_'.$entry['id'].'" VALUE="';
			if($entry['ml'] != "" && $entry['ml'] > 0): $code .= number_format(($entry['ml']*1.609), 2, '.', ''); endif;
			$code .= '" onKeyUp="to_miles(this.value,document.getElementById(\'ml_'.$entry['id'].'\'));" STYLE="font-size:9pt; width:50px;">&nbsp;&nbsp;';
		$code .= 'Time: <INPUT TYPE="text" NAME="time[]" ID="time_'.$entry['id'].'" VALUE="'.$entry['time'].'" STYLE="font-size:9pt; width:120px;">';
		$code .= '</DIV>';

	/*
	$headline = array();
		if($entry['title'] != ""): array_push($headline,$entry['title']); endif;
		$dist = "";
		if($entry['ml'] != ""){
			$dist .= $entry['ml'].' mi';
			if($entry['ml'] != "" && $entry['ml'] > 0): $dist .= ' / '; endif;
			}
		if($entry['ml'] != "" && $entry['ml'] > 0): $dist .= number_format(($entry['ml']*1.609), 2, '.', '').' km'; endif;
		if($dist != ""): array_push($headline,$dist); endif;
		if($entry['time'] != ""): array_push($headline,$entry['time']); endif;
		$headline = implode(' &nbsp;-&nbsp; ',$headline);
	if($headline != ""): $code .= '<DIV STYLE="padding:4px; padding-left:8px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold;">'.$headline.'</DIV>'."\n"; endif;
	*/

	$code .= '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR><TD ALIGN="center" VALIGN="top" STYLE="padding:0px; padding-left:8px; padding-top:8px; width:120px; font-family:Arial; font-size:8pt;">';
		if($entry['map'] != "0"){
			$thisimage = imgform($images['i'.$entry['map']]['filename'],120,180);
			$code .= '<A HREF="../images/'.$images['i'.$entry['map']]['filename'].'" ID="map'.$entry['id'].'href" TARGET="_blank"><IMG SRC="../images/'.$thisimage['filename'].'" ID="map'.$entry['id'].'img" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" STYLE="border: #666666 solid 1px;" ALT="Map"></A>';
			} else {
			$code .= '<A ID="map'.$entry['id'].'href" TARGET="_blank"><IMG ID="map'.$entry['id'].'img" BORDER="0" STYLE="display:none; border: #666666 solid 1px;" ALT="Map"></A>';
			}
		$code .= '<BR><INPUT TYPE="hidden" ID="map'.$entry['id'].'" NAME="map[]" VALUE="'.$entry['map'].'"><INPUT TYPE="button" VALUE="Set map" STYLE="font-size:8pt; width:90px;" onClick="findimgs(\'o\',\'chgmap\',\'map'.$entry['id'].'\');"><BR><INPUT TYPE="button" VALUE="Remove map" STYLE="font-size:8pt; width:90px;" onClick="rmvmap(\'map'.$entry['id'].'\');">';
		$code .= '</TD><TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-right:0px; padding-bottom:0px; font-family:Arial,Helvetica,sans-serif; font-size:9pt;">';
		$code .= '<TEXTAREA NAME="comments[]" ID="comments_'.$entry['id'].'" STYLE="font-size:9pt; width:100%; height:220px;">'.$entry['comments'].'</TEXTAREA>';
		//$code .= nl2br($entry['comments']);

		$code .= '<DIV ID="img_assoc'.$entry['id'].'" STYLE="text-align:center;">';
		if(is_array($entry['images_assoc']) && count($entry['images_assoc']) > 0){
			foreach($entry['images_assoc'] as $key => $img){
				$thisimage = imgform($images['i'.$img]['filename'],180,90);
				$code .= ' <INPUT TYPE="hidden" NAME="imgs'.$entry['id'].'[]" VALUE="'.$img.'">';
				//$code .= '<A HREF="../images/'.$images['i'.$img]['filename'].'" TARGET="_blank" TITLE="'.$images['i'.$img]['caption'].'">';
				$code .= '<IMG SRC="../images/'.$thisimage['filename'].'" ID="'.$entry['id'].'_'.$key.'" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" ALT="'.$images['i'.$img]['caption'].'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(this); showdelimg(this,\''.$entry['id'].'\',\''.$key.'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
				//$code .= '</A>';
				}
			} //End images_assoc if statement
			$code .= '</DIV>';
			$code .= '<CENTER><SPAN STYLE="font-family:Arial; font-size:10px; font-weight:normal; color:#666666;">Hover over an image and click on "Remove" to remove.<BR>Click and drag images to change the order in which they appear.</SPAN><BR><INPUT TYPE="button" VALUE="Add images" STYLE="font-size:9pt;" onClick="findimgs(\'m\',\'addimg\',\''.$entry['id'].'\');"></CENTER>';

		$code .= '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Video: (paste embed code below)<TEXTAREA NAME="youtube[]" ID="youtube_'.$entry['id'].'" STYLE="font-size:9pt; width:100%; height:60px;">'.$entry['youtube'].'</TEXTAREA></DIV>';

		$code .= '</TD></TR></TABLE>'."\n";

		$code .= '<DIV STYLE="font-family:Arial; font-size:10px; text-align:center;"><A HREF="#top" STYLE="text-decoration:underline;">TOP</A></DIV>'."\n";

	return $code;
	}


echo '<A NAME="top"></A>'."\n";

echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">
<INPUT TYPE="hidden" NAME="utaction" VALUE="update">
<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_REQUEST['edit'].'">'."\n\n";

//echo '<script src="dragdrop.js" type="text/javascript"></script>'."\n";


?><SCRIPT><!--

var itineraryids = new Array('<? echo implode("','",$it_ids); ?>');
var newids = new Array();

var itineraryimgs = new Array();
<?
	foreach($itinerary as $it){
		$imgs = array_values($it['images_assoc']);
		echo "\t".'itineraryimgs[\'i'.$it['id'].'\'] = new Array(';
			if(count($imgs) > 0): echo '\''.implode("','",$imgs).'\''; endif;
			echo ');';
		}
	?>
	//itineraryimgs['inew'] = new Array();

var image_data = new Array();
<?
	foreach($images as $img){
		$img['filename'] = str_replace("'",'\\\'',$img['filename']);
		$img['data']['filename'] = str_replace("'",'\\\'',$img['data']['filename']);
		$img['caption'] = str_replace("'",'\\\'',$img['caption']);
		echo "\t".'image_data[\'i'.$img['id'].'\'] = new Array();';
		echo ' image_data[\'i'.$img['id'].'\'][\'large\']=\''.$img['filename'].'\';';
		foreach($img['data'] as $key => $val){
			echo ' image_data[\'i'.$img['id'].'\'][\''.$key.'\']=\''.$val.'\';';
			}
		echo ' image_data[\'i'.$img['id'].'\'][\'caption\']=\''.$img['caption'].'\';';
		echo "\n";
		}
	?>

function to_miles(km,obj){
	var miles = eval(km*0.6214);
	obj.value = miles.toFixed(2);
	}

function to_km(miles,obj){
	var km = eval(miles*1.609);
	obj.value = km.toFixed(2);
	}

function addstep(){
	var n = newids.length;
		var nid = 'new'+n;
		newids.push(n);
		itineraryids.push(nid);
		itineraryimgs['i'+nid] = new Array();

	var code = '<INPUT TYPE="hidden" NAME="id[]" VALUE="'+nid+'">';
		code += '<DIV STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:9pt; font-weight:bold; text-align:center; padding-bottom:4px;">';
		code += '<SPAN STYLE="cursor:help;" TITLE="Move this entry to a different position in the itinerary.">Order:</SPAN>';
		code += '<SELECT NAME="step[]" ID="step_'+nid+'" STYLE="font-size:9pt;" onChange="mvstep(this.value,this.parentNode.parentNode.parentNode.rowIndex);">';
				for(i=0; i<itineraryids.length; i++){
				code += '<OPTION VALUE="'+i+'">'+eval(i+1)+'</OPTION>';
				}
				code += '</SELECT>&nbsp;&nbsp;';
		code += 'Title: <INPUT TYPE="text" NAME="title[]" ID="title_'+nid+'" VALUE="" STYLE="font-size:9pt; width:220px;">&nbsp;&nbsp;';
		code += 'Miles/Km: <INPUT TYPE="text" NAME="ml[]" ID="ml_'+nid+'" VALUE="" onKeyUp="to_km(this.value,document.getElementById(\'km_'+nid+'\'));" STYLE="font-size:9pt; width:50px;">/<INPUT TYPE="text" ID="km_'+nid+'" VALUE="" onKeyUp="to_miles(this.value,document.getElementById(\'ml_'+nid+'\'));" STYLE="font-size:9pt; width:50px;">&nbsp;&nbsp;';
		code += 'Time: <INPUT TYPE="text" NAME="time[]" ID="time_'+nid+'" VALUE="" STYLE="font-size:9pt; width:100px;">';
		code += '</DIV>';
		code += '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>';
		code += '<TD ALIGN="center" VALIGN="top" STYLE="padding:0px; padding-left:8px; padding-top:8px; width:120px; font-family:Arial; font-size:8pt;">';
		code += '<A ID="map'+nid+'href" TARGET="_blank"><IMG ID="map'+nid+'img" BORDER="0" STYLE="display:none; border: #666666 solid 1px;" ALT="Map"></A><BR>';
		code += '<INPUT TYPE="hidden" ID="map'+nid+'" NAME="map[]" VALUE="0">';
		code += '<INPUT TYPE="button" VALUE="Set map" STYLE="font-size:8pt; width:90px;" onClick="findimgs(\'o\',\'chgmap\',\'map'+nid+'\');"><BR>';
		code += '<INPUT TYPE="button" VALUE="Remove map" STYLE="font-size:8pt; width:90px;" onClick="rmvmap(\'map'+nid+'\');">';
		code += '</TD>';
		code += '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-right:0px; padding-bottom:0px; font-family:Arial,Helvetica,sans-serif; font-size:9pt;">';
		code += '<TEXTAREA NAME="comments[]" ID="comments_'+nid+'" STYLE="font-size:9pt; width:100%; height:220px;"></TEXTAREA>';
		code += '<DIV ID="img_assoc'+nid+'" STYLE="text-align:center;"></DIV>';
		code += '<CENTER><SPAN STYLE="font-family:Arial; font-size:10px; font-weight:normal; color:#666666;">Hover over an image and click on "Remove" to remove.<BR>Click and drag images to change the order in which they appear.</SPAN><BR><INPUT TYPE="button" VALUE="Add images" STYLE="font-size:9pt;" onClick="findimgs(\'m\',\'addimg\',\''+nid+'\');"></CENTER>';
		code += '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Video: (paste embed code below)<TEXTAREA NAME="youtube[]" ID="youtube_'+nid+'" STYLE="font-size:9pt; width:100%; height:60px;"></TEXTAREA></DIV>';
		code += '</TD>';
		code += '</TR></TABLE>';
		code += '<DIV STYLE="font-family:Arial; font-size:10px; text-align:center;"><A HREF="#top" STYLE="text-decoration:underline;">TOP</A></DIV>';

	var t = document.getElementById('ittable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.backgroundColor = '#CCCCCC';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '14pt';
		d.style.color = '#666666';
		d.style.padding = '4px';
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = '&nbsp;';

		var d = r.insertCell(1);
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = code;

	ref_ord();
	CreateDragContainer();
	}

function mvstep(to,from){
	if(to != from){
		var t = document.getElementById('ittable');
		var oid = itineraryids[from];

		//Save old data
		var ohtml = t.rows[from].cells[1].innerHTML;
		var otitle = document.getElementById('title_'+oid).value;
		var oml = document.getElementById('ml_'+oid).value;
		var okm = document.getElementById('km_'+oid).value;
		var otime = document.getElementById('time_'+oid).value;
		var ocomments = document.getElementById('comments_'+oid).value;
		var oyoutube = document.getElementById('youtube_'+oid).value;

		t.deleteRow(from);
		itineraryids.splice(from,1);

		var r = t.insertRow(to);
		itineraryids.splice(to,0,oid);

		var d = r.insertCell(0);
		d.style.backgroundColor = '#CCCCCC';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '14pt';
		d.style.color = '#666666';
		d.style.padding = '4px';
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = '&nbsp;';

		var d = r.insertCell(1);
		d.style.borderBottom = '4px solid #FFFFFF';

		//Copy old data
		d.innerHTML = ohtml;
		document.getElementById('title_'+oid).value = otitle;
		document.getElementById('ml_'+oid).value = oml;
		document.getElementById('km_'+oid).value = okm;
		document.getElementById('time_'+oid).value = otime;
		document.getElementById('comments_'+oid).value = ocomments;
		document.getElementById('youtube_'+oid).value = oyoutube;

		ref_ord();
		CreateDragContainer();
		}
	}

function rmvstep(obj){
	var i = obj.parentNode.parentNode.rowIndex;

	var c = confirm('Delete itinerary entry '+eval(i+1)+'?');
	if(c){
		document.getElementById('delfields').innerHTML += '<INPUT TYPE="hidden" NAME="delit[]" VALUE="'+itineraryids[i]+'">';

		var t = document.getElementById('ittable');
		itineraryimgs['i'+itineraryids[i]] = new Array();
		itineraryids.splice(i,1);
		t.deleteRow(i);

		ref_ord();
		CreateDragContainer();
		}

	}

function ref_ord(){
	var t = document.getElementById('ittable');

	for(i=0; i<t.rows.length; i++){
		t.rows[i].cells[0].innerHTML = eval(i+1)+'<BR><BR><INPUT TYPE="button" VALUE="X" TITLE="Remove this entry" STYLE="color:#FF0000; width:22px; text-align:center;" onClick="rmvstep(this);">';
		}

	for(i=0; i<itineraryids.length; i++){
		var s = document.getElementById('step_'+itineraryids[i]);
		while(s.length > 0){ s.remove(0); }
		for(ii=0; ii<itineraryids.length; ii++){
			var o = document.createElement('option');
			o.value = ii;
			o.text = eval(ii+1);
			try{ s.add(o,null); } catch(ex){ s.add(o); }
			}
		s.value = s.parentNode.parentNode.parentNode.rowIndex;
		}
	}

function findimgs(choose,func,fid){
	var win = window.open("3_imagewin.php?choose="+choose+"&func="+func+"&fid="+fid,"findimages","width=790,height=600,scrollbars=yes,resizable=yes");
	}

function chgmap(file,thumb,w,h,id,fid){
	var rand = Math.random();
	document.getElementById(fid+'img').style.display = '';
	document.getElementById(fid+'img').src = '../images/'+thumb+'?'+rand;
	document.getElementById(fid+'img').style.width = w;
	document.getElementById(fid+'img').style.height = h;
	document.getElementById(fid+'href').href = '../images/'+file;
	document.getElementById(fid).value = id;
	}

function rmvmap(fid){
	document.getElementById(fid+'img').style.display = 'none';
	document.getElementById(fid+'img').src = '';
	document.getElementById(fid+'href').href = '';
	document.getElementById(fid).value = '0';
	}

function addimg(file,thumb,w,h,id,fid){
	image_data['i'+id] = new Array();
		image_data['i'+id]['large'] = file;
		image_data['i'+id]['filename'] = thumb;
		image_data['i'+id]['w'] = w;
		image_data['i'+id]['h'] = h;
		image_data['i'+id]['caption'] = '';

	if(testIsValidObject(itineraryimgs['i'+fid])){
		itineraryimgs['i'+fid].push(id);
		} else {
		itineraryimgs['i'+fid] = new Array(id);
		}

	rebuildimgs(fid);
	CreateDragContainer();
	}

function rmvimg(fid,i){
	document.getElementById('delimgbtn').style.display = 'none';
	itineraryimgs['i'+fid].splice(i,1);
	rebuildimgs(fid);
	CreateDragContainer();
	}

function rebuildimgs(fid){
	x = document.getElementById('img_assoc'+fid);
	var code = '';
	for(i in itineraryimgs['i'+fid]){
		var id = itineraryimgs['i'+fid][i];
		code += ' <INPUT TYPE="hidden" NAME="imgs'+fid+'[]" VALUE="'+id+'">';
		//code += '<A HREF="../images/'+image_data['i'+id]['large']+'" TARGET="_blank" TITLE="'+image_data['i'+id]['caption']+'">';
		code += '<IMG SRC="../images/'+image_data['i'+id]['filename']+'" BORDER="0" WIDTH="'+image_data['i'+id]['w']+'" HEIGHT="'+image_data['i'+id]['h']+'" ID="'+fid+'_'+i+'" ALT="'+image_data['i'+id]['caption']+'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(this); showdelimg(this,\''+fid+'\',\''+i+'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
		//code += '</A>';
		}
	x.innerHTML = code;
	}

function showdelimg(obj,fid,i){
	var x = document.getElementById('delimgbtn');

	var newcode = '<INPUT TYPE="button" VALUE="Remove" STYLE="font-size:8pt;" onClick="rmvimg(\''+fid+'\',\''+i+'\')">';
	x.innerHTML = newcode;
	x.style.display = "";
	x.style.position = "absolute";

	var boxpos = findPos(obj);
	x.style.top = eval(boxpos[1]) + 'px';
	x.style.left = eval(boxpos[0] - 2) + 'px';
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

//BEING DRAG AND DROP FUNCTIONS
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = 'n';
var dragTarget = 'n';
var mouseOffset = null;
var dropTargets = new Array();

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function addDropTarget(dropTarget){
	dropTargets.push(dropTarget);
}

function CreateDragContainer(){
	dropTargets = new Array();
	for(a in itineraryids){
		fid = itineraryids[a];
		for(i=0; i<itineraryimgs['i'+fid].length; i++){
			addDropTarget( document.getElementById(fid+'_'+i) );
		}
	}
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets.length > 1){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(item){
	if(!item) return;
	item.onmousedown = function(ev){

		shadow = document.getElementById("forshadow");
		dragObject  = shadow;

		shadow.innerHTML = '<IMG SRC="'+item.src+'" WIDTH="'+eval(item.offsetWidth-8)+'" HEIGHT="'+eval(item.offsetHeight-8)+'" BORDER="0">'; // STYLE="border:4px solid #FFFFFF"

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin = 'n';
		for(var i=0; i<dropTargets.length; i++){
			if(item.id == dropTargets[i].id){
			dragOrigin = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin   = 'n';
	cleartarg();
}

function movem(){
	if(dragOrigin != 'n' && dragTarget != 'n'){

	o = dropTargets[dragOrigin].id.split('_');
	n = dropTargets[dragTarget].id.split('_');

	var op = document.getElementById(dropTargets[dragOrigin].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var np = document.getElementById(dropTargets[dragTarget].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var oa = o[1];

	if(np < op){
		n[1]++;
	} else if(np > op){
		//oa++;
	} else if(dragTarget < dragOrigin){
		oa++;
	} else if(dragTarget > dragOrigin){
		n[1]++;
	}

	itineraryimgs['i'+n[0]].splice(n[1],0,itineraryimgs['i'+o[0]][o[1]]);
	itineraryimgs['i'+o[0]].splice(oa,1);
	//alert(n[0]+' '+n[1]+' '+o[0]+' '+o[1]);

	rebuildimgs(o[0]);
	rebuildimgs(n[0]);
	CreateDragContainer();
	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(var i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';

		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				//newTarget = dropTargets[i].id;
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget){
		cleartarg();
		dragTarget = newTarget;
			var x = document.getElementById(dropTargets[newTarget].id);
			var o = document.getElementById(dropTargets[dragOrigin].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
			var n = x.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;

			if(n < o){
				x.style.borderRight = "4px solid #0000FF";
			} else if(n > o){
				x.style.borderLeft = "4px solid #0000FF";
			} else if(newTarget < dragOrigin){
				x.style.borderLeft = "4px solid #0000FF";
			} else if(newTarget > dragOrigin){
				x.style.borderRight = "4px solid #0000FF";
			}
		}
	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget != 'n' && testIsValidObject( dropTargets[dragTarget] )){
		document.getElementById( dropTargets[dragTarget].id ).style.borderLeft = "4px solid #FFFFFF";
		document.getElementById( dropTargets[dragTarget].id ).style.borderRight = "4px solid #FFFFFF";
		}
	dragTarget = 'n';
}

//--></SCRIPT><? echo "\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLPADDING="0" CELLSPACING="0" ID="ittable">'."\n";

	$step=0;
	bgcolor('');
	foreach($itinerary as $row){
	echo '<TR><TD ALIGN="left" VALIGN="top" STYLE="border-bottom:4px solid #FFFFFF;">'."\n";
		$row['step'] = $step;
		echo "\t".build_it_entry($row,count($itinerary));
		echo "\t".'</TD></TR>'."\n\n";
		$step++;
		}

echo '</TABLE>';

//echo '<INPUT TYPE="button" VALUE="Add another entry" onClick="addstep();"><BR><BR>'."\n\n";

echo '<DIV STYLE="width:93%; background:#CCCCCC; padding:3px;"><INPUT TYPE="submit" VALUE="Save Itinerary Entry" STYLE="width:180px;"></DIV><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A><BR><BR>'."\n\n";

echo '<DIV ID="delfields" STYLE="display:none;"></DIV>'."\n\n";

echo '</FORM>'."\n\n";

echo '<DIV ID="forshadow" STYLE="display:none; cursor:move;"></DIV>'."\n\n";
echo '<DIV ID="delimgbtn" STYLE="display:none;" onMouseOver="this.style.display=\'\';" onMouseOut="this.style.display=\'none\';"></DIV>'."\n\n";


} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked itinerary items, associated translations, and remove from tours?");
	return r;
}

//--></SCRIPT><?
//GET ITINERARIES
$itinerary = array();
$itids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS itinerary.`id`, itinerary.`nickname`, itinerary.`title`, itinerary.`comments`';
	$query .= ' FROM `itinerary`';
	$query .= ' WHERE `routeid` = 0';
	$query .= ' ORDER BY '.$_SESSION['itinerary']['sortby'].' '.$_SESSION['itinerary']['sortdir'].', itinerary.`nickname` ASC, itinerary.`id` ASC';
	$query .= ' LIMIT '.(($_SESSION['itinerary']['p']-1)*$_SESSION['itinerary']['limit']).','.$_SESSION['itinerary']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['complete'] = array();
	$itinerary['i'.$row['id']] = $row;
	array_push($itids,$row['id']);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['itinerary']['limit']);
if($numpages > 0 && $_SESSION['itinerary']['p'] > $numpages): $_SESSION['itinerary']['p'] = $numpages; endif;

$link = '';

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['itinerary']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['itinerary']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['itinerary']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['itinerary']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';

echo '<INPUT TYPE="button" VALUE="New Itinerary Entry" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'"><BR><BR>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['itinerary']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['itinerary']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['itinerary']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['itinerary']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['itinerary']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Nickname</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Content</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($itinerary as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt; width:30%;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">'.$row['nickname'].'</A></TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">';
		if($row['title'] != "" && $row['comments'] != ""){ $row['title'] .= ' - '; }
		if($row['comments'] != ""){ $row['title'] .= $row['comments']; }
		$row['title'] = strip_tags($row['title']);
		if(strlen($row['title']) > 160){
			$row['title'] = substr($row['title'],0,160);
			if(strstr($row['title']," ")){ $row['title'] = trim(substr($row['title'],0,strrpos($row['title']," "))); }
			if(substr($row['title'],-1) == '.' || substr($row['title'],-1) == ','){ $row['title'] = substr($row['title'],0,-1); }
			$row['title'] .= '...';
			}
		echo $row['title'];
		echo '</A></TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	$sel++;
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['itinerary']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['itinerary']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['itinerary']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['itinerary']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['itinerary']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Itinerary Entry" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Checked" STYLE="color:#FF0000;">';
	echo '<BR>'."\n\n";

echo '</FORM>'."\n\n";

} //END edit NEW IF STATEMENT


require("footer.php");

?>