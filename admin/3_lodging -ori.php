<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 6; echo 'REMOVE THIS LINE!!!!!';

$working = 0;

$pageid = "3_lodging";
require("validate.php");
require("header.php");

if(!isset($_SESSION['lodging']['p'])): $_SESSION['lodging']['p'] = 1; endif;
if(!isset($_SESSION['lodging']['sortby'])): $_SESSION['lodging']['sortby'] = "lodging.`name`"; endif;
	$sortby = array('lodging.`id`'=>'ID','lodging.`name`'=>'Name','lodging.`type`'=>'Type','vendor_name'=>'Vendor');
if(!isset($_SESSION['lodging']['sortdir'])): $_SESSION['lodging']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['lodging']['limit'])): $_SESSION['lodging']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['lodging']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['lodging']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['lodging']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['lodging']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['lodging']['p'] = '1';
	}


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `lodging`(`name`,`type`,`subunit`,`available`,`url`,`vendor`)';
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['type'].'","'.$_POST['subunit'].'","'.$_POST['available'].'","'.$_POST['url'].'","'.$_POST['vendor'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){ $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new lodging unit "'.stripslashes($_POST['name']).'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }

	} else {
	//UPDATE
	$query = 'UPDATE `lodging` SET `name` = "'.$_POST['name'].'", `type` = "'.$_POST['type'].'", `subunit` = "'.$_POST['subunit'].'", `available` = "'.$_POST['available'].'", `url` = "'.$_POST['url'].'", `vendor` = "'.$_POST['vendor'].'"';
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved lodging unit "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

	$successnew = array();
	$successupd = array();

	foreach($_POST['pricing_id'] as $key => $id){
		$startdate = mktime(0,0,0,$_POST['start_month'][$key],$_POST['start_day'][$key],$_POST['start_year'][$key]);
		$enddate = mktime(0,0,0,$_POST['end_month'][$key],$_POST['end_day'][$key],$_POST['end_year'][$key]);
		if($id == "*new*" && $_POST['start_month'][$key] != "*rm*" && $_POST['end_month'][$key] != "*rm*"){
			$query = 'INSERT INTO `lodging_pricing`(`lodgeid`,`price`,`startdate`,`enddate`,`min_nights`,`max_nights`,`calc`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['price'][$key].'","'.$startdate.'","'.$enddate.'","'.$_POST['min_nights'][$key].'","'.$_POST['max_nights'][$key].'","'.$_POST['calc'][$key].'")';
			@mysql_query($query);
			$thiserror = mysql_error();
			$newid = mysql_insert_id();
			if($thiserror == ""){ array_push($successnew,$newid); } else { array_push($errormsg,$thiserror); }
		} elseif($id != "*new*") {
			if($_POST['start_month'][$key] == "*rm*" || $_POST['end_month'][$key] == "*rm*"){
			$query = 'DELETE FROM `lodging_pricing` WHERE `id` = "'.$id.'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ array_push($successmsg,'Lodging pricing record ('.$id.') was deleted.'); } else { array_push($errormsg,$thiserror); }
			} else {
			$query = 'UPDATE `lodging_pricing` SET `price` = "'.$_POST['price'][$key].'", `startdate` = "'.$startdate.'", `enddate` = "'.$enddate.'", `min_nights` = "'.$_POST['min_nights'][$key].'", `max_nights` = "'.$_POST['max_nights'][$key].'", `calc` = "'.$_POST['calc'][$key].'" WHERE `id` = "'.$id.'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ array_push($successupd,$id); } else { array_push($errormsg,$thiserror); }
			}
		}
	}

	if(count($successnew) > 0): array_push($successmsg,'The following pricing records were added to lodging type "'.$_REQUEST['type'].'": '.implode(", ",$successnew)); endif;
	if(count($successupd) > 0): array_push($successmsg,'The following pricing records were updated: '.implode(", ",$successupd)); endif;

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

$query = 'DELETE FROM `lodging_pricing` WHERE `lodgeid` = "'.$_REQUEST['edit'].'"';
	@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Deleted all lodging pricing records for type ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

$query = 'DELETE FROM `lodging` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Deleted lodging type ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

unset($_REQUEST['edit']);

}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Lodging</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


$types = array(
	'y' => 'Yellowstone lodging',
	't' => 'Tour lodging'
	);

if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `lodging` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

//GET OTHER TYPES?
$forsubs = array();
$query = 'SELECT * FROM `lodging` WHERE `id` != "'.getval('id').'" AND `type` = "y" ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$forsubs['f'.$row['id']] = $row;
	}


bgcolor('');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:300px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Type</TD><TD><SELECT NAME="type" STYLE="width:200px;">';
		foreach($types as $key => $name){
		echo '<OPTION VALUE="'.$key.'"';
		if(getval('type') == $key): echo ' SELECTED'; endif;
		echo '>'.$name.'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Description</TD><TD><TEXTAREA NAME="description" STYLE="width:400px; height:100px;">'.getval('description').'</TEXTAREA></TD></TR>'."\n";
	/*echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Pricing<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This will adjust the price of a tour.<BR>(Negative values ok)</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addprice();"></TD><TD STYLE="font-family:Arial; font-size:12pt;">';
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" ID="pricetable">';
		if(isset($fillform['pricing']) && count($fillform['pricing']) > 0){
			foreach($fillform['pricing'] as $row){
				echo '<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-bottom:2px;">For <INPUT TYPE="text" STYLE="font-size:9pt; width:40px;" NAME="guests[]" VALUE="'.$row['guests'].'"> guests, price will be $<INPUT TYPE="text" STYLE="font-size:9pt; width:60px; text-align:right;" NAME="price[]" VALUE="'.$row['price'].'"></TD><TD STYLE="vertical-align:middle; padding-left:3px; padding-bottom:2px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
				} //End For Each
		} else {
			echo '<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; padding-bottom:2px;">For <INPUT TYPE="text" STYLE="font-size:9pt; width:40px;" NAME="guests[]" VALUE=""> guests, price will be $<INPUT TYPE="text" STYLE="font-size:9pt; width:60px; text-align:right;" NAME="price[]" VALUE=""></TD><TD STYLE="vertical-align:middle; padding-left:3px; padding-bottom:2px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>';
		} //End Not New If Statement
		echo '</TABLE>';
		//echo '$<INPUT TYPE="text" NAME="price" STYLE="width:100px;" VALUE="'.getval('price').'"> per night';
		echo '</TD></TR>'."\n";*/
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Website URL</TD><TD><INPUT TYPE="text" NAME="url" STYLE="width:300px;" VALUE="'.getval('url').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
		foreach($vendors as $vendor){
		echo '<OPTION VALUE="'.$vendor['id'].'"';
		if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
		echo '>'.$vendor['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Sub-Unit?<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>Is this unit part of a larger unit?</SPAN></TD><TD><SELECT NAME="subunit" STYLE="width:200px;">';
		echo '<OPTION VALUE="0">No</OPTION>';
		foreach($forsubs as $unit){
		echo '<OPTION VALUE="'.$unit['id'].'"';
		if(getval('subunit') == $unit['id']): echo ' SELECTED'; endif;
		echo '>'.$unit['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Units available<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>How many of these units are<BR>available on each date listed below?</SPAN></TD><TD><INPUT TYPE="text" NAME="available" STYLE="width:60px;" VALUE="'.getval('available').'"></TD></TR>'."\n";
	echo '</TABLE><BR>'."\n\n";


//GET LODGING PRICING
$pricing = array();
$query = 'SELECT * FROM `lodging_pricing` WHERE `lodgeid` = "'.$_REQUEST['edit'].'" ORDER BY `startdate` ASC, `min_nights` ASC, `max_nights` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($pricing,$row);
	}

?><SCRIPT><!--

function addrow(ins){
	var time = new Date();
	var forid = time.getTime();
	if(ins % 2 == 1){ var bg = '#DDDDDD'; } else { var bg = '#FFFFFF'; }

	var x = document.getElementById('pricetable').insertRow(ins);
	x.style.backgroundColor = bg;

	var y = x.insertCell(0);
	y.style.paddingLeft = "6px";
	y.style.textAlign = "center";
	y.innerHTML = '<INPUT TYPE="hidden" NAME="pricing_id[]" VALUE="*new*"><SELECT NAME="start_month[]" ID="start_month'+forid+'" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION><? for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_day[]" ID="start_day\'+forid+\'" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_year[]" ID="start_year\'+forid+\'" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("Y",$time) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   } ?></SELECT>';

	var y = x.insertCell(1);
	y.style.textAlign = "center";
	y.innerHTML = '<SELECT NAME="end_month[]" ID="end_month'+forid+'" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION><? for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_day[]" ID="end_day\'+forid+\'" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_year[]" ID="end_year\'+forid+\'" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("Y",$time) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   } ?></SELECT>';

	var y = x.insertCell(2);
	y.style.textAlign = "center";
	y.style.fontFamily = "Arial";
	y.style.fontSize = "10pt";
	y.innerHTML = '<SPAN STYLE="white-space:nowrap;"><INPUT TYPE="text" NAME="min_nights[]" ID="min_nights'+forid+'" VALUE="1" STYLE="font-size:9pt; width:30px; text-align:center;"> to <INPUT TYPE="text" NAME="max_nights[]" ID="max_nights'+forid+'" VALUE="1" STYLE="font-size:9pt; width:30px; text-align:center;"></SPAN>';

	var y = x.insertCell(3);
	y.style.textAlign = "center";
	y.innerHTML = '<SELECT NAME="calc[]" ID="calc'+forid+'" CLASS="smallsel"><OPTION VALUE="*">X</OPTION><OPTION VALUE="=">=</OPTION></SELECT>';

	var y = x.insertCell(4);
	y.style.fontFamily = "Arial";
	y.style.fontSize = "10pt";
	y.style.textAlign = "center";
	y.innerHTML = '$<INPUT TYPE="text" NAME="price[]" ID="price'+forid+'" VALUE="" STYLE="font-size:9pt; width:50px; text-align:right;">';

	var y = x.insertCell(5);
	y.style.paddingRight = "6px";
	y.style.textAlign = "center";
	y.innerHTML = '<INPUT TYPE="button" VALUE="Del" TITLE="Delete this row" STYLE="font-size:8pt;" onClick="document.getElementById(\'start_month'+forid+'\').value=\'*rm*\'; document.getElementById(\'end_month'+forid+'\').value=\'*rm*\';">';
}

// --></SCRIPT><?

if($_REQUEST['edit'] != "*new*"){
	echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Order URL: <A HREF="https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'" TARGET="_blank">https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'</A></SPAN><BR>'."\n\n";
	}

echo '<BR>'."\n\n";

echo '<TABLE ID="pricetable" BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="93%">'."\n\n";

echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">Start Date</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">End Date</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; color:#FFFFFF;"># of Nights</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; color:#FFFFFF;">x / =</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">Price</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-right:6px;">Del</TD>';
	echo '</TR>'."\n";

	if(count($pricing) > 0){
	foreach($pricing as $row){
	echo '<TR BGCOLOR="#'.bgcolor('').'">';
		echo '<TD ALIGN="center" STYLE="padding-left:6px;">';
			echo '<INPUT TYPE="hidden" NAME="pricing_id[]" VALUE="'.$row['id'].'">';
			echo '<SELECT NAME="start_month[]" ID="start_month'.$row['id'].'" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION>';   for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("n",$row['startdate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_day[]" ID="start_day'.$row['id'].'" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("j",$row['startdate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_year[]" ID="start_year'.$row['id'].'" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("Y",$row['startdate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   }   echo '</SELECT>';
			echo '</TD>';
		echo '<TD ALIGN="center">';
			echo '<SELECT NAME="end_month[]" ID="end_month'.$row['id'].'" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION>';   for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("n",$row['enddate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_day[]" ID="end_day'.$row['id'].'" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("j",$row['enddate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_year[]" ID="end_year'.$row['id'].'" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'"'; if( date("Y",$row['enddate']) == $ii ): echo " SELECTED"; endif; echo '>'.$ii.'</OPTION>';   }   echo '</SELECT>';
			echo '</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; white-space:nowrap;"><INPUT TYPE="text" NAME="min_nights[]" ID="min_nights'.$row['id'].'" VALUE="'.$row['min_nights'].'" STYLE="font-size:9pt; width:30px; text-align:center;"> to <INPUT TYPE="text" NAME="max_nights[]" ID="max_nights'.$row['id'].'" VALUE="'.$row['max_nights'].'" STYLE="font-size:9pt; width:30px; text-align:center;"></TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;"><SELECT NAME="calc[]" ID="calc'.$row['id'].'" CLASS="smallsel">';
			echo '<OPTION VALUE="*"'; if($row['calc'] == "*"): echo ' SELECTED'; endif; echo '>X</OPTION>';
			echo '<OPTION VALUE="="'; if($row['calc'] == "="): echo ' SELECTED'; endif; echo '>=</OPTION>';
			echo '</SELECT></TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;">$<INPUT TYPE="text" NAME="price[]" ID="price'.$row['id'].'" VALUE="'.$row['price'].'" STYLE="font-size:9pt; width:50px; text-align:right;"></TD>';
		echo '<TD ALIGN="center" STYLE="padding-right:6px;"><INPUT TYPE="button" VALUE="Del" TITLE="Delete this row" STYLE="font-size:8pt;" onClick="document.getElementById(\'start_month'.$row['id'].'\').value=\'*rm*\'; document.getElementById(\'end_month'.$row['id'].'\').value=\'*rm*\';"></TD>';
		echo '</TR>'."\n";
		}
	} else {
	echo '<TR BGCOLOR="#'.bgcolor('').'">';
		echo '<TD ALIGN="center" STYLE="padding-left:6px;">';
			echo '<INPUT TYPE="hidden" NAME="pricing_id[]" VALUE="*new*">';
			echo '<SELECT NAME="start_month[]" ID="start_month00" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION>';   for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_day[]" ID="start_day00" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="start_year[]" ID="start_year00" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT>';
			echo '</TD>';
		echo '<TD ALIGN="center">';
			echo '<SELECT NAME="end_month[]" ID="end_month00" CLASS="smallsel"><OPTION VALUE="*rm*" STYLE="color:#FF0000;">*Remove</OPTION>';   for($ii=1; $ii<13; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.' ('.date("M",mktime("0","0","0",$ii,"1","2005")).')</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_day[]" ID="end_day00" CLASS="smallsel">';   for($ii=1; $ii<32; $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT> / <SELECT NAME="end_year[]" ID="end_year00" CLASS="smallsel">';   for($ii=2009; $ii<(date("Y",$time)+11); $ii++){   echo '<OPTION VALUE="'.$ii.'">'.$ii.'</OPTION>';   }   echo '</SELECT>';
			echo '</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; white-space:nowrap;"><INPUT TYPE="text" NAME="min_nights[]" ID="min_nights00" VALUE="1" STYLE="font-size:9pt; width:30px; text-align:center;"> to <INPUT TYPE="text" NAME="max_nights[]" ID="max_nights00" VALUE="1" STYLE="font-size:9pt; width:30px; text-align:center;"></TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;"><SELECT NAME="calc[]" ID="calc00" CLASS="smallsel">';
			echo '<OPTION VALUE="*">X</OPTION>';
			echo '<OPTION VALUE="=">=</OPTION>';
			echo '</SELECT></TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt;">$<INPUT TYPE="text" NAME="price[]" ID="price00" VALUE="" STYLE="font-size:9pt; width:50px; text-align:right;"></TD>';
		echo '<TD ALIGN="center" STYLE="padding-right:6px;"><INPUT TYPE="button" VALUE="Del" TITLE="Delete this row" STYLE="font-size:8pt;" onClick="document.getElementById(\'start_month00\').value=\'*rm*\'; document.getElementById(\'end_month00\').value=\'*rm*\';"></TD>';
		echo '</TR>'."\n";
	}

echo '<TR><TD COLSPAN="6" ALIGN="center" STYLE="padding-bottom:18px; border-top:1px solid #666666;"><INPUT TYPE="button" VALUE="Add Another Row" STYLE="font-size:9pt;" onClick="addrow(this.parentNode.parentNode.rowIndex)"></TD></TR>'."\n\n";

echo '</TABLE>'."\n\n";


echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


if(getval('id') != "*new*"){

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" WIDTH="93%"><BR>'."\n\n";

	echo '<FORM NAME="deleteform" ID="deleteform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return confirm(\'Delete lodging type '.getval('id').'?\');">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="delete">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Delete this lodging type" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '</FORM>'."\n\n";

	} //End *new* if statement



} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked?");
	return r;
}

//--></SCRIPT><?


//GET LODGING
$lodging = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS lodging.*, vendors.`name` AS `vendor_name` FROM `lodging` LEFT JOIN `vendors` ON lodging.`vendor` = vendors.`id`';
	$query .= ' ORDER BY '.$_SESSION['lodging']['sortby'].' '.$_SESSION['lodging']['sortdir'].', lodging.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['lodging']['p']-1)*$_SESSION['lodging']['limit']).','.$_SESSION['lodging']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($lodging,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['lodging']['limit']);
if($numpages > 0 && $_SESSION['lodging']['p'] > $numpages): $_SESSION['lodging']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['lodging']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['lodging']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['lodging']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['lodging']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['lodging']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['lodging']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['lodging']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['lodging']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['lodging']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Type</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Vendor</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($lodging as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$types[$row['type']].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['vendor_name'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['lodging']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['lodging']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['lodging']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['lodging']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['lodging']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New lodging unit" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;">';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>