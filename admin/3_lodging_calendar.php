<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_lodging_calendar";
require("validate.php");
require("header.php");

//!**ISSUE WITH UNIT FILTER


//!SETUP CALENDAR
if(!isset($_SESSION['lodging_calendar'])){
	//Find soonest upcoming month
	/*$query = 'SELECT `date` FROM `reservations_assoc` WHERE `date` >= "'.$time.'" AND `type` = "l" AND `canceled` = "0" ORDER BY `date` ASC LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){
		$soonest = mysql_fetch_assoc($result);
		$soonest = $soonest['date'];
		$_SESSION['lodging_calendar']['y'] = date("Y",$soonest);
		$_SESSION['lodging_calendar']['m'] = date("n",$soonest);
		} else {*/
		$_SESSION['lodging_calendar']['y'] = date("Y",$time);
		$_SESSION['lodging_calendar']['m'] = date("n",$time);
		}
	//} // End isset($_SESSION['lodging_calendar'])

if(!isset($_SESSION['lodging_calendar']['unit'])): $_SESSION['lodging_calendar']['unit'] = "*all*"; endif;
if(!isset($_SESSION['lodging_calendar']['view'])): $_SESSION['lodging_calendar']['view'] = "cal"; endif;

if(isset($_REQUEST['y']) && $_REQUEST['y'] != ""): $_SESSION['lodging_calendar']['y'] = $_REQUEST['y']; endif;
if(isset($_REQUEST['m']) && $_REQUEST['m'] != ""): $_SESSION['lodging_calendar']['m'] = $_REQUEST['m']; endif;
if(isset($_REQUEST['unit']) && $_REQUEST['unit'] != ""): $_SESSION['lodging_calendar']['unit'] = $_REQUEST['unit']; endif;
if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""): $_SESSION['lodging_calendar']['view'] = $_REQUEST['view']; endif;

$calfirst = mktime(0,0,0,$_SESSION['lodging_calendar']['m'],1,$_SESSION['lodging_calendar']['y']);
$calendar = array(
	"first" => $calfirst,
	"start" => date("w",$calfirst),
	"days" => date("t",$calfirst),
	"m" => date("n",$calfirst),
	"y" => date("Y",$calfirst),
	"prevm" => mktime(0,0,0,(date("n",$calfirst)-1),1,date("Y",$calfirst)),
	"nextm" => mktime(0,0,0,(date("n",$calfirst)+1),1,date("Y",$calfirst))
	);

$calw = (7*100);

$colors = array('#ffcc00','#ff6600','#33ccff','#ffcc99','#cccc99','#99cccc','#33cc99','#669933','#cc99ff','#99ffff','#6699ff','#666699','#cc66ff','#ff99ff','#cc3366','#999999');
	//foreach($colors as $c){
	//	echo '<DIV STYLE="padding:2px; background-color:'.$c.'; border-top:1px solid #C7C7C7;">'.$c.'</DIV>';
	//	}

//GET UNITS
$units = array();
$substructure = array();
$query = 'SELECT lodging.* FROM `lodging` WHERE `type` = "y"';
	//if($_SESSION['lodging_calendar']['unit'] != "" && $_SESSION['lodging_calendar']['unit'] != "*all*"){ $query .= ' AND `lodgeid` = "'.$_SESSION['lodging_calendar']['unit'].'"'; }
	$query .= ' ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if($row['subunit'] > 0){
			if(!isset($substructure['u'.$row['subunit']])){ $substructure['u'.$row['subunit']] = array(); }
			array_push($substructure['u'.$row['subunit']],'u'.$row['id']);
			}
		$row['color'] = array_shift($colors);
		$units['u'.$row['id']] = $row;
		}
	//echo '<PRE STYLE="text-align:left;">'; print_r($units); echo '</PRE>';
	//echo '<PRE STYLE="text-align:left;">'; print_r($substructure); echo '</PRE>';

//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['lodging_calendar']); echo '</PRE>';
//echo '<PRE>'; print_r($calendar); echo '</PRE>';


$successmsg = array();
$errormsg = array();


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Lodging Calendar</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


/*
echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:'.$calw.'px; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Show lodging unit:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="unit" STYLE="font-size:9pt; width:220px;">';
		echo '<OPTION VALUE="*all*">All units</OPTION>';
			foreach($units as $row){
			echo '<OPTION VALUE="'.$row['id'].'"';
			if($_SESSION['lodging_calendar']['unit'] == $row['id']): echo ' SELECTED'; endif;
			echo '>'.$row['name'].'</OPTION>';
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">View as a:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="view" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="cal"'; if($_SESSION['lodging_calendar']['view'] == "cal"): echo " SELECTED"; endif; echo '>Calendar</OPTION>';
			echo '<OPTION VALUE="list"'; if($_SESSION['lodging_calendar']['view'] == "list"): echo " SELECTED"; endif; echo '>List</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Show" STYLE="font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';
*/


echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";

//!GET RESERVATIONS
	$reservations = array();
	$query = 'SELECT reservations_assoc.`reservation`, reservations_assoc.`date`, FROM_UNIXTIME(reservations_assoc.`date`+3600) AS `date2`, (reservations_assoc.`adults` + reservations_assoc.`seniors` + reservations_assoc.`children`) AS `numguests`, IF(reservations_assoc.`name`!="",reservations_assoc.`name`,reservations.`name`) AS `name`, reservations_assoc.`lodgeid`, lodging.`subunit`, reservations_assoc.`nights`';
		$query .= ' FROM `reservations`, `reservations_assoc`, `lodging`';
		$query .= ' WHERE reservations.`id` = reservations_assoc.`reservation`';
		$query .= ' AND reservations_assoc.`type` = "l" AND reservations_assoc.`lodgeid` > 0';
		$query .= ' AND reservations_assoc.`lodgeid` = lodging.`id` AND lodging.`type` = "y"';
		$query .= ' AND reservations.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
		$query .= ' AND ((reservations_assoc.`date`+(reservations_assoc.`nights`*86400)+3600) >= '.$calendar['first'].' AND reservations_assoc.`date` < '.strtotime("+1 month",$calendar['first']).')';
		//if($_SESSION['lodging_calendar']['unit'] != "" && $_SESSION['lodging_calendar']['unit'] != "*all*"){ $query .= ' AND (reservations_assoc.`lodgeid` = "'.$_SESSION['lodging_calendar']['unit'].'" OR lodging.`subunit` = "'.$_SESSION['lodging_calendar']['unit'].'")'; }
		$query .= ' ORDER BY `date` ASC, reservations_assoc.`reservation` ASC';
		//echo $query.'<BR>';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			//$day = date("j",$row['date']);
			//if(!isset($reservations[$day])){ $reservations[$day] = array(); }
			//if(!isset($reservations[$day]['u'.$row['lodgeid']])){ $reservations[$day]['u'.$row['lodgeid']] = array(); }
			//array_push($reservations[$day]['u'.$row['lodgeid']]['reservations'],$row['reservation']);
			array_push($reservations,$row);
			}
	//echo '<PRE STYLE="text-align:left;">'; print_r($reservations); echo '</PRE>';

//!FILL OCCUPANCY ARRAY
	$occupancy = array();

	function addoc($d,$u,$a,$num,$res){
		global $occupancy;
		if(!isset($occupancy[$d])){ $occupancy[$d] = array(); }
		if(!isset($occupancy[$d][$u])){ $occupancy[$d][$u] = array('reservations'=>array(),'booked'=>0,'available'=>$a); }
		$occupancy[$d][$u]['booked'] += $num;
		array_push($occupancy[$d][$u]['reservations'],$res);
		}

	foreach($reservations as $row){
		$num = 1;
		$u = 'u'.$row['lodgeid'];
		$a = $units[$u]['available'];

		$resline = '<A HREF="3_reservations.php?view='.$row['reservation'].'">'.$row['reservation'].' - '.$row['name'].'</A>, '.$row['nights'].' nights'; //, '.$row['numguests'].' guests

		for($i=0; $i<$row['nights']; $i++){
			$d = strtotime("+".$i." day",$row['date']);
			//echo $u.' '.$d.' '.date("n/j/Y",$d).'<BR>';
			if(date("n",$d) == $calendar['m']){
				//Make sure date is in the current month
				addoc(date("j",$d),$u,$a,$num,$resline);
				//Fill substructure if needed
				if(isset($substructure[$u])){  foreach($substructure[$u] as $id){
					addoc(date("j",$d),$id,$units[$id]['available'],$units[$id]['available'],$resline);
					}}
				}
			}
		}
	//echo '<PRE STYLE="text-align:left;">'; print_r($occupancy); echo '</PRE>';

$filter = array_keys($substructure);

//!Print unit color key
echo '<DIV STYLE="width:'.$calw.'px; margin-bottom:6px; font-family:Helvetica; font-size:10pt;">';
	echo '<DIV STYLE="margin-bottom:6px; text-align:center; font-size:11pt; clear:both;">Lodging Unit Color Key</DIV>';
	foreach($units as $key => $row){ if(!in_array($key,$filter)){
		echo '<DIV STYLE="float:left; margin-right:10px; margin-bottom:3px;">';
			echo '<DIV STYLE="background-color:'.$row['color'].'; height:15px; width:15px; float:left; margin-right:2px; margin-bottom:3px;"></DIV>';
			echo $row['name'].'</DIV>';
		}}
	echo '<DIV STYLE="float:left; margin-right:10px; margin-bottom:3px;"><img src="img/dot_red.png" alt="Overbooked!!!" width="14" height="14" style="float:left; margin-right:1px;">Overbooked!</DIV>';
	echo '<BR STYLE="clear:both;">';
	echo '</DIV>';

echo '<DIV STYLE="width:'.$calw.'px; margin-top:-4px; margin-bottom:10px; font-family:Helvetica; font-size:10pt; font-style:italic; color:#333333;">Click on a unit to see more details.</DIV>';

?><DIV ID="moreinfo" STYLE="display:none; z-index:1000; position:absolute; width:230px; background-color:white; border:1px solid #999999; padding:8px; text-align:left; font-family:Helvetica; font-size:10pt;"></DIV>

<SCRIPT><!--

function showres(obj,id){
	var moreinfo = document.getElementById("moreinfo");
	var newhtml = document.getElementById(id).innerHTML;

	if(newhtml != ""){
		//moreinfo.onmouseover = function(){ showres(obj,id); }
		//moreinfo.onmouseout = function(){ hideres(); }

		moreinfo.innerHTML = newhtml;
		moreinfo.style.display = "";
	
		var pos = findPos(obj);
		pos[0] += (parseInt(obj.offsetWidth)/2)-(parseInt(moreinfo.offsetWidth)/2);
		pos[1] += parseInt(obj.offsetHeight);
	
		moreinfo.style.left = pos[0] + 'px';
		moreinfo.style.top = pos[1] + 'px';
		}
	}

function hideres(){
	document.getElementById("moreinfo").style.display = "none";
	}

function findPos(obj){
	var curleft = curtop = 0;
	if(obj.offsetParent){
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while(obj = obj.offsetParent){
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
			}
		}
	return [curleft,curtop];
	}

// -->
</SCRIPT><?



echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="calendar" WIDTH="'.$calw.'" STYLE="border:1px solid #7F9DB9;">
<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px; background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">
	<TD ALIGN="left" STYLE="padding-right:8px;"><INPUT TYPE="button" STYLE="font-size:12pt;" VALUE="&lt;&lt;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?y='.date("Y",$calendar['prevm']).'&m='.date("n",$calendar['prevm']).'\'"></TD>
	<TD ALIGN="center"><SELECT STYLE="font-size:12pt;" ID="choose_month" onChange="window.location=\''.$_SERVER['PHP_SELF'].'?y=\'+document.getElementById(\'choose_year\').value+\'&m=\'+document.getElementById(\'choose_month\').value;">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['m'] == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> <SELECT STYLE="font-size:12pt;" ID="choose_year" onChange="window.location=\''.$_SERVER['PHP_SELF'].'?y=\'+document.getElementById(\'choose_year\').value+\'&m=\'+document.getElementById(\'choose_month\').value;">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['y'] == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></TD>
	<TD ALIGN="right" STYLE="padding-left:8px;"><INPUT TYPE="button" STYLE="font-size:12pt;" VALUE="&gt;&gt;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?y='.date("Y",$calendar['nextm']).'&m='.date("n",$calendar['nextm']).'\'"></TD>
	</TABLE>
	</TD></TR>'."\n\n";


//!VIEW AS LIST ******************************************************
	//Left over from route calendar.  Not going to utilize just yet.
/*if($_SESSION['lodging_calendar']['view'] == "list"){
	echo '</TABLE><BR>'."\n\n";


echo '<TABLE BORDER="0" WIDTH="'.$calw.'" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($reservations as $day => $date){

	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD ALIGN="left" COLSPAN="2" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">'.date("l, F j, Y",$date[0]['date']).'</TD>';
		echo '<TD ALIGN="right" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:10px;">Reserved</TD>';
		echo '<TD ALIGN="right" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:10px;">Capacity</TD>';
		echo '</TR>'."\n";
	bgcolor('reset');
	foreach($date as $row){
		if(isset($routes_booked['r'.$row['routeid']]['d'.$row['date']])){ $row['booked'] = ($routes_booked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$routes_booked['r'.$row['routeid']]['d'.$row['date']]['route_seats']); }
		echo '<TR BGCOLOR="#'.bgcolor('').'">';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt; padding-left:10px;">'.date("g:ia",$row['time']).'</TD>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt;"><A HREF="3_routes.php?edit='.$row['routeid'].'">'.$row['name'].'</A></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; padding-right:10px;';
			if($row['booked'] > 0){
				echo ' font-weight:bold;';
				} else {
				echo ' color:#C7C7C7;';
				}
			echo '">'.$row['booked'].'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; padding-right:10px;">'.$row['seats'].'</TD>';
		echo '</TR>'."\n";
		}
	} //End For Each
	echo '</TABLE><BR>'."\n\n";


//!VIEW AS CALENDAR ******************************************************
} else {*/

echo '<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Sun</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Mon</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Tue</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Wed</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Thu</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Fri</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Sat</TD>
	</TR>'."\n\n";

echo '<TR BGCOLOR="#'.bgcolor('DDDDDD').'">';

	$day = 0;
	$col = $calendar['start'];

	if($calendar['start'] > 0): echo '<TD COLSPAN="'.$calendar['start'].'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#FFFFFF; border-top:1px solid #999999; border-right:1px solid #999999;">&nbsp;</TD>'; endif;

	$i=0;
	while($day<$calendar['days']){
	$col++;
	++$day;
	echo '	<TD ALIGN="left" VALIGN="top" ID="d'.$day.'" STYLE="padding:0px; width:'.ceil($calw/7).'px; height:'.ceil($calw/7).'px; font-family:Arial; font-size:8pt; border-top:1px solid #999999;';
		if($col < 7){ echo ' border-right:1px solid #999999;'; }
		echo '">';
		echo '<DIV STYLE="padding:4px; font-size:11pt; font-weight:bold;">'.$day.'</DIV>';
		foreach($units as $key => $type){ if(!in_array($key,$filter)){
			$i++;
			$a = $type['available'];
				//if($type['subavailable'] > 0){ $a = ($type['available']*$type['subavailable']); }

			if(isset($occupancy[$day]) && isset($occupancy[$day][$key])){
				$u = $occupancy[$day][$key];
				} else {
				$u = array('reservations'=>array(),'booked'=>0,'available'=>$a);
				}

			$per = ($u['booked']/$a)*100;
			$per = number_format($per,0,'.','');

			if(count($u['reservations']) > 0){
				$moreinfo = '<DIV STYLE="margin-bottom:4px; background-color:#EEEEEE; font-size:11pt; text-align:center; font-weight:bold;">'.$type['name'].'</DIV>';
					$moreinfo .= '<DIV STYLE="text-align:center; margin-bottom:4px;">';
					if($u['booked'] > $a){
						$moreinfo .= $per.'% Overbooked!!!';
						} else {
						$moreinfo .= $per.'% Booked';
						}
						$moreinfo .= ' - '.$u['booked'].'/'.$a.' rooms</DIV>';
					$moreinfo .= '<P STYLE="margin:0px; padding-left:20px; text-indent:-20px;">'.implode('</P><P STYLE="margin:0px; padding-left:20px; text-indent:-20px;">',$u['reservations']).'</P>';
					$moreinfo .= '<DIV STYLE="margin-top:4px; background-color:#EEEEEE; text-align:center; font-size:9pt; font-style:italic; cursor:pointer;" onClick="hideres();">- Click here to hide -</DIV>';
					$moreinfo = str_replace("'",'\\\'',$moreinfo);
				} else {
				$moreinfo = '';
				}

			if($per > 100){ $per = 100; } elseif($per < 2){ $per = 2; } else { $per = number_format($per,0,'.',''); }

			echo '<DIV STYLE="position:relative; height:18px; border-top:1px solid #666666;';
				if(count($u['reservations']) > 0){ echo ' cursor:pointer;'; }
				echo '" onClick="showres(this,\'info'.$i.'\');"">'; // onMouseOut="hideres();
				echo '<DIV STYLE="background-color:'.$units[$key]['color'].'; position:absolute; width:'.$per.'%; height:18px; left:0px; top:0px;"></DIV>';
				echo '<DIV STYLE="position:absolute; padding:2px 0px 0px 3px; width:'.(($calw/7)-6).'px; height:14px; white-space:nowrap; overflow:hidden;">';
					if($u['booked'] > $a){ echo '<img src="img/dot_red.png" alt="Overbooked!!!" width="12" height="12" style="float:left; margin-right:1px;">'; }
					echo $u['booked'].'/'.$u['available'].' : '.$type['name'].'</DIV>';
				echo '</DIV>'."\n";
			echo '<DIV STYLE="display:none;" ID="info'.$i.'">'.$moreinfo.'</DIV>';

			}}
		echo '</TD>'."\n";
	if($col == 7){
		echo '</TR>'."\n".'<TR BGCOLOR="#'.bgcolor('').'">';
		$col = 0;
		}
	}

	if($col > 0 && $col < 7): echo '<TD COLSPAN="'.(7 - $col).'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#FFFFFF; border-top:1px solid #999999;">&nbsp;</TD>'."\n"; endif;

	echo '</TR></TABLE>'."\n\n";

//}

echo '</FORM>'."\n\n";


require("footer.php");

?>