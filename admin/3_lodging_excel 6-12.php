<?php 
$pageid = "3_lodging_new";
require("validate.php");
require("header2.php");


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();

echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Excel</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


$month_arr = [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];

$selected_day = isset($_REQUEST['day_selected']) ? $_REQUEST['day_selected'] : date('j');
$selected_month = isset($_REQUEST['month_selected']) ? $_REQUEST['month_selected'] : date('n');
$selected_year = isset($_REQUEST['year_selected']) ? $_REQUEST['year_selected'] : date('Y');

$selected_day_end = isset($_REQUEST['day_selected_end']) ? $_REQUEST['day_selected_end'] : date('j', strtotime("+21 day"));
$selected_month_end = isset($_REQUEST['month_selected_end']) ? $_REQUEST['month_selected_end'] : date('n');
$selected_year_end = isset($_REQUEST['year_selected_end']) ? $_REQUEST['year_selected_end'] : date('Y');

$search_start_date = mktime(0, 0, 0, $selected_month, $selected_day, $selected_year);//date('Y')
//echo date("m-d-Y", $max_end_date) . '==';
$search_end_date = mktime(0, 0, 0, $selected_month_end, $selected_day_end, $selected_year_end);//date('Y')

echo '<form action="">';
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";

echo '<TR STYLE="background:#'.bgcolor('').'">'
        . '<TD width="25%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'

        . ''
        . '<SELECT NAME="day_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($d = 1; $d < 32; $d++){
            echo '<OPTION VALUE="'.$d.'"';
            if($selected_day == $d): echo ' SELECTED'; endif;
            echo '>'.$d.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'
        . '<SELECT NAME="month_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            foreach($month_arr as $k => $each_month){
            echo '<OPTION VALUE="'.$k.'"';
            if($selected_month == $k): echo ' SELECTED'; endif;
            echo '>'.$each_month.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'

        . '<SELECT NAME="year_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($y = 2017; $y <= 2030; $y++){
            echo '<OPTION VALUE="'.$y.'"';
            if($selected_year == $y): echo ' SELECTED'; endif;
            echo '>'.$y.'</OPTION>';
            }
        echo '</select></td>'
            . '<td  WIDTH="20" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
            . '&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;&nbsp;' //. '<input type="submit" value="go" />'
            . '</TD>';
    //. '</TR>'."\n";


echo ''//'<TR STYLE="background:#'.bgcolor('').'">'
        . '<TD width="25%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'

        . ''
        . '<SELECT NAME="day_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($d = 1; $d < 32; $d++){
            echo '<OPTION VALUE="'.$d.'"';
            if($selected_day_end == $d): echo ' SELECTED'; endif;
            echo '>'.$d.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'
        . '<SELECT NAME="month_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            foreach($month_arr as $k => $each_month){
            echo '<OPTION VALUE="'.$k.'"';
            if($selected_month_end == $k): echo ' SELECTED'; endif;
            echo '>'.$each_month.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'

        . '<SELECT NAME="year_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($y = 2017; $y <= 2030; $y++){
            echo '<OPTION VALUE="'.$y.'"';
            if($selected_year_end == $y): echo ' SELECTED'; endif;
            echo '>'.$y.'</OPTION>';
            }
        echo '</select></td>'
            . '<td STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
            . '<input type="submit" value="go" />'
            . '</TD>'
    . '</TR>'."\n";         

echo '</TABLE></form><BR>'."\n\n";


echo '<BR>'."\n\n";

//GET RESERVATIONS
$reservations = array();
$query = 'SELECT r.id, ra.date, ra.date_v2, ra.lodgeid, ra.nights, ra.notes FROM `reservations` as r LEFT JOIN `reservations_assoc` as ra ON r.`id` = ra.`reservation`'
        . ' WHERE ra.type = "l" AND ra.date >= ' . $search_start_date . ' AND ra.date <= ' . $search_end_date;
$result = mysql_query($query);
echo $query;
//echo mysql_error().'<BR>';

$num_results = mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
    $row = mysql_fetch_assoc($result);
    array_push($reservations,$row);
}

echo '<pre>';
print_r($reservations);
echo '</pre>';
  

require("footer.php");

