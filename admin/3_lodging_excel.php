<?php
error_reporting(9);
$pageid = "3_lodging_new";
if(!isset($path_to_common)) {
	$path_to_common = '../';
}
require_once $path_to_common.'common.inc.php';




//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();

$month_arr = [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];

$selected_day = isset($_REQUEST['day_selected']) ? $_REQUEST['day_selected'] : 1;
$selected_month = isset($_REQUEST['month_selected']) ? $_REQUEST['month_selected'] : date('n');
$selected_year = isset($_REQUEST['year_selected']) ? $_REQUEST['year_selected'] : date('Y');

/*$selected_day_end = isset($_REQUEST['day_selected_end']) ? $_REQUEST['day_selected_end'] : 31;
$selected_month_end = isset($_REQUEST['month_selected_end']) ? $_REQUEST['month_selected_end'] : date('n');
$selected_year_end = isset($_REQUEST['year_selected_end']) ? $_REQUEST['year_selected_end'] : date('Y');*/
$selected_day_end = isset($_REQUEST['day_selected_end']) ? $_REQUEST['day_selected_end'] : 31;
$selected_month_end = isset($_REQUEST['month_selected']) ? $_REQUEST['month_selected'] : date('n');
$selected_year_end = isset($_REQUEST['year_selected']) ? $_REQUEST['year_selected'] : date('Y');

$search_start_date = mktime(0, 0, 0, $selected_month, $selected_day, $selected_year);//date('Y')
//echo date("m-d-Y", $max_end_date) . '==';
$search_end_date = mktime(0, 0, 0, $selected_month_end, $selected_day_end, $selected_year_end);//date('Y')



if (isset($_REQUEST['go'])) {

    
    $dateOffset = 1;
    
    /** Set default timezone (will throw a notice otherwise) */
    date_default_timezone_set('Asia/Dhaka');

    // include PHPExcel
    require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

    // create new PHPExcel object
    $objPHPExcel = new PHPExcel;

    // set default font
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

    // set default font size
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

    // create the writer
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");



    /**

     * Define currency and number format.

     */

    // currency format, € with < 0 being in red color
    $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

    // number format, with thousands separator and two decimal points.
    $numberFormat = '#,#0.##;[Red]-#,#0.##';


    // writer already created the first sheet for us, let's get it
    $objSheet = $objPHPExcel->getActiveSheet();

    // rename the sheet
    $objSheet->setTitle(date('F'));

    $objSheet->setShowGridlines(false);


    // let's bold and size the header font and write the header
    // as you can see, we can specify a range of cells, like here: cells from A1 to A4
    $objSheet->getStyle('A1:M1')->getFont()->setBold(true)->setSize(10);

    /*$styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                  'color' => array('rgb' => PHPExcel_Style_Color::COLOR_WHITE)
              )
          )
      );
    $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);*/



    // write header

    $objSheet->getCell('A1')->setValue('Date');
    $objSheet->getCell('B1')->setValue('Stduio3');
    $objSheet->getCell('C1')->setValue('Stduio3A');
    $objSheet->getCell('D1')->setValue('Stduio7');
    $objSheet->getCell('E1')->setValue('Stduio1');
    $objSheet->getCell('F1')->setValue('Stduio2');
    $objSheet->getCell('G1')->setValue('Stduio3');
    $objSheet->getCell('H1')->setValue('Stduio4');
    $objSheet->getCell('I1')->setValue('Stduio5');
    $objSheet->getCell('J1')->setValue('Stduio8 (1D, 1Q)');
    $objSheet->getCell('K1')->setValue('2 bdrm tw nhse');
    $objSheet->getCell('L1')->setValue('Modular 9');
    $objSheet->getCell('M1')->setValue('Modular 10');
    $objSheet->getCell('N1')->setValue('1 Bdrm Apt#11');
    $objSheet->getCell('O1')->setValue('Mobile Home');

    $border_style= array('borders' => array('bottom' => 
        array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '766f6e'),)));
    $objSheet->getStyle("B1:O1")->applyFromArray($border_style);
    
    $last_row = 31 * 2 + $dateOffset;
    $border_style= array('borders' => array('right' => 
        array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '766f6e'),)));
    $objSheet->getStyle("A2:A".$last_row)->applyFromArray($border_style);
    

    //GET RESERVATIONS
    $reservations = array();
    $query = 'SELECT r.id, ra.date, ra.date_v2, ra.lodgeid, ra.nights, ra.notes FROM `reservations` as r LEFT JOIN `reservations_assoc` as ra ON r.`id` = ra.`reservation`'
            . ' WHERE ra.type = "l" AND ra.date >= ' . $search_start_date . ' AND ra.date <= ' . $search_end_date;// . ' ORDER BY r.id';
    $result = mysql_query($query);
    //echo $query;
    //echo mysql_error().'<BR>';

    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        array_push($reservations,$row);
    }

    /*echo '<pre>';
    print_r($reservations);
    echo '</pre>';*/


    $columnArr = [];
    $columnArr[5][0] = '';

    $columnArr[6][1] = 'B';
    $columnArr[6][2] = 'C';
    $columnArr[6][3] = 'D';
    $columnArr[6][4] = 'E';
    $columnArr[6][5] = 'F';
    $columnArr[6][6] = 'G';
    $columnArr[6][7] = 'H';
    $columnArr[6][8] = 'I';

    $columnArr[35][1] = 'J';

    $columnArr[23][1] = 'K';

    $columnArr[34][1] = 'L';
    $columnArr[34][2] = 'M';

    $prepareArray = [];
    

    
    for($d = 1; $d <= 31; $d++) {
        $objSheet->getCell('A'.((2*$d)+$dateOffset-1))->setValue($d);
        
        /*$objSheet->getCell('B'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('C'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('D'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('E'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('F'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('G'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('H'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('I'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('J'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('K'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('L'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('M'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('N'.((2*$d)+$dateOffset-1))->setValue('');
        $objSheet->getCell('O'.((2*$d)+$dateOffset-1))->setValue('');*/
        
        $border_style= array('borders' => array('bottom' => 
        array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => '766f6e'),)));
    
        $objSheet->getStyle('A'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('B'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('C'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('D'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('E'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('F'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('G'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('H'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('I'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('J'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('K'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('L'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('M'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('N'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        $objSheet->getStyle('O'.((2*$d)+$dateOffset))->applyFromArray($border_style);
        
    }

    foreach($reservations as $eachReservation) {
        $reservationId = $eachReservation['id'];

        $lodgeid = $eachReservation['lodgeid'];
        if (!in_array($lodgeid, array(5,6,35,23,34))) {
            continue;
        }
        //echo '<br /><br />'.$reservationId.'--';
        $startDateTime = $eachReservation['date'];
        $startDate = date("j", $startDateTime);
        //echo $startDate . '--';
        $endDateTime = strtotime("+{$eachReservation['nights']} day", $startDateTime);
        $endDate = date("j", $endDateTime);
        //echo $endDate . '++';
        for($i = 1; $i <= 7; $i++) {
            //echo '<br />'.$i.'::';
            if (!isset($columnArr[$lodgeid][$i])) {
                $failedReservations[] = $eachReservation['id'];
                break;
            }

            $columnClear = true;
            if ($startDateTime > $endDateTime) {
                $columnClear = false;
            }
            $l = $startDateTime;
            //echo $l.'##';
            //echo $endDateTime.'#'.date("j", $endDateTime).'##';
            while($l <= $endDateTime) {
               $tmpDate = date("j", $l);
               //echo $tmpDate . '*';
               if (!empty($prepareArray[$lodgeid][$i][$tmpDate]['Night'])) {
                   $columnClear = false;
               }
               $l =  strtotime("+1 day", $l);
               //echo $l;
               //if ($l == 30) break;
               //break;
            }

            if ($columnClear === true) {
                $k = $startDateTime;
                while($k <= $endDateTime) {
                    $tmpDate = date("j", $k);
                    $currentCellDay = $columnArr[$lodgeid][$i].((2*$tmpDate)+$dateOffset-1);
                    $currentCellNight = $columnArr[$lodgeid][$i].((2*$tmpDate)+$dateOffset);
                    //echo $tmpDate . '-';
                    
                   
                    if ($k == $startDateTime) {
                        $prepareArray[$lodgeid][$i][$tmpDate]['Night'] = $reservationId;
                        $objSheet->getCell($currentCellNight)->setValue($reservationId);
                        $objSheet->getStyle($currentCellNight)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('dda4b2');
                    } else if ($k == $endDateTime) {
                        $prepareArray[$lodgeid][$i][$tmpDate]['Day'] = $reservationId;
                        $objSheet->getCell($currentCellDay)->setValue($reservationId);
                        $objSheet->getStyle($currentCellDay)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('a1cc61');
                    } else {
                        $prepareArray[$lodgeid][$i][$tmpDate]['Night'] = $reservationId;
                        $prepareArray[$lodgeid][$i][$tmpDate]['Day'] = $reservationId;
                        $objSheet->getCell($currentCellDay)->setValue($reservationId);
                    }
                    
                    //echo $columnArr[$lodgeid][$i].($tmpDate+$dateOffset) . '==';
                    $k =  strtotime("+1 day", $k);
                }
                
                break;
            }
            //break;
        }

        //break;
    }

    $pendingRowNum = (2*31)+$dateOffset+2;
    $objSheet->mergeCells('A'.$pendingRowNum.':O'.$pendingRowNum);
    $objSheet->getCell('A'.$pendingRowNum)->setValue("Pending Reservations: ".implode(', ', $failedReservations));
    
//die();

    // autosize the columns
    $objSheet->getColumnDimension('A')->setAutoSize(true);
    $objSheet->getColumnDimension('B')->setAutoSize(true);
    $objSheet->getColumnDimension('C')->setAutoSize(true);
    $objSheet->getColumnDimension('D')->setAutoSize(true);
    $objSheet->getColumnDimension('E')->setAutoSize(true);
    $objSheet->getColumnDimension('F')->setAutoSize(true);
    $objSheet->getColumnDimension('G')->setAutoSize(true);
    $objSheet->getColumnDimension('H')->setAutoSize(true);
    $objSheet->getColumnDimension('I')->setAutoSize(true);
    $objSheet->getColumnDimension('J')->setAutoSize(true);
    $objSheet->getColumnDimension('K')->setAutoSize(true);
    $objSheet->getColumnDimension('L')->setAutoSize(true);
    $objSheet->getColumnDimension('M')->setAutoSize(true);
    $objSheet->getColumnDimension('N')->setAutoSize(true);
    $objSheet->getColumnDimension('O')->setAutoSize(true);
    
    

    
    //echo  'A2:' . $objSheet->getHighestColumn() . $objSheet->getHighestRow();
    //die();
    


    //Setting the header type
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="file.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');

    /* If you want to save the file on the server instead of downloading, replace the last 4 lines by 
        $objWriter->save('file.xlsx');
    */



    die();

}




require("validate.php");
require("header2.php");




////////////////
/*$search_start_date = mktime(0, 0, 0, $selected_month, $selected_day, $selected_year);//date('Y')
$max_end_date = strtotime("+1 month", $search_start_date);
$max_end_date = strtotime("-1 day", $max_end_date);
//echo date("m-d-Y", $max_end_date) . '==';
$search_end_date = mktime(0, 0, 0, $selected_month_end, $selected_day_end, $selected_year_end);//date('Y')

if ($search_end_date > $max_end_date) {
    $search_end_date = $max_end_date;

    $selected_day_end = date('j', $search_end_date);
    $selected_month_end = date('m', $search_end_date);
    $selected_year_end = date('Y', $search_end_date);
}*/
///////////////

echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Excel</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


echo '<form action="">';
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";

echo '<TR STYLE="background:#'.bgcolor('').'">'
        . '<TD width="30%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'

        . ''
        . '<!--<SELECT NAME="day_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($d = 1; $d < 32; $d++){
            echo '<OPTION VALUE="'.$d.'"';
            if($selected_day == $d): echo ' SELECTED'; endif;
            echo '>'.$d.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;-->'
        . '<SELECT NAME="month_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            foreach($month_arr as $k => $each_month){
            echo '<OPTION VALUE="'.$k.'"';
            if($selected_month == $k): echo ' SELECTED'; endif;
            echo '>'.$each_month.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'

        . '<SELECT NAME="year_selected" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($y = 2017; $y <= 2030; $y++){
            echo '<OPTION VALUE="'.$y.'"';
            if($selected_year == $y): echo ' SELECTED'; endif;
            echo '>'.$y.'</OPTION>';
            }
        echo '</select></td>'
            . '<!--<td  WIDTH="20" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
            . '&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;&nbsp;' //. '<input type="submit" value="go" />'
            . '</TD>-->';
    //. '</TR>'."\n";


echo ''//'<TR STYLE="background:#'.bgcolor('').'">'
        . '<!--<TD width="25%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'

        . ''
        . '<SELECT NAME="day_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($d = 1; $d < 32; $d++){
            echo '<OPTION VALUE="'.$d.'"';
            if($selected_day_end == $d): echo ' SELECTED'; endif;
            echo '>'.$d.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'
        . '<SELECT NAME="month_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            foreach($month_arr as $k => $each_month){
            echo '<OPTION VALUE="'.$k.'"';
            if($selected_month_end == $k): echo ' SELECTED'; endif;
            echo '>'.$each_month.'</OPTION>';
            }
        echo '</select>'
            . '&nbsp;&nbsp;'

        . '<SELECT NAME="year_selected_end" STYLE="">';
            //echo '<OPTION VALUE="0">No</OPTION>';
            for($y = 2017; $y <= 2030; $y++){
            echo '<OPTION VALUE="'.$y.'"';
            if($selected_year_end == $y): echo ' SELECTED'; endif;
            echo '>'.$y.'</OPTION>';
            }
        echo '</select></td>-->'
            . '<td STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
            . '<input name="go" type="submit" value="go" />'
            . '</TD>'
    . '</TR>'."\n";         

echo '</TABLE></form><BR>'."\n\n";


echo '<BR>'."\n\n";


/*echo '<pre>';
print_r($prepareArray);
echo '</pre>';*/

require("footer.php");

