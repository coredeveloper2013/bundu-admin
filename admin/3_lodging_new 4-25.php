<?php //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 6; echo 'REMOVE THIS LINE!!!!!';

$working = 0;

$pageid = "3_lodging";
require("validate.php");
require("header.php");
?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
<?php
if(!isset($_SESSION['lodging']['p'])): $_SESSION['lodging']['p'] = 1; endif;
if(!isset($_SESSION['lodging']['sortby'])): $_SESSION['lodging']['sortby'] = "lodging.`name`"; endif;
	$sortby = array('lodging.`id`'=>'ID','lodging.`name`'=>'Name','lodging.`type`'=>'Type','vendor_name'=>'Vendor');
if(!isset($_SESSION['lodging']['sortdir'])): $_SESSION['lodging']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['lodging']['limit'])): $_SESSION['lodging']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['lodging']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['lodging']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['lodging']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['lodging']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['lodging']['p'] = '1';
}


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
            //INSERT NEW
            $query = 'INSERT INTO `lodging`(`name`,`type`,`subunit`,`available`,`url`,`vendor`)';
                    $query .= ' VALUES("'.$_POST['name'].'","'.$_POST['type'].'","'.$_POST['subunit'].'","'.$_POST['available'].'","'.$_POST['url'].'","'.$_POST['vendor'].'")';
                    @mysql_query($query);
            $thiserror = mysql_error();
            if($thiserror == ""){ $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new lodging unit "'.stripslashes($_POST['name']).'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }

	} else {
            //UPDATE
            $query = 'UPDATE `lodging` SET `name` = "'.$_POST['name'].'", `type` = "'.$_POST['type'].'", `subunit` = "'.$_POST['subunit'].'", `available` = "'.$_POST['available'].'", `url` = "'.$_POST['url'].'", `vendor` = "'.$_POST['vendor'].'"';
                    $query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
                    @mysql_query($query);
            $thiserror = mysql_error();
            if($thiserror == ""): array_push($successmsg,'Saved lodging unit "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

	$successnew = array();
	$successupd = array();

	foreach($_POST['pricing_id'] as $key => $id){
		$startdate = mktime(0,0,0,$_POST['start_month'][$key],$_POST['start_day'][$key],$_POST['start_year'][$key]);
		$enddate = mktime(0,0,0,$_POST['end_month'][$key],$_POST['end_day'][$key],$_POST['end_year'][$key]);
		if($id == "*new*" && $_POST['start_month'][$key] != "*rm*" && $_POST['end_month'][$key] != "*rm*"){
			$query = 'INSERT INTO `lodging_pricing`(`lodgeid`,`price`,`startdate`,`enddate`,`min_nights`,`max_nights`,`calc`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['price'][$key].'","'.$startdate.'","'.$enddate.'","'.$_POST['min_nights'][$key].'","'.$_POST['max_nights'][$key].'","'.$_POST['calc'][$key].'")';
			@mysql_query($query);
			$thiserror = mysql_error();
			$newid = mysql_insert_id();
			if($thiserror == ""){ array_push($successnew,$newid); } else { array_push($errormsg,$thiserror); }
		} elseif($id != "*new*") {
			if($_POST['start_month'][$key] == "*rm*" || $_POST['end_month'][$key] == "*rm*"){
                            $query = 'DELETE FROM `lodging_pricing` WHERE `id` = "'.$id.'" LIMIT 1';
                            @mysql_query($query);
                            $thiserror = mysql_error();
                            if($thiserror == ""){ array_push($successmsg,'Lodging pricing record ('.$id.') was deleted.'); } else { array_push($errormsg,$thiserror); }
			} else {
                            $query = 'UPDATE `lodging_pricing` SET `price` = "'.$_POST['price'][$key].'", `startdate` = "'.$startdate.'", `enddate` = "'.$enddate.'", `min_nights` = "'.$_POST['min_nights'][$key].'", `max_nights` = "'.$_POST['max_nights'][$key].'", `calc` = "'.$_POST['calc'][$key].'" WHERE `id` = "'.$id.'" LIMIT 1';
                            @mysql_query($query);
                            $thiserror = mysql_error();
                            if($thiserror == ""){ array_push($successupd,$id); } else { array_push($errormsg,$thiserror); }
			}
		}
	}

	if(count($successnew) > 0): array_push($successmsg,'The following pricing records were added to lodging type "'.$_REQUEST['type'].'": '.implode(", ",$successnew)); endif;
	if(count($successupd) > 0): array_push($successmsg,'The following pricing records were updated: '.implode(", ",$successupd)); endif;

} 


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Lodging</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


$types = array(
    'y' => 'Yellowstone lodging',
    't' => 'Tour lodging'
);

if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

    $query = 'SELECT * FROM `lodging` WHERE 1';
    $fillform_result = mysql_query($query);

    //GET VENDORS
    $vendors = array();
    $query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        $vendors['v'.$row['id']] = $row;
    }

    //GET OTHER TYPES?
    $forsubs = array();
    $query = 'SELECT * FROM `lodging` WHERE `id` != "'.getval('id').'" AND `type` = "y" ORDER BY `name` ASC';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        $forsubs['f'.$row['id']] = $row;
    }


    bgcolor('');

    echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
    echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
    echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

    echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
            //echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:300px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Type</TD><TD><SELECT NAME="type" STYLE="width:200px;">';
                    foreach($types as $key => $name){
                    echo '<OPTION VALUE="'.$key.'"';
                    if(getval('type') == $key): echo ' SELECTED'; endif;
                    echo '>'.$name.'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Website URL</TD><TD><INPUT TYPE="text" NAME="url" STYLE="width:300px;" VALUE="'.getval('url').'"></TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
                    foreach($vendors as $vendor){
                    echo '<OPTION VALUE="'.$vendor['id'].'"';
                    if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
                    echo '>'.$vendor['name'].'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Sub-Unit?<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>Is this unit part of a larger unit?</SPAN></TD><TD><SELECT NAME="subunit" STYLE="width:200px;">';
                    echo '<OPTION VALUE="0">No</OPTION>';
                    foreach($forsubs as $unit){
                    echo '<OPTION VALUE="'.$unit['id'].'"';
                    if(getval('subunit') == $unit['id']): echo ' SELECTED'; endif;
                    echo '>'.$unit['name'].'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Units available<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>How many of these units are<BR>available on each date listed below?</SPAN></TD><TD><INPUT TYPE="text" NAME="available" STYLE="width:60px;" VALUE="'.getval('available').'"></TD></TR>'."\n";
            echo '</TABLE><BR>'."\n\n";


if($_REQUEST['edit'] != "*new*"){
	echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Order URL: <A HREF="https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'" TARGET="_blank">https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'</A></SPAN><BR>'."\n\n";
}

echo '<BR>'."\n\n";
?>
  <style>
      .ui-dialog{
          font-size: 12px;
      }
  </style>
 <div id="dialog-form" title="Update Price" >
     <p class="validateTips" style="display: none;"></p>
  <form>
    <fieldset>
        <input type="hidden" id="dialog_lodging_price_id" value="" />
      <div for="name" style="width: 50px; float: left;">Price</div>
      <input type="text" name="dialog_price" id="dialog_price" value="0.00" class="text ui-widget-content ui-corner-all" style="width: 50px; height: 22px;">
      <div style="clear: both;"></div>
      <label for="name" style="width: 50px; float: left;">Units</label>
      <input type="text" name="dialog_unit" id="dialog_unit" value="0" class="text ui-widget-content ui-corner-all" style="width: 50px; height: 22px;">
      
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
  
<?php
echo '<TABLE ID="pricetable" BORDER="0" CELLPADDING="2" CELLSPACING="1" WIDTH="1000">'."\n\n";

echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">Lodging Name</TD>';
        for($d = 1; $d <= 31; $d++) {
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">'.$d.'</TD>';
        }
	echo '</TR>'."\n";

        $month_first_day = mktime(0, 0, 0, date('n'), date('j'), 2013);//date('Y')
        $month_last_day = mktime(0, 0, 0, date('n'), date('j'), 2018);//date('Y')
        
        while($fillform_row = mysql_fetch_assoc($fillform_result)) {
            //GET LODGING PRICING
            $pricing = array();
            $query = 'SELECT * FROM `lodging_pricing_new` WHERE `lodgeid` = "'.$fillform_row['id'].'" AND date >= ' . $month_first_day . ' AND date <= ' . $month_last_day . ' ORDER BY `date` ASC';
            $result = mysql_query($query);
            $num_results = mysql_num_rows($result);
            for($i=0; $i<$num_results; $i++){
                $row = mysql_fetch_assoc($result);
                /*echo '<pre>';
                print_r($row['date']);
                echo '</pre>';*/
                //array_push($pricing,$row);
                $day = date("j", $row['date']);
                $pricing[$day] = $row;
            }
    
            echo '<TR BGCOLOR="#'.bgcolor('').'">';
                echo '<TD ALIGN="center" STYLE="padding-left:6px;  font-size: 12px;">';
                    echo $fillform_row['name'];
                echo '</TD>';
                for($d = 1; $d <= 31; $d++) {
                    echo '<TD ALIGN="center" class="each_cell" STYLE="padding-left:3px; font-size: 10px; cursor: pointer;" WIDTH="30">';
                        if (!empty($pricing[$d])) echo $pricing[$d]['price'];
                        //else echo '<a  href="javascript: void(0)">x</a>';
                        echo '<INPUT TYPE="text" class="lodging_pricing_id" VALUE="'.$pricing[$d]['id'].'">'."\n\n";
                    echo '</TD>';
                }
            echo '</TR>'."\n";
        }

echo '</TABLE>'."\n\n";


echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";
?>
  
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      //name = $( "#name" ),
      //email = $( "#email" ),
      //password = $( "#password" ),
      //allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
        $('.validateTips').css('display', 'block');
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n ) {
      if ( o.val().length < 1 ) {
        o.addClass( "ui-state-error" );
        updateTips( n + " Cannot be empty." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser() {
        var valid = true;
        var dialog_price = $("#dialog_price");
        var dialog_unit = $("#dialog_unit");
        var allFields = $( [] ).add( dialog_price ).add( dialog_unit );
        
        allFields.removeClass( "ui-state-error" );
        $('.validateTips').css('display', 'hidden');

        valid = valid && checkLength( dialog_price, "Price");
        valid = valid && checkLength( dialog_unit, "Unit");
        //valid = valid && checkLength( password, "password", 5, 16 );

        valid = valid && checkRegexp( dialog_price, /^([0-9.])+$/i, "Any number" );
        valid = valid && checkRegexp( dialog_unit, /^([0-9])+$/i, "Any digit" );
        //valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

        if ( valid ) {
          alert('yes');
          dialog.dialog( "close" );
        }
        
        return valid;
    }
 
    dialog = $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 200,
        width: 160,
        modal: false,
        buttons: {
            "Update": addUser,
            Cancel: function() {
                dialog.dialog( "close" );
            }
        },
        close: function() {
          //form[ 0 ].reset();
          //allFields.removeClass( "ui-state-error" );
        },
    });
    
    /*form = dialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addUser();
    });*/
 
    $( ".each_cell" ).on( "click", function() {
    
        var lodging_price_id = $('.lodging_pricing_id', this).val();
        $("#dialog_lodging_price_id").val(lodging_price_id);
        
        dialog.dialog( "open" );
        
        //$(".ui-dialog-titlebar").hide();
        
        dialog.dialog('widget').position({
            my: "left top",
            at: "right bottom",
            of: this //window
        });
        
        return false;
    });
    
});
</script>
  
  
  
<?php  

require("footer.php");

?>