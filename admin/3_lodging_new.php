<?php //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 6; echo 'REMOVE THIS LINE!!!!!';

$working = 0;

$pageid = "3_lodging_new";
require("validate.php");
require("header2.php");
?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
  <style>
    label, input { display:block; }
    input.text { margin-bottom:5px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:5px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
<?php
if(!isset($_SESSION['lodging']['p'])): $_SESSION['lodging']['p'] = 1; endif;
if(!isset($_SESSION['lodging']['sortby'])): $_SESSION['lodging']['sortby'] = "lodging.`name`"; endif;
	$sortby = array('lodging.`id`'=>'ID','lodging.`name`'=>'Name','lodging.`type`'=>'Type','vendor_name'=>'Vendor');
if(!isset($_SESSION['lodging']['sortdir'])): $_SESSION['lodging']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['lodging']['limit'])): $_SESSION['lodging']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['lodging']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['lodging']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['lodging']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['lodging']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['lodging']['p'] = '1';
}


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
            //INSERT NEW
            $query = 'INSERT INTO `lodging`(`name`,`type`,`subunit`,`available`,`url`,`vendor`)';
                    $query .= ' VALUES("'.$_POST['name'].'","'.$_POST['type'].'","'.$_POST['subunit'].'","'.$_POST['available'].'","'.$_POST['url'].'","'.$_POST['vendor'].'")';
                    @mysql_query($query);
            $thiserror = mysql_error();
            if($thiserror == ""){ $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new lodging unit "'.stripslashes($_POST['name']).'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }

	} else {
            //UPDATE
            $query = 'UPDATE `lodging` SET `name` = "'.$_POST['name'].'", `type` = "'.$_POST['type'].'", `subunit` = "'.$_POST['subunit'].'", `available` = "'.$_POST['available'].'", `url` = "'.$_POST['url'].'", `vendor` = "'.$_POST['vendor'].'"';
                    $query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
                    @mysql_query($query);
            $thiserror = mysql_error();
            if($thiserror == ""): array_push($successmsg,'Saved lodging unit "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

	$successnew = array();
	$successupd = array();

	foreach($_POST['pricing_id'] as $key => $id){
		$startdate = mktime(0,0,0,$_POST['start_month'][$key],$_POST['start_day'][$key],$_POST['start_year'][$key]);
		$enddate = mktime(0,0,0,$_POST['end_month'][$key],$_POST['end_day'][$key],$_POST['end_year'][$key]);
		if($id == "*new*" && $_POST['start_month'][$key] != "*rm*" && $_POST['end_month'][$key] != "*rm*"){
			$query = 'INSERT INTO `lodging_pricing`(`lodgeid`,`price`,`startdate`,`enddate`,`min_nights`,`max_nights`,`calc`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['price'][$key].'","'.$startdate.'","'.$enddate.'","'.$_POST['min_nights'][$key].'","'.$_POST['max_nights'][$key].'","'.$_POST['calc'][$key].'")';
			@mysql_query($query);
			$thiserror = mysql_error();
			$newid = mysql_insert_id();
			if($thiserror == ""){ array_push($successnew,$newid); } else { array_push($errormsg,$thiserror); }
		} elseif($id != "*new*") {
			if($_POST['start_month'][$key] == "*rm*" || $_POST['end_month'][$key] == "*rm*"){
                            $query = 'DELETE FROM `lodging_pricing` WHERE `id` = "'.$id.'" LIMIT 1';
                            @mysql_query($query);
                            $thiserror = mysql_error();
                            if($thiserror == ""){ array_push($successmsg,'Lodging pricing record ('.$id.') was deleted.'); } else { array_push($errormsg,$thiserror); }
			} else {
                            $query = 'UPDATE `lodging_pricing` SET `price` = "'.$_POST['price'][$key].'", `startdate` = "'.$startdate.'", `enddate` = "'.$enddate.'", `min_nights` = "'.$_POST['min_nights'][$key].'", `max_nights` = "'.$_POST['max_nights'][$key].'", `calc` = "'.$_POST['calc'][$key].'" WHERE `id` = "'.$id.'" LIMIT 1';
                            @mysql_query($query);
                            $thiserror = mysql_error();
                            if($thiserror == ""){ array_push($successupd,$id); } else { array_push($errormsg,$thiserror); }
			}
		}
	}

	if(count($successnew) > 0): array_push($successmsg,'The following pricing records were added to lodging type "'.$_REQUEST['type'].'": '.implode(", ",$successnew)); endif;
	if(count($successupd) > 0): array_push($successmsg,'The following pricing records were updated: '.implode(", ",$successupd)); endif;

} 


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Lodging</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


$types = array(
    'y' => 'Yellowstone lodging',
    't' => 'Tour lodging'
);

//if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){
if(1){

    $query = 'SELECT * FROM `lodging` WHERE type = "y"';
    $fillform_result = mysql_query($query);

    //GET VENDORS
    $vendors = array();
    $query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        $vendors['v'.$row['id']] = $row;
    }

    //GET OTHER TYPES?
    $forsubs = array();
    $query = 'SELECT * FROM `lodging` WHERE `id` != "'.getval('id').'" AND `type` = "y" ORDER BY `name` ASC';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        $forsubs['f'.$row['id']] = $row;
    }


    bgcolor('');

    //echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
    //echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
    //echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

    //echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
            /*echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:300px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Type</TD><TD><SELECT NAME="type" STYLE="width:200px;">';
                    foreach($types as $key => $name){
                    echo '<OPTION VALUE="'.$key.'"';
                    if(getval('type') == $key): echo ' SELECTED'; endif;
                    echo '>'.$name.'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Website URL</TD><TD><INPUT TYPE="text" NAME="url" STYLE="width:300px;" VALUE="'.getval('url').'"></TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
                    foreach($vendors as $vendor){
                    echo '<OPTION VALUE="'.$vendor['id'].'"';
                    if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
                    echo '>'.$vendor['name'].'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Sub-Unit?<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>Is this unit part of a larger unit?</SPAN></TD><TD><SELECT NAME="subunit" STYLE="width:200px;">';
                    echo '<OPTION VALUE="0">No</OPTION>';
                    foreach($forsubs as $unit){
                    echo '<OPTION VALUE="'.$unit['id'].'"';
                    if(getval('subunit') == $unit['id']): echo ' SELECTED'; endif;
                    echo '>'.$unit['name'].'</OPTION>';
                    }
                    echo '</TD></TR>'."\n";
            echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Units available<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For Yellowstone lodging only.<BR>How many of these units are<BR>available on each date listed below?</SPAN></TD><TD><INPUT TYPE="text" NAME="available" STYLE="width:60px;" VALUE="'.getval('available').'"></TD></TR>'."\n";*/
            
    //echo '</TABLE>';
    
                $month_arr = [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];
    
                $selected_day = isset($_REQUEST['day_selected']) ? $_REQUEST['day_selected'] : date('j');
                $selected_month = isset($_REQUEST['month_selected']) ? $_REQUEST['month_selected'] : date('n');
                $selected_year = isset($_REQUEST['year_selected']) ? $_REQUEST['year_selected'] : date('Y');
                
                $selected_day_end = isset($_REQUEST['day_selected_end']) ? $_REQUEST['day_selected_end'] : date('j', strtotime("+21 day"));
                $selected_month_end = isset($_REQUEST['month_selected_end']) ? $_REQUEST['month_selected_end'] : date('n');
                $selected_year_end = isset($_REQUEST['year_selected_end']) ? $_REQUEST['year_selected_end'] : date('Y');
    
                $search_start_date = mktime(0, 0, 0, $selected_month, $selected_day, $selected_year);//date('Y')
                $max_end_date = strtotime("+1 month", $search_start_date);
                $max_end_date = strtotime("-1 day", $max_end_date);
                //echo date("m-d-Y", $max_end_date) . '==';
                $search_end_date = mktime(0, 0, 0, $selected_month_end, $selected_day_end, $selected_year_end);//date('Y')
        
                if ($search_end_date > $max_end_date) {
                    $search_end_date = $max_end_date;
                    
                    $selected_day_end = date('j', $search_end_date);
                    $selected_month_end = date('m', $search_end_date);
                    $selected_year_end = date('Y', $search_end_date);
                }
                
                echo '<form action="">';
                echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
                
                echo '<TR STYLE="background:#'.bgcolor('').'">'
                        . '<TD width="25%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
                        
                        . ''
                        . '<SELECT NAME="day_selected" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            for($d = 1; $d < 32; $d++){
                            echo '<OPTION VALUE="'.$d.'"';
                            if($selected_day == $d): echo ' SELECTED'; endif;
                            echo '>'.$d.'</OPTION>';
                            }
                        echo '</select>'
                            . '&nbsp;&nbsp;'
                        . '<SELECT NAME="month_selected" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            foreach($month_arr as $k => $each_month){
                            echo '<OPTION VALUE="'.$k.'"';
                            if($selected_month == $k): echo ' SELECTED'; endif;
                            echo '>'.$each_month.'</OPTION>';
                            }
                        echo '</select>'
                            . '&nbsp;&nbsp;'
                        
                        . '<SELECT NAME="year_selected" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            for($y = 2017; $y <= 2030; $y++){
                            echo '<OPTION VALUE="'.$y.'"';
                            if($selected_year == $y): echo ' SELECTED'; endif;
                            echo '>'.$y.'</OPTION>';
                            }
                        echo '</select></td>'
                            . '<td  WIDTH="20" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
                            . '&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;&nbsp;' //. '<input type="submit" value="go" />'
                            . '</TD>';
                    //. '</TR>'."\n";
                        
                        
               echo ''//'<TR STYLE="background:#'.bgcolor('').'">'
                        . '<TD width="25%" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
                        
                        . ''
                        . '<SELECT NAME="day_selected_end" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            for($d = 1; $d < 32; $d++){
                            echo '<OPTION VALUE="'.$d.'"';
                            if($selected_day_end == $d): echo ' SELECTED'; endif;
                            echo '>'.$d.'</OPTION>';
                            }
                        echo '</select>'
                            . '&nbsp;&nbsp;'
                        . '<SELECT NAME="month_selected_end" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            foreach($month_arr as $k => $each_month){
                            echo '<OPTION VALUE="'.$k.'"';
                            if($selected_month_end == $k): echo ' SELECTED'; endif;
                            echo '>'.$each_month.'</OPTION>';
                            }
                        echo '</select>'
                            . '&nbsp;&nbsp;'
                        
                        . '<SELECT NAME="year_selected_end" STYLE="">';
                            //echo '<OPTION VALUE="0">No</OPTION>';
                            for($y = 2017; $y <= 2030; $y++){
                            echo '<OPTION VALUE="'.$y.'"';
                            if($selected_year_end == $y): echo ' SELECTED'; endif;
                            echo '>'.$y.'</OPTION>';
                            }
                        echo '</select></td>'
                            . '<td STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; ">'
                            . '<input type="submit" value="go" />'
                            . '</TD>'
                    . '</TR>'."\n";         
                        
            echo '</TABLE></form><BR>'."\n\n";


if($_REQUEST['edit'] != "*new*"){
	//echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Order URL: <A HREF="https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'" TARGET="_blank">https://www.bundubashers.com/reserve_lodging.php?type='.$_REQUEST['edit'].'</A></SPAN><BR>'."\n\n";
}

echo '<BR>'."\n\n";
?>
  <style>
      .ui-dialog{
          font-size: 12px;
      }
  </style>
 
  
<?php
echo '<TABLE ID="pricetable" BORDER="0" CELLPADDING="2" CELLSPACING="1" WIDTH="1000">'."\n\n";
?>
  <tr><td colspan="3">  
<div id="dialog-form" title="Update Price" >
     <p class="validateTips" style="display: none;"></p>
  <form>
    <fieldset>
        <input type="hidden" name="dialog_lodging_price_id" id="dialog_lodging_price_id" value="" />
        
        <input type="hidden" name="dialog_day" id="dialog_day" value="" />
        <input type="hidden" name="dialog_month" id="dialog_month" value="" />
        <input type="hidden" name="dialog_year" id="dialog_year" value="" />
        <input type="hidden" name="dialog_lodgeid" id="dialog_lodgeid" value="" />
        
        <div for="name" style="width: 50px; float: left;">Price</div>
        <input type="text" name="dialog_price" id="dialog_price" value="0.00" class="text ui-widget-content ui-corner-all" style="width: 50px; height: 22px;">
        <div style="clear: both;"></div>
        <label for="name" style="width: 50px; float: left;">Units</label>
        <input type="text" name="dialog_unit" id="dialog_unit" value="0" class="text ui-widget-content ui-corner-all" style="width: 50px; height: 22px;">

        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
          
      </td></tr>
<?php  

echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">Lodging Name</TD>';
        echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">&nbsp;</TD>';
        for($cur_date = $search_start_date; $cur_date <= $search_end_date; $cur_date = $cur_date + 3600*24) {
                    $d = date("j", $cur_date);
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">'.$d.'</TD>';
        }
	echo '</TR>'."\n";
                
        while($fillform_row = mysql_fetch_assoc($fillform_result)) {
            //GET LODGING PRICING
            $pricing = array();
            $query = 'SELECT * FROM `lodging_pricing_new` WHERE `lodgeid` = "'.$fillform_row['id'].'" AND date >= ' . $search_start_date . ' AND date <= ' . $search_end_date . ' ORDER BY `date` ASC';
            $result = mysql_query($query);
            $num_results = mysql_num_rows($result);
            for($i=0; $i<$num_results; $i++){
                $row = mysql_fetch_assoc($result);
                /*echo '<pre>';
                print_r($row['date']);
                echo '</pre>';*/
                //array_push($pricing,$row);
                $day = date("j", $row['date']);
                $pricing[$day] = $row;
            }
    
            echo "<tr><td colspan='33' style='padding-top: 5px;'>" . '<SPAN STYLE="font-family:Arial; font-size:10pt;">Order URL: <A HREF="https://www.bundubashers.com/reserve_lodging_new.php?type='.$fillform_row['id'].'" TARGET="_blank">https://www.bundubashers.com/reserve_lodging_new.php?type='.$fillform_row['id'].'</A></SPAN><BR>'."\n\n" . "</td></tr>";
            echo '<TR BGCOLOR="#'.bgcolor('').'">';
                echo '<TD ALIGN="center" STYLE="padding-left:6px;  font-size: 12px; width: 100px;">';
                    echo $fillform_row['name'];
                echo '</TD>';
                echo '<TD ALIGN="center" STYLE="padding-left:2px;  font-size: 12px; ">';
                    echo '$ <br /> u';
                echo '</TD>';
                for($cur_date = $search_start_date; $cur_date <= $search_end_date; $cur_date = $cur_date + 3600*24) {
                    $d = date("j", $cur_date);
                    echo '<TD ALIGN="center" class="each_cell" STYLE="padding-left:3px; font-size: 10px; cursor: pointer;" WIDTH="30">';
                        echo "<span class='price_span'>";
                        if (!empty($pricing[$d])) {
                            echo number_format($pricing[$d]['price'], 0) ;
                        } else echo '0';
                        echo "</span>";
                        echo "<br />";
                        echo "<span class='unit_span'>";
                        if (!empty($pricing[$d])) {
                            echo $pricing[$d]['unit'];
                        } else echo '0';
                        echo "</span>";
                        //else echo '<a  href="javascript: void(0)">x</a>';
                        echo '<INPUT TYPE="hidden" class="day" VALUE="'.$d.'">'."\n\n";
                        echo '<INPUT TYPE="hidden" class="month" VALUE="'.$selected_month.'">'."\n\n";
                        echo '<INPUT TYPE="hidden" class="year" VALUE="'.$selected_year.'">'."\n\n";
                        
                        echo '<INPUT TYPE="hidden" class="lodgeid" VALUE="'.$fillform_row['id'].'">'."\n\n";
                        echo '<INPUT TYPE="hidden" class="lodging_pricing_id" VALUE="'.$pricing[$d]['id'].'">'."\n\n";
                        echo '<INPUT TYPE="hidden" class="price" VALUE="'.$pricing[$d]['price'].'">'."\n\n";
                        echo '<INPUT TYPE="hidden" class="unit" VALUE="'.$pricing[$d]['unit'].'">'."\n\n";
                    echo '</TD>';
                }
            echo '</TR>'."\n";
        }

echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">&nbsp;</TD>';
        echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; padding-left:6px;">&nbsp;</TD>';
        for($cur_date = $search_start_date; $cur_date <= $search_end_date; $cur_date = $cur_date + 3600*24) {
                    $d = date("j", $cur_date);
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">'.$d.'</TD>';
        }
	echo '</TR>'."\n";
        
echo '</TABLE>'."\n\n";


//echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";
?>
  
  
  <script src="/jquery-1.12.4.js"></script>
  <script src="/jquery-ui.js"></script>
  <script>
  $( function() {
    var dialog, form, cell_this,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      //emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      //name = $( "#name" ),
      //email = $( "#email" ),
      //password = $( "#password" ),
      //allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
        $('.validateTips').css('display', 'block');
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n ) {
      if ( o.val().length < 1 ) {
        o.addClass( "ui-state-error" );
        updateTips( n + " Cannot be empty." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function process_data(dialog_price, dialog_unit)
    {
        var lodging_price_id = $("#dialog_lodging_price_id").val();
        var day = $("#dialog_day").val();
        var month = $("#dialog_month").val();
        var year = $("#dialog_year").val();
        var lodgeid = $("#dialog_lodgeid").val();
          // alert(dialog_price + '==' + dialog_unit + '==' + day+'=='+month+'=='+year+'=='+lodgeid+'=='+lodging_price_idx);
        $.ajax({
            url: '3_lodging_update_ajax.php',
            type: 'post',
            dataType: 'html',
            data: {
                lodging_price_id: lodging_price_id,
                price: dialog_price,
                unit: dialog_unit,
                day: day,
                month: month,
                year: year,
                lodgeid: lodgeid
            },
            success: function(result){
                //alert(result);
                //alert($('.price_span', cell_this).html());
                $('.price_span', cell_this).html(dialog_price);
                $('.unit_span', cell_this).html(dialog_unit);
 
            },
            error: function(x, y, z){
                alert(y);
            }
        });
    }
    
    function addUser() {
        var valid = true;
        var dialog_price = $("#dialog_price");
        var dialog_unit = $("#dialog_unit");
        var allFields = $( [] ).add( dialog_price ).add( dialog_unit );
        
        allFields.removeClass( "ui-state-error" );
        $('.validateTips').css('display', 'hidden');

        valid = valid && checkLength( dialog_price, "Price");
        valid = valid && checkLength( dialog_unit, "Unit");
        //valid = valid && checkLength( password, "password", 5, 16 );

        valid = valid && checkRegexp( dialog_price, /^([0-9.])+$/i, "Any number" );
        valid = valid && checkRegexp( dialog_unit, /^([0-9])+$/i, "Any digit" );
        //valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

        if ( valid ) {
          
          process_data(dialog_price.val(), dialog_unit.val());
          
          dialog.dialog( "close" );
        }
        
        return valid;
    }
 
    dialog = $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 160,
        width: 160,
        modal: false,
        buttons: {
            "Update": addUser,
            "Cancel": function() {
                dialog.dialog( "close" );
            }
        },
        close: function() {
          //form[ 0 ].reset();
          //allFields.removeClass( "ui-state-error" );
        },
    });
    
    /*form = dialog.find( "form" ).on( "submit", function( event ) {
        event.preventDefault();
        addUser();
    });*/
 
    $( ".each_cell" ).on( "click", function() {
    
        var lodging_price_id = $('.lodging_pricing_id', this).val();
        $("#dialog_lodging_price_id").val(lodging_price_id);
        
        var price = $('.price', this).val();
        $("#dialog_price").val(price);
        
        var unit = $('.unit', this).val();
        $("#dialog_unit").val(unit);
        
        var day = $('.day', this).val();
        $("#dialog_day").val(day);
        
        var month = $('.month', this).val();
        $("#dialog_month").val(month);
        
        var year = $('.year', this).val();
        $("#dialog_year").val(year);
        
        var lodgeid = $('.lodgeid', this).val();
        $("#dialog_lodgeid").val(lodgeid);
        
        cell_this = this;
        
        dialog.dialog( "open" );
        
        //$(".ui-dialog-titlebar").hide();
        
        dialog.dialog('widget').position({
            my: "left top",
            at: "left bottom",
            of: this //window
        });
        
        //return false;
    });
    
});
</script>
  
  
  
<?php  

require("footer.php");

?>