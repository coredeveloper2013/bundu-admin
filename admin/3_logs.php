<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_logs";
require("validate.php");
require("header.php");

if(!isset($_SESSION['logs']['p'])): $_SESSION['logs']['p'] = 1; endif;
if(!isset($_SESSION['logs']['search'])): $_SESSION['logs']['search'] = ''; endif;
if(!isset($_SESSION['logs']['sortby'])): $_SESSION['logs']['sortby'] = 'logs.`time`'; endif;
	$sortby = array('logs.`time`'=>'Date & Time','logs.`page`'=>'Page','`user_name`'=>'User');
if(!isset($_SESSION['logs']['sortdir'])): $_SESSION['logs']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['logs']['limit'])): $_SESSION['logs']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['logs']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['search'])): $_SESSION['logs']['search'] = $_REQUEST['search']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['logs']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['logs']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['logs']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['logs']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Logs</U></FONT><BR>';

printmsgs($successmsg,$errormsg);


//GET LOGS
$logs = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS logs.*, users.`username` AS `user_name` FROM `logs` LEFT JOIN `users` ON logs.`user` = users.`userid`';
	if($_SESSION['logs']['search'] != ""){ $query .= ' WHERE logs.`log` LIKE "%'.$_SESSION['logs']['search'].'%"'; }
	$query .= ' ORDER BY '.$_SESSION['logs']['sortby'].' '.$_SESSION['logs']['sortdir'].', logs.`time` DESC';
	$query .= ' LIMIT '.(($_SESSION['logs']['p']-1)*$_SESSION['logs']['limit']).','.$_SESSION['logs']['limit'];
$result = mysql_query($query);
echo mysql_error().'<BR>';
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($logs,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['logs']['limit']);
if($numpages > 0 && $_SESSION['logs']['p'] > $numpages): $_SESSION['logs']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Search for:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="search" STYLE="width:120px; font-size:9pt;" VALUE="'.$_SESSION['logs']['search'].'"></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['logs']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['logs']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['logs']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['logs']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['logs']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['logs']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['logs']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['logs']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['logs']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Date/Time</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">User</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Page</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Log</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($logs as $key => $row){
	if($row['user_name'] == ""){ $row['user_name'] = $row['user']; }
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:9pt;">'.date("n/j/Y",$row['time']).'<BR>'.date("g:ia",$row['time']).'</TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['user_name'].'</TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['page'].'</TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:9pt;">'.nl2br($row['log']).'</TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['logs']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['logs']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['logs']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['logs']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['logs']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing


echo '</FORM>'."\n\n";


require("footer.php");

?>