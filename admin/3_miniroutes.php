<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 3;

$working = 0;

$pageid = "3_miniroutes";
require("validate.php");
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""): $onload = array('hl_left();'); endif; //checkend()
require("header.php");

if(!isset($_SESSION['miniroutes']['p'])): $_SESSION['miniroutes']['p'] = 1; endif;
if(!isset($_SESSION['miniroutes']['sortby'])): $_SESSION['miniroutes']['sortby'] = "miniroutes.`title`"; endif;
	$sortby = array('miniroutes.`id`'=>'ID','miniroutes.`title`'=>'Title');
if(!isset($_SESSION['miniroutes']['sortdir'])): $_SESSION['miniroutes']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['miniroutes']['limit'])): $_SESSION['miniroutes']['limit'] = "200"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['miniroutes']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['filter']) && $_REQUEST['filter'] != ""): $_SESSION['miniroutes']['filter'] = $_REQUEST['filter']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['miniroutes']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['miniroutes']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['miniroutes']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['miniroutes']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';


//!FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$_POST['description'] = trim($_POST['description']);

	if($_POST['edit'] == "*new*"){
	//!INSERT NEW -------------------------
	$query = 'INSERT INTO `miniroutes`(`title`,`description`,`price`)';
		$query .= ' VALUES("'.$_POST['title'].'","'.$_POST['description'].'","'.$_POST['price'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new mini route "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	} else {

	//!UPDATE -------------------------
	$query = 'UPDATE `miniroutes` SET `title` = "'.$_POST['title'].'", `description` = "'.$_POST['description'].'", `price` = "'.$_POST['price'].'"';
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved mini route "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	}

	//!ROUTES -------------------------
	@mysql_query('DELETE FROM `miniroutes_assoc` WHERE `minirouteid` = "'.$_REQUEST['edit'].'"');
	if(isset($_POST['miniroute_id'])){
	foreach($_POST['miniroute_id'] as $key => $id){ if($id != ""){
		$query = 'INSERT INTO `miniroutes_assoc`(`minirouteid`,`type`,`typeid`,`order`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['miniroute_type'][$key].'","'.$_POST['miniroute_id'][$key].'","'.$key.'")';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""): array_push($errormsg,$thiserror); endif;
		}} //End foreach
		} //End if statement

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//!DELETE MINI ROUTE
	$query = 'DELETE FROM `miniroutes` WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){
		array_push($successmsg,'Mini route '.$_POST['edit'].' has been deleted.');
		$_POST['edit'] = '';
		$_REQUEST['edit'] = '';
		} else {
		array_push($errormsg,$thiserror);
		}

}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Mini Routes</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){
//!EDIT MINI ROUTES **************************************************************//


if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `miniroutes` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}


include_once('sups/timezones.php');

//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}

//GET ROUTES
$routes = array();
$query = 'SELECT * FROM `routes` ORDER BY `name` ASC, `dep_loc` ASC, `arr_loc` ASC, `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['description'] = ''; $row['details'] = '';
	if($row['anchor'] == 'arr'){
		$dep_time = ($today+$row['arr_time']-$row['travel_time']+tz_offset($row['arr_loc'],$row['dep_loc']));
		$arr_time = ($today+$row['arr_time']);
		$row['arr_time'] = ($today+$row['arr_time']+tz_offset($row['arr_loc'],9));
		$row['dep_time'] = ($row['arr_time']-$row['travel_time']);
		} else {
		$dep_time = ($today+$row['dep_time']);
		$arr_time = ($today+$row['dep_time']+$row['travel_time']+tz_offset($row['dep_loc'],$row['arr_loc']));
		$row['dep_time'] = ($today+$row['dep_time']+tz_offset($row['dep_loc'],9));
		$row['arr_time'] = ($row['dep_time']+$row['travel_time']);
		}

	$row['subhead'] = $locations['l'.$row['dep_loc']]['name'].' '.date("g:ia",$dep_time).' <I>to</I> '.$locations['l'.$row['arr_loc']]['name'].' '.date("g:ia",$arr_time);
	$routes['r'.$row['id']] = $row;
	//array_push($routes,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($routes); echo '</PRE>';

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

$tour_types = array(
	'r' => 'routes'
	);
function valid_step($type,$id){
	global $tour_types;
	global ${$tour_types[$type]};
	if( isset( ${$tour_types[$type]}[$type.$id] ) ){
		return true;
		}
	return false;
	}

//GET STEPS OF TOUR
$fillform['steps'] = array();
$query = 'SELECT * FROM `miniroutes_assoc` WHERE `minirouteid` = "'.getval('id').'" AND `minirouteid` != 0 ORDER BY `order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if( valid_step($row['type'],$row['typeid']) ){
		array_push($fillform['steps'],$row);
		//} else {
		//echo 'not<BR>';
		}
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($fillform['steps']); echo '</PRE>';


?><SCRIPT><!--

//window.onLoad = hl_left;

var miniroute = new Array();
<?
	echo "\t".'miniroute = new Array();'."\n";
	foreach($fillform['steps'] as $i => $step){
		echo "\t".'miniroute['.$i.'] = new Array(); miniroute['.$i.']["type"] = "'.$step['type'].'"; miniroute['.$i.']["id"] = "'.$step['typeid'].'";'."\n";
		}
	?>

var optinfo = new Array();
<?
	foreach($routes as $row){
		echo "\t".'optinfo["r'.$row['id'].'"] = new Array(); optinfo["r'.$row['id'].'"]["type"] = "r"; optinfo["r'.$row['id'].'"]["sel"] = 0; optinfo["r'.$row['id'].'"]["hlp"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["r'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	?>

var colors = new Array(); colors['r'] = 'blue'; colors['l'] = 'green'; colors['e'] = 'ltblue'; colors['p'] = 'orange'; colors['a'] = 'red'; colors['i'] = 'grey';

/*function show_hide(c){
	var state = document.getElementById('choose_'+c).style.display;
	var sections = new Array('Routes','Lodging','Lodging extensions','Transport','Activities','Itinerary');
	for(i in sections){
		document.getElementById('choose_'+sections[i]).style.display = 'none';
		document.getElementById('label_'+sections[i]).innerHTML = '&nbsp;+&nbsp;'+sections[i];
		}
	if(state == 'none'){
		document.getElementById('choose_'+c).style.display = '';
		document.getElementById('label_'+c).innerHTML = '&nbsp;-&nbsp;'+c;
		//window.location = '#designtop';
		hl_left();
		}
	adjlcon();
	}*/

function hl(type,id,o){
	var x = document.getElementById(type+'_'+id);
	var sel = 0; if(optinfo[type+id] != undefined){ sel = optinfo[type+id]['sel']; }
	var hlp = 0; if(type == "r" && optinfo['r'+id] != undefined){ hlp = optinfo['r'+id]['hlp']; }

	if(o == 1){
		x.style.color = '#0000FF';
		} else if(o == 0) {
		x.style.color = '#000000';
		}
	if(o == 1 || sel == 1){
		x.style.backgroundImage = 'url(\'img/fade_r_'+colors[type]+'.jpg\')';
		x.style.backgroundPosition = 'center right';
		x.style.backgroundRepeat = 'repeat-y';
		} else if(hlp == 1){
		x.style.backgroundImage = 'url(\'img/fade_r_gold.jpg\')';
		x.style.backgroundPosition = 'center right';
		x.style.backgroundRepeat = 'repeat-y';
		} else {
		x.style.backgroundImage = '';
		x.style.backgroundPosition = '';
		x.style.backgroundRepeat = '';
		}
	}

function hl_left(){
	//alert("Test");

	//Clear all highlighted
	for(i in optinfo){
		optinfo[i]['sel'] = 0;
		optinfo[i]['hlp'] = 0;
		}

	//Find included
	for(t in miniroute){
		optinfo[miniroute[t]['type']+miniroute[t]['id']]['sel'] = 1;
		}

	//Find next routes
	lastr = findlastr(miniroute.length);
	if(lastr > -1){
		for(i in optinfo){
			if(optinfo[i]['type'] == 'r' && optinfo[i]['dep_loc'] == optinfo['r'+miniroute[lastr]['id']]['arr_loc']){
				optinfo[i]['hlp'] = 1;
				}
			}
		}

	//Highlight all
	for(i in optinfo){
		hl(optinfo[i]['type'],optinfo[i]['id'],'b');
		}
	}

function adjlcon(){
	document.getElementById('lcon').style.height = (document.getElementById('toverview').offsetHeight)+'px';
	}

function addtominiroute(type,id){
	var add = new Array();
		add['id'] = id;
		add['type'] = type;

	miniroute.push(add);
	build();
	}

function delsetup(obj){
	var x = document.getElementById('fordel');
	x.style.position = 'absolute';
	x.style.display = '';
	x.style.padding = '0px';
	x.innerHTML = obj.innerHTML;
	x.style.zIndex = 1000;

	var boxpos = findPos(obj);
	x.style.top = parseInt(boxpos[1])+'px';
	x.style.left = parseInt(boxpos[0])+'px';
	}

function rmvfromminiroute(key){
	miniroute.splice(key,1);
	build();
	}


function build(){
	var t = document.getElementById('toverview');

	t.innerHTML = '';
	var code = '';
	if(miniroute.length > 0){
		for(i in miniroute){
			var optid = miniroute[i]['type']+miniroute[i]['id'];
			code += '<DIV ID="tstep_'+i+'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_'+colors[miniroute[i]['type']]+'.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">'+"\n";
				code += '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from miniroute" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromminiroute(\''+i+'\');"></SPAN>'+"\n";
				code += '&#149; '+optinfo[optid]['name'];
					code += '<BR><SPAN STYLE="font-size:10px; color:#666666;">'+optinfo[optid]['subhead']+'</SPAN>'+"\n";
				code += '</DIV>'+"\n\n";

			}
		} else {
		var code = '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>';
		}
	t.innerHTML = code;

	buildfields();
	CreateDragContainer();
	document.getElementById('fordel').display = 'none';
	hl_left();
	adjlcon();
	}

function buildfields(){
	var f = document.getElementById('tourfields');
	f.innerHTML = '';

	for(i in miniroute){
		f.innerHTML += '<INPUT TYPE="hidden" NAME="miniroute_type[]" VALUE="'+miniroute[i]['type']+'"><INPUT TYPE="hidden" NAME="miniroute_id[]" VALUE="'+miniroute[i]['id']+'">'+"\n";
		}
	}

function findlastr(i){
	findit = (parseInt(i) - 1);
	if(findit > -1){
		while(miniroute[findit] != undefined && miniroute[findit]['type'] != 'r'){ findit--; }
		} else if(findit == 0 && miniroute[findit]['type'] != 'r'){
		findit = -1;
		}
	return findit;
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

//BEGIN DRAG AND DROP FUNCTIONS
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = 'n';
var dragTarget = 'n';
var dropTargets = new Array();
var mouseOffset = null;

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function CreateDragContainer(){
	dropTargets = new Array();
	for(i in miniroute){
		addDropTarget(document.getElementById('tstep_'+i));
		}
	}

function addDropTarget(obj){
	dropTargets.push(obj);
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets.length > 0){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(item){
	if(!item) return;
	item.onmousedown = function(ev){

		dragObject = document.getElementById("forshadow");

		dragObject.innerHTML = item.innerHTML;
		dragObject.style.width = item.offsetWidth;
		dragObject.style.height = item.offsetHeight;
		dragObject.style.padding = item.style.padding;

		dragObject.style.backgroundColor = item.style.backgroundColor;
		dragObject.style.backgroundImage = item.style.backgroundImage;
		dragObject.style.backgroundPosition = item.style.backgroundPosition;
		dragObject.style.backgroundRepeat = item.style.backgroundRepeat;

		dragObject.style.textAlign = 'left';
		dragObject.style.fontFamily = document.getElementById('toverview').style.fontFamily;
		dragObject.style.fontSize = document.getElementById('toverview').style.fontSize;

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin = 'n';
		for(i=0; i<dropTargets.length; i++){
			if(item.id == dropTargets[i].id){
			dragOrigin = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin   = 'n';
	cleartarg();
}

function movem(){

	if(dragOrigin != 'n' && dragTarget != 'n'){
	//alert(dragOrigin+' '+dragTarget);

	var sv = miniroute[dragOrigin];
	miniroute.splice(dragOrigin,1);
	miniroute.splice(dragTarget,0,sv);
	build();

	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';
		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget){
		cleartarg();
		dragTarget = newTarget;
		if(newTarget < dragOrigin){
			document.getElementById( dropTargets[newTarget].id ).style.borderTop = "4px solid #0000FF";
			} else if(newTarget > dragOrigin)  {
			document.getElementById( dropTargets[newTarget].id ).style.borderBottom = "4px solid #0000FF";
			}
		}
	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget != 'n' && testIsValidObject( dropTargets[dragTarget] )){
		document.getElementById( dropTargets[dragTarget].id ).style.borderTop = "0px solid #FFFFFF";
		document.getElementById( dropTargets[dragTarget].id ).style.borderBottom = "1px solid #DDDDDD";
		}
	dragTarget = 'n';
}

function sortNumber(a,b){ return a - b; }

function is_int(value){
	if((parseFloat(value) == parseInt(value)) && !isNaN(parseInt(value))){
    	return true;
		} else {
		return false;
		}
	}

function IsNumeric(sText){
	var ValidChars = "0123456789.";
	var IsNumber = true;
	var Char;
	for(i=0; i<sText.length && IsNumber == true; i++){ 
		Char = sText.charAt(i);
		if(ValidChars.indexOf(Char) == -1){ IsNumber = false; }
		}
	return IsNumber;
	}

function finalconfirm(){
	var r = confirm('Are you sure you\'d like to save?');
	return r;
	}

//--></SCRIPT><? echo "\n\n";


//echo '<INPUT TYPE="button" VALUE="Test" onClick="hl_left();">';


//!BASIC
echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n"; // onSubmit="return finalconfirm();"
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Basic</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Title</TD><TD><INPUT TYPE="text" NAME="title" STYLE="width:400px;" VALUE="'.getval('title').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Price</TD><TD STYLE="font-family:Arial; font-size:10pt;">$<INPUT TYPE="text" NAME="price" STYLE="width:80px;" VALUE="'.getval('price').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Description</TD><TD><TEXTAREA NAME="description" COLS="50" ROWS="4" STYLE="width:400px; height:100px;">'.getval('description').'</TEXTAREA></TD></TR>'."\n";


//!DESIGN MINI ROUTE
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;"><A NAME="designtop"></A>Design Mini Route</TD>';
		echo '</TR>'."\n\n";
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-left:0px; padding-right:0px;">';


//!LEFT SIDE - OPTIONS
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>';
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:48%; padding-right:6px; border-right:2px solid #333333; text-align:center; font-family:Arial; font-size:9pt;" ID="lconcon">';
		echo '<DIV STYLE="overflow:scroll; overflow-x:hidden; height:400px;" ID="lcon">';
			foreach($routes as $row){
			echo '<DIV ID="r_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'r\',\''.$row['id'].'\',1);" onMouseOut="hl(\'r\',\''.$row['id'].'\',0);" onDblClick="addtominiroute(\'r\',\''.$row['id'].'\');">';
				echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to mini route" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtominiroute(\'r\',\''.$row['id'].'\');"></SPAN>';
				echo '<SPAN ID="rhtml_'.$row['id'].'">';
					echo '&#149; '.$row['name'].'<BR>';
					echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
					echo '</SPAN>';
				echo '</DIV>'."\n";
			}
			echo '</DIV>';
			echo '</TD>';


//!RIGHT SIDE - MINI ROUTE OVERVIEW
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:52%; padding-left:6px; text-align:center; font-family:Arial; font-size:11pt;">'."\n\n";

			echo '<DIV ID="toverview" STYLE="padding-top:4px; font-family:Arial; font-size:12pt; text-align:center; min-height:400px;">'."\n";
			if(isset($fillform['steps']) && count($fillform['steps']) > 0){
			$colors = array('r'=>'blue','l'=>'green','e'=>'ltblue','p'=>'orange','a'=>'red','i'=>'grey');
			foreach($fillform['steps'] as $i => $step){
				$name = $routes['r'.$step['typeid']]['name'];
				$subhead = $routes['r'.$step['typeid']]['subhead'];
				echo '<DIV ID="tstep_'.$i.'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_'.$colors[$step['type']].'.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">';
					echo '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from mini route" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromminiroute(\''.$i.'\');"></SPAN>';
					echo '&#149; '.$name.'<BR><SPAN STYLE="font-size:10px; color:#666666;">'.$subhead.'</SPAN>';
					echo '</DIV>'."\n";
				}
			} else {
			echo '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>'."\n";
			}
			echo "\n";

			echo '</TD>';
		echo '</TR></TABLE>'."\n\n";

		echo '</DIV>';
		echo '</TD></TR>'."\n\n";

	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";

echo '<DIV ID="tourfields" STYLE="display:none;">';
	foreach($fillform['steps'] as $i => $step){
		echo '<INPUT TYPE="hidden" NAME="miniroute_type[]" VALUE="'.$step['type'].'"><INPUT TYPE="hidden" NAME="miniroute_id[]" VALUE="'.$step['typeid'].'">'."\n";
		}
	echo '</DIV>'."\n\n";
echo '<DIV ID="forshadow" STYLE="display:none; cursor:move;"></DIV>'."\n\n";
echo '<DIV ID="fordel" STYLE="display:none;" onMouseOut="this.style.display=\'none\'"></DIV>'."\n\n";


if(getval('id') != "*new*"){

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" WIDTH="93%"><BR>'."\n\n";

	echo '<FORM NAME="deleteform" ID="deleteform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return confirm(\'Delete this mini route?\');">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="delete">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Delete" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '</FORM>'."\n\n";

} //End *new* if statement



} else {
//!MINI ROUTE INDEX PAGE ****************************************//


//GET MINIROUTES
$miniroutes = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `miniroutes`';
	$query .= ' ORDER BY '.$_SESSION['miniroutes']['sortby'].' '.$_SESSION['miniroutes']['sortdir'].', miniroutes.`title` ASC';
	$query .= ' LIMIT '.(($_SESSION['miniroutes']['p']-1)*$_SESSION['miniroutes']['limit']).','.$_SESSION['miniroutes']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($miniroutes,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($miniroutes); echo '</PRE>';

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['miniroutes']['limit']);
if($numpages > 0 && $_SESSION['miniroutes']['p'] > $numpages): $_SESSION['miniroutes']['p'] = $numpages; endif;


//echo '<INPUT TYPE="button" VALUE="Design a New Tour" STYLE="width:200px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";

	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['tours']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['miniroutes']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['miniroutes']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['miniroutes']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Go" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


echo '<FORM METHOD="post" NAME="listing" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['miniroutes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['miniroutes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['miniroutes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['miniroutes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['miniroutes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Price</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;">Edit</TD>';
	//echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;" TITLE="View on Bundu Bashers website">View</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($miniroutes as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">'.$row['title'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">$'.$row['price'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	//echo '<TD ALIGN="center"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'" TARGET="_blank"><IMG SRC="img/viewico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['miniroutes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['miniroutes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['miniroutes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['miniroutes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['miniroutes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Mini Route" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="CSV File" onClick="window.location=\'sups/csvexport_tours.php\'">';
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;" DISABLED>';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>