<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_prvtours";
require("validate.php");
require("header.php");

if(!isset($_SESSION['prvtours']['p'])): $_SESSION['prvtours']['p'] = 1; endif;
if(!isset($_SESSION['prvtours']['sortby'])): $_SESSION['prvtours']['sortby'] = "tours_prv.`id`"; endif;
	$sortby = array('tours_prv.`id`'=>'ID','tours_prv.`title`'=>'Title');
if(!isset($_SESSION['prvtours']['sortdir'])): $_SESSION['prvtours']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['prvtours']['limit'])): $_SESSION['prvtours']['limit'] = "200"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['prvtours']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['prvtours']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['prvtours']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['prvtours']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	if(!isset($_REQUEST['showhidden'])): $_SESSION['prvtours']['showhidden'] = '0'; endif;
	$_SESSION['prvtours']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//!FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//Find/Format tour info
	$_POST['desc'] = trim($_POST['desc']);
	$_POST['details'] = trim($_POST['details']);

	if($_POST['edit'] == "*new*"){
	//!INSERT NEW -------------------------
	$query = 'INSERT INTO `tours_prv`(`title`,`amount`,`getweights`,`getpreftime`,`getlunch`,`desc`,`details`,`vendor`)';
		$query .= ' VALUES("'.$_POST['title'].'","'.$_POST['amount'].'","'.$_POST['getweights'].'","'.$_POST['getpreftime'].'","'.$_POST['getlunch'].'","'.$_POST['desc'].'","'.$_POST['details'].'","'.$_POST['vendor'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new private tour "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	} else {

	//!UPDATE -------------------------
	$query = 'UPDATE `tours_prv` SET `title` = "'.$_POST['title'].'", `amount` = "'.$_POST['amount'].'", `getweights` = "'.$_POST['getweights'].'", `getpreftime` = "'.$_POST['getpreftime'].'", `getlunch` = "'.$_POST['getlunch'].'", `desc` = "'.$_POST['desc'].'", `details` = "'.$_POST['details'].'", `vendor` = "'.$_POST['vendor'].'"';
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved private tour "'.stripslashes($_POST['title']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;
	}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){

	$query = 'DELETE FROM `tours_prv` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' private tours were deleted.'); else: array_push($errormsg,$thiserror); endif;

}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Private Tours</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//!EDIT PRIVATE TOUR
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'getweights' => 'n',
		'getlunch' => 'n',
		'getpreftime' => 'n',
		'vendor' => '1'
		);
	} else {
	$query = 'SELECT * FROM `tours_prv` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}


//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}


echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n"; // onSubmit="return finalconfirm();"
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	bgcolor('reset');
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Title</TD><TD><INPUT TYPE="text" NAME="title" STYLE="width:400px;" VALUE="'.getval('title').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
		foreach($vendors as $vendor){
		echo '<OPTION VALUE="'.$vendor['id'].'"';
		if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
		echo '>'.$vendor['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Total price</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="amount" ID="amount" STYLE="width:60px;" VALUE="'.getval('amount').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Ask for guest weights?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getweights" ID="gwn" VALUE="n"';
		if(getval('getweights') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gwn">No</LABEL> / <INPUT TYPE="radio" NAME="getweights" ID="gwy" VALUE="y"';
		if(getval('getweights') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gwy">Yes</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Ask for lunch type?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getlunch" ID="gltn" VALUE="n"';
		if(getval('getlunch') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gltn">No</LABEL> / <INPUT TYPE="radio" NAME="getlunch" ID="glty" VALUE="y"';
		if(getval('getlunch') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="glty">Yes</LABEL></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Preferred time of day?</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="radio" NAME="getpreftime" ID="gptn" VALUE="n"';
		if(getval('getpreftime') == "n"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gptn">No</LABEL> / <INPUT TYPE="radio" NAME="getpreftime" ID="gpty" VALUE="y"';
		if(getval('getpreftime') == "y"): echo ' CHECKED'; endif;
		echo '><LABEL FOR="gpty">Yes</LABEL></TD></TR>'."\n";
	//echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Bundu costs</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="bunducosts" STYLE="width:60px;" VALUE="'.getval('bunducosts').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Description<BR><TEXTAREA NAME="desc" COLS="64" ROWS="8" STYLE="width:100%; height:120px;">'.htmlspecialchars( getval('desc') ).'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD ALIGN="center" COLSPAN="2" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Details<BR><TEXTAREA NAME="details" COLS="64" ROWS="14" STYLE="width:100%; height:240px;">'.htmlspecialchars( getval('details') ).'</TEXTAREA></TD></TR>'."\n";
	if(getval('id') != '*new*'){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Order form URL</TD><TD STYLE="font-family:Arial; font-size:10pt;"><A HREF="https://www.bundubashers.com/reserve_private.php?t='.getval('id').'" TARGET="_blank">https://www.bundubashers.com/reserve_private.php?t='.getval('id').'</A></TD></TR>'."\n"; }

	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {
//!TOUR INDEX PAGE ****************************************//


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked tours?");
	return r;
}

//--></SCRIPT><?


//GET TOURS
$tours = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS tours_prv.*, vendors.`name` AS `vendor_name` FROM `tours_prv` LEFT JOIN `vendors` ON tours_prv.`vendor` = vendors.`id` WHERE 1 = 1';
	$query .= ' ORDER BY '.$_SESSION['prvtours']['sortby'].' '.$_SESSION['prvtours']['sortdir'].', tours_prv.`title` ASC';
	$query .= ' LIMIT '.(($_SESSION['prvtours']['p']-1)*$_SESSION['prvtours']['limit']).','.$_SESSION['prvtours']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['prvtours']['limit']);
if($numpages > 0 && $_SESSION['prvtours']['p'] > $numpages): $_SESSION['prvtours']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";

	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['prvtours']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['prvtours']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['prvtours']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['prvtours']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Go" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


echo '<FORM METHOD="post" NAME="listing" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['prvtours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['prvtours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['prvtours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['prvtours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['prvtours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Vendor</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;">Edit</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;" TITLE="View the order form">Order URL</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($tours as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">'.$row['title'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:9pt; white-space:nowrap;">'.$row['vendor_name'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '<TD ALIGN="center"><A HREF="https://www.bundubashers.com/reserve_private.php?t='.$row['id'].'" TARGET="_blank"><IMG SRC="img/viewico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['prvtours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['prvtours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['prvtours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['prvtours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['prvtours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="Create a new private tour" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>