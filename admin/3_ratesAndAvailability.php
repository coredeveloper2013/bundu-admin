<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com
$working = 0;
$pageid = "3_ratesAndAvailability";
require("validate.php");
require("header.php");
//echo '<PRE>'; print_r($_REQUEST); echo '</PRE>';

$conv_types = array(
    'a' => 'Activity/Tour option',
    'r' => 'Bundu route',
    't' => 'Bundu tour',
    'l' => 'Lodging',
    's' => 'Shuttle',
    'p' => 'Tour transport',
    'o' => 'Other'
);


//?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="css/mail.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
      integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.css"
      integrity="sha256-HeLyn2WjS2tIY/dM8LuVJ6kxLsvrkxHQjWOstG4euN8=" crossorigin="anonymous"/>

<style>
    /*.custom-table {*/
    /*    width: 70% !important;*/
    /*}*/
</style>
<div class="container-fluid" id="vue_wraper" style="font-size: 12px">
    <div class="row">
        <div class="col-md-12 row pt-4 pb-2" style="border-bottom: 1px solid black">
            <div class="col-sm-3">
                <h3>
                    <input class="p-2 text-center form-control check_in" style="width: 100%;" v-on:change="jumpDate()">
                </h3>
            </div>
            <div style="padding-top: 1px;cursor: pointer" v-on:click="dateShow('left')" v-if="back > 0">
                <i class="material-icons" style="font-size: 40px;border-radius: 5px;color: #000000">
                    chevron_left</i>
            </div>
            <div style="padding-top: 1px;cursor: pointer" v-else="">
                <i class="material-icons"
                   style="font-size: 40px;background-color: #fffffc;border-radius: 5px;color: #9c9c9c">
                    chevron_left</i>
            </div>
            <div style="padding-top: 1px;padding-left: 2px;cursor: pointer" v-on:click="dateShow('right')">
                <i class="material-icons"
                   style="font-size: 40px;background-color: #fffff8;border-radius: 5px;color: #010009">
                    chevron_right</i>
            </div>
        </div>
        <div class="col-md-12 row pt-3 div-pad" id="date-pic">
            <div class="col-md-2 div-pad">

            </div>
            <div class="col-md-10 row text-center div-pad ">
                <div class="date-section" v-for="(item , index) in Dates">{{item.month}} <br> {{item.date}} <br>{{item.day}}
                </div>
            </div>
        </div>
    </div>
    <div class="overFlow">
        <div class="col-md-12 text-primary" id="fetchingData" style="text-align: center;padding-top: 100px">
            <div class="spin">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <div v-for="(item, index) in resultData">

            <div>
                <div class="ml-3">
                    <div class="co-12 row pt-3 pb-3 allClick" data-toggle="collapse" data-parent="#accordion"
                         :href="'#collapse'+index"
                         style="border-bottom: 1px solid black;cursor: pointer;background-color: #d3d6d6">
                        <div>
                            <h6 style="padding: 0px">
                                <i class="material-icons">arrow_drop_down</i> {{item.name}} {{item.max_min}}
                            </h6>
                        </div>
                    </div>
                    <div class="row pr-2 collapse" :id="'collapse'+index">
                        <div class="col-md-12 row div-pad">
                            <div class="col-md-2  div-pad text-center pt-3" id="data-pd">
                                Availability :
                            </div>
                            <div class="col-md-10 row text-center div-pad " id="data-pd"
                                 style="padding-right: 0px!important;">
                                <div v-for="(book , index) in item.data" :class="[book.unit > 0 ? 'avail' : 'booked']">
                                    <i v-if="book.unit > 0" class="material-icons text-white pt-2">check</i>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 row div-pad">
                            <div class="col-md-2 div-pad text-center pt-3" id="data-pd">
                                Rates (USD) :
                            </div>

                            <div class="col-md-10 row text-center div-pad" id="data-pd"
                                 style="padding-right: 0px!important;">
                                <div class="inventory" v-for="(rate , index) in item.data">
                                    <input type="number" v-model="rate.price"
                                           v-on:change="updatePrice(rate.price, item.id, rate.date)"
                                           class="input-text" min="0">
                                </div>
                            </div>


                        </div>
                        <div class="col-md-12 row div-pad">
                            <div class="col-md-2 div-pad text-center pt-3" id="data-pd">
                                Inventory :
                            </div>
                            <div class="col-md-10 row text-center div-pad" id="data-pd"
                                 style="padding-right: 0px!important;">

                                <div class="inventory" v-for="(avail , index1) in item.data" >
                                    <input type="number"  id="inventory" :value="item.available-avail.unit" min="0"

                                           v-on:change="updateInventory(item.available, avail.unit, item.id,avail.date,index,index1, $event)"
                                           class="input-text">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 row div-pad">
                            <div class="col-md-2 div-pad text-center pt-3" id="data-pd">
                                Booked :
                            </div>
                            <div class="col-md-10 row text-center div-pad" id="data-pd"
                                 style="padding-right: 0px!important;">
                                <div class="inventory" v-for="(book , index1) in item.data">

                                    <input type="number" v-model="book.unit"
                                           v-on:change="updateBooked(item.available, book.unit, item.id,book.date,index,index1)"
                                           class="input-text" min="0">

                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 row div-pad">
                            <div class="col-md-2 div-pad text-center pt-3" id="data-pd">
                                Minimum Nights :
                            </div>
                            <div class="col-md-10 row text-center div-pad" id="data-pd"
                                 style="padding-right: 0px!important;">
                                <div class="inventory" v-for="(min , index) in item.data">
                                    <input type="number" v-model="min.min_nights" min="1"
                                           v-on:change="updateMinNights(min.min_nights , item.id,min.date)"
                                           class="input-text">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 row div-pad ml-0">
                            <div class="col-md-2 div-pad text-center pt-3" id="data-pd">
                                Maximum Nights :
                            </div>
                            <div class="col-md-10 row text-center div-pad" id="data-pd"
                                 style="padding-right: 0px!important;">
                                <div class="inventory" v-for="(max , index) in item.data">
                                    <input type="number" v-model="max.max_nights" min="1"
                                           v-on:change="updateMaxNights(max.max_nights , item.id,max.date)"
                                           class="input-text ">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="js/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.1/flatpickr.js"
        integrity="sha256-jE3bMDv9Qf7g8/6ZRVKueSBUU4cyPHdXXP4a6t+ztdQ=" crossorigin="anonymous"></script>


<script language="javascript">


    $(function () {
        new Vue({
            el: '#vue_wraper',
            data: {
                date: null,
                lodgeData: [],
                startDate: null,
                endDate: null,
                Dates: [],
                url: 'https://localhost/bundu-admin',
                back: 0,
                resultData: [],
                error: null,
                update_value: null,
            },
            methods: {
                dateShow(type) {
                    this.lodgeData = [];
                    this.Dates = [];
                    var _this = this;
                    if (type === 'left') {
                        if (_this.back != 0) {
                            _this.back--;
                        }
                        _this.endDate.setDate(this.endDate.getDate() - 24);
                        _this.getDatesNow(this.endDate);
                        _this.getSearchResult(this.endDate, 12);

                    } else if (type === 'right') {

                        _this.back++;
                        _this.getDatesNow(this.endDate);
                        _this.getSearchResult(this.endDate, 12);
                    }
                },
                today() {
                    let dateObj = new Date();
                    let date = dateObj.getDate();
                    let month = dateObj.getMonth();
                    let year = dateObj.getFullYear();
                    month = month + 1;
                    month = month < 10 ? '0' + month : month;
                    return year + '-' + month + '-' + date + ' 00:00:00';
                },
                renderDatePick(start) {
                    let THIS = this;
                    var min = new Date(THIS.today());
                    $(".check_in").flatpickr({
                        allowInput: true,
                        defaultDate: new Date(start),
                        minDate: min,
                        altInput: true,
                        altFormat: 'd F, Y'
                    });
                },
                getDatesNow(start) {
                    this.renderDatePick(start);
                    let Months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    let Days = ['Sun', "Mon", 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                    for (let i = 0; i < 12; i++) {
                        let dateObj = new Date(start);
                        let date = dateObj.getDate();
                        let day = dateObj.getDay();
                        let month = dateObj.getMonth();

                        this.Dates.push({
                            date: date,
                            day: Days[day],
                            month: Months[month],
                        }),
                            start.setDate(start.getDate() + 1);
                    }

                    this.endDate = start;
                    let dateObj1 = new Date();
                    let date = dateObj1.getDate();
                    let year = dateObj1.getFullYear();
                    let month = dateObj1.getMonth();
                    this.date = date + ' ' + Months[month] + ' ' + year;

                },

                updateInventory(available, unit, id,date,index,index1, e) {
                    let THIS = this;
                    let input = $(e.target);
                    let val = input.val();
                    let oldVal = available-unit;
                    let booked = unit;
                    if (val <= available){
                        if(val > oldVal){
                            booked = parseInt(booked) - parseInt(val - oldVal);
                            this.resultData[index].data[index1].unit = booked;
                        } else {
                            booked = parseInt(booked) + parseInt(oldVal - val);
                            this.resultData[index].data[index1].unit = booked;
                        }
                        this.updateBooked(available, booked, id, date, index, index1)
                    }else {
                        input.val(available-unit);
                        alert("You can't add available lodge more than total inventory")
                    }

                },
                updatePrice(price, id, date) {
                    let THIS = this;
                    if (price >= 0) {
                        $.ajax({
                            type: 'POST',
                            data: {
                                price: price,
                                lodge_id: id,
                                date: date
                            },
                            url: THIS.url + '/api/RatesAndAvailabilityUpdate.php',
                            success: function (response) {
                                response = JSON.parse(response);
                                // console.log(response)
                            }
                        });
                    } else {
                        alert('Invalid value !!')
                    }

                },
                updateMaxNights(max_nights, id, date) {
                    let THIS = this;
                    if (max_nights > 0) {
                        $.ajax({
                            type: 'POST',
                            data: {
                                max_nights: max_nights,
                                lodge_id: id,
                                date: date
                            },
                            url: THIS.url + '/api/RatesAndAvailabilityUpdate.php',
                            success: function (response) {
                                response = JSON.parse(response);
                                // console.log(response)
                            }
                        });
                    } else {
                        alert('Invalid value !!')
                    }


                },
                updateMinNights(min_nights, id, date) {
                    let THIS = this;
                    if (min_nights > 0) {
                        $.ajax({
                            type: 'POST',
                            data: {
                                min_nights: min_nights,
                                lodge_id: id,
                                date: date
                            },
                            url: THIS.url + '/api/RatesAndAvailabilityUpdate.php',
                            success: function (response) {
                                response = JSON.parse(response);
                                // console.log(response)
                            }
                        });
                    } else {
                        alert('Invalid value !!')
                    }

                },
                updateBooked(available, booked, id, date, index, index1) {
                    var THIS = this;
                    if (available < booked) {
                        alert("You can't  booked more then " + available + ' lodge !')
                        this.resultData[index].data[index1].unit = available;

                    } else {
                        // this.resultData[index].data[index1].unit = this.resultData[index].data[index1].available
                        let THIS = this;
                        $.ajax({
                            type: 'POST',
                            data: {
                                booked: booked,
                                lodge_id: id,
                                date: date
                            },
                            url: THIS.url + '/api/RatesAndAvailabilityUpdate.php',
                            success: function (response) {
                                response = JSON.parse(response);
                                if (response.status == 5000) {
                                    alert(response.error)
                                    THIS.resultData[index].data[index1].unit = response.booked;
                                }
                                // console.log(response)
                            }
                        });
                    }
                },
                getSearchResult(start, nights) {
                    $("#fetchingData").show();
                    // console.log(start);
                    this.resultData = [];
                    var day = start.getDate();
                    var monthIndex = start.getMonth();
                    var year = start.getFullYear();
                    var month = monthIndex + 1;
                    month = month < 10 ? '0' + month : month
                    // console.log(year+'-'+0+(monthIndex+1)+'-'+0+day);
                    start = year + '-' + month + '-' + day;
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        url: THIS.url + '/api/RatesAndAvailability.php',
                        data: {
                            start_date: start,
                            nights: nights
                        },
                        success: function (res) {
                            var res = JSON.parse(res);
                            setTimeout(function () {
                                $("#fetchingData").hide();
                                THIS.resultData = res.data;
                            }, 1000)
                            // console.log(THIS.resultData);

                        }
                    });
                },
                jumpDate() {
                    this.lodgeData = [];
                    this.Dates = [];
                    let date = $('.check_in').val();
                    date = new Date(date);
                    this.getDatesNow(date);
                    this.getSearchResult(date, 12);
                }

            },
            mounted() {

                // this.getDatesNow(new Date('2014-11-05'));
                this.getDatesNow(new Date());
                this.getSearchResult(new Date(), 12);


            }
        });
    })
</script>


</BODY>

w Date(), 12);


            }
        });
    })
</script>


</BODY>

