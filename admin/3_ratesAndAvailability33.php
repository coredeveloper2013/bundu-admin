<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com
$working = 0;
$pageid = "3_lodging_rates";
require("validate.php");
require("header.php");
//echo '<PRE>'; print_r($_REQUEST); echo '</PRE>';

$conv_types = array(
    'a' => 'Activity/Tour option',
    'r' => 'Bundu route',
    't' => 'Bundu tour',
    'l' => 'Lodging',
    's' => 'Shuttle',
    'p' => 'Tour transport',
    'o' => 'Other'
);

?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="css/mail.css" rel="stylesheet" type="text/css">
<style>
    .custom-table {
        width: 70% !important;
    }
</style>
<div class="container" id="vue_wraper" style="font-size: 12px">
    <div class="row">
        <div class="col-md-12 row pt-3 div-pad" id="date-pic">
            <div class="col-md-3 div-pad ">
                <h5 class="title">
                    <b>Rates & Availability Config</b>
                </h5>
            </div>
            <div class="col-md-9 row text-center div-pad ">
                <div class="date-section">
                    Inventory
                </div>
<!--                <div class="date-section">-->
<!--                    Available-->
<!--                </div>-->
                <div class="date-section">
                    Max-Guest
                </div>
                <div class="date-section">
                    Rates (USD)
                </div>
                <div class="date-section">
                    Max-Night
                </div>
                <div class="date-section">
                    Min-Night
                </div>
                <div class="date-section">
                    Booked
                </div>

            </div>
        </div>
    </div>
    <div class="overFlow">
        <div v-for="(item, index) in resultData">
            <div>
                <div class="ml-3">
                    <div class="co-12 row pt-3 pb-3 allClick" data-toggle="collapse" data-parent="#accordion" :href="'#collapse'+index"
                         style="border-bottom: 1px solid black;cursor: pointer;background-color: #d3d6d6">
                        <div>
                            <h6 style="padding: 0 10px">
                                <i class="material-icons">arrow_drop_down</i> {{item.name}}
                            </h6>
                        </div>
                    </div>
                    <div class="row pr-2 collapse" :id="'collapse'+index">

                        <div class="col-md-12 row div-pad" v-for="(item1 , index1) in item.pricing">
                            <div class="col-md-3 div-pad text-center pt-3" id="data-pd">
                                {{item1.start_date_formatted}} - {{item1.end_date_formatted}}
                            </div>
                            <div class="col-md-9 row text-center div-pad" id="data-pd" style="padding-right: 0px!important;">
                                <div class="inventory">
                                    <input type="number" v-model="item.available" v-on:change="updateInventory(item.available, item.id)" class="input-text">
                                </div>
<!--                                <div class="inventory">-->
<!--                                    <input type="number" v-model="item.available" class="input-text" readonly>-->
<!--                                </div>-->
                                <div class="inventory">
                                    <input type="number" v-model="item.capacity" v-on:change="updateCapacity(item.capacity, item.id)" class="input-text">
                                </div>
                                <div class="inventory">
                                    <input type="number" v-model="item1.price" v-on:change="updatePrice(item1.price, item1.id)" class="input-text">
                                </div>
                                <div class="inventory">
                                    <input type="number" v-model="item1.max_nights" v-on:change="updateMaxNights(item1.max_nights, item1.id)" class="input-text">
                                </div>
                                <div class="inventory">
                                    <input type="number" v-model="item1.min_nights" v-on:change="updateMinNights(item1.min_nights, item1.id)" class="input-text">
                                </div>
                                <div class="inventory">
                                        <input type="number" v-model="item1.booked" v-on:change="updateBooked(item1.booked, item1.id,index1, index)" class="input-text" min="0" max="1" step="1">
                                </div>
                                <div v-bind:class="[(item1.booked != 0) ? 'avail' : 'booked']">
                                    <i v-if="item1.booked != 0" class="material-icons text-white pt-2">check</i>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 row div-pad" v-if="item.pricing.length <=0 ">
                            <div class="col-md-12 div-pad text-center pt-3" id="data-pd">
                                <h6 class="text-danger">There have no rating formation available for this lodging!</h6>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>



    </div>


</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="js/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>





<script language="javascript">

    $(function () {
        new Vue({
            el: '#vue_wraper',
            data: {
                url : 'http://localhost/bundu-admin',
                startDate: null,
                endDate: null,


                resultData : [],
                error: null,
                update_value: null,
            },
            methods: {
                getSearchResult() {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        url: THIS.url+'/api/RatesAndAvailabilityNew.php',
                        success: function(res){
                            var res= JSON.parse(res);
                            THIS.resultData=res.data;
                            console.log(THIS.resultData);

                        }
                    });
                },
                updateInventory(available, id) {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            available: available,
                            lodge_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                },
                updateCapacity(capacity, id) {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            capacity: capacity,
                            lodge_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                },
                updatePrice(price, id) {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            price: price,
                            lodging_pricing_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                },
                updateMaxNights(max_nights, id) {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            max_nights: max_nights,
                            lodging_pricing_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                },
                updateMinNights(min_nights, id) {
                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            min_nights: min_nights,
                            lodging_pricing_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                },
                updateBooked(booked, id,index1,index) {
                    if (booked > 1)
                    {
                        booked = 1;
                        this.resultData[index].pricing[index1].booked = 1;
                    }
                    if (booked < 0){
                        booked = 0;
                        this.resultData[index].pricing[index1].booked = 0;
                    }

                    let THIS = this;
                    $.ajax({
                        type: 'POST',
                        data: {
                            booked: booked,
                            lodging_pricing_id: id
                        },
                        url: THIS.url+'/api/RatesAndAvailabilityUpdate.php',
                        success: function(response){
                            response = JSON.parse(response);
                            if (response.status === 2000) {
                                // alert(response.success);
                            } else {
                                // alert(response.error);
                            }
                        }
                    });
                }
            },
            mounted() {
                this.getSearchResult();
            }
        });
    })
</script>


</BODY>

