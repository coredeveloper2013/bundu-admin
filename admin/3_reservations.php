<?php //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_reservations";
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){ $pageid .= '_edit'; }
require("validate.php");
require("header.php");
//require_once("../common.inc.php");

if(!isset($_SESSION['reservations']['p'])): $_SESSION['reservations']['p'] = 1; endif;
if(!isset($_SESSION['reservations']['sortby'])): $_SESSION['reservations']['sortby'] = "reservations.`id`"; endif;
	$sortby = array('reservations.`id`'=>'Conf#','reservations.`date_booked`'=>'Booked','`name`'=>'Name');
if(!isset($_SESSION['reservations']['sortdir'])): $_SESSION['reservations']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['reservations']['limit'])): $_SESSION['reservations']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['reservations']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['reservations']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['reservations']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['reservations']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['reservations']['p'] = '1';
}

//GATHER NEEDED DATA
$restypes = array(
	'a' => 'Activity/Tour option',
	'r' => 'Bundu route',
	't' => 'Bundu tour',
	'l' => 'Lodging',
	's' => 'Shuttle',
	'p' => 'Tour transport',
	'h' => 'HOHO Pass',
	'm' => 'Mini Route Pass',
	'o' => 'All other'
	);

$preftimes = array("Morning","Mid Day","Afternoon","Evening");

$lunchtypes = array("Turkey","Beef","Ham","Vegetarian");

//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}

//GET ACTIVITIES
$activities = array();
$query = 'SELECT * FROM `activities` ORDER BY `disable` ASC, `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$activities['a'.$row['id']] = $row;
	}

//GET TOURS
$tours = array();
	$tours['t1'] = array('id'=>1,'title'=>'Private tour','subhead'=>'Private tour');
$query = 'SELECT `id`,`title` FROM `tours` ORDER BY `id` DESC, `title` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = $row['id'].' : '.str_replace("'",'',$row['title']);
	if(strlen($row['subhead']) > 90){ $row['subhead'] = substr($row['subhead'],0,90).'...'; }
	$tours['t'.$row['id']] = $row;
	}

//GET ROUTES
$routes = array();
$query = 'SELECT * FROM `routes` ORDER BY `name` ASC, `dep_loc` ASC, `arr_loc` ASC, `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = $row['name'].'&nbsp;&nbsp;-&nbsp;&nbsp;Depart '.date("g:ia",($today+$row['dep_time'])).' '.$locations['l'.$row['dep_loc']]['name'].' - Arrive '.date("g:ia",($today+$row['arr_time'])).' '.$locations['l'.$row['arr_loc']]['name'];
	if(strlen($row['subhead']) > 100){ $row['subhead'] = substr($row['subhead'],0,100).'...'; }
	$routes['r'.$row['id']] = $row;
	}

//GET LODGING
$lodging = array();
$query = 'SELECT * FROM `lodging` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$lodging['l'.$row['id']] = $row;
	}

//GET MARKETS
$markets = array();
$query = 'SELECT * FROM `markets` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$markets['m'.$row['id']] = $row;
	}

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

//GET SHUTTLE TRANS TYPES
$transtypes = array();
$query = 'SELECT * FROM `shuttle_transtypes` ORDER BY `disable` ASC, `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$transtypes['t'.$row['id']] = $row;
	}

//HOHO TICKETS
$hoho = array();
$query = 'SELECT `id`,`title` AS `name`,`price` FROM `bundubus_hopon` ORDER BY `price` ASC, `title` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$hoho['h'.$row['id']] = $row;
	}

//MINI ROUTES
$miniroutes = array();
$query = 'SELECT `id`,`title` AS `name` FROM `miniroutes` ORDER BY `title` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$miniroutes['m'.$row['id']] = $row;
	}

//GET AGENTS
$agents = array();
$query = 'SELECT `id`,`name` FROM `agents` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$agents['a'.$row['id']] = $row;
	}

//GET SUBAGENTS
$subagents = array();
$query = 'SELECT * FROM subagents ORDER BY name ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$disp = $agents['a'.$row['id_agent']]['name'].' : '.$row['name'];
		$subagents[$row['id']] = $disp;
	}



//echo '<PRE>'; print_r($_POST); echo '</PRE>';

/*
$reslog = '';
if(isset($_POST) && count($_POST) > 0){
	//$reslog .= "\n".'//!'.date("M j, Y g:ia").' ---------------------------------------------------------------------------------------------------------------------------------------------'."\n\n";
	//$reslog .= print_r($_POST,true)."\n\n";
}
*/

//!FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$date_booked = $_POST['date_booked_year'].'-'.$_POST['date_booked_month'].'-'.$_POST['date_booked_day'];

	$resObj = new reservation($_POST['edit']);

	$isNew = false;
	if($resObj->id == "") {
		$isNew = true;
	}

	$resObj->setName($_POST['name']);
	$resObj->setPhoneHomeBusiness($_POST['phone_homebus']);
	$resObj->setPhoneCell($_POST['phone_cell']);
	$resObj->setPhoneCellCountry($_POST['cell_country']);
	$resObj->setEmail($_POST['email']);
	$resObj->setEmailInstructions($_POST['email_inst']);
	$resObj->setAmount($_POST['amount']);
	$resObj->setCCname($_POST['cc_name']);
	$resObj->setCCnumber($_POST['cc_num']);
	$resObj->setCCexpDate($_POST['cc_expdate']);
	$resObj->setCCsecurityCode($_POST['cc_scode']);
	$resObj->setCCzip($_POST['cc_zip']);
	$resObj->setPayMethod($_POST['pay_method']);
	$resObj->setAlternateName($_POST['alt_name']);
	$resObj->setPNPorderID($_POST['pnp_orderid']);
	$resObj->setConfirmationDetails($_POST['conf_details']);
	$resObj->setIDagent($_POST['agent']);
	$resObj->setIDsubAgent($_POST['id_subagent']);
	$resObj->setAgentConfirmation($_POST['agent_conf']);
	$resObj->setReferral($_POST['referral']);
	$resObj->setNotes($_POST['notes']);
	$resObj->setBooker($_POST['booker']);
	$resObj->setDateTimeBooked($date_booked);
	//$resObj->setComments($_POST['comments']);
	$resObj->save();

	if($GLOBALS['mysql_error'] != "") {
		$errormsg[] = $GLOBALS['mysql_error'];
	} else {
		$successmsg[] = 'Saved'.($isNew?' new':'').' reservation <B>'.$resObj->id.'</B> for '.stripslashes($_POST['name']).'.';
	}

	$_REQUEST['edit'] = $resObj->id;


	//START EMAIL CONFIRMATION MESSAGE
	$numsht = 0;
	$email_conf = '<HTML><BODY>';
	$email_conf .= '<FONT FACE="Arial" SIZE="2">Did you know that if you book your lodging through our rental division, <A HREF="http://www.parkcityrental.com">Park City Rentals</A>, you will get free airport transfers?<BR><BR></FONT>'."\n\n";
	$email_conf .= '<FONT FACE="Arial" SIZE="2" COLOR="#FF0000"><B>Please call us, toll free, at 1 800 7AIRPORT (1 800 724 7767), or locally at 435 658 2227, as soon as you get in to the Salt Lake City airport, before picking up your luggage, and you will be advised where to find your vehicle.</B><BR><BR></FONT>'."\n\n";
	$email_conf .= '<FONT FACE="Arial" SIZE="2">'."\n".'Thank you for your reservation which we confirm as follows:<BR><BR>'."\n\nReservation number: ".$_REQUEST['edit']."<BR>\nName: ".$_POST['name']."<BR><BR>\n\n";
	if(trim($_POST['email_inst']) != ""){ $email_conf .= nl2br($_POST['email_inst'])."<BR><BR>\n\n"; }

	//SUBRESERVATIONS -------------------------
	$numupdated = 0;
	foreach($_POST['assoc_id'] as $key => $id){

		if($id == "*new*"){
			$id = '';
		}

		$depLoc = $_POST['assoc_dep_loc'][$key];
		$depTime = $_POST['assoc_dep_time_hour'][$key].':'.$_POST['assoc_dep_time_mins'][$key].':00';
		$arrLoc = $_POST['assoc_arr_loc'][$key];
		$arrTime = $_POST['assoc_arr_time_hour'][$key].':'.$_POST['assoc_arr_time_mins'][$key].':00';
		if($_POST['assoc_type'][$key] == 'r' && $_POST['assoc_routeid'][$key] != ""){
			$routeObj = new route($_POST['assoc_routeid'][$key]);
			$depLoc = $routeObj->getDepLocation();
			$depTime = $routeObj->getDepTime();
			$arrLoc = $routeObj->getArrLocation();
			$arrTime = $routeObj->getArrTime();
			unset($routeObj);
		}
		$dateVendorPaid = null;
			if($_POST['assoc_vendorpaid'][$key] != 0){
				$dateVendorPaid = mysqlDate($_POST['assoc_vendorpay_year'][$key].'-'.$_POST['assoc_vendorpay_month'][$key].'-'.$_POST['assoc_vendorpay_day'][$key]);
			}

		$resAssocObj = new reservation_assoc($id);
			$resAssocObj->setIDreservation($_REQUEST['edit']);
			$resAssocObj->setReference($_POST['assoc_reference'][$key]);
			$resAssocObj->setType($_POST['assoc_type'][$key]);
			$resAssocObj->setName($_POST['assoc_name'][$key]);
			$resAssocObj->setNumAdults($_POST['assoc_adults'][$key]);
			$resAssocObj->setNumSeniors($_POST['assoc_seniors'][$key]);
			$resAssocObj->setNumChildren($_POST['assoc_children'][$key]);
			$resAssocObj->setDate($_POST['assoc_date_year'][$key].'-'.$_POST['assoc_date_month'][$key].'-'.$_POST['assoc_date_day'][$key]);
			$resAssocObj->setDepLocation($depLoc);
			$resAssocObj->setDepTime($depTime);
			$resAssocObj->setArrLocation($arrLoc);
			$resAssocObj->setArrTime($arrTime);
			$resAssocObj->setPrefTime($_POST['assoc_preftime'][$key]);
			$resAssocObj->setIDactivity($_POST['assoc_activityid'][$key]);
			$resAssocObj->setIDmarket($_POST['assoc_market'][$key]);
			$resAssocObj->setIDtransType($_POST['assoc_trans_type'][$key]);
			$resAssocObj->setNotes($_POST['assoc_notes'][$key]);
			$resAssocObj->setDriverInfo($_POST['assoc_driver_info'][$key]);
			$resAssocObj->setFlagOnlyDateAvailable($_POST['assoc_onlydateavl'][$key]);
			$resAssocObj->setAlternateDates($_POST['assoc_altdates'][$key]);
			$resAssocObj->setIDlodge($_POST['assoc_lodgeid'][$key]);
			$resAssocObj->setNumNights($_POST['assoc_nights'][$key]);
			$resAssocObj->setAmount($_POST['assoc_amount'][$key]);
			$resAssocObj->setCosts($_POST['assoc_costs'][$key]);
			$resAssocObj->setIDtour($_POST['assoc_tourid'][$key]);
			$resAssocObj->setIDroute($_POST['assoc_routeid'][$key]);
			$resAssocObj->setIDbbTicket($_POST['assoc_bbticket'][$key]);
			$resAssocObj->setIDminiRoute($_POST['assoc_miniroute'][$key]);
			$resAssocObj->setUsername($_POST['assoc_username'][$key]);
			$resAssocObj->setPassword($_POST['assoc_password'][$key]);
			$resAssocObj->setNumDays($_POST['assoc_days'][$key]);
			$resAssocObj->setIDvendor($_POST['assoc_vendor'][$key]);
			$resAssocObj->setVendorConfNum($_POST['assoc_vendorconf'][$key]);
			$resAssocObj->setDateVendorPaid($dateVendorPaid);
			$resAssocObj->setVendorPayAmount($_POST['assoc_vendorpayamt'][$key]);
			if($_POST['assoc_canceled'][$key] == "1"){
				$resAssocObj->setDateTimeCanceled("now", true);
			} else {
				$resAssocObj->setDateTimeCanceled(null);
			}
			$resAssocObj->save();
			unset($resAssocObj);

		$_POST['assoc_date'][$key] = mktime(0,0,0,$_POST['assoc_date_month'][$key],$_POST['assoc_date_day'][$key],$_POST['assoc_date_year'][$key]);
		$_POST['assoc_dep_time'][$key] = mktime($_POST['assoc_dep_time_hour'][$key],$_POST['assoc_dep_time_mins'][$key],0,$_POST['assoc_date_month'][$key],$_POST['assoc_date_day'][$key],$_POST['assoc_date_year'][$key]);
		if($_POST['assoc_vendorpaid'][$key] != 0){
			$_POST['assoc_vendorpaid'][$key] = mktime(0,0,0,$_POST['assoc_vendorpay_month'][$key],$_POST['assoc_vendorpay_day'][$key],$_POST['assoc_vendorpay_year'][$key]);
		}
		$_POST['assoc_arr_time'][$key] = $_POST['assoc_date'][$key];

		if($_POST['assoc_type'][$key] == 'r' && $_POST['assoc_routeid'][$key] != ""){
			$_POST['assoc_dep_loc'][$key] = $locations['l'.$routes['r'.$_POST['assoc_routeid'][$key]]['dep_loc']]['name'];
			$_POST['assoc_dep_time'][$key] = ($_POST['assoc_date'][$key]+$routes['r'.$_POST['assoc_routeid'][$key]]['dep_time']);
			$_POST['assoc_arr_loc'][$key] = $locations['l'.$routes['r'.$_POST['assoc_routeid'][$key]]['arr_loc']]['name'];
			$_POST['assoc_arr_time'][$key] = ($_POST['assoc_date'][$key]+$routes['r'.$_POST['assoc_routeid'][$key]]['arr_time']);
			}
		//if(trim($_POST['assoc_password'][$key]) != ""){ $_POST['assoc_password'][$key] = md5($_POST['assoc_password'][$key]); }

		if($_POST['assoc_canceled'][$key] == "1"){ $_POST['assoc_canceled'][$key] = $time; }

/*
		if($_POST['assoc_id'][$key] == "*new*"){
		//INSERT NEW
		$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`preftime`,`activityid`,`market`,`trans_type`,`notes`,`driver_info`,`onlydateavl`,`altdates`,`lodgeid`,`nights`,`amount`,`costs`,`tourid`,`routeid`,`bbticket`,`miniroute`,`username`,`password`,`days`,`vendor`,`vendorconf`,`vendorpaid`,`vendorpayamt`,`canceled`)';
			$query .= ' VALUES("'.$_REQUEST['edit'].'","'.$_POST['assoc_reference'][$key].'","'.$_POST['assoc_type'][$key].'","'.$_POST['assoc_name'][$key].'","'.$_POST['assoc_adults'][$key].'","'.$_POST['assoc_seniors'][$key].'","'.$_POST['assoc_children'][$key].'","'.$_POST['assoc_date'][$key].'","'.$_POST['assoc_dep_loc'][$key].'","'.$_POST['assoc_dep_time'][$key].'","'.$_POST['assoc_arr_loc'][$key].'","'.$_POST['assoc_arr_time'][$key].'","'.$_POST['assoc_preftime'][$key].'","'.$_POST['assoc_activityid'][$key].'","'.$_POST['assoc_market'][$key].'","'.$_POST['assoc_trans_type'][$key].'","'.$_POST['assoc_notes'][$key].'","'.$_POST['assoc_driver_info'][$key].'","'.$_POST['assoc_onlydateavl'][$key].'","'.$_POST['assoc_altdates'][$key].'","'.$_POST['assoc_lodgeid'][$key].'","'.$_POST['assoc_nights'][$key].'"';
			$query .= ',"'.$_POST['assoc_amount'][$key].'","'.$_POST['assoc_costs'][$key].'","'.$_POST['assoc_tourid'][$key].'","'.$_POST['assoc_routeid'][$key].'","'.$_POST['assoc_bbticket'][$key].'","'.$_POST['assoc_miniroute'][$key].'","'.$_POST['assoc_username'][$key].'","'.$_POST['assoc_password'][$key].'","'.$_POST['assoc_days'][$key].'","'.$_POST['assoc_vendor'][$key].'","'.$_POST['assoc_vendorconf'][$key].'","'.$_POST['assoc_vendorpaid'][$key].'","'.$_POST['assoc_vendorpayamt'][$key].'","'.$_POST['assoc_canceled'][$key].'")';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""): $_POST['assoc_id'][$key] = mysql_insert_id(); $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
		} else {
		//UPDATE
		$query = 'UPDATE `reservations_assoc` SET `reference` = "'.$_POST['assoc_reference'][$key].'", `type` = "'.$_POST['assoc_type'][$key].'", `name` = "'.$_POST['assoc_name'][$key].'", `adults` = "'.$_POST['assoc_adults'][$key].'", `seniors` = "'.$_POST['assoc_seniors'][$key].'", `children` = "'.$_POST['assoc_children'][$key].'", `date` = "'.$_POST['assoc_date'][$key].'", `dep_loc` = "'.$_POST['assoc_dep_loc'][$key].'", `dep_time` = "'.$_POST['assoc_dep_time'][$key].'", `arr_loc` = "'.$_POST['assoc_arr_loc'][$key].'", `arr_time` = "'.$_POST['assoc_arr_time'][$key].'", `preftime` = "'.$_POST['assoc_preftime'][$key].'", `activityid` = "'.$_POST['assoc_activityid'][$key].'", `market` = "'.$_POST['assoc_market'][$key].'", `trans_type` = "'.$_POST['assoc_trans_type'][$key].'", `notes` = "'.$_POST['assoc_notes'][$key].'", `driver_info` = "'.$_POST['assoc_driver_info'][$key].'"';
			$query .= ', `onlydateavl` = "'.$_POST['assoc_onlydateavl'][$key].'", `altdates` = "'.$_POST['assoc_altdates'][$key].'", `lodgeid` = "'.$_POST['assoc_lodgeid'][$key].'", `nights` = "'.$_POST['assoc_nights'][$key].'", `amount` = "'.$_POST['assoc_amount'][$key].'", `costs` = "'.$_POST['assoc_costs'][$key].'", `tourid` = "'.$_POST['assoc_tourid'][$key].'", `routeid` = "'.$_POST['assoc_routeid'][$key].'", `bbticket` = "'.$_POST['assoc_bbticket'][$key].'", `miniroute` = "'.$_POST['assoc_miniroute'][$key].'", `username` = "'.$_POST['assoc_username'][$key].'", `password` = "'.$_POST['assoc_password'][$key].'", `days` = "'.$_POST['assoc_days'][$key].'", `vendor` = "'.$_POST['assoc_vendor'][$key].'", `vendorconf` = "'.$_POST['assoc_vendorconf'][$key].'", `vendorpaid` = "'.$_POST['assoc_vendorpaid'][$key].'", `vendorpayamt` = "'.$_POST['assoc_vendorpayamt'][$key].'", `canceled` = "'.$_POST['assoc_canceled'][$key].'"';
			$query .= ' WHERE `id` = "'.$_POST['assoc_id'][$key].'" LIMIT 1';
			@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror == ""): $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
		}
*/

		//Update tour confirmations
		if($_POST['assoc_tourid'][$key] > 0) {
			$confirm = @$_POST['assoc_confirm'][$key];
			if($confirm == "") {
				$confirm = 0;
			}
			//If confirmed, add the date.  If not, only update if the date already exists.
			$tourDateObj = new tour_date(array(
				'id_tour' => $_POST['assoc_tourid'][$key],
				'date' => $_POST['assoc_date'][$key]
				));
			if($tourDateObj->id > 0 || $confirm == 1) {
				$tourDateObj->setIDtour($_POST['assoc_tourid'][$key]);
				$tourDateObj->setDate($_POST['assoc_date'][$key]);
				$tourDateObj->setConfirmed($confirm);
				$tourDateObj->save();
			}
			unset($tourDateObj);
		}

		//$reslog .= $query."\n";
		//$reslog .= $thiserror."\n";

		//GUESTS -------------------------
		@mysqlQuery('DELETE FROM `reservations_guests` WHERE `associd` = "'.$_POST['assoc_id'][$key].'"');
		if(isset($_POST['assoc_guests'.$key.'_name'])){
		foreach($_POST['assoc_guests'.$key.'_name'] as $guestid => $guest){ if($guest != ""){
			$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$_POST['assoc_id'][$key].'","'.$_POST['assoc_guests'.$key.'_name'][$guestid].'","'.$_POST['assoc_guests'.$key.'_weight'][$guestid].'","'.$_POST['assoc_guests'.$key.'_lunch'][$guestid].'")';
			@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror != ""): array_push($errormsg,$thiserror); endif;
			//$reslog .= $query."\n";
			//$reslog .= $thiserror."\n";
			}} //End foreach
			} //End if statement

		//TOUR LODGING
		@mysqlQuery('DELETE FROM `reservations_lodging` WHERE `associd` = "'.$_POST['assoc_id'][$key].'"');
		if(isset($_POST['assoc_room'.$key.'_numguests'])){
		foreach($_POST['assoc_room'.$key.'_numguests'] as $rm => $room){ if($room != ""){
			$query = 'INSERT INTO `reservations_lodging`(`associd`,`room`,`numguests`,`adjust`) VALUES("'.$_POST['assoc_id'][$key].'","'.($rm+1).'","'.$_POST['assoc_room'.$key.'_numguests'][$rm].'","'.$_POST['assoc_room'.$key.'_adjust'][$rm].'")';
			@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror != ""): array_push($errormsg,$thiserror); endif;
			//$reslog .= $query."\n";
			//$reslog .= $thiserror."\n";
			}} //End foreach
			} //End if statement

		if($_POST['assoc_canceled'][$key] == 0 && $_POST['assoc_type'][$key] == 's'){
			$numsht++;
			if(trim($_POST['assoc_reference'][$key]) != ""){ $email_conf .= $_POST['assoc_reference'][$key]."<BR>\n"; }
			$email_conf .= date("D. M j, Y",$_POST['assoc_date'][$key])."<BR>\n";
			if(trim($_POST['assoc_notes'][$key]) != ""){ $email_conf .= "Flight: ".$_POST['assoc_notes'][$key]."<BR>\n"; }
			$email_conf .= "Pick Up Time: ".date("g:ia",$_POST['assoc_dep_time'][$key])."<BR>\n";		
			if(trim($_POST['assoc_dep_loc'][$key]) != ""){ $email_conf .= "Pick up: ".$_POST['assoc_dep_loc'][$key]."<BR>\n"; }
			if(trim($_POST['assoc_arr_loc'][$key]) != ""){ $email_conf .= "Drop off: ".$_POST['assoc_arr_loc'][$key]."<BR>\n"; }
			$email_conf .= "Name: ".$_POST['assoc_name'][$key]."<BR>\n";
			$email_conf .= "Type of Transportation: ".$transtypes['t'.$_POST['assoc_trans_type'][$key]]['name']."<BR>\nNumber of guests: ".($_POST['assoc_adults'][$key]+$_POST['assoc_seniors'][$key]+$_POST['assoc_children'][$key])."<BR>\n";
			$email_conf .= "<BR>\n";
			//if(trim($_REQUEST['flight_info'.$i]) != ""){ $email_conf .= "Flight Information: ".trim($_REQUEST['flight_info'.$i])."<BR>\n"; } $email_conf .= "<BR>\n";
			}

		} //End subreservation foreach
		if($numupdated > 0): array_push($successmsg,$numupdated.' subreservations were added/updated.'); endif;

	//SET EMAIL CONFIRMATION AS SENT
	if(isset($_POST['confsent']) && $_POST['confsent'] == "y"){
		@mysqlQuery('UPDATE `reservations` SET `email_conf` = "'.$time.'" WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1');
	}

	//FINISH/SEND EMAIL CONFIRMATION MESSAGE
	$email_conf .= "Credit card: ".substr($_POST['cc_num'],0,2).'**'.substr($_POST['cc_num'],-2)."<BR>\nCost: $".number_format($_POST['amount'], 2, '.', '').", excluding optional gratuity.<BR>\nMethod of payment: Credit card.  Kindly ensure that the credit card you used will be present at the time of transportation as we will need to take an imprint of it.<BR><BR>\n\nCancellation policy:  Attempted cancellations made within 24 hours of the travel time will result in the full amount being charged to your credit card. We can be reached toll free at 1 800 7AIRPORT (1 800 724 7767), or locally at 435 658 2227.<BR><BR>\n\nPlease access the following link for details on how to locate us at the airport: <A HREF=\"http://www.parkcitycabs.com/finding_us_at_airport.htm\">http://www.parkcitycabs.com/finding_us_at_airport.htm</A><BR><BR>\n\n<B>Kindly hit the reply button, and then send, to confirm receipt of this email.</B><BR><BR>\n\nWe look forward to being of service.\n\n</BODY></HTML>";
	if(isset($_POST['sendconf']) && $_POST['sendconf'] == "y" && $numsht > 0){
		$email_to = $_POST['name'].' <'.$_POST['email'].'>';
		$email_from = 'Utah Transportation Group <utahtransport@gmail.com>';
		$email_subject = 'Reservation Confirmation #'.$_REQUEST['edit'];

		$emailObj = new email();
		$emailObj->setIDreservation($_REQUEST['edit']);
		$emailObj->setTo($email_to);
		$emailObj->setFrom($email_from);
		$emailObj->setSubject($email_subject);
		$emailObj->setBodyHTML($email_conf);
		$result = $emailObj->send();

		if($result) {
			array_push($successmsg,'Sent email confirmation for '.$numsht.' shuttle subreservations to '.$_POST['email']);
			@mysqlQuery('UPDATE `reservations` SET `email_conf` = "'.$time.'" WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1');
		} else {
			array_push($errormsg,'Unable to send email confirmation.');
		}
	} elseif(isset($_POST['sendconf']) && $_POST['sendconf'] == "y" && $numsht == 0) {
			array_push($errormsg,'There are no shuttle subreservations, and so no email was generated.');
	}

	//RUN CREDIT CARD -------------------------
	if(isset($_POST['runcard']) && $_POST['runcard'] == "y") {
		$transactionObj = new pnp_transaction();
		$transactionObj->setIDreservation($resObj->id);
		$transactionObj->setCCname($_POST['cc_name']);
		$transactionObj->setCCnumber($_POST['cc_num']);
		$transactionObj->setCCexpDate($_POST['cc_expdate']);
		$transactionObj->setCCsecurityCode($_POST['cc_scode']);
		$transactionObj->setPNPorderID($_POST['pnp_orderid']);
		$transactionObj->setEmail($_POST['email']);
		$transactionObj->setAuthType($_POST['authtype']);
		$transactionObj->setAmount($_POST['cardamount']);
                
                if ($_POST['assoc_type'][$key] == 'l') {
                    $transactionObj->setPublisherName('yellowston2');
                } else {
                    $transactionObj->setPublisherName('utahtransp');
                }
                
		$pnp_transaction_array = $transactionObj->runTransaction();

		$conf_details = $_POST['conf_details'];
		if($conf_details == "") {
			$conf_details = $resObj->getConfirmationDetails();
		}

		if($pnp_transaction_array['FinalStatus'] == "success" || $pnp_transaction_array['FinalStatus'] == "pending") {
			$successmsg[] = $_REQUEST['edit'].' Credit Card: "Transaction was successful."';
			if($conf_details != "") {
				$conf_details .= "\n\n";
			}
			$conf_details .= $pnp_transaction_array['conf_string'];

			$resObj->clearCCnum();
			$resObj->setConfirmationDetails($conf_details);
			$resObj->setPNPorderID(@$pnp_transaction_array['orderID']);
			$resObj->setDateTimeCardRun("now");
			$resObj->save();

		} elseif($pnp_transaction_array['FinalStatus'] == "badcard") {
			$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
			array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to decline from processor."<BR>'.$thiserror);

		} elseif($pnp_transaction_array['FinalStatus'] == "fraud") {
			$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
			array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to failure of FraudTrack2 filter settings."<BR>'.$thiserror);

		} elseif($pnp_transaction_array['FinalStatus'] == "problem") {
			$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
			array_push($errormsg,$_REQUEST['edit'].' Credit Card: "Transaction failed due to problem unrelated to credit card."<BR>'.$thiserror);

		} else {
	        // this should not happen
			$thiserror = trim($pnp_transaction_array['resp-code'].' '.$pnp_transaction_array['MErrMsg'].' '.$pnp_transaction_array['Errdetails'].' '.$pnp_transaction_array['aux-msg']);
			array_push($errormsg,'Credit Card: "Transaction failed due to an unknown error."<BR>'.$thiserror);
		} //End Trans Result If Statement

	} //End $_POST['runcard'] if statement

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "cancel" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$resObj = new reservation($_POST['edit']);
	$result = $resObj->fullCancel();
	if($result) {
		$successmsg[] = 'Canceled reservation <b>'.$resObj->id.'</b>.';
	}
	if($GLOBALS['mysql_error'] != "") {
		$errormsg[] = $GLOBALS['mysql_error'];
	}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "restore" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$resObj = new reservation($_POST['edit']);
	$resObj->setDateTimeCanceled(null);
	$resObj->save();

	if($GLOBALS['mysql_error'] != "") {
		$errormsg[] = $GLOBALS['mysql_error'];
	} else {
		$successmsg[] = 'Restored reservation <b>'.$resObj->id.'</b>.';
	}

	//@mysqlQuery('UPDATE `reservations` SET `canceled` = "0" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1');
	//if($thiserror != ""): array_push($errormsg,$thiserror); endif;
	//if(count($errormsg) < 1): array_push($successmsg,'Restored reservation <B>'.$_POST['edit'].'</B>.'); endif;

/* } elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){
	$query = 'DELETE FROM `reservations` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' transportation types were deleted.'); else: array_push($errormsg,$thiserror); endif; */
}


/*
if(isset($reslog) && $reslog != ""){
	@touch('/home2/sesselsa/reslog/reslog');
	@file_put_contents('/home2/sesselsa/reslog/reslog',$reslog,FILE_APPEND | LOCK_EX);
	}
*/


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Reservations</U></FONT><BR>'."\n\n";

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""){  //!VIEW RESERVATION ************************************************

echo '<BR>'."\n\n";

$query = 'SELECT `id`,`canceled` FROM `reservations` WHERE `id` = "'.$_REQUEST['view'].'" LIMIT 1';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1){
	$row = mysql_fetch_array($result);
	$canceled = $row['canceled'];

	//Previous Res
	$query = 'SELECT `id` FROM `reservations` WHERE `id` < "'.$_REQUEST['view'].'" ORDER BY `id` DESC LIMIT 1';
		$result = mysqlQuery($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$row = mysql_fetch_array($result);
			$prevres = $row['id'];
			}

	//Next Res
	$query = 'SELECT `id` FROM `reservations` WHERE `id` > "'.$_REQUEST['view'].'" ORDER BY `id` ASC LIMIT 1';
		$result = mysqlQuery($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$row = mysql_fetch_array($result);
			$nextres = $row['id'];
			}

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		echo '<A HREF="viewres.php?view='.$_REQUEST['view'].'&print=y" TARGET="_blank">Print Reservation</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['view'].'">Edit Reservation</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'; if($canceled > 0){ echo '<A HREF="3_cancellations.php">Cancellation Index</A>'; } else { echo '<A HREF="'.$_SERVER['PHP_SELF'].'">Reservation Index</A>'; }
		if(isset($prevres) && $prevres != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$prevres.'">&lt; Prev</A>'; }
		if(isset($nextres) && $nextres != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$nextres.'">Next &gt;</A>'; }
		echo '</SPAN><BR><BR>'."\n\n";

	include('viewres.php');

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		echo '<A HREF="viewres.php?view='.$_REQUEST['view'].'&print=y" TARGET="_blank">Print Reservation</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['view'].'">Edit Reservation</A>';
		echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'; if($canceled > 0){ echo '<A HREF="3_cancellations.php">Cancellation Index</A>'; } else { echo '<A HREF="'.$_SERVER['PHP_SELF'].'">Reservation Index</A>'; }
		if(isset($prevres) && $prevres != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$prevres.'">&lt; Prev</A>'; }
		if(isset($nextres) && $nextres != ""){ echo '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'?view='.$nextres.'">Next &gt;</A>'; }
		echo '</SPAN><BR><BR>'."\n\n";

	} else {
	echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Error: Unable to find the reservation you requested.<BR><BR><A HREF="'.$_SERVER['PHP_SELF'].'">Please click here for the reservation index</A></SPAN>'."\n\n";
	}

} elseif(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){  //!EDIT RESERVATION ************************************************

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'date_booked' => $time,
		'canceled' => 0
		);
	} else {
		$resObj = new reservation($_REQUEST['edit']);
		$query = 'SELECT * FROM `reservations` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
		$result = mysqlQuery($query);
		$fillform = mysql_fetch_assoc($result);
		$fillform['cc_num'] = $resObj->getCCnumber();
	}

if(getval('canceled') > 0){ //!EDIT: CANCELLATION STATEMENT *********************************************************************************************

echo '<BR>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:12pt;"><I>This reservation, "'.$_REQUEST['edit'].'", has been canceled.<BR>It must be restored before you can make changes.</I></SPAN><BR><BR>'."\n\n";

echo '<FORM NAME="restoreform" ID="restoreform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="restore">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_REQUEST['edit'].'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Restore '.$_REQUEST['edit'].'" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '</FORM><BR>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Note that you will need to individually restore subreservations as needed.</SPAN><BR><BR>'."\n\n";


} else { //!EDIT: START RESERVATION FORM *********************************************************************************************


//GET SUBRESERVATIONS
$reservations_assoc = array();
$query = 'SELECT *, IFNULL((SELECT `confirmed` FROM `tours_dates` WHERE `tourid` = reservations_assoc.`tourid` AND `date` = reservations_assoc.`date` LIMIT 1),0) AS `confirmed` FROM `reservations_assoc` WHERE `reservation` = "'.getval('id').'" ORDER BY `date` ASC, `dep_time` ASC, `id` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
	array_push($reservations_assoc,$row);
	}
$blankres_assoc = array('id'=>'*new*','reference'=>'','type'=>'t','name'=>'','adults'=>'','seniors'=>'','children'=>'','date'=>$time,'dep_loc'=>'','dep_time'=>'0','arr_loc'=>'','arr_time'=>'0','preftime'=>'','activityid'=>'','market'=>'','trans_type'=>'','notes'=>'','driver_info'=>'','onlydateavl'=>'0','altdates'=>'','lodgeid'=>'','nights'=>'','dep_time'=>mktime(0,0,0,1,1,2005),'amount'=>'','costs'=>'','tourid'=>'','routeid'=>'','bbticket'=>'','username'=>'','password'=>'','days'=>'','vendor'=>'','vendorconf'=>'','vendorpaid'=>'0','vendorpayamt'=>'','canceled'=>'0');
if(!isset($reservations_assoc) || count($reservations_assoc) == 0){
	$reservations_assoc = array($blankres_assoc);
	}

//GET GUESTS
$reservations_guests = array();
$query = 'SELECT reservations_guests.* FROM `reservations_guests`,`reservations_assoc` WHERE reservations_guests.`associd` = reservations_assoc.`id`  AND reservations_assoc.`reservation` = "'.getval('id').'" ORDER BY reservations_guests.`associd` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($reservations_guests['r'.$row['associd']])){ $reservations_guests['r'.$row['associd']] = array(); }
	array_push($reservations_guests['r'.$row['associd']],$row);
	}

//GET TOUR LODGING
$reservations_lodging = array();
$query = 'SELECT reservations_lodging.* FROM `reservations_lodging`,`reservations_assoc` WHERE reservations_lodging.`associd` = reservations_assoc.`id`  AND reservations_assoc.`reservation` = "'.getval('id').'" ORDER BY reservations_lodging.`associd` ASC, reservations_lodging.`room` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($reservations_lodging['r'.$row['associd']])){ $reservations_lodging['r'.$row['associd']] = array(); }
	array_push($reservations_lodging['r'.$row['associd']],$row);
	}

//GET PAYMENTS
/*$reservations_payments = array();
$query = 'SELECT * FROM `reservations_payments` ORDER BY `date` ASC, `desc` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($reservations_payments,$row);
	}*/

//SHOW/HIDE FIELDS
$cols_all = array('name','numguests','guestinfo','tourlodging','date','confirmed','dep_loc','dep_time','arr_loc','preftime','activityid','market','trans_type','notes','driver_info','onlydateavl','altdates','lodgeid','nights','amount','costs','copy','tourid','routeid','bbticket','miniroute','username','password','days','vendor','vendorconf','vendorpaid'); //'reference','type',
$cols_hide = array(
	'a' => array('tourlodging','confirmed','onlydateavl','altdates','tourid','routeid','bbticket','miniroute','market','trans_type','lodgeid','nights','dep_loc','arr_loc','driver_info','username','password','days'),
	'r' => array('tourlodging','confirmed','onlydateavl','altdates','preftime','activityid','tourid','market','trans_type','lodgeid','nights','dep_time','dep_loc','arr_loc','bbticket','miniroute','username','password','days'),
	't' => array('preftime','activityid','routeid','bbticket','miniroute','market','trans_type','lodgeid','nights','username','password','days'),
	's' => array('tourlodging','confirmed','onlydateavl','altdates','preftime','activityid','tourid','routeid','bbticket','miniroute','lodgeid','nights','guestinfo','username','password','days'),
	'p' => array('tourlodging','confirmed','onlydateavl','altdates','preftime','activityid','market','trans_type','tourid','routeid','bbticket','miniroute','lodgeid','nights','guestinfo','username','password','days'),
	'l' => array('tourlodging','confirmed','onlydateavl','altdates','preftime','activityid','tourid','routeid','bbticket','miniroute','market','trans_type','guestinfo','dep_time','dep_loc','arr_loc','driver_info','username','password','days'),
	'h' => array('numguests','guestinfo','tourlodging','confirmed','date','dep_loc','dep_time','arr_loc','preftime','activityid','market','trans_type','driver_info','onlydateavl','altdates','lodgeid','nights','tourid','routeid','miniroute','vendor','vendorconf','vendorpaid'),
	'm' => array('numguests','guestinfo','tourlodging','confirmed','dep_loc','dep_time','arr_loc','preftime','activityid','market','trans_type','driver_info','onlydateavl','altdates','lodgeid','nights','tourid','routeid','bbticket','vendor','vendorconf','vendorpaid')
	);

function col_show_hide($obj,$field,$row){
	global $cols_hide;

	if($row['canceled'] > 0){
		if($obj == 'tr'){ return ''; } else { return 'display:none;'; }
		} elseif(isset($cols_hide[$row['type']]) && in_array($field, make_array($cols_hide[$row['type']]))) {
		if($obj == 'tr'){ return ''; } else { return 'display:none;'; }
		} else {
		if($obj == 'tr'){ return ' STYLE="background:#'.bgcolor('').'"'; } else { return ''; }
		}
	}


?><script language="javascript"><!--

function copy_subres(c){
	addsubres();
	var assoc = 0; while(document.getElementById('assoc_id'+(eval(assoc)+1)) != undefined){ assoc++; }
	//alert(c+':'+assoc+' '+cols_all);  //cols_hide[type].indexOf(cols_all[c]) > -1

	document.getElementById('assoc_reference'+assoc).value = document.getElementById('assoc_reference'+c).value;
	document.getElementById('assoc_type'+assoc).value = document.getElementById('assoc_type'+c).value;
	var i;
	for(i in cols_all){
		if(cols_all[i] != "numguests" && cols_all[i] != "confirmed" && cols_all[i] != "guestinfo" && cols_all[i] != "tourlodging" && cols_all[i] != "date" && cols_all[i] != "dep_time" && cols_all[i] != "arr_time" && cols_all[i] != "copy") {
			//alert(cols_all[i]+' '+document.getElementById('assoc_'+cols_all[i]+c).value);
			document.getElementById('assoc_'+cols_all[i]+assoc).value = document.getElementById('assoc_'+cols_all[i]+c).value;
			}
		}

	//Date & time
	document.getElementById('assoc_date_year'+assoc).value = document.getElementById('assoc_date_year'+c).value;
	document.getElementById('assoc_date_month'+assoc).value = document.getElementById('assoc_date_month'+c).value;
	document.getElementById('assoc_date_day'+assoc).value = document.getElementById('assoc_date_day'+c).value;
	document.getElementById('assoc_dep_time_hour'+assoc).value = document.getElementById('assoc_dep_time_hour'+c).value;
	document.getElementById('assoc_dep_time_mins'+assoc).value = document.getElementById('assoc_dep_time_mins'+c).value;

	//Number of guests
	document.getElementById('assoc_adults'+assoc).value = document.getElementById('assoc_adults'+c).value;
	document.getElementById('assoc_seniors'+assoc).value = document.getElementById('assoc_seniors'+c).value;
	document.getElementById('assoc_children'+assoc).value = document.getElementById('assoc_children'+c).value;

	//Vendor stuff
	document.getElementById('assoc_vendorpayamt'+assoc).value = document.getElementById('assoc_vendorpayamt'+c).value;
	document.getElementById('assoc_vendorpay_year'+assoc).value = document.getElementById('assoc_vendorpay_year'+c).value;
	document.getElementById('assoc_vendorpay_month'+assoc).value = document.getElementById('assoc_vendorpay_month'+c).value;
	document.getElementById('assoc_vendorpay_day'+assoc).value = document.getElementById('assoc_vendorpay_day'+c).value;

	//Guest information
	var info = new Array( new Array() );
	var x = document.getElementById('editform').elements;
	var r = 0;
	for(i in x){ if(x[i] != undefined){
		if(x[i]['name'] == 'assoc_guests'+c+'_name[]'){
			info[r]['name'] = x[i]['value'];
			} else if(x[i]['name'] == 'assoc_guests'+c+'_weight[]'){
			info[r]['weight'] = x[i]['value'];
			} else if(x[i]['name'] == 'assoc_guests'+c+'_lunch[]'){
			info[r]['lunch'] = x[i]['value'];
			//alert(info[r]['name']+' : '+info[r]['weight']+' : '+info[r]['lunch']);
			r++;
			info[r] = new Array();
			}
		}}
	for(i in info){
		if(i > 0 && info[i]['name'] != undefined){ addguest(assoc); }
		}
	r = 0;
	for(i in x){ if(x[i] != undefined){
		if(x[i]['name'] == 'assoc_guests'+assoc+'_name[]' && info[r]['name'] != undefined){
			x[i]['value'] = info[r]['name'];
			} else if(x[i]['name'] == 'assoc_guests'+assoc+'_weight[]' && info[r]['weight'] != undefined){
			x[i]['value'] = info[r]['weight'];
			} else if(x[i]['name'] == 'assoc_guests'+assoc+'_lunch[]' && info[r]['lunch'] != undefined){
			x[i]['value'] = info[r]['lunch'];
			r++;
			}
		}}

	//Tour lodging
	info = new Array( new Array() );
	r = 0;
	for(i in x){ if(x[i] != undefined){
		if(x[i]['name'] == 'assoc_room'+c+'_numguests[]'){
			info[r]['numguests'] = x[i]['value'];
			} else if(x[i]['name'] == 'assoc_room'+c+'_adjust[]'){
			info[r]['adjust'] = x[i]['value'];
			r++;
			info[r] = new Array();
			}
		}}
	for(i in info){
		if(i > 0 && info[i]['numguests'] != undefined){ addroom(assoc); }
		}
	r = 0;
	for(i in x){ if(x[i] != undefined){
		if(x[i]['name'] == 'assoc_room'+assoc+'_numguests[]' && info[r]['numguests'] != undefined){
			x[i]['value'] = info[r]['numguests'];
			} else if(x[i]['name'] == 'assoc_room'+assoc+'_adjust[]' && info[r]['adjust'] != undefined){
			x[i]['value'] = info[r]['adjust'];
			r++;
			}
		}}

	//Fix visible
	show_hide(assoc);	
	vendpay_show_hide(assoc);
	dp_upd(assoc);

	window.location = '#subres'+assoc;
	}

function addsubres(){
	var pos = document.getElementById('newsubrow').rowIndex;
	var assoc = 0; while(document.getElementById('assoc_id'+assoc) != undefined){ assoc++; }

	var assocrow = 0;
	var t = document.getElementById('restable');

	var r = t.insertRow(pos++);
		r.style.backgroundColor = '666666';
		r.style.backgroundImage = 'url(\'img/topbar.jpg\')';
		r.style.backgroundPosition = 'center center';
		r.style.backgroundRepeat = 'repeat-x';
		r.style.cursor = 'pointer';
		r.onclick = function(){ exp_col_assoc(assoc); }
		r.onmouseover = function(){ document.getElementById('head_subres_'+assoc+'_l').style.color='FFCC33'; document.getElementById('head_subres_'+assoc+'_r').style.color='FFCC33'; }
		r.onmouseout = function(){ document.getElementById('head_subres_'+assoc+'_l').style.color='FFFFFF'; document.getElementById('head_subres_'+assoc+'_r').style.color='FFFFFF'; }
	var d = r.insertCell(0);
		d.style.textAlign = 'left';
		d.style.borderBottom = 'solid 1px #000000';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.color = 'FFFFFF';
		d.style.paddingLeft = '10px';
		d.innerHTML = '<A NAME="subres'+assoc+'"></A><SPAN ID="head_subres_'+assoc+'_l">- Subreservation '+eval(assoc+1)+'</SPAN><INPUT TYPE="hidden" NAME="assoc_id[]" ID="assoc_id'+assoc+'" VALUE="*new*">';
	var d = r.insertCell(1);
		d.style.textAlign = 'right';
		d.style.borderBottom = 'solid 1px #000000';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.color = 'FFFFFF';
		d.style.paddingRight = '10px';
		d.innerHTML = '<SPAN ID="head_subres_'+assoc+'_r">Click here to show/hide</SPAN>';

	var field = 'type';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Type<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Be sure to first choose the correct type.</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_type[]" ID="assoc_type'+assoc+'" onChange="show_hide(\''+assoc+'\'); dp_upd(\''+assoc+'\');"><?
			foreach($restypes as $key => $val){
				echo '<OPTION VALUE="'.$key.'"'; if($key == $blankres_assoc['type']){ echo ' SELECTED'; } echo '>'.str_replace("'",'\\\'',$val).'</OPTION>';
				}
			echo '</SELECT>'; ?>';

	var field = 'reference';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Reference/Title';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_reference[]" ID="assoc_reference'+assoc+'" STYLE="width:300px;" VALUE="<? echo $blankres_assoc['reference']; ?>">';

	var field = 'tourid';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Bundu tour';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_tourid[]" ID="assoc_tourid'+assoc+'" STYLE="font-size:9pt; width:400px;" onChange="dp_upd(\''+assoc+'\');"><OPTION VALUE="0">n/a</OPTION><?
			foreach($tours as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($blankres_assoc['type'] == 't' && $val['id'] == $blankres_assoc['tourid']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['subhead']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'routeid';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Bundu route';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_routeid[]" ID="assoc_routeid'+assoc+'" STYLE="font-size:9pt; width:400px;" onChange="dp_upd(\''+assoc+'\');"><OPTION VALUE="0">n/a</OPTION><?
			foreach($routes as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($blankres_assoc['type'] == 'r' && $val['id'] == $blankres_assoc['routeid']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['subhead']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'activityid';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Activity / Tour option';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_activityid[]" ID="assoc_activityid'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
			foreach($activities as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($blankres_assoc['type'] == 'a' && $val['id'] == $blankres_assoc['activityid']): echo ' SELECTED'; endif;
			if(isset($val['disable']) && $val['disable'] == 1){ echo ' STYLE="color:#999999;"'; }
			echo '>'.str_replace("'",'\\\'',$val['name']);
			if(isset($val['disable']) && $val['disable'] == 1){ echo ' *NO LONGER AVAILABLE*'; }
			echo '</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'lodgeid';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Lodging unit';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_lodgeid[]" ID="assoc_lodgeid'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
		foreach($lodging as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($blankres_assoc['type'] == 'l' && $val['id'] == $blankres_assoc['lodgeid']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['name']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'name';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Name<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'assoc_name'+assoc+'\').value=document.getElementById(\'name\').value">Click here to copy from above</SPAN><BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="mkround('+eval(eval(assoc)-1)+','+assoc+');">Round Trip (copy and invert locations from above)</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" ID="assoc_name'+assoc+'" NAME="assoc_name[]" STYLE="width:300px;" VALUE="<? echo $blankres_assoc['name']; ?>">';

	var field = 'date';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Date';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_date_month[]" ID="assoc_date_month'+assoc+'" onChange="checkdate(\''+assoc+'\');"><?
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("n",$blankres_assoc['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="assoc_date_day[]" ID="assoc_date_day\'+assoc+\'" onChange="checkdate(\\\'\'+assoc+\'\\\');">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("j",$blankres_assoc['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="assoc_date_year[]" ID="assoc_date_year\'+assoc+\'" onChange="checkdate(\\\'\'+assoc+\'\\\');">';
		for($i=2006; $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("Y",$blankres_assoc['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT>'; ?> <IMG SRC="img/calico.png" ALT="Find a date" STYLE="cursor:pointer;" ID="assoc_date_dp'+assoc+'" onClick="dp_getdate(\''+assoc+'\',\'assoc_date_year'+assoc+'\',\'assoc_date_month'+assoc+'\',\'assoc_date_day'+assoc+'\');"> <SPAN ID="assoc_date_msg'+assoc+'" STYLE="font-family:Arial; font-size:9pt; color:red;"></SPAN>';

	var field = 'confirmed';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Confirm this tour on this date?';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '11pt';
		d.innerHTML = '<SELECT NAME="assoc_confirm[]" ID="assoc_confirm'+assoc+'"><OPTION VALUE="0">No</OPTION><OPTION VALUE="1">Yes</OPTION></SELECT>';

	var field = 'onlydateavl';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Is this the <I>only</I> date guests have available?';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_onlydateavl[]" ID="assoc_onlydateavl'+assoc+'" onChange="show_hide(\''+assoc+'\')"><?
		echo '<OPTION VALUE="0"'; if($blankres_assoc['onlydateavl'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if($blankres_assoc['onlydateavl'] == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT>'; ?>';

	var field = 'altdates';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Alternate dates';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TEXTAREA NAME="assoc_altdates[]" ID="assoc_altdates'+assoc+'" STYLE="height:55px; width:300px;"><? echo $blankres_assoc['altdates']; ?></TEXTAREA>';

	var field = 'dep_time';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Time';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_dep_time_hour[]" ID="assoc_dep_time_hour'+assoc+'"><?
		for($ii=0; $ii<24; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("G",$blankres_assoc['dep_time']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("ga",mktime($ii,"1","0","1","1","2005")).'</OPTION>';
			}
			echo '</SELECT>:<INPUT TYPE="text" SIZE="3" NAME="assoc_dep_time_mins[]" ID="assoc_dep_time_mins\'+assoc+\'" VALUE="'.date("i",$blankres_assoc['dep_time']).'">'; ?>';

	var field = 'preftime';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Preferred time of day';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_preftime[]" ID="assoc_preftime'+assoc+'" STYLE="width:300px;"><OPTION VALUE="">n/a</OPTION><?
			foreach($preftimes as $val){
				echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'"';
				if($val == $blankres_assoc['preftime']): echo ' SELECTED'; endif;
				echo '>'.str_replace("'",'\\\'',$val).'</OPTION>';
				}
			echo '</SELECT>'; ?>';

	var field = 'notes';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Notes/Flight information<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For shuttle, this is included in confirmation email.</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TEXTAREA NAME="assoc_notes[]" ID="assoc_notes'+assoc+'" STYLE="height:70px; width:400px;"><? echo $blankres_assoc['notes']; ?></TEXTAREA>';

	var field = 'market';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Market';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_market[]" ID="assoc_market'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
			foreach($markets as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($val['id'] == $blankres_assoc['market']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['name']).'&nbsp;&nbsp;-&nbsp;&nbsp;'.str_replace("'",'\\\'',$val['desc']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'trans_type';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Transportation type';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_trans_type[]" ID="assoc_trans_type'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
			foreach($transtypes as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($val['id'] == $blankres_assoc['trans_type']): echo ' SELECTED'; endif;
			if(isset($val['disable']) && $val['disable'] == 1){ echo ' STYLE="color:#999999;"'; }
			echo '>'.str_replace("'",'\\\'',$val['name']);
			if(isset($val['disable']) && $val['disable'] == 1){ echo ' *NO LONGER AVAILABLE*'; }
			echo '</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'nights';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Number of nights';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_nights[]" ID="assoc_nights'+assoc+'" VALUE="<? echo $blankres_assoc['nights']; ?>" STYLE="width:80px;">';

	var field = 'numguests';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Number of guests';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0"><?
			echo '<TR><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_adults[]"  ID="assoc_adults\'+assoc+\'" VALUE="'.$blankres_assoc['adults'].'" STYLE="width:50px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_seniors[]"  ID="assoc_seniors\'+assoc+\'" VALUE="'.$blankres_assoc['seniors'].'" STYLE="width:50px;"></TD><TD><INPUT TYPE="text" NAME="assoc_children[]" ID="assoc_children\'+assoc+\'" VALUE="'.$blankres_assoc['children'].'" STYLE="width:50px;"></TD></TR>';
			echo '<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Adults</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Seniors</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Children</TD></TR>';
			echo '</TABLE>'; ?>';

	var field = 'guestinfo';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Guest information<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If needed, list names, weights and lunch preferences.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addguest(\''+assoc+'\');">';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TABLE BORDER="0" CELLPADDING="0" ID="gueststable'+assoc+'"><TR><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_guests'+assoc+'_name[]" VALUE="" STYLE="font-size:9pt; width:150px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_guests'+assoc+'_weight[]" VALUE="" STYLE="font-size:9pt; width:80px;"></TD><TD STYLE="padding-right:5px;"><SELECT NAME="assoc_guests'+assoc+'_lunch[]" STYLE="font-size:9pt;"><OPTION VALUE="">n/a</OPTION><?
		foreach($lunchtypes as $val){
			echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'">'.str_replace("'",'\\\'',$val).'</OPTION>';
				}
			echo '</SELECT>'; ?></TD><TD STYLE="vertical-align:middle;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR><TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Name</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Weight</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Lunch</TD></TR></TABLE>';

	var field = 'tourlodging';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Tour lodging<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For multi day tours.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addroom(\''+assoc+'\');">';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TABLE BORDER="0" CELLPADDING="0" ID="roomstable'+assoc+'"><TR><TD STYLE="font-family:Arial; font-size:9pt; padding-right:5px;">Room 1</TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_room'+assoc+'_numguests[]" VALUE="" STYLE="text-align:right; width:50px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_room'+assoc+'_adjust[]" VALUE="" STYLE="width:80px;"></TD><TD STYLE="vertical-align:middle;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR><TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Room</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Guests</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Adjust</TD></TR></TABLE>';

	var field = 'dep_loc';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Pick up location';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TEXTAREA NAME="assoc_dep_loc[]" ID="assoc_dep_loc'+assoc+'" STYLE="height:40px; width:300px;"><? echo $blankres_assoc['dep_loc']; ?></TEXTAREA>';

	var field = 'arr_loc';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Drop off location';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TEXTAREA NAME="assoc_arr_loc[]" ID="assoc_arr_loc'+assoc+'" STYLE="height:40px; width:300px;"><? echo $blankres_assoc['arr_loc']; ?></TEXTAREA>';

	var field = 'driver_info';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Driver information';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<TEXTAREA NAME="assoc_driver_info[]" ID="assoc_driver_info'+assoc+'" STYLE="height:55px; width:400px;"><? echo $blankres_assoc['driver_info']; ?></TEXTAREA>';

	var field = 'bbticket';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Hop On Hop Off Type';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_bbticket[]" ID="assoc_bbticket'+assoc+'" STYLE="width:300px;" onChange="calc_assoc('+assoc+')"><OPTION VALUE="0">n/a</OPTION><?
		foreach($hoho as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			echo '>'.str_replace("'",'\\\'',$val['name']).' : $'.$val['price'].'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'miniroute';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Mini Route Type';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_miniroute[]" ID="assoc_miniroute'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
		foreach($miniroutes as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			//if($val['id'] == $blankres_assoc['miniroute']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['name']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'username';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'User name<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'assoc_username'+assoc+'\').value=document.getElementById(\'email\').value">Click here to copy from email above</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" ID="assoc_username'+assoc+'" NAME="assoc_username[]" STYLE="width:300px;" VALUE="<? echo $blankres_assoc['username']; ?>">';

	var field = 'password';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Password';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_password[]" ID="assoc_password'+assoc+'" VALUE="<? echo $blankres_assoc['password']; ?>" STYLE="width:300px;">';

	var field = 'days';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Days';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_days[]" ID="assoc_days'+assoc+'" VALUE="<? echo $blankres_assoc['days']; ?>" STYLE="width:60px;">';

	var field = 'vendor';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Vendor';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_vendor[]" ID="assoc_vendor'+assoc+'" STYLE="width:300px;"><OPTION VALUE="0">n/a</OPTION><?
		foreach($vendors as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($val['id'] == $blankres_assoc['vendor']): echo ' SELECTED'; endif;
			echo '>'.str_replace("'",'\\\'',$val['name']).'</OPTION>';
			}
			echo '</SELECT>'; ?>';

	var field = 'vendorconf';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Vendor conf #';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_vendorconf[]" ID="assoc_vendorconf'+assoc+'" STYLE="width:300px;" VALUE="<? echo $blankres_assoc['vendorconf']; ?>">';

	var field = 'vendorpaid';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Vendor commission paid';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.whiteSpace = 'nowrap';
		d.innerHTML = '<SELECT NAME="assoc_vendorpaid[]" ID="assoc_vendorpaid'+assoc+'" onChange="vendpay_show_hide(\''+assoc+'\')"><?
		echo '<OPTION VALUE="0"'; if(!isset($blankres_assoc['vendorpaid']) || $blankres_assoc['vendorpaid'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if(isset($blankres_assoc['vendorpaid']) && $blankres_assoc['vendorpaid'] > "0"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT> '; ?>';
		d.innerHTML += '<SPAN ID="vendorpaydate'+assoc+'"<?
			if(!isset($blankres_assoc['vendorpaid']) || $blankres_assoc['vendorpaid'] == "0"){ echo ' STYLE="display:none;"'; $blankres_assoc['vendorpaid'] = $time; }
			echo '>$<INPUT TYPE="text" NAME="assoc_vendorpayamt[]" ID="assoc_vendorpayamt\'+assoc+\'" VALUE="'.$blankres_assoc['vendorpayamt'].'" STYLE="width:80px;"> on ';
			echo '<SELECT NAME="assoc_vendorpay_month[]" ID="assoc_vendorpay_month\'+assoc+\'">';
				for($i=1; $i<13; $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("n",$blankres_assoc['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
					}
			echo '</SELECT> / <SELECT NAME="assoc_vendorpay_day[]" ID="assoc_vendorpay_day\'+assoc+\'">';
				for($i=1; $i<32; $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("j",$blankres_assoc['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.'</OPTION>';
					}
			echo '</SELECT> / <SELECT NAME="assoc_vendorpay_year[]" ID="assoc_vendorpay_year\'+assoc+\'">';
				for($i=2006; $i<(date("Y",$time)+11); $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("Y",$blankres_assoc['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.'</OPTION>';
					}
			echo '</SELECT></SPAN>'; ?>';

	var field = 'amount';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Subreservation amount/price<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">The sum of these subreservation amounts will determine the total reservation amount.</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '14pt';
		d.style.fontWeight = 'bold';
		d.style.verticalAlign = 'middle';
		d.innerHTML = '$<INPUT TYPE="text" NAME="assoc_amount[]" ID="assoc_amount'+assoc+'" VALUE="<? echo $blankres_assoc['amount']; ?>" STYLE="font-size:14pt; font-weight:bold; width:100px;"><INPUT TYPE="button" VALUE="Calculate" onClick="calc_assoc('+assoc+')" STYLE="font-size:10pt;">';

	var field = 'costs';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Internal costs';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '12pt';
		d.innerHTML = '$<INPUT TYPE="text" NAME="assoc_costs[]" ID="assoc_costs'+assoc+'" VALUE="<? echo $blankres_assoc['costs']; ?>" STYLE="width:100px;">';

	var field = 'copy';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Copy this subreservation<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Copy this subreservation into a new one.</SPAN>';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '12pt';
		d.innerHTML = '<INPUT TYPE="button" VALUE="Copy" onClick="copy_subres(\''+assoc+'\');">';

	var field = 'canceled';
	var r = t.insertRow(pos++);
		r.id = 'row_'+assoc+'_'+assocrow++;
	var d = r.insertCell(0);
		d.id = 'col_'+field+'_'+assoc+'_l';
		d.style.textAlign = 'right';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '10pt';
		d.style.fontWeight = 'bold';
		d.style.paddingRight = '10px';
		d.innerHTML = 'Cancel this subreservation?';
	var d = r.insertCell(1);
		d.id = 'col_'+field+'_'+assoc+'_r';
		d.innerHTML = '<SELECT NAME="assoc_canceled[]" ID="assoc_canceled'+assoc+'" onChange="cancel_assoc(\''+assoc+'\')"><?
		echo '<OPTION VALUE="0"'; if($blankres_assoc['canceled'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if($blankres_assoc['canceled'] == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT>'; ?>';

	hidesub[assoc] = 0;
	show_hide(assoc);
	}

function addguest(a){
	var t = document.getElementById('gueststable'+a);
		var r = t.insertRow( eval(t.rows.length-1) );

		var d = r.insertCell(0);
		d.style.paddingRight = '5px';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_guests'+a+'_name[]" VALUE="" STYLE="font-size:9pt; width:150px;">';

		var d = r.insertCell(1);
		d.style.paddingRight = '5px';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_guests'+a+'_weight[]" VALUE="" STYLE="font-size:9pt; width:80px;">';

		var d = r.insertCell(2);
		d.style.paddingRight = '5px';
		d.innerHTML = '<SELECT NAME="assoc_guests'+a+'_lunch[]" STYLE="font-size:9pt;"><OPTION VALUE="">n/a</OPTION><?
		foreach($lunchtypes as $val){
			echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'">'.str_replace("'",'\\\'',$val).'</OPTION>';
				}
			echo '</SELECT>'; ?>';

		var d = r.insertCell(3);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
	}

function addroom(a){
	var t = document.getElementById('roomstable'+a);
		var rm = eval(t.rows.length);
		var r = t.insertRow( eval(rm-1) );

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.paddingRight = '5px';
		d.innerHTML = 'Room '+rm;

		var d = r.insertCell(1);
		d.style.paddingRight = '5px';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_room'+a+'_numguests[]" VALUE="" STYLE="text-align:right; width:50px;">';

		var d = r.insertCell(2);
		d.style.paddingRight = '5px';
		d.innerHTML = '<INPUT TYPE="text" NAME="assoc_room'+a+'_adjust[]" VALUE="" STYLE="width:80px;">';

		var d = r.insertCell(3);
		d.style.verticalAlign = 'middle';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex); roomlabels('+a+');">';
	}

function roomlabels(a){
	var t = document.getElementById('roomstable'+a);
	
	for(i=0; i<eval(t.rows.length-1); i++){
		var rm = eval(i+1);
		t.rows[i].cells[0].innerHTML = 'Room '+rm;
		}

	}



var bg = 'EEEEEE';
function bgcolor(){
	if(bg == "FFFFFF"){ bg = "EEEEEE"; } else { bg = "FFFFFF"; }
	return bg;
	}

function mkround(from,to){
	document.getElementById("assoc_type"+to).value = document.getElementById("assoc_type"+from).value;
	document.getElementById("assoc_name"+to).value = document.getElementById("assoc_name"+from).value;
	document.getElementById("assoc_market"+to).value = document.getElementById("assoc_market"+from).value;
	document.getElementById("assoc_trans_type"+to).value = document.getElementById("assoc_trans_type"+from).value;
	document.getElementById("assoc_adults"+to).value = document.getElementById("assoc_adults"+from).value;
	document.getElementById("assoc_seniors"+to).value = document.getElementById("assoc_seniors"+from).value;
	document.getElementById("assoc_children"+to).value = document.getElementById("assoc_children"+from).value;
	document.getElementById("assoc_dep_loc"+to).value = document.getElementById("assoc_arr_loc"+from).value;
	document.getElementById("assoc_arr_loc"+to).value = document.getElementById("assoc_dep_loc"+from).value;
	document.getElementById("assoc_vendor"+to).value = document.getElementById("assoc_vendor"+from).value;
	document.getElementById("assoc_vendorconf"+to).value = document.getElementById("assoc_vendorconf"+from).value;
	show_hide(to);
	}

var cols_all = new Array('<? echo implode("','",$cols_all); ?>');
var cols_hide = new Array();
<? foreach($cols_hide as $key => $row){
	echo "\t".'cols_hide[\''.$key.'\'] = new Array(\''.implode("','",$row).'\');'."\n";
	} ?>

var hidesub = new Array();
	<? foreach($reservations_assoc as $assoc => $row){
		echo "\t".'hidesub['.$assoc.'] = '.$row['canceled'].';'."\n";
		} ?>

function show_hide(i){
	var type = document.getElementById('assoc_type'+i).value;

	for(c in cols_all){
		//alert( cols_hide[type] + "\n" + '|'+cols_all[c]+'| ' + cols_hide[type].indexOf(cols_all[c]) );
		if(cols_hide[type] != undefined && cols_hide[type].indexOf(cols_all[c]) > -1){
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = 'none';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = 'none';
			} else {
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = '';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = '';
			}
		}

	if(cols_hide[type] != undefined && cols_hide[type].indexOf('onlydateavl') == -1 && document.getElementById('assoc_onlydateavl'+i).value == 0 || cols_hide[type] == undefined && document.getElementById('assoc_onlydateavl'+i).value == 0){
		document.getElementById('col_altdates_'+i+'_l').style.display = '';
		document.getElementById('col_altdates_'+i+'_r').style.display = '';
		} else {
		document.getElementById('col_altdates_'+i+'_l').style.display = 'none';
		document.getElementById('col_altdates_'+i+'_r').style.display = 'none';
		}

	rowsback(i);
	document.getElementById('head_subres_'+i+'_l').innerHTML = '- Subreservation '+eval(eval(i)+1);
	hidesub[i] = 0;
	}

function cancel_assoc(i){
	if(eval(document.getElementById('assoc_canceled'+i).value) > 0){
		for(c in cols_all){
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = 'none';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = 'none';
			}
		if(hidesub[i] == 0){ window.location = '#subres'+i; }
		document.getElementById('head_subres_'+i+'_l').innerHTML = '+ Subreservation '+eval(eval(i)+1);
		hidesub[i] = 1;
		rowsback(i);
		} else {
		for(c in cols_all){
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = '';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = '';
			}
		document.getElementById('head_subres_'+i+'_l').innerHTML = '- Subreservation '+eval(eval(i)+1);
		hidesub[i] = 0;
		show_hide(i);
		}
	}

function exp_col_assoc(i){
	if(hidesub[i] == 0){
		for(c in cols_all){
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = 'none';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = 'none';
			}
		document.getElementById('head_subres_'+i+'_l').innerHTML = '+ Subreservation '+eval(eval(i)+1);
		hidesub[i] = 1;
		rowsback(i);
		} else {
		for(c in cols_all){
			document.getElementById('col_'+cols_all[c]+'_'+i+'_l').style.display = '';
			document.getElementById('col_'+cols_all[c]+'_'+i+'_r').style.display = '';
			}
		document.getElementById('head_subres_'+i+'_l').innerHTML = '- Subreservation '+eval(eval(i)+1);
		hidesub[i] = 0;
		show_hide(i);
		}
	}

function rowsback(i){
	bg = 'DDDDDD';
	var assocrow = 0;
	while(document.getElementById('row_'+i+'_'+assocrow) != undefined){
		if(document.getElementById('row_'+i+'_'+assocrow).firstChild.style.display != "none"){
			document.getElementById('row_'+i+'_'+assocrow).style.backgroundColor = bgcolor();
			}
		assocrow++;
		}
	}

function vendpay_show_hide(i){
	if(document.getElementById('assoc_vendorpaid'+i).value == 0){
		document.getElementById('vendorpaydate'+i).style.display = 'none';
		} else {
		document.getElementById('vendorpaydate'+i).style.display = '';
		}
	}

function clk_runcard(){
	if(document.getElementById("runcard").checked){
		document.getElementById("cc_newtrans").style.display = '';
		if(document.getElementById("cardamount").value == ""){
			document.getElementById("cardamount").value = document.getElementById("amount").value;
			}
		} else {
		document.getElementById("cc_newtrans").style.display = 'none';
		}

	if(document.getElementById("pnp_orderid") != undefined && document.getElementById("pnp_orderid").value != ""){
		document.getElementById("cc_prevcharge").disabled = 0;
		document.getElementById("lab_cc_prevcharge").style.color = '#000000';
		document.getElementById("cc_refund").disabled = 0;
		document.getElementById("lab_cc_refund").style.color = '#000000';
		} else {
		document.getElementById("cc_newcharge").checked = 1;
		document.getElementById("cc_prevcharge").disabled = 1;
		document.getElementById("lab_cc_prevcharge").style.color = '#C7C7C7';
		document.getElementById("cc_refund").disabled = 1;
		document.getElementById("lab_cc_refund").style.color = '#C7C7C7';
		}
	}

function IsNumeric(sText){
	sText = sText.replace(/^\s+|\s+$/g,"");
	var ValidChars = "0123456789.-";
	var IsNumber = true;
	var Char;
	for(var i=0; i<sText.length && IsNumber == true; i++){ 
		Char = sText.charAt(i);
		if(ValidChars.indexOf(Char) == -1){ IsNumber = false; }
		}
	if(sText == ""){ IsNumber = false; }
	return IsNumber;
	}

function GetXmlHttpObject(){
	if(window.XMLHttpRequest){
		return new XMLHttpRequest();
		}
	if(window.ActiveXObject){
		return new ActiveXObject("Microsoft.XMLHTTP");
		}
	return null;
	}

var xmlhttp = new Array();
var gfields = new Array('type','date_month','date_day','date_year','tourid','routeid','activityid','bbticket','miniroute','market','trans_type','adults','seniors','children','lodgeid','nights');

function calc_assoc(i) {
	var url = '../getamount.php?getamount=1';
	for(key in gfields){
		url += '&'+gfields[key]+'='+document.getElementById('assoc_'+gfields[key]+i).value;
		}
	xmlhttp[i]=GetXmlHttpObject();
	xmlhttp[i].onreadystatechange = function(){
		if(xmlhttp[i].readyState == 4){
			var results = xmlhttp[i].responseText;
			var rows = results.split("\n");

			var read = 0;
			for(y in rows){
				if(rows[y] == '||END||'){ read = 0; }
				if(read == 1){
					var amt = eval(eval(rows[y]) + eval(assoc_lodgeadjust(i)));
					//alert(amt);
					document.getElementById('assoc_amount'+i).value = amt.toFixed(2);
					break;
					}
				if(rows[y] == '||BEGIN||'){ read = 1; }
				}

			}
		}
	//console.info(url);
	xmlhttp[i].open('GET',url,true);
	xmlhttp[i].send(null);
}

function assoc_lodgeadjust(a){
	var adj = 0;
	//var out = '';
	if(cols_hide[document.getElementById('assoc_type'+a).value].indexOf('tourlodging') == -1){
		var x = document.getElementById('editform').elements;
		for(var i in x){ if(x[i] != undefined){
			if(x[i]['name'] == 'assoc_room'+a+'_adjust[]'){
				if(IsNumeric(x[i]['value'])){ adj = eval(adj + eval(x[i]['value'])); }
				//out += i+' : '+x[i]['name']+' : '+x[i]['value']+"\n";
				}
			}}
		}
	//alert(adj);
	return adj;
	}

function totalsubs(){
	var total = 0;
	var assocrow = 0;
	while(document.getElementById('assoc_amount'+assocrow) != undefined){
		if(document.getElementById('assoc_amount'+assocrow).value != "" && document.getElementById('assoc_amount'+assocrow).value > 0 && eval(document.getElementById('assoc_canceled'+assocrow).value) == 0){
			total = eval(total + eval(document.getElementById('assoc_amount'+assocrow).value) );
			}
		assocrow++;
		}
	document.getElementById('amount').value = total.toFixed(2);
}

function calc_all(){
	var data = $("#editform").serialize()+'&command=calc_all';
	$.post("reservations_ajax.php", data, function(response){
		for(i in response){
			if(i == "total"){
				$('#amount').val(response[i]);
			} else {
				//$('#assoc_amount'+i).val(response[i]);
			}
		}
	}, "json");
}

function checkdate(assoc){
	if( dp_testdate(assoc,document.getElementById('assoc_date_year'+assoc).value,(eval(document.getElementById('assoc_date_month'+assoc).value)-1),document.getElementById('assoc_date_day'+assoc).value) ){
		document.getElementById('assoc_date_msg'+assoc).innerHTML = '';
		} else {
		document.getElementById('assoc_date_msg'+assoc).innerHTML = '<BR>Warning: This date is not available for the route or tour chosen.';
		}
	}


//Date picker
var dp_date = new Date();

var dp_months = new Array(<?
	$month_names = array();
		for($i=1; $i<13; $i++){ array_push($month_names,date("M",mktime(0,0,0,$i,1,2010))); }
		echo '"'.implode('","',$month_names).'"';
	?>);

var dp_cal = new Array();
<? for($y=2006; $y<=(date("Y",$time)+10); $y++){
	echo "\t".'dp_cal['.$y.'] = new Array();';
		for($m=1; $m<13; $m++){
		$first = mktime(0,0,0,$m,1,$y);
		echo ' dp_cal['.$y.']['.($m-1).'] = new Array('.date("w",$first).','.date("t",$first).');';
		}
		echo "\n";
	} ?>

var dp_avail = new Array();
<? foreach($reservations_assoc as $assoc => $row){ if($row['type'] == "r" || $row['type'] == "t"){
	$dp_avail = array();
	//Get running dates
	if($row['type'] == "r" && $row['routeid'] > 0){
		$query = 'SELECT `date` FROM `routes_dates` WHERE `routeid` = "'.$row['routeid'].'" ORDER BY `date` ASC';
		} elseif($row['type'] == "t" && $row['tourid'] > 1){
		$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$row['tourid'].'" ORDER BY `date` ASC';
		}
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$d = mysql_fetch_assoc($result);
		//echo '<PRE STYLE="text-align:left;">'; print_r($d); echo '</PRE>';
		if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
		if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
		$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = 0;
		}
	//Get blocked seats
	if($row['type'] == "r" && $row['routeid'] > 0){
		include_once('../getblocked.php');
			$blocked = getblocked($row['routeid'],'*','*');
			//echo '</CENTER><PRE>'; print_r($blocked); echo '</PRE>';
			if(isset($blocked['r'.$row['routeid']])){
			foreach($blocked['r'.$row['routeid']] as $key => $d){
				if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
				if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
				$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = ($d['tour_seats']+$d['route_seats']);
				}}
		} elseif($row['type'] == "t" && $row['tourid'] > 1){
		include_once('../getblocked.php');
			$blocked = getblocked_tours($row['tourid'],'*','*');
			//echo '</CENTER><PRE>'; print_r($blocked); echo '</PRE>';
			if(isset($blocked['t'.$row['tourid']])){
			foreach($blocked['t'.$row['tourid']] as $key => $d){
				if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
				if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
				$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = $d['blocked'];
				}}
		}
	if(count($dp_avail) > 0){
		echo "\t".'dp_avail[\'a'.$assoc.'\'] = new Array();'."\n";
		foreach($dp_avail as $y => $years){
			echo "\t".'dp_avail[\'a'.$assoc.'\']['.$y.'] = new Array();'."\n";
			foreach($years as $m => $months){
				echo "\t".'dp_avail[\'a'.$assoc.'\']['.$y.']['.$m.'] = new Array();';
				foreach($months as $d => $day){
					echo ' dp_avail[\'a'.$assoc.'\']['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
					}
				echo "\n";
				}
			echo "\n";
			}
		}
	}} ?>

var dp_xmlhttp = new Array();

function dp_upd(assoc){
	dp_clearavail(assoc);

	var url  = 'sups/calassist.php?get=avail';
		url += '&type='+document.getElementById('assoc_type'+assoc).value;
		url += '&routeid='+document.getElementById('assoc_routeid'+assoc).value;
		url += '&tourid='+document.getElementById('assoc_tourid'+assoc).value;
	dp_xmlhttp[assoc]=GetXmlHttpObject();
	dp_xmlhttp[assoc].onreadystatechange = function(){
		if(dp_xmlhttp[assoc].readyState == 4){
			var results = dp_xmlhttp[assoc].responseText;
			if(results.indexOf('||END||') > -1){
				dp_avail['a'+assoc] = new Array();
				var rows = results.split("\n");
				var read = 0;
				for(y in rows){
					if(rows[y] == '||END||'){ read = 0; }
					if(read == 1){
						var d = rows[y].split('|');
						if(dp_avail['a'+assoc][d[0]] == undefined){ dp_avail['a'+assoc][d[0]] = new Array(); }
						if(dp_avail['a'+assoc][d[0]][d[1]] == undefined){ dp_avail['a'+assoc][d[0]][d[1]] = new Array(); }
						dp_avail['a'+assoc][d[0]][d[1]][d[2]] = d[3];
						}
					if(rows[y] == '||BEGIN||'){ read = 1; }
					}
				} //End results if
			} //End state change
		} //End function
	dp_xmlhttp[assoc].open('GET',url,true);
	dp_xmlhttp[assoc].send(null);
	}

function dp_testdate(id,y,m,d){
	var allow = false;

	if(id < 0 || dp_avail == undefined || dp_avail['a'+id] == undefined){
		allow = true;
		} else if(id > -1 && dp_avail != undefined && dp_avail['a'+id] != undefined && dp_avail['a'+id][y] != undefined && dp_avail['a'+id][y][m] != undefined && dp_avail['a'+id][y][m][d] != undefined && dp_avail['a'+id][y][m][d] > -1){
		allow = true;
		//alert(id+' '+y+' '+m+' '+d+' '+dp_avail['a'+id][y][m][d]);
		}

	return allow;
	}

function dp_clearavail(assoc){
	dp_id = -1;
	dp_avail['a'+assoc] = undefined;
	dp_hide();
	}

function dp_movecal(i){
	dp_date.setFullYear(document.getElementById("dp_year").value,eval(eval(document.getElementById("dp_month").value)+eval(i)),1); //dp_date.getDate()
	dp_buildcal();
	}

function dp_setcal(){
	dp_date.setFullYear(document.getElementById("dp_year").value,document.getElementById("dp_month").value,dp_date.getDate());
	dp_buildcal();
	}

var dp_x = 0;
var dp_y = 0;
var dp_id = -1;

function dp_getdate(assoc,id_year,id_month,id_day){
	if(dp_id == assoc && document.getElementById('dp_div').style.display == ''){
		dp_hide();
		} else {
		dp_id = assoc;
	
		dp_date.setFullYear(document.getElementById('assoc_date_year'+assoc).value,(eval(document.getElementById('assoc_date_month'+assoc).value)-eval(1)),document.getElementById('assoc_date_day'+assoc).value);
	
		var newpos = dp_findPos(document.getElementById('assoc_date_dp'+assoc));
			dp_x = newpos[0];
			dp_y = newpos[1];
	
		dp_buildcal();
		}
	}

function dp_buildcal(){
	var dp_calw = eval(7*32);
	var dp_div = document.getElementById('dp_div');

	var y = dp_date.getFullYear();
	var m = dp_date.getMonth();

	var html = '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="dp_calendar" WIDTH="'+dp_calw+'" STYLE="background:#FFFFFF; border:1px solid #7F9DB9;">';
		html += '<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px;">';
			html += '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">';
				html += '<TD ALIGN="left" STYLE="padding-right:4px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&lt;&lt;" onClick="dp_movecal(-1);"></TD>';
				html += '<TD ALIGN="center" STYLE="font-size:8pt;"><SELECT ID="dp_month" STYLE="font-size:8pt;" onChange="dp_setcal();">';
				for(i=0; i<=11; i++){
					html += '<OPTION VALUE="'+i+'"';
					if(m == i){ html += " SELECTED"; }
					html += '>'+dp_months[i]+'</OPTION>';
					}
				html += '</SELECT> <SELECT ID="dp_year" STYLE="font-size:8pt;" onChange="dp_setcal();">';
					for(i=2006; i<=<? echo (date("Y",$time)+10); ?>; i++){
					html += '<OPTION VALUE="'+i+'"';
					if(y == i){ html += " SELECTED"; }
					html += '>'+i+'</OPTION>';
					}
				html += '</SELECT></TD>';
		html += '<TD ALIGN="right" STYLE="padding-left:4px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&gt;&gt;" onClick="dp_movecal(1);"></TD>';
		html += '</TABLE>';
		html += '</TD></TR>';

		html += '<TR BGCOLOR="#CCCCCC" onClick="dp_hide();">';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">M</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">W</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">F</TD>';
			html += '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>';
			html += '</TR>';

		html += '</TABLE>';

	dp_div.style.width = dp_calw;
	dp_div.style.position = 'absolute';
	dp_div.style.display = '';
	dp_div.innerHTML = html;

	var col = dp_cal[y][m][0];

	var r = document.getElementById('dp_calendar').insertRow(document.getElementById('dp_calendar').rows.length);
	r.style.backgroundColor = 'FFFFFF';

	if(dp_cal[y][m][0] > 0){
		var c = r.insertCell(r.cells.length);
		c.colSpan = dp_cal[y][m][0];
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.borderRight = '1px solid #999999';
		c.style.height = Math.ceil(dp_calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		c.onclick = function(){ dp_hide(); }
		}

	for(day=1; day<=dp_cal[y][m][1]; day++){
		col++;

		var c = r.insertCell(r.cells.length);
		c.id = 'd'+day;
		c.style.borderTop = '1px solid #999999';
		if(col < 7){
			c.style.borderRight = '1px solid #999999';
			}
		c.style.textAlign = 'left';
		c.style.verticalAlign = 'top';
		c.style.padding = '3px';
		c.style.width = Math.ceil(dp_calw/7)+'px';
		c.style.height = Math.ceil(dp_calw/7)+'px';
		c.style.fontFamily = 'Arial';
		c.style.fontSize = '11px';
		c.abbr = day;
		c.innerHTML = day;

		c.style.cursor = 'pointer';
		c.onclick = function(){ dp_pickdate(this.abbr); }

		var allow = dp_testdate(dp_id,y,m,day);

		//alert(dp_id+' '+y+' '+m+' '+day+' '+dp_avail['a'+dp_id][y][m][day]);

		if(dp_id > -1 && dp_avail != undefined && dp_avail['a'+dp_id] != undefined && dp_avail['a'+dp_id][y] != undefined && dp_avail['a'+dp_id][y][m] != undefined && dp_avail['a'+dp_id][y][m][day] != undefined && eval(dp_avail['a'+dp_id][y][m][day]) > 0){
			c.innerHTML += '<DIV STYLE="text-align:center; color:#666666;">('+dp_avail['a'+dp_id][y][m][day]+')</DIV>';
			}

		if(allow){
			c.onmouseover = function(){ this.style.backgroundColor='#bbd8f1'; }
			c.onmouseout = function(){ this.style.backgroundColor='transparent'; }
			} else {
			c.style.color='#999999';
			c.style.backgroundColor='#DDDDDD';
			}

		if(col == 7){
			var r = document.getElementById('dp_calendar').insertRow(document.getElementById('dp_calendar').rows.length);
			//r.style.backgroundColor = 'FFFFFF';
			col = 0;
			}

		} //End For Loop

	if(col > 0 && col < 7){
		var c = r.insertCell(r.cells.length);
		c.colSpan = eval(7-col);
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.height = Math.ceil(dp_calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		c.onclick = function(){ dp_hide(); }
		}

	var r = document.getElementById('dp_calendar').insertRow(document.getElementById('dp_calendar').rows.length);
	r.style.backgroundColor = 'CCCCCC';

	var c = r.insertCell(r.cells.length);
	c.colSpan = 7;
	c.style.borderTop = '1px solid #999999';
	c.style.fontFamily = 'Arial';
	c.style.fontSize = '10pt';
	c.style.textAlign = 'center';
	c.style.cursor = 'pointer';
	c.innerHTML = '<I>- Close -</I>';
	c.onclick = function(){ dp_hide(); }

	dp_div.style.left = (eval(dp_x) - eval(dp_calw) - 2);
	dp_div.style.top = (eval(dp_y) + 1);

	}

function dp_pickdate(d){
	dp_date.setDate(d);
	if(document.getElementById('assoc_date_year'+dp_id)){ document.getElementById('assoc_date_year'+dp_id).value = dp_date.getFullYear(); }
	if(document.getElementById('assoc_date_month'+dp_id)){ document.getElementById('assoc_date_month'+dp_id).value = (eval(dp_date.getMonth()) + eval(1)); }
	if(document.getElementById('assoc_date_day'+dp_id)){ document.getElementById('assoc_date_day'+dp_id).value = dp_date.getDate(); }
	document.getElementById('dp_div').style.display = 'none';
	checkdate(dp_id);
	}

function dp_hide(){
	document.getElementById('dp_div').style.display = 'none';
	}

var dp_bg = 'EEEEEE';
function dp_bgcolor(){
	if(dp_bg == "FFFFFF"){ dp_bg = "EEEEEE"; } else { dp_bg = "FFFFFF"; }
	return dp_bg;
	}

function dp_findPos(obj){
	var curleft = curtop = 0;
	if(obj.offsetParent){
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while(obj = obj.offsetParent){
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
			}
		}
	return [curleft,curtop];
	}

function test(i){
	alert(i);
	}

//--></script><? echo "\n\n";


bgcolor('');

echo '<BR>'."\n\n";

echo '<DIV ID="dp_div" STYLE="position:absolute; display:none;"></DIV>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

//echo '<INPUT TYPE="button" VALUE="TEST" onClick="test(\'0\')"><BR>'."\n\n";

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" ID="restable">'."\n";

//!BASIC
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Basic / Contact Info</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Confirmation number</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval2('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name</TD><TD><INPUT TYPE="text" ID="name" NAME="name" STYLE="width:300px;" VALUE="'.getval2('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Home/business phone</TD><TD><INPUT TYPE="text" NAME="phone_homebus" STYLE="width:300px;" VALUE="'.getval2('phone_homebus').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cell phone</TD><TD><INPUT TYPE="text" NAME="phone_cell" STYLE="width:300px;" VALUE="'.getval2('phone_cell').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cell country</TD><TD><INPUT TYPE="text" NAME="cell_country" STYLE="width:300px;" VALUE="'.getval2('cell_country').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Email</TD><TD><INPUT TYPE="text" NAME="email" ID="email" STYLE="width:300px;" VALUE="'.getval2('email').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Special instructions<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This will be included in the customer email confirmation.</SPAN></TD><TD><TEXTAREA NAME="email_inst" STYLE="height:100px; width:400px;">'.getval('email_inst').'</TEXTAREA></TD></TR>'."\n";
	if(getval('email_conf') > 0){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Email confirmation was sent</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.date("g:ia n/d/Y",getval('email_conf')).'</TD></TR>'."\n";
		} else {
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD VALIGN="top" ALIGN="right" STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="confsent" ID="confsent" VALUE="y"></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="confsent">This customer has already received an email confirmation</LABEL></TD></TR>'."\n"; //<BR><SPAN STYLE="font-size:9pt; font-weight:normal; color:#666666;">If you sent an email confirmation to the customer, check this and click on "Save"</SPAN>
		}
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD VALIGN="top" ALIGN="right" STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="sendconf" ID="sendconf" VALUE="y"></TD><TD STYLE="font-family:Arial; font-size:11pt;"><LABEL FOR="sendconf">Send a new email confirmation on "Save"</LABEL><BR><SPAN STYLE="font-size:9pt; font-weight:normal; color:#666666;">Note: At this time only <B><I>shuttle</I></B> subreservations are included in the confirmation email.</SPAN></TD></TR>'."\n";
	echo "\n";

//!GUESTS
/*	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Guests</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:8pt; color:#666666;">If needed, list names, weights and lunch preferences for this reservation.<BR>'."\n";
		echo '<DIV STYLE="padding-top:4px; padding-bottom:4px;"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" ID="gueststable">';
		foreach($reservations_guests as $row){
			echo "\t".'<TR><TD STYLE="font-family:Arial; font-size:10pt; vertical-align:middle; white-space:nowrap; padding-right:8px;">Name: <INPUT TYPE="text" NAME="guests_name[]" VALUE="'.$row['name'].'" STYLE="width:200px;">&nbsp;&nbsp;Weight: <INPUT TYPE="text" NAME="guests_weight[]" VALUE="'.$row['weight'].'" STYLE="width:100px;">&nbsp;&nbsp;Lunch: <SELECT NAME="guests_lunch[]"><OPTION VALUE="">n/a</OPTION>';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.$val.'"';
					if($row['lunch'] == $val){ echo ' SELECTED'; }
					echo '>'.$val.'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>'."\n";
			}
		echo '</TABLE></DIV>';
		echo '<INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addguest();">';
		echo '</TD></TR>'."\n";
	echo "\n";*/

//!ROUTES/ACTIVITIES/ETC.
foreach($reservations_assoc as $assoc => $row){
	$assocrow = 0;
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x; cursor:pointer;" onClick="exp_col_assoc(\''.$assoc.'\')" onMouseOver="document.getElementById(\'head_subres_'.$assoc.'_l\').style.color=\'FFCC33\'; document.getElementById(\'head_subres_'.$assoc.'_r\').style.color=\'FFCC33\';" onMouseOut="document.getElementById(\'head_subres_'.$assoc.'_l\').style.color=\'FFFFFF\'; document.getElementById(\'head_subres_'.$assoc.'_r\').style.color=\'FFFFFF\';">';
		echo '<TD ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">';
		echo '<A NAME="subres'.$assoc.'"></A>';
		echo '<SPAN ID="head_subres_'.$assoc.'_l">'; if($row['canceled'] > 0){ echo '+ '; } else { echo '- '; } echo 'Subreservation '.($assoc+1).'</SPAN>';
		echo '<INPUT TYPE="hidden" NAME="assoc_id[]" ID="assoc_id'.$assoc.'" VALUE="'.$row['id'].'">';
		echo '</TD><TD ALIGN="right" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; color:#FFFFFF; padding-right:10px;"><SPAN ID="head_subres_'.$assoc.'_r">Click here to show/hide</SPAN></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'" ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;" ID="col_type_'.$assoc.'_l">Type<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Be sure to first choose the correct type.</SPAN></TD><TD ID="col_type_'.$assoc.'_r">';
		echo '<SELECT NAME="assoc_type[]" ID="assoc_type'.$assoc.'" onChange="show_hide(\''.$assoc.'\'); dp_upd(\''.$assoc.'\');">';
		foreach($restypes as $key => $val){
			echo '<OPTION VALUE="'.$key.'"';
				if($row['type'] == $key){ echo ' SELECTED'; }
				echo '>'.$val.'</OPTION>';
			}
			echo '</SELECT>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'" ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;" ID="col_reference_'.$assoc.'_l">Reference/Title</TD><TD ID="col_reference_'.$assoc.'_r"><INPUT TYPE="text" NAME="assoc_reference[]" ID="assoc_reference'.$assoc.'" STYLE="width:300px;" VALUE="'.$row['reference'].'"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','tourid',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','tourid',$row).'" ID="col_tourid_'.$assoc.'_l">Bundu tour</TD><TD STYLE="'.col_show_hide('td','tourid',$row).'" ID="col_tourid_'.$assoc.'_r"><SELECT NAME="assoc_tourid[]" ID="assoc_tourid'.$assoc.'" STYLE="font-size:9pt; width:400px;" onChange="dp_upd(\''.$assoc.'\');">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($tours as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($row['type'] == 't' && $val['id'] == $row['tourid']): echo ' SELECTED'; endif;
		echo '>'.$val['subhead'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','routeid',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','routeid',$row).'" ID="col_routeid_'.$assoc.'_l">Bundu route</TD><TD STYLE="'.col_show_hide('td','routeid',$row).'" ID="col_routeid_'.$assoc.'_r"><SELECT NAME="assoc_routeid[]" ID="assoc_routeid'.$assoc.'" STYLE="font-size:9pt; width:400px;" onChange="dp_upd(\''.$assoc.'\');">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($routes as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($row['type'] == 'r' && $val['id'] == $row['routeid']): echo ' SELECTED'; endif;
		echo '>'.$val['subhead'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','activityid',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','activityid',$row).'" ID="col_activityid_'.$assoc.'_l">Activity / Tour option</TD><TD STYLE="'.col_show_hide('td','activityid',$row).'" ID="col_activityid_'.$assoc.'_r"><SELECT NAME="assoc_activityid[]" ID="assoc_activityid'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($activities as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($row['type'] == 'a' && $val['id'] == $row['activityid']): echo ' SELECTED'; endif;
		if(isset($val['disable']) && $val['disable'] == 1){ echo ' STYLE="color:#999999;"'; }
		echo '>'.$val['name'];
		if(isset($val['disable']) && $val['disable'] == 1){ echo ' *NO LONGER AVAILABLE'; }
		echo '</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','lodgeid',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','lodgeid',$row).'" ID="col_lodgeid_'.$assoc.'_l">Lodging unit</TD><TD STYLE="'.col_show_hide('td','lodgeid',$row).'" ID="col_lodgeid_'.$assoc.'_r"><SELECT NAME="assoc_lodgeid[]" ID="assoc_lodgeid'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($lodging as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($row['type'] == 'l' && $val['id'] == $row['lodgeid']): echo ' SELECTED'; endif;
		echo '>'.$val['name'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','name',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','name',$row).'" ID="col_name_'.$assoc.'_l">Name';
		echo '<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'assoc_name'.$assoc.'\').value=document.getElementById(\'name\').value">Click here to copy from above</SPAN>';
		if($assoc > 0){ echo '<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="mkround('.($assoc-1).','.$assoc.');">Round Trip (copy and invert locations from above)</SPAN>'; }
		echo '</TD><TD STYLE="'.col_show_hide('td','name',$row).'" ID="col_name_'.$assoc.'_r"><INPUT TYPE="text" ID="assoc_name'.$assoc.'" NAME="assoc_name[]" STYLE="width:300px;" VALUE="'.$row['name'].'"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','date',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','date',$row).'" ID="col_date_'.$assoc.'_l">Date</TD><TD STYLE="'.col_show_hide('td','date',$row).'" ID="col_date_'.$assoc.'_r">';
		echo '<SELECT NAME="assoc_date_month[]" ID="assoc_date_month'.$assoc.'" onChange="checkdate(\''.$assoc.'\');">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("n",$row['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="assoc_date_day[]" ID="assoc_date_day'.$assoc.'" onChange="checkdate(\''.$assoc.'\');">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("j",$row['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="assoc_date_year[]" ID="assoc_date_year'.$assoc.'" onChange="checkdate(\''.$assoc.'\');">';
		for($i=2006; $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("Y",$row['date']) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> <IMG SRC="img/calico.png" ALT="Find a date" ID="assoc_date_dp'.$assoc.'" STYLE="cursor:pointer;" onClick="dp_getdate(\''.$assoc.'\',\'assoc_date_year'.$assoc.'\',\'assoc_date_month'.$assoc.'\',\'assoc_date_day'.$assoc.'\');"> <SPAN ID="assoc_date_msg'.$assoc.'" STYLE="font-family:Arial; font-size:9pt; color:red;"></SPAN></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','confirmed',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','confirmed',$row).'" ID="col_confirmed_'.$assoc.'_l">Confirm this tour on this date?</TD>';
		echo '<TD STYLE="'.col_show_hide('td','confirmed',$row).' font-family:Arial; font-size:11pt;" ID="col_confirmed_'.$assoc.'_r"><SELECT NAME="assoc_confirm[]" ID="assoc_confirm'.$assoc.'">';
		echo '<OPTION VALUE="0">No</OPTION>';
		echo '<OPTION VALUE="1"'; if($row['confirmed'] == 1): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','onlydateavl',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','onlydateavl',$row).'" ID="col_onlydateavl_'.$assoc.'_l">Is this the <I>only</I> date guests have available?</TD><TD STYLE="'.col_show_hide('td','onlydateavl',$row).'" ID="col_onlydateavl_'.$assoc.'_r"><SELECT NAME="assoc_onlydateavl[]" ID="assoc_onlydateavl'.$assoc.'" onChange="show_hide(\''.$assoc.'\')">';
		echo '<OPTION VALUE="0"'; if($row['onlydateavl'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if($row['onlydateavl'] == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR';
		if($row['canceled'] == 0 && !in_array('altdates', make_array($cols_hide[$row['type']])) && $row['onlydateavl'] !== '1'){ echo ' STYLE="background:#'.bgcolor('').'"'; }
		echo ' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;';
		if(	$row['altdates'] == ""
			&& ($row['canceled'] > 0 || in_array('altdates', make_array($cols_hide[$row['type']])) || $row['onlydateavl'] === '1')
			){
			echo ' display:none;';
		}
		echo '" ID="col_altdates_'.$assoc.'_l">Alternate dates</TD><TD';
		if(	$row['altdates'] == ""
			&& ($row['canceled'] > 0 || in_array('altdates', make_array($cols_hide[$row['type']])) || $row['onlydateavl'] === '1')
			){
			echo ' STYLE="display:none;"';
		}
		echo ' ID="col_altdates_'.$assoc.'_r"><TEXTAREA NAME="assoc_altdates[]" ID="assoc_altdates'.$assoc.'" STYLE="height:55px; width:300px;">'.$row['altdates'].'</TEXTAREA></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','dep_time',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','dep_time',$row).'" ID="col_dep_time_'.$assoc.'_l">Time</TD><TD STYLE="'.col_show_hide('td','dep_time',$row).'" ID="col_dep_time_'.$assoc.'_r">';
		echo '<SELECT NAME="assoc_dep_time_hour[]" ID="assoc_dep_time_hour'.$assoc.'">';
		for($ii=0; $ii<24; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("G",$row['dep_time']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("ga",mktime($ii,"1","0","1","1","2005")).'</OPTION>';
			}
			echo '</SELECT>:<INPUT TYPE="text" SIZE="3" NAME="assoc_dep_time_mins[]" ID="assoc_dep_time_mins'.$assoc.'" VALUE="'.date("i",$row['dep_time']).'">';
		echo '</TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','preftime',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','preftime',$row).'" ID="col_preftime_'.$assoc.'_l">Preferred time of day</TD><TD STYLE="'.col_show_hide('td','preftime',$row).'" ID="col_preftime_'.$assoc.'_r"><SELECT NAME="assoc_preftime[]" ID="assoc_preftime'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="">n/a</OPTION>';
	foreach($preftimes as $val){
		echo '<OPTION VALUE="'.$val.'"';
		if($val == $row['preftime']): echo ' SELECTED'; endif;
		echo '>'.$val.'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','notes',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','notes',$row).'" ID="col_notes_'.$assoc.'_l">Notes/Flight information<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For shuttle, this is included in confirmation email.</SPAN></TD><TD STYLE="'.col_show_hide('td','notes',$row).'" ID="col_notes_'.$assoc.'_r"><TEXTAREA NAME="assoc_notes[]" ID="assoc_notes'.$assoc.'" STYLE="height:70px; width:400px;">'.$row['notes'].'</TEXTAREA></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','market',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','market',$row).'" ID="col_market_'.$assoc.'_l">Market</TD><TD STYLE="'.col_show_hide('td','market',$row).'" ID="col_market_'.$assoc.'_r"><SELECT NAME="assoc_market[]" ID="assoc_market'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($markets as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($val['id'] == $row['market']): echo ' SELECTED'; endif;
		echo '>'.$val['name'].'&nbsp;&nbsp;-&nbsp;&nbsp;'.$val['desc'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','trans_type',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','trans_type',$row).'" ID="col_trans_type_'.$assoc.'_l">Transportation type</TD><TD STYLE="'.col_show_hide('td','trans_type',$row).'" ID="col_trans_type_'.$assoc.'_r"><SELECT NAME="assoc_trans_type[]" ID="assoc_trans_type'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($transtypes as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($val['id'] == $row['trans_type']): echo ' SELECTED'; endif;
		if(isset($val['disable']) && $val['disable'] == 1){ echo ' STYLE="color:#999999;"'; }
		echo '>'.$val['name'];
		if(isset($val['disable']) && $val['disable'] == 1){ echo ' *NO LONGER AVAILABLE'; }
		echo '</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','nights',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','nights',$row).'" ID="col_nights_'.$assoc.'_l">Number of nights</TD><TD STYLE="'.col_show_hide('td','nights',$row).'" ID="col_nights_'.$assoc.'_r"><INPUT TYPE="text" NAME="assoc_nights[]" ID="assoc_nights'.$assoc.'" VALUE="'.$row['nights'].'" STYLE="width:80px;"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','numguests',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','numguests',$row).'" ID="col_numguests_'.$assoc.'_l">Number of guests</TD><TD STYLE="'.col_show_hide('td','numguests',$row).'" ID="col_numguests_'.$assoc.'_r">';
		echo '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0">';
			echo '<TR><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_adults[]" ID="assoc_adults'.$assoc.'" VALUE="'.$row['adults'].'" STYLE="width:50px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_seniors[]" ID="assoc_seniors'.$assoc.'" VALUE="'.$row['seniors'].'" STYLE="width:50px;"></TD><TD><INPUT TYPE="text" NAME="assoc_children[]" ID="assoc_children'.$assoc.'" VALUE="'.$row['children'].'" STYLE="width:50px;"></TD></TR>';
			echo '<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Adults</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Seniors</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Children</TD></TR>';
			echo '</TABLE>';
		echo '</TD></TR>'."\n";
	if(!isset($reservations_guests['r'.$row['id']]) || count($reservations_guests['r'.$row['id']]) == 0){
		$reservations_guests['r'.$row['id']] = array(array('name' => '','weight' => '','lunch' => ''));
		}
	echo '<TR'.col_show_hide('tr','guestinfo',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','guestinfo',$row).'" ID="col_guestinfo_'.$assoc.'_l">Guest information<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If needed, list names, weights and lunch preferences.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addguest(\''.$assoc.'\');"></TD><TD STYLE="'.col_show_hide('td','guestinfo',$row).'" ID="col_guestinfo_'.$assoc.'_r">';
		echo '<TABLE BORDER="0" CELLPADDING="0" ID="gueststable'.$assoc.'">';
		foreach($reservations_guests['r'.$row['id']] as $guest){
			echo "\t".'<TR><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_guests'.$assoc.'_name[]" VALUE="'.$guest['name'].'" STYLE="font-size:9pt; width:150px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_guests'.$assoc.'_weight[]" VALUE="'.$guest['weight'].'" STYLE="font-size:9pt; width:80px;"></TD><TD STYLE="padding-right:5px;"><SELECT NAME="assoc_guests'.$assoc.'_lunch[]" STYLE="font-size:9pt;"><OPTION VALUE="">n/a</OPTION>';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.$val.'"';
					if($guest['lunch'] == $val){ echo ' SELECTED'; }
					echo '>'.$val.'</OPTION>';
					}
				echo '</SELECT></TD><TD STYLE="vertical-align:middle;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>'."\n";
			}
		echo '<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Name</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Weight</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Lunch</TD></TR>';
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	if(!isset($reservations_lodging['r'.$row['id']]) || count($reservations_lodging['r'.$row['id']]) == 0){
		$reservations_lodging['r'.$row['id']] = array(array('room' => '1','numguests' => '','adjust' => ''));
		}
	echo '<TR'.col_show_hide('tr','tourlodging',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','tourlodging',$row).'" ID="col_tourlodging_'.$assoc.'_l">Tour lodging<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For multi day tours.</SPAN><BR><INPUT TYPE="button" VALUE="Add another" STYLE="font-size:8pt;" onClick="addroom(\''.$assoc.'\');"></TD><TD STYLE="'.col_show_hide('td','tourlodging',$row).'" ID="col_tourlodging_'.$assoc.'_r">';
		echo '<TABLE BORDER="0" CELLPADDING="0" ID="roomstable'.$assoc.'">';
		foreach($reservations_lodging['r'.$row['id']] as $room){
			echo "\t".'<TR><TD STYLE="font-family:Arial; font-size:9pt; padding-right:5px;">Room '.$room['room'].'</TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_room'.$assoc.'_numguests[]" VALUE="'.$room['numguests'].'" STYLE="text-align:right; width:50px;"></TD><TD STYLE="padding-right:5px;"><INPUT TYPE="text" NAME="assoc_room'.$assoc.'_adjust[]" VALUE="'.$room['adjust'].'" STYLE="width:80px;"></TD><TD STYLE="vertical-align:middle;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex); roomlabels('.$assoc.');"></TD></TR>'."\n";
			}
		echo '<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Room</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Guests</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Adjust</TD></TR>';
		echo '</TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','dep_loc',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','dep_loc',$row).'" ID="col_dep_loc_'.$assoc.'_l">Pick up location</TD><TD STYLE="'.col_show_hide('td','dep_loc',$row).'" ID="col_dep_loc_'.$assoc.'_r"><TEXTAREA NAME="assoc_dep_loc[]" ID="assoc_dep_loc'.$assoc.'" STYLE="height:40px; width:300px;">'.$row['dep_loc'].'</TEXTAREA></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','arr_loc',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','arr_loc',$row).'" ID="col_arr_loc_'.$assoc.'_l">Drop off location</TD><TD STYLE="'.col_show_hide('td','arr_loc',$row).'" ID="col_arr_loc_'.$assoc.'_r"><TEXTAREA NAME="assoc_arr_loc[]" ID="assoc_arr_loc'.$assoc.'" STYLE="height:40px; width:300px;">'.$row['arr_loc'].'</TEXTAREA></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','driver_info',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','driver_info',$row).'" ID="col_driver_info_'.$assoc.'_l">Driver information</TD><TD STYLE="'.col_show_hide('td','driver_info',$row).'" ID="col_driver_info_'.$assoc.'_r"><TEXTAREA NAME="assoc_driver_info[]" ID="assoc_driver_info'.$assoc.'" STYLE="height:55px; width:400px;">'.$row['driver_info'].'</TEXTAREA></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','bbticket',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','bbticket',$row).'" ID="col_bbticket_'.$assoc.'_l">Hop On Hop Off Type<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">For pricing.</SPAN></TD><TD STYLE="'.col_show_hide('td','bbticket',$row).'" ID="col_bbticket_'.$assoc.'_r"><SELECT NAME="assoc_bbticket[]" ID="assoc_bbticket'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($hoho as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($val['id'] == $row['bbticket']): echo ' SELECTED'; endif;
		echo '>'.$val['name'].' : $'.$val['price'].'</OPTION>';
		}
		echo '</SELECT>';
		//<INPUT TYPE="text" NAME="assoc_bbticket[]" ID="assoc_bbticket'.$assoc.'" VALUE="'.$row['bbticket'].'" STYLE="width:60px;">
		echo '</TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','miniroute',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','miniroute',$row).'" ID="col_miniroute_'.$assoc.'_l">Mini Route Type</TD><TD STYLE="'.col_show_hide('td','miniroute',$row).'" ID="col_miniroute_'.$assoc.'_r"><SELECT NAME="assoc_miniroute[]" ID="assoc_miniroute'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($miniroutes as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($val['id'] == $row['miniroute']): echo ' SELECTED'; endif;
		echo '>'.$val['name'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','username',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','username',$row).'" ID="col_username_'.$assoc.'_l">User name';
		echo '<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'assoc_username'.$assoc.'\').value=document.getElementById(\'email\').value">Click here to copy email from above</SPAN>';
		echo '</TD><TD STYLE="'.col_show_hide('td','username',$row).'" ID="col_username_'.$assoc.'_r"><INPUT TYPE="text" ID="assoc_username'.$assoc.'" NAME="assoc_username[]" STYLE="width:300px;" VALUE="'.$row['username'].'"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','password',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','password',$row).'" ID="col_password_'.$assoc.'_l">Password</TD><TD STYLE="'.col_show_hide('td','password',$row).'" ID="col_password_'.$assoc.'_r"><INPUT TYPE="text" ID="assoc_password'.$assoc.'" NAME="assoc_password[]" STYLE="width:300px;" VALUE="'.$row['password'].'"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','days',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','days',$row).'" ID="col_days_'.$assoc.'_l">Days</TD><TD STYLE="'.col_show_hide('td','days',$row).'" ID="col_days_'.$assoc.'_r"><INPUT TYPE="text" NAME="assoc_days[]" ID="assoc_days'.$assoc.'" VALUE="'.$row['days'].'" STYLE="width:60px;"></TD></TR>'."\n";

	echo '<TR'.col_show_hide('tr','vendor',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','vendor',$row).'" ID="col_vendor_'.$assoc.'_l">Vendor</TD><TD STYLE="'.col_show_hide('td','vendor',$row).'" ID="col_vendor_'.$assoc.'_r"><SELECT NAME="assoc_vendor[]" ID="assoc_vendor'.$assoc.'" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
	foreach($vendors as $val){
		echo '<OPTION VALUE="'.$val['id'].'"';
		if($val['id'] == $row['vendor']): echo ' SELECTED'; endif;
		echo '>'.$val['name'].'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','vendorconf',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','vendorconf',$row).'" ID="col_vendorconf_'.$assoc.'_l">Vendor conf #</TD><TD STYLE="'.col_show_hide('td','vendorconf',$row).'" ID="col_vendorconf_'.$assoc.'_r"><INPUT TYPE="text" NAME="assoc_vendorconf[]" ID="assoc_vendorconf'.$assoc.'" STYLE="width:300px;" VALUE="'.$row['vendorconf'].'"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','vendorpaid',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','vendorpaid',$row).'" ID="col_vendorpaid_'.$assoc.'_l">Vendor commission paid</TD><TD STYLE="font-family:Arial; font-size:10pt; white-space:nowrap; '.col_show_hide('td','vendorpaid',$row).'" ID="col_vendorpaid_'.$assoc.'_r"><SELECT NAME="assoc_vendorpaid[]" ID="assoc_vendorpaid'.$assoc.'" onChange="vendpay_show_hide(\''.$assoc.'\')">';
		echo '<OPTION VALUE="0"'; if(!isset($row['vendorpaid']) || $row['vendorpaid'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if(isset($row['vendorpaid']) && $row['vendorpaid'] > "0"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT> ';
		echo '<SPAN ID="vendorpaydate'.$assoc.'"';
			if(!isset($row['vendorpaid']) || $row['vendorpaid'] == "0"){ echo ' STYLE="display:none;"'; $row['vendorpaid'] = $time; }
			echo '>$<INPUT TYPE="text" NAME="assoc_vendorpayamt[]" ID="assoc_vendorpayamt'.$assoc.'" VALUE="'.$row['vendorpayamt'].'" STYLE="width:80px;"> on ';
			echo '<SELECT NAME="assoc_vendorpay_month[]" ID="assoc_vendorpay_month'.$assoc.'">';
				for($i=1; $i<13; $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("n",$row['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
					}
			echo '</SELECT> / <SELECT NAME="assoc_vendorpay_day[]" ID="assoc_vendorpay_day'.$assoc.'">';
				for($i=1; $i<32; $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("j",$row['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.'</OPTION>';
					}
			echo '</SELECT> / <SELECT NAME="assoc_vendorpay_year[]" ID="assoc_vendorpay_year'.$assoc.'">';
				for($i=2006; $i<(date("Y",$time)+11); $i++){
					echo '<OPTION VALUE="'.$i.'"';
					if(date("Y",$row['vendorpaid']) == $i){ echo " SELECTED"; }
					echo '>'.$i.'</OPTION>';
					}
			echo '</SELECT></SPAN>';
		echo '</TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','amount',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','amount',$row).'" ID="col_amount_'.$assoc.'_l">Subreservation amount/price<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">The sum of these subreservation amounts will determine the total reservation amount.</SPAN></TD><TD STYLE="font-family:Arial; font-size:14pt; font-weight:bold; vertical-align:middle; '.col_show_hide('td','amount',$row).'" ID="col_amount_'.$assoc.'_r">$<INPUT TYPE="text" NAME="assoc_amount[]" ID="assoc_amount'.$assoc.'" VALUE="'.$row['amount'].'" STYLE="font-size:14pt; font-weight:bold; width:100px;"><INPUT TYPE="button" VALUE="Calculate" onClick="calc_assoc(\''.$assoc.'\')" STYLE="font-size:10pt;"></TD></TR>'."\n";
	echo '<TR'.col_show_hide('tr','costs',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','costs',$row).'" ID="col_costs_'.$assoc.'_l">Internal costs</TD><TD STYLE="font-family:Arial; font-size:12pt; '.col_show_hide('td','costs',$row).'" ID="col_costs_'.$assoc.'_r">$<INPUT TYPE="text" NAME="assoc_costs[]" ID="assoc_costs'.$assoc.'" VALUE="'.$row['costs'].'" STYLE="width:100px;"></TD></TR>'."\n";

	echo '<TR'.col_show_hide('tr','copy',$row).' ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; '.col_show_hide('td','copy',$row).'" ID="col_copy_'.$assoc.'_l">Copy this subreservation<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Copy this subreservation into a new one.</SPAN></TD><TD STYLE="font-family:Arial; font-size:12pt; '.col_show_hide('td','copy',$row).'" ID="col_copy_'.$assoc.'_r"><INPUT TYPE="button" VALUE="Copy" onClick="copy_subres(\''.$assoc.'\');"></TD></TR>'."\n";

	echo '<TR STYLE="background:#'.bgcolor('').'" ID="row_'.$assoc.'_'.$assocrow++.'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Cancel this subreservation?</TD><TD><SELECT NAME="assoc_canceled[]" ID="assoc_canceled'.$assoc.'" onChange="cancel_assoc(\''.$assoc.'\')">';
		echo '<OPTION VALUE="0"'; if($row['canceled'] == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		//if($row['canceled'] == 0){ $cval = 1; } else { $cval = $row['canceled']; }
		echo '<OPTION VALUE="1"'; if($row['canceled'] > 0): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo "\n";
	$assoc++;
	} //End Reservation Assoc For Each

//!ADD NEW SUBRESERVATION
	//bgcolor('reset');
	//echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//	echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">Add a new subreservation</TD>';
	//	echo '</TR>'."\n";
	echo '<TR STYLE="background:#666666 url(\'img/toplinksback.jpg\') top center repeat-x;" ID="newsubrow"><TD COLSPAN="2" ALIGN="center" STYLE="padding:10px;">';
		//echo '<SELECT STYLE="font-size:12pt;"><OPTION VALUE="">Copy and so forth...</OPTION></SELECT>';
		echo '<INPUT TYPE="button" VALUE="Add a new subreservation" STYLE="font-size:12pt;" onClick="addsubres();"></TD></TR>'."\n"; //this.parentNode.parentNode.rowIndex

//!PAYMENTS
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Total / Payments</TD>';
		echo '</TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Reservation total';
		echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Clicking on "Calculate" will total all subreservation amounts not canceled.  It will also add fuel surcharges for tours.</SPAN>';
		//echo '<BR><SPAN STYLE="font-size:8pt; font-weight:bold;">IMPORTANT!  CLICKING ON CALCULATE WILL OVERRIDE ANY AMOUNTS YOU\'VE ENTERED MANUALLY</SPAN>';
		echo '</TD><TD STYLE="font-family:Arial; font-size:14pt; font-weight:bold;">$<INPUT TYPE="text" NAME="amount" ID="amount" VALUE="'.getval2('amount').'" STYLE="font-size:14pt; font-weight:bold; width:110px;"> <INPUT TYPE="button" VALUE="Calculate" STYLE="font-size:12pt;" onClick="calc_all();"></TD></TR>'."\n";

	//if(in_array('3_reservations_edit',$thisuser['allow'])){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Credit card name<BR><SPAN STYLE="font-size:8pt; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'cc_name\').value=document.getElementById(\'name\').value">Click here to copy from above</SPAN></TD><TD><INPUT TYPE="text" ID="cc_name" NAME="cc_name" STYLE="width:300px;" VALUE="'.getval2('cc_name').'"></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Credit card number</TD><TD><INPUT TYPE="text" ID="cc_num" NAME="cc_num" STYLE="width:300px;" VALUE="'.getval2('cc_num').'"></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Expiration date</TD><TD STYLE="font-family:Arial; font-size:11pt;"><INPUT TYPE="text" ID="cc_expdate" NAME="cc_expdate" STYLE="width:100px;" VALUE="'.getval2('cc_expdate').'"> MM/YY</TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Security code</TD><TD><INPUT TYPE="text" ID="cc_scode" NAME="cc_scode" STYLE="width:100px;" VALUE="'.getval2('cc_scode').'"></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Zip/Postal code</TD><TD><INPUT TYPE="text" ID="cc_zip" NAME="cc_zip" STYLE="width:100px;" VALUE="'.getval2('cc_zip').'"></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Method of payment</TD><TD><INPUT TYPE="text" ID="pay_method" NAME="pay_method" STYLE="width:300px;" VALUE="'.getval2('pay_method').'"></TD></TR>'."\n";
	//}
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Alternate name<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If the card holder will not be there,<BR>provide the name of one of the guests</SPAN></TD><TD><INPUT TYPE="text" ID="alt_name" NAME="alt_name" STYLE="width:300px;" VALUE="'.getval2('alt_name').'"></TD></TR>'."\n";
	//if(in_array('3_reservations_edit',$thisuser['allow'])){
		if(getval('card_run') != "" && getval('card_run') > 0){
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Card was run</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.date("g:ia n/d/Y",getval('card_run')).'</TD></TR>'."\n";
			}
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">PlugnPay transaction ID<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This can be used to charge a new amount<BR>to a previously charged card.</SPAN></TD><TD><INPUT TYPE="text" ID="pnp_orderid" NAME="pnp_orderid" STYLE="width:300px;" VALUE="'.getval2('pnp_orderid').'" onFocus="clk_runcard();" onChange="clk_runcard();"></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD VALIGN="top" STYLE="text-align:right; padding-right:10px;"><INPUT TYPE="checkbox" NAME="runcard" ID="runcard" VALUE="y" onClick="clk_runcard()"></TD><TD STYLE="font-family:Arial; font-size:11pt; padding-top:4px; padding-bottom:4px;"><LABEL FOR="runcard" onClick="clk_runcard()">Run new credit card transaction on "Save"</LABEL><BR><DIV ID="cc_newtrans" STYLE="display:none;">';
			echo '<TABLE BORDER="0" CELLSPACING="3" CELLPADDING="0">';
			echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_newcharge" VALUE="cc_newcharge"';
				if(trim(getval('pnp_orderid')) == ""){ echo ' CHECKED'; }
				echo ' onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_newcharge" ID="lab_cc_newcharge" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">New credit card charge</LABEL></TD></TR>'."\n";
			echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_prevcharge" VALUE="cc_prevcharge"';
				if(trim(getval('pnp_orderid')) != ""){ echo ' CHECKED'; }
				echo ' onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_prevcharge" ID="lab_cc_prevcharge" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">Charge card from previous transaction</LABEL></TD></TR>'."\n";
			echo '<TR><TD ALIGN="right" VALIGN="middle"><INPUT TYPE="radio" NAME="authtype" ID="cc_refund" VALUE="cc_refund';
				if(getval('card_run') > 0 && getval('card_run') < ($time-15552000)){ echo '6mos'; }
				echo '" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();"></TD><TD VALIGN="middle" STYLE="padding-top:4px; font-family:Arial; font-size:9pt;"><LABEL FOR="cc_refund" ID="lab_cc_refund" onClick="document.getElementById(\'runcard\').checked=1; clk_runcard();">Refund credit card <SPAN STYLE="font-size:8pt;">(any amount not larger than previous charge)</SPAN></LABEL></TD></TR>'."\n";
			echo '<TR><TD></TD><TD VALIGN="middle" STYLE="font-family:Arial; font-size:10pt;">Transaction Amount<BR>$<INPUT TYPE="text" NAME="cardamount" ID="cardamount" VALUE="" STYLE="width:100px;" onClick="document.getElementById(\'runcard\').checked=1;"> <SPAN STYLE="font-size:8pt; font-weight:bold; color:#666666; cursor:pointer;" onMouseOver="this.style.color=\'#0000CC\';" onMouseOut="this.style.color=\'#666666\';" onClick="document.getElementById(\'cardamount\').value=document.getElementById(\'amount\').value;">Copy reservation total</SPAN></TD></TR>'."\n";
			echo '</TABLE>';
			echo '</DIV></TD></TR>'."\n";
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">PlugnPay confirmation details</TD><TD STYLE="font-family:Arial; font-size:9pt;"><TEXTAREA NAME="conf_details" STYLE="height:100px; width:400px;">'.getval('conf_details').'</TEXTAREA></TD></TR>'."\n";
	//}
	echo "\n";

//!NOTES/ETC.
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Notes / Etc.</TD>';
		echo '</TR>'."\n";
	if(getval('comments') != ""){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">User comments<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Entered online while making reservation.</SPAN></TD><TD STYLE="font-family:Arial; font-size:10pt;"><DIV STYLE="width:400px;">'.getval('comments').'</DIV></TD></TR>'."\n";
		}
	if(getval('http_referer') != ""){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Referred from</TD><TD STYLE="font-family:Arial; font-size:10pt;"><DIV STYLE="width:400px;"><A HREF="'.getval('http_referer').'" TARGET="_blank">'.wordwrap(getval('http_referer'),40,'<BR>',1).'</A></DIV></TD></TR>'."\n";
		}
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Agent</TD><TD><SELECT NAME="agent" STYLE="width:300px;">';
		echo '<OPTION VALUE="0">n/a</OPTION>';
		foreach($agents as $val){
			echo '<OPTION VALUE="'.$val['id'].'"';
			if($val['id'] == getval('agent')){ echo ' SELECTED'; }
			echo '>'.$val['name'].'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n";
		//<INPUT TYPE="text" ID="agent" NAME="agent" STYLE="width:300px;" VALUE="'.getval('agent').'">
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Subagent</TD><TD><SELECT NAME="id_subagent" STYLE="width:300px;">';
		echo '<OPTION VALUE="">n/a</OPTION>';
		foreach($subagents as $id => $disp){
			echo '<OPTION VALUE="'.$id.'"';
			if($id == getval('id_subagent')){ echo ' SELECTED'; }
			echo '>'.$disp.'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Agent confirmation</TD><TD><INPUT TYPE="text" ID="agent_conf" NAME="agent_conf" STYLE="width:300px;" VALUE="'.getval2('agent_conf').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Referral</TD><TD><INPUT TYPE="text" ID="referral" NAME="referral" STYLE="width:300px;" VALUE="'.getval2('referral').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Booker</TD><TD><INPUT TYPE="text" ID="booker" NAME="booker" STYLE="width:300px;" VALUE="'.getval2('booker').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Date booked</TD><TD>';
		echo '<SELECT NAME="date_booked_month">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("n",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_booked_day">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("j",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_booked_year">';
		for($i=2006; $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( date("Y",getval('date_booked')) == $i ){ echo " SELECTED"; }
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Notes</TD><TD><TEXTAREA NAME="notes" STYLE="height:180px; width:400px;">'.getval('notes').'</TEXTAREA></TD></TR>'."\n";
	echo "\n";

	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR>'."\n\n";

echo '</FORM>'."\n\n";

if(getval('id') != '*new*'){

	echo '<HR SIZE="1" WIDTH="94%"><BR>'."\n\n";

	echo '<FORM NAME="cancelform" ID="cancelform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return confirm(\'Cancel '.getval('id').'?\');">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="cancel">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<INPUT TYPE="submit" VALUE="Cancel '.getval('id').'" STYLE="color:#FF0000; width:180px;">'."\n";
	echo '</FORM>'."\n\n";
	}

} //!EDIT: END CANCELLATION STATEMENT *********************************************************************************************


echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Reservation index</A>'."\n\n";


} else {  //!RESERVATION LISTING *****************************************************************


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

//--></SCRIPT><?


//GET RESERVATIONS
$reservations = array();
$resids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS *';
	//$query .= ', (SELECT COUNT(reservations_assoc.`vendorconf`) FROM `reservations_assoc` WHERE reservations_assoc.`reservation` = reservations.`id` AND reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0 AND TRIM(reservations_assoc.`vendorconf`) NOT LIKE "") AS `numvconf`';
	//$query .= ', (SELECT COUNT(reservations_assoc.`vendor`) FROM `reservations_assoc` WHERE reservations_assoc.`reservation` = reservations.`id` AND reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0) AS `numvendors`';
	$query .= ' FROM `reservations`';
	$query .= ' WHERE `canceled` = 0';
	$query .= ' ORDER BY '.$_SESSION['reservations']['sortby'].' '.$_SESSION['reservations']['sortdir'].', reservations.`id` '.$_SESSION['reservations']['sortdir'];
	$query .= ' LIMIT '.(($_SESSION['reservations']['p']-1)*$_SESSION['reservations']['limit']).','.$_SESSION['reservations']['limit'];
$result = mysqlQuery($query);
//echo '<!-- '.$query.' -->'."\n\n";
//echo $GLOBALS['mysql_error'].'<BR>';
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($reservations,$row);
	array_push($resids,$row['id']);
	}

$numitems = @mysqlQuery('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['reservations']['limit']);
if($numpages > 0 && $_SESSION['reservations']['p'] > $numpages): $_SESSION['reservations']['p'] = $numpages; endif;

$link = '';

//GET VENDOR CONFIRMATIONS
$vendorconfs = array();
$query = 'SELECT reservations_assoc.`reservation`,vendors.`name`,reservations_assoc.`vendor`,reservations_assoc.`vendorconf` FROM `reservations_assoc` LEFT JOIN `vendors` ON reservations_assoc.`vendor` = vendors.`id` WHERE reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0 AND (reservations_assoc.`reservation` = "'.implode('" OR reservations_assoc.`reservation` = "',$resids).'") ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC, reservations_assoc.`id` ASC';
$result = mysqlQuery($query);
//echo '<!-- '.$query.' -->'."\n\n";
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($vendorconfs['v'.$row['reservation']])){ $vendorconfs['v'.$row['reservation']] = array(); }
	array_push($vendorconfs['v'.$row['reservation']],$row);
	}


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['reservations']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['reservations']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['reservations']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['reservations']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['reservations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['reservations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['reservations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['reservations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['reservations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Conf#</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Booked</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD COLSPAN="2" ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; white-space:nowrap; cursor:help;" TITLE="Vendor Confirmations">Vendor Conf.</TD>';
	echo '<TD COLSPAN="2" ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Email</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($reservations as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	$thisemail = strtolower($row['email']);
	if(strlen($thisemail) > 20){ $thisemail = substr($thisemail,0,18).'...'; }
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:5px; padding-right:10px; font-family:Arial; font-size:10pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?view='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.date("m/d/y",$row['date_booked']).'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['name'].'</TD>';
	echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;">';
		$v = 0;
		if(isset($vendorconfs['v'.$row['id']]) && count($vendorconfs['v'.$row['id']]) > 0){
			foreach($vendorconfs['v'.$row['id']] as $vrow){
				if($vrow['vendor'] > 0 && trim($vrow['vendorconf']) != ""){ $v++; }
				}
			echo $v.' of '.count($vendorconfs['v'.$row['id']]);
			}
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">';
		if(isset($vendorconfs['v'.$row['id']]) && count($vendorconfs['v'.$row['id']]) > 0 && $v == count($vendorconfs['v'.$row['id']])){ echo '<IMG SRC="img/check.png" BORDER="0" ALT="X" TITLE="All vendor bookings have been confirmed.">'; }
		echo '</TD>';
	echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt;">';
		if(strpos($row['email'],'@') !== false){ echo '<A HREF="mailto:'.$row['email'].'">'.$thisemail.'</A>'; } else { echo '<I>'.$thisemail.'</I>'; }
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">';
		if($row['email_conf'] > 0){ echo '<IMG SRC="img/check.png" BORDER="0" ALT="X" TITLE="Confirmation email has been sent">'; }
		echo '</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['reservations']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['reservations']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['reservations']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['reservations']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['reservations']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Reservation" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '<BR><BR>'."\n\n";

echo '</FORM>'."\n\n";

} //END EDIT IF STATEMENT

require("footer.php");

?>