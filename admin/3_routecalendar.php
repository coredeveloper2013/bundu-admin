<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_routecalendar";
require("validate.php");
require("header.php");

//SETUP CALENDAR
if(!isset($_SESSION['routecalendar'])){
	//Find soonest upcoming month
	$query = 'SELECT `date` FROM `routes_dates` WHERE `date` > '.$time.' ORDER BY `date` ASC LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){
		$soonest = mysql_fetch_assoc($result);
		$soonest = $soonest['date'];
		$_SESSION['routecalendar']['y'] = date("Y",$soonest);
		$_SESSION['routecalendar']['m'] = date("n",$soonest);
		} else {
		$_SESSION['routecalendar']['y'] = date("Y",$time);
		$_SESSION['routecalendar']['m'] = date("n",$time);
		}
	} // End isset($_SESSION['routecalendar'])

if(!isset($_SESSION['routecalendar']['vendor'])): $_SESSION['routecalendar']['vendor'] = "1"; endif;
if(!isset($_SESSION['routecalendar']['view'])): $_SESSION['routecalendar']['view'] = "cal"; endif;

if(isset($_REQUEST['y']) && $_REQUEST['y'] != ""): $_SESSION['routecalendar']['y'] = $_REQUEST['y']; endif;
if(isset($_REQUEST['m']) && $_REQUEST['m'] != ""): $_SESSION['routecalendar']['m'] = $_REQUEST['m']; endif;
if(isset($_REQUEST['vendor']) && $_REQUEST['vendor'] != ""): $_SESSION['routecalendar']['vendor'] = $_REQUEST['vendor']; endif;
if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""): $_SESSION['routecalendar']['view'] = $_REQUEST['view']; endif;

$calfirst = mktime(0,0,0,$_SESSION['routecalendar']['m'],1,$_SESSION['routecalendar']['y']);
$calendar = array(
	"first" => $calfirst,
	"start" => date("w",$calfirst),
	"days" => date("t",$calfirst),
	"m" => date("n",$calfirst),
	"y" => date("Y",$calfirst),
	"prevm" => mktime(0,0,0,(date("n",$calfirst)-1),1,date("Y",$calfirst)),
	"nextm" => mktime(0,0,0,(date("n",$calfirst)+1),1,date("Y",$calfirst))
	);

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['routecalendar']); echo '</PRE>';
//echo '<PRE>'; print_r($calendar); echo '</PRE>';


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

}


include_once('sups/timezones.php');


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Route Calendar</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);



echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Show routes from vendor:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="vendor" STYLE="font-size:9pt;">';
		echo '<OPTION VALUE="*all*">All vendors</OPTION>';
			foreach($vendors as $vendor){
			echo '<OPTION VALUE="'.$vendor['id'].'"';
			if($_SESSION['routecalendar']['vendor'] == $vendor['id']): echo ' SELECTED'; endif;
			echo '>'.$vendor['name'].'</OPTION>';
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">View as a:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="view" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="cal"'; if($_SESSION['routecalendar']['view'] == "cal"): echo " SELECTED"; endif; echo '>Calendar</OPTION>';
			echo '<OPTION VALUE="list"'; if($_SESSION['routecalendar']['view'] == "list"): echo " SELECTED"; endif; echo '>List</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Show" STYLE="font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";


//GET ROUTES
$routes_dates = array();
$query = 'SELECT routes_dates.`date`, (IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) + routes_dates.`date`) AS `time`, routes.`id` AS `routeid`, routes.`name`, routes.`dep_loc`, IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`, routes.`arr_loc`, IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`, routes.`travel_time`, routes.`anchor`, IF(routes_dates.`seats`=-1,routes.`seats`,routes_dates.`seats`) AS `seats`';
	//$query .= ', (SELECT (COALESCE((SELECT SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) FROM `reservations_assoc`,`tours_assoc`,`tours_dates` WHERE ((reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` = tours_assoc.`tourid` AND tours_dates.`tourid` = tours_assoc.`tourid` AND tours_assoc.`dir` = tours_dates.`dir` AND tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND tours_dates.`date` = reservations_assoc.`date` AND tours_dates.`date` = (routes_dates.`date` - ((tours_assoc.`day`-1)*86400))) GROUP BY tours_assoc.`typeid`),0) + COALESCE((SELECT SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) FROM `reservations_assoc` WHERE ((reservations_assoc.`type` = "r" OR reservations_assoc.`type` = "o") AND reservations_assoc.`date` = routes_dates.`date` AND reservations_assoc.`routeid` = routes.`id`) GROUP BY reservations_assoc.`routeid`),0))) AS `booked`';
	$query .= ' FROM `routes`,`routes_dates`';
	$query .= ' WHERE (routes_dates.`date` >= '.$calendar['first'].' AND routes_dates.`date` <= '.mktime(0,0,0,date("n",$calendar['first']),$calendar['days'],date("Y",$calendar['first'])).') AND (routes.`id` = routes_dates.`routeid`)';
	if($_SESSION['routecalendar']['vendor'] != "" && $_SESSION['routecalendar']['vendor'] != "*all*"){ $query .= ' AND routes.`vendor` = "'.$_SESSION['routecalendar']['vendor'].'"'; }
	//$query .= ' AND (routes.`id` = 117 OR routes.`id` = 16)';
	$query .= ' ORDER BY routes_dates.`date` ASC, `time` ASC, routes.`name` ASC';
	//echo '>= '.date("n/j/Y g:ia",$calendar['first']).' AND <= '.date("n/j/Y g:ia",mktime(0,0,0,date("n",$calendar['first']),$calendar['days'],date("Y",$calendar['first']))).'<BR>';
	//echo $query.'<BR>';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
		$datestr = date("F j, Y",$row['date']);
		if($row['anchor'] == 'arr'){
			$row['arr_time'] = strtotime($datestr. ' '.date("H:i",(1262329200+$row['arr_time'])));
			$row['dep_time'] = ($row['arr_time']-$row['travel_time']+tz_offset($row['arr_loc'],$row['dep_loc'],($datestr.' '.date("H:i",$row['arr_time']-$row['travel_time']))));
			} else {
			$row['dep_time'] = strtotime($datestr. ' '.date("H:i",(1262329200+$row['dep_time'])));
			//$row['arr_time'] = ($row['dep_time']+$row['travel_time']+tz_offset($row['dep_loc'],$row['arr_loc'],($datestr.' '.date("H:i",$row['dep_time']+$row['travel_time']))));
			}
			$row['time'] = $row['dep_time'];
		$row['booked'] = 0;
		$day = date("j",$row['date']);
		if(!isset( $routes_dates[$day] )){ $routes_dates[$day] = array(); }
		array_push($routes_dates[$day],$row);
		}
		//echo '</CENTER><PRE>'; print_r($routes_dates); echo '</PRE>';

//GET BOOKED
if(false && $_SERVER['REMOTE_ADDR'] == '76.103.138.139') {
	include_once('../getblocked3.php');
} else {
	include_once('../getblocked.php');
}
	$routes_booked = getblocked('*',$calendar['first'],mktime(0,0,0,date("n",$calendar['first']),$calendar['days'],date("Y",$calendar['first'])));
	//echo '</CENTER><PRE>'; print_r($routes_booked); echo '</PRE>';

$calw = (7*98);

echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="calendar" WIDTH="'.$calw.'" STYLE="border:1px solid #7F9DB9;">
<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px; background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">
	<TD ALIGN="left" STYLE="padding-right:8px;"><INPUT TYPE="button" STYLE="font-size:12pt;" VALUE="&lt;&lt;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?y='.date("Y",$calendar['prevm']).'&m='.date("n",$calendar['prevm']).'\'"></TD>
	<TD ALIGN="center"><SELECT STYLE="font-size:12pt;" ID="choose_month" onChange="window.location=\''.$_SERVER['PHP_SELF'].'?y=\'+document.getElementById(\'choose_year\').value+\'&m=\'+document.getElementById(\'choose_month\').value;">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['m'] == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> <SELECT STYLE="font-size:12pt;" ID="choose_year" onChange="window.location=\''.$_SERVER['PHP_SELF'].'?y=\'+document.getElementById(\'choose_year\').value+\'&m=\'+document.getElementById(\'choose_month\').value;">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['y'] == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></TD>
	<TD ALIGN="right" STYLE="padding-left:8px;"><INPUT TYPE="button" STYLE="font-size:12pt;" VALUE="&gt;&gt;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?y='.date("Y",$calendar['nextm']).'&m='.date("n",$calendar['nextm']).'\'"></TD>
	</TABLE>
	</TD></TR>'."\n\n";


//VIEW AS LIST ******************************************************
if($_SESSION['routecalendar']['view'] == "list"){
	echo '</TABLE><BR>'."\n\n";


echo '<TABLE BORDER="0" WIDTH="'.$calw.'" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($routes_dates as $day => $date){

	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD ALIGN="left" COLSPAN="2" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">'.date("l, F j, Y",$date[0]['date']).'</TD>';
		echo '<TD ALIGN="right" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:10px;">Reserved</TD>';
		echo '<TD ALIGN="right" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:10px;">Capacity</TD>';
		echo '</TR>'."\n";
	bgcolor('reset');
	foreach($date as $row){
		if(isset($routes_booked['r'.$row['routeid']]['d'.$row['date']])){ $row['booked'] = ($routes_booked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$routes_booked['r'.$row['routeid']]['d'.$row['date']]['route_seats']); }
		echo '<TR BGCOLOR="#'.bgcolor('').'">';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt; padding-left:10px;">'.date("g:ia",$row['time']).'</TD>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt;"><A HREF="3_routes.php?edit='.$row['routeid'].'">'.$row['name'].'</A></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; padding-right:10px;';
			if($row['booked'] > 0){
				echo ' font-weight:bold;';
				} else {
				echo ' color:#C7C7C7;';
				}
			echo '">'.$row['booked'].'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; padding-right:10px;">'.$row['seats'].'</TD>';
		echo '</TR>'."\n";
		}
	} //End For Each
	echo '</TABLE><BR>'."\n\n";


//VIEW AS CALENDAR ******************************************************
} else {

echo '<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Sun</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Mon</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Tue</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Wed</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Thu</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Fri</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; border-top:1px solid #999999;">Sat</TD>
	</TR>'."\n\n";

echo '<TR BGCOLOR="#'.bgcolor('DDDDDD').'">';

	$day = 0;
	$col = $calendar['start'];

	if($calendar['start'] > 0): echo '<TD COLSPAN="'.$calendar['start'].'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#FFFFFF; border-top:1px solid #999999; border-right:1px solid #999999;">&nbsp;</TD>'; endif;

	for($i=0; $day<$calendar['days']; $i++){
	$col++;
	++$day;
	echo '	<TD ALIGN="left" VALIGN="top" ID="d'.$day.'" STYLE="padding:0px; width:'.ceil($calw/7).'px; height:'.ceil($calw/7).'px; font-family:Arial; font-size:10px; border-top:1px solid #999999;';
		if($col < 7){ echo ' border-right:1px solid #999999;'; }
		echo '">';
		echo '<DIV STYLE="padding:4px; font-size:11pt; font-weight:bold;">'.$day.'</DIV>';
		if(isset($routes_dates[$day]) && count($routes_dates[$day]) > 0){
			foreach($routes_dates[$day] as $row){
				if(isset($routes_booked['r'.$row['routeid']]['d'.$row['date']])){ $row['booked'] = ($routes_booked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$routes_booked['r'.$row['routeid']]['d'.$row['date']]['route_seats']); }
				echo '<DIV STYLE="padding:4px; padding-top:5px; padding-bottom:5px; border-top:1px solid #C7C7C7;';
					if($row['booked'] > 0 && $row['booked'] <= $row['seats']){
						echo ' background:#FFFFFF url(\'img/fade_r_blue.jpg\') center center repeat-y;';
						} elseif($row['booked'] > $row['seats']) {
						echo ' background:#FFFFFF url(\'img/fade_r_red.jpg\') center center repeat-y;';
						}
					echo '">';
				echo date("g:ia",$row['time']).'<BR>';
				echo '<A HREF="3_routes.php?edit='.$row['routeid'].'">'.$row['name'].'</A><BR>';
				echo '<SPAN STYLE="color:#757575; vertical-align:bottom; white-space:nowrap;">Reserved: ';
					if($row['booked'] > 0){ echo '<SPAN STYLE="color:#0000FF; font-weight:bold;">'; }
					echo $row['booked'].' / '.$row['seats'].'</SPAN>';
					if($row['booked'] > 0){ echo '</SPAN>'; }
				echo '</DIV>';
				}
			}
		echo '</TD>'."\n";
	if($col == 7){
		echo '</TR>'."\n".'<TR BGCOLOR="#'.bgcolor('').'">';
		$col = 0;
		}
	}

	if($col > 0 && $col < 7): echo '<TD COLSPAN="'.(7 - $col).'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#FFFFFF; border-top:1px solid #999999;">&nbsp;</TD>'."\n"; endif;

	echo '</TR></TABLE>'."\n\n";

}

echo '</FORM>'."\n\n";


require("footer.php");

?>