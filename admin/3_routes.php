<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 189;  echo 'REMOVE THIS LINE!!!';

$working = 0;
$basetime = strtotime('2010-1-1');

$pageid = "3_routes";
require("validate.php");
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""): $onload = array('checkend()'); endif;
require("header.php");

if(!isset($_SESSION['routes']['p'])): $_SESSION['routes']['p'] = 1; endif;
if(!isset($_SESSION['routes']['sortby'])): $_SESSION['routes']['sortby'] = "routes.`name`"; endif;
	$sortby = array('routes.`id`'=>'ID','routes.`name`'=>'Name','dep_name'=>'Departure location','routes.`dep_time`'=>'Departure time','arr_name'=>'Arrival location','routes.`arr_time`'=>'Arrival time','upcoming'=>'# of future dates','routes.`bbincl`'=>'Bundu Bus/Google Transit');
if(!isset($_SESSION['routes']['sortdir'])): $_SESSION['routes']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['routes']['showhidden'])): $_SESSION['routes']['showhidden'] = "0"; endif;
if(!isset($_SESSION['routes']['limit'])): $_SESSION['routes']['limit'] = "200"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['routes']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['routes']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['routes']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['showhidden']) && $_REQUEST['showhidden'] != ""): $_SESSION['routes']['showhidden'] = $_REQUEST['showhidden']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['routes']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	if(!isset($_REQUEST['showhidden'])): $_SESSION['routes']['showhidden'] = '0'; endif;
	$_SESSION['routes']['p'] = '1';
	}

//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';


//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$_POST['dep_time'] = ($_POST['dep_time_hour']*3600)+($_POST['dep_time_mins']*60);
	$_POST['travel_time'] = ($_POST['travel_hrs']*3600)+($_POST['travel_mins']*60);
	$_POST['arr_time'] = ($_POST['arr_time_hour']*3600)+($_POST['arr_time_mins']*60);

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `routes`(`name`,`seats`,`minimum`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`travel_time`,`anchor`,`miles`,`details`,`bbprice`,`vendor`,`bbincl`,`hidden`)';
		$query .= ' VALUES("'.mysql_real_escape_string($_POST['name']).'","'.mysql_real_escape_string($_POST['seats']).'","'.mysql_real_escape_string($_POST['minimum']).'","'.mysql_real_escape_string($_POST['dep_loc']).'","'.mysql_real_escape_string($_POST['dep_time']).'","'.mysql_real_escape_string($_POST['arr_loc']).'","'.mysql_real_escape_string($_POST['arr_time']).'","'.mysql_real_escape_string($_POST['travel_time']).'","'.mysql_real_escape_string($_POST['anchor']).'","'.mysql_real_escape_string($_POST['miles']).'","'.mysql_real_escape_string($_POST['details']).'","'.mysql_real_escape_string($_POST['bbprice']).'","'.mysql_real_escape_string($_POST['vendor']).'","'.mysql_real_escape_string($_POST['bbincl']).'","'.mysql_real_escape_string($_POST['hidden']).'")';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new route "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	} else {
	//UPDATE
	$query = 'UPDATE `routes` SET `name` = "'.mysql_real_escape_string($_POST['name']).'", `seats` = "'.mysql_real_escape_string($_POST['seats']).'", `minimum` = "'.mysql_real_escape_string($_POST['minimum']).'", `dep_loc` = "'.mysql_real_escape_string($_POST['dep_loc']).'", `dep_time` = "'.mysql_real_escape_string($_POST['dep_time']).'", `arr_loc` = "'.mysql_real_escape_string($_POST['arr_loc']).'", `arr_time` = "'.mysql_real_escape_string($_POST['arr_time']).'", `travel_time` = "'.mysql_real_escape_string($_POST['travel_time']).'", `anchor` = "'.mysql_real_escape_string($_POST['anchor']).'", `miles` = "'.mysql_real_escape_string($_POST['miles']).'", `details` = "'.mysql_real_escape_string($_POST['details']).'", `bbprice` = "'.mysql_real_escape_string($_POST['bbprice']).'", `vendor` = "'.mysql_real_escape_string($_POST['vendor']).'", `bbincl` = "'.mysql_real_escape_string($_POST['bbincl']).'", `hidden` = "'.mysql_real_escape_string($_POST['hidden']).'"';
		$query .= ' WHERE `id` = "'.mysql_real_escape_string($_POST['edit']).'" LIMIT 1';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror == ""): array_push($successmsg,'Saved route "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}

	//UPDATE DATES
	require_once('sups/dates.php');
	require_once(doc_root.'/getblocked.php');

	$to_add = array(); $to_rmv = array(); $appdates = array();
	foreach($_POST['runs'] as $run){
		$run = explode('|',$run);
		$date = mktime(0,0,0,$run[1],$run[2],$run[0]);
		if($run[3] == 0){
			//$blocked = getblocked($_REQUEST['edit'],$date,$date);
			//echo '<PRE STYLE="text-align:left;">'; print_r($blocked); echo '</PRE>';
			$usedby = route_usedby($_REQUEST['edit'],$date,$date);
			//echo '<PRE STYLE="text-align:left;">'; print_r($usedby); echo '</PRE>';
			if(!isset($usedby['d'.$date]) || count($usedby['d'.$date]) == 0){
				array_push($to_rmv,$date);
			} else {
				$appdates = array_merge($appdates,$usedby);
			}
		} else {
			array_push($to_add,$date);
		}
	} //End Foreach

	$result = rmv_route_dates($_REQUEST['edit'],$to_rmv);
	$successmsg = array_merge($successmsg,$result['success']);
	$errormsg = array_merge($errormsg,$result['error']);

	$result = add_route_dates($_REQUEST['edit'],$to_add);
	//echo '<PRE>'; print_r($result); echo '</PRE>';
	$successmsg = array_merge($successmsg,$result['success']);
	$errormsg = array_merge($errormsg,$result['error']);

	//!ITINERARY STEPS/OPTIONS -------------------------
	/*@mysqlQuery('DELETE FROM `routes_assoc` WHERE `routeid` = "'.$_REQUEST['edit'].'"');
	if(isset($_POST['it_ids'])){
	foreach($_POST['it_ids'] as $key => $id){ if($id != ""){
		$query = 'INSERT INTO `routes_assoc`(`routeid`,`itid`,`order`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['it_ids'][$key].'","'.$key.'")';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""){ array_push($errormsg,$thiserror); }
		}} //End foreach
		} //End if statement*/

	//UPDATE ITINERARY NICKNAMES FOR TRANSLATION TOOL -------------------------
	$query = 'UPDATE `itinerary` SET `nickname` = CONCAT((SELECT `name` FROM `routes` WHERE `id` = "'.mysql_real_escape_string($_REQUEST['edit']).'")," - Step",itinerary.`step`), `modified` = itinerary.`modified` WHERE `routeid` = "'.mysql_real_escape_string($_REQUEST['edit']).'"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""){ array_push($errormsg,$thiserror); }

	//UPDATE UNUSED TIME FIELDS FOR SORTING
	$query = 'UPDATE `routes` SET `arr_time` = (`dep_time`+`travel_time`) WHERE `anchor` = "dep"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""){ array_push($errormsg,$thiserror); }
	$query = 'UPDATE `routes` SET `dep_time` = (`arr_time`-`travel_time`) WHERE `anchor` = "arr"';
		@mysqlQuery($query);
		$thiserror = $GLOBALS['mysql_error'];
		if($thiserror != ""){ array_push($errormsg,$thiserror); }


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "copy" && isset($_POST['edit']) && $_POST['edit'] != ""){


	$old = array();
	//COPY BASIC INFO
	if(isset($_POST['basic']) && $_POST['basic'] == "y"){
		//GET OLD INFO
		$query = 'SELECT * FROM `routes` WHERE `id` = "'.mysql_real_escape_string($_POST['edit']).'" LIMIT 1';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		$old = @mysql_fetch_assoc($result);
		unset($old['id']);
		$old['name'] = 'COPY: '.$old['name'];
		}
	$old_keys = array();
	$old_values = array();
	foreach($old as $key => $value){
		array_push($old_keys,$key);
		array_push($old_values,str_replace('"','\"',$value));
		}
	$query = 'INSERT INTO `routes`(';
		if(count($old_keys) > 0): $query .= '`'.implode('`,`',$old_keys).'`'; endif;
		$query .= ') VALUES(';
		if(count($old_values) > 0): $query .= '"'.implode('","',$old_values).'"'; endif;
		$query .= ')';
		@mysqlQuery($query);
		$newid = mysql_insert_id();
		$thiserror = $GLOBALS['mysql_error'];

	if($thiserror != ""){
		array_push($errormsg,$thiserror);
	} else {
		array_push($successmsg,'Saved new route "'.$old['name'].'" ('.$newid.').');

	//COPY DATES
		if(isset($_POST['dates']) && $_POST['dates'] == "y"){
		$numsaved = 0;
		$query = 'INSERT INTO `routes_dates`(`routeid`,`date`,`dep_time`,`arr_time`,`seats`,`minimum`,`notes`) SELECT "'.mysql_real_escape_string($newid).'" AS `routeid`,`date`,`dep_time`,`arr_time`,`seats`,`minimum`,`notes` FROM `routes_dates` WHERE `routeid` = "'.mysql_real_escape_string($_POST['edit']).'"';
			@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror == ""){ $numsaved=($numsaved+mysql_affected_rows()); }
			if($numsaved > 0): array_push($successmsg,$numsaved.' dates were copied.'); endif;
		} //End dates if statement

	//COPY ITINERARY
		if(isset($_POST['itinerary']) && $_POST['itinerary'] == "y"){
		$numsaved = 0;
		//$query = 'INSERT INTO `routes_assoc`(`routeid`,`itid`,`order`) SELECT "'.$newid.'" AS `routeid`,`itid`,`order` FROM `routes_assoc` WHERE `routeid` = "'.$_POST['edit'].'"';
		//	@mysqlQuery($query);
		//	$thiserror = $GLOBALS['mysql_error'];
		//	if($thiserror == ""){ $numsaved=($numsaved+mysql_affected_rows()); }
		$numimgscopied = 0;
		$numtranscopied = 0;
		$old = array();
		$imgs = array();
		$query = 'SELECT * FROM `itinerary` WHERE `routeid` = "'.mysql_real_escape_string($_POST['edit']).'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			$row['routeid'] = $newid;
			array_push($old,$row);
			} //End for loop
		foreach($old as $row){
			//Get images
			$imgs = array();
				$query = 'SELECT * FROM `images_assoc_itinerary` WHERE `itid` = "'.mysql_real_escape_string($row['id']).'"';
				$result = @mysqlQuery($query);
				$num_results = @mysql_num_rows($result);
				for($i=0; $i<$num_results; $i++){
					$img = @mysql_fetch_assoc($result);
					array_push($imgs,$img);
					} //End for loop
			$old_keys = array();
			$old_values = array();
			$olditid = $row['id'];
			unset($row['id']);
			foreach($row as $key => $value){
				array_push($old_keys,$key);
				array_push($old_values,str_replace('"','\"',$value));
				}
			$query = 'INSERT INTO `itinerary`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
				@mysqlQuery($query);
				$newitid = mysql_insert_id();
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""){
					$numsaved=($numsaved+mysql_affected_rows());

					//Copy images
					foreach($imgs as $img){
					$old_keys = array();
					$old_values = array();
					$img['itid'] = $newitid;
					foreach($img as $key => $value){
						array_push($old_keys,$key);
						array_push($old_values,str_replace('"','\"',$value));
						}
					$query = 'INSERT INTO `images_assoc_itinerary`(`'.implode('`,`',$old_keys).'`) VALUES("'.implode('","',$old_values).'")';
						@mysqlQuery($query);
						$thiserror = $GLOBALS['mysql_error'];
						if($thiserror == ""): $numimgscopied=($numimgscopied+mysql_affected_rows()); else: array_push($errormsg,'Unable to copy image: '.$thiserror); endif;
						} //End foreach

					//Copy translations
					if(isset($_POST['translations']) && $_POST['translations'] == "y"){
					$query = 'INSERT INTO `itinerary_translations`(`itid`,`lang`,`title`,`time`,`comments`,`modified`) SELECT "'.mysql_real_escape_string($newitid).'" AS `itid`, itinerary_translations.`lang`, itinerary_translations.`title`, itinerary_translations.`time`, itinerary_translations.`comments`, itinerary_translations.`modified` FROM `itinerary`,`itinerary_translations` WHERE itinerary.`id` = "'.mysql_real_escape_string($olditid).'" AND itinerary.`id` = itinerary_translations.`itid`';
						@mysqlQuery($query);
						$thiserror = $GLOBALS['mysql_error'];
						if($thiserror == ""): $numtranscopied=($numtranscopied+mysql_affected_rows()); else: array_push($errormsg,'Unable to copy translation: '.$thiserror); endif;
						}

					} else {
					array_push($errormsg,$thiserror);
					}
			} //End foreach

		//!UPDATE ROUTES_ASSOC -------------------------
		@mysqlQuery('DELETE FROM `routes_assoc` WHERE `routeid` = "'.mysql_real_escape_string($newid).'"');
		$query = 'INSERT INTO `routes_assoc`(`routeid`,`itid`,`order`) SELECT `routeid`, `id` AS `itid`, `step` AS `order` FROM `itinerary` WHERE `routeid` = "'.mysql_real_escape_string($newid).'"';
			@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror != ""){ array_push($errormsg,$thiserror); }

		if($numimgscopied > 0): array_push($successmsg,$numimgscopied.' itinerary images were copied.'); endif;
		if($numtranscopied > 0): array_push($successmsg,$numtranscopied.' itinerary translations were copied.'); endif;
		if($numsaved > 0): array_push($successmsg,$numsaved.' itinerary entries were copied.'); endif;
		} //End itinerary if statement

	$_REQUEST['edit'] = $newid;
	} //End initial new route error if statement


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "dates" && isset($_POST['edit']) && $_POST['edit'] != ""){


	//!REMOVE APPROVED DATES
	require_once('sups/dates.php');
	require_once(doc_root.'/getblocked.php');
	//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';
	if(isset($_POST['submit']) && $_POST['submit'] == "Approve selected"){ $go = false; } else { $go = true; }

	//Remove approved tour dates
	$numremoved = 0;
	$tours_were_using = array();
	$routes_clear = array();
	$tour_dates_removed = array();
	if(isset($_POST['del_routedate'])){
		foreach($_POST['del_routedate'] as $key => $date){
			if(!isset($routes_clear[$_POST['del_routedate'][$key]])){
				$routes_clear[$_POST['del_routedate'][$key]] = array();
			}
			if($go || $_POST['choice_'.$date] == "y"){
				if(!isset($tours_were_using[$_POST['del_tourid'][$key]])){
					$tours_were_using[$_POST['del_tourid'][$key]] = array();
				}

				$id_tour = $_POST['del_tourid'][$key];
				$tour_date = $_POST['del_tourdate'][$key];

				$tour_was_using = tour_using($id_tour, $tour_date, $tour_date);
				if(count($tour_was_using) > 0){
					$tours_were_using[$id_tour] = array_merge($tours_were_using[$id_tour], $tour_was_using);
				}

				$result = rmv_tour_dates($id_tour, $tour_date);

				if(!isset($tour_dates_removed[$id_tour])) {
					$tour_dates_removed[$id_tour] = array();
				}
				$tour_dates_removed[$id_tour][] = mysqlDate($tour_date);

				$numremoved = ($numremoved + $result['affected']);
				//$successmsg = array_merge($successmsg,$result['success']);
				if(count($result['error']) > 0){
					array_push($routes_clear[$_POST['del_routedate'][$key]],1);
					$errormsg = array_merge($errormsg,$result['error']);
				} else {
					array_push($routes_clear[$_POST['del_routedate'][$key]],0);
				}
			}
		}
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($tours_were_using); echo '</PRE>';
	//echo '<PRE STYLE="text-align:left;">'; print_r($routes_clear); echo '</PRE>';
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($tour_dates_removed,true)).'</PRE>';
	if($numremoved > 0) {
		$successmsg[] = $numremoved.' tour dates were removed.';

		//Create Closeout Speadsheet
		$spreadsheetObj = new closeout_spreadsheet();
		$spreadsheetObj->setData($tour_dates_removed);
		$spreadsheetObj->save();
		$successmsg[] = 'Generated close out spreadsheet: <a href="3_tour_closeout_spreadsheets.php?command=download&id='.$spreadsheetObj->id.'">Click here to download CSV file</a>';
	}

	//Remove approved and cleared route dates
	$numremoved = 0;
	if(isset($_POST['del_routedate'])){
		foreach($_POST['del_routedate'] as $key => $date){
			if(isset($routes_clear[$date]) && count($routes_clear[$date]) > 0 && array_sum($routes_clear[$date]) === 0) {
				$result = rmv_route_dates($_REQUEST['edit'],$date);
				$numremoved = ($numremoved + $result['affected']);
				$errormsg = array_merge($errormsg,$result['error']);
			}
		}
	}
	if($numremoved > 0){ array_push($successmsg,$numremoved.' route dates were removed.'); }

	//Any unused route dates?
	$unusedroutes = false;
	$routes_todel = array();
	foreach($tours_were_using as $tourid => $tour_was_using) {
		foreach($tour_was_using as $key1 => $date) {
			foreach($date as $key2 => $route) {
				if($route['routeid'] != $_REQUEST['edit'] && !isset($routes_todel[$route['routeid'].'_'.$route['route_date']])) {
					$routes_todel[$route['routeid'].'_'.$route['route_date']] = array();
					$blocked = getblocked($route['routeid'],$route['route_date'],$route['route_date']);
					//echo '<PRE STYLE="text-align:left;">getblocked output:'; print_r($blocked); echo '</PRE>';
					$used = route_usedby($route['routeid'],$route['route_date'],$route['route_date']);
					$check1 = (count($blocked) == 0);
					$check2 = (!isset($used['d'.$route['route_date']]) || count($used['d'.$route['route_date']]) == 0);
					if($check1 && $check2) {
						$routes_todel[$route['routeid'].'_'.$route['route_date']] = array('id'=>$route['routeid'],'date'=>$route['route_date']);
						$unusedroutes = true;
					}
				}
			}
		}
	}

	if(!$unusedroutes && isset($_POST['followup']) && $_POST['followup'] == "delete") {
		$_POST['utaction'] = "delete";
	}


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "rmvroutedates" && isset($_POST['del_routeid']) && is_array($_POST['del_routeid']) && count($_POST['del_routeid']) > 0){


	//!REMOVE APPROVED ROUTE DATES
	@include_once('sups/dates.php');
	//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';
	if(isset($_POST['submit']) && $_POST['submit'] == "Approve selected"){ $go = false; } else { $go = true; }

	$numremoved = 0;
	foreach($_POST['del_routedate'] as $key => $date){
		if($go || $_POST['choice_'.$_POST['del_routeid'][$key].'_'.$date] == "y"){
		$result = rmv_route_dates($_POST['del_routeid'][$key],$_POST['del_routedate'][$key]);
			$numremoved = ($numremoved + $result['affected']);
			//$successmsg = array_merge($successmsg,$result['success']);
			$errormsg = array_merge($errormsg,$result['error']);
			}
		}
	if($numremoved > 0){ array_push($successmsg,$numremoved.' unused route dates were removed.'); }

	if(isset($_POST['followup']) && $_POST['followup'] == "delete"){
		$_POST['utaction'] = "delete";
		}


} //End actions if statements
if(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//!DELETE ROUTE
	require_once('sups/dates.php');
	require_once(doc_root.'/getblocked.php');

	//Remove dates first
	$to_rmv = array(); $appdates = array();
	$query = 'SELECT `date` FROM `routes_dates` WHERE `routeid` = "'.mysql_real_escape_string($_POST['edit']).'"';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = @mysql_fetch_assoc($result);
			//array_push($to_rmv,$row['date']);
			$blocked = getblocked($_POST['edit'],$row['date'],$row['date']);
			//echo '<PRE STYLE="text-align:left;">getblocked output:'; print_r($blocked); echo '</PRE>';
			$usedby = route_usedby($_POST['edit'],$row['date'],$row['date']);
			//echo '<PRE STYLE="text-align:left;">route_usedby output:'; print_r($usedby); echo '</PRE>';
			$check1 = (count($blocked) == 0);
			$check2 = (!isset($usedby['d'.$row['date']]) || count($usedby['d'.$row['date']]) == 0);
			if($check1 && $check2){
				array_push($to_rmv,$row['date']);
				} elseif(!$check1){
				array_push($errormsg,'One or more dates have been reserved.  This route cannot be deleted.');
				unset($appdates);
				break;
				} else {
				$appdates = array_merge($appdates,$usedby);
				}

			}

	if(count($errormsg) == 0){
		$result = rmv_route_dates($_POST['edit'],$to_rmv,true);
		//echo '<PRE>'; print_r($result); echo '</PRE>';
			$successmsg = array_merge($successmsg,$result['success']);
			$errormsg = array_merge($errormsg,$result['error']);
		if(count($result['error']) > 0){
			array_push($errormsg,'Unable to delete route.');
			} elseif(count($appdates) == 0){
	
			//Delete from tours_assoc
			@mysqlQuery('DELETE FROM `tours_assoc` WHERE `type` = "r" AND `typeid` = "'.mysql_real_escape_string($_POST['edit']).'"');
	
			//Delete Itinerary Images
			$itids = array();
			$query = 'SELECT DISTINCT images_assoc_itinerary.`itid` FROM `images_assoc_itinerary`,`itinerary` WHERE images_assoc_itinerary.`itid` = itinerary.`id` AND itinerary.`routeid` = "'.mysql_real_escape_string($_POST['edit']).'"';
			$result = @mysqlQuery($query);
			$num_results = @mysql_num_rows($result);
				for($i=0; $i<$num_results; $i++){
				$row = mysql_fetch_assoc($result);
				array_push($itids,$row['itid']);
				}
			$query = 'DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.implode('" OR `itid` = "',$itids).'"';
				@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' itinerary images were deleted.'); else: array_push($errormsg,$thiserror); endif;
		
			//Delete Itinerary Entries
			$query = 'DELETE FROM `itinerary` WHERE `routeid` = "'.mysql_real_escape_string($_POST['edit']).'"';
				@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' itinerary entries were deleted.'); else: array_push($errormsg,$thiserror); endif;
			@mysqlQuery('DELETE FROM `routes_assoc` WHERE `routeid` = "'.mysql_real_escape_string($_POST['edit']).'"');
	
			//Delete Routes
			$query = 'DELETE FROM `routes` WHERE `id` = "'.mysql_real_escape_string($_POST['edit']).'"';
				@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror == ""){
				array_push($successmsg,'Route '.$_POST['edit'].' has been deleted.');
				unset($_REQUEST['edit']);
				} else {
				array_push($errormsg,$thiserror);
				}
	
			}
		}

} //End Delete if statement



echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Routes</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}


if(isset($_POST['utaction']) && $_POST['utaction'] == "dates" && $unusedroutes && count($routes_todel) > 0){
//!DELETED TOUR DATES - APPROVE UNUSED ROUTE DATE REMOVAL **************************************************************//


if(!isset($_POST['edit'])){ $_POST['edit'] = ''; }

echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">Would you like to remove these unused route dates?</SPAN><BR><BR>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="rmvroutedates">'."\n\n";
if(isset($_POST['followup'])){
	echo '<INPUT TYPE="hidden" NAME="followup" VALUE="'.$_POST['followup'].'">'."\n";
	}
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_POST['edit'].'">'."\n\n";

//echo '<PRE STYLE="text-align:left;">'; print_r($routes_todel); echo '</PRE>';


//GET ROUTE NAMES
$routeids = array();
foreach($routes_todel as $route){
	if(isset($route['id']) && $route['id'] != ""){
		array_push($routeids,$route['id']);
		}
	}
	$routeids = array_unique($routeids);
$routenames = array();
	$query = 'SELECT DISTINCT `id`,`name` FROM `routes` WHERE `id` = "'.implode('" OR `id` = "',$routeids).'" ORDER BY `id` ASC';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$routenames[$row['id']] = $row['name'];
		}
	//echo '<PRE>'; print_r($routenames); echo '</PRE>';

echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";


//Print out dates for "Approve all listed"
$choicecount = 0;
foreach($routes_todel as $route){
	if(isset($route['id']) && $route['id'] != ""){
		$choicecount++;
		echo '<INPUT TYPE="hidden" NAME="del_routeid[]" VALUE="'.$route['id'].'"><INPUT TYPE="hidden" NAME="del_routedate[]" VALUE="'.$route['date'].'">'."\n";
		}
	}
	echo "\n";


//Print out dates for approval
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($routes_todel as $route){ if(isset($route['id']) && $route['id'] != ""){

	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">Remove unused date '.date("F j, Y",$route['date']).' for "'.$routenames[$route['id']].'"</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$route['id'].'_'.$route['date'].'" ID="choice_'.$route['id'].'_'.$route['date'].'_y" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:11pt; cursor:pointer;" onClick="document.getElementById(\'choice_'.$route['id'].'_'.$route['date'].'_y\').checked=1;';
		echo '"><B>Yes, remove this unused route date.</B>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px; border-bottom:5px solid #FFFFFF;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$route['id'].'_'.$route['date'].'" ID="choice_'.$route['id'].'_'.$route['date'].'_n" VALUE="n"></TD><TD STYLE="font-family:Arial; font-size:11pt; border-bottom:5px solid #FFFFFF; cursor:pointer;" onClick="document.getElementById(\'choice_'.$route['id'].'_'.$route['date'].'_n\').checked=1;">No, do not remove this route date.</TD></TR>'."\n\n";

	}} //End For Each
	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";

?><SCRIPT><!--
function checkend(){ }
//--></SCRIPT><? echo "\n\n";


} elseif(isset($_POST['utaction']) && isset($_POST['edit']) && $_POST['edit'] != "" && isset($appdates) && count($appdates) > 0){
//!UNAPPROVED DATES PAGE **************************************************************//


echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">Some changes to dates need your approval...</SPAN><BR><BR>'."\n\n";

echo '<FORM METHOD="post" NAME="editform" ID="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="dates">'."\n";
if($_POST['utaction'] == "delete"){
	echo '<INPUT TYPE="hidden" NAME="followup" VALUE="delete">'."\n";
	}
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_POST['edit'].'">'."\n\n";

//GET TOUR NAMES
$tourids = array();
foreach($appdates as $thisdate){
	foreach($thisdate as $tour){
		array_push($tourids,$tour['tourid']);
		}
	}
	$tourids = array_unique($tourids);
$tournames = array();
	$query = 'SELECT DISTINCT `id`,`title` FROM `tours` WHERE `id` = "'.implode('" OR `id` = "',$tourids).'" ORDER BY `id` ASC';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(strlen($row['title']) > 60){ $row['title'] = substr($row['title'],0,29).' ... '.substr($row['title'],-29); }
		$tournames[$row['id']] = $row['title'];
		}

echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['edit'].'\'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";

@include_once('sups/dates.php');
//echo '<DIV STYLE="text-align:left;"><PRE>'; print_r($appdates); echo '</PRE></DIV>';

//Print out dates for "Approve all listed"
foreach($appdates as $thisdate){
	foreach($thisdate as $tour){
		echo '<INPUT TYPE="hidden" NAME="del_routedate[]" VALUE="'.$tour['route_date'].'"><INPUT TYPE="hidden" NAME="del_tourdate[]" VALUE="'.$tour['tour_date'].'"><INPUT TYPE="hidden" NAME="del_tourid[]" VALUE="'.$tour['tourid'].'">'."\n";
		}
	}
	echo "\n";

//Print out dates for approval
echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n\n";
foreach($appdates as $thisdate){

	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-left:10px;">In order to remove this route on '.date("F j, Y",$thisdate[0]['route_date']).':</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['route_date'].'" ID="choice_'.$thisdate[0]['route_date'].'_y" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:11pt; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['route_date'].'_y\').checked=1;';
		if(count($appdates) > 5 && count($thisdate) > 4){ echo ' document.getElementById(\'detlink_'.$thisdate[0]['route_date'].'\').style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['route_date'].'\').style.display=\'\';'; }
		echo '"><B>'.count($thisdate).' tour dates</B> will also need to be removed.';
		if(count($appdates) > 5 && count($thisdate) > 4){ echo '<DIV ID="detlink_'.$thisdate[0]['route_date'].'" onClick="this.style.display=\'none\'; document.getElementById(\'details_'.$thisdate[0]['route_date'].'\').style.display=\'\';" STYLE="padding-left:20px; color:#000099;">Click here to see details...</DIV>'; }
		echo '<DIV ID="details_'.$thisdate[0]['route_date'].'" STYLE="font-size:9pt;';
			if(count($appdates) > 5 && count($thisdate) > 4){ echo ' display:none;'; }
			echo '">';
		foreach($thisdate as $tour){
			echo '<LI STYLE="margin-left:16px;">Remove '.date("D, n/j/Y",$tour['tour_date']).' from tour ['.$tour['tourid'].'] "'.$tournames[$tour['tourid']].'"<BR>';
			}
		echo '</DIV>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="width:20px; padding:6px; border-bottom:5px solid #FFFFFF;" ALIGN="center" VALIGN="top"><INPUT TYPE="radio" NAME="choice_'.$thisdate[0]['route_date'].'" ID="choice_'.$thisdate[0]['route_date'].'_n" VALUE="n"></TD><TD STYLE="font-family:Arial; font-size:11pt; border-bottom:5px solid #FFFFFF; cursor:pointer;" onClick="document.getElementById(\'choice_'.$thisdate[0]['route_date'].'_n\').checked=1;">Do not remove this route date and these tour dates.</TD></TR>'."\n\n";

	} //End For Each
	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" NAME="submit" VALUE="**Approve all listed**" STYLE="width:200px; color:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="Approve none listed" STYLE="color:red;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit='.$_REQUEST['edit'].'\'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" NAME="submit" VALUE="Approve selected"><BR><BR>'."\n\n";


?><SCRIPT><!--
function checkend(){ }
//--></SCRIPT><? echo "\n\n";


//!EDIT ROUTE PAGE
} elseif(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

//GET BUNDUBUS DEFAULTS
$defaults = array();
$query = 'SELECT `setting`,`var` FROM `bundubus_settings`';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$defaults[$row['setting']] = $row['var'];
	}

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*',
		'seats' => $defaults['seats'],
		'minimum' => '2',
		'dep_loc' => '1',
		'arr_loc' => '1',
		'vendor' => '1',
		'hidden' => '0'
		);
	} else {
	$query = 'SELECT routes.*';
		$query .= ', COUNT(routes_assoc.`id`) AS `itcount`';
		//$query .= ', COUNT(itinerary.`id`) AS `itcount`';
		$query .= ' FROM `routes`';
		$query .= ' LEFT JOIN `routes_assoc` ON routes.`id` = routes_assoc.`routeid`';
		//$query .= ' LEFT JOIN `itinerary` ON routes.`id` = itinerary.`routeid`';
		$query .= ' WHERE routes.`id` = "'.$_REQUEST['edit'].'"';
		$query .= ' GROUP BY routes.`id` LIMIT 1';
		//echo $query;
	$result = mysqlQuery($query);
	//echo $GLOBALS['mysql_error'];
	$fillform = mysql_fetch_assoc($result);
	$fillform['kilometres'] = number_format(($fillform['miles']*1.609), 2, '.', '');
	}

//GET TIME OFFSET FUNCTION
include_once('sups/timezones.php');
$basetime = strtotime('2010-1-1');

function fromsecs($secs=0){
	$out = array('h'=>0,'m'=>0,'s'=>0);
	$out['h'] = intval($secs / 3600);
		$secs_left = ($secs - ($out['h'] * 3600));
	$out['m'] = intval($secs_left / 60);
	$out['s'] = ($secs_left - ($out['m'] * 60));
	return $out;
	}

//GET RUNS
$routes_dates = array();
$query = 'SELECT * FROM `routes_dates` WHERE `routeid` = "'.getval('id').'" ORDER BY `date` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($routes_dates[date("Y",$row['date'])])){ $routes_dates[date("Y",$row['date'])] = array(); }
	if(!isset($routes_dates[date("Y",$row['date'])][date("n",$row['date'])])){ $routes_dates[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
	$routes_dates[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = 1;
	//array_push($routes_dates,$row);
	}
	//echo '<PRE>Runs'; print_r($routes_dates); echo '</PRE>';

//GET BLOCKED
$routes_blocked = array();
include_once('../getblocked.php');
	$blocked = getblocked(getval('id'),'*','*');
	//echo '<PRE STYLE="text-align:left;">'; print_r($blocked); echo '</PRE>';
	if(isset($blocked['r'.getval('id')])){
	foreach($blocked['r'.getval('id')] as $key => $row){
		if(!isset($routes_blocked[date("Y",$row['date'])])){ $routes_blocked[date("Y",$row['date'])] = array(); }
		if(!isset($routes_blocked[date("Y",$row['date'])][date("n",$row['date'])])){ $routes_blocked[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
		$routes_blocked[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = ($row['tour_seats']+$row['route_seats']);
		}
		}
	//echo '<PRE>Blocked'; print_r($routes_blocked); echo '</PRE>';

//GET ASSOCIATED TOURS
$tours = array();
$query = 'SELECT DISTINCT tours.`id`,tours.`title` FROM `tours`,`tours_assoc` WHERE tours.`id` = tours_assoc.`tourid` AND tours_assoc.`type` = "r" AND tours_assoc.`typeid` = "'.getval('id').'" ORDER BY tours.`title` ASC';
//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours,$row);
	}

//GET OTHER ROUTES LIST
$oroutes = array();
$query = 'SELECT * FROM `routes` WHERE `id` != "'.getval('id').'" ORDER BY routes.`name` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($oroutes,$row);
	}

//GET ITINERARY
/*$itinerary = array();
$query = 'SELECT `id`,`nickname`,`title`,`comments` FROM `itinerary`';
	$query .= ' ORDER BY `nickname` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if($row['nickname'] != ""){
		$row['name'] = $row['nickname'];
		} else {
		$row['name'] = '[No nickname]';
		}
	if($row['title'] != ""){ $row['comments'] = $row['title'].' - '.$row['comments']; }
	$row['comments'] = strip_tags($row['comments']);
	$row['comments'] = str_replace("\r",'',$row['comments']);
	$row['comments'] = str_replace("\n",' ',$row['comments']);
	$row['comments'] = str_replace('"','&quot;',$row['comments']);
	if(strlen($row['comments']) > 180){
		$row['comments'] = trim(substr($row['comments'],0,89)).' ... '.trim(substr($row['comments'],-89));
		}
	$row['subhead'] = $row['comments'];
	$itinerary['i'.$row['id']] = $row;
	}

//GET ITINERARY STEPS
$fillform['steps'] = array();
$query = 'SELECT * FROM `routes_assoc` WHERE `routeid` = "'.getval('id').'" AND `routeid` != 0 ORDER BY `order` ASC';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if( isset($itinerary['i'.$row['itid']]) ){
		array_push($fillform['steps'],$row);
		}
	}*/

//SETUP CALENDAR VARS
	//Find soonest upcoming month
	$query = 'SELECT `date` FROM `routes_dates` WHERE `routeid` = "'.getval('id').'" AND `date` > '.$time.' ORDER BY `date` ASC LIMIT 1';
	$result = mysqlQuery($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){
	$soonest = mysql_fetch_assoc($result);
	$soonest = $soonest['date'];
	$calfirst = mktime(0,0,0,date("n",$soonest),1,date("Y",$soonest));
	} else {
	$calfirst = mktime(0,0,0,date("n",$time),1,date("Y",$time));
	}
	$calendar = array(
		"first" => $calfirst,
		"start" => date("w",$calfirst),
		"days" => date("t",$calfirst),
		"m" => date("n",$calfirst),
		"y" => date("Y",$calfirst)
		);
$calw = (7*42);

?><SCRIPT><!--

var anchor = '<? getval('anchor'); ?>';
var cur_dst = <? echo date("I"); ?>;
var st_dep_offset = <? echo tz_offset(getval('dep_loc'),getval('arr_loc'),"2010-1-1"); ?>;
var dst_dep_offset = <? echo tz_offset(getval('dep_loc'),getval('arr_loc'),"2010-6-1"); ?>;
var st_arr_offset = <? echo tz_offset(getval('arr_loc'),getval('dep_loc'),"2010-1-1"); ?>;
var dst_arr_offset = <? echo tz_offset(getval('arr_loc'),getval('dep_loc'),"2010-6-1"); ?>;

var tzs = new Array();<? foreach($locations as $key => $row){
	echo ' tzs[\''.$key.'\'] = \''.str_replace('_',' ',substr($row['timezone_id'],8)).'\';';
	}
	echo "\n";
	?>

function to_miles(){
	var km = eval(document.getElementById('kilometres').value);
	var miles = eval(km*0.6214);
	document.getElementById('miles').value = miles.toFixed(2);
	calc_bbprice();
	}

function to_km(){
	var miles = eval(document.getElementById('miles').value);
	var km = eval(miles*1.609);
	document.getElementById('kilometres').value = km.toFixed(2);
	calc_bbprice();
	}

var getoffset;
function ext_getoffset(){
	document.getElementById('dep_tz_desc').innerHTML = tzs['l'+document.getElementById('dep_loc').value];
	document.getElementById('arr_tz_desc').innerHTML = tzs['l'+document.getElementById('arr_loc').value];

	url = 'sups/timezones.php?tz_offset_dep_loc='+document.getElementById('dep_loc').value+'&tz_offset_arr_loc='+document.getElementById('arr_loc').value;
	getoffset=db_GetXmlHttpObject();
	getoffset.onreadystatechange=ext_setoffset;
	getoffset.open('GET',url,true);
	getoffset.send(null);
	}

function ext_setoffset(){ if(getoffset.readyState == 4){
	var results = getoffset.responseText;
	var findoff = results.split("\n");

	var read = 0;
	for(i in findoff){
		if(findoff[i] == '||END||'){ read = 0; }
		if(read == 1){
			var o = findoff[i].split('|');
			st_dep_offset = eval(o[0]);
			dst_dep_offset = eval(o[1]);
			st_arr_offset = eval(o[2]);
			dst_arr_offset = eval(o[3]);
			break;
			}
		if(findoff[i] == '||BEGIN||'){ read = 1; }
		}

	calctravel();
	}}

function calctravel(){
	//Find departure times
	var arrtime = new Date();
	arrtime.setHours(document.getElementById('arr_time_hour').value,eval(document.getElementById('arr_time_mins').value),0,0);
	a = arrtime.getTime();

	var st_dep_time = eval( a - (eval(document.getElementById('travel_hrs').value)*3600*1000) - (eval(document.getElementById('travel_mins').value)*60*1000) + (eval(st_arr_offset)*1000) );
	var dst_dep_time = eval( a - (eval(document.getElementById('travel_hrs').value)*3600*1000) - (eval(document.getElementById('travel_mins').value)*60*1000) + (eval(dst_arr_offset)*1000) );

	var st_deptime = new Date();
	st_deptime.setTime(st_dep_time);
		var Hours = st_deptime.getHours();
		var Minutes = st_deptime.getMinutes();
		Minutes = ( Minutes < 10 ? "0" : "" ) + Minutes;
		var timeOfDay = ( Hours < 12 ) ? "am" : "pm";
		Hours = ( Hours > 12 ) ? Hours - 12 : Hours;
		Hours = ( Hours == 0 ) ? 12 : Hours;
	var st_dep_time_desc = Hours + ":" + Minutes + timeOfDay;

	var dst_deptime = new Date();
	dst_deptime.setTime(dst_dep_time);
		var Hours = dst_deptime.getHours();
		var Minutes = dst_deptime.getMinutes();
		Minutes = ( Minutes < 10 ? "0" : "" ) + Minutes;
		var timeOfDay = ( Hours < 12 ) ? "am" : "pm";
		Hours = ( Hours > 12 ) ? Hours - 12 : Hours;
		Hours = ( Hours == 0 ) ? 12 : Hours;
	var dst_dep_time_desc = Hours + ":" + Minutes + timeOfDay;

	var html = '';
		if(st_dep_time_desc == dst_dep_time_desc){
			html += st_dep_time_desc;
			} else {
			html += st_dep_time_desc+' ST / ';
			html += dst_dep_time_desc+' DST';
			}

	document.getElementById('dep_time_desc').innerHTML = html;

	//Find arrival times
	var deptime = new Date();
	deptime.setHours(document.getElementById('dep_time_hour').value,eval(document.getElementById('dep_time_mins').value),0,0);
	d = deptime.getTime();

	var st_arr_time = eval( d + (eval(document.getElementById('travel_hrs').value)*3600*1000) + (eval(document.getElementById('travel_mins').value)*60*1000) + (eval(st_dep_offset)*1000) );
	var dst_arr_time = eval( d + (eval(document.getElementById('travel_hrs').value)*3600*1000) + (eval(document.getElementById('travel_mins').value)*60*1000) + (eval(dst_dep_offset)*1000) );

	var st_arrtime = new Date();
	st_arrtime.setTime(st_arr_time);
		var Hours = st_arrtime.getHours();
		var Minutes = st_arrtime.getMinutes();
		Minutes = ( Minutes < 10 ? "0" : "" ) + Minutes;
		var timeOfDay = ( Hours < 12 ) ? "am" : "pm";
		Hours = ( Hours > 12 ) ? Hours - 12 : Hours;
		Hours = ( Hours == 0 ) ? 12 : Hours;
	var st_arr_time_desc = Hours + ":" + Minutes + timeOfDay;

	var dst_arrtime = new Date();
	dst_arrtime.setTime(dst_arr_time);
		var Hours = dst_arrtime.getHours();
		var Minutes = dst_arrtime.getMinutes();
		Minutes = ( Minutes < 10 ? "0" : "" ) + Minutes;
		var timeOfDay = ( Hours < 12 ) ? "am" : "pm";
		Hours = ( Hours > 12 ) ? Hours - 12 : Hours;
		Hours = ( Hours == 0 ) ? 12 : Hours;
	var dst_arr_time_desc = Hours + ":" + Minutes + timeOfDay;

	var html = '';
		if(st_arr_time_desc == dst_arr_time_desc){
			html += st_arr_time_desc;
			} else {
			html += st_arr_time_desc+' ST / ';
			html += dst_arr_time_desc+' DST';
			}

	document.getElementById('arr_time_desc').innerHTML = html;
	}

function change_timeanchor(){
	if(document.getElementById('time_anchor_arr').checked){ anchor = 'arr'; } else { anchor = 'dep'; }
	var other = 'arr'; if(anchor == 'arr'){ other = 'dep'; }
	document.getElementById(anchor+'_time_form').style.display = '';
	document.getElementById(anchor+'_time_desc').style.display = 'none';
	document.getElementById(other+'_time_form').style.display = 'none';
	document.getElementById(other+'_time_desc').style.display = '';
	ext_getoffset();
	}

var calw = <? echo $calw; ?>;

var bg = 'DDDDDD';
function bgcolor(){
	if(bg == "FFFFFF"){ bg = "DDDDDD"; } else { bg = "FFFFFF"; }
	return bg;
	}

var cal = new Array();
<? for($y=2009; $y<(date("Y",$time)+6); $y++){
	echo "\t".'cal['.$y.'] = new Array();';
		for($m=1; $m<13; $m++){
		$first = mktime(0,0,0,$m,1,$y);
		echo ' cal['.$y.']['.$m.'] = new Array('.date("w",$first).','.date("t",$first).');';
		}
		echo "\n";
	} ?>

var runs = new Array();
<? foreach($routes_dates as $y => $years){
	echo "\t".'runs['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'runs['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' runs['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

var blocked = new Array();
<? foreach($routes_blocked as $y => $years){
	echo "\t".'blocked['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'blocked['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' blocked['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

function movecal(i){
	var newmonth = new Date();
	newmonth.setFullYear(document.getElementById("choose_year").value,eval(document.getElementById("choose_month").value - 1 + i),1);
	document.getElementById('choose_month').value = eval(newmonth.getMonth()+1);
	document.getElementById('choose_year').value = newmonth.getFullYear();
	buildcal(document.getElementById('choose_year').value,document.getElementById('choose_month').value);
	}

function buildcal(y,m){
	while(document.getElementById('calendar').rows.length > 2){ document.getElementById('calendar').deleteRow( eval(document.getElementById('calendar').rows.length-1) ) }

	var col = cal[y][m][0];

	//bg = 'DDDDDD';
	var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
	r.style.backgroundColor = 'FFFFFF'; //bgcolor();

	if(cal[y][m][0] > 0){
		var c = r.insertCell(r.cells.length);
		c.colSpan = cal[y][m][0];
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.borderRight = '1px solid #999999';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		}

	for(day=1; day<=cal[y][m][1]; day++){
		col++;

		var c = r.insertCell(r.cells.length);
		c.id = 'd'+day;
		c.style.borderTop = '1px solid #999999';
		if(col < 7){
			c.style.borderRight = '1px solid #999999';
			}
		c.style.textAlign = 'left';
		c.style.verticalAlign = 'top';
		c.style.padding = '3px';
		c.style.cursor = 'pointer';
		c.style.width = Math.ceil(calw/7)+'px';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontFamily = 'Arial';
		c.style.fontSize = '11px';
		c.abbr = day;
		c.innerHTML = day;
		c.onclick = function(){ toggle_run(y,m,this.abbr); hl_runs(y,m); update_runs(); }

	if(col == 7){
		var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
		r.style.backgroundColor = 'FFFFFF'; //bgcolor();
		col = 0;
		}

	} //End For Loop

	if(col > 0 && col < 7){
		var c = r.insertCell(r.cells.length);
		c.colSpan = eval(7-col);
		c.style.backgroundColor = 'DDDDDD';
		c.style.borderTop = '1px solid #999999';
		c.style.height = Math.ceil(calw/7)+'px';
		c.style.fontSize = '4px';
		c.innerHTML = '&nbsp;';
		}

	hl_runs(y,m);
	if(runs[y] != undefined){ sum_show_hide(y); }
	}

function toggle_run(y,m,d){
	if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] == 1){
		set_run(y,m,d,0);
		} else {
		set_run(y,m,d,1);
		}
	}

function set_run(y,m,d,s){
	if(blocked[y] != undefined && blocked[y][m] != undefined && blocked[y][m][d] != undefined && blocked[y][m][d] > 0){ s = 1; }
	if(runs[y] == undefined){
		runs[y] = new Array();
		}
	if(runs[y][m] == undefined){
		runs[y][m] = new Array();
		}
	runs[y][m][d] = s;
	}

function hl_runs(y,m){
	for(d=1; d<=cal[y][m][1]; d++){
		if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] == 1){
			document.getElementById('d'+d).style.backgroundColor = '#a5c7ff';
		} else {
			document.getElementById('d'+d).style.backgroundColor = document.getElementById('d'+d).parentNode.style.backgroundColor;
		}
		if(blocked[y] != undefined && blocked[y][m] != undefined && blocked[y][m][d] != undefined && blocked[y][m][d] > 0){
			//document.getElementById('d'+d).title = blocked[y][m][d]+' seats booked.  Can\'t be removed.';
			document.getElementById('d'+d).style.cursor = 'help'; //url(\'img/delete.ico\'),
			document.getElementById('d'+d).innerHTML = d+'<DIV STYLE="margin-top:3px; text-align:center; font-size:12px; color:#666666;">(<SPAN STYLE="color:#000000; font-weight:bold;">'+blocked[y][m][d]+'</SPAN>)</DIV>';
			document.getElementById('d'+d).onclick = '';
			$(document.getElementById('d'+d)).bind('mouseover', function(){
				show_booked(this);
			});
			$(document.getElementById('d'+d)).bind('mouseout', function(){
				hide_booked();
			});
		}
	} //End for loop
}

function bulkchange(){
	var sdate = new Date();
	sdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value); sdate.setHours(0,0,0,0);
	var edate = new Date();
	edate.setFullYear(document.getElementById("bulk_eyear").value,eval(document.getElementById("bulk_emonth").value-1),document.getElementById("bulk_eday").value); edate.setHours(0,0,0,0);

	var freq = document.getElementById("bulk_frequency").value;
	var f = 0;
	var i = sdate;
	while(i >= sdate && i <= edate){
		f++;
		if(freq == "sun" && i.getDay() == 0 || freq == "mon" && i.getDay() == 1 || freq == "tue" && i.getDay() == 2 || freq == "wed" && i.getDay() == 3 || freq == "thu" && i.getDay() == 4 || freq == "fri" && i.getDay() == 5 || freq == "sat" && i.getDay() == 6 || freq != "sun" && freq != "mon" && freq != "tue" && freq != "wed" && freq != "thu" && freq != "fri" && freq != "sat" && i == sdate || f == freq){
			set_run(i.getFullYear(),eval(i.getMonth()+1),i.getDate(),document.getElementById("bulk_setto").value);
			f = 0;
			}
		var n = new Date();
		n.setFullYear(i.getFullYear(),i.getMonth(),eval(i.getDate()+1)); n.setHours(0,0,0,0);
		i = n;
		}
	//alert(i+"\n"+edate);
	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}

function sortNumber(a,b){ return a - b; }

var month_names = new Array(''<? for($i=1; $i<13; $i++){ echo ',\''.date("M",mktime(0,0,0,$i,1,2009)).'\''; } ?>);

function update_runs(){
	var totalruns = 0;
	var fields = '';
	var summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">';

	var years = new Array();
	for(y in runs){
		years.push(y);
		}
		years.sort(sortNumber);

	for(i in years){
		y = years[i];
		var ysubtotal = 0;
		for(m in runs[y]){
			for(d in runs[y][m]){
				if(runs[y][m][d] != 0){ ysubtotal++; }
				}
			}
		summary += '<TR BGCOLOR="#DDDDDD" onClick="sum_show_hide(\''+y+'\');"><TD STYLE="font-family:Arial; font-size:9pt; border-bottom:1px solid #999999; cursor:pointer;">'+y+'</TD><TD ALIGN="right" STYLE="font-family:Arial; font-size:10px; color:#666666; border-bottom:1px solid #999999; cursor:pointer;">'+ysubtotal+' dates</TD></TR>';
		for(m in runs[y]){
			var countruns = 0;
			for(d in runs[y][m]){
				fields += '<INPUT TYPE="hidden" NAME="runs[]" VALUE="'+y+'|'+m+'|'+d+'|'+runs[y][m][d]+'">';
				if(runs[y][m][d] == 1){ countruns++; }
				}
			if(countruns > 0){
				summary += '<TR><TD ID="rs_'+y+'_'+m+'_1" STYLE="font-family:Arial; font-size:10px; text-align:right; padding-left:10px; padding-right:18px; cursor:pointer;';
				if(y != document.getElementById("choose_year").value){ summary += ' display:none;'; }
				summary += '" onMouseOver="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#000000\');" onClick="chgcal_sumline(\''+y+'\',\''+m+'\');">'+month_names[m]+'</TD><TD ID="rs_'+y+'_'+m+'_2" STYLE="font-family:Arial; font-size:10px; text-align:right; cursor:pointer;';
				if(y != document.getElementById("choose_year").value){ summary += ' display:none;'; }
				summary += '" onMouseOver="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'+y+'_'+m+'_\',\'#000000\');" onClick="chgcal_sumline(\''+y+'\',\''+m+'\');">'+countruns+' dates</TD></TR>';
				}
			totalruns = eval(totalruns+countruns);
			}
		}
	summary += '<TR><TD STYLE="border-top:1px solid #666666; font-family:Arial; font-size:9pt; text-align:right; padding-right:18px;">Total</TD><TD ALIGN="right" STYLE="border-top:1px solid #666666; font-family:Arial; font-size:10px;">'+totalruns+' dates</TD></TR>';
	summary += '<TR><TD COLSPAN="2"><INPUT TYPE="button" VALUE="Clear all dates" onClick="runs_rmvall();" STYLE="font-size:8pt;"></TD></TR>';
	summary += '</TABLE>';
	document.getElementById('run_fields').innerHTML = fields;
	document.getElementById('run_summary').innerHTML = summary;
	}

function runs_rmvall(){
	for(y in runs){
		for(m in runs[y]){
			for(d in runs[y][m]){
				if(blocked[y] == undefined || blocked[y][m] == undefined || blocked[y][m][d] === undefined || blocked[y][m][d] == 0){ runs[y][m][d] = 0; }
				}
			}
		}
	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}

function adjustend(adddays){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value);

	var findend = startdate.getTime();
	findend = eval(findend + (86400000*eval(adddays)));

	var end = new Date();
	end.setTime(findend);

	document.getElementById("bulk_emonth").value = eval(end.getMonth()+1);
	document.getElementById("bulk_eday").value = end.getDate();
	document.getElementById("bulk_eyear").value = end.getFullYear();
	}


function checkend(){
	//Adjust days available for month/year chosen
	var sday = document.getElementById("bulk_sday");
	if(sday.length < cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
		while(sday.length < cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
			var newday = document.createElement('option');
				newday.text = eval(sday.length+1);
				newday.value = eval(sday.length+1);
				try { sday.add(newday,null); } catch(ex) { sday.add(newday); }
			}
		} else if(sday.length > cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]) {
		while(sday.length > cal[document.getElementById("bulk_syear").value][document.getElementById("bulk_smonth").value][1]){
			sday.remove( eval(sday.length-1) );
			}
		}
	var eday = document.getElementById("bulk_eday");
	if(eday.length < cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
		while(eday.length < cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
			var newday = document.createElement('option');
				newday.text = eval(eday.length+1);
				newday.value = eval(eday.length+1);
				try { eday.add(newday,null); } catch(ex) { eday.add(newday); }
			}
		} else if(eday.length > cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]) {
		while(eday.length > cal[document.getElementById("bulk_eyear").value][document.getElementById("bulk_emonth").value][1]){
			eday.remove( eval(eday.length-1) );
			}
		}

	//Adjust ending date if later starting date is chosen
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("bulk_syear").value,eval(document.getElementById("bulk_smonth").value-1),document.getElementById("bulk_sday").value);
	var chkdate = startdate.getTime();

	var enddate = new Date();
	enddate.setFullYear(document.getElementById("bulk_eyear").value,eval(document.getElementById("bulk_emonth").value-1),document.getElementById("bulk_eday").value);
	var chkend = enddate.getTime();

	if(chkend < chkdate){ adjustend(0); }
	}


var xmlhttp;

function db_getruns(){
	url = 'sups/getruns.php?id='+document.getElementById('copyfromroute').value;
	xmlhttp=db_GetXmlHttpObject();
	xmlhttp.onreadystatechange=db_process;
	xmlhttp.open('GET',url,true);
	xmlhttp.send(null);
	}

function db_GetXmlHttpObject(){
	if(window.XMLHttpRequest){
		return new XMLHttpRequest();
		}
	if(window.ActiveXObject){
		return new ActiveXObject("Microsoft.XMLHTTP");
		}
	return null;
	}

function db_process(){
if(xmlhttp.readyState == 4){
	var results = xmlhttp.responseText;
	fromroute = results.split("\n");

	if(document.getElementById('copyfromtype').value == "replace"){ runs_rmvall(); }

	var read = 0;
	for(y in fromroute){
		if(fromroute[y] == '||END||'){ read = 0; }
		if(read == 1){
			var add = fromroute[y].split('|');
			set_run(add[0],add[1],add[2],1);
			}
		if(fromroute[y] == '||BEGIN||'){ read = 1; }
		}

	hl_runs(document.getElementById("choose_year").value,document.getElementById("choose_month").value);
	update_runs();
	}
	}

function showme(i,name){
	var x = document.getElementById(name);
	if(i == 'toggle' && x.style.display == 'none'){ i = ''; } else if(i == 'toggle'){ i = 'none'; }
	x.style.display = i;

	if(i == 'none'){ i = ''; } else { i = 'none'; }
	document.getElementById(name+'_s').style.display = i;
	}

function sum_show_hide(show){
	for(y in runs){
	var disp = '';
	if(y != show){ disp = 'none'; }
		for(m in runs[y]){
			if(document.getElementById('rs_'+y+'_'+m+'_1') != undefined){ document.getElementById('rs_'+y+'_'+m+'_1').style.display = disp; }
			if(document.getElementById('rs_'+y+'_'+m+'_2') != undefined){ document.getElementById('rs_'+y+'_'+m+'_2').style.display = disp; }
			}
		} //End y for loop
	}

function hl_sumline(name,c){
	document.getElementById(name+'1').style.color = c;
	document.getElementById(name+'2').style.color = c;
	}

function chgcal_sumline(y,m){
	document.getElementById('choose_month').value = m;
	document.getElementById('choose_year').value = y;
	buildcal(y,m);
	}

function calc_bbprice(){
	var cpm = <? echo $defaults['cpm']; ?>;
	var bbp = eval(document.getElementById('bbprice').value);
	var mileage = eval(document.getElementById('miles').value);
	if(bbp == "" || bbp == undefined || bbp == 0){ bbp = eval(mileage*cpm); }
	document.getElementById('show_bbprice').innerHTML = '$'+bbp.toFixed(2);
	}

/*var itinerary = new Array();
<?
	foreach($fillform['steps'] as $i => $step){
		echo "\t".'itinerary['.$i.'] = new Array(); itinerary['.$i.']["id"] = "'.$step['itid'].'";'."\n";
		}
	?>

var optinfo = new Array();
<?
	foreach($itinerary as $row){
		echo "\t".'optinfo["i'.$row['id'].'"] = new Array(); optinfo["i'.$row['id'].'"]["sel"] = 0;';
			foreach($row as $key => $val){
				echo ' optinfo["i'.$row['id'].'"]["'.$key.'"] = "'.str_replace('"','\\"',$val).'";';
				}
			echo "\n";
		}
	?>

function hl_left(){
	//Clear all highlighted
	for(i in optinfo){
		optinfo[i]['sel'] = 0;
		optinfo[i]['hlp'] = 0;
		}

	//Find included
	for(t in itinerary){
		optinfo['i'+itinerary[t]['id']]['sel'] = 1;
		}

	//Highlight all
	for(i in optinfo){
		hl('i',optinfo[i]['id'],'b');
		}
	}

function hl(type,id,o){
	var x = document.getElementById(type+'_'+id);
	var sel = 0; if(optinfo[type+id] != undefined){ sel = optinfo[type+id]['sel']; }

	if(o == 1){
		x.style.color = '#0000FF';
		} else if(o == 0) {
		x.style.color = '#000000';
		}
	if(o == 1 || sel == 1){
		x.style.backgroundImage = 'url(\'img/fade_r_grey.jpg\')';
		x.style.backgroundPosition = 'center right';
		x.style.backgroundRepeat = 'repeat-y';
		} else {
		x.style.backgroundImage = '';
		x.style.backgroundPosition = '';
		x.style.backgroundRepeat = '';
		}
	}

function adjlcon(){
	document.getElementById('lcon').style.height = (document.getElementById('toverview').offsetHeight)+'px';
	}

function addtoit(type,id){
	var add = new Array();
		add['id'] = id;
		add['type'] = type;

	itinerary.push(add);
	buildit();
	}

function delsetup(obj){
	var x = document.getElementById('fordel');
	x.style.position = 'absolute';
	x.style.display = '';
	x.style.padding = '0px';
	x.innerHTML = obj.innerHTML;
	x.style.zIndex = 1000;

	var boxpos = findPos(obj);
	x.style.top = eval(boxpos[1])+'px';
	x.style.left = eval(boxpos[0])+'px';
	}

function rmvfromit(key){
	itinerary.splice(key,1);
	buildit();
	}

function buildit(){
	var t = document.getElementById('toverview');

	t.innerHTML = '';
	var code = '';
	if(itinerary.length > 0){
		for(i in itinerary){
			var optid = 'i'+itinerary[i]['id'];
			code += '<DIV ID="tstep_'+i+'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_grey.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">'+"\n";
				code += '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from itinerary" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromit(\''+i+'\');"></SPAN>'+"\n";
				code += '&#149; '+optinfo[optid]['name'];
					code += '<BR><SPAN STYLE="font-size:10px; color:#666666;">'+optinfo[optid]['subhead']+'</SPAN>'+"\n";
				code += '</DIV>'+"\n\n";
			}
		} else {
		var code = '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>';
		}
	t.innerHTML = code;

	builditfields();
	CreateDragContainer();
	document.getElementById('fordel').display = 'none';
	hl_left();
	adjlcon();
	}

function builditfields(){
	var f = document.getElementById('itfields');
	f.innerHTML = '';
	for(i in itinerary){
		f.innerHTML += '<INPUT TYPE="hidden" NAME="it_ids[]" VALUE="'+itinerary[i]['id']+'">'+"\n";
		}
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

//BEGIN DRAG AND DROP FUNCTIONS
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = 'n';
var dragTarget = 'n';
var dropTargets = new Array();
var mouseOffset = null;
var cur = 0;

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function CreateDragContainer(){
	dropTargets = new Array();
	for(i in itinerary){
		addDropTarget(document.getElementById('tstep_'+i));
		}
}

function addDropTarget(obj){
	dropTargets.push(obj);
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets.length > 0){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(t,item){
	if(!item) return;
	cur = t;
	item.onmousedown = function(ev){

		dragObject = document.getElementById("forshadow");

		dragObject.innerHTML = item.innerHTML;
		dragObject.style.width = item.offsetWidth;
		dragObject.style.height = item.offsetHeight;
		dragObject.style.padding = item.style.padding;

		dragObject.style.backgroundColor = item.style.backgroundColor;
		dragObject.style.backgroundImage = item.style.backgroundImage;
		dragObject.style.backgroundPosition = item.style.backgroundPosition;
		dragObject.style.backgroundRepeat = item.style.backgroundRepeat;

		dragObject.style.textAlign = 'left';
		dragObject.style.fontFamily = document.getElementById('toverview').style.fontFamily;
		dragObject.style.fontSize = document.getElementById('toverview').style.fontSize;

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin = 'n';
		for(i=0; i<dropTargets.length; i++){
			if(item.id == dropTargets[i].id){
			dragOrigin = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin   = 'n';
	cleartarg();
}

function movem(){
	if(dragOrigin != 'n' && dragTarget != 'n'){
		var sv = itinerary[dragOrigin];
		itinerary.splice(dragOrigin,1);
		itinerary.splice(dragTarget,0,sv);
		buildit();
	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';
		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget){
		cleartarg();
		dragTarget = newTarget;
			if(newTarget < dragOrigin){
				document.getElementById( dropTargets[newTarget].id ).style.borderTop = "4px solid #0000FF";
				} else if(newTarget > dragOrigin){
				document.getElementById( dropTargets[newTarget].id ).style.borderBottom = "4px solid #0000FF";
				}
		}
	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget != 'n' && testIsValidObject( dropTargets[dragTarget] )){
		document.getElementById( dropTargets[dragTarget].id ).style.borderTop = "0px solid #FFFFFF";
		document.getElementById( dropTargets[dragTarget].id ).style.borderBottom = "1px solid #DDDDDD";
		}
	dragTarget = 'n';
}*/


function show_booked(obj){
	var offset = $(obj).offset();
	$('#cal_info').html('<img src="img/load.gif" border="0" class="show_booked_spinner">');
	$('#cal_info').css('left', (parseInt(offset.left) + parseInt($(obj).width()) + 6)+'px');
	$('#cal_info').css('top', offset.top);
	$('#cal_info').delay(500).fadeTo(200,1);

	var day = $(obj).attr('id').replace(/d/,'');

	var url = 'sups/calassist.php';
	var post_data = 'get=route_booked&id_route=<? echo getval('id'); ?>&date='+$('#choose_year').val()+'-'+$('#choose_month').val()+'-'+day;
	//console.log(url+'?'+post_data);

	$.ajax({
		url: url,
		type: "GET",
		data: post_data,
		dataType: 'json',
		cache: false,
		error: function(){
			$('.show_booked_spinner').attr('src','img/caution16x16.png');
		},
		success: function(result){
			//alert(result);
			if(typeof result.error != "undefined"){
				$('.show_booked_spinner').attr('src','img/caution16x16.png');
			} else {
				var html = '<span style="font-size:11pt;">'+result.date_display+'</span><br>';
					html += 'Seats blocked by tours: '+result.tour_seats+'<br>';
					html += 'Seats blocked directly: '+result.route_seats+'<br>';
					html += '<div style="margin-top:6px;">';
					html += '	Reservations:<br>';
					for(i in result.reservations){
						html += '	<a href="3_reservations.php?view='+result.reservations[i]+'">'+result.reservations[i]+'</a><br>';
					}
					html += '</div>';
				$('#cal_info').html(html);
			}
		}
	});
}

function hide_booked(){
	$('#cal_info').hide();
}

function keep_booked(){
	$('#cal_info').show();
}


// -->
</SCRIPT>

<div id="cal_info" style="display:none; position:absolute; background-color:#FFFFFF; border:1px solid grey; padding:8px; font-family:Helvetica; font-size:9pt; text-align:left;" onmousemove="keep_booked();" onmouseout="hide_booked();"></div>

<? echo "\n\n";


echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo routenav();

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";

bgcolor('reset');
//echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
//	echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Basic</TD>';
//	echo '</TR>'."\n\n";

	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">ID</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Name/Reference</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:400px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Seats available</TD><TD><INPUT TYPE="text" NAME="seats" STYLE="width:50px;" VALUE="'.getval('seats').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Minimum PAX needed to run</TD><TD><INPUT TYPE="text" NAME="minimum" STYLE="width:50px;" VALUE="'.getval('minimum').'"></TD></TR>'."\n";

	//Departure location/time
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Departure location/time zone</TD><TD STYLE="font-family:Arial; font-size:10pt;"><SELECT NAME="dep_loc" ID="dep_loc" STYLE="width:200px;" onChange="ext_getoffset();">';
		foreach($locations as $loc){
		echo '<OPTION VALUE="'.$loc['id'].'"';
		if(getval('dep_loc') == $loc['id']): echo ' SELECTED'; endif;
		echo '>'.$loc['name'].'</OPTION>';
		}
		echo '</SELECT>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Departure time</TD><TD STYLE="font-family:Arial; font-size:10pt; height:28px;">';
		echo '<DIV STYLE="width:200px; float:left;"><INPUT TYPE="radio" ID="time_anchor_dep" NAME="anchor" VALUE="dep"';
			if(getval('anchor') != 'arr'){ echo ' CHECKED'; }
			echo ' onChange="change_timeanchor();"><LABEL FOR="time_anchor_dep" STYLE="margin-right:10px;">Fix in: <SPAN ID="dep_tz_desc" STYLE="font-style:italic;">'.str_replace('_',' ',substr($locations['l'.getval('dep_loc')]['timezone_id'],8)).'</SPAN> time</LABEL></DIV>';
		echo '<SPAN ID="dep_time_form"';
			if(getval('anchor') == 'arr'){ echo ' STYLE="display:none;"'; }
			echo '><SELECT NAME="dep_time_hour" ID="dep_time_hour" onChange="calctravel();">';
		for($ii=0; $ii<24; $ii++){
		echo '<OPTION VALUE="'.$ii.'"';
		if( date("G",($basetime+getval('dep_time'))) == $ii ): echo " SELECTED"; endif;
		echo '>'.date("ga",mktime($ii,"1","0","1","1","2005")).'</OPTION>';
		}
		echo '</SELECT>:<INPUT TYPE="text" NAME="dep_time_mins" ID="dep_time_mins" STYLE="width:30px;" VALUE="'.date("i",($basetime+getval('dep_time'))).'" onChange="calctravel();" onKeyUp="calctravel();"></SPAN>';
		echo '<SPAN ID="dep_time_desc"';
			if(getval('anchor') != 'arr'){ echo ' STYLE="display:none;"'; }
			echo '>';
		$st_dep_time = ($basetime+getval('arr_time')-getval('travel_time')+tz_offset(getval('arr_loc'),getval('dep_loc'),"2010-1-1"));
		$dst_dep_time = ($basetime+getval('arr_time')-getval('travel_time')+tz_offset(getval('arr_loc'),getval('dep_loc'),"2010-6-1"));
			if($st_dep_time == $dst_dep_time){
				echo date("g:ia",$st_dep_time);
				} else {
				echo date("g:ia",$st_dep_time).' ST / ';
				echo date("g:ia",$dst_dep_time).' DST';
				}
			echo '</SPAN></TD></TR>'."\n";

	//Travel time
	$timedesc = fromsecs(getval('travel_time'));
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Travel time</TD><TD STYLE="font-family:Arial; font-size:10pt;"><INPUT TYPE="text" NAME="travel_hrs" ID="travel_hrs" STYLE="width:30px;" VALUE="'.$timedesc['h'].'" onChange="calctravel();" onKeyUp="calctravel();"> hours, <INPUT TYPE="text" NAME="travel_mins" ID="travel_mins" STYLE="width:30px;" VALUE="'.$timedesc['m'].'" onChange="calctravel();" onKeyUp="calctravel();"> minutes</TD></TR>'."\n";

	//Arrival location/time
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Arrival location/time zone</TD><TD STYLE="font-family:Arial; font-size:10pt;"><SELECT NAME="arr_loc" ID="arr_loc" STYLE="width:200px;" onChange="ext_getoffset();">';
		foreach($locations as $loc){
		echo '<OPTION VALUE="'.$loc['id'].'"';
		if(getval('arr_loc') == $loc['id']): echo ' SELECTED'; endif;
		echo '>'.$loc['name'].'</OPTION>'; //&nbsp;&nbsp;-&nbsp;&nbsp;('.str_replace('_',' ',substr($locations['l'.$loc['id']]['timezone_id'],8)).' time)
		}
		echo '</SELECT>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Arrival time</TD><TD STYLE="font-family:Arial; font-size:10pt; height:28px;">';
		echo '<DIV STYLE="width:200px; float:left;"><INPUT TYPE="radio" ID="time_anchor_arr" NAME="anchor" VALUE="arr"';
			if(getval('anchor') == 'arr'){ echo ' CHECKED'; }
			echo ' onChange="change_timeanchor();"><LABEL FOR="time_anchor_arr" STYLE="margin-right:10px;">Fix in: <SPAN ID="arr_tz_desc" STYLE="font-style:italic;">'.str_replace('_',' ',substr($locations['l'.getval('arr_loc')]['timezone_id'],8)).'</SPAN> time</LABEL></DIV>';
		echo '<SPAN ID="arr_time_form"';
			if(getval('anchor') != 'arr'){ echo ' STYLE="display:none;"'; }
			echo '><SELECT NAME="arr_time_hour" ID="arr_time_hour" onChange="calctravel();">';
			for($ii=0; $ii<24; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("G",($basetime+getval('arr_time'))) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("ga",mktime($ii,"1","0","1","1","2005")).'</OPTION>';
			}
		echo '</SELECT>:<INPUT TYPE="text" NAME="arr_time_mins" ID="arr_time_mins" STYLE="width:30px;" VALUE="'.date("i",($basetime+getval('arr_time'))).'" onChange="calctravel();" onKeyUp="calctravel();"></SPAN>';
		echo '<SPAN ID="arr_time_desc"';
			if(getval('anchor') == 'arr'){ echo ' STYLE="display:none;"'; }
			echo '>';
		$st_arr_time = ($basetime+getval('dep_time')+getval('travel_time')+tz_offset(getval('dep_loc'),getval('arr_loc'),"2010-1-1"));
		$dst_arr_time = ($basetime+getval('dep_time')+getval('travel_time')+tz_offset(getval('dep_loc'),getval('arr_loc'),"2010-6-1"));
			if($st_arr_time == $dst_arr_time){
				echo date("g:ia",$st_arr_time);
				} else {
				echo date("g:ia",$st_arr_time).' ST / ';
				echo date("g:ia",$dst_arr_time).' DST';
				}
			echo '</SPAN></TD></TR>'."\n";

	echo '<TR STYLE="background:#'.bgcolor('').'"><TD VALIGN="top" STYLE="padding-top:4px; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; white-space:nowrap;">Add/Remove individual dates<BR><SPAN STYLE="font-family:Arial; font-size:8pt; font-weight:normal;"><SPAN STYLE="color:#666666;">Click on a date to add, again to remove.</SPAN><BR><BR>';

		echo '<I>-or-</I><BR><BR>';

		echo '<SPAN STYLE="font-size:10pt; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="showme(\'toggle\',\'runs_addbatch\');">Add/Remove a batch of dates</SPAN><BR><SPAN STYLE="color:#666666;">Add/remove multiple dates quickly.<BR>Dates already booked will be ignored.</SPAN><BR>';
		echo '<DIV ID="runs_addbatch" STYLE="display:none;">';
		echo 'Add/Remove: <SELECT ID="bulk_setto" STYLE="font-size:8pt;"><OPTION VALUE="1">Add dates</OPTION><OPTION VALUE="0">Remove dates</OPTION></SELECT><BR>';
		echo '<NOBR>Starting: <SELECT ID="bulk_smonth" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("n",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_sday" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<32; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("j",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_syear" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("Y",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></NOBR><BR>';
		echo '<NOBR>Ending: <SELECT ID="bulk_emonth" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("n",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_eday" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=1; $ii<32; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("j",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT> / <SELECT ID="bulk_eyear" STYLE="font-size:8pt;" onChange="checkend()">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( date("Y",$calendar['first']) == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></NOBR><BR>';
		echo 'Frequency: <SELECT ID="bulk_frequency" STYLE="font-size:8pt;"><OPTION VALUE="1">Every day</OPTION><OPTION VALUE="2">Every other day</OPTION><OPTION VALUE="3">Every 3rd day</OPTION><OPTION VALUE="4">Every 4th day</OPTION><OPTION VALUE="5">Every 5th day</OPTION><OPTION VALUE="6">Every 6th day</OPTION><OPTION VALUE="sun">Every Sunday</OPTION><OPTION VALUE="mon">Every Monday</OPTION><OPTION VALUE="tue">Every Tuesday</OPTION><OPTION VALUE="wed">Every Wednesday</OPTION><OPTION VALUE="thu">Every Thursday</OPTION><OPTION VALUE="fri">Every Friday</OPTION><OPTION VALUE="sat">Every Saturday</OPTION></SELECT><BR>';
		echo '<INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="Update calendar" onClick="bulkchange()">';
		echo '</SPAN></DIV><DIV ID="runs_addbatch_s"><SPAN STYLE="color:#0000FF; cursor:pointer;" onClick="showme(\'\',\'runs_addbatch\');">Click here</SPAN></DIV><BR>';

		echo '<I>-or-</I><BR><BR>';

		echo '<SPAN STYLE="font-size:10pt; font-weight:bold; cursor:pointer;" onMouseOver="this.style.color=\'#0000FF\';" onMouseOut="this.style.color=\'#000000\';" onClick="showme(\'toggle\',\'runs_copyfromdb\');">Copy dates from another route</SPAN><BR><SPAN STYLE="color:#666666;">Dates already booked will be ignored.</SPAN><BR>';
		echo '<DIV ID="runs_copyfromdb" STYLE="display:none;">';

		echo 'Route: <SELECT ID="copyfromroute" STYLE="font-size:8pt; width:200px;">';
		foreach($oroutes as $row){
			echo '<OPTION VALUE="'.$row['id'].'">'.$row['name'].'</OPTION>';
			}
			echo '</SELECT><BR>';
		echo 'Replace/Add: <SELECT ID="copyfromtype" STYLE="font-size:8pt;"><OPTION VALUE="replace">Replace entire calendar</OPTION><OPTION VALUE="add">Add to current calendar</OPTION></SELECT><BR>';
		echo '<INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="Copy" onClick="db_getruns()">';

		echo '</DIV><DIV ID="runs_copyfromdb_s"><SPAN STYLE="color:#0000FF; cursor:pointer;" onClick="showme(\'\',\'runs_copyfromdb\');">Click here</SPAN></DIV>';
		echo '</TD><TD VALIGN="top" STYLE="white-space:nowrap; width:61%;">';
$svbg = $GLOBALS['bgcolor'];
$GLOBALS['bgcolor'] = 'FFFFFF';

echo '<DIV STYLE="float:left; padding-right:10px;"><TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="calendar" WIDTH="'.$calw.'" STYLE="border:1px solid #7F9DB9;">
<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px;">
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">
	<TD ALIGN="left" STYLE="padding-right:8px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&lt;&lt;" onClick="movecal(-1);"></TD>
	<TD ALIGN="center" STYLE="font-size:8pt;"><SELECT ID="choose_month" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['m'] == $ii ): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$ii,"1","2005")).'</OPTION>';
			}
			echo '</SELECT> <SELECT ID="choose_year" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=2009; $ii<(date("Y",$time)+6); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['y'] == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></TD>
	<TD ALIGN="right" STYLE="padding-left:8px;"><INPUT TYPE="button" STYLE="font-size:8pt;" VALUE="&gt;&gt;" onClick="movecal(1);"></TD>
	</TABLE>
	</TD></TR>

<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">M</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">W</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">T</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">F</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999;">S</TD>
	</TR>

<TR BGCOLOR="#FFFFFF">'; //'.bgcolor('DDDDDD').'

$day = 0;
$col = $calendar['start'];

if($calendar['start'] > 0): echo '<TD COLSPAN="'.$calendar['start'].'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#DDDDDD; border-top:1px solid #999999; border-right:1px solid #999999;">&nbsp;</TD>'; endif;

for($i=0; $day<$calendar['days']; $i++){
	$col++;
	++$day;
	if(isset($routes_dates[$calendar['y']][$calendar['m']][$day])){ $thisdate = $routes_dates[$calendar['y']][$calendar['m']][$day]; } else { $thisdate = 0; }
	if(isset($routes_blocked[$calendar['y']][$calendar['m']][$day]) && $routes_blocked[$calendar['y']][$calendar['m']][$day] > 0){ $thisblock = $routes_blocked[$calendar['y']][$calendar['m']][$day]; } else { $thisblock = 0; }

	echo '	<TD ALIGN="left" VALIGN="top" ID="d'.$day.'" STYLE="';
	if($thisdate == 1){ echo 'background:#a5c7ff; '; }
	echo 'padding:3px; width:'.ceil($calw/7).'px; height:'.ceil($calw/7).'px; font-family:Arial; font-size:11px; border-top:1px solid #999999;';
	if($col < 7){ echo ' border-right:1px solid #999999;'; }
	if($thisblock > 0){
		echo ' cursor:help;" onMouseOver="show_booked(this);" onMouseOut="hide_booked();">'.$day; //url(\'img/delete.ico\'), // TITLE="'.$thisblock.' seats booked.  Can\'t be removed."
		echo '<DIV STYLE="margin-top:3px; text-align:center; font-size:12px; color:#666666;">(<SPAN STYLE="color:#000000; font-weight:bold;">'.$thisblock.'</SPAN>)</DIV>';
		} else {
		echo ' cursor:pointer;" onClick="toggle_run(\''.$calendar['y'].'\',\''.$calendar['m'].'\',\''.$day.'\'); hl_runs(\''.$calendar['y'].'\',\''.$calendar['m'].'\'); update_runs();">'.$day;
		}
	echo '</TD>'."\n";

if($col == 7){
	echo '</TR>'."\n".'<TR BGCOLOR="#FFFFFF">';
	$col = 0;
	}

}

if($col > 0 && $col < 7): echo '<TD COLSPAN="'.(7 - $col).'" STYLE="height:'.ceil($calw/7).'px; font-size:4px; background-color:#DDDDDD; border-top:1px solid #999999;">&nbsp;</TD>'."\n"; endif;

echo '</TR></TABLE></DIV>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Summary</SPAN><BR>';
echo '<DIV ID="run_summary"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100px;">';
	$totalruns = 0;
	foreach($routes_dates as $y => $year){
		$ysubtotal = 0; foreach($year as $m => $month){ $ysubtotal = ($ysubtotal+count($month)); }
		echo '<TR BGCOLOR="#DDDDDD" onClick="sum_show_hide(\''.$y.'\');"><TD STYLE="font-family:Arial; font-size:9pt; border-bottom:1px solid #999999; cursor:pointer;">'.$y.'</TD><TD ALIGN="right" STYLE="font-family:Arial; font-size:10px; color:#666666; border-bottom:1px solid #999999; cursor:pointer;">'.$ysubtotal.' dates</TD></TR>';
		foreach($year as $m => $month){
			echo '<TR><TD ID="rs_'.$y.'_'.$m.'_1" STYLE="font-family:Arial; font-size:10px; text-align:right; padding-left:10px; padding-right:18px; cursor:pointer;';
			if($y != $calendar['y']){ echo ' display:none;'; }
			echo '" onMouseOver="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#000000\');" onClick="chgcal_sumline(\''.$y.'\',\''.$m.'\');">'.date("M",mktime(0,0,0,$m,1,$y)).'</TD><TD ID="rs_'.$y.'_'.$m.'_2" STYLE="font-family:Arial; font-size:10px; text-align:right; cursor:pointer;';
			if($y != $calendar['y']){ echo ' display:none;'; }
			echo '" onMouseOver="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#0000FF\');" onMouseOut="hl_sumline(\'rs_'.$y.'_'.$m.'_\',\'#000000\');" onClick="chgcal_sumline(\''.$y.'\',\''.$m.'\');">'.count($month).' dates</TD></TR>';
			$totalruns = ($totalruns+count($month));
			}
		}
	echo '<TR><TD STYLE="border-top:1px solid #666666; font-family:Arial; font-size:9pt; text-align:right; padding-right:18px;">Total</TD><TD ALIGN="right" STYLE="border-top:1px solid #666666; font-family:Arial; font-size:10px;">'.$totalruns.' dates</TD></TR>';
	echo '<TR><TD COLSPAN="2"><INPUT TYPE="button" VALUE="Clear all dates" onClick="runs_rmvall();" STYLE="font-size:8pt;"></TD></TR>';
	echo '</TABLE>';
	echo '</DIV>'."\n\n";

echo '<DIV ID="run_fields">';
foreach($routes_dates as $y => $years){
	foreach($years as $m => $months){
		foreach($months as $d => $day){
			echo '<INPUT TYPE="hidden" NAME="runs[]" VALUE="'.$y.'|'.$m.'|'.$d.'|'.$day.'">';
			}
		echo "\n";
		}
	echo "\n";
	}
	echo '</DIV>'."\n\n";

$GLOBALS['bgcolor'] = $svbg;
		echo '</TD></TR>'."\n";

	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Miles / Kilometres</TD><TD><INPUT TYPE="text" ID="miles" NAME="miles" STYLE="width:80px;" VALUE="'.getval('miles').'" onKeyUp="to_km()" onChange="to_km()"> / <INPUT TYPE="text" ID="kilometres" STYLE="width:80px;" VALUE="'.getval('kilometres').'" onKeyUp="to_miles()" onChange="to_miles()"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Bundu Bus Details<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Will appear as a pop up information box on Bundu Bus reservations page.</SPAN></TD><TD><TEXTAREA NAME="details" STYLE="height:100px; width:400px;">'.getval('details').'</TEXTAREA></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Bundu Bus Price<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If 0, price will be based on mileage.</SPAN></TD><TD STYLE="font-family:Arial; font-size:12pt;">$<INPUT TYPE="text" ID="bbprice" NAME="bbprice" STYLE="width:80px;" VALUE="'.getval('bbprice').'" onKeyUp="calc_bbprice()" onChange="calc_bbprice()"> &nbsp;&nbsp;Price will be: <SPAN STYLE="font-weight:bold;" ID="show_bbprice">$';
		if(getval('bbprice') == 0){ echo number_format((getval('miles')*$defaults['cpm']),2,'.',''); } else { echo getval('bbprice'); }
		echo '</SPAN></TD></TR>'."\n";

	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
		foreach($vendors as $vendor){
		echo '<OPTION VALUE="'.$vendor['id'].'"';
		if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
		echo '>'.$vendor['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";

	if(getval('id') != "*new*"){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Itinerary</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
		if(getval('itcount') < 1){ echo 'No'; } else { echo getval('itcount'); }
		if(getval('itcount') == 1){ echo ' entry'; } else { echo ' entries'; }
		echo '&nbsp; [ <A HREF="3_routes_itinerary.php?edit='.getval('id').'">Edit</A> ]</TD></TR>'."\n";
		}

//!DESIGN ITINERARY
	/*
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;"><A NAME="designtop"></A>Itinerary</TD>';
		echo '</TR>'."\n\n";
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-left:0px; padding-right:0px;">';

//!LEFT SIDE - OPTIONS
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>';
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:48%; padding-right:6px; border-right:2px solid #333333; text-align:center; font-family:Arial; font-size:9pt;" ID="lconcon">';
		echo '<DIV STYLE="overflow:scroll; overflow-x:hidden; height:300px;" ID="lcon">';
			foreach($itinerary as $row){
			echo '<DIV ID="i_'.$row['id'].'" STYLE="border-bottom:1px solid #DDDDDD; padding:2px; text-align:left; cursor:pointer;" onMouseOver="hl(\'i\',\''.$row['id'].'\',1);" onMouseOut="hl(\'i\',\''.$row['id'].'\',0);" onDblClick="addtoit(\'i\',\''.$row['id'].'\');">';
				echo '<SPAN STYLE="float:right"><INPUT TYPE="button" VALUE="+" TITLE="Add to tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="addtoit(\'i\',\''.$row['id'].'\');"></SPAN>';
				echo '<SPAN ID="ihtml_'.$row['id'].'">';
					echo '&#149; '.$row['name'].'<BR>';
					echo '<SPAN STYLE="font-size:10px; color:#666666;">'.$row['subhead'].'</SPAN>';
					echo '</SPAN>';
				echo '</DIV>'."\n";
			}
			echo '</TD>';

//!RIGHT SIDE - ITINERARY
		echo '<TD VALIGN="top" ALIGN="center" STYLE="width:52%; padding-left:6px; text-align:center; font-family:Arial; font-size:11pt;">'."\n\n";
			echo '<DIV ID="toverview" STYLE="padding-top:4px; font-family:Arial; font-size:12pt; text-align:center; min-height:300px;">'."\n";
			if(isset($fillform['steps']) && count($fillform['steps']) > 0){
			foreach($fillform['steps'] as $i => $step){
				$name = $itinerary['i'.$step['itid']]['name'];
				$subhead = $itinerary['i'.$step['itid']]['subhead'];
				echo '<DIV ID="tstep_'.$i.'" STYLE="border-bottom:1px solid #DDDDDD; background:#FFFFFF url(\'img/fade_r_grey.jpg\') center right repeat-y; padding:2px; text-align:left;" onMouseOver="makeDraggable(0,this);">';
					echo '<SPAN STYLE="padding:0px; float:right;" onMouseOver="delsetup(this);"><INPUT TYPE="button" VALUE="-" TITLE="Remove from tour" STYLE="font-size:9pt; width:30px; height:22px;" onClick="rmvfromit(\''.$i.'\');"></SPAN>';
					echo '&#149; '.$name.'<BR><SPAN STYLE="font-size:10px; color:#666666;">'.$subhead.'</SPAN>';
					echo '</DIV>'."\n";
				}
			} else {
			echo '<SPAN STYLE="font-family:Arial; font-size:14pt; color:#999999;"><BR>Add items here<BR>from the left column<BR><BR></SPAN>'."\n";
			}
			echo "\n";
			echo '</TD>';
		echo '</TR></TABLE>'."\n\n";

		echo '</DIV>';
		echo '</TD></TR>'."\n\n";
		*/

//bgcolor('reset');
//echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
//	echo '<TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Availability</TD>';
//	echo '</TR>'."\n\n";
	if(getval('id') != "*new*"){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Appears in tours</TD><TD STYLE="font-family:Arial; font-size:8pt;">';
		if(count($tours) > 0){
			foreach($tours as $key => $tour){
				echo '<DIV STYLE="padding-top:2px; padding-bottom:2px;';
				if($key > 0){ echo ' border-top:1px solid #C7C7C7;'; }
				echo '">'.$tour['id'].' : <A HREF="3_tours.php?edit='.$tour['id'].'">'.$tour['title'].'</A></DIV>';
				}
			} else {
			echo 'None';
			}
		echo '</TD></TR>'."\n";
		}
	/*echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Omit from Bundu Bus?<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">If "Yes", route can be made part of a tour, but will not be available on Bundu Bus.</SPAN></TD><TD><SELECT NAME="bbomit">';
		echo '<OPTION VALUE="0"'; if(getval('bbomit') == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if(getval('bbomit') == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT></TD></TR>'."\n";*/
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Include on Bundu Bus?</TD><TD><SELECT NAME="bbincl">';
		echo '<OPTION VALUE="a"'; if(getval('bbincl') == "a"): echo ' SELECTED'; endif; echo '>Always include</OPTION>';
		echo '<OPTION VALUE="c"'; if(getval('bbincl') == "c"): echo ' SELECTED'; endif; echo '>Include only with confirmed tours</OPTION>';
		echo '<OPTION VALUE="n"'; if(getval('bbincl') == "n"): echo ' SELECTED'; endif; echo '>Never include</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Hide from public view?</TD><TD><SELECT NAME="hidden">';
		echo '<OPTION VALUE="0"'; if(getval('hidden') == "0"): echo ' SELECTED'; endif; echo '>No</OPTION>';
		echo '<OPTION VALUE="1"'; if(getval('hidden') == "1"): echo ' SELECTED'; endif; echo '>Yes</OPTION>';
		echo '</SELECT></TD></TR>'."\n";
	echo '</TABLE><BR>'."\n\n";


echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A><BR>'."\n\n";

echo '<DIV ID="itfields" STYLE="display:none;">';
	if(is_array($fillform['steps'])) {
		foreach($fillform['steps'] as $i => $step){
			echo '<INPUT TYPE="hidden" NAME="it_ids[]" VALUE="'.$step['itid'].'">'."\n";
		}
	}
	echo '</DIV>'."\n\n";
echo '<DIV ID="forshadow" STYLE="display:none; cursor:move;"></DIV>'."\n\n";
echo '<DIV ID="fordel" STYLE="display:none;" onMouseOut="this.style.display=\'none\'"></DIV>'."\n\n";
echo '<DIV ID="delimgbtn" STYLE="display:none;" onMouseOver="this.style.display=\'\';" onMouseOut="this.style.display=\'none\';"></DIV>'."\n\n";


if(getval('id') != "*new*"){

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" WIDTH="93%"><BR>'."\n\n";

echo '<INPUT TYPE="button" ID="copybtn" VALUE="Copy this route" STYLE="width:180px;" onClick="document.getElementById(\'copybtn\').style.display=\'none\'; document.getElementById(\'copyroute\').style.display=\'\';"><BR>'."\n";

echo '<FORM METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="copy">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<DIV ID="copyroute" STYLE="display:none;">';
		echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">Create a new route with the following aspects of this route:<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Be sure to save changes you\'ve made to this route before copying.</SPAN></SPAN><BR>'."\n";
		echo '<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="0">'."\n\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="basic" ID="copy_basic" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_basic">Basic info, options, details, etc.</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="dates" ID="copy_dates" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_dates">Date scheme</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="itinerary" ID="copy_itinerary" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_itinerary">Itinerary</LABEL></TD></TR>'."\n";
		echo '<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="translations" ID="copy_translations" VALUE="y" CHECKED></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="copy_translations">Translations</LABEL></TD></TR>'."\n";
		echo '</TABLE>'."\n\n";
		echo '<INPUT TYPE="submit" VALUE="Copy" STYLE="width:180px;"><BR>'."\n";
		echo '</DIV>'."\n";
	echo '</FORM>'."\n\n";

echo '<INPUT TYPE="button" ID="delbtn" VALUE="Delete this route" STYLE="width:180px;" onClick="document.getElementById(\'delbtn\').style.display=\'none\'; document.getElementById(\'delroute\').style.display=\'\';"><BR>'."\n";

echo '<FORM METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n";
	echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n";
	echo '<DIV ID="delroute" STYLE="display:none;">';
		echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold; color:red;">Are you sure you want to delete this route?</SPAN><BR>'."\n";
		echo '<INPUT TYPE="submit" VALUE="Yes, delete" STYLE="width:180px; color:red;"><BR>'."\n";
		echo '</DIV>'."\n";
	echo '</FORM>'."\n\n";


} //End *new* if statement


//!ROUTES INDEX PAGE
} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
	for(i=0; i<invids.length; i++){
	document.getElementById("chkinv"+invids[i]).checked = document.getElementById("selall").checked;
	}
}

function del(){
	var r=confirm("Delete checked routes?");
	return r;
	//if(r==true){
	//document.getElementById('utaction').value = 'delete';
	//document.getElementById('routeform').submit();
	//}
}

//--></SCRIPT><?

include_once('sups/timezones.php');
$basetime = strtotime('2010-1-1');

//GET ROUTES
$routes = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS routes.*,';
	if($_SESSION['routes']['sortby'] == 'dep_name'){
		$query .= ' locations.`name` AS `dep_name`,';
		} elseif($_SESSION['routes']['sortby'] == 'arr_name'){
		$query .= ' locations.`name` AS `arr_name`,';
		}
	$query .= ' COUNT(routes_dates.`date`) AS `upcoming` FROM';
	if($_SESSION['routes']['sortby'] == 'dep_name'){
		$query .= ' (`routes` LEFT JOIN `locations` ON routes.`dep_loc` = locations.`id`)';
		} elseif($_SESSION['routes']['sortby'] == 'arr_name'){
		$query .= ' (`routes` LEFT JOIN `locations` ON routes.`arr_loc` = locations.`id`)';
		} else {
		$query .= ' `routes`';
		}
	$query .= ' LEFT JOIN `routes_dates` ON routes.`id` = routes_dates.`routeid` AND (routes_dates.`date` + routes.`dep_time`) > '.$time.' WHERE 1 = 1';
	if($_SESSION['routes']['showhidden'] != "1"): $query .= ' AND `hidden` = "0"'; endif;
	$query .= ' GROUP BY routes.`id`';
	$query .= ' ORDER BY '.$_SESSION['routes']['sortby'].' '.$_SESSION['routes']['sortdir'].', routes.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['routes']['p']-1)*$_SESSION['routes']['limit']).','.$_SESSION['routes']['limit'];
	//echo $query.'<BR>';

$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$routes['r'.$row['id']] = $row;
	}

$numitems = @mysqlQuery('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['routes']['limit']);
if($numpages > 0 && $_SESSION['routes']['p'] > $numpages): $_SESSION['routes']['p'] = $numpages; endif;

//echo '<INPUT TYPE="button" VALUE="New Route" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'"><BR><BR>'."\n\n";


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['routes']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['routes']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['routes']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;"><LABEL FOR="showhidden">Show hidden:</LABEL></TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="checkbox" NAME="showhidden" ID="showhidden" VALUE="1"';
		if($_SESSION['routes']['showhidden'] == "1"): echo ' CHECKED'; endif;
		echo '></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['routes']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['routes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['routes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['routes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['routes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['routes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" COLSPAN="2" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Departs</TD>';
	echo '<TD ALIGN="center" COLSPAN="2" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Arrives</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:8px; cursor:help;" TITLE="*Number of scheduled future dates">Dates</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; padding-right:8px; cursor:help;" TITLE="Checked routes appear on Bundu Bus/Google Transit">BB</TD>';
	//echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;" TITLE="Default Seats Available">Seats</TD>';
	//echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;" TITLE="Default Minimum PAX to Run">Min.</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($routes as $key => $row){
	$row['dep_name'] = $locations['l'.$row['dep_loc']]['name'];
	if(strlen($row['dep_name']) > 16){ $row['dep_name'] = substr($row['dep_name'],0,16).'...'; }
	$row['arr_name'] = $locations['l'.$row['arr_loc']]['name'];
	if(strlen($row['arr_name']) > 16){ $row['arr_name'] = substr($row['arr_name'],0,16).'...'; }

if($row['anchor'] == 'arr'){
	$dep_time = ($basetime+$row['arr_time']-$row['travel_time']+tz_offset($row['arr_loc'],$row['dep_loc']));
	$arr_time = ($basetime+$row['arr_time']);
	} else {
	$dep_time = ($basetime+$row['dep_time']);
	$arr_time = ($basetime+$row['dep_time']+$row['travel_time']+tz_offset($row['dep_loc'],$row['arr_loc']));
	}

echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selroutes[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt; '.tohide($row['hidden']).'"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:6px; padding-right:10px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">';
		if($row['hidden'] == '1'): echo '[HIDDEN] '; endif;
		echo '<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:8px; font-family:Arial; font-size:10pt; white-space:nowrap;'.tohide($row['hidden']).'">'.$row['dep_name'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">'.date("g:ia",$dep_time).'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:8px; font-family:Arial; font-size:10pt; white-space:nowrap;'.tohide($row['hidden']).'">'.$row['arr_name'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">'.date("g:ia",$arr_time).'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:8px; font-family:Arial; font-size:10pt; cursor:help;'.tohide($row['hidden']).'" TITLE="Number of scheduled future dates">'.$row['upcoming'].'</TD>';
	echo '<TD ALIGN="center" STYLE="padding-right:8px; font-family:Arial; font-size:10pt;">';
		if($row['bbincl'] != "n"){
		echo '<IMG SRC="img/check.png" BORDER="0" ALT="X" TITLE="This route appears on Bundu Bus/Google Transit" STYLE="cursor:help;">';
		}
		echo '</TD>';
	//echo '<TD ALIGN="right" STYLE="padding-right:5px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">'.$row['seats'].'</TD>';
	//echo '<TD ALIGN="right" STYLE="padding-right:5px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'">'.$row['minimum'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['routes']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['routes']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['routes']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['routes']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['routes']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New Route" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;" DISABLED>';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>