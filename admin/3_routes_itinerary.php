<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

//$_REQUEST['edit'] = 152; echo 'REMOVE THIS LINE!!!';

$working = 0;

$pageid = "3_routes";
require("validate.php");
//$onload = array('CreateDragContainer()');
require("header.php");

$today = mktime(0,0,0,date("j",$time),date("n",$time),date("Y",$time));

//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($_REQUEST,true)).'</PRE>'; exit;

if(!isset($_POST['delit'])){ $_POST['delit'] = array(); }
$numremoved = 0; $numupdated = 0;

foreach($_POST['id'] as $key => $id){
	$_POST['title'][$key] = encodeSQL(trim($_POST['title'][$key]));
	$_POST['map'][$key] = encodeSQL(trim($_POST['map'][$key]));
	$_POST['ml'][$key] = encodeSQL(trim($_POST['ml'][$key]));
	$_POST['time'][$key] = encodeSQL(trim($_POST['time'][$key]));
	$_POST['comments'][$key] = encodeSQL(trim($_POST['comments'][$key]));
	$_POST['youtube_title'][$key] = encodeSQL(trim($_POST['youtube_title'][$key]));
	$_POST['youtube'][$key] = encodeSQL(trim($_POST['youtube'][$key]));

	//INSERT NEW ---------------------
	if(substr($id,0,3) == 'new'){ if(trim($_POST['title'][$key]) != "" || trim($_POST['ml'][$key]) != "" || trim($_POST['time'][$key]) != "" || trim($_POST['comments'][$key]) != "" || trim($_POST['youtube_title'][$key]) != "" || trim($_POST['youtube'][$key]) != "" || $_POST['map'][$key] > 0 || count($_POST['imgs'.$id]) > 0){
	$query = 'INSERT INTO `itinerary`(`routeid`,`title`,`map`,`ml`,`time`,`comments`,`youtube_title`,`youtube`,`step`)';
		$query .= ' VALUES ("'.$_POST['edit'].'","'.trim($_POST['title'][$key]).'","'.$_POST['map'][$key].'","'.trim($_POST['ml'][$key]).'","'.trim($_POST['time'][$key]).'","'.trim($_POST['comments'][$key]).'","'.trim($_POST['youtube_title'][$key]).'","'.trim($_POST['youtube'][$key]).'","'.($key+1).'")';
		@mysql_query($query);
		$thiserror = mysql_error();
		$id = mysql_insert_id();
		if($thiserror == ""): $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;

	//UPDATE ESTABLISHED -------------
	}} else { if(trim($_POST['title'][$key]) == "" && trim($_POST['ml'][$key]) == "" && trim($_POST['time'][$key]) == "" && trim($_POST['comments'][$key]) == "" && trim($_POST['youtube_title'][$key]) == "" && trim($_POST['youtube'][$key]) == "" && $_POST['map'][$key] < 1 && count($_POST['imgs'.$id]) < 1){
	array_push($_POST['delit'],$id);
	} else {
	$query = 'UPDATE `itinerary` SET `title` = "'.$_POST['title'][$key].'", `map` = "'.$_POST['map'][$key].'", `ml` = "'.$_POST['ml'][$key].'", `time` = "'.$_POST['time'][$key].'", `comments` = "'.$_POST['comments'][$key].'", `youtube_title` = "'.$_POST['youtube_title'][$key].'", `youtube` = "'.$_POST['youtube'][$key].'", `step` = "'.($key+1).'"';
		$query .= ' WHERE `id` = "'.$id.'" LIMIT 1';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""): $numupdated=($numupdated+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
	}}

	//IMAGES -------------------------
	@mysql_query('DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.$id.'"');
	if(isset($_POST['imgs'.$_POST['id'][$key]])){
	foreach($_POST['imgs'.$_POST['id'][$key]] as $ord => $imgid){
		$query = 'INSERT INTO `images_assoc_itinerary`(`itid`,`imgid`,`order`) VALUES("'.$id.'","'.$imgid.'","'.$ord.'") ON DUPLICATE KEY UPDATE `order` = "'.$ord.'"';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""): array_push($errormsg,$thiserror); endif; //$imgupdated=($imgupdated+mysql_affected_rows()); else:
		} //End img foreach
		} //End if statement

	} //End Ids foreach

	//REMOVE STEPS -------------------
	if(isset($_POST['delit']) && count($_POST['delit']) > 0){
		foreach($_POST['delit'] as $del){
			$query = 'DELETE FROM `itinerary` WHERE `id` = "'.$del.'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""): $numremoved=($numremoved+mysql_affected_rows()); else: array_push($errormsg,$thiserror); endif;
			@mysql_query('DELETE FROM `images_assoc_itinerary` WHERE `itid` = "'.$del.'"');
			} //End For Loop
	} //END delit IF STATEMENT

	//UPDATE ROUTES_ASSOC -------------------------
	@mysql_query('DELETE FROM `routes_assoc` WHERE `routeid` = "'.$_REQUEST['edit'].'"');
	$query = 'INSERT INTO `routes_assoc`(`routeid`,`itid`,`order`) SELECT `routeid`, `id` AS `itid`, `step` AS `order` FROM `itinerary` WHERE `routeid` = "'.$_REQUEST['edit'].'"';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }

	//UPDATE NICKNAMES FOR TRANSLATION TOOL -------------------------
	$query = 'UPDATE `itinerary` SET `nickname` = CONCAT((SELECT `name` FROM `routes` WHERE `id` = "'.$_REQUEST['edit'].'")," - Step",itinerary.`step`), `modified` = itinerary.`modified` WHERE `routeid` = "'.$_REQUEST['edit'].'"';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }

	if($numremoved > 0): array_push($successmsg,$numremoved.' itinerary entires were removed. ('.$_POST['edit'].')'); endif;
	if($numupdated > 0): array_push($successmsg,$numupdated.' itinerary entries were added/updated. ('.$_POST['edit'].')'); endif;
}


if(!isset($_REQUEST['edit'])): $_REQUEST['edit'] = "*new*"; endif;

echo '<CENTER>'."\n\n";

echo '<IMG SRC="spacer.gif" BORDER="0" HEIGHT="12"><BR><FONT FACE="Arial" SIZE="5"><U>Edit Route Itinerary</U></FONT><BR><BR>'."\n\n";

echo routenav();

printmsgs($successmsg,$errormsg);


if($_REQUEST['edit'] != "*new*"){

//GET ROUTE INFO
$query = 'SELECT * FROM `routes` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
$routeinfo = @mysql_fetch_assoc($result);

//GET ITINERARY
$itinerary = array();
$it_ids = array();
$query = 'SELECT * FROM `itinerary` WHERE `routeid` = "'.$routeinfo['id'].'" ORDER BY `step` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$itinerary['i'.$row['id']] = $row;
	$itinerary['i'.$row['id']]['images_assoc'] = array();
	array_push($it_ids,$row['id']);
	}
	//array_push($it_ids,'new');

//GET IMAGES
$query = 'SELECT images_assoc_itinerary.* FROM `images_assoc_itinerary`,`itinerary` WHERE images_assoc_itinerary.`itid` = itinerary.`id` AND itinerary.`routeid` = "'.$routeinfo['id'].'" ORDER BY itinerary.`id` ASC, images_assoc_itinerary.`order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($itinerary['i'.$row['itid']]['images_assoc'],$row['imgid']);
	}
	//echo '<PRE>'; print_r($it_images); echo '</PRE>';
$images = array();
$query = 'SELECT DISTINCT images.* FROM `images`,`images_assoc_itinerary`,`itinerary` WHERE (images.`id` = images_assoc_itinerary.`imgid` AND images_assoc_itinerary.`itid` = itinerary.`id` AND itinerary.`routeid` = "'.$routeinfo['id'].'") OR (images.`id` = itinerary.`map` AND itinerary.`routeid` = "'.$routeinfo['id'].'")';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$images['i'.$row['id']] = $row;
	$images['i'.$row['id']]['data'] = imgform($row['filename'],180,90);
	}


function build_it_entry($entry,$count){
	global $images;
	$code = '';

	$code .= '<INPUT TYPE="hidden" NAME="id[]" VALUE="'.$entry['id'].'">';
	$code .= '<DIV STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:9pt; font-weight:bold; text-align:center; padding-bottom:4px;">';
		$code .= '<SPAN STYLE="cursor:help;" TITLE="Move this entry to a different position in the itinerary.">Order:</SPAN> <SELECT NAME="step[]" ID="step_'.$entry['id'].'" STYLE="font-size:9pt;" onChange="mvstep(this.value,this.parentNode.parentNode.parentNode.rowIndex);">'; //onClick="alert(this.id+\' \'+this.value);"
			for($i=0; $i<$count; $i++){
				$code .= '<OPTION VALUE="'.$i.'"';
				if($i == $entry['step']): $code .= ' SELECTED'; endif;
				$code .= '>'.($i+1).'</OPTION>';
				}
			$code .= '</SELECT>&nbsp;&nbsp;';
		$code .= 'Title: <INPUT TYPE="text" NAME="title[]" ID="title_'.$entry['id'].'" VALUE="'.$entry['title'].'" STYLE="font-size:9pt; width:210px;">&nbsp;&nbsp;';
		$code .= 'Miles/Km: <INPUT TYPE="text" NAME="ml[]" ID="ml_'.$entry['id'].'" VALUE="'.$entry['ml'].'" onKeyUp="to_km(this.value,document.getElementById(\'km_'.$entry['id'].'\'));" STYLE="font-size:9pt; width:50px;">/<INPUT TYPE="text" ID="km_'.$entry['id'].'" VALUE="';
			if($entry['ml'] != "" && $entry['ml'] > 0): $code .= number_format(($entry['ml']*1.609), 2, '.', ''); endif;
			$code .= '" onKeyUp="to_miles(this.value,document.getElementById(\'ml_'.$entry['id'].'\'));" STYLE="font-size:9pt; width:50px;">&nbsp;&nbsp;';
		$code .= 'Time: <INPUT TYPE="text" NAME="time[]" ID="time_'.$entry['id'].'" VALUE="'.$entry['time'].'" STYLE="font-size:9pt; width:90px;">';
		$code .= '</DIV>';

	/*
	$headline = array();
		if($entry['title'] != ""): array_push($headline,$entry['title']); endif;
		$dist = "";
		if($entry['ml'] != ""){
			$dist .= $entry['ml'].' mi';
			if($entry['ml'] != "" && $entry['ml'] > 0): $dist .= ' / '; endif;
			}
		if($entry['ml'] != "" && $entry['ml'] > 0): $dist .= number_format(($entry['ml']*1.609), 2, '.', '').' km'; endif;
		if($dist != ""): array_push($headline,$dist); endif;
		if($entry['time'] != ""): array_push($headline,$entry['time']); endif;
		$headline = implode(' &nbsp;-&nbsp; ',$headline);
	if($headline != ""): $code .= '<DIV STYLE="padding:4px; padding-left:8px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold;">'.$headline.'</DIV>'."\n"; endif;
	*/

	$code .= '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR><TD ALIGN="center" VALIGN="top" STYLE="padding:0px; padding-left:8px; padding-top:8px; width:120px; font-family:Arial; font-size:8pt;">';
		if($entry['map'] != "0"){
			$thisimage = imgform($images['i'.$entry['map']]['filename'],120,180);
			$code .= '<A HREF="../images/'.$images['i'.$entry['map']]['filename'].'" ID="map'.$entry['id'].'href" TARGET="_blank"><IMG SRC="../images/'.$thisimage['filename'].'" ID="map'.$entry['id'].'img" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" STYLE="border: #666666 solid 1px;" ALT="Map"></A>';
			} else {
			$code .= '<A ID="map'.$entry['id'].'href" TARGET="_blank"><IMG ID="map'.$entry['id'].'img" BORDER="0" STYLE="display:none; border: #666666 solid 1px;" ALT="Map"></A>';
			}
		$code .= '<BR><INPUT TYPE="hidden" ID="map'.$entry['id'].'" NAME="map[]" VALUE="'.$entry['map'].'"><INPUT TYPE="button" VALUE="Set map" STYLE="font-size:8pt; width:90px;" onClick="findimgs(\'o\',\'chgmap\',\'map'.$entry['id'].'\');"><BR><INPUT TYPE="button" VALUE="Remove map" STYLE="font-size:8pt; width:90px;" onClick="rmvmap(\'map'.$entry['id'].'\');">';
		$code .= '</TD><TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-right:0px; padding-bottom:0px; font-family:Arial,Helvetica,sans-serif; font-size:9pt;">';
		$code .= '<TEXTAREA NAME="comments[]" ID="comments_'.$entry['id'].'" STYLE="font-size:9pt; width:100%; height:220px;">'.$entry['comments'].'</TEXTAREA>';
		//$code .= nl2br($entry['comments']);

		$code .= '<DIV ID="img_assoc'.$entry['id'].'" STYLE="text-align:center;">';
		if(is_array($entry['images_assoc']) && count($entry['images_assoc']) > 0){
			foreach($entry['images_assoc'] as $key => $img){
				$thisimage = imgform($images['i'.$img]['filename'],180,90);
				$code .= ' <INPUT TYPE="hidden" NAME="imgs'.$entry['id'].'[]" VALUE="'.$img.'">';
				//$code .= '<A HREF="../images/'.$images['i'.$img]['filename'].'" TARGET="_blank" TITLE="'.$images['i'.$img]['caption'].'">';
				$code .= '<IMG SRC="../images/'.$thisimage['filename'].'" ID="'.$entry['id'].'_'.$key.'" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" ALT="'.$images['i'.$img]['caption'].'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(this); showdelimg(this,\''.$entry['id'].'\',\''.$key.'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
				//$code .= '</A>';
				}
			} //End images_assoc if statement
			$code .= '</DIV>';
			$code .= '<CENTER><SPAN STYLE="font-family:Arial; font-size:10px; font-weight:normal; color:#666666;">Hover over an image and click on "Remove" to remove.<BR>Click and drag images to change the order in which they appear.</SPAN><BR><INPUT TYPE="button" VALUE="Add images" STYLE="font-size:9pt;" onClick="findimgs(\'m\',\'addimg\',\''.$entry['id'].'\');"></CENTER>';

		$code .= '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Title: <INPUT NAME="youtube_title[]" ID="youtube_title_'.$entry['id'].'" STYLE="width:100%;" VALUE="'.str_replace('"','&quot;',$entry['youtube_title']).'"></DIV>';
		$code .= '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Video: (paste embed code below)<TEXTAREA NAME="youtube[]" ID="youtube_'.$entry['id'].'" STYLE="font-size:9pt; width:100%; height:60px;">'.$entry['youtube'].'</TEXTAREA></DIV>';

		$code .= '</TD></TR></TABLE>'."\n";

		$code .= '<DIV STYLE="font-family:Arial; font-size:10px; text-align:center;"><A HREF="#top" STYLE="text-decoration:underline;">TOP</A></DIV>'."\n";

	return $code;
	}


echo '<A NAME="top"></A>'."\n";

echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">
<INPUT TYPE="hidden" NAME="utaction" VALUE="update">
<INPUT TYPE="hidden" NAME="edit" VALUE="'.$routeinfo['id'].'">

<FONT FACE="Arial" SIZE="4">'.$routeinfo['name'].' ('.$routeinfo['id'].')</FONT><BR><BR>'."\n\n";

//echo '<script src="dragdrop.js" type="text/javascript"></script>'."\n";


?><SCRIPT><!--

var itineraryids = new Array('<? echo implode("','",$it_ids); ?>');
var newids = new Array();

var itineraryimgs = new Array();
<?
	foreach($itinerary as $it){
		$imgs = array_values($it['images_assoc']);
		echo "\t".'itineraryimgs[\'i'.$it['id'].'\'] = new Array(';
			if(count($imgs) > 0): echo '\''.implode("','",$imgs).'\''; endif;
			echo ');';
		}
	?>
	//itineraryimgs['inew'] = new Array();

var image_data = new Array();
<?
	foreach($images as $img){
		$img['filename'] = str_replace("'",'\\\'',$img['filename']);
		$img['data']['filename'] = str_replace("'",'\\\'',$img['data']['filename']);
		$img['caption'] = str_replace("'",'\\\'',$img['caption']);
		echo "\t".'image_data[\'i'.$img['id'].'\'] = new Array();';
		echo ' image_data[\'i'.$img['id'].'\'][\'large\']=\''.$img['filename'].'\';';
		foreach($img['data'] as $key => $val){
			echo ' image_data[\'i'.$img['id'].'\'][\''.$key.'\']=\''.$val.'\';';
			}
		echo ' image_data[\'i'.$img['id'].'\'][\'caption\']=\''.$img['caption'].'\';';
		echo "\n";
		}
	?>

function to_miles(km,obj){
	var miles = eval(km*0.6214);
	obj.value = miles.toFixed(2);
	}

function to_km(miles,obj){
	var km = eval(miles*1.609);
	obj.value = km.toFixed(2);
	}

function addstep(){
	var n = newids.length;
		var nid = 'new'+n;
		newids.push(n);
		itineraryids.push(nid);
		itineraryimgs['i'+nid] = new Array();

	var code = '<INPUT TYPE="hidden" NAME="id[]" VALUE="'+nid+'">';
		code += '<DIV STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:9pt; font-weight:bold; text-align:center; padding-bottom:4px;">';
		code += '<SPAN STYLE="cursor:help;" TITLE="Move this entry to a different position in the itinerary.">Order:</SPAN>';
		code += '<SELECT NAME="step[]" ID="step_'+nid+'" STYLE="font-size:9pt;" onChange="mvstep(this.value,this.parentNode.parentNode.parentNode.rowIndex);">';
				for(i=0; i<itineraryids.length; i++){
				code += '<OPTION VALUE="'+i+'">'+eval(i+1)+'</OPTION>';
				}
				code += '</SELECT>&nbsp;&nbsp;';
		code += 'Title: <INPUT TYPE="text" NAME="title[]" ID="title_'+nid+'" VALUE="" STYLE="font-size:9pt; width:220px;">&nbsp;&nbsp;';
		code += 'Miles/Km: <INPUT TYPE="text" NAME="ml[]" ID="ml_'+nid+'" VALUE="" onKeyUp="to_km(this.value,document.getElementById(\'km_'+nid+'\'));" STYLE="font-size:9pt; width:50px;">/<INPUT TYPE="text" ID="km_'+nid+'" VALUE="" onKeyUp="to_miles(this.value,document.getElementById(\'ml_'+nid+'\'));" STYLE="font-size:9pt; width:50px;">&nbsp;&nbsp;';
		code += 'Time: <INPUT TYPE="text" NAME="time[]" ID="time_'+nid+'" VALUE="" STYLE="font-size:9pt; width:100px;">';
		code += '</DIV>';
		code += '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>';
		code += '<TD ALIGN="center" VALIGN="top" STYLE="padding:0px; padding-left:8px; padding-top:8px; width:120px; font-family:Arial; font-size:8pt;">';
		code += '<A ID="map'+nid+'href" TARGET="_blank"><IMG ID="map'+nid+'img" BORDER="0" STYLE="display:none; border: #666666 solid 1px;" ALT="Map"></A><BR>';
		code += '<INPUT TYPE="hidden" ID="map'+nid+'" NAME="map[]" VALUE="0">';
		code += '<INPUT TYPE="button" VALUE="Set map" STYLE="font-size:8pt; width:90px;" onClick="findimgs(\'o\',\'chgmap\',\'map'+nid+'\');"><BR>';
		code += '<INPUT TYPE="button" VALUE="Remove map" STYLE="font-size:8pt; width:90px;" onClick="rmvmap(\'map'+nid+'\');">';
		code += '</TD>';
		code += '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-right:0px; padding-bottom:0px; font-family:Arial,Helvetica,sans-serif; font-size:9pt;">';
		code += '<TEXTAREA NAME="comments[]" ID="comments_'+nid+'" STYLE="font-size:9pt; width:100%; height:220px;"></TEXTAREA>';
		code += '<DIV ID="img_assoc'+nid+'" STYLE="text-align:center;"></DIV>';
		code += '<CENTER><SPAN STYLE="font-family:Arial; font-size:10px; font-weight:normal; color:#666666;">Hover over an image and click on "Remove" to remove.<BR>Click and drag images to change the order in which they appear.</SPAN><BR><INPUT TYPE="button" VALUE="Add images" STYLE="font-size:9pt;" onClick="findimgs(\'m\',\'addimg\',\''+nid+'\');"></CENTER>';
		code += '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Title: <INPUT NAME="youtube_title[]" ID="youtube_title_'+nid+'" STYLE="width:100%;" VALUE=""></DIV>';
		code += '<DIV STYLE="margin-top:6px; font-size:10pt; text-align:center;">YouTube Video: (paste embed code below)<TEXTAREA NAME="youtube[]" ID="youtube_'+nid+'" STYLE="font-size:9pt; width:100%; height:60px;"></TEXTAREA></DIV>';
		code += '</TD>';
		code += '</TR></TABLE>';
		code += '<DIV STYLE="font-family:Arial; font-size:10px; text-align:center;"><A HREF="#top" STYLE="text-decoration:underline;">TOP</A></DIV>';

	var t = document.getElementById('ittable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.backgroundColor = '#CCCCCC';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '14pt';
		d.style.color = '#666666';
		d.style.padding = '4px';
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = '&nbsp;';

		var d = r.insertCell(1);
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = code;

	ref_ord();
	CreateDragContainer();
	}

function mvstep(to,from){
	if(to != from){
		var t = document.getElementById('ittable');
		var oid = itineraryids[from];

		//Save old data
		var ohtml = t.rows[from].cells[1].innerHTML;
		var otitle = document.getElementById('title_'+oid).value;
		var oml = document.getElementById('ml_'+oid).value;
		var okm = document.getElementById('km_'+oid).value;
		var otime = document.getElementById('time_'+oid).value;
		var ocomments = document.getElementById('comments_'+oid).value;
		var oyoutube_title = document.getElementById('youtube_title_'+oid).value;
		var oyoutube = document.getElementById('youtube_'+oid).value;

		t.deleteRow(from);
		itineraryids.splice(from,1);

		var r = t.insertRow(to);
		itineraryids.splice(to,0,oid);

		var d = r.insertCell(0);
		d.style.backgroundColor = '#CCCCCC';
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '14pt';
		d.style.color = '#666666';
		d.style.padding = '4px';
		d.style.borderBottom = '4px solid #FFFFFF';
		d.innerHTML = '&nbsp;';

		var d = r.insertCell(1);
		d.style.borderBottom = '4px solid #FFFFFF';

		//Copy old data
		d.innerHTML = ohtml;
		document.getElementById('title_'+oid).value = otitle;
		document.getElementById('ml_'+oid).value = oml;
		document.getElementById('km_'+oid).value = okm;
		document.getElementById('time_'+oid).value = otime;
		document.getElementById('comments_'+oid).value = ocomments;
		document.getElementById('youtube_title_'+oid).value = oyoutube_title;
		document.getElementById('youtube_'+oid).value = oyoutube;

		ref_ord();
		CreateDragContainer();
		}
	}

function rmvstep(obj){
	var i = obj.parentNode.parentNode.rowIndex;

	var c = confirm('Delete itinerary entry '+eval(i+1)+'?');
	if(c){
		document.getElementById('delfields').innerHTML += '<INPUT TYPE="hidden" NAME="delit[]" VALUE="'+itineraryids[i]+'">';

		var t = document.getElementById('ittable');
		itineraryimgs['i'+itineraryids[i]] = new Array();
		itineraryids.splice(i,1);
		t.deleteRow(i);

		ref_ord();
		CreateDragContainer();
		}

	}

function ref_ord(){
	var t = document.getElementById('ittable');

	for(i=0; i<t.rows.length; i++){
		t.rows[i].cells[0].innerHTML = eval(i+1)+'<BR><BR><INPUT TYPE="button" VALUE="X" TITLE="Remove this entry" STYLE="color:#FF0000; width:22px; text-align:center;" onClick="rmvstep(this);">';
		}

	for(i=0; i<itineraryids.length; i++){
		var s = document.getElementById('step_'+itineraryids[i]);
		while(s.length > 0){ s.remove(0); }
		for(ii=0; ii<itineraryids.length; ii++){
			var o = document.createElement('option');
			o.value = ii;
			o.text = eval(ii+1);
			try{ s.add(o,null); } catch(ex){ s.add(o); }
			}
		s.value = s.parentNode.parentNode.parentNode.rowIndex;
		}
	}

function findimgs(choose,func,fid){
	var win = window.open("3_imagewin.php?choose="+choose+"&func="+func+"&fid="+fid,"findimages","width=790,height=600,scrollbars=yes,resizable=yes");
	}

function chgmap(file,thumb,w,h,id,fid){
	var rand = Math.random();
	document.getElementById(fid+'img').style.display = '';
	document.getElementById(fid+'img').src = '../images/'+thumb+'?'+rand;
	document.getElementById(fid+'img').style.width = w;
	document.getElementById(fid+'img').style.height = h;
	document.getElementById(fid+'href').href = '../images/'+file;
	document.getElementById(fid).value = id;
	}

function rmvmap(fid){
	document.getElementById(fid+'img').style.display = 'none';
	document.getElementById(fid+'img').src = '';
	document.getElementById(fid+'href').href = '';
	document.getElementById(fid).value = '0';
	}

function addimg(file,thumb,w,h,id,fid){
	image_data['i'+id] = new Array();
		image_data['i'+id]['large'] = file;
		image_data['i'+id]['filename'] = thumb;
		image_data['i'+id]['w'] = w;
		image_data['i'+id]['h'] = h;
		image_data['i'+id]['caption'] = '';

	if(testIsValidObject(itineraryimgs['i'+fid])){
		itineraryimgs['i'+fid].push(id);
		} else {
		itineraryimgs['i'+fid] = new Array(id);
		}

	rebuildimgs(fid);
	CreateDragContainer();
	}

function rmvimg(fid,i){
	document.getElementById('delimgbtn').style.display = 'none';
	itineraryimgs['i'+fid].splice(i,1);
	rebuildimgs(fid);
	CreateDragContainer();
	}

function rebuildimgs(fid){
	x = document.getElementById('img_assoc'+fid);
	var code = '';
	for(i in itineraryimgs['i'+fid]){
		var id = itineraryimgs['i'+fid][i];
		code += ' <INPUT TYPE="hidden" NAME="imgs'+fid+'[]" VALUE="'+id+'">';
		//code += '<A HREF="../images/'+image_data['i'+id]['large']+'" TARGET="_blank" TITLE="'+image_data['i'+id]['caption']+'">';
		code += '<IMG SRC="../images/'+image_data['i'+id]['filename']+'" BORDER="0" WIDTH="'+image_data['i'+id]['w']+'" HEIGHT="'+image_data['i'+id]['h']+'" ID="'+fid+'_'+i+'" ALT="'+image_data['i'+id]['caption']+'" STYLE="border:4px solid #FFFFFF;" onMouseOver="makeDraggable(this); showdelimg(this,\''+fid+'\',\''+i+'\');" onMouseOut="document.getElementById(\'delimgbtn\').style.display=\'none\';">';
		//code += '</A>';
		}
	x.innerHTML = code;
	}

function showdelimg(obj,fid,i){
	var x = document.getElementById('delimgbtn');

	var newcode = '<INPUT TYPE="button" VALUE="Remove" STYLE="font-size:8pt;" onClick="rmvimg(\''+fid+'\',\''+i+'\')">';
	x.innerHTML = newcode;
	x.style.display = "";
	x.style.position = "absolute";

	var boxpos = findPos(obj);
	x.style.top = eval(boxpos[1]) + 'px';
	x.style.left = eval(boxpos[0] - 2) + 'px';
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

//BEING DRAG AND DROP FUNCTIONS
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = 'n';
var dragTarget = 'n';
var mouseOffset = null;
var dropTargets = new Array();

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function addDropTarget(dropTarget){
	dropTargets.push(dropTarget);
}

function CreateDragContainer(){
	dropTargets = new Array();
	for(a in itineraryids){
		fid = itineraryids[a];
		for(i=0; i<itineraryimgs['i'+fid].length; i++){
			addDropTarget( document.getElementById(fid+'_'+i) );
		}
	}
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets.length > 1){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(item){
	if(!item) return;
	item.onmousedown = function(ev){

		shadow = document.getElementById("forshadow");
		dragObject  = shadow;

		shadow.innerHTML = '<IMG SRC="'+item.src+'" WIDTH="'+eval(item.offsetWidth-8)+'" HEIGHT="'+eval(item.offsetHeight-8)+'" BORDER="0">'; // STYLE="border:4px solid #FFFFFF"

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin = 'n';
		for(var i=0; i<dropTargets.length; i++){
			if(item.id == dropTargets[i].id){
			dragOrigin = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin   = 'n';
	cleartarg();
}

function movem(){
	if(dragOrigin != 'n' && dragTarget != 'n'){

	o = dropTargets[dragOrigin].id.split('_');
	n = dropTargets[dragTarget].id.split('_');

	var op = document.getElementById(dropTargets[dragOrigin].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var np = document.getElementById(dropTargets[dragTarget].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var oa = o[1];

	if(np < op){
		n[1]++;
	} else if(np > op){
		//oa++;
	} else if(dragTarget < dragOrigin){
		oa++;
	} else if(dragTarget > dragOrigin){
		n[1]++;
	}

	itineraryimgs['i'+n[0]].splice(n[1],0,itineraryimgs['i'+o[0]][o[1]]);
	itineraryimgs['i'+o[0]].splice(oa,1);
	//alert(n[0]+' '+n[1]+' '+o[0]+' '+o[1]);

	rebuildimgs(o[0]);
	rebuildimgs(n[0]);
	CreateDragContainer();
	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(var i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';

		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				//newTarget = dropTargets[i].id;
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget){
		cleartarg();
		dragTarget = newTarget;
			var x = document.getElementById(dropTargets[newTarget].id);
			var o = document.getElementById(dropTargets[dragOrigin].id).parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
			var n = x.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;

			if(n < o){
				x.style.borderRight = "4px solid #0000FF";
			} else if(n > o){
				x.style.borderLeft = "4px solid #0000FF";
			} else if(newTarget < dragOrigin){
				x.style.borderLeft = "4px solid #0000FF";
			} else if(newTarget > dragOrigin){
				x.style.borderRight = "4px solid #0000FF";
			}
		}
	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget != 'n' && testIsValidObject( dropTargets[dragTarget] )){
		document.getElementById( dropTargets[dragTarget].id ).style.borderLeft = "4px solid #FFFFFF";
		document.getElementById( dropTargets[dragTarget].id ).style.borderRight = "4px solid #FFFFFF";
		}
	dragTarget = 'n';
}

//--></SCRIPT><? echo "\n\n";


echo '<TABLE BORDER="0" WIDTH="93%" CELLPADDING="0" CELLSPACING="0" ID="ittable">'."\n";

	$step=0;
	bgcolor('');
	foreach($itinerary as $row){
	echo '<TR><TD BGCOLOR="#CCCCCC" ALIGN="center" STYLE="font-family:Arial; font-size:14pt; color:#666666; padding:4px; border-bottom:4px solid #FFFFFF;">'.($step+1).'<BR><BR><INPUT TYPE="button" VALUE="X" TITLE="Remove this entry" STYLE="color:#FF0000; width:22px; text-align:center;" onClick="rmvstep(this);"></TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="border-bottom:4px solid #FFFFFF;">'."\n";
		$row['step'] = $step;
		echo "\t".build_it_entry($row,count($itinerary));
		echo "\t".'</TD></TR>'."\n\n";
		$step++;
		}

	/*echo '<TR><TD BGCOLOR="#CCCCCC" ALIGN="center" STYLE="font-family:Arial; font-size:14pt; color:#666666; padding:4px; border-bottom:4px solid #FFFFFF;">'.($step+1).'</TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="border-bottom:4px solid #FFFFFF;">'."\n";
		$newrow = array('id'=>'new','routeid'=>$routeinfo['id'],'title'=>'','map'=>'0','ml'=>'','time'=>'','comments'=>'','step'=>$step);
		echo "\t".build_it_entry($newrow,(count($itinerary)+1));
		echo "\t".'</TD></TR>'."\n\n";*/

echo '</TABLE>';

echo '<INPUT TYPE="button" VALUE="Add another entry" onClick="addstep();"><BR><BR>'."\n\n";

echo '<DIV STYLE="width:93%; background:#CCCCCC; padding:3px;"><INPUT TYPE="submit" VALUE="Save Itinerary" STYLE="width:180px;"></DIV><BR>'."\n\n";

echo '<A HREF="3_routes.php?edit='.$routeinfo['id'].'" STYLE="font-family:Arial; font-size:12pt;">Back to edit main route/options</A><BR><BR>'."\n\n";
echo '<A HREF="3_routes.php" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A><BR><BR>'."\n\n";

echo '<DIV ID="delfields" STYLE="display:none;"></DIV>'."\n\n";

echo '</FORM>'."\n\n";

echo '<DIV ID="forshadow" STYLE="display:none; cursor:move;"></DIV>'."\n\n";
echo '<DIV ID="delimgbtn" STYLE="display:none;" onMouseOver="this.style.display=\'\';" onMouseOut="this.style.display=\'none\';"></DIV>'."\n\n";


} else {

echo '<FONT FACE="Arial" SIZE="2"><A HREF="3_routes.php">Click Here</A> to choose a route to edit.</FONT>';

} //END edit NEW IF STATEMENT


require("footer.php");

?>