<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_schedule";
require("validate.php");

if(!isset($_REQUEST['start']) || trim($_REQUEST['start']) == ""){
	$_REQUEST['start'] = $today;
	$_SERVER['QUERY_STRING'] = 'start='.$_REQUEST['start'];
	}
if(!isset($_REQUEST['end']) || trim($_REQUEST['end']) == ""){ $_REQUEST['end'] = $_REQUEST['start']; }
if($_REQUEST['end'] < $_REQUEST['start']){ $_REQUEST['end'] = $_REQUEST['start']; }
if(isset($_REQUEST['start_month']) && trim($_REQUEST['start_month']) != "" && isset($_REQUEST['start_day']) && trim($_REQUEST['start_day']) != "" && isset($_REQUEST['start_year']) && trim($_REQUEST['start_year']) != ""){
	$_REQUEST['start'] = mktime(0,0,0,$_REQUEST['start_month'],$_REQUEST['start_day'],$_REQUEST['start_year']);
	}
if(isset($_REQUEST['end_month']) && trim($_REQUEST['end_month']) != "" && isset($_REQUEST['end_day']) && trim($_REQUEST['end_day']) != "" && isset($_REQUEST['end_year']) && trim($_REQUEST['end_year']) != ""){
	$_REQUEST['end'] = mktime(0,0,0,$_REQUEST['end_month'],$_REQUEST['end_day'],$_REQUEST['end_year']);
	}

require("header.php");


//echo '<PRE>'; print_r($_REQUEST); echo '</PRE>';

$conv_types = array(
	'a' => 'Activity/Tour option',
	'r' => 'Bundu route',
	't' => 'Bundu tour',
	'l' => 'Lodging',
	's' => 'Shuttle',
	'p' => 'Tour transport',
	'o' => 'Other'
	);

//GET TOURS
$tours = array();
$query = 'SELECT tours.`id`,tours.`title` FROM `tours`,`reservations_assoc` WHERE reservations_assoc.`tourid` = tours.`id` AND reservations_assoc.`canceled` = 0 ORDER BY `id` DESC, `title` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = $row['id'].' : '.str_replace("'",'',$row['title']);
	if(strlen($row['subhead']) > 90){ $row['subhead'] = substr($row['subhead'],0,90).'...'; }
	$tours['t'.$row['id']] = $row;
	}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Schedule</U></FONT><BR>'."\n\n";
	echo '<BR>'."\n\n";


?><script language="javascript"><!--

function adjustend(adddays){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("start_year").value,eval(document.getElementById("start_month").value-1),document.getElementById("start_day").value);

	var findend = startdate.getTime();
	findend = eval(findend + (86400000*eval(adddays)));

	var end = new Date();
	end.setTime(findend);

	document.getElementById("end_month").value = eval(end.getMonth()+1);
	document.getElementById("end_day").value = end.getDate();
	document.getElementById("end_year").value = end.getFullYear();
}

function checkend(){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("start_year").value,eval(document.getElementById("start_month").value-1),document.getElementById("start_day").value);
	var chkdate = startdate.getTime();

	var enddate = new Date();
	enddate.setFullYear(document.getElementById("end_year").value,eval(document.getElementById("end_month").value-1),document.getElementById("end_day").value);
	var chkend = enddate.getTime();

	if(chkend < chkdate){ adjustend(0); }
}

//--></script><? echo "\n\n";


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<TABLE BORDER="0" BGCOLOR="#D9DBEC" WIDTH="94%" CELLPADDING="0"><TR><TD ALIGN="center">';
	echo '<TABLE BORDER="0"><TR>';
		echo '<TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>Date Range:&nbsp;</B></FONT></TD>';
		echo '<TD><SELECT NAME="start_month" ID="start_month" onChange="adjustend(0)">';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("n",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="start_day" ID="start_day" onChange="adjustend(0)">';
			for($i=1; $i<32; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("j",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="start_year" ID="start_year" onChange="adjustend(0)">';
			for($i=2006; $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("Y",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD>';
		echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><B>&nbsp;-&nbsp;</B></FONT></TD>';
		echo '<TD><SELECT NAME="end_month" ID="end_month" onChange="checkend()">';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("n",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="end_day" ID="end_day" onChange="checkend()">';
			for($i=1; $i<32; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("j",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="end_year" ID="end_year" onChange="checkend()">';
			for($i=2006; $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("Y",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD>';
		echo '<TD ROWSPAN="2"><INPUT TYPE="submit" VALUE="View"></TD>';
		echo '</TR><TR>';
		echo '<TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>View:&nbsp;</B></FONT></TD>';
		echo '<TD COLSPAN="3"><SELECT NAME="filter" ID="filter" STYLE="width:420px;"><OPTION VALUE="*all*">All Tours, Shuttle, Activities, etc.</OPTION>';
			foreach($conv_types as $key => $type){
				echo '<OPTION VALUE="'.$key.'"';
				if(isset($_REQUEST['filter']) && $key == $_REQUEST['filter']){ echo ' SELECTED'; }
				echo '>'.$type.'</OPTION>';
				}
			foreach($tours as $key => $tour){
				echo '<OPTION VALUE="'.$tour['id'].'"';
				if(isset($_REQUEST['filter']) && $tour['id'] == $_REQUEST['filter']){ echo ' SELECTED'; }
				echo '>'.$tour['subhead'].'</OPTION>';
				}
			echo '</SELECT></TD>';
	echo '</TR></TABLE>';
echo '</TD></TR></TABLE>';
echo '</FORM>';

echo '<DIV STYLE="width:94%; background-color:#EEEEEE; text-align:center; font-family:Arial; font-size:12pt;"><A HREF="viewsched.php?start='.$_REQUEST['start'].'&end='.$_REQUEST['end'].'&filter='.$_REQUEST['filter'].'&print=y" TARGET="_blank">Print this schedule</A></DIV><BR>'."\n\n";

	include('viewsched.php');

echo '<DIV STYLE="width:94%; background-color:#EEEEEE; text-align:center; font-family:Arial; font-size:12pt;"><A HREF="viewsched.php?start='.$_REQUEST['start'].'&end='.$_REQUEST['end'].'&filter='.$_REQUEST['filter'].'&print=y" TARGET="_blank">Print this schedule</A></DIV><BR>'."\n\n";

require("footer.php");

?>