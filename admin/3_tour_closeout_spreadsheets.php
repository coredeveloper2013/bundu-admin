<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$pageid = "3_tour_closeout_spreadsheets";
require_once("validate.php");


$command = @$_REQUEST['command'];


if($command == "download") {
	$data = array();
	$ssYear = dateFormat("now", "Y");
	$datetime_prev_closures = false;
	$file_name = 'closeout.csv';

	if(@$_REQUEST['id'] > 0) {
		$ssObj = new closeout_spreadsheet($_REQUEST['id']);
		if($ssObj->id > 0) {
			$data = $ssObj->getData();
			$ssYear = dateFormat($ssObj->getDateTimeCreated(), "Y");
			$datetime_prev_closures = $ssObj->getDateTimeCreated();
			//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($data,true)).'</PRE>';
		}
	}

	if(@$_REQUEST['date'] != "") {
		$query = 'SELECT * FROM closeout_spreadsheets WHERE date = "'.mysql_real_escape_string(mysqlDate($_REQUEST['date'])).'"';
		$result = @mysqlQuery($query);
		while($row = @mysql_fetch_assoc($result)) {
			$row_data = json_decode($row['data'], true);
			foreach($row_data as $id_tour => $dates) {
				if(!isset($data[$id_tour])) {
					$data[$id_tour] = array();
				}
				$data[$id_tour] = array_merge($data[$id_tour], $dates);
				sort($data[$id_tour]);
			}
		}
		$ssYear = dateFormat($_REQUEST['date'], "Y");
		$datetime_prev_closures = mysqlDateTime($_REQUEST['date'].' - 1 second');
		$file_name = mysqlDate($_REQUEST['date']).' closeouts.csv';
	}

	if(count($data) > 0) {
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$file_name);

		$output = fopen('php://output', 'w');

		foreach($data as $id_tour => $tour_dates) {
			$tourObj = new tour($id_tour);

			$title = $tourObj->getNickname();
			if($title == "") {
				$title = $tourObj->getTitle();
			}
			fputcsv($output, array($id_tour, $title));

			$new_dates_row = array("Newly closed dates:");
			$new_dates_row = array_merge($new_dates_row, $tour_dates);
			fputcsv($output, $new_dates_row);

			$old_dates_row = array();
			$query = 'SELECT id
						FROM closeout_spreadsheets
						WHERE datetime_created <= "'.mysql_real_escape_string($datetime_prev_closures).'"
							AND data LIKE "%'.mysql_real_escape_string($id_tour).'%"';
			$oldIDs = getMysqlValues($query);
			foreach($oldIDs as $old_ss_id) {
				$oldSSobj = new closeout_spreadsheet($old_ss_id);
				$oldData = $oldSSobj->getData();
				foreach($oldData[$id_tour] as $old_date) {
					//if(mysqlDate($old_date) >= mysqlDate($oldSSobj->getDateTimeCreated())) {
					if(mysqlDate($old_date) >= mysqlDate($ssYear.'-01-01')) {
						$old_dates_row[] = $old_date;
					}
				}
			}

			if(count($old_dates_row) > 0) {
				sort($old_dates_row);
				array_unshift($old_dates_row, "Previously closed dates:");
				fputcsv($output, $old_dates_row);
			}

			fputcsv($output, array(" "));
		}

	} else {
		echo 'Error: Missing/invalid spreadsheet ID or date.';
	}
	
	exit;
}


require_once("header.php");


//Sticky filter
if(!is_array($_SESSION['tour_closeout_spreadsheets']) || isset($_REQUEST['clear'])){
	$_SESSION['tour_closeout_spreadsheets'] = array();
}
$sessParams = &$_SESSION['tour_closeout_spreadsheets'];

$fields = array('p');
foreach($fields as $key){
	if(isset($_REQUEST[$key])){
		$sessParams[$key] = $_REQUEST[$key];
	}
}
if(!is_numeric($sessParams['p']) || $sessParams['p'] < 1) {
	$sessParams['p'] = 1;
}
$limit = 100;



echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Tour Close Out Spreadsheets</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if($command == "") {

	//GET SPREADSHEETS - Group By Date
	$rows = array();
	$query = 'SELECT SQL_CALC_FOUND_ROWS *
				FROM closeout_spreadsheets
				LIMIT '.(($sessParams['p']-1)*$limit).','.$limit;
	$result = mysqlQuery($query);
	while($row = @mysql_fetch_assoc($result)) {
		$date = $row['date'];
		$data = json_decode($row['data'], true);
		foreach($data as $id_tour => $dates) {
			if(!isset($rows[$date][$id_tour])) {
				$rows[$date][$id_tour] = array();
			}
			$rows[$date][$id_tour] = array_merge($rows[$date][$id_tour], $dates);
		}
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($rows,true)).'</PRE>';

	$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
	$numitems = mysql_fetch_assoc($numitems);
	$numitems = $numitems['numitems'];
	$numpages = ceil($numitems / $limit);
	if($numpages > 0 && $sessParams['p'] > $numpages): $sessParams['p'] = $numpages; endif;
	
	
	/*
	echo '<FORM METHOD="GET" ACTION="'.$_SERVER['SCRIPT_NAME'].'">
		<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";
	
	echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
		echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
		echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
		echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
			foreach($sortby as $key => $sort){
				echo '<OPTION VALUE="'.$key.'"';
				if($_SESSION['activities']['sortby'] == $key): echo " SELECTED"; endif;
				echo '>'.$sort.'</OPTION>'."\n";
				}
			echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
				echo '<OPTION VALUE="ASC"'; if($_SESSION['activities']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
				echo '<OPTION VALUE="DESC"'; if($_SESSION['activities']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
				echo '</SELECT></TD>'."\n";
		echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
		echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$limit.'"></TD>'."\n";
		echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
		echo '	</TR></TABLE>'."\n";
	echo '</CENTER></DIV>'."\n";
	
	echo '</FORM>';
	*/

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($sessParams['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($sessParams['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $sessParams['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($sessParams['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($sessParams['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing
	
	echo '<TABLE BORDER="0" CELLSPACING="0" STYLE="width:94%">'."\n";

	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Date Created</TD>';
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;"># Tours Closed Out</TD>';
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;"># Dates Closed Out</TD>';
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Download Link</TD>';
		echo '</TR>'."\n\n";

	bgcolor('');

	if(count($rows) == 0) {
		?> 
		<tr style="background:#<?=bgcolor('')?>">
			<td align="center" colspan="4">
				<br>
				- No spreadsheets found -
			</td>
		</tr>
		<?
	}

	foreach($rows as $date => $data) {
		$num_dates = 0;
		foreach($data as $id_tour => $dates) {
			$num_dates += count($dates);
		}
		echo '<TR STYLE="background:#'.bgcolor('').'">';
			echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.dateFormat($date, "m/d/Y").'</TD>';
			echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.count($data).'</TD>';
			echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$num_dates.'</TD>';
			echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><a href="'.$_SERVER['SCRIPT_NAME'].'?command=download&date='.$date.'">Download</a></TD>';
		echo '</TR>'."\n\n";
	}
	
	echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($sessParams['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($sessParams['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $sessParams['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($sessParams['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($sessParams['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

}


require("footer.php");

?>