<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_tours_extlodging";
require("validate.php");
require("header.php");

if(!isset($_SESSION['tours_extlodging']['p'])): $_SESSION['tours_extlodging']['p'] = 1; endif;
if(!isset($_SESSION['tours_extlodging']['sortby'])): $_SESSION['tours_extlodging']['sortby'] = "tours_extlodging.`name`"; endif;
	$sortby = array('tours_extlodging.`id`'=>'ID','tours_extlodging.`name`'=>'Name','lodge_name'=>'Lodging','tours_extlodging.`perguest`'=>'Per guest','tours_extlodging.`single`'=>'Single','tours_extlodging.`triple`'=>'Triple','tours_extlodging.`quad`'=>'Quad');
if(!isset($_SESSION['tours_extlodging']['sortdir'])): $_SESSION['tours_extlodging']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['tours_extlodging']['limit'])): $_SESSION['tours_extlodging']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['tours_extlodging']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['tours_extlodging']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['tours_extlodging']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['tours_extlodging']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['tours_extlodging']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($_POST['edit'] == "*new*"){
	//INSERT NEW
	$query = 'INSERT INTO `tours_extlodging`(`name`,`lodgeid`,`perguest`,`single`,`triple`,`quad`)';
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['lodgeid'].'","'.$_POST['perguest'].'","'.$_POST['single'].'","'.$_POST['triple'].'","'.$_POST['quad'].'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new tour lodging extension option "'.stripslashes($_POST['name']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	} else {
	//UPDATE
	$query = 'UPDATE `tours_extlodging` SET `name` = "'.$_POST['name'].'", `lodgeid` = "'.$_POST['lodgeid'].'", `perguest` = "'.$_POST['perguest'].'", `single` = "'.$_POST['single'].'", `triple` = "'.$_POST['triple'].'", `quad` = "'.$_POST['quad'].'"';
		$query .= ' WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""){ array_push($successmsg,'Saved tour lodging extension option "'.$_POST['name'].'" ('.$_REQUEST['edit'].').'); } else { array_push($errormsg,$thiserror); }

	}

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){

	$query = 'DELETE FROM `tours_extlodging` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' tour lodging extension options were deleted.'); else: array_push($errormsg,$thiserror); endif;

}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Tour Lodging Extension Options</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){
//!EDIT

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `tours_extlodging` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);
	}

//GET VENDORS
/*$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}*/

//GET TOUR LODGING
$lodging = array();
$query = 'SELECT * FROM `lodging` WHERE `type` = "t" ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$lodging['l'.$row['id']] = $row;
	}

bgcolor('');


?><SCRIPT><!--

function cl_single(){
	var pp_single = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('single').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_single = (pp_a + pp_b).toFixed(2);
		} else {
		pp_single = document.getElementById('perguest').value;
		}
	document.getElementById('pp_single').innerHTML = '$'+pp_single;
	}

function cl_triple(){
	var pp_triple = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('triple').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_triple = (((pp_a*3)-pp_b)/3).toFixed(2);
		} else {
		pp_triple = document.getElementById('perguest').value;
		}
	document.getElementById('pp_triple').innerHTML = '$'+pp_triple;
	}

function cl_quad(){
	var pp_quad = 0;
	var pp_a = parseFloat(document.getElementById('perguest').value);
	var pp_b = parseFloat(document.getElementById('quad').value);
	if(!isNaN(pp_a) && !isNaN(pp_b)){
		pp_quad = (((pp_a*4)-pp_b)/4).toFixed(2);
		} else {
		pp_quad = document.getElementById('perguest').value;
		}
	document.getElementById('pp_quad').innerHTML = '$'+pp_quad;
	}

//--></SCRIPT><? echo "\n\n";


echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Extend your stay at...</TD><TD><INPUT TYPE="text" NAME="name" STYLE="width:300px;" VALUE="'.getval('name').'"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Lodging<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Where should extra nights be added for a reservation?</SPAN></TD><TD><SELECT NAME="lodgeid" STYLE="width:200px;">';
		foreach($lodging as $row){
		echo '<OPTION VALUE="'.$row['id'].'"';
		if(getval('lodgeid') == $row['id']): echo ' SELECTED'; endif;
		echo '>'.$row['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Price per person, per night<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Double occupancy is assumed.</SPAN></TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="perguest" ID="perguest" STYLE="width:60px;" VALUE="'.getval('perguest').'" onKeyUp="cl_single(); cl_triple(); cl_quad();"></TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Single occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Surcharge</SPAN></TD><TD>';
		$pp_single = number_format((getval('perguest')+getval('single')),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_single">$'.$pp_single.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">+</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="single" ID="single" STYLE="width:60px;" VALUE="'.getval('single').'" onKeyUp="cl_single()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Triple occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Discount</SPAN></TD><TD>';
		$pp_triple = number_format((((getval('perguest')*3)-getval('triple'))/3),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_triple">$'.$pp_triple.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">-</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="triple" ID="triple" STYLE="width:60px;" VALUE="'.getval('triple').'" onKeyUp="cl_triple()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Quad occupancy<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666; white-space:nowrap;">Discount</SPAN></TD><TD>';
		$pp_quad = number_format((((getval('perguest')*4)-getval('quad'))/4),2,'.','');
		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0"><TR><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; width:70px;" ID="pp_quad">$'.$pp_quad.'</TD><TD STYLE="font-family:Arial; font-size:10pt; text-align:right; padding-right:4px; width:20px;">-</TD><TD STYLE="font-family:Arial; font-size:11pt;">$<INPUT TYPE="text" NAME="quad" ID="quad" STYLE="width:60px;" VALUE="'.getval('quad').'" onKeyUp="cl_quad()"></TD></TR></TABLE>';
		echo '</TD></TR>'."\n";
	/*echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">Vendor</TD><TD><SELECT NAME="vendor" STYLE="width:200px;">';
		foreach($vendors as $vendor){
		echo '<OPTION VALUE="'.$vendor['id'].'"';
		if(getval('vendor') == $vendor['id']): echo ' SELECTED'; endif;
		echo '>'.$vendor['name'].'</OPTION>';
		}
		echo '</TD></TR>'."\n";*/
	echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {
//!INDEX


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked?");
	return r;
}

//--></SCRIPT><?


//GET ITEMS
$extlodging = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS tours_extlodging.*, lodging.`name` AS `lodge_name` FROM `tours_extlodging` LEFT JOIN `lodging` ON tours_extlodging.`lodgeid` = lodging.`id`';
	$query .= ' ORDER BY '.$_SESSION['tours_extlodging']['sortby'].' '.$_SESSION['tours_extlodging']['sortdir'].', tours_extlodging.`name` ASC';
	$query .= ' LIMIT '.(($_SESSION['tours_extlodging']['p']-1)*$_SESSION['tours_extlodging']['limit']).','.$_SESSION['tours_extlodging']['limit'];
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($extlodging,$row);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['tours_extlodging']['limit']);
if($numpages > 0 && $_SESSION['tours_extlodging']['p'] > $numpages): $_SESSION['tours_extlodging']['p'] = $numpages; endif;


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort by:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['tours_extlodging']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['tours_extlodging']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['tours_extlodging']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['tours_extlodging']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours_extlodging']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours_extlodging']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['tours_extlodging']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours_extlodging']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours_extlodging']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="return del();">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="delete">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Lodging</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Per guest</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Single</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Triple</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Quad</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($extlodging as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['name'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['lodge_name'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">$'.$row['perguest'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">+$'.$row['single'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">-$'.$row['triple'].'</TD>';
	echo '<TD ALIGN="right" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">-$'.$row['quad'].'</TD>';
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours_extlodging']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours_extlodging']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['tours_extlodging']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['tours_extlodging']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['tours_extlodging']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR><INPUT TYPE="button" VALUE="New lodging extension option" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;">';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>