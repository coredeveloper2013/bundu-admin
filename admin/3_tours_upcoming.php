<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_tours_upcoming";
require("validate.php");
//require_once("../common.inc.php");


//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['driver_info'])){
	foreach($_POST['driver_info'] as $key => $info){
		$query = 'INSERT INTO `driver_info`(`date`,`tourid`,`driver`) VALUES("'.$_POST['date'][$key].'","'.$_POST['tourid'][$key].'","'.$_POST['driver_info'][$key].'") ON DUPLICATE KEY UPDATE `driver` = "'.$_POST['driver_info'][$key].'"';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }
		}
	foreach($_POST['dateids'] as $id){
		$conf = 0;
		if(isset($_POST['confirmed']) && in_array($id,$_POST['confirmed'])) {
			$conf = 1;
		}

		$tourDateObj = new tour_date($id);
		if($tourDateObj->id > 0) {
			$tourDateObj->setConfirmed($conf);
			$tourDateObj->save();
		}
		unset($tourDateObj);

		/*
		$query = 'UPDATE `tours_dates` SET `confirmed` = "'.$conf.'" WHERE `id` = "'.$id.'" LIMIT 1';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }
		*/
	}
	if(count($errormsg) == 0){
		array_push($successmsg,'Saved driver information and confirmed/unconfirmed dates.');
		}
	}


if(!isset($_REQUEST['start']) || trim($_REQUEST['start']) == ""){
	$_REQUEST['start'] = $today;
	//$_SERVER['QUERY_STRING'] = 'start='.$_REQUEST['start'];
	}
if(!isset($_REQUEST['end']) || trim($_REQUEST['end']) == ""){ $_REQUEST['end'] = strtotime("+ 3 months"); }
if($_REQUEST['end'] < $_REQUEST['start']){ $_REQUEST['end'] = $_REQUEST['start']; }
if(isset($_REQUEST['start_month']) && trim($_REQUEST['start_month']) != "" && isset($_REQUEST['start_day']) && trim($_REQUEST['start_day']) != "" && isset($_REQUEST['start_year']) && trim($_REQUEST['start_year']) != ""){
	$_REQUEST['start'] = mktime(0,0,0,$_REQUEST['start_month'],$_REQUEST['start_day'],$_REQUEST['start_year']);
	}
if(isset($_REQUEST['end_month']) && trim($_REQUEST['end_month']) != "" && isset($_REQUEST['end_day']) && trim($_REQUEST['end_day']) != "" && isset($_REQUEST['end_year']) && trim($_REQUEST['end_year']) != ""){
	$_REQUEST['end'] = mktime(0,0,0,$_REQUEST['end_month'],$_REQUEST['end_day'],$_REQUEST['end_year']);
	}

require("header.php");


if(!isset($_REQUEST['filter'])){ $_REQUEST['filter'] = '*all*'; }

//GET TOURS
$tours = array();
$query = 'SELECT tours.`id`,tours.`title` FROM `tours`,`reservations_assoc` WHERE reservations_assoc.`tourid` = tours.`id` AND reservations_assoc.`canceled` = 0 AND tours.`vendor` = 1 ORDER BY `id` DESC, `title` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['subhead'] = $row['id'].' : '.str_replace("'",'',$row['title']);
	if(strlen($row['subhead']) > 100){ $row['subhead'] = substr($row['subhead'],0,49).' ... '.substr($row['subhead'],-49); }
	$tours['t'.$row['id']] = $row;
	}


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Upcoming Tours</U></FONT><BR>'."\n\n";

printmsgs($successmsg,$errormsg);

echo '<BR>'."\n\n";


?><script language="javascript"><!--

function adjustend(adddays){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("start_year").value,eval(document.getElementById("start_month").value-1),document.getElementById("start_day").value);

	var findend = startdate.getTime();
	findend = eval(findend + (86400000*eval(adddays)));

	var end = new Date();
	end.setTime(findend);

	document.getElementById("end_month").value = eval(end.getMonth()+1);
	document.getElementById("end_day").value = end.getDate();
	document.getElementById("end_year").value = end.getFullYear();
}

function checkend(){
	var startdate = new Date();
	startdate.setFullYear(document.getElementById("start_year").value,eval(document.getElementById("start_month").value-1),document.getElementById("start_day").value);
	var chkdate = startdate.getTime();

	var enddate = new Date();
	enddate.setFullYear(document.getElementById("end_year").value,eval(document.getElementById("end_month").value-1),document.getElementById("end_day").value);
	var chkend = enddate.getTime();

	if(chkend < chkdate){ adjustend(0); }
}

//--></script><? echo "\n\n";


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<TABLE BORDER="0" BGCOLOR="#D9DBEC" WIDTH="94%" CELLPADDING="0"><TR><TD ALIGN="center">';
	echo '<TABLE BORDER="0"><TR>';
		echo '<TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>Date Range:&nbsp;</B></FONT></TD>';
		echo '<TD><SELECT NAME="start_month" ID="start_month" onChange="adjustend(0)">';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("n",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="start_day" ID="start_day" onChange="adjustend(0)">';
			for($i=1; $i<32; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("j",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="start_year" ID="start_year" onChange="adjustend(0)">';
			for($i=2006; $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("Y",$_REQUEST['start']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD>';
		echo '<TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><B>&nbsp;-&nbsp;</B></FONT></TD>';
		echo '<TD><SELECT NAME="end_month" ID="end_month" onChange="checkend()">';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("n",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.' ('.date("M",mktime("0","0","0",$i,"1","2005")).')</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="end_day" ID="end_day" onChange="checkend()">';
			for($i=1; $i<32; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("j",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="end_year" ID="end_year" onChange="checkend()">';
			for($i=2006; $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if(date("Y",$_REQUEST['end']) == $i){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD>';
		echo '<TD ROWSPAN="2"><INPUT TYPE="submit" VALUE="View"></TD>';
		echo '</TR><TR>';
		echo '<TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>View:&nbsp;</B></FONT></TD>';
		echo '<TD COLSPAN="3"><SELECT NAME="filter" ID="filter" STYLE="width:420px;"><OPTION VALUE="*all*">All Tours</OPTION>';
			foreach($tours as $key => $tour){
				echo '<OPTION VALUE="'.$tour['id'].'"';
				if(isset($_REQUEST['filter']) && $tour['id'] == $_REQUEST['filter']){ echo ' SELECTED'; }
				echo '>'.$tour['subhead'].'</OPTION>';
				}
			echo '</SELECT></TD>';
	echo '</TR></TABLE>';
echo '</TD></TR></TABLE>';
echo '</FORM>';

echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="start" VALUE="'.$_REQUEST['start'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="end" VALUE="'.$_REQUEST['end'].'">'."\n";
echo '<INPUT TYPE="submit" VALUE="Save Changes"><BR><BR>'."\n\n";

echo '<DIV STYLE="width:94%; background-color:#EEEEEE; text-align:center; font-family:Arial; font-size:12pt;"><A HREF="viewuptours.php?start='.$_REQUEST['start'].'&end='.$_REQUEST['end'].'&filter='.$_REQUEST['filter'].'&print=y" TARGET="_blank">Print this schedule</A></DIV><BR>'."\n\n";

	include('viewuptours.php');

echo '<DIV STYLE="width:94%; background-color:#EEEEEE; text-align:center; font-family:Arial; font-size:12pt;"><A HREF="viewuptours.php?start='.$_REQUEST['start'].'&end='.$_REQUEST['end'].'&filter='.$_REQUEST['filter'].'&print=y" TARGET="_blank">Print this schedule</A></DIV><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save Changes">'."\n\n";
echo '</FORM>'."\n\n";


require("footer.php");

?>