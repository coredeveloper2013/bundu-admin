<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_translations_itinerary";
require("validate.php");
require("header.php");

if(!isset($_SESSION['translations_itinerary']['p'])): $_SESSION['translations_itinerary']['p'] = 1; endif;
if(!isset($_SESSION['translations_itinerary']['sortby'])): $_SESSION['translations_itinerary']['sortby'] = '`nickname`'; endif;
	$sortby = array('complete'=>'By translations completed','`nickname`'=>'By reference','itinerary.`id`'=>'As added');
if(!isset($_SESSION['translations_itinerary']['sortdir'])): $_SESSION['translations_itinerary']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['translations_itinerary']['limit'])): $_SESSION['translations_itinerary']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['translations_itinerary']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['translations_itinerary']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['translations_itinerary']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['translations_itinerary']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['translations_itinerary']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//Find next...
	$itids = array();
	if(isset($_POST['findnext']) && $_POST['findnext'] != ""){
		$query = 'SELECT itinerary.`id`, itinerary.`nickname`, COUNT(itinerary_translations.`id`) as `complete` FROM';
			$query .= ' (`itinerary` LEFT JOIN `routes` ON itinerary.`routeid` = routes.`id`)';
			$query .= ' LEFT JOIN `itinerary_translations` ON itinerary.`id` = itinerary_translations.`itid`';
				$query .= ' AND CONCAT(itinerary_translations.`title`,itinerary_translations.`time`,itinerary_translations.`comments`,itinerary_translations.`youtube_title`) != ""';
				$query .= ' AND itinerary_translations.`modified` >= itinerary.`modified`';
				if($thisuser['languages'] !== "*"){ $query .= ' AND (itinerary_translations.`lang` = "'.implode('" OR itinerary_translations.`lang` = "',$thisuser['languages']).'")'; }
			$query .= ' GROUP BY itinerary.`id`';
			$query .= ' ORDER BY '.$_SESSION['translations_itinerary']['sortby'].' '.$_SESSION['translations_itinerary']['sortdir'].', `nickname` ASC, itinerary.`step` ASC, itinerary.`id` ASC';
			//echo $query.'<BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($itids,$row['id']);
			}
		} //End find next If

	$numupdated = 0;
	foreach($_POST['langid'] as $key => $id){
		$query = 'INSERT INTO `itinerary_translations`(`itid`,`lang`,`title`,`time`,`comments`,`youtube_title`)';
			$query .= ' VALUES("'.$_POST['edit'].'","'.$_POST['langid'][$key].'","'.trim($_POST['title'][$key]).'","'.trim($_POST['time'][$key]).'","'.trim($_POST['comments'][$key]).'","'.trim($_POST['youtube_title'][$key]).'")';
			$query .= ' ON DUPLICATE KEY UPDATE `title` = "'.trim($_POST['title'][$key]).'", `time` = "'.trim($_POST['time'][$key]).'", `comments` = "'.trim($_POST['comments'][$key]).'", `youtube_title` = "'.trim($_POST['youtube_title'][$key]).'"';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ if(mysql_affected_rows() > 0){ $numupdated++; } } else { array_push($errormsg,$thiserror); }

		} //End ForEach
	if($numupdated > 0): array_push($successmsg,$numupdated.' translations were added/updated for itinerary '.$_POST['edit'].'.'); endif;

	$index = array_search($_REQUEST['edit'],$itids);
	if(isset($_POST['findnext']) && $_POST['findnext'] != "" && $index !== false && isset($itids[($index+1)])){ $_REQUEST['edit'] = $itids[($index+1)]; }

}



echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Translations: Route Itinerary Entries</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//GET LANGUAGES
$languages = array();
$query = 'SELECT * FROM `languages` WHERE `accr` != "en"';
	if($thisuser['languages'] != "*"){ $query .= ' AND (`id` = "'.implode('" OR `id` = "',$thisuser['languages']).'")'; }
	$query .= ' ORDER BY `reference` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$languages['l'.$row['id']] = $row;
	}


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

$query = 'SELECT * FROM `itinerary` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
$result = mysql_query($query);
$itinfo = mysql_fetch_assoc($result);

$translations = array();
$query = 'SELECT * FROM `itinerary_translations` WHERE `itid` = "'.$_REQUEST['edit'].'"';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$translations['t'.$row['lang']] = $row;
	}

//GET ASSOCIATED TOURS
$tours = array();
$query = 'SELECT DISTINCT tours.`id`,tours.`alias`,tours.`title` FROM `tours`,`tours_assoc`,`itinerary` WHERE tours.`id` = tours_assoc.`tourid` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = itinerary.`routeid` AND itinerary.`id` = "'.$_REQUEST['edit'].'" ORDER BY tours.`title` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours,$row);
	}

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$itinfo['id'].'">'."\n\n";

foreach($languages as $lang){
	echo '<INPUT TYPE="hidden" NAME="langid[]" VALUE="'.$lang['id'].'">'."\n";
	}
	echo "\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	//nl2br(htmlspecialchars($itinfo[$field]))

	//TITLE
	$field = 'title';

echo '<!-- Title: '.$itinfo[$field].' -->';

	if(isset($itinfo[$field]) && trim($itinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		if(strtotime($itinfo['modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($itinfo['modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($itinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = @$translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<INPUT TYPE="text" NAME="'.$field.'[]" STYLE="width:100%;" VALUE="'.getval($field).'">'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//TIME
	$field = 'time';
	if(isset($itinfo[$field]) && trim($itinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Time</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		if(strtotime($itinfo['modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($itinfo['modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($itinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = @$translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<INPUT TYPE="text" NAME="'.$field.'[]" STYLE="width:100%;" VALUE="'.getval($field).'">'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//COMMENTS
	$field = 'comments';
	if(isset($itinfo[$field]) && trim($itinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Comments</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		if(strtotime($itinfo['modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($itinfo['modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($itinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = @$translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:240px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//YOUTUBE TITLE
	$field = 'youtube_title';
	if(isset($itinfo[$field]) && trim($itinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">YouTube Title</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		if(strtotime($itinfo['modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($itinfo['modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($itinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = @$translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<INPUT TYPE="text" NAME="'.$field.'[]" STYLE="width:100%;" VALUE="'.getval($field).'">'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-left:162px;">';
		echo '<DIV STYLE="width:160px; text-align:right; float:right;"><INPUT TYPE="submit" NAME="findnext" VALUE="Save and edit next &gt;" STYLE="color:#0000FF;"></DIV>';
		echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;">';
		echo '</TD></TR>'."\n";
		}

	//WHERE DOES THIS APPEAR?
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Where does this appear?</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD COLSPAN="2" STYLE="font-family:Arial; font-size:9pt;">';
	if(count($tours) > 0){
		foreach($tours as $key => $tour){
			echo '<DIV STYLE="padding-top:2px; padding-bottom:2px;';
			if($key > 0){ echo ' border-top:1px solid #C7C7C7;'; }
			echo '">'.$tour['id'].' : <A HREF="http://www.bundubashers.com/tour.php?id='.$tour['alias'].'" TARGET="_blank">'.$tour['title'].'</A></DIV>';
			}
		} else {
		echo 'This does not yet appear on the web site.';
		}
		echo '</TD></TR>'."\n\n";
	

	echo '</TABLE><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {


$link = '';

//GET ITINERARIES
$itinerary = array();
$itids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS itinerary.`id`, itinerary.`nickname`, routes.`name` AS `routename`, itinerary.`title`, itinerary.`comments`, itinerary.`youtube_title`, COUNT(itinerary_translations.`id`) as `complete`';
	$query .= ', (SELECT tours.`alias` FROM `tours`,`tours_assoc` WHERE tours.`id` = tours_assoc.`tourid` AND tours_assoc.`type` = "r" AND tours_assoc.`dir` = "f" AND tours_assoc.`typeid` = itinerary.`routeid` ORDER BY tours.`id` DESC LIMIT 1) as `tourlink`';
	$query .= ' FROM';
	$query .= ' (`itinerary` LEFT JOIN `routes` ON itinerary.`routeid` = routes.`id`)';
	$query .= ' LEFT JOIN `itinerary_translations` ON itinerary.`id` = itinerary_translations.`itid`';
		$query .= ' AND CONCAT(itinerary_translations.`title`,itinerary_translations.`time`,itinerary_translations.`comments`,itinerary_translations.`youtube_title`) != ""';
		$query .= ' AND itinerary_translations.`modified` >= itinerary.`modified`';
		if($thisuser['languages'] !== "*"){ $query .= ' AND (itinerary_translations.`lang` = "'.implode('" OR itinerary_translations.`lang` = "',$thisuser['languages']).'")'; }
	$query .= ' GROUP BY itinerary.`id`';
	$query .= ' ORDER BY '.$_SESSION['translations_itinerary']['sortby'].' '.$_SESSION['translations_itinerary']['sortdir'].', `nickname` ASC, itinerary.`step` ASC, itinerary.`id` ASC';
	$query .= ' LIMIT '.(($_SESSION['translations_itinerary']['p']-1)*$_SESSION['translations_itinerary']['limit']).','.$_SESSION['translations_itinerary']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query); //echo mysql_error();
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['complete'] = array();
	$row['updated'] = array();
	$itinerary['i'.$row['id']] = $row;
	array_push($itids,$row['id']);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($itinerary); echo '</PRE>';

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['translations_itinerary']['limit']);
if($numpages > 0 && $_SESSION['translations_itinerary']['p'] > $numpages): $_SESSION['translations_itinerary']['p'] = $numpages; endif;

//GET COMPLETED TRANSLATIONS
$query = 'SELECT itinerary_translations.`itid`,itinerary_translations.`lang` FROM `itinerary_translations`,`itinerary` WHERE';
	$query .= ' itinerary_translations.`itid` = itinerary.`id` AND itinerary_translations.`modified` >= itinerary.`modified`';
	$query .= ' AND CONCAT(itinerary_translations.`title`,itinerary_translations.`time`,itinerary_translations.`comments`,itinerary_translations.`youtube_title`) != ""';
	$query .= ' AND (`itid` = "'.implode('" OR `itid` = "',$itids).'")';
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($itinerary['i'.$row['itid']]['complete'],$row['lang']);
	}

//GET UPDATED TRANSLATIONS
$query = 'SELECT itinerary_translations.`itid`,itinerary_translations.`lang` FROM `itinerary_translations`,`itinerary` WHERE';
	$query .= ' itinerary_translations.`itid` = itinerary.`id` AND itinerary_translations.`modified` < itinerary.`modified`';
	$query .= ' AND CONCAT(itinerary_translations.`title`,itinerary_translations.`time`,itinerary_translations.`comments`,itinerary_translations.`youtube_title`) != ""';
	$query .= ' AND (`itid` = "'.implode('" OR `itid` = "',$itids).'")';
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($itinerary['i'.$row['itid']]['updated'],$row['lang']);
	}

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['translations_itinerary']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['translations_itinerary']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['translations_itinerary']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['translations_itinerary']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';

echo '<TABLE BORDER="0"><TR>
	<TD ALIGN="right" VALIGN="middle"><IMG SRC="img/check.png" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation is complete and up to date.</TD>
	<TD ALIGN="right" VALIGN="middle" STYLE="padding-left:20px;"><IMG SRC="img/dot_yellow.gif" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation needs to be updated.</TD>
	</TR></TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_itinerary']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_itinerary']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_itinerary']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_itinerary']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_itinerary']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="batch">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Reference</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Content</TD>';
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; padding-left:2px; padding-right:2px; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; cursor:help;" TITLE="'.$lang['reference'].'">'.ucwords($lang['accr']).'</TD>';
		}
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($itinerary as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt; width:20%;">'.$row['nickname'].'</TD>';
	echo '<TD ALIGN="left" VALIGN="top" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">';
		if($row['title'] != "" && $row['comments'] != ""){ $row['title'] .= ' - '; }
		if($row['comments'] != ""){ $row['title'] .= $row['comments']; }
		if($row['title'] != "" && $row['youtube_title'] != ""){ $row['title'] .= ' - '; }
		if($row['youtube_title'] != ""){ $row['title'] .= $row['youtube_title']; }
		$row['title'] = strip_tags($row['title']);
		if(strlen($row['title']) > 180){
			$row['title'] = substr($row['title'],0,89).' ... '.substr($row['title'],-89);
			//if(strstr($row['title']," ")){ $row['title'] = trim(substr($row['title'],0,strrpos($row['title']," "))); }
			//if(substr($row['title'],-1) == '.' || substr($row['title'],-1) == ','){ $row['title'] = substr($row['title'],0,-1); }
			//$row['title'] .= '...';
			}
		echo $row['title'];
		echo '</A></TD>';
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="padding-left:2px; padding-right:2px; font-family:Arial; font-size:8pt;">';
			if(in_array($lang['id'],$row['complete'])){
				if(isset($row['tourlink']) && $row['tourlink'] != ""){ echo '<A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($row['tourlink']).'&lang='.$lang['id'].'" TARGET="_blank">'; }
				echo '<IMG SRC="img/check.png" BORDER="0" TITLE="'.$lang['reference'].'">';
				if(isset($row['tourlink']) && $row['tourlink'] != ""){ echo '</A>'; }

				} elseif(in_array($lang['id'],$row['updated'])){
				echo '<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/dot_yellow.gif" BORDER="0" TITLE="Please review and update."></A>';

				} else {
				echo '&nbsp;';
				}
			echo '</TD>';
		}
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	$sel++;
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_itinerary']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_itinerary']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_itinerary']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_itinerary']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_itinerary']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0"><TR>
	<TD ALIGN="right" VALIGN="middle"><IMG SRC="img/check.png" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation is complete and up to date.</TD>
	<TD ALIGN="right" VALIGN="middle" STYLE="padding-left:20px;"><IMG SRC="img/dot_yellow.gif" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation needs to be updated.</TD>
	</TR></TABLE>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>