<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_translations_static";
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] == '*new*'){ $pageid .= '_edit'; }
require("validate.php");
require("header.php");

if(!isset($_SESSION['translations_static']['p'])): $_SESSION['translations_static']['p'] = 1; endif;
if(!isset($_SESSION['translations_static']['sortby'])): $_SESSION['translations_static']['sortby'] = "complete"; endif;
	$sortby = array('trans_static.`english`'=>'Alphabetically','trans_static.`id`'=>'As added','complete'=>'By translations completed');
if(!isset($_SESSION['translations_static']['sortdir'])): $_SESSION['translations_static']['sortdir'] = "ASC"; endif;
if(!isset($_SESSION['translations_static']['limit'])): $_SESSION['translations_static']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['translations_static']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['translations_static']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['translations_static']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['translations_static']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['translations_static']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	if($thisuser['languages'] == "*" || in_array('1',$thisuser['languages'])){
	if($_POST['edit'] == "*new*" && trim($_POST['english']) != ""){
	//INSERT NEW
	$query = 'INSERT INTO `trans_static`(`english`) VALUES("'.trim($_POST['english']).'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): $_REQUEST['edit'] = mysql_insert_id(); array_push($successmsg,'Saved new word/phrase "'.trim($_POST['english']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	} elseif(trim($_POST['english']) != ""){
	//UPDATE
	$query = 'UPDATE `trans_static` SET `english` = "'.trim($_POST['english']).'" WHERE `id` = "'.$_POST['edit'].'" LIMIT 1';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,'Saved word/phrase "'.trim($_POST['english']).'" ('.$_REQUEST['edit'].').'); else: array_push($errormsg,$thiserror); endif;

	}
	} //End allow language if statement

	if($_REQUEST['edit'] != "*new*" && $_REQUEST['edit'] != ""){

	//Find next...
	$staticids = array();
	if(isset($_POST['findnext']) && $_POST['findnext'] != ""){
		$query = 'SELECT trans_static.`id`, COUNT(trans_translations.`id`) as `complete` FROM `trans_static`';
			$query .= ' LEFT JOIN `trans_translations` ON trans_static.`id` = trans_translations.`staticid` AND trans_translations.`translation` != ""';
				$query .= ' AND trans_translations.`modified` >= trans_static.`modified`';
				if($thisuser['languages'] !== "*"){ $query .= ' AND (trans_translations.`language` = "'.implode('" OR trans_translations.`language` = "',$thisuser['languages']).'")'; }
			$query .= ' GROUP BY trans_static.`id`';
			$query .= ' ORDER BY '.$_SESSION['translations_static']['sortby'].' '.$_SESSION['translations_static']['sortdir'].', trans_static.`english` ASC';
			//echo $query.'<BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($staticids,$row['id']);
			}
		} //End find next If

	$numupdated = 0;
	foreach($_POST['langid'] as $key => $langid){
		$query = 'INSERT INTO `trans_translations`(`staticid`,`language`,`translation`) VALUES("'.$_REQUEST['edit'].'","'.$_POST['langid'][$key].'","'.trim($_POST['trans'][$key]).'") ON DUPLICATE KEY UPDATE `translation` = "'.trim($_POST['trans'][$key]).'"';
			@mysql_query($query);
			//echo $query.'<BR>'.mysql_affected_rows().'<BR><BR>';
		$thiserror = mysql_error();
		if($thiserror == ""){ if(mysql_affected_rows() > 0){ $numupdated++; } } else { array_push($errormsg,$thiserror); }
		}
	if($numupdated > 0): array_push($successmsg,$numupdated.' translations were added/updated. (<A HREF="3_translations_static.php?edit='.$_REQUEST['edit'].'">'.$_REQUEST['edit'].'</A>)'); endif;

	$index = array_search($_REQUEST['edit'],$staticids);
	if(isset($_POST['findnext']) && $_POST['findnext'] != "" && $index !== false && isset($staticids[($index+1)])){ $_REQUEST['edit'] = $staticids[($index+1)]; }

	} //End $_REQUEST['edit'] if statement

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "batch_update" && isset($_POST['staticid']) && count($_POST['staticid']) > 0){

	$numupdated = 0;
	foreach($_POST['staticid'] as $key => $id){
	if($_POST['langid'][$key] == "1"){
		$query = 'UPDATE `trans_static` SET `english` = "'.trim($_POST['trans'][$key]).'" WHERE `id` = "'.$_POST['staticid'][$key].'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""): array_push($successmsg,'Saved word/phrase "'.trim($_POST['trans'][$key]).'" ('.$_POST['staticid'][$key].').'); else: array_push($errormsg,$thiserror); endif;
		} else {
		$query = 'INSERT INTO `trans_translations`(`staticid`,`language`,`translation`) VALUES("'.$_POST['staticid'][$key].'","'.$_POST['langid'][$key].'","'.trim($_POST['trans'][$key]).'") ON DUPLICATE KEY UPDATE `translation` = "'.trim($_POST['trans'][$key]).'"';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ if(mysql_affected_rows() > 0){ $numupdated++; } } else { array_push($errormsg,$thiserror); }
		} //End Lang if statement
		} //End ForEach
	if($numupdated > 0): array_push($successmsg,$numupdated.' translations were added/updated.'); endif;

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "delete" && isset($_POST['selitems']) && count($_POST['selitems']) > 0){
/*
	$query = 'DELETE FROM `locations` WHERE `id` = "'.implode('" OR `id` = "',$_POST['selitems']).'"';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror == ""): array_push($successmsg,mysql_affected_rows().' locations were deleted.'); else: array_push($errormsg,$thiserror); endif;
*/
}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Translations: Words/Phrases</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//GET LANGUAGES
$languages = array();
$query = 'SELECT * FROM `languages` WHERE `accr` != "en"';
	if($thisuser['languages'] != "*"){ $query .= ' AND (`id` = "'.implode('" OR `id` = "',$thisuser['languages']).'")'; }
	$query .= ' ORDER BY `reference` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$languages['l'.$row['id']] = $row;
	}


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

if($_REQUEST['edit'] == "*new*" && count($errormsg) > 0){
	$fillform = $_POST;
	} elseif($_REQUEST['edit'] == "*new*"){
	$fillform = array(
		'id' => '*new*'
		);
	} else {
	$query = 'SELECT * FROM `trans_static` WHERE `id` = "'.$_REQUEST['edit'].'" LIMIT 1';
	$result = mysql_query($query);
	$fillform = mysql_fetch_assoc($result);

	$query = 'SELECT * FROM `trans_translations` WHERE `staticid` = "'.$_REQUEST['edit'].'"';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$fillform['l'.$row['language']] = $row['translation'];
		$fillform['m'.$row['language']] = strtotime($row['modified']);
		}
	}


//bgcolor('reset');

echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.getval('id').'">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="3">'."\n";
	echo '<TR STYLE="background:#'.bgcolor('reset').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		if(getval('context') != ""){
			$link = getval('context');
			if(strpos($link,'?') !== false){ $link.= '&lang=1'; } else { $link.= '?lang=1'; }
			echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal;"><A HREF="'.$link.'" TARGET="_blank">View in context</A></SPAN>';
			}
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
		if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){
			if($thisuser['languages'] == "*" || in_array('1',$thisuser['languages'])){
				echo '<TEXTAREA NAME="english" STYLE="width:100%; height:80px;">'.getval('english').'</TEXTAREA>';
				} else {
				echo nl2br(getval('english'));
				}
			} else {
			echo nl2br(getval('english'));
			}
		echo '</TD></TR>'."\n";
	foreach($languages as $lang){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
			if(getval('m'.$lang['id']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",getval('m'.$lang['id'])).'</SPAN>'; }
			echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			echo '<INPUT TYPE="hidden" NAME="langid[]" VALUE="'.$lang['id'].'">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="trans[]" STYLE="width:100%; height:80px;">'.getval('l'.$lang['id']).'</TEXTAREA>'; } else { echo nl2br(getval('l'.$lang['id'])); }
			echo '</TD></TR>'."\n";
		}

	if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-top:12px;';
	if($_REQUEST['edit'] != "*new*"){
		echo ' padding-left:162px;">';
		echo '<DIV STYLE="width:160px; text-align:right; float:right;"><INPUT TYPE="submit" NAME="findnext" VALUE="Save and edit next &gt;" STYLE="color:#0000FF;"></DIV>';
		} else {
		echo '">';
		}
		echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;">';
		echo '</TD></TR>'."\n";
		}

	echo '</TABLE><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "batch" && is_array($_POST['selitems']) && count($_POST['selitems']) > 0){


$phrases = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS trans_static.*, COUNT(trans_translations.`id`) as `complete` FROM `trans_static`';
	$query .= ' LEFT JOIN `trans_translations` ON trans_static.`id` = trans_translations.`staticid` AND trans_translations.`translation` != ""';
		$query .= ' AND trans_translations.`modified` >= trans_static.`modified`';
		if($thisuser['languages'] !== "*"){ $query .= ' AND (trans_translations.`language` = "'.implode('" OR trans_translations.`language` = "',$thisuser['languages']).'")'; }
	$query .= ' WHERE trans_static.`id` = "'.implode('" OR trans_static.`id` = "',$_POST['selitems']).'"';
	$query .= ' GROUP BY trans_static.`id`';
	$query .= ' ORDER BY '.$_SESSION['translations_static']['sortby'].' '.$_SESSION['translations_static']['sortdir'].', trans_static.`english` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$phrases['p'.$row['id']] = $row;
	}

$query = 'SELECT * FROM `trans_translations` WHERE `staticid` = "'.implode('" OR `staticid` = "',$_POST['selitems']).'"';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$phrases['p'.$row['staticid']]['l'.$row['language']] = $row['translation'];
	$phrases['p'.$row['staticid']]['m'.$row['language']] = strtotime($row['modified']);
	}
	//echo '<PRE>'; print_r($phrases); echo '</PRE>';


echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="batch_update">'."\n\n";

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="3">'."\n\n";

foreach($phrases as $row){
	//bgcolor('reset');
	$fillform = $row;

	echo '<TR STYLE="background:#'.bgcolor('reset').'"><TD STYLE="border-top:2px solid #000000; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		if(strtotime($row['modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($row['modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="border-top:2px solid #000000; font-family:Arial; font-size:10pt;">';
		if($thisuser['languages'] == "*" || in_array('1',$thisuser['languages'])){
			echo '<INPUT TYPE="hidden" NAME="staticid[]" VALUE="'.getval('id').'">';
			echo '<INPUT TYPE="hidden" NAME="langid[]" VALUE="1">';
			echo '<TEXTAREA NAME="trans[]" STYLE="width:100%; height:80px;">'.getval('english').'</TEXTAREA>';
			} else {
			echo nl2br(getval('english'));
			}
		echo '</TD></TR>'."\n";
	foreach($languages as $lang){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
			if(getval('m'.$lang['id']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",getval('m'.$lang['id'])).'</SPAN>'; }
			echo '</TD><TD>';
			echo '<INPUT TYPE="hidden" NAME="staticid[]" VALUE="'.getval('id').'">';
			echo '<INPUT TYPE="hidden" NAME="langid[]" VALUE="'.$lang['id'].'">';
			echo '<TEXTAREA NAME="trans[]" STYLE="width:100%; height:80px;">'.getval('l'.$lang['id']).'</TEXTAREA>';
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:8pt; height:30px;">&nbsp;</TD></TR>'."\n\n";

	}
	echo '</TABLE>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";


} else {


?><SCRIPT><!--

function selectall(){
	i=0;
	while(document.getElementById("sel"+i)){
		document.getElementById("sel"+i).checked = document.getElementById("selall").checked;
		i++;
		}
}

function del(){
	var r=confirm("Delete checked words/phrases?");
	return r;
}

//--></SCRIPT><?


//GET ENGLISH
$english = array();
$staticids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS trans_static.*, COUNT(trans_translations.`id`) as `complete` FROM `trans_static`';
	$query .= ' LEFT JOIN `trans_translations` ON trans_static.`id` = trans_translations.`staticid` AND trans_translations.`translation` != ""';
		$query .= ' AND trans_translations.`modified` >= trans_static.`modified`';
		if($thisuser['languages'] !== "*"){ $query .= ' AND (trans_translations.`language` = "'.implode('" OR trans_translations.`language` = "',$thisuser['languages']).'")'; }
	$query .= ' GROUP BY trans_static.`id`';
	$query .= ' ORDER BY '.$_SESSION['translations_static']['sortby'].' '.$_SESSION['translations_static']['sortdir'].', trans_static.`english` ASC';
	$query .= ' LIMIT '.(($_SESSION['translations_static']['p']-1)*$_SESSION['translations_static']['limit']).','.$_SESSION['translations_static']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['complete'] = array();
	//array_push($english,$row);
	$english['e'.$row['id']] = $row;
	array_push($staticids,$row['id']);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['translations_static']['limit']);
if($numpages > 0 && $_SESSION['translations_static']['p'] > $numpages): $_SESSION['translations_static']['p'] = $numpages; endif;

//GET COMPLETED TRANSLATIONS
$query = 'SELECT trans_translations.`staticid`,trans_translations.`language` FROM `trans_translations`,`trans_static`';
	$query .= ' WHERE trans_static.`id` = trans_translations.`staticid`';
	$query .= ' AND trans_translations.`translation` != ""';
	$query .= ' AND trans_translations.`modified` >= trans_static.`modified`';
	$query .= ' AND (trans_translations.`staticid` = "'.implode('" OR trans_translations.`staticid` = "',$staticids).'")';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($english['e'.$row['staticid']]['complete'],$row['language']);
	}


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['translations_static']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['translations_static']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['translations_static']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['translations_static']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_static']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_static']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_static']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_static']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_static']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="batch">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">English</TD>';
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; padding-left:2px; padding-right:2px; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; cursor:help;" TITLE="'.$lang['reference'].'">'.ucwords($lang['accr']).'</TD>';
		}
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($english as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:5px; font-family:Arial; font-size:9pt;"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:#000000;">'.$row['english'].'</A></TD>'; //<LABEL FOR="sel'.$sel.'">'.$row['english'].'</LABEL>
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="padding-left:2px; padding-right:2px; font-family:Arial; font-size:8pt;">';
			if(in_array($lang['id'],$row['complete'])){
				echo '<IMG SRC="img/check.png" BORDER="0" TITLE="'.$lang['reference'].'">';
				} else {
				echo '&nbsp;';
				}
			echo '</TD>';
		}
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	$sel++;
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_static']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_static']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_static']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_static']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_static']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<BR>';
	echo '<INPUT TYPE="submit" VALUE="Edit checked" STYLE="width:180px;">';
	if($thisuser['languages'] == "*" || in_array('1',$thisuser['languages'])){ echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="New Word/Phrase" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?edit=*new*\'">'; }
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="submit" VALUE="Delete Selected" STYLE="color:#FF0000;" DISABLED>';
	echo '<BR><BR>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>