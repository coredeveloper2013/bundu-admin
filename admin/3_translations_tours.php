<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_translations_tours";
require("validate.php");
require("header.php");

if(!isset($_SESSION['translations_tours']['p'])): $_SESSION['translations_tours']['p'] = 1; endif;
if(!isset($_SESSION['translations_tours']['sortby'])): $_SESSION['translations_tours']['sortby'] = 'tours.`id`'; endif;
	$sortby = array('complete'=>'By translations completed','tours.`id`'=>'ID','tours.`title`'=>'Title');
if(!isset($_SESSION['translations_tours']['sortdir'])): $_SESSION['translations_tours']['sortdir'] = "DESC"; endif;
if(!isset($_SESSION['translations_tours']['showhidden'])): $_SESSION['translations_tours']['showhidden'] = "0"; endif;
if(!isset($_SESSION['translations_tours']['limit'])): $_SESSION['translations_tours']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['translations_tours']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['sortby']) && $_REQUEST['sortby'] != ""): $_SESSION['translations_tours']['sortby'] = $_REQUEST['sortby']; endif;
if(isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] != ""): $_SESSION['translations_tours']['sortdir'] = $_REQUEST['sortdir']; endif;
if(isset($_REQUEST['showhidden']) && $_REQUEST['showhidden'] != ""): $_SESSION['translations_tours']['showhidden'] = $_REQUEST['showhidden']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['translations_tours']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	if(!isset($_REQUEST['showhidden'])): $_SESSION['translations_tours']['showhidden'] = '0'; endif;
	$_SESSION['translations_tours']['p'] = '1';
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

//FUNCTIONAL CODE
$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "update" && isset($_POST['edit']) && $_POST['edit'] != ""){

	//Find next...
	$tourids = array();
	if(isset($_POST['findnext']) && $_POST['findnext'] != ""){
		$query = 'SELECT tours.`id`, COUNT(tours_translations.`id`) as `complete` FROM `tours`';
			$query .= ' LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid`';
				$query .= ' AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != ""';
				$query .= ' AND tours_translations.`modified` >= tours.`eng_modified`';
				if($thisuser['languages'] !== "*"){ $query .= ' AND (tours_translations.`lang` = "'.implode('" OR tours_translations.`lang` = "',$thisuser['languages']).'")'; }
			$query .= ' WHERE 1 = 1';
			if($_SESSION['translations_tours']['showhidden'] != "1"){ $query .= ' AND tours.`hidden` = "0"'; }
			//if($thisuser['languages'] !== "*"){ $query .= ' AND (tours.`id` >= 1109 OR tours.`id` = 1006 OR tours.`id` = 1026 OR tours.`id` = 1030 OR tours.`id` = 1037)'; }
			$query .= ' GROUP BY tours.`id`';
			$query .= ' ORDER BY '.$_SESSION['translations_tours']['sortby'].' '.$_SESSION['translations_tours']['sortdir'].', tours.`id` DESC';
			//echo $query.'<BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($tourids,$row['id']);
			}
		} //End find next If

	$numupdated = 0;
	foreach($_POST['langid'] as $key => $id){
		$query = 'INSERT INTO `tours_translations`(`tourid`,`lang`,`title`,`eztitle`,`short_desc`,`highlights`,`details`,`pleasenote`,`toptitle`,`topdesc`)';
			$query .= ' VALUES("'.mysql_real_escape_string($_POST['edit']).'","'.mysql_real_escape_string($_POST['langid'][$key]).'","'.mysql_real_escape_string(trim($_POST['title'][$key])).'","'.mysql_real_escape_string(trim($_POST['eztitle'][$key])).'","'.mysql_real_escape_string(trim($_POST['short_desc'][$key])).'","'.mysql_real_escape_string(trim($_POST['highlights'][$key])).'","'.mysql_real_escape_string(trim($_POST['details'][$key])).'","'.mysql_real_escape_string(trim($_POST['pleasenote'][$key])).'","'.mysql_real_escape_string(trim($_POST['toptitle'][$key])).'","'.mysql_real_escape_string(trim($_POST['topdesc'][$key])).'")';
			$query .= ' ON DUPLICATE KEY UPDATE `title` = "'.mysql_real_escape_string(trim($_POST['title'][$key])).'", `eztitle` = "'.mysql_real_escape_string(trim($_POST['eztitle'][$key])).'", `short_desc` = "'.mysql_real_escape_string(trim($_POST['short_desc'][$key])).'", `highlights` = "'.mysql_real_escape_string(trim($_POST['highlights'][$key])).'", `details` = "'.mysql_real_escape_string(trim($_POST['details'][$key])).'", `pleasenote` = "'.mysql_real_escape_string(trim($_POST['pleasenote'][$key])).'", `toptitle` = "'.mysql_real_escape_string(trim($_POST['toptitle'][$key])).'", `topdesc` = "'.mysql_real_escape_string(trim($_POST['topdesc'][$key])).'"';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ if(mysql_affected_rows() > 0){ $numupdated++; } } else { array_push($errormsg,$thiserror); }

		} //End ForEach
	if($numupdated > 0): array_push($successmsg,$numupdated.' translations were added/updated for tour '.$_POST['edit'].'.'); endif;

	$index = array_search($_REQUEST['edit'],$tourids);
	if(isset($_POST['findnext']) && $_POST['findnext'] != "" && $index !== false && isset($tourids[($index+1)])){ $_REQUEST['edit'] = $tourids[($index+1)]; }


} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "current" && isset($_POST['edit']) && $_POST['edit'] != ""){

	$query = 'UPDATE `tours_translations` SET `modified` = CURRENT_TIMESTAMP WHERE `tourid` = "'.mysql_real_escape_string($_POST['edit']).'"';
		if($thisuser['languages'] !== "*"){ $query .= ' AND (tours_translations.`lang` = "'.implode('" OR tours_translations.`lang` = "',$thisuser['languages']).'")'; }
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""){ array_push($successmsg,mysql_affected_rows().' translations were marked as current for tour '.$_POST['edit'].'.'); } else { array_push($errormsg,$thiserror); }

}




echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Translations: Tours</U></FONT><BR><BR>';

printmsgs($successmsg,$errormsg);


//GET LANGUAGES
$languages = array();
$query = 'SELECT * FROM `languages` WHERE `accr` != "en"';
	if($thisuser['languages'] != "*"){ $query .= ' AND (`id` = "'.implode('" OR `id` = "',$thisuser['languages']).'")'; }
	$query .= ' ORDER BY `reference` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$languages['l'.$row['id']] = $row;
	}


if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){

$query = 'SELECT * FROM `tours` WHERE `id` = "'.$_REQUEST['edit'].'"';
	//if($thisuser['languages'] !== "*"){ $query .= ' AND (tours.`id` >= 1109 OR tours.`id` = 1006 OR tours.`id` = 1026 OR tours.`id` = 1030 OR tours.`id` = 1037)'; }
	$query .= ' LIMIT 1';
$result = mysql_query($query);
$tourinfo = mysql_fetch_assoc($result);

$translations = array();
$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$_REQUEST['edit'].'"';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$translations['t'.$row['lang']] = $row;
	}

//NEEDS UPDATING?
$query = 'SELECT tours_translations.`tourid`,tours_translations.`lang` FROM `tours_translations`,`tours` WHERE';
	$query .= ' tours_translations.`tourid` = tours.`id` AND tours_translations.`modified` < tours.`eng_modified`';
	$query .= ' AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != ""';
	$query .= ' AND `tourid` = "'.$_REQUEST['edit'].'"';
	if($thisuser['languages'] !== "*"){ $query .= ' AND (tours_translations.`lang` = "'.implode('" OR tours_translations.`lang` = "',$thisuser['languages']).'")'; }
	//echo '<!-- '.$query.' -->';
$result = mysql_query($query);
$neweng = mysql_num_rows($result);

if($neweng > 0){
	echo '<CENTER><TABLE CELLPADDING="3" CELLSPACING="0" STYLE="margin-top:20px; width:94%; border:1px solid #B00000;">'."\n";
		//echo '<TR><TD ALIGN="center" STYLE="background-color:#FFFFFF; border-bottom:1px solid #B00000; font-family:Arial; font-size:11pt;">WARNING</TD></TR>'."\n";
		echo '<TR><TD ALIGN="center" STYLE="background-color:#F3D9D9; font-family:Arial; font-size:10pt;">Some English text has changed since being translated.<BR>Please review all English text and update the corresponding translations as needed.</TD></TR>'."\n";
		echo '</TABLE><BR></CENTER>'."\n\n";
	}


echo '<FORM METHOD="post" NAME="editform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="update">'."\n";
echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$tourinfo['id'].'">'."\n\n";

foreach($languages as $lang){
	echo '<INPUT TYPE="hidden" NAME="langid[]" VALUE="'.$lang['id'].'">'."\n";
	}
	echo "\n";

echo '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="0" CELLPADDING="3">'."\n";
	//nl2br(htmlspecialchars($tourinfo[$field]))

	//TITLE
	$field = 'title';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:100px;">English';
		echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal;"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($tourinfo['alias']).'&lang=1" TARGET="_blank">View in context</A></SPAN>';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:40px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//EZTITLE
	$field = 'eztitle';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title for EZ Tour Finder</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:40px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//SHORT DESC
	$field = 'short_desc';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Short Description for EZ Tour Finder</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:80px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//HIGHLIGHTS
	$field = 'highlights';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Highlights (appears as bulleted list)</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal;"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($tourinfo['alias']).'&lang=1" TARGET="_blank">View in context</A></SPAN>';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
		$tourinfo[$field] = explode("\n",$tourinfo[$field]);
		foreach($tourinfo[$field] as $var){
			$var = trim($var);
			if($var != ""){ echo $var.'<BR>'; }
			}
		echo '</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:200px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//DETAILS
	$field = 'details';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Details</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal;"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($tourinfo['alias']).'&lang=1#details" TARGET="_blank">View in context</A></SPAN>';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:240px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//PLEASE NOTE
	$field = 'pleasenote';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Please note the following... (appears as bulleted list)</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal;"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($tourinfo['alias']).'&lang=1#pleasenote" TARGET="_blank">View in context</A></SPAN>';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
		$tourinfo[$field] = explode("\n",$tourinfo[$field]);
		foreach($tourinfo[$field] as $var){
			$var = trim($var);
			if($var != ""){ echo $var.'<BR>'; }
			}
		echo '</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:200px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//TOP TITLE
	$field = 'toptitle';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title for "Top Grand Canyon Tours"</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:40px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	//TOP DESC
	$field = 'topdesc';
	if(isset($tourinfo[$field]) && trim($tourinfo[$field]) != ""){
	bgcolor('reset');
	echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;"><TD COLSPAN="2" ALIGN="center" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Short Description for "Top Grand Canyon Tours"</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">English';
		if(strtotime($tourinfo['eng_modified']) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime($tourinfo['eng_modified'])).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">'.nl2br($tourinfo[$field]).'</TD></TR>'."\n";
	foreach($languages as $lang){
		$fillform = $translations['t'.$lang['id']];
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.$lang['reference'];
		if(strtotime(getval('modified')) > 0){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.date("n/j/Y g:ia",strtotime(getval('modified'))).'</SPAN>'; }
		echo '</TD><TD STYLE="font-family:Arial; font-size:10pt;">';
			if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){ echo '<TEXTAREA NAME="'.$field.'[]" STYLE="width:100%; height:80px;">'.getval($field).'</TEXTAREA>'; } else { echo nl2br(getval($field)); }
			echo '</TD></TR>'."\n";
		}
	echo '<TR><TD COLSPAN="2" STYLE="font-size:2pt; height:20px;">&nbsp;</TD></TR>'."\n\n";
	} else {
	echo '<TR><TD>';
		foreach($languages as $lang){ echo '<INPUT TYPE="hidden" NAME="'.$field.'[]" VALUE="">'; }
		echo '</TD></TR>'."\n\n";
	} //End Field if statement

	if($thisuser['allow'] == "*" || in_array($pageid.'_edit',$thisuser['allow'])){
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="padding-left:162px;">';
		echo '<DIV STYLE="width:160px; text-align:right; float:right;"><INPUT TYPE="submit" NAME="findnext" VALUE="Save and edit next &gt;" STYLE="color:#0000FF;"></DIV>';
		echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;">';
		echo '</TD></TR>'."\n";
		}

	echo '</TABLE><BR>'."\n\n";

//echo '<INPUT TYPE="submit" VALUE="Save" STYLE="width:180px;"><BR><BR>'."\n\n";


if($neweng > 0){
	echo '</FORM>'."\n\n";

	echo '<FORM NAME="restoreform" ID="restoreform" METHOD="POST" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
		echo '<INPUT TYPE="hidden" NAME="utaction" ID="utaction" VALUE="current">'."\n";
		echo '<INPUT TYPE="hidden" NAME="edit" VALUE="'.$_REQUEST['edit'].'">'."\n";

	echo '<TABLE CELLPADDING="3" CELLSPACING="0" STYLE="margin-top:20px; width:94%; border:1px solid #B00000;">'."\n";
		echo '<TR><TD ALIGN="center" STYLE="background-color:#F3D9D9; font-family:Arial; font-size:10pt;">';
			echo '<B>If you have made changes above, <I>CLICK ON SAVE</I>.  The green check mark will appear.</B><BR>However, if you have not made changes, but have reviewed all translations and they are current and accurate, please click below.<BR>';
			echo '<INPUT TYPE="submit" VALUE="All translations are current">'."\n";
			echo '</TD></TR>'."\n";
		echo '</TABLE><BR>'."\n\n";
	}


echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Back to Index</A>'."\n\n";




} else {


//GET TOURS
$tours = array();
$tourids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS tours.`id`, tours.`alias`, tours.`title`, tours.`hidden`, COUNT(tours_translations.`id`) as `complete` FROM `tours`';
	$query .= ' LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid`';
		$query .= ' AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != ""';
		$query .= ' AND tours_translations.`modified` >= tours.`eng_modified`';
		if($thisuser['languages'] !== "*"){ $query .= ' AND (tours_translations.`lang` = "'.implode('" OR tours_translations.`lang` = "',$thisuser['languages']).'")'; }
	$query .= ' WHERE 1 = 1';
	if($_SESSION['translations_tours']['showhidden'] != "1"): $query .= ' AND tours.`hidden` = "0"'; endif;
	//if($thisuser['languages'] !== "*"){ $query .= ' AND (tours.`id` >= 1109 OR tours.`id` = 1006 OR tours.`id` = 1026 OR tours.`id` = 1030 OR tours.`id` = 1037)'; }
	$query .= ' GROUP BY tours.`id`';
	$query .= ' ORDER BY '.$_SESSION['translations_tours']['sortby'].' '.$_SESSION['translations_tours']['sortdir'].', tours.`id` DESC';
	$query .= ' LIMIT '.(($_SESSION['translations_tours']['p']-1)*$_SESSION['translations_tours']['limit']).','.$_SESSION['translations_tours']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$row['complete'] = array();
	$row['updated'] = array();
	$tours['t'.$row['id']] = $row;
	array_push($tourids,$row['id']);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['translations_tours']['limit']);
if($numpages > 0 && $_SESSION['translations_tours']['p'] > $numpages): $_SESSION['translations_tours']['p'] = $numpages; endif;

//GET COMPLETED TRANSLATIONS
$query = 'SELECT tours_translations.`tourid`,tours_translations.`lang` FROM `tours_translations`,`tours` WHERE';
	$query .= ' tours_translations.`tourid` = tours.`id` AND tours_translations.`modified` >= tours.`eng_modified`';
	$query .= ' AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != ""';
	$query .= ' AND (tours_translations.`tourid` = "'.implode('" OR tours_translations.`tourid` = "',$tourids).'")';
	//echo '<!-- '.$query.' -->';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours['t'.$row['tourid']]['complete'],$row['lang']);
	}

//GET UPDATED TRANSLATIONS
$query = 'SELECT tours_translations.`tourid`,tours_translations.`lang` FROM `tours_translations`,`tours` WHERE';
	$query .= ' tours_translations.`tourid` = tours.`id` AND tours_translations.`modified` < tours.`eng_modified`';
	$query .= ' AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != ""';
	$query .= ' AND (tours_translations.`tourid` = "'.implode('" OR tours_translations.`tourid` = "',$tourids).'")';
	//echo '<!-- '.$query.' -->';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours['t'.$row['tourid']]['updated'],$row['lang']);
	}


echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Sort:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="sortby" STYLE="font-size:9pt;">';
		foreach($sortby as $key => $sort){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['translations_tours']['sortby'] == $key): echo " SELECTED"; endif;
			echo '>'.$sort.'</OPTION>'."\n";
			}
		echo '</SELECT><SELECT NAME="sortdir" STYLE="font-size:9pt;">';
			echo '<OPTION VALUE="ASC"'; if($_SESSION['translations_tours']['sortdir'] == "ASC"): echo " SELECTED"; endif; echo '>Asc</OPTION>';
			echo '<OPTION VALUE="DESC"'; if($_SESSION['translations_tours']['sortdir'] == "DESC"): echo " SELECTED"; endif; echo '>Desc</OPTION>';
			echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:0px;"><LABEL FOR="showhidden">Show hidden:</LABEL></TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="checkbox" NAME="showhidden" ID="showhidden" VALUE="1"';
		if($_SESSION['translations_tours']['showhidden'] == "1"): echo ' CHECKED'; endif;
		echo '></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['translations_tours']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="Sort" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


echo '<TABLE BORDER="0"><TR>
	<TD ALIGN="right" VALIGN="middle"><IMG SRC="img/check.png" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation is complete and up to date.</TD>
	<TD ALIGN="right" VALIGN="middle" STYLE="padding-left:20px;"><IMG SRC="img/dot_yellow.gif" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation needs to be updated.</TD>
	</TR></TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_tours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_tours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_tours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_tours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_tours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<FORM METHOD="post" NAME="routeform" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="batch">'."\n\n";

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">ID</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Title</TD>';
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; padding-left:2px; padding-right:2px; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; cursor:help;" TITLE="'.$lang['reference'].'">'.ucwords($lang['accr']).'</TD>';
		}
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;">Edit</TD>';
	echo '<TD ALIGN="center" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF; padding-right:3px;" TITLE="View on Bundu Bashers website">View</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($tours as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:6px; padding-right:8px; font-family:Arial; font-size:10pt;'.tohide($row['hidden']).'"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt; '.tohide($row['hidden']).'"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'" STYLE="color:inherit; text-decoration:none;">';
		if($row['hidden'] == '1'): echo '[HIDDEN] '; endif;
		echo $row['title'].'</A></TD>';
	foreach($languages as $lang){
		echo '<TD ALIGN="center" STYLE="padding-left:2px; padding-right:2px; font-family:Arial; font-size:8pt;">';
			if(in_array($lang['id'],$row['complete'])){
				echo '<A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'&lang='.$lang['id'].'" TARGET="_blank"><IMG SRC="img/check.png" BORDER="0" TITLE="'.$lang['reference'].'"></A>';
				} elseif(in_array($lang['id'],$row['updated'])){
				echo '<A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/dot_yellow.gif" BORDER="0" TITLE="Please review and update."></A>';
				} else {
				echo '&nbsp;';
				}
			echo '</TD>';
		}
	echo '<TD ALIGN="center"><A HREF="'.$_SERVER['PHP_SELF'].'?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '<TD ALIGN="center"><A HREF="http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'&lang=1" TARGET="_blank"><IMG SRC="img/viewico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	$sel++;
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_tours']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_tours']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['translations_tours']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['translations_tours']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['translations_tours']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0"><TR>
	<TD ALIGN="right" VALIGN="middle"><IMG SRC="img/check.png" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation is complete and up to date.</TD>
	<TD ALIGN="right" VALIGN="middle" STYLE="padding-left:20px;"><IMG SRC="img/dot_yellow.gif" BORDER="0"></TD>
	<TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:9pt;">= Translation needs to be updated.</TD>
	</TR></TABLE>'."\n\n";

} //END EDIT IF STATEMENT


echo '</FORM>'."\n\n";


require("footer.php");

?>