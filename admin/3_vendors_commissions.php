<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$working = 0;

$pageid = "3_vendors_commissions";
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != ""){ $pageid .= '_edit'; }
require("validate.php");
require("header.php");

if(!isset($_SESSION['vendors_commissions']['p'])): $_SESSION['vendors_commissions']['p'] = 1; endif;
if(!isset($_SESSION['vendors_commissions']['view'])): $_SESSION['vendors_commissions']['view'] = 'unpaid'; endif;
if(!isset($_SESSION['vendors_commissions']['vendor'])): $_SESSION['vendors_commissions']['vendor'] = '*'; endif;
if(!isset($_SESSION['vendors_commissions']['limit'])): $_SESSION['vendors_commissions']['limit'] = "50"; endif;

if(isset($_REQUEST['p']) && $_REQUEST['p'] != ""): $_SESSION['vendors_commissions']['p'] = $_REQUEST['p']; endif;
if(isset($_REQUEST['view']) && $_REQUEST['view'] != ""): $_SESSION['vendors_commissions']['view'] = $_REQUEST['view']; endif;
if(isset($_REQUEST['vendor']) && $_REQUEST['vendor'] != ""): $_SESSION['vendors_commissions']['vendor'] = $_REQUEST['vendor']; endif;
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != ""): $_SESSION['vendors_commissions']['limit'] = $_REQUEST['limit']; endif;

if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "changesort"){
	$_SESSION['vendors_commissions']['p'] = '1';
	}

$viewopts = array(
	'unpaid' => 'Unpaid commissions',
	'paid' => 'Paid commissions',
	'*' => 'All'
	);

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}


//echo '<PRE>'; print_r($_POST); echo '</PRE>';


echo '<CENTER><BR><FONT FACE="Arial" SIZE="5"><U>Vendor Commissions</U></FONT><BR>'."\n\n";


//GET RESERVATIONS
$reservations = array();
$resids = array();
$query = 'SELECT SQL_CALC_FOUND_ROWS reservations.`id`, reservations_assoc.`name`, reservations_assoc.`date`, reservations_assoc.`vendor`, vendors.`name` AS `vendorname`, reservations_assoc.`vendorconf`, reservations_assoc.`vendorpaid`';
	//$query .= ', (SELECT COUNT(reservations_assoc.`vendorconf`) FROM `reservations_assoc` WHERE reservations_assoc.`reservation` = reservations.`id` AND reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0 AND TRIM(reservations_assoc.`vendorconf`) NOT LIKE "") AS `numvconf`';
	//$query .= ', (SELECT COUNT(reservations_assoc.`vendor`) FROM `reservations_assoc` WHERE reservations_assoc.`reservation` = reservations.`id` AND reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0) AS `numvendors`';
	$query .= ' FROM `reservations`';
	$query .= ' JOIN `reservations_assoc` ON reservations_assoc.`reservation` = reservations.`id` AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`vendor` != 0';
		if($_SESSION['vendors_commissions']['view'] == 'paid'){
			$query .= ' AND `vendorpaid` > 0';
			} elseif($_SESSION['vendors_commissions']['view'] == 'unpaid'){
			$query .= ' AND `vendorpaid` = 0';
			}
	$query .= ' JOIN `vendors` ON vendors.`id` = reservations_assoc.`vendor`';
		if($_SESSION['vendors_commissions']['vendor'] != '*'){
			$query .= ' AND vendors.`id` = '.$_SESSION['vendors_commissions']['vendor'];
			}
	$query .= ' WHERE reservations.`canceled` = 0';
		//$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`vendor` != 0';
	$query .= ' ORDER BY reservations.`id` DESC';
	$query .= ' LIMIT '.(($_SESSION['vendors_commissions']['p']-1)*$_SESSION['vendors_commissions']['limit']).','.$_SESSION['vendors_commissions']['limit'];
$result = mysql_query($query);
//echo $query.'<BR>'."\n";
//echo mysql_error().'<BR>';
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($reservations,$row);
	array_push($resids,$row['id']);
	}

$numitems = @mysql_query('SELECT FOUND_ROWS() as `numitems`');
$numitems = mysql_fetch_assoc($numitems);
$numitems = $numitems['numitems'];
$numpages = ceil($numitems / $_SESSION['vendors_commissions']['limit']);
if($numpages > 0 && $_SESSION['vendors_commissions']['p'] > $numpages): $_SESSION['vendors_commissions']['p'] = $numpages; endif;

$link = '';

//GET VENDOR CONFIRMATIONS
/*$vendorconfs = array();
$query = 'SELECT reservations_assoc.`reservation`,vendors.`name`,reservations_assoc.`vendor`,reservations_assoc.`vendorconf` FROM `reservations_assoc` LEFT JOIN `vendors` ON reservations_assoc.`vendor` = vendors.`id` WHERE reservations_assoc.`vendor` > 0 AND reservations_assoc.`canceled` = 0 AND (reservations_assoc.`reservation` = "'.implode('" OR reservations_assoc.`reservation` = "',$resids).'") ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC, reservations_assoc.`id` ASC';
$result = mysql_query($query);
//echo '<!-- '.$query.' -->'."\n\n";
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($vendorconfs['v'.$row['reservation']])){ $vendorconfs['v'.$row['reservation']] = array(); }
	array_push($vendorconfs['v'.$row['reservation']],$row);
	}*/

echo '<FORM METHOD="GET" ACTION="'.$_SERVER['PHP_SELF'].'">
	<INPUT TYPE="hidden" NAME="utaction" VALUE="changesort">'."\n";

echo '<DIV STYLE="width:94%; background:#CCCCFF; border:1px solid #666666; padding:2px; text-align:center;"><CENTER>'."\n";
	echo '	<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"><TR>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">View:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="view" STYLE="font-size:9pt;">';
		foreach($viewopts as $key => $val){
			echo '<OPTION VALUE="'.$key.'"';
			if($_SESSION['vendors_commissions']['view'] == $key): echo " SELECTED"; endif;
			echo '>'.$val.'</OPTION>'."\n";
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Vendor:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><SELECT NAME="vendor" STYLE="font-size:9pt; width:200px;">';
			echo '<OPTION VALUE="*">All vendors</OPTION>';
		foreach($vendors as $row){
			echo '<OPTION VALUE="'.$row['id'].'"';
			if($_SESSION['vendors_commissions']['vendor'] == $row['id']): echo " SELECTED"; endif;
			echo '>'.$row['name'].'</OPTION>'."\n";
			}
		echo '</SELECT></TD>'."\n";
	echo '	<TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:4px;">Items/page:</TD>'."\n";
	echo '	<TD ALIGN="left" STYLE="padding-right:10px;"><INPUT TYPE="text" NAME="limit" STYLE="width:30px; font-size:9pt;" VALUE="'.$_SESSION['vendors_commissions']['limit'].'"></TD>'."\n";
	echo '	<TD><INPUT TYPE="submit" VALUE="View" STYLE="width:40px; font-size:9pt;"></TD>'."\n";
	echo '	</TR></TABLE>'."\n";
echo '</CENTER></DIV>'."\n";

echo '</FORM>';


	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['vendors_commissions']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['vendors_commissions']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['vendors_commissions']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['vendors_commissions']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['vendors_commissions']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing

echo '<TABLE BORDER="0" CELLSPACING="0" ID="routetable" STYLE="width:94%">'."\n";

echo '<TR STYLE="background:#666666 url(\'img/topbar.jpg\') center center repeat-x;">';
	//echo '<TD ALIGN="center" STYLE="height:26px; border-bottom:solid 2px #000000;"><INPUT TYPE="checkbox" ID="selall" onClick="selectall()"></TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Conf#</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Name</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Date</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF;">Vendor details</TD>';
	//echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; white-space:nowrap;">Confirmation</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10pt; font-weight:bold; color:#FFFFFF; white-space:nowrap; padding-right:10px;">Com. Paid</TD>';
	echo '<TD ALIGN="center" VALIGN="middle" STYLE="border-bottom:solid 2px #000000; font-family:Arial; font-size:10px; font-weight:bold; color:#FFFFFF;">Edit</TD>';
	echo '</TR>'."\n\n";

$sel = 0;
bgcolor('');

foreach($reservations as $key => $row){
echo '<TR STYLE="background:#'.bgcolor('').'">';
	//echo '<TD ALIGN="center"><INPUT TYPE="checkbox" ID="sel'.$sel++.'" NAME="selitems[]" VALUE="'.$row['id'].'"></TD>';
	echo '<TD ALIGN="left" STYLE="padding-left:5px; padding-right:10px; font-family:Arial; font-size:10pt; font-weight:bold;"><A HREF="3_reservations.php?view='.$row['id'].'">'.$row['id'].'</A></TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.$row['name'].'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.date("m/d/y",$row['date']).'</TD>';
	echo '<TD ALIGN="left" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">'.trim($row['vendorname'].' '.$row['vendorconf']).'</TD>';
	echo '<TD ALIGN="center" STYLE="padding-right:10px; font-family:Arial; font-size:10pt;">';
		if($row['vendorpaid'] > 0){ echo 'Yes'; } else { echo 'No'; }
		echo '</TD>';
	echo '<TD ALIGN="center"><A HREF="3_reservations.php?edit='.$row['id'].'"><IMG SRC="img/editico.gif" BORDER="0"></A></TD>';
	echo '</TR>'."\n\n";
	}

echo '</TABLE>'."\n\n";

	// PRINT OUT PAGE LISTING
	if($numpages > 1){
	echo '<TABLE BORDER="0" STYLE="width:94%" CELLSPACING="0" CELLPADDING="0"><TR>';
	echo '<TD ALIGN="left" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-left:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['vendors_commissions']['p'] > 1): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['vendors_commissions']['p']-1).'">&lt; Previous Page</A></B>'; endif;
		echo '</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-top:2px; padding-bottom:2px;">'.$numitems.' items total - Viewing page <SELECT STYLE="font-size:9pt;" onChange="javascript:window.location='."'".$_SERVER['PHP_SELF'].'?'.$link.'p=\'+this.value;">';
		for($i=1; $i<=$numpages; $i++){
		echo '<OPTION VALUE="'.$i.'"';
			if($i == $_SESSION['vendors_commissions']['p']): echo ' SELECTED'; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT> of '.$numpages.'</FONT></TD>';
	echo '<TD ALIGN="right" WIDTH="200" STYLE="font-family:Arial; font-size:9pt; padding-right:6px; padding-top:2px; padding-bottom:2px;">';
		if($_SESSION['vendors_commissions']['p'] < $numpages): echo '<B><A HREF="'.$_SERVER['PHP_SELF'].'?'.$link.'p='.($_SESSION['vendors_commissions']['p']+1).'">Next Page &gt;</A></B>'; endif;
		echo '</TD>';
	echo '</TR></TABLE>'."\n\n";
	} //End Page Listing




require("footer.php");

?>