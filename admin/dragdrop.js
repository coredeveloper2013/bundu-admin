
window.onload = CreateDragContainer;
document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;

var dragObject  = null;
var dragOrigin = 'n';
var dragTarget = 'n';
var mouseOffset = null;
var dropTargets = new Array();

function testIsValidObject(objToTest){
	if(null == objToTest){
		return false;
	}
	if("undefined" == typeof(objToTest) ){
		return false;
	}
	return true;
}

function addDropTarget(dropTarget){
	dropTargets.push(dropTarget);
}

function CreateDragContainer(){
	dropTargets = new Array();
	var i=1;
	while( testIsValidObject( document.getElementById("step"+i) ) ){
	addDropTarget( document.getElementById("step"+i) );
	i++;
	}
	//document.write('<DIV ID="forshadow" STYLE="display:none;"></DIV>');
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}

function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject && dropTargets.length > 1){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;
		dragObject.style.opacity  = '.70';
		dragObject.style.filter   = 'alpha(opacity=70)';
		dragObject.style.display = '';

		findtarget(ev);

		return false;
	}
}

function makeDraggable(item){
	if(!item) return;
	item.onmousedown = function(ev){

		shadow = document.getElementById("forshadow");
		dragObject  = shadow;

		shadow.innerHTML = item.innerHTML;

		mouseOffset = getMouseOffset(item, ev);

		dragOrigin = 'n';
		for(var i=0; i<dropTargets.length; i++){
			if(item.id == dropTargets[i].id){ 
			dragOrigin = i;
			break;
			}
		}
		return false;
	}
	item.style.cursor = "pointer";
}

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseUp(ev){
	ev           = ev || window.event;
	findtarget(ev);
	movem();

	document.getElementById("forshadow").style.display = 'none';
	dragObject   = null;
	dragOrigin   = 'n';
	cleartarg();
}

function manualmov(o,t){
	document.getElementById("man"+o).value = '';
	o--; t--;
	if( testIsValidObject( dropTargets[o] ) && testIsValidObject( dropTargets[t] )){
	dragOrigin = o;
	dragTarget = t;
	movem();
	}
}

function movem(){
	if(dragOrigin != 'n' && dragTarget != 'n'){

	svid = document.getElementById( "itid"+dropTargets[dragOrigin].id ).value;
	svcode = document.getElementById( dropTargets[dragOrigin].id ).innerHTML;

	i=dragOrigin;
	while(i != dragTarget){
		if(dragOrigin < dragTarget){ newi = eval(i + 1); } else { newi = eval(i - 1); }
		//alert('Move '+newi+' to '+i+'.');
		document.getElementById( "itid"+dropTargets[i].id ).value = document.getElementById( "itid"+dropTargets[newi].id ).value;
		document.getElementById( dropTargets[i].id ).innerHTML = document.getElementById( dropTargets[newi].id ).innerHTML;
		i = newi;
		}
	document.getElementById( "itid"+dropTargets[dragTarget].id ).value = svid;
	document.getElementById( dropTargets[dragTarget].id ).innerHTML = svcode;

	}
}

function findtarget(ev){
	var mousePos = mouseCoords(ev);
	for(var i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);
		var newTarget  = 'n';

		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
				//newTarget = dropTargets[i].id;
				newTarget = i;
				break;
		}
	}

	if(newTarget != "n"){
		if(newTarget != dragTarget){
		cleartarg();
		dragTarget = newTarget;
			if(newTarget < dragOrigin){
			document.getElementById( dropTargets[newTarget].id ).style.borderTop = "4px solid #0000FF";
			} else if(newTarget > dragOrigin)  {
			document.getElementById( dropTargets[newTarget].id ).style.borderBottom = "4px solid #0000FF";
			}
		}
	} else {
		cleartarg();
	}
}

function cleartarg(){
	if(dragTarget != 'n' && testIsValidObject( dropTargets[dragTarget] )){
		document.getElementById( dropTargets[dragTarget].id ).style.borderTop = "0px solid #FFFFFF";
		document.getElementById( dropTargets[dragTarget].id ).style.borderBottom = "0px solid #FFFFFF";
		}
	dragTarget = 'n';
}
