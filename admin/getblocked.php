<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


function getblocked($routes='*',$start='*',$end='*',$showdebug='n'){

$blocked = array();
$debug = '';

	//PROCESS VARIABLES
	if($routes != '*' && !is_array($routes)){
		$routes = str_replace('|',',',$routes);
		$routes = explode(',',$routes);
		}
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){
		if(!is_array($start)){
			$start = str_replace('|',',',$start);
			$start = explode(',',start);
			}
		sort($start,SORT_NUMERIC);
		}

	$debug .= 'Routes: '.print_r($routes,TRUE)."\n";
		$debug .= 'Start: '; if(is_numeric($start)){ $debug .= date("n/j/Y G",$start); } else { $debug .= $start; } $debug .= "\n";
		$debug .= 'End: '; if(is_numeric($end)){ $debug .= date("n/j/Y G",$end); } else { $debug .= $end; } $debug .= "\n";
		$debug .= "\n";

	//COLLECT TOUR SEATS
	$query = 'SELECT routes_dates.`routeid`, routes_dates.`date`, SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) AS `guests`';
		$query .= ' FROM `reservations_assoc`,`tours_assoc`,`tours_dates`,`routes_dates`';
		$query .= ' WHERE reservations_assoc.`canceled` = 0';
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o")';
			$query .= ' AND reservations_assoc.`tourid` = tours_assoc.`tourid`';
			$query .= ' AND tours_dates.`tourid` = tours_assoc.`tourid`';

			$query .= ' AND tours_assoc.`dir` = tours_dates.`dir`';

			$query .= ' AND tours_assoc.`type` = "r"';
			$query .= ' AND routes_dates.`routeid` = tours_assoc.`typeid`';
			if(is_array($routes)){ $query .= ' AND (routes_dates.`routeid` = "'.implode('" OR routes_dates.`routeid` = "',$routes).'")'; }

			if(is_array($start) && count($start) > 0){
				$query .= ' AND (routes_dates.`date` = "'.implode('" OR routes_dates.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND routes_dates.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND routes_dates.`date` <= '.$end; }
				}

			$query .= ' AND tours_dates.`date` = reservations_assoc.`date`';
			$query .= ' AND tours_dates.`date` >= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) - 3600)';
			$query .= ' AND tours_dates.`date` <= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) + 3600)';

		$query .= ' GROUP BY routes_dates.`routeid`, routes_dates.`date` ORDER BY NULL';
		$debug .= 'Starting on tour seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror = ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
		if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
			$blocked['r'.$row['routeid']]['d'.$row['date']] = array('route'=>$row['routeid'],'date'=>$row['date'],'tour_seats'=>0,'route_seats'=>0);
			}
			$debug .= "\t".'Adding - Route: '.$row['routeid'].' Date: '.date("n/j/Y G",$row['date']).' Tour Seats: '.$row['guests']."\n";
		$blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['tour_seats']+$row['guests']);
		}
		$debug .= "\n";

	//COLLECT ROUTE SEATS
	$query = 'SELECT routes_dates.`routeid`, routes_dates.`date`, SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) AS `guests`';
		$query .= ' FROM `reservations_assoc`,`routes_dates`';
		$query .= ' WHERE reservations_assoc.`canceled` = 0';
			$query .= ' AND (reservations_assoc.`type` = "r" OR reservations_assoc.`type` = "o")';
			$query .= ' AND reservations_assoc.`date` = routes_dates.`date`';
			$query .= ' AND reservations_assoc.`routeid` = routes_dates.`routeid`';
			if(is_array($routes)){ $query .= ' AND (routes_dates.`routeid` = "'.implode('" OR routes_dates.`routeid` = "',$routes).'")'; }
			if(is_array($start) && count($start) > 0){
				$query .= ' AND (routes_dates.`date` = "'.implode('" OR routes_dates.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND routes_dates.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND routes_dates.`date` <= '.$end; }
				}
		$query .= ' GROUP BY routes_dates.`routeid`, routes_dates.`date` ORDER BY NULL';
		$debug .= 'Starting on route seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror = ""){ $debug .= 'Error with route seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' route/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(!isset($blocked['r'.$row['routeid']])){ $blocked['r'.$row['routeid']] = array(); }
		if(!isset($blocked['r'.$row['routeid']]['d'.$row['date']])){
			$blocked['r'.$row['routeid']]['d'.$row['date']] = array('route'=>$row['routeid'],'date'=>$row['date'],'tour_seats'=>0,'route_seats'=>0);
			}
			$debug .= "\t".'Adding - Route: '.$row['routeid'].' Date: '.date("n/j/Y G",$row['date']).' Route Seats: '.$row['guests']."\n";
		$blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats'] = ($blocked['r'.$row['routeid']]['d'.$row['date']]['route_seats']+$row['guests']);
		}

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $blocked;

	} //END getblocked()


function getblocked_tours($tours='*',$start='*',$end='*',$showdebug='n'){

$blocked = array();
$debug = '';

	//PROCESS VARIABLES
	if($tours != '*' && !is_array($tours)){
		$tours = str_replace('|',',',$tours);
		$tours = explode(',',$tours);
		}
	$debug .= 'Tours: '.print_r($tours,TRUE)."\n";
		$debug .= 'Start: '; if(is_numeric($start)){ $debug .= date("n/j/Y G",$start); } else { $debug .= $start; } $debug .= "\n";
		$debug .= 'End: '; if(is_numeric($end)){ $debug .= date("n/j/Y G",$end); } else { $debug .= $end; } $debug .= "\n";
		$debug .= "\n";

	//COLLECT TOUR SEATS
	$query = 'SELECT reservations_assoc.`tourid`, reservations_assoc.`date`, SUM((reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`)) AS `guests`';
		$query .= ' FROM `reservations_assoc`';
		$query .= ' WHERE reservations_assoc.`canceled` = 0';
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o")';
			$query .= ' AND reservations_assoc.`tourid` > 0';
			if(is_array($tours)){ $query .= ' AND (reservations_assoc.`tourid` = "'.implode('" OR reservations_assoc.`tourid` = "',$tours).'")'; }
			
			if(is_numeric($start)){ $query .= ' AND reservations_assoc.`date` >= '.$start; }
			if(is_numeric($end)){ $query .= ' AND reservations_assoc.`date` <= '.$end; }

		$query .= ' GROUP BY reservations_assoc.`tourid`, reservations_assoc.`date` ORDER BY NULL';
		$debug .= 'Finding tour seats...'."\n";
		//$debug .= $query."\n\n";
	$result = @mysql_query($query);
	$thiserror = @mysql_error(); if($thiserror = ""){ $debug .= 'Error with tour seats query: '.$thiserror."\n"; }
	$num_results = @mysql_num_rows($result);
	$debug .= 'Found '.$num_results.' tour/date sets.'."\n";
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(!isset($blocked['t'.$row['tourid']])){ $blocked['t'.$row['tourid']] = array(); }
		if(!isset($blocked['t'.$row['tourid']]['d'.$row['date']])){
			$debug .= "\t".'Adding - Tour: '.$row['tourid'].' Date: '.date("n/j/Y G",$row['date']).' Blocked: '.$row['guests']."\n";
			$blocked['t'.$row['tourid']]['d'.$row['date']] = array('tour'=>$row['tourid'],'date'=>$row['date'],'blocked'=>$row['guests']);
			}
		}
		$debug .= "\n";

	if($showdebug == "y"){ echo '<PRE>'.$debug.'</PRE>'; }
	return $blocked;

	} //END getblocked_tours()


if(isset($_REQUEST['getblocked_routes'])){

	@date_default_timezone_set('America/Denver');
	$time = mktime();

	if(!$db){ $db = @MYSQL_CONNECT("localhost","sessel_bbsite","?R&zTQ=UdFX$"); }
	@mysql_select_db("sessel_bundubashers");
	@mysql_query("SET NAMES utf8");

	foreach($_REQUEST as $key => $row){
		$_REQUEST[$key] = trim($_REQUEST[$key]);
		$_REQUEST[$key] = strip_tags($_REQUEST[$key]);
		}
	
	if(!isset($_REQUEST['getblocked_start']) || trim($_REQUEST['getblocked_start']) == ""){ $_REQUEST['getblocked_start'] = '*'; }
	if(!isset($_REQUEST['getblocked_end']) || trim($_REQUEST['getblocked_end']) == ""){ $_REQUEST['getblocked_end'] = '*'; }
	if(!isset($_REQUEST['getblocked_debug']) || trim($_REQUEST['getblocked_debug']) == ""){ $_REQUEST['getblocked_debug'] = 'n'; }

	$blocked = getblocked($_REQUEST['getblocked_routes'],$_REQUEST['getblocked_start'],$_REQUEST['getblocked_end'],$_REQUEST['getblocked_debug']);
		//echo '<PRE>'; print_r($blocked); echo '</PRE>';

	echo '||BEGIN||'."\n";
	foreach($blocked as $key => $route){
		foreach($route as $thisdate){
			echo date("Y",$thisdate['date']).'|'.date("n",$thisdate['date']).'|'.date("j",$thisdate['date']).'|'.$thisdate['route'].'|'.$thisdate['tour_seats'].'|'.$thisdate['route_seats']."\n";
			}
		}
		echo '||END||'."\n";

	} //End ID if statement

?>