<?

require_once '../common.inc.php';

$version = 'Bundu Administration';


if(!isset($_SESSION['valid_user'])){
$menu = array(
	array("index","Log In")
	//array("resetpass","Reset Password")
	);
} else {

$aday = 86400;
$menucats = array(
	"mc1" => "Schedule",
	"mc2" => "Reservations",
	"mc3" => "Routes",
	"mc4" => "Tours",
	"mc5" => "Shuttle",
	"mc6" => "Vendors",
	"mc7" => "Translations",
	"mc8" => "Admin",
	"mc9" => "Lodging"
	);
$menu = array(
	array("3_schedule","Today",1,'start='.$today),
	array("3_schedule","Tomorrow",1,'start='.mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time))),
	array("3_schedule",date("l",mktime(0,0,0,date("n",$time),(date("j",$time)+2),date("Y",$time))),1,'start='.mktime(0,0,0,date("n",$time),(date("j",$time)+2),date("Y",$time))),
	array("3_schedule","Next 10 days",1,'start='.$today.'&end='.mktime(0,0,0,date("n",$time),(date("j",$time)+10),date("Y",$time))),
	array("3_schedule","Next 45 days",1,'start='.$today.'&end='.mktime(0,0,0,date("n",$time),(date("j",$time)+45),date("Y",$time))),
	array("3_tours_upcoming","Upcoming tours",1),

	array("3_reservations","New Reservation",2,'edit=*new*'),
	array("3_search","Search",2),
	array("3_reservations","Review",2),
	array("3_bbtickets","HOHO Tickets",2),
	array("3_cancellations","Cancellations",2),
	array("3_agents","Agents",2),
	array("3_agents_reservations","Agent Reservations",2),

	array("3_tours","New Tour",4,'edit=*new*'),
	array("3_tours","Tour Index",4),
	array("3_tour_closeout_spreadsheets","Close Out Spreadsheets",4),
	array("3_prvtours","Private Tours",4),
	array("3_tours_extlodging","Lodging extensions",4),
	array("3_tours_transport","Tour transport",4),
	array("3_activities","Activities",4),
	array("3_itinerary","Itinerary entries",4),
	array("3_images","Images",4),
	array("3_meansoftrans","Means of Transp...",4),
	array("3_bashersmenu","Bashers Menu",4),

	array("3_routes","New Route",3,'edit=*new*'),
	array("3_routes","Route Index",3),
	array("3_routecalendar","Route Calendar",3),
	array("3_locations","Stops/Locations",3),
	array("3_miniroutes","Mini Routes",3),
	array("3_bussettings","Bundu Bus Settings",3),

	array("3_markets","Markets",5),
	array("3_transtypes","Trans. Types",5),

	array("3_lodging","Lodging",9),
        array("3_lodging_new","Lodging Admin",9),
	array("3_lodging_calendar","Calendar",9),
    array("3_ratesAndAvailability", "Rates & Availability", 9),

	array("3_vendors","Vendors",6),
	array("3_vendors_commissions","Commissions",6),
	array("3_bookings","Needed bookings",6),

	array("3_translations_tours","Tours",7),
	array("3_translations_itinerary","Routes",7),
	array("3_translations_static","Words/Phrases",7),

	array("3_logs","View Logs",8),
	array("3_backup","Backup",8)
	);
}


function tohide($i){
	if($i == '1' || strtolower($i) == 'y'){
		return ' color:#666666; font-style:italic;';
		} else {
		return '';
		}
	}

function bbinfo($lang,$field,$id){
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_URL,"http://www.bundubashers.com/getinfo.php?id=".$id."&lang=".$lang."&field=".$field);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	$info = curl_exec($ch);
	//echo curl_error($ch);
	//@curl_exec($ch);
	@curl_close($ch);
	return $info;
}


function routenav(){
	global $_REQUEST;
	$code = '<TABLE BORDER="0" WIDTH="93%" CELLSPACING="2" CELLPADDING="4" BGCOLOR="#bbd8f1"><TR><TD ALIGN="center">'; //b3d5ff
	$code .= '<SELECT onChange="javascript:window.location='."this.value".'">';
		$code .= '<OPTION VALUE="">Choose an aspect of this route to edit...</OPTION>';
		$code .= '<OPTION VALUE="3_routes.php?edit='.$_REQUEST['edit'].'">Edit main route info/options</OPTION>';
		$code .= '<OPTION VALUE="3_routes_itinerary.php?edit='.$_REQUEST['edit'].'">Edit itinerary</OPTION>';
		$code .= '</SELECT>'."\n\n";
	$code .= '</TD></TR></TABLE><HR SIZE="1" WIDTH="93%"><BR>'."\n\n";
	return $code;
}


//SORT LISTS
$sortlist = array();
	array_push($sortlist,array("sort"=>"confirmation_num","name"=>"Confirmation#"));
	array_push($sortlist,array("sort"=>"name","name"=>"Name"));
	array_push($sortlist,array("sort"=>"date_booked","name"=>"Date Booked"));
$sortb = array("asc","desc");


//ONLOAD ARRAY
if(!isset($onload)){ $onload = array();	}

//echo '<PRE>'; print_r($_SESSION); echo '</PRE>';

//Find Page Title
$title = "";
foreach($menu as $tm){
	if($tm[0] == $pageid && isset($tm[3]) && $tm[3] == $_SERVER['QUERY_STRING']){
		$title = ' - ';
		if(isset($tm[2]) && $tm[2] != ""): $title .= $menucats['mc'.$tm[2]].': '; endif;
		$title .= $tm[1];
		break;
	} elseif($tm[0] == $pageid && !isset($tm[3])){
		$title = ' - ';
		if(isset($tm[2]) && $tm[2] != ""): $title .= $menucats['mc'.$tm[2]].': '; endif;
		$title .= $tm[1];
		break;
	}
}

//BUILD QUERY ARRAY
$hqueries = array();
foreach($menu as $row){
	if(isset($row[3]) && $row[3] != ""){ array_push($hqueries,$row[3]); }
	}

//E1E1E1

echo '<HTML>

<HEAD>
	<TITLE>'.$version.$title.'</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="stylesheet.css" rel="stylesheet" type="text/css">
	<!-- <script src="/jquery-1.6.2.min.js"></script> -->
	<style>
	    * {
            outline: 0!important;
        }
		body {
			font-family: Arial, Helvetica;
		}
	</style>
</HEAD>
<BODY BGCOLOR="#CCCCCC" TOPMARGIN="0" LEFTMARGIN="0" onLoad="'.implode('; ',$onload).'">';


if(isDev()) {
	$link_to_prod = $_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'];
	$link_to_prod = str_replace(dev_root, '', $link_to_prod);
	?> 
	<style>

		#staging_div {
			background-image: url('../../stagingv4/css/images/message_back.png');
			background-color: #FFE066;
			border: 1px solid #E6B800;
			border-top: none;
			padding: 10px;
			font-family: Helvetica, Arial;
			font-size: 15px;
			text-align: center;
			margin: 0px 15px 0px;
			box-shadow: 2px 2px 12px 0px #cccccc;
		}
	</style>

	<div id="staging_div" class="round4BL round4BR">
		You are working in a staging area.  Changes made here will not be reflected on the production site.<br>
		<a href="<?=$link_to_prod?>">Return to Production</a>
	</div>
	<?
}


echo '<CENTER><BR>

<TABLE BORDER="0" WIDTH="890" BGCOLOR="#FFFFFF" TEXT="#000000" LINK="#000099" VLINK="#000099" CELLPADDING="0" CELLSPACING="0">

<TR><TD WIDTH="140" HEIGHT="55" ALIGN="center" VALIGN="middle" BGCOLOR="#000066" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; background:#000066 url(\'img/leftbar.jpg\') top left repeat-y;">';
	if(isset($_SESSION['valid_user'])): echo '<B>'.date("l",$time).'<BR>'.date("M j, Y",$time).'</B><BR><SPAN STYLE="font-size:10px;">'.date("g:ia T",$time).'</SPAN>'; endif;
	echo '</TD><TD ALIGN="right" BGCOLOR="#666666" STYLE="background:#666666 url(\'img/topbar.jpg\') top center repeat-x;"><FONT FACE="Arial" SIZE="5"><B><A HREF="'.dirname($_SERVER["PHP_SELF"]).'" CLASS="headlink">Bundu Administration</A>&nbsp;</B></FONT></TD></TR>

<TR><TD HEIGHT="10" BGCOLOR="#666600"></TD><TD BGCOLOR="FFCC33"></TD></TR>

<TR><TD ALIGN="left" VALIGN="top" WIDTH="140" HEIGHT="500" BGCOLOR="#000066" STYLE="background:#000066 url(\'img/leftbar.jpg\') top left repeat-y; font-family:Arial; font-size:10pt; padding-top:10px; padding-left:10px; padding-right:8px;">'."\n\n";


$menucat = 0;
for($i=0; $i<count($menu); $i++){
	if(!isset($thisuser['allow'])) {
		break;
	}
	if($thisuser['allow'] != "*" && !in_array($menu[$i][0], $thisuser['allow'])) {
		continue;
	}

	if(isset($menu[$i][2]) && $menu[$i][2] == "n") {
		echo '<STRIKE>'.$menu[$i][1].'</STRIKE><BR><IMG SRC="spacer.gif"><BR>'."\n";
		continue;
	}

	if(isset($menu[$i][2]) && $menucat != $menu[$i][2]){
		if($i > 0): echo '</FONT><BR>'; endif;
		if($menu[$i][2] != ""): echo '<FONT FACE="Arial" SIZE="3" COLOR="#FFFFFF"><B><U>'.$menucats["mc".$menu[$i][2]].'</U></B></FONT><BR><IMG SRC="spacer.gif" HEIGHT="4"><BR>'."\n"; endif;
		echo '<FONT FACE="Arial" SIZE="2" COLOR="#FFFFFF">'."\n";
	}

	if($menu[$i][0] == ""){
		echo '<BR>'."\n";
	} else {
		if(strstr($menu[$i][0],".")){
			$thislink = $menu[$i][0];
			} else {
			$thislink = $menu[$i][0].'.php';
				if(isset($menu[$i][3])){ $thislink .= '?'.$menu[$i][3]; }
			}
		echo '- <A HREF="'.$thislink.'" CLASS="menulink';
		if(isset($pageid) && $pageid == $menu[$i][0]){
			if(!isset($menu[$i][3])){ $menu[$i][3] = ''; }
			$qstr = $_SERVER['QUERY_STRING'];
			if(!in_array($qstr,$hqueries)){ $qstr = ''; }
			if($menu[$i][3] == $qstr){ echo 'h'; }
			}
		echo '">'.$menu[$i][1].'</A><BR><IMG SRC="spacer.gif" HEIGHT="4"><BR>'."\n";
		if(isset($menu[$i][2])): $menucat = $menu[$i][2]; endif;
	}
} //END MENU FOR LOOP


if(isset($_SESSION['valid_user']) && $_SESSION['valid_user'] != "") {

	echo '<BR>- <A HREF="index.php?logout=y" CLASS="menulink">Log Out</A><BR><IMG SRC="spacer.gif" HEIGHT="4"><BR>'."\n";
	
	echo '<BR><BR>';
	
	echo '<script language="javascript"><!--'."\n\n";
	
	echo 'function getbackstat(){
		var newcode = \'\';
		var r = Math.floor(Math.random()*1001)
		newcode = \'<IFRAME SRC="getstatus.php" NAME="statframe" onLoad="loadstatus()" WIDTH="1" HEIGHT="1" FRAMEBORDER="0" MARGINHEIGHT="0" MARGINWIDTH="0" SCROLLING="no"></IFRAME>\';
		document.getElementById(\'bstatfeed\').innerHTML = newcode;
	}
	
	function loadstatus(){
		var status = window.frames[\'statframe\'].feedstatus();
		if(status == \'*online*\'){
			newcode = \'<SPAN STYLE="font-family:Arial; font-size:10pt; color:#339900;"><B>ONLINE</B></SPAN>\';
			} else {
			newcode = \'<SPAN STYLE="font-family:Arial; font-size:10pt; color:red;">ERROR</SPAN>\';
			}
		document.getElementById(\'statusdiv\').innerHTML = newcode;
		}
	
	//--></script>'."\n\n";
	
	//SHOW BACKUP STATUS
	/*echo '<FONT FACE="Arial" SIZE="1" COLOR="#999999"><B>Backup Server Status<BR>';
		echo '<DIV ID="statusdiv"><TABLE BORDER="0"><TR><TD ALIGN="right" VALIGN="middle"><IMG SRC="img/working.gif" BORDER="0"></TD><TD ALIGN="left" VALIGN="middle" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF;">Checking...</TD></TR></TABLE></DIV>'."\n\n";
		echo '<BR><BR></B></FONT>';*/
	
	
	//SHOW SESSION COOKIE
	echo '<FONT FACE="Arial" SIZE="1" COLOR="#999999"><B>';
		echo 'PHP Version: '.phpversion().'<br><br>';
		//echo $_COOKIE['UTASess'].'<BR>';
		if(isset($_COOKIE['UTASess']) && $_COOKIE['UTASess'] != "") {
			$chkcookie = explode('|',$_COOKIE['UTASess']);
			echo 'Session Cookie Set<BR>Set at: '.date("g:ia",($chkcookie[3]-21600)).'<BR>Expires: '.date("g:ia",$chkcookie[3]);
		} else {
			echo 'Session Cookie Error';
		}
	echo '<BR><BR></B></FONT>';

}

echo '</TD>

<TD ALIGN="left" VALIGN="top" WIDTH="750">'."\n\n";


if(isset($pageid) && trim($pageid) != "" && isset($_REQUEST['edit']) && trim($_REQUEST['edit']) != ""){
	$window = date("Y-m-d H:i:s",($time-1200)); //2010-01-17 21:02:34
	$query = 'SELECT `username`,`modified` FROM `users` WHERE `userid` != "'.$thisuser['userid'].'" AND (`languages` = "*" OR `languages` = "'.$thisuser['languages'].'")';
		$query .= ' AND `last_tool` = "'.trim($pageid).'" AND `last_edit` = "'.trim($_REQUEST['edit']).'" AND `modified` > "'.$window.'"';
		$query .= ' ORDER BY `modified` DESC LIMIT 1';
	//echo $query.'<BR>';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1) {
		$row = mysql_fetch_assoc($result);
		//echo '<PRE>'; print_r($row); echo '</PRE>';
		echo '<CENTER><TABLE CELLPADDING="3" CELLSPACING="0" STYLE="margin-top:20px; width:94%; border:1px solid #B00000;">'."\n";
			echo '<TR><TD ALIGN="center" STYLE="background-color:#FFFFFF; border-bottom:1px solid #B00000; font-family:Arial; font-size:11pt;">WARNING</TD></TR>'."\n";
			echo '<TR><TD ALIGN="center" STYLE="background-color:#F3D9D9; font-family:Arial; font-size:10pt;"><I>User "'.$row['username'].'" accessed this page at '.date("g:ia",strtotime($row['modified'])).', about '.floor((($time-strtotime($row['modified']))/60)).' minutes ago.<BR>To avoid any data loss, make sure '.$row['username'].' is not still working on this page.</I></TD></TR>'."\n";
			echo '</TABLE></CENTER>'."\n\n";
	}
}

?>