<?  // This custom utility created by Dominick Bernal - www.bernalwebservices.com

require_once '../common.inc.php';

error_reporting('E_ALL'); ini_set('display_errors','1');

if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	header("Location: https://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"]));
	exit;
	}

$time = time();

if(isset($_SESSION['valid_user'])): unset($_SESSION['valid_user']); endif;
session_start();

$loginmsg = "You are not logged in.<BR>Please enter your username and password below.";

if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == "y"){
	session_destroy();
	unset($_SESSION);
	unset($valid_user);
	setcookie('UTASess','',time(),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);
	unset($_COOKIE['UTASess']);
	$loginmsg = '<B>You have been successfully logged out.';
}

if(!isset($_SESSION['valid_user']) && isset($_POST['submit']) && $_POST['submit'] == "Log In"){

$loginpasscr = md5($_POST['loginpass']);

$query = 'select * from `users` where `username` = "'.$_POST['loginname'].'" and `password` = "'.$loginpasscr.'"';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);

if($num_results < 1){
$loginmsg = "Your username and/or password is incorrect.  Please try again.";
//@mysql_query('insert into `log`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$_POST['loginname'].'","index","User failed to log in.")');
} elseif($num_results == 1){

$row = mysql_fetch_array($result);
	$home = explode('|',$row['allow']); if($home[0] == "*"){ $home[0] = '3_schedule'; }
	$valid_user = array("id"=>$row['userid'], "user"=>$row['username'], "pass"=>$row['password'], "passcr"=>$loginpasscr, "home"=>$home[0]);
	$_SESSION['valid_user'] = $valid_user;

setcookie("UTASess",$row['userid'].'|'.$row['username'].'|'.$loginpasscr.'|'.($time+21600),(time()+21600),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);

if(isset($_POST['remuser']) && $_POST['remuser'] == "y"){
	setcookie('UTALogin',$row['username'],($time+31536000),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);
	} elseif(isset($_COOKIE['UTALogin'])) {
	setcookie("UTALogin",'',$time,dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);
	} //End Set Cookie

//mysql_query('insert into `log`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$valid_user['id'].'","index","User successfully logged in.")');

} // END SET VALID USER

} // END SUBMIT IF STATEMENT


//print_r($_COOKIE);

if(!isset($_SESSION['valid_user'])){

	$pageid = "index";
	
	$onload = 'document.getElementById(\'';
		if(isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): $onload .= 'loginpass'; else: $onload .= 'loginname'; endif;
		$onload .= '\').focus();';
		$onload = array($onload);
	
	require("header.php");
	
	echo "\n\n".'<CENTER>'."\n\n";
	
	echo '<IMG SRC="spacer.gif" HEIGHT="20" BORDER="0"><BR><FONT FACE="Arial" SIZE="2">'.$loginmsg."<BR><BR>\n\n";
	
	echo '<FORM name="loginform" action="'.$_SERVER['SCRIPT_NAME'].'" method="post">'."\n\n";
	
	echo '<TABLE BORDER="0">
	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3">Username: </FONT></TD><TD ALIGN="left"><INPUT TYPE="text" SIZE="25" STYLE="width:200px;" ID="loginname" NAME="loginname" VALUE="';
		if(isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): echo $_COOKIE['UTALogin']; endif;
		echo '"></TD></TR>
	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3">Password: </FONT></TD><TD ALIGN="left"><INPUT TYPE="password" SIZE="25" STYLE="width:200px;" ID="loginpass" NAME="loginpass"></TD></TR>
	<TR><TD COLSPAN="3" ALIGN="center"><FONT FACE="Arial" SIZE="2"><INPUT TYPE="checkbox" NAME="remuser" VALUE="y"';
		if(isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): echo ' CHECKED'; endif;
		echo ' ID="u"> <label for="u">Remember my username</label></TD></TR>
	<TR><TD COLSPAN="3" ALIGN="center"><INPUT TYPE="submit" NAME="submit" VALUE="Log In"></TD></TR>
	</TABLE><BR></FONT>';
	
	echo '</FORM>'."\n\n";
	
	echo '<div style="font-family:Helvetica; font-size:11px; color:grey; font-style:italic;">'.$_SERVER['SERVER_ADDR'].'</div>';
	
	require("footer.php");

} else {

	if($_SESSION['valid_user']['home'] == ""){ $_SESSION['valid_user']['home'] = "3_schedule"; }
	//echo dev_root.'/admin/'.$_SESSION['valid_user']['home'].'.php';
	header('Location: '.dev_root.'/admin/'.$_SESSION['valid_user']['home'].'.php');

}

?>
