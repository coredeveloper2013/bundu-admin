<?php

require_once '../common.inc.php';
error_reporting('E_ALL'); ini_set('display_errors','1');
if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
    header("Location: https://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"]));
    exit;
}
$time = time();
session_start();
$loginmsg = "";
if (!isset($_SESSION['valid_user']) && isset($_POST['submit']) && $_POST['submit'] == "Log In") {

    $loginpasscr = md5($_POST['loginpass']);

    $query = 'select * from `users` where `username` = "' . $_POST['loginname'] . '" and `password` = "' . $loginpasscr . '"';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);

    if ($num_results < 1) {
        $loginmsg = "Your username and/or password is incorrect.";
//@mysql_query('insert into `log`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$_POST['loginname'].'","index","User failed to log in.")');
    }
    elseif ($num_results == 1) {

        $row = mysql_fetch_array($result);
        $home = explode('|', $row['allow']);
        if ($home[0] == "*") {
            $home[0] = '3_schedule';
        }
        $valid_user = array("id" => $row['userid'], "user" => $row['username'], "pass" => $row['password'], "passcr" => $loginpasscr, "home" => $home[0]);
        $_SESSION['valid_user'] = $valid_user;

        setcookie("UTASess", $row['userid'] . '|' . $row['username'] . '|' . $loginpasscr . '|' . ($time + 21600), (time() + 21600), dirname($_SERVER["PHP_SELF"]), $_SERVER["HTTP_HOST"], TRUE);

        if (isset($_POST['remuser']) && $_POST['remuser'] == "y") {
            setcookie('UTALogin', $row['username'], ($time + 31536000), dirname($_SERVER["PHP_SELF"]), $_SERVER["HTTP_HOST"], TRUE);
        } elseif (isset($_COOKIE['UTALogin'])) {
            setcookie("UTALogin", '', $time, dirname($_SERVER["PHP_SELF"]), $_SERVER["HTTP_HOST"], TRUE);
        } //End Set Cookie

//mysql_query('insert into `log`(`time`,`user`,`page`,`log`) values("'.$time.'","'.$valid_user['id'].'","index","User successfully logged in.")');

    } // END SET VALID USER

}
$logOutmsg = '';
if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == "y"){
    session_destroy();
    unset($_SESSION);
    unset($valid_user);
    setcookie('UTASess','',time(),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"],TRUE);
    unset($_COOKIE['UTASess']);
    $logOutmsg = '<B>You have been successfully logged out.';
}
else {
    if(isset($_SESSION['valid_user'])){
        if ($_SESSION['valid_user']['home'] == "") {
            $_SESSION['valid_user']['home'] = "3_schedule";
        }
        //echo dev_root.'/admin/'.$_SESSION['valid_user']['home'].'.php';
        header('Location: ' . dev_root . '/admin/' . $_SESSION['valid_user']['home'] . '.php');
    };
}

$pageid = "index";
$onload = 'document.getElementById(\'';
if (isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): $onload .= 'loginpass';
else: $onload .= 'loginname'; endif;
$onload .= '\').focus();';
$onload = array($onload);






?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bundu Administration</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style>
        /* BASIC */
        .error_message{
            background-color: #ff000026;
            margin: auto;
            display: table;
            width: 380px;
            border-radius: 4px;
            padding: 20px 10px;
            font-size: 14px;
            color: red;
            margin-bottom: 5px;
        }
        .info_message{
            background-color: rgba(28, 161, 236, 0.21);
            margin: auto;
            display: table;
            width: 380px;
            border-radius: 4px;
            padding: 20px 10px;
            font-size: 14px;
            color: #1ca1ec;
            margin-bottom: 5px;
        }
        html {
            background-color: #56baed;
        }

        body {
            font-family: "Poppins", sans-serif;
            height: 100vh;
        }

        a {
            color: #92badd;
            display: inline-block;
            text-decoration: none;
            font-weight: 400;
        }

        h2 {
            text-align: center;
            font-size: 16px;
            font-weight: 600;
            text-transform: uppercase;
            display: inline-block;
            margin: 40px 8px 10px 8px;
            color: #cccccc;
        }


        /* STRUCTURE */

        .wrapper {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            width: 100%;
            min-height: 100%;
            padding: 20px;
        }

        #formContent {
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
            background: #fff;
            padding: 30px;
            width: 90%;
            max-width: 450px;
            position: relative;
            padding: 0px;
            -webkit-box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 60px 0 rgba(0, 0, 0, 0.3);
            text-align: center;
        }

        #formFooter {
            background-color: #f6f6f6;
            border-top: 1px solid #dce8f1;
            padding: 25px;
            text-align: center;
            -webkit-border-radius: 0 0 10px 10px;
            border-radius: 0 0 10px 10px;
        }


        /* TABS */

        h2.inactive {
            color: #cccccc;
        }

        h2.active {
            color: #0d0d0d;
            border-bottom: 2px solid #5fbae9;
        }


        /* FORM TYPOGRAPHY*/

        input[type=button], input[type=submit], input[type=reset] {
            background-color: #56baed;
            border: none;
            color: white;
            padding: 15px 80px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            text-transform: uppercase;
            font-size: 13px;
            -webkit-box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
            box-shadow: 0 10px 30px 0 rgba(95, 186, 233, 0.4);
            -webkit-border-radius: 5px 5px 5px 5px;
            border-radius: 5px 5px 5px 5px;
            margin: 5px 20px 40px 20px;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover {
            background-color: #39ace7;
        }

        input[type=button]:active, input[type=submit]:active, input[type=reset]:active {
            -moz-transform: scale(0.95);
            -webkit-transform: scale(0.95);
            -o-transform: scale(0.95);
            -ms-transform: scale(0.95);
            transform: scale(0.95);
        }

        input[type=text] ,input[type=password] {
            background-color: #f6f6f6;
            border: none;
            color: #0d0d0d;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 5px;
            width: 85%;
            border: 2px solid #f6f6f6;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
            -webkit-border-radius: 5px 5px 5px 5px;
            border-radius: 5px 5px 5px 5px;
        }

        input[type=text]:focus ,input[type=password]:focus {
            background-color: #fff;
            border-bottom: 2px solid #5fbae9;
        }

        input[type=text]::placeholder {
            color: #cccccc;
        }
        input[type=password]::placeholder {
            color: #cccccc;
        }


        /* ANIMATIONS */

        /* Simple CSS3 Fade-in-down Animation */
        .fadeInDown {
            -webkit-animation-name: fadeInDown;
            animation-name: fadeInDown;
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
        }

        @-webkit-keyframes fadeInDown {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, -100%, 0);
                transform: translate3d(0, -100%, 0);
            }
            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }
        }

        @keyframes fadeInDown {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, -100%, 0);
                transform: translate3d(0, -100%, 0);
            }
            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }
        }

        /* Simple CSS3 Fade-in Animation */
        @-webkit-keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        @-moz-keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        @keyframes fadeIn {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        .fadeIn {
            opacity: 0;
            -webkit-animation: fadeIn ease-in 1;
            -moz-animation: fadeIn ease-in 1;
            animation: fadeIn ease-in 1;

            -webkit-animation-fill-mode: forwards;
            -moz-animation-fill-mode: forwards;
            animation-fill-mode: forwards;

            -webkit-animation-duration: 1s;
            -moz-animation-duration: 1s;
            animation-duration: 1s;
        }

        .fadeIn.first {
            -webkit-animation-delay: 0.4s;
            -moz-animation-delay: 0.4s;
            animation-delay: 0.4s;
        }

        .fadeIn.second {
            -webkit-animation-delay: 0.6s;
            -moz-animation-delay: 0.6s;
            animation-delay: 0.6s;
        }

        .fadeIn.third {
            -webkit-animation-delay: 0.8s;
            -moz-animation-delay: 0.8s;
            animation-delay: 0.8s;
        }

        .fadeIn.fourth {
            -webkit-animation-delay: 1s;
            -moz-animation-delay: 1s;
            animation-delay: 1s;
        }

        /* Simple CSS3 Fade-in Animation */
        .underlineHover:after {
            display: block;
            left: 0;
            bottom: -10px;
            width: 0;
            height: 2px;
            background-color: #56baed;
            content: "";
            transition: width 0.2s;
        }

        .underlineHover:hover {
            color: #0d0d0d;
        }

        .underlineHover:hover:after {
            width: 100%;
        }

        h1 {
            color: #60a0ff;
        }

        /* OTHERS */

        *:focus {
            outline: none;
        }

        #icon {
            width: 30%;
        }

    </style>
</head>
<body>

<div class="wrapper <?php echo $loginmsg == '' ? 'fadeInDown' : '' ?>">
    <div id="formContent">
        <div class="fadeIn first">
<!--            			<img src="https://www.b-cube.in/wp-content/uploads/2014/05/aditya-300x177.jpg" id="icon" alt="User Icon" />-->
            <h1 style="margin-top: 30px;
                        margin-bottom: 30px;
                        font-family: 'Harlow Solid Italic';
                        font-weight: bold;
                        color: #0b5683;">Bundu <br>Administration</h1>
            <?php if($loginmsg != ''){ ?><p class="error_message"><?php echo $loginmsg ?></p> <?php } ?>
            <?php if($logOutmsg != ''){ ?><p class="info_message"><?php echo $logOutmsg ?></p> <?php } ?>
        </div>
        <!-- Login Form -->
        <form name="loginform" action="<?php echo $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <input type="text" id="loginname" class="fadeIn second" name="loginname" placeholder="Username"
                   value="<?php if (isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): echo $_COOKIE['UTALogin']; endif; ?>">
            <input type="password" id="loginpass" class="fadeIn third" name="loginpass" placeholder="Password" value=""><br><br>
            <input type="checkbox" name="remuser" id="u"
                   class="fadeIn third" <?php if (isset($_COOKIE['UTALogin']) && $_COOKIE['UTALogin'] != ""): echo ' CHECKED'; endif; ?>>
            <label class="fadeIn third" for="u">Remember my username</label><br>
            <input type="submit" name="submit" class="fadeIn fourth mt-4" style="cursor: pointer" value="Log In">
        </form>

    </div>
</div>

</body>
</html>



