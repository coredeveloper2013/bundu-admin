<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


require_once("validate.php");
require_once("../getamount.php");

$command = @$_REQUEST['command'];


if($command == "calc_all"){
	$out = array(
		'total' => 0
		);

	$fields = array('type','date_month','date_day','date_year','tourid','routeid','activityid','bbticket','miniroute','market','trans_type','adults','seniors','children','lodgeid','nights');

	foreach(@$_REQUEST['assoc_id'] as $key => $val){
		$amount = @$_REQUEST['assoc_amount'][$key];

		//Include fuel surcharge for tours
		if(@$_REQUEST['assoc_type'][$key] == "t"){
			$tourObj = new tour(@$_REQUEST['assoc_tourid'][$key]);
			$numguests = (@$_REQUEST['assoc_adults'][$key] + @$_REQUEST['assoc_seniors'][$key] + @$_REQUEST['assoc_children'][$key]);
			if($tourObj->id > 0){
				$amount += ($numguests * $tourObj->getFuelSurcharge());
			}
			unset($tourObj);
		}

		/*
		$row = array();
		foreach($fields as $fieldname){
			$row[$fieldname] = @$_REQUEST['assoc_'.$fieldname][$key];
		}
		$amount = getamount($row);
		*/
		$amount = number_format($amount, 2, '.', '');
		$out[$key] = $amount;

		if(@$_REQUEST['assoc_canceled'][$key] == "0"){
			$out['total'] += $amount;
		}
	}
	
	$out['total'] = number_format($out['total'], 2, '.', '');
	
	echo json_encode($out);
}


?>