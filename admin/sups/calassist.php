<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


if(isset($_REQUEST['get']) && $_REQUEST['get'] == "avail" && isset($_REQUEST['type']) && $_REQUEST['type'] != ""){

	$pageid = "3_reservations";
	$path_to_common = '../../';
	require_once("../validate.php");

	$query = '';

	$dp_avail = array();
	//Get running dates
	if($_REQUEST['type'] == "r" && $_REQUEST['routeid'] > 0) {
		$query = 'SELECT `date` FROM `routes_dates` WHERE `routeid` = "'.$_REQUEST['routeid'].'" AND `date` > 0 AND `routeid` > 0 ORDER BY `date` ASC';
	} elseif($_REQUEST['type'] == "t" && $_REQUEST['tourid'] > 1) {
		$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$_REQUEST['tourid'].'" AND `date` > 0 ORDER BY `date` ASC';
	}
	$result = mysqlQuery($query);
	while($d = @mysql_fetch_assoc($result)) {
		if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
		if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
		$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = 0;
	}
	//Get blocked seats
	if($_REQUEST['type'] == "r" && $_REQUEST['routeid'] > 0){
		include_once doc_root.'/getblocked.php';
			$blocked = getblocked($_REQUEST['routeid'],'*','*');
			//echo '</CENTER><PRE>'; print_r($blocked); echo '</PRE>';
			if(isset($blocked['r'.$_REQUEST['routeid']])){
			foreach($blocked['r'.$_REQUEST['routeid']] as $key => $d){
				if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
				if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
				$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = ($d['tour_seats']+$d['route_seats']);
				}}
		} elseif($_REQUEST['type'] == "t" && $_REQUEST['tourid'] > 1){
			include_once doc_root.'/getblocked.php';
			$blocked = getblocked_tours($_REQUEST['tourid'],'*','*');
			//echo '</CENTER><PRE>'; print_r($blocked); echo '</PRE>';
			if(isset($blocked['t'.$_REQUEST['tourid']])){
			foreach($blocked['t'.$_REQUEST['tourid']] as $key => $d){
				if(!isset($dp_avail[date("Y",$d['date'])])){ $dp_avail[date("Y",$d['date'])] = array(); }
				if(!isset($dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)])){ $dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)] = array(); }
				$dp_avail[date("Y",$d['date'])][(date("n",$d['date'])-1)][date("j",$d['date'])] = $d['blocked'];
				}}
		}
	if(count($dp_avail) > 0){
	echo '||BEGIN||'."\n";

		foreach($dp_avail as $y => $years){
			foreach($years as $m => $months){
				foreach($months as $d => $day){
					echo $y.'|'.$m.'|'.$d.'|'.$day."\n";
					}
				}
			}

		echo '||END||'."\n";
		}

} //End if statement


if(@$_REQUEST['get'] == "route_booked" && @$_REQUEST['id_route'] != "" && @$_REQUEST['date'] != "") {
	$pageid = "3_routes";
	$path_to_common = '../../';
	require_once '../validate.php';

	include_once doc_root.'/getblocked.php';
	$blocked = getblocked($_REQUEST['id_route'], strtotime($_REQUEST['date']), strtotime($_REQUEST['date']));

	//echo $_REQUEST['id_route'].'<br>';
	//echo date('m/j/Y g:ia', strtotime($_REQUEST['date']) );
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blocked,true)).'</PRE>';
	
	$out = @$blocked['r'.$_REQUEST['id_route']]['d'.strtotime($_REQUEST['date'])];
	$out['date_display'] = date('n/j/Y', strtotime($_REQUEST['date']));

	echo json_encode($out);
}


if(@$_REQUEST['get'] == "tour_booked" && @$_REQUEST['id_tour'] != "" && @$_REQUEST['date'] != "") {
	$pageid = "3_tours";
	$path_to_common = '../../';
	require_once '../validate.php';

	include_once doc_root.'/getblocked.php';
	$blocked = getblocked_tours($_REQUEST['id_tour'], strtotime($_REQUEST['date']), strtotime($_REQUEST['date']));

	//echo $_REQUEST['id_route'].'<br>';
	//echo date('m/j/Y g:ia', strtotime($_REQUEST['date']) );
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blocked,true)).'</PRE>';

	$out = @$blocked['t'.$_REQUEST['id_tour']]['d'.strtotime($_REQUEST['date'])];
	$out['date_display'] = date('n/j/Y', strtotime($_REQUEST['date']));

	echo json_encode($out);
}


?>