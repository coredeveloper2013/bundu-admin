<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

$pageid = '3_tours';
$path_to_common = '../../';
require_once("../validate.php");


//GET TOURS
$tours = array();
$query = 'SELECT tours.*, COUNT(tours_dates.`tourid`) AS `upcoming`, vendors.`name` AS `vendor_name` FROM (`tours` LEFT JOIN `tours_dates` ON tours.`id` = tours_dates.`tourid` AND tours_dates.`date` > '.$time.') LEFT JOIN `vendors` ON tours.`vendor` = vendors.`id` WHERE 1 = 1';
	if($_SESSION['tours']['showhidden'] != "1"): $query .= ' AND tours.`hidden` = "0"'; endif;
	if(isset($_SESSION['tours']['filter']) && $_SESSION['tours']['filter'] == "available"){
		$query .= ' AND tours.`archived` = "0"';
		} elseif(isset($_SESSION['tours']['filter']) && $_SESSION['tours']['filter'] == "archived"){
		$query .= ' AND tours.`archived` > 0';
		}
	$query .= ' GROUP BY tours.`id`';
	$query .= ' ORDER BY '.$_SESSION['tours']['sortby'].' '.$_SESSION['tours']['sortdir'].', tours.`title` ASC';
	//$query .= ' LIMIT '.(($_SESSION['tours']['p']-1)*$_SESSION['tours']['limit']).','.$_SESSION['tours']['limit'];
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tours,$row);
	}
	//echo '<PRE>'; print_r($tours); echo '</PRE>';
mysql_close($db);

//echo '<PRE>';
$csvout = '"Tour","Title","Per Guest","URL"'."\r\n";
foreach($tours as $row){
	foreach($row as $key => $val){
		$row[$key] = str_replace("\r",'',$row[$key]);
		//$row[$key] = str_replace("\n",'\n',$row[$key]);
		$row[$key] = str_replace('"','""',$row[$key]);
		}
	$csvout .= '"'.$row['id'].'","'.$row['title'].'","'.$row['perguest'].'","http://www.bundubashers.com/tour.php?id='.urlencode($row['alias']).'"'."\r\n";
	}
//echo $csvout;
//echo '</PRE>';
//echo mb_strlen($csvout,'latin1');

header('Content-Disposition: attachment; filename="bundubashers_tours.csv"');
header('Content-Length: '.mb_strlen($csvout,'latin1'));
header('Content-Type: application/octet-stream;');
header('Cache-Control: no-cache, must-revalidate');
header('Cache-Control: private');
header('Pragma: private');
echo $csvout;
exit;

?>