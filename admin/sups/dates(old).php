<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


function dates_sort($dates){
	if(!is_array($dates) && trim($dates) == ""){ $dates = array(); }
	if(!is_array($dates)){
		$dates = str_replace('|',',',$dates);
		$dates = explode(',',$dates);
		}
		sort($dates,SORT_NUMERIC);
	return $dates;
	}


function route_usedby($routeid=0,$start='*',$end='*',$exclude=array(),$showdebug=false){
	$usedby = array();
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){ $start = dates_sort($start); }
	if(!is_array($exclude)){ $exclude = dates_sort($exclude); }
	$debug = '';

	if($showdebug){ $debug .= '<B>route_usedby() called</B>'."\n"; }

	if($routeid > 0){
		$query = 'SELECT tours_dates.`date` AS `tour_date`, routes_dates.`date` AS `route_date`, tours_assoc.`typeid` AS `routeid`, tours_assoc.`tourid`, tours_assoc.`dir`, tours_assoc.`day`';
			$query .= ' FROM `tours_dates`,`routes_dates`,`tours_assoc`';
			$query .= ' WHERE routes_dates.`routeid` = "'.$routeid.'" AND routes_dates.`routeid` = tours_assoc.`typeid` AND tours_assoc.`type` = "r"';
			$query .= ' AND tours_dates.`tourid` = tours_assoc.`tourid` AND tours_dates.`dir` = tours_assoc.`dir`';
			if(count($exclude) > 0){ $query .= ' AND (tours_dates.`tourid` != "'.implode('" AND tours_dates.`tourid` != "',$exclude).'")'; }
			$query .= ' AND tours_dates.`date` >= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) - 3600)';
			$query .= ' AND tours_dates.`date` <= (routes_dates.`date` - ((tours_assoc.`day`-1)*86400) + 3600)';
			if(is_array($start) && count($start) > 0){
				$query .= ' AND (routes_dates.`date` = "'.implode('" OR routes_dates.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND routes_dates.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND routes_dates.`date` <= '.$end; }
				}
			$query .= ' ORDER BY NULL';
			//echo $query.'<BR><BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			if(!isset($usedby['d'.$row['route_date']])){ $usedby['d'.$row['route_date']] = array(); }
			array_push($usedby['d'.$row['route_date']],$row);
			if($showdebug){ $debug .= 'Route used by tour '.$row['tourid'].' ('.date("n/j/Y",$row['tour_date']).') on '.date("n/j/Y",$row['route_date'])."\n"; }
			}
			//echo '<PRE>'; print_r($usedby); echo '</PRE>';
			if($showdebug){ $debug .= 'Found '.$num_results.' tour associations on '.count($usedby).' dates for route '.$routeid.".\n"; }
		} else {
		if($showdebug){ $debug .= 'No route id: exit.'."\n"; }
		} //End routeid if

	if($showdebug){ echo '<PRE>'.$debug."\n".'</PRE>'; }
	return $usedby;
	}


function add_route_dates($routeid=0,$dates=array(),$showdebug=false){
	$result = array('success'=>array(),'error'=>array(),'affected'=>0);
	$dates = dates_sort($dates);
	$debug = '';
		
	if($showdebug){ $debug .= '<B>add_route_dates() called</B>'."\n"; }

	if($routeid > 0 && count($dates) > 0){
		foreach($dates as $d){
			$query = 'INSERT INTO `routes_dates`(`routeid`,`date`) VALUES("'.$routeid.'","'.$d.'") ON DUPLICATE KEY UPDATE `date` = "'.$d.'"';
			@mysql_query($query);
			$thiserror = mysql_error();
			$thisaffected = mysql_affected_rows();
			if($thiserror == ""){
				if($showdebug){ $debug .= '<SPAN STYLE="color:green;">'.$query.' ['.$thisaffected.']</SPAN>'."\n"; }
				if($thisaffected > 0){ $result['affected']++; }
				} elseif($thiserror != ""){
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$query.'</SPAN><BR>'."\t".$thiserror."\n"; }
				}
			} //End Foreach

		if($result['affected'] > 0){ array_push($result['success'],$result['affected'].' dates added to route '.$routeid.'.'); }
		} else {
		if($showdebug){ $debug .= 'No route id and/or dates: exit.'."\n"; }
		} //End routeid if

	if($showdebug){ echo '<PRE>'.$debug.implode("\n",$result['success'])."\n".'</PRE>'; }
	return $result;
	}


function rmv_route_dates($routeid=0,$dates=array(),$showdebug=false){
	$result = array('success'=>array(),'error'=>array(),'affected'=>0);
	$dates = dates_sort($dates);
	$debug = '';

	if($showdebug){ $debug .= '<B>rmv_route_dates() called</B>'."\n"; }

	if($routeid > 0 && count($dates) > 0){
		include_once($_SERVER['DOCUMENT_ROOT'].'/getblocked.php');
		$blocked = getblocked($routeid,$dates);
		if($showdebug){ $debug .= 'getblocked() called'."\n"; }
		//echo '<PRE STYLE="text-align:left;">getblocked output:'; print_r($blocked); echo '</PRE>';
		$usedby = route_usedby($routeid,$dates);
		if($showdebug){ $debug .= 'route_usedby() called'."\n"; }
		//echo '<PRE STYLE="text-align:left;">route_usedby output:'; print_r($usedby); echo '</PRE>';

		foreach($dates as $d){
			if(isset($blocked['r'.$routeid]['d'.$d]) && ($blocked['r'.$routeid]['d'.$d]['tour_seats'] + $blocked['r'.$routeid]['d'.$d]['route_seats']) > 0){
				$thiserror = 'Cannot remove date '.date("n/j/Y",$d).' from route '.$routeid.': '.($blocked['r'.$routeid]['d'.$d]['tour_seats'] + $blocked['r'.$routeid]['d'.$d]['route_seats']).' seats booked.';
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$thiserror.'</SPAN>'."\n"; }

				} elseif(isset($usedby['d'.$d]) && count($usedby['d'.$d]) > 0){
				$thiserror = 'Cannot remove date '.date("n/j/Y",$d).' from route '.$routeid.': utilized by '.count($usedby['d'.$d]).' tours.';
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$thiserror.'</SPAN>'."\n"; }
								
				} else {
				$query = 'DELETE FROM `routes_dates` WHERE `routeid` = "'.$routeid.'" AND `date` = "'.$d.'" LIMIT 1';
				@mysql_query($query);
				$thiserror = mysql_error();
				$thisaffected = mysql_affected_rows();
				if($thiserror == ""){
					if($showdebug){ $debug .= '<SPAN STYLE="color:green;">'.$query.' ['.$thisaffected.']</SPAN>'."\n"; }
					if($thisaffected > 0){ $result['affected']++; }
					} elseif($thiserror != ""){
					array_push($result['error'],$thiserror);
					if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$query.'</SPAN><BR>'."\t".$thiserror."\n"; }
					}
				}
			} //End Foreach

		if($result['affected'] > 0){ array_push($result['success'],$result['affected'].' dates removed from route '.$routeid.'.'); }
		} else {
		if($showdebug){ $debug .= 'No route id and/or dates: exit.'."\n"; }
		} //End routeid if

	if($showdebug){ echo '<PRE>'.$debug.implode("\n",$result['success'])."\n".'</PRE>'; }
	return $result;
	}


function tour_using($tourid=0,$start='*',$end='*',$showdebug=false){
	$using = array();
	if(is_array($start) || strpos($start,'|') !== false || strpos($start,',') !== false){ $start = dates_sort($start); }
	$debug = '';

	if($showdebug){ $debug .= '<B>tour_using() called</B>'."\n"; }

	if($tourid > 0){
		$query = 'SELECT tours_dates.`date` AS `tour_date`, routes_dates.`date` AS `route_date`, tours_assoc.`dir`, tours_assoc.`typeid` AS `routeid`, tours_assoc.`day`, tours_assoc.`order`';
			$query .= ' FROM `tours_dates`';
			$query .= ' JOIN `tours_assoc` ON tours_dates.`tourid` = tours_assoc.`tourid` AND tours_dates.`dir` = tours_assoc.`dir`';
			$query .= ' JOIN `routes_dates` ON routes_dates.`routeid` = tours_assoc.`typeid` AND routes_dates.`date` >= (tours_dates.`date` + ((tours_assoc.`day`-1)*86400) - 3600) AND routes_dates.`date` <= (tours_dates.`date` + ((tours_assoc.`day`-1)*86400) + 3600)';
			$query .= ' WHERE tours_dates.`tourid` = "'.$tourid.'" AND tours_assoc.`tourid` != 0 AND tours_assoc.`type` = "r"';
			if(is_array($start) && count($start) > 0){
				$query .= ' AND (tours_dates.`date` = "'.implode('" OR tours_dates.`date` = "',$start).'")';
				} else {
				if(is_numeric($start)){ $query .= ' AND tours_dates.`date` >= '.$start; }
				if(is_numeric($end)){ $query .= ' AND tours_dates.`date` <= '.$end; }
				}
			//$query .= ' ORDER BY NULL';
			$query .= ' ORDER BY routes_dates.`date` ASC';
			//echo $query.'<BR>';
		$result = mysql_query($query);
		//echo mysql_error(),'<BR>';
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			if(!isset($using['d'.$row['tour_date']])){ $using['d'.$row['tour_date']] = array(); }
			array_push($using['d'.$row['tour_date']],$row);
			}
			//echo '<PRE>'; print_r($using); echo '</PRE>';
			if($showdebug){ $debug .= 'Found '.$num_results.' route associations on '.count($using).' dates for tour '.$tourid.".\n"; }
		} else {
		if($showdebug){ $debug .= 'No tour id: exit.'."\n"; }
		} //End routeid if

	if($showdebug){ echo '<PRE>'.$debug."\n".'</PRE>'; }
	return $using;
	}


function propose_tour($tourid=0,$dir='f',$dates=array(),$report=false,$showdebug=false){
	$tours = array();
	$dates = dates_sort($dates);
	$ready = true;
	$debug = '';

	GLOBAL $propose_tour_tt; $propose_tour_tt++;

	if($showdebug){ $debug .= '<B>propose_tour() called</B>'."\n"; }
	if($tourid > 0 && count($dates) > 0){

	mysql_query('CREATE TEMPORARY TABLE `new_dates'.$propose_tour_tt.'`(`tour_date` INT NOT NULL, PRIMARY KEY(`tour_date`))');
	mysql_query('INSERT INTO `new_dates'.$propose_tour_tt.'`(`tour_date`) VALUES('.implode('),(',$dates).')');
	//echo mysql_error().'<BR>';

	if($report){
		$query = 'SELECT new_dates'.$propose_tour_tt.'.`tour_date`, routes_dates.`date` AS `route_date`, tours_assoc.`dir`, tours_assoc.`typeid` AS `routeid`, tours_assoc.`day`, tours_assoc.`order`';
			$query .= ' FROM `tours_assoc`';
			$query .= ' JOIN `new_dates'.$propose_tour_tt.'`';
			$query .= ' LEFT JOIN `routes_dates` ON routes_dates.`routeid` = tours_assoc.`typeid` AND routes_dates.`date` >= (new_dates'.$propose_tour_tt.'.`tour_date` + ((tours_assoc.`day`-1)*86400) - 3600) AND routes_dates.`date` <= (new_dates'.$propose_tour_tt.'.`tour_date` + ((tours_assoc.`day`-1)*86400) + 3600)';
			$query .= ' WHERE tours_assoc.`tourid` != 0 AND tours_assoc.`type` = "r" AND tours_assoc.`tourid` = "'.$tourid.'" AND tours_assoc.`dir` = "'.$dir.'"';
			$query .= ' ORDER BY new_dates'.$propose_tour_tt.'.`tour_date` ASC, tours_assoc.`dir` ASC, tours_assoc.`day` ASC, tours_assoc.`order` ASC';
			//echo $query.'<BR>';
		$result = mysql_query($query);
		//echo mysql_error().'<BR>';
		$num_results = mysql_num_rows($result);

		if($num_results == 0){
			if($showdebug){ $debug .= 'No routes for this tour/direction: exit.'."\n"; }
			$ready = false;
			}

		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			$row['proposed_route'] = mktime(0,0,0,date("n",$row['tour_date']),(date("j",$row['tour_date'])+($row['day']-1)),date("Y",$row['tour_date']));			
			if(!isset($tours['d'.$row['tour_date']])){ $tours['d'.$row['tour_date']] = array(); }
			array_push($tours['d'.$row['tour_date']],$row);
			}
			//echo '<PRE>'; print_r($tours); echo '</PRE>';

		foreach($tours as $date){
			if($showdebug){ $debug .= 'To run tour '.$tourid.' '.strtoupper($dir).' on '.date("n/j/Y",$date[0]['tour_date'])."...\n"; }
			foreach($date as $route){
				if($route['route_date'] != $route['proposed_route']){ $ready = false; }
				if($showdebug && $route['route_date'] != $route['proposed_route']){
					$debug .= 'Day '.$route['day'].': <SPAN STYLE="color:red;">Add date '.date("n/j/Y",$route['proposed_route']).' to route '.$route['routeid'].'.</SPAN>'."\n";
					} elseif($showdebug) {
					$debug .= 'Day '.$route['day'].': <SPAN STYLE="color:green;">Route '.$route['routeid'].' running on '.date("n/j/Y",$route['proposed_route']).'.</SPAN>'."\n";
					}
				}
			}
	} else {
		$query = 'SELECT new_dates'.$propose_tour_tt.'.`tour_date`, count(routes_dates.`date`) AS `running`, count(tours_assoc.`typeid`) AS `needed`';
			$query .= ' FROM `tours_assoc`';
			$query .= ' JOIN `new_dates'.$propose_tour_tt.'`';
			$query .= ' LEFT JOIN `routes_dates` ON routes_dates.`routeid` = tours_assoc.`typeid` AND routes_dates.`date` >= (new_dates'.$propose_tour_tt.'.`tour_date` + ((tours_assoc.`day`-1)*86400) - 3600) AND routes_dates.`date` <= (new_dates'.$propose_tour_tt.'.`tour_date` + ((tours_assoc.`day`-1)*86400) + 3600)';
			$query .= ' WHERE tours_assoc.`tourid` != 0 AND tours_assoc.`type` = "r" AND tours_assoc.`tourid` = "'.$tourid.'" AND tours_assoc.`dir` = "'.$dir.'"';
			$query .= ' GROUP BY new_dates'.$propose_tour_tt.'.`tour_date`';
			$query .= ' ORDER BY NULL';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			if($row['running'] != $row['needed']){
				$ready = false;
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.date("n/j/Y",$row['tour_date']).' not valid for tour '.$tourid.' running '.strtoupper($dir).', exit.</SPAN>'."\n"; }
				break;
				} elseif($showdebug){
				$debug .= '<SPAN STYLE="color:green;">'.date("n/j/Y",$row['tour_date']).' valid for tour '.$tourid.' running '.strtoupper($dir).'.</SPAN>'."\n";
				}
			}
	} //End report if statement

	} else {
	if($showdebug){ $debug .= 'No tour id and/or date: exit.'."\n"; }
	$ready = false;
	} //End routeid and date if

	if($showdebug && $ready){ $debug .= '<SPAN STYLE="color:green;">All tour dates valid!</SPAN>'."\n"; } elseif($showdebug) { $debug .= '<SPAN STYLE="color:red;">Not all tour dates are valid.</SPAN>'."\n"; }

	if($showdebug){ echo '<PRE>'.$debug."\n".'</PRE>'; }
	if($report){ return $tours; } else { return $ready; }
	}


function add_tour_dates($tourid=0,$dir='f',$dates=array(),$commit=false,$showdebug=false){
	$result = array('success'=>array(),'error'=>array(),'prereqs'=>array(),'affected'=>0,'routes_affected'=>0);
	$dates = dates_sort($dates);
	$debug = '';

	if($showdebug){ $debug .= '<B>add_tour_dates() called</B>'."\n"; }

	if($tourid > 0 && count($dates) > 0){
		$vdates = array();
		$tour_dates = propose_tour($tourid,$dir,$dates,1);
		//echo '<PRE>'; print_r($tour_dates); echo '</PRE>';
		foreach($tour_dates as $d){
			foreach($d as $route){
				if($commit && $route['route_date'] != $route['proposed_route']){
					$add = add_route_dates($route['routeid'],$route['proposed_route']);
					$result['success'] = array_merge($result['success'],$add['success']);
					$result['error'] = array_merge($result['error'],$add['error']);
					$result['routes_affected'] = ($result['routes_affected'] + $add['affected']);
					if(count($add['error']) > 0){
						if(!isset($result['prereqs']['d'.$route['tour_date']])){ $result['prereqs']['d'.$route['tour_date']] = array(); }
						array_push($result['prereqs']['d'.$route['tour_date']],$route);
						}
					} elseif($route['route_date'] != $route['proposed_route']){
					if(!isset($result['prereqs']['d'.$route['tour_date']])){ $result['prereqs']['d'.$route['tour_date']] = array(); }
					array_push($result['prereqs']['d'.$route['tour_date']],$route);
					}
				}
			if(!isset($result['prereqs']['d'.$d[0]['tour_date']]) || count($result['prereqs']['d'.$d[0]['tour_date']]) == 0){
				array_push($vdates,$d[0]['tour_date']);
				} elseif($showdebug){
				$debug .= '<SPAN STYLE="color:red;">Date '.date("n/j/Y",$d[0]['tour_date']).' needs route dates added.</SPAN>'."\n";
				}
			}
		//echo '<PRE>'; print_r($result['prereqs']); echo '</PRE>';

		foreach($vdates as $d){
			$query = 'INSERT INTO `tours_dates`(`tourid`,`date`,`dir`) VALUES("'.$tourid.'","'.$d.'","'.$dir.'") ON DUPLICATE KEY UPDATE `dir` = "'.$dir.'"';
			@mysql_query($query);
			$thiserror = mysql_error();
			$thisaffected = mysql_affected_rows();
			if($thiserror == ""){
				if($showdebug){ $debug .= '<SPAN STYLE="color:green;">'.$query.' ['.$thisaffected.']</SPAN>'."\n"; }
				if($thisaffected > 0){ $result['affected']++; }
				} elseif($thiserror != ""){
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$query.'</SPAN><BR>'."\t".$thiserror."\n"; }
				}
			} //End Foreach

		$tr = array('f'=>'forward','r'=>'in reverse');
		if($result['affected'] > 0){ array_push($result['success'],$result['affected'].' dates running '.$tr[$dir].' added to tour '.$tourid.'.'); }
		} else {
		if($showdebug){ $debug .= 'No tour id and/or dates: exit.'."\n"; }
		} //End tourid/dates if

	if($showdebug){ echo '<PRE>'.$debug.implode("\n",$result['success'])."\n".implode("\n",$result['error'])."\n".'</PRE>'; }
	return $result;
	}


function rmv_tour_dates($tourid=0,$dates=array(),$rmv_routes=false,$showdebug=false){
	$result = array('success'=>array(),'error'=>array(),'affected'=>0);
	$dates = dates_sort($dates);
	$debug = '';

	if($showdebug){ $debug .= '<B>rmv_tour_dates() called</B>'."\n"; }

	if($tourid > 0 && count($dates) > 0){

		include_once($_SERVER['DOCUMENT_ROOT'].'/getblocked.php');
		$blocked = getblocked_tours($tourid,$dates);
		//echo '<PRE>'; print_r($blocked); echo '</PRE>';

		foreach($dates as $d){
			if(isset($blocked['t'.$tourid]['d'.$d]) && $blocked['t'.$tourid]['d'.$d]['blocked'] > 0){
				$thiserror = 'Cannot remove date '.date("n/j/Y",$d).' from tour '.$tourid.': '.$blocked['t'.$tourid]['d'.$d]['blocked'].' seats booked.';
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$thiserror.'</SPAN>'."\n"; }
				} else {
				$query = 'DELETE FROM `tours_dates` WHERE `tourid` = "'.$tourid.'" AND `date` = "'.$d.'" LIMIT 1';
				@mysql_query($query);
				$thiserror = mysql_error();
				$thisaffected = mysql_affected_rows();
				if($thiserror == ""){
					if($showdebug){ $debug .= '<SPAN STYLE="color:green;">'.$query.' ['.$thisaffected.']</SPAN>'."\n"; }
					if($thisaffected > 0){ $result['affected']++; }
					} elseif($thiserror != ""){
					array_push($result['error'],$thiserror);
					if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$query.'</SPAN><BR>'."\t".$thiserror."\n"; }
					}
				}
			} //End Foreach

		/*WOULD BE NEEDED TO AUTOMATICALLY REMOVE ORPHANED ROUTE DATES AFTER DELETING TOUR DATES
		$rmv_tour_dates = 0;
		$rmv_route_dates = 0;
		$exclude = array($tourid);

		$using = tour_using($tourid,$dates);
		//echo '<PRE>'; print_r($using); echo '</PRE>';
		if($showdebug){ $debug .= 'Will examine '.count($using).' dates for tour '.$tourid."...\n"; }

		foreach($using as $d){
		if(isset($blocked['t'.$tourid]['d'.$d[0]['tour_date']]) && $blocked['t'.$tourid]['d'.$d[0]['tour_date']]['blocked'] > 0){

			$thiserror = "\n".'Cannot remove date '.date("n/j/Y",$d[0]['tour_date']).' from tour '.$tourid.': '.$blocked['t'.$tourid]['d'.$d[0]['tour_date']]['blocked'].' seats booked.';
			array_push($result['error'],$thiserror);
			if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$thiserror.'</SPAN>'."\n"; }

			} else {

			$query = 'DELETE FROM `tours_dates` WHERE `tourid` = "'.$tourid.'" AND `date` = "'.$d[0]['tour_date'].'" LIMIT 1';
			@mysql_query($query);
			$thiserror = mysql_error();
			$thisaffected = mysql_affected_rows();
			if($thiserror == ""){
				if($showdebug){ $debug .= '<SPAN STYLE="color:green;">'.$query.' ['.$thisaffected.']</SPAN>'."\n"; }
				if($thisaffected > 0){ $result['affected']++; $rmv_tour_dates++; }
				} elseif($thiserror != ""){
				array_push($result['error'],$thiserror);
				if($showdebug){ $debug .= '<SPAN STYLE="color:red;">'.$query.'</SPAN><BR>'."\t".$thiserror."\n"; }
				}

			if($showdebug){ $debug .= "\n".count($d).' routes on '.date("n/j/Y",$d[0]['tour_date']).':'."\n"; }
			foreach($d as $route){

				//REMOVE ROUTE DATE
				$rmv = rmv_route_dates($route['routeid'],$route['route_date']);
				//$result['success'] = array_merge($result['success'],$rmv['success']);
				//$result['error'] = array_merge($result['error'],$rmv['error']);
				$rmv_route_dates = ($rmv_route_dates+$rmv['affected']);
				if($showdebug && $rmv['affected'] > 0){ $debug .= '<SPAN STYLE="color:green;">Route '.$route['routeid'].' on '.date("n/j/Y",$d[0]['route_date']).' removed.</SPAN>'."\n"; }

				//THIS BLOCK FOR 2ND TIER REMOVAL
				$usedby = route_usedby($route['routeid'],$route['route_date'],$route['route_date'],$exclude);
				//echo '<PRE>'; print_r($usedby); echo '</PRE>';
				if(count($usedby) > 0){
					//ROUTE IS BEING USED BY OTHER TOURS
					if($showdebug){ $debug .= '<SPAN STYLE="color:red;">Route '.$route['routeid'].' on '.date("n/j/Y",$route['route_date']).' is being used by '.count($usedby['d'.$route['route_date']]).' other tours.</SPAN>'."\n"; }
						//THE FOLLOWING WOULD THEN REMOVE ASSOCIATED TOUR DATES AS WELL
						if($ntier){
							foreach($usedby['d'.$route['route_date']] as $u){
								//REMOVE TOUR DATE?
								$rmv_tour_dates++;
								array_push($exclude,$u['tourid']);
								$using2 = tour_using($u['tourid'],$u['tour_date'],$u['tour_date']);
								//echo '<PRE>'; print_r($using2); echo '</PRE>';
								if($showdebug){ $debug .= "\t".'Tour '.$u['tourid'].' on '.date("n/j/Y",$u['tour_date']).' ('.count($using2['d'.$u['tour_date']]).' routes)...'."\n"; }
								foreach($using2['d'.$u['tour_date']] as $route2){
									$usedby2 = route_usedby($route2['routeid'],$route2['route_date'],$route2['route_date'],$exclude);
									//echo '<PRE>'; print_r($usedby2); echo '</PRE>';
									if(count($usedby2) > 0){
										if($showdebug){ $debug .= "\t".'<SPAN STYLE="color:red;">Route '.$route2['routeid'].' on '.date("n/j/Y",$route2['route_date']).' is being used by '.count($usedby2['d'.$route2['route_date']]).' other tours:</SPAN>'; }
											foreach($usedby2['d'.$route2['route_date']] as $u2){
												if($showdebug){ $debug .= ' '.$u2['tourid']; }
												}
												if($showdebug){ $debug .= "\n"; }
										} else {
										//CAN REMOVE ROUTE DATE
										$rmv_route_dates++;
										if($showdebug){ $debug .= "\t".'<SPAN STYLE="color:green;">Route '.$route2['routeid'].' on '.date("n/j/Y",$route2['route_date']).' will be removed.</SPAN>'."\n"; }
										}
									}
								} //End $usedby['d'.$route['route_date']] foreach
							} //End Tier If
					} else {
					//would remove route date here
					} //End usedby if

				} //End $d foreach

			} //End blocked if statement
			} //End $using foreach*/

		} else {
		if($showdebug){ $debug .= 'No tour id and/or dates: exit.'."\n"; }
		} //End tourid if

	if($showdebug){ $debug .= '<B>'.$result['affected'].' tour dates removed.</B>'."\n"; }
	if($result['affected'] > 0){ array_push($result['success'],$result['affected'].' dates removed from tour '.$tourid.'.'); }

	if($showdebug){ echo '<PRE>'.$debug.implode("\n",$result['success'])."\n".implode("\n",$result['error'])."\n".'</PRE>'; }
	return $result;
	}


?>