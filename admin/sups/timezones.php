<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


function gettimediff($dep_loc,$arr_loc){
	$info = array();
	$query = 'SELECT locations.`id`, locations.`name`, locations.`timezone_id`, timezones.`name` AS `timezone`, timezones.`offset_std` FROM `locations`,`timezones` WHERE (locations.`id` = "'.$dep_loc.'" OR locations.`id` = "'.$arr_loc.'") AND locations.`timezone` = timezones.`id` LIMIT 2';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$info['i'.$row['id']] = $row;
		}

	$offset = 0;
	if($info['i'.$dep_loc]['timezone_id'] != "" && $info['i'.$arr_loc]['timezone_id'] != ""){
		$offset = get_timezone_offset($info['i'.$arr_loc]['timezone_id'],$info['i'.$dep_loc]['timezone_id']);
		} else {
		$offset = ($info['i'.$dep_loc]['offset_std'] - $info['i'.$arr_loc]['offset_std']);
		}

	$output = array(
		'dep_name' => $info['i'.$dep_loc]['name'],
		'dep_timezone' => $info['i'.$dep_loc]['timezone'],
		'dep_offset' => $info['i'.$dep_loc]['offset_std'],
		'dep_timezone_id' => $info['i'.$dep_loc]['timezone_id'],
		'arr_name' => $info['i'.$arr_loc]['name'],
		'arr_timezone' => $info['i'.$arr_loc]['timezone'],
		'arr_offset' => $info['i'.$arr_loc]['offset_std'],
		'arr_timezone_id' => $info['i'.$arr_loc]['timezone_id'],
		'offset' => $offset
		);

	return $output;
	}

function get_timezone_offset($remote_tz,$origin_tz=null,$datetime="now") {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime($datetime, $origin_dtz);
    $remote_dt = new DateTime($datetime, $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}

$loc_tzs = array();
function tz_offset($dep_loc=1,$arr_loc=1,$datetime="now"){
	global $loc_tzs;

	//Get timezone info if needed
	if(!isset($loc_tzs['l'.$dep_loc]) || !isset($loc_tzs['l'.$arr_loc])){
		$query = 'SELECT `id`,`timezone_id` FROM `locations` WHERE `id` = "'.$dep_loc.'" OR `id` = "'.$arr_loc.'"';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			$loc_tzs['l'.$row['id']] = $row['timezone_id'];
			}
		}
	if(!isset($loc_tzs['l'.$dep_loc])){	$loc_tzs['l'.$dep_loc] = date_default_timezone_get(); }
	if(!isset($loc_tzs['l'.$arr_loc])){ $loc_tzs['l'.$arr_loc] = date_default_timezone_get(); }

    $dep_tz = new DateTimeZone($loc_tzs['l'.$dep_loc]);
    $arr_tz = new DateTimeZone($loc_tzs['l'.$arr_loc]);

    $dep_dt = new DateTime($datetime,$dep_tz);
    $arr_dt = new DateTime($datetime,$arr_tz);

    $offset = ($arr_tz->getOffset($arr_dt) - $dep_tz->getOffset($dep_dt));
    return $offset;
	}



if(isset($_REQUEST['timeoffset_dep_loc']) && trim($_REQUEST['timeoffset_dep_loc']) != "" && isset($_REQUEST['timeoffset_arr_loc']) && trim($_REQUEST['timeoffset_arr_loc']) != ""){

$pageid = "3_routes";
require_once("validate.php");

$thisoffset = gettimediff($_REQUEST['timeoffset_dep_loc'],$_REQUEST['timeoffset_arr_loc']);

if(isset($_REQUEST['debug']) && trim($_REQUEST['debug']) == "y"){
	echo '<PRE>'; print_r($thisoffset); echo '</PRE>';
	}

echo '||BEGIN||'."\n";
	echo $thisoffset['offset']."\n";
	echo '||END||'."\n";

} elseif(isset($_REQUEST['tz_offset_dep_loc']) && trim($_REQUEST['tz_offset_dep_loc']) != "" && isset($_REQUEST['tz_offset_arr_loc']) && trim($_REQUEST['tz_offset_arr_loc']) != ""){

$pageid = "3_routes";
require_once("validate.php");

$st_dep_offset = tz_offset($_REQUEST['tz_offset_dep_loc'],$_REQUEST['tz_offset_arr_loc'],"2010-1-1");
$dst_dep_offset = tz_offset($_REQUEST['tz_offset_dep_loc'],$_REQUEST['tz_offset_arr_loc'],"2010-6-1");
$st_arr_offset = tz_offset($_REQUEST['tz_offset_arr_loc'],$_REQUEST['tz_offset_dep_loc'],"2010-1-1");
$dst_arr_offset = tz_offset($_REQUEST['tz_offset_arr_loc'],$_REQUEST['tz_offset_dep_loc'],"2010-6-1");

echo '||BEGIN||'."\n";
	echo $st_dep_offset.'|'.$dst_dep_offset.'|'.$st_arr_offset.'|'.$dst_arr_offset."\n";
	echo '||END||'."\n";

} elseif(isset($_REQUEST['get_timezone_offset_agency']) && trim($_REQUEST['get_timezone_offset_agency']) != "" && isset($_REQUEST['get_timezone_offset_dep']) && trim($_REQUEST['get_timezone_offset_dep']) != "" && isset($_REQUEST['get_timezone_offset_arr']) && trim($_REQUEST['get_timezone_offset_arr']) != ""){

$pageid = "3_routes";
require_once("validate.php");

$st_dep_offset = get_timezone_offset($_REQUEST['get_timezone_offset_agency'],$_REQUEST['get_timezone_offset_dep'],"2010-1-1");
$dst_dep_offset = get_timezone_offset($_REQUEST['get_timezone_offset_agency'],$_REQUEST['get_timezone_offset_dep'],"2010-6-1");
$st_arr_offset = get_timezone_offset($_REQUEST['get_timezone_offset_agency'],$_REQUEST['get_timezone_offset_arr'],"2010-1-1");
$dst_arr_offset = get_timezone_offset($_REQUEST['get_timezone_offset_agency'],$_REQUEST['get_timezone_offset_arr'],"2010-6-1");

echo '||BEGIN||'."\n";
	echo $st_dep_offset.'|'.$dst_dep_offset.'|'.$st_arr_offset.'|'.$dst_arr_offset."\n";
	echo '||END||'."\n";

} //End if statement


?>