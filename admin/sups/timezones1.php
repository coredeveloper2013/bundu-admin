<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com

@date_default_timezone_set("America/Denver");

function gettimediff($dep_loc,$arr_loc){
	$info = array();
	$query = 'SELECT locations.`id`, locations.`name`, locations.`timezone_id`, timezones.`name` AS `timezone`, timezones.`offset_std` FROM `locations`,`timezones` WHERE (locations.`id` = "'.$dep_loc.'" OR locations.`id` = "'.$arr_loc.'") AND locations.`timezone` = timezones.`id` LIMIT 2';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$info['i'.$row['id']] = $row;
		}

	$offset = 0;
	if($info['i'.$dep_loc]['timezone_id'] != "" && $info['i'.$arr_loc]['timezone_id'] != ""){
		$offset = get_timezone_offset( $info['i'.$dep_loc]['timezone_id'] , $info['i'.$arr_loc]['timezone_id'] );
		echo $info['i'.$dep_loc]['timezone_id'] .' '. $info['i'.$arr_loc]['timezone_id'];
		} else {
		$offset = ($info['i'.$dep_loc]['offset_std'] - $info['i'.$arr_loc]['offset_std']);
		}

	$output = array(
		'dep_name' => $info['i'.$dep_loc]['name'],
		'dep_timezone' => $info['i'.$dep_loc]['timezone'],
		'dep_offset' => $info['i'.$dep_loc]['offset_std'],
		'arr_name' => $info['i'.$arr_loc]['name'],
		'arr_timezone' => $info['i'.$arr_loc]['timezone'],
		'arr_offset' => $info['i'.$arr_loc]['offset_std'],
		'offset' => $offset
		);

	return $output;
	}

function get_timezone_offset($remote_tz, $origin_tz = null) {
    if($origin_tz === null) {
        if(!is_string($origin_tz = date_default_timezone_get())) {
            return false; // A UTC timestamp was returned -- bail out!
        }
    }
    $origin_dtz = new DateTimeZone($origin_tz);
    $remote_dtz = new DateTimeZone($remote_tz);
    $origin_dt = new DateTime("now", $origin_dtz);
    $remote_dt = new DateTime("now", $remote_dtz);
    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
    return $offset;
}



if(isset($_REQUEST['timeoffset_dep_loc']) && trim($_REQUEST['timeoffset_dep_loc']) != "" && isset($_REQUEST['timeoffset_arr_loc']) && trim($_REQUEST['timeoffset_arr_loc']) != ""){

$pageid = "3_routes";
require("../validate.php");

$thisoffset = gettimediff($_REQUEST['timeoffset_dep_loc'],$_REQUEST['timeoffset_arr_loc']);

if(isset($_REQUEST['debug']) && trim($_REQUEST['debug']) == "y"){
	echo '<PRE>'; print_r($thisoffset); echo '</PRE>';
	}

echo '||BEGIN||'."\n";
	echo $thisoffset['offset']."\n";
	echo '||END||'."\n";

} //End if statement

?>


