
Submit this form... works just fine:

<FORM method="post" action="test1.php">
	<textarea name="youtube[]">This is simple text.</textarea>
	<textarea name="youtube[]">Multiple text areas.</textarea>
	<input type="submit" value="submit">
</FORM>


<BR><BR><BR>


Submit this form... get an error:

<FORM method="post" action="test1.php">
	<textarea name="youtube[]" rows="30">In this form I will use a youtube embed code.</textarea>
	<textarea name="youtube[]" rows="30"><object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/u2XF29M_d-o?fs=1&hl=en_US&rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/u2XF29M_d-o?fs=1&hl=en_US&rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object></textarea>
	<input type="submit" value="submit">
</FORM>