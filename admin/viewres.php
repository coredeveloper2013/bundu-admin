<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$pageid = "3_reservations";
	require("validate.php");
	}

$query = 'SELECT reservations.*';
	$query .= ', (SELECT agents.`name` FROM `agents` WHERE agents.`id` = reservations.`agent` LIMIT 1) AS `agent`';
	$query .= ', (SELECT subagents.name FROM subagents WHERE subagents.id = reservations.id_subagent LIMIT 1) AS subagent';
	$query .= ' FROM `reservations` WHERE `id` = "'.$_REQUEST['view'].'" LIMIT 1';
$result = mysql_query($query);
$fillform = mysql_fetch_assoc($result);

//GET SUBRESERVATIONS
$reservations_assoc = array();
$query = 'SELECT reservations_assoc.*';
	$query .= ', IF(reservations_assoc.`tourid`=1,"Private tour",(SELECT CONCAT(tours.`id`,": ",tours.`title`) FROM `tours` WHERE tours.`id` = reservations_assoc.`tourid` LIMIT 1)) AS `tourid`';
	$query .= ', (SELECT tours_dates.`dir` FROM `tours_dates` WHERE tours_dates.`tourid` = reservations_assoc.`tourid` AND reservations_assoc.`date` = tours_dates.`date` LIMIT 1) as `dir`';
	$query .= ', (SELECT routes.`name` FROM `routes` WHERE routes.`id` = reservations_assoc.`routeid` LIMIT 1) AS `routeid`';
	$query .= ', (SELECT activities.`name` FROM `activities` WHERE activities.`id` = reservations_assoc.`activityid` LIMIT 1) AS `activityid`';
	$query .= ', (SELECT lodging.`name` FROM `lodging` WHERE lodging.`id` = reservations_assoc.`lodgeid` LIMIT 1) AS `lodgeid`';
	$query .= ', (SELECT lodging.`type` FROM `lodging` WHERE lodging.`id` = reservations_assoc.`lodgeid` LIMIT 1) AS `lodgetype`';
	$query .= ', (SELECT CONCAT(markets.`name`,": ",markets.`desc`) FROM `markets` WHERE markets.`id` = reservations_assoc.`market` LIMIT 1) AS `market`';
	$query .= ', (SELECT shuttle_transtypes.`name` FROM `shuttle_transtypes` WHERE shuttle_transtypes.`id` = reservations_assoc.`trans_type` LIMIT 1) AS `trans_type`';
	$query .= ', IFNULL((SELECT bundubus_hopon.`title` FROM `bundubus_hopon` WHERE bundubus_hopon.`id` = reservations_assoc.`bbticket` LIMIT 1),reservations_assoc.`bbticket`) AS `bbticket`';
	$query .= ', IFNULL((SELECT miniroutes.`title` FROM `miniroutes` WHERE miniroutes.`id` = reservations_assoc.`miniroute` LIMIT 1),reservations_assoc.`miniroute`) AS `miniroute`';
	$query .= ', (SELECT vendors.`name` FROM `vendors` WHERE vendors.`id` = reservations_assoc.`vendor` LIMIT 1) AS `vendor`';
	$query .= ' FROM `reservations_assoc` WHERE `reservation` = "'.getval('id').'" ORDER BY `date` ASC, `dep_time` ASC, `id` ASC';
	//echo '<!-- '.$query.' -->'."\n\n";
$result = mysql_query($query);
echo mysql_error();
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($reservations_assoc,$row);
	}

//GET GUESTS
$reservations_guests = array();
$query = 'SELECT reservations_guests.* FROM `reservations_guests`,`reservations_assoc` WHERE reservations_guests.`associd` = reservations_assoc.`id`  AND reservations_assoc.`reservation` = "'.getval('id').'" ORDER BY reservations_guests.`associd` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($reservations_guests['r'.$row['associd']])){ $reservations_guests['r'.$row['associd']] = array(); }
	array_push($reservations_guests['r'.$row['associd']],$row);
	}

//GET TOUR LODGING
$reservations_lodging = array();
$query = 'SELECT reservations_lodging.* FROM `reservations_lodging`,`reservations_assoc` WHERE reservations_lodging.`associd` = reservations_assoc.`id`  AND reservations_assoc.`reservation` = "'.getval('id').'" ORDER BY reservations_lodging.`associd` ASC, reservations_lodging.`room` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($reservations_lodging['r'.$row['associd']])){ $reservations_lodging['r'.$row['associd']] = array(); }
	array_push($reservations_lodging['r'.$row['associd']],$row);
	}

//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}

//CONVERSION ARRAYS
$conv_binary = array('No','Yes');
$conv_types = array(
	'a' => 'Activity/Tour option',
	'r' => 'Bundu route',
	't' => 'Bundu tour',
	'l' => 'Lodging',
	's' => 'Shuttle',
	'p' => 'Tour transport',
	'h' => 'Hop On, Hop Off Pass',
	'm' => 'Mini Route Pass',
	'o' => 'Other'
	);
$conv_tourdir = array('r'=>'Reverse','f'=>'Forward');

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '<HTML>'."\n\n";
	echo '<HEAD><TITLE>Reservation: '.$_REQUEST['view'].'</TITLE><LINK HREF="stylesheet.css" REL="stylesheet" TYPE="text/css"></HEAD>'."\n\n";
	echo '<BODY BGCOLOR="#FFFFFF" TOPMARGIN="0" LEFTMARGIN="0" onLoad="javascript:window.print();">'."\n\n";
	echo '<CENTER>'."\n\n";
	echo '<FONT FACE="Arial" SIZE="5"><U>Reservation: ';
		if(getval('canceled') > 0){ echo 'C'; }
		echo $_REQUEST['view'].'</U></FONT><BR><BR>'."\n\n";
	}

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" STYLE="empty-cells:show">'."\n";

//BASIC
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Basic / Contact Info</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Confirmation number</TD><TD CLASS="viewresr">';
		if(getval('canceled') > 0){ echo 'C'; }
		echo getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Name</TD><TD CLASS="viewresr">'.getval('name').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Home/business phone</TD><TD CLASS="viewresr">'.getval('phone_homebus').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Cell phone</TD><TD CLASS="viewresr">'.getval('phone_cell').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Cell country</TD><TD CLASS="viewresr">'.getval('cell_country').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Email</TD><TD CLASS="viewresr"><A HREF="mailto:'.getval('email').'">'.getval('email').'</A></TD></TR>'."\n";
	if(getval('email_inst') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Special instructions<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This will be included in the customer email confirmation.</SPAN></TD><TD CLASS="viewresr">'.nl2br(getval('email_inst')).'</TD></TR>'."\n"; }
	if(getval('email_conf') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Email confirmation was sent</TD><TD CLASS="viewresr">'.date("g:ia n/d/Y",getval('email_conf')).'</TD></TR>'."\n"; }
	echo "\n";

//ROUTES/ACTIVITIES/ETC.
foreach($reservations_assoc as $assoc => $row){
	if($row['lodgetype'] == "t"){
		echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
			echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #999999; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Subreservation '.($assoc+1);
			if($row['canceled'] > 0){ echo ' - <SPAN STYLE="color:#FF0000;"><I>Canceled</I></SPAN>'; }
			echo '<SPAN STYLE="color:#666666; font-weight:normal; font-size:90%;"><I> - Tour lodging (hidden) - '.date("l, F j, Y",$row['date']).' - '.$row['lodgeid'].'</I></SPAN>';
			echo '</TD></TR>'."\n\n";
		} elseif($row['type'] == "h") {
		bgcolor('reset');
		echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
			echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Subreservation '.($assoc+1);
			if($row['canceled'] > 0){ echo ' - <SPAN STYLE="color:#FF0000;"><I>Canceled</I></SPAN>'; }
			echo '</TD></TR>'."\n\n";
		if($row['type'] != "" && $row['type'] != "o"){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Type</TD><TD CLASS="viewresr">'.$conv_types[$row['type']].'</TD></TR>'."\n"; }
		if($row['reference'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Reference/Title</TD><TD CLASS="viewresr">'.$row['reference'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Name</TD><TD CLASS="viewresr">'.$row['name'].'</TD></TR>'."\n";
		if($row['notes'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Notes/Flight information</TD><TD CLASS="viewresr">'.nl2br($row['notes']).'</TD></TR>'."\n"; }
		if($row['bbticket'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Hop On Hop Off Type</TD><TD CLASS="viewresr">'.$row['bbticket'].'</TD></TR>'."\n"; }
		if($row['username'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">User name</TD><TD CLASS="viewresr">'.$row['username'].'</TD></TR>'."\n"; }
		if($row['days'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Days</TD><TD CLASS="viewresr">'.$row['days'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Subreservation amount/price</TD><TD CLASS="viewresr">$'.$row['amount'].'</TD></TR>'."\n";
		if($row['costs'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Internal costs</TD><TD CLASS="viewresr">'.$row['costs'].'</TD></TR>'."\n"; }
		if($row['canceled'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Canceled</TD><TD CLASS="viewresr">'.date("g:ia n/j/Y",$row['canceled']).'</TD></TR>'."\n"; }
		} elseif($row['type'] == "m") {
		bgcolor('reset');
		echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
			echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Subreservation '.($assoc+1);
			if($row['canceled'] > 0){ echo ' - <SPAN STYLE="color:#FF0000;"><I>Canceled</I></SPAN>'; }
			echo '</TD></TR>'."\n\n";
		if($row['type'] != "" && $row['type'] != "o"){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Type</TD><TD CLASS="viewresr">'.$conv_types[$row['type']].'</TD></TR>'."\n"; }
		if($row['reference'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Reference/Title</TD><TD CLASS="viewresr">'.$row['reference'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Name</TD><TD CLASS="viewresr">'.$row['name'].'</TD></TR>'."\n";
		if($row['notes'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Notes/Flight information</TD><TD CLASS="viewresr">'.nl2br($row['notes']).'</TD></TR>'."\n"; }
		if($row['miniroute'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Mini Route Pass</TD><TD CLASS="viewresr">'.$row['miniroute'].'</TD></TR>'."\n"; }
		if($row['username'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">User name</TD><TD CLASS="viewresr">'.$row['username'].'</TD></TR>'."\n"; }
		if($row['days'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Days</TD><TD CLASS="viewresr">'.$row['days'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Subreservation amount/price</TD><TD CLASS="viewresr">$'.$row['amount'].'</TD></TR>'."\n";
		if($row['costs'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Internal costs</TD><TD CLASS="viewresr">'.$row['costs'].'</TD></TR>'."\n"; }
		if($row['canceled'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Canceled</TD><TD CLASS="viewresr">'.date("g:ia n/j/Y",$row['canceled']).'</TD></TR>'."\n"; }
		} else {
		bgcolor('reset');
		echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
			echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Subreservation '.($assoc+1);
			if($row['canceled'] > 0){ echo ' - <SPAN STYLE="color:#FF0000;"><I>Canceled</I></SPAN>'; }
			echo '</TD></TR>'."\n\n";
		if($row['type'] != "" && $row['type'] != "o"){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Type</TD><TD CLASS="viewresr">'.$conv_types[$row['type']].'</TD></TR>'."\n"; }
		if($row['reference'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Reference/Title</TD><TD CLASS="viewresr">'.$row['reference'].'</TD></TR>'."\n"; }
		if($row['tourid'] != ""){
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Bundu tour</TD><TD CLASS="viewresr">'.$row['tourid'];
			if($row['dir'] != NULL){ echo '<BR><I>Tour direction: '.$conv_tourdir[$row['dir']].'</I>'; }
			echo '</TD></TR>'."\n";
			}
		if($row['routeid'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Bundu route</TD><TD CLASS="viewresr">'.$row['routeid'].'</TD></TR>'."\n"; }
		if($row['activityid'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Activity / Tour option</TD><TD CLASS="viewresr">'.$row['activityid'].'</TD></TR>'."\n"; }
		if($row['lodgeid'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Lodging unit</TD><TD CLASS="viewresr">'.$row['lodgeid'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Name</TD><TD CLASS="viewresr">'.$row['name'].'</TD></TR>'."\n";
		if($row['date'] > 0){
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Date</TD><TD CLASS="viewresr">';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){ echo '<A HREF="3_schedule.php?start='.$row['date'].'" STYLE="color:#000000; text-decoration:none;">'; }
			echo date("l, F j, Y",$row['date']);
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){ echo '</A>'; }
			echo '</TD></TR>'."\n";
			}
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Is this the <I>only</I> date guests have available?</TD><TD CLASS="viewresr">'.$conv_binary[$row['onlydateavl']].'</TD></TR>'."\n";
		if($row['altdates'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Alternate dates</TD><TD CLASS="viewresr">'.$row['altdates'].'</TD></TR>'."\n"; }
		if($row['dep_time'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Pick up time</TD><TD CLASS="viewresr">'.date("g:ia",$row['dep_time']).'</TD></TR>'."\n"; }
		if($row['preftime'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Preferred time of day</TD><TD CLASS="viewresr">'.$row['preftime'].'</TD></TR>'."\n"; }
		if($row['notes'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Notes/Flight information</TD><TD CLASS="viewresr">'.nl2br($row['notes']).'</TD></TR>'."\n"; }
		if($row['bbticket'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Hop On Hop Off Pass ID</TD><TD CLASS="viewresr">'.$row['bbticket'].'</TD></TR>'."\n"; }
		if($row['market'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Market</TD><TD CLASS="viewresr">'.$row['market'].'</TD></TR>'."\n"; }
		if($row['trans_type'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Transportation type</TD><TD CLASS="viewresr">'.$row['trans_type'].'</TD></TR>'."\n"; }
		if($row['nights'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Number of nights</TD><TD CLASS="viewresr">'.$row['nights'].'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Number of guests</TD><TD CLASS="viewresr">';
			echo '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0">';
				echo '<TR><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$row['adults'].'</TD><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$row['seniors'].'</TD><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$row['children'].'</TD><TD STYLE="padding-left:10px; font-family:Arial; font-size:10pt;">=</TD><TD CLASS="viewresr" STYLE="padding-left:10px; border-bottom:0px; width:auto;">'.($row['adults']+$row['seniors']+$row['children']).'</TD></TR>';
				echo '<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Adults</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Seniors</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Children</TD><TD></TD><TD STYLE="padding-left:10px; font-family:Arial; font-size:8pt; color:#666666;">Total</TD></TR>';
				echo '</TABLE>';
			echo '</TD></TR>'."\n";
		if(isset($reservations_guests['r'.$row['id']]) && count($reservations_guests['r'.$row['id']]) > 0){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Guest information</TD><TD CLASS="viewresr">';
			echo '<TABLE BORDER="0" CELLPADDING="0">';
			foreach($reservations_guests['r'.$row['id']] as $guest){
				echo "\t".'<TR><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$guest['name'].'</TD><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$guest['weight'].'</TD><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$guest['lunch'].'</TD></TR>'."\n";
				}
			echo "\t".'<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Name</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Weight</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Lunch</TD></TR>';
			echo '</TABLE>';
			echo '</TD></TR>'."\n";
			}
		if(isset($reservations_lodging['r'.$row['id']]) && count($reservations_lodging['r'.$row['id']]) > 0){
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Tour Lodging</TD><TD CLASS="viewresr">';
			echo '<TABLE BORDER="0" CELLPADDING="0">';
			foreach($reservations_lodging['r'.$row['id']] as $room){
				echo "\t".'<TR><TD CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">Room '.$room['room'].'</TD><TD ALIGN="right" CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$room['numguests'].'</TD><TD ALIGN="right" CLASS="viewresr" STYLE="border-bottom:0px; width:auto; padding-right:5px;">'.$room['adjust'].'</TD></TR>'."\n";
				}
			echo "\t".'<TR><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Room</TD><TD STYLE="padding-right:5px; font-family:Arial; font-size:8pt; color:#666666;">Guests</TD><TD STYLE="font-family:Arial; font-size:8pt; color:#666666;">Adjust</TD></TR>';
			echo '</TABLE>';
			echo '</TD></TR>'."\n";
			}
		//if($row['dep_time'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Pick up time</TD><TD CLASS="viewresr">'.date("g:ia",$row['dep_time']).'</TD></TR>'."\n"; }
		if($row['dep_loc'] != ""){
			echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Pick up location</TD><TD CLASS="viewresr">';
			if($row['routeid'] != "" && is_numeric($row['dep_loc'])){
				echo $locations['l'.$row['dep_loc']]['name'];
				} else {
				echo nl2br($row['dep_loc']);
				}
			echo '</TD></TR>'."\n";
			}
		//if($row['arr_time'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Drop off time</TD><TD CLASS="viewresr">'.date("g:ia",$row['arr_time']).'</TD></TR>'."\n"; }
		if($row['arr_loc'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Drop off location</TD><TD CLASS="viewresr">';
			if($row['routeid'] != "" && is_numeric($row['arr_loc'])){
				echo $locations['l'.$row['arr_loc']]['name'];
				} else {
				echo nl2br($row['arr_loc']);
				}
			echo '</TD></TR>'."\n"; }
		if($row['driver_info'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Driver information</TD><TD CLASS="viewresr">'.nl2br(wordwrap($row['driver_info'], 80, "\n", true)).'</TD></TR>'."\n"; }
		if($row['vendor'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Vendor</TD><TD CLASS="viewresr">'.$row['vendor'].'</TD></TR>'."\n"; }
		if($row['vendorconf'] != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Vendor conf #</TD><TD CLASS="viewresr">'.$row['vendorconf'].'</TD></TR>'."\n"; }
		if($row['vendorpaid'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Vendor commission paid</TD><TD CLASS="viewresr">$'.$row['vendorpayamt'].' on '.date("n/j/Y",$row['vendorpaid']).'</TD></TR>'."\n"; }
		echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Subreservation amount/price</TD><TD CLASS="viewresr">$'.$row['amount'].'</TD></TR>'."\n";
		if($row['costs'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Internal costs</TD><TD CLASS="viewresr">'.$row['costs'].'</TD></TR>'."\n"; }
		if($row['canceled'] > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Canceled</TD><TD CLASS="viewresr">'.date("g:ia n/j/Y",$row['canceled']).'</TD></TR>'."\n"; }
		echo "\n";
		}
	} //End Reservation Assoc For Each

//PAYMENTS
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Total / Payments</TD>';
		echo '</TR>'."\n\n";
	if(getval('amount') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Reservation total</TD><TD CLASS="viewresr">$'.getval('amount').'</TD></TR>'."\n"; }
	if(getval('cc_name') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Credit card name</TD><TD CLASS="viewresr">'.getval('cc_name').'</TD></TR>'."\n"; }
	if(getval('cc_num') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Credit card number</TD><TD CLASS="viewresr">';
		if(isset($thisuser['allow']) && is_array($thisuser['allow']) && in_array('3_reservations_cc',$thisuser['allow'])){
			echo getval('cc_num');
			} else {
			echo substr(getval('cc_num'),0,4).'**'.substr(getval('cc_num'),-2);
			}
		echo '</TD></TR>'."\n"; }
	if(getval('cc_expdate') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Expiration date</TD><TD CLASS="viewresr">'.getval('cc_expdate').'</TD></TR>'."\n"; }
	if(getval('cc_scode') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Security code</TD><TD CLASS="viewresr">'.getval('cc_scode').'</TD></TR>'."\n"; }
	if(getval('cc_zip') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Zip/Postal code</TD><TD CLASS="viewresr">'.getval('cc_zip').'</TD></TR>'."\n"; }
	if(getval('pay_method') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Method of payment</TD><TD CLASS="viewresr">'.getval('pay_method').'</TD></TR>'."\n"; }
	if(getval('alt_name') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Alternate name</TD><TD CLASS="viewresr">'.getval('alt_name').'</TD></TR>'."\n"; }
	if(getval('card_run') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Card was run</TD><TD CLASS="viewresr">'.date("g:ia n/j/Y",getval('card_run')).'</TD></TR>'."\n"; }
	if(getval('pnp_orderid') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">PlugnPay transaction ID</TD><TD CLASS="viewresr">'.getval('pnp_orderid').'</TD></TR>'."\n"; }
	if(getval('conf_details') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">PlugnPay confirmation details</TD><TD CLASS="viewresr" STYLE="font-weight:normal;">'.nl2br(getval('conf_details')).'</TD></TR>'."\n"; }
	echo "\n";

//NOTES/ETC.
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Notes / Etc.</TD>';
		echo '</TR>'."\n\n";
	if(getval('comments') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">User comments<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Entered online while making reservation.</SPAN></TD><TD CLASS="viewresr">'.nl2br(getval('comments')).'</TD></TR>'."\n"; }
	if(getval('http_referer') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Referred from</TD><TD CLASS="viewresr"><A HREF="'.getval('http_referer').'" TARGET="_blank">'.wordwrap(getval('http_referer'),40,'<BR>',1).'</A></TD></TR>'."\n"; }	
	if(getval('agent') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Agent</TD><TD CLASS="viewresr">'.getval('agent').'</TD></TR>'."\n"; }
	if(getval('subagent') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Subagent</TD><TD CLASS="viewresr">'.getval('subagent').'</TD></TR>'."\n"; }
	if(getval('agent_conf') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Agent confirmation</TD><TD CLASS="viewresr">'.getval('agent_conf').'</TD></TR>'."\n"; }
	if(getval('referral') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Referral</TD><TD CLASS="viewresr">'.getval('referral').'</TD></TR>'."\n"; }
	if(getval('booker') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Booker</TD><TD CLASS="viewresr">'.getval('booker').'</TD></TR>'."\n"; }
	if(getval('date_booked') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Date booked</TD><TD CLASS="viewresr">'.date("n/j/Y",getval('date_booked')).'</TD></TR>'."\n"; }
	if(getval('notes') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Notes</TD><TD CLASS="viewresr" STYLE="font-weight:normal;">'.nl2br(getval('notes')).'</TD></TR>'."\n"; }
	echo "\n";

	echo '</TABLE><BR>'."\n\n";

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '</CENTER>'."\n\n";
	echo '</BODY>'."\n\n";
	echo '</HTML>'."\n\n";
	}


?>