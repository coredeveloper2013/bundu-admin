<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$pageid = "3_schedule";
	require("validate.php");
	}

if(!isset($_REQUEST['start']) || trim($_REQUEST['start']) == ""){ $_REQUEST['start'] = $today; }
if(!isset($_REQUEST['end']) || trim($_REQUEST['end']) == ""){ $_REQUEST['end'] = $_REQUEST['start']; }
if($_REQUEST['end'] < $_REQUEST['start']){ $_REQUEST['end'] = $_REQUEST['start']; }

$assoc = array();
$query = 'SELECT reservations_assoc.*, (reservations_assoc.`adults`+reservations_assoc.`seniors`+reservations_assoc.`children`) as `totalpax`';
	$query .= ', (SELECT tours_dates.`dir` FROM `tours_dates` WHERE tours_dates.`tourid` = reservations_assoc.`tourid` AND reservations_assoc.`date` = tours_dates.`date` LIMIT 1) as `dir`';
	$query .= ' FROM `reservations_assoc`,`reservations`';
	//$query .= ' LEFT JOIN `reservations` ON reservations_assoc.`reservation` = reservations.`id` AND reservations.`canceled` = 0';
	//$query .= ' LEFT JOIN `tours_dates` ON tours_dates.`tourid` = reservations_assoc.`tourid` AND reservations_assoc.`date` = tours_dates.`date`';
	$query .= ' WHERE reservations_assoc.`reservation` = reservations.`id` AND (reservations.canceled = 0 OR reservations.canceled IS NULL) AND (reservations_assoc.canceled = 0 OR reservations_assoc.canceled IS NULL)';
		$query .= ' AND reservations_assoc.`date` >= "'.$_REQUEST['start'].'" AND reservations_assoc.`date` <= "'.$_REQUEST['end'].'"';
		if(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && is_numeric($_REQUEST['filter'])){
			$query .= ' AND (reservations_assoc.`type` = "t" OR reservations_assoc.`type` = "o") AND reservations_assoc.`tourid` = "'.$_REQUEST['filter'].'"';
			} elseif(isset($_REQUEST['filter']) && trim($_REQUEST['filter']) != "" && $_REQUEST['filter'] != "*all*"){
			$query .= ' AND reservations_assoc.`type` = "'.$_REQUEST['filter'].'"';
			}
		$query .= ' ORDER BY `date` ASC, `dep_time` ASC';
		//echo '<SPAN STYLE="font-size:10px;">'.$query.'<BR></SPAN>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($assoc,$row);
	}

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '<HTML>'."\n\n";
	echo '<HEAD><TITLE>Schedule</TITLE><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><LINK HREF="stylesheet.css" REL="stylesheet" TYPE="text/css"></HEAD>'."\n\n";
	echo '<BODY BGCOLOR="#FFFFFF" TOPMARGIN="0" LEFTMARGIN="0" onLoad="javascript:window.print();">'."\n\n";
	echo '<CENTER>'."\n\n";
	}

if($_SERVER['REMOTE_ADDR'] == '76.103.138.139'){
	//echo $query;
}

?>
<style>
	.note_header {
		color: #666666;
		font-style: italic;
	}
</style>
<?

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" STYLE="empty-cells:show">'."\n";

if($num_results == 0){

echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
	echo '<TD ALIGN="left" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:9pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$_REQUEST['start']),(date("j",$_REQUEST['start'])-1),date("Y",$_REQUEST['start'])).'">&lt; Previous Day</A></TD>';
	echo '<TD ALIGN="center" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold;">'.date("l, F j, Y",$_REQUEST['start']);
		if($_REQUEST['end'] != $_REQUEST['start']){ echo ' - '.date("l, F j, Y",$_REQUEST['end']); }
		echo '</TD>';
	echo '<TD ALIGN="right" STYLE="padding:5px; border-bottom:solid 1px #000000; font-family:Arial; font-size:9pt; font-weight:bold;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$_REQUEST['end']),(date("j",$_REQUEST['end'])+1),date("Y",$_REQUEST['end'])).'">Next Day &gt;</A></TD>';
	echo '</TR>'."\n\n";

echo '<TR><TD COLSPAN="3" ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><I>- No reservations found for this time period. -</I></TD></TR>'."\n\n";

} else {

//CONVERSION ARRAYS
$conv_binary = array('No','Yes');
$conv_types = array(
	'a' => 'Activity/Tour option',
	'r' => 'Bundu route',
	't' => 'Bundu tour',
	'l' => 'Lodging',
	's' => 'Shuttle',
	'p' => 'Tour transport',
	'o' => 'Other'
	);

//GET MARKETS
$markets = array();
$query = 'SELECT * FROM `markets` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$markets['m'.$row['id']] = $row;
	}

//GET VENDORS
$vendors = array();
$query = 'SELECT * FROM `vendors` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$vendors['v'.$row['id']] = $row;
	}

//GET SHUTTLE TRANS TYPES
$transtypes = array();
$query = 'SELECT * FROM `shuttle_transtypes` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$transtypes['t'.$row['id']] = $row;
	}

//GET LOCATIONS
$locations = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$locations['l'.$row['id']] = $row;
	}

$trackdate = 0;
foreach($assoc as $key => $row){
	if($row['date'] != $trackdate){
		echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;"><TD ALIGN="center" COLSPAN="5" STYLE="border-bottom:solid 1px #000000;">';
			echo '<TABLE BORDER="0" STYLE="width:100%"><TR>';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){ echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; font-weight:bold; width:100px;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$row['date']),(date("j",$row['date'])-1),date("Y",$row['date'])).'">&lt; Previous Day</A></TD>'; }
			echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">'.date("l, F j, Y",$row['date']).'</TD>';
			if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){ echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; font-weight:bold; width:100px;"><A HREF="'.$_SERVER['PHP_SELF'].'?start='.mktime(0,0,0,date("n",$row['date']),(date("j",$row['date'])+1),date("Y",$row['date'])).'">Next Day &gt;</A></TD>'; }
			echo '</TR></TABLE>';
			echo '</TD></TR>'."\n\n";
			bgcolor('reset');
		}

	echo '<TR STYLE="background:#'.bgcolor('').'">';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:4px; font-weight:bold;">';
			echo date("g:ia",$row['dep_time']);
			//echo '<div style="font-size:9px; color:grey;">'.date("n/j", $row['dep_time']).'</div>';
			echo '</TD>';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:5px;"><B>'.$row['name'].'</B><BR>';
			echo '<SPAN STYLE="font-size:9pt;">';
			echo '<A HREF="3_reservations.php?view='.$row['reservation'].'" TITLE="View Reservation">'.$row['reservation'].'</A><BR>';
			echo '<I>';
			if($row['type'] == "s"){
				echo $transtypes['t'.$row['trans_type']]['name'].'<BR>';
				} else {
				echo $conv_types[$row['type']].'<BR>';
				}
			echo 'Pax: '.$row['totalpax'];
			echo '</I></SPAN></TD>';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:5px; font-size:8pt; max-width:600px;">';
			if(trim($row['reference']) != ""){ echo nl2br($row['reference']).'<BR>'; }
			if($row['dir'] != NULL){
				echo '<span class="note_header">Tour direction:</span>';
				if($row['dir'] == "r"){ echo ' Reverse'; } else { echo ' Forward'; }
				echo '<BR>';
				}
			//if($row['dir'] != NULL && trim($row['notes']) != ""){ echo '<BR>'; }
			if(trim($row['notes']) != ""){
				if($row['type'] == "s"){ echo '<span class="note_header">Flight:</span>'; } else { echo '<span class="note_header">Notes:</span>'; }
				echo ' '.wordwrap($row['notes'], 60, "\n", true);
				echo '<BR>';
				}
			//if(trim($row['notes']) != "" && trim($row['driver_info']) != ""){ echo '<BR>'; }
			if(trim($row['driver_info']) != ""){
				echo '<span class="note_header">Driver:</span> ';
				echo wordwrap($row['driver_info'], 60, "\n", true);
			}
			echo '</TD>';
		echo '<TD VALIGN="top" CLASS="viewsched" STYLE="padding-left:5px; font-size:8pt;">';
			if(trim($row['dep_loc']) != ""){
				echo '<span class="note_header">Pick up:</span> ';
				if($row['routeid'] > 0 && is_numeric($row['dep_loc'])){
					echo $locations['l'.$row['dep_loc']]['name'];
					} else {
					echo nl2br($row['dep_loc']);
					}
				}
			if(trim($row['dep_loc']) != "" && trim($row['arr_loc']) != ""){ echo '<BR>'; }
			if(trim($row['arr_loc']) != ""){
				echo '<span class="note_header">Drop off:</span> ';
				if($row['routeid'] > 0 && is_numeric($row['arr_loc'])){
					echo $locations['l'.$row['arr_loc']]['name'];
					} else {
					echo nl2br($row['arr_loc']);
					}
				}
			echo '</TD>';
		if(!isset($_REQUEST['print']) || $_REQUEST['print'] != "y"){
			echo '<TD VALIGN="top" ALIGN="center" CLASS="viewsched">';
				echo '<A HREF="3_reservations.php?view='.$row['reservation'].'" TITLE="View Reservation"><IMG SRC="img/viewico.gif" BORDER="0"></A>';
				echo '<BR>';
				echo '<A HREF="3_reservations.php?edit='.$row['reservation'].'" TITLE="Edit Reservation"><IMG SRC="img/editico.gif" BORDER="0"></A>';
				echo '</TD>';
			}
		echo '</TR>'."\n";

	$trackdate = $row['date'];
	} //End ForEach


} //End $num_results if statement

echo '</TABLE><BR>'."\n\n";

/*
foreach($assoc as $key => $row){
	echo $row['id'].', ';
}
*/


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '</CENTER>'."\n\n";
	echo '</BODY>'."\n\n";
	echo '</HTML>'."\n\n";
	}


?>