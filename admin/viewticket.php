<? //This custom utility created by Dominick Bernal - www.bernalwebservices.com


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	$pageid = "3_bbtickets";
	require("validate.php");
	}

$query = 'SELECT * FROM `bundubus_tickets` WHERE `id` = "'.$_REQUEST['view'].'" LIMIT 1';
$result = mysql_query($query);
$fillform = mysql_fetch_assoc($result);

//GET BUNDU BUS STOPS
$stops = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$stops['s'.$row['id']] = $row;
	}

//GET ASSOCIATED ROUTES
$assoc = array();
$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`bbticket` > 0 AND reservations_assoc.`bbticket` = "'.getval('id').'" AND reservations_assoc.`routeid` = routes.`id` ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($assoc,$row);
	}


if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '<HTML>'."\n\n";
	echo '<HEAD><TITLE>Reservation: '.$_REQUEST['view'].'</TITLE><LINK HREF="stylesheet.css" REL="stylesheet" TYPE="text/css"></HEAD>'."\n\n";
	echo '<BODY BGCOLOR="#FFFFFF" TOPMARGIN="0" LEFTMARGIN="0" onLoad="javascript:window.print();">'."\n\n";
	echo '<CENTER>'."\n\n";
	echo '<FONT FACE="Arial" SIZE="5"><U>Hop On, Hop Off Ticket: '.$_REQUEST['view'].'</U></FONT><BR><BR>'."\n\n";
	}

echo '<TABLE BORDER="0" WIDTH="94%" CELLSPACING="0" CELLPADDING="2" STYLE="empty-cells:show">'."\n";

//BASIC
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Basic / Contact Info</TD>';
		echo '</TR>'."\n\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Ticket ID</TD><TD CLASS="viewresr">'.getval('id').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Name</TD><TD CLASS="viewresr">'.getval('firstname').' '.getval('lastname').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Home/business phone</TD><TD CLASS="viewresr">'.getval('phone_homebus').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Cell phone</TD><TD CLASS="viewresr">'.getval('phone_cell').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Cell country</TD><TD CLASS="viewresr">'.getval('cell_country').'</TD></TR>'."\n";
	echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Email</TD><TD CLASS="viewresr"><A HREF="mailto:'.getval('email').'">'.getval('email').'</A></TD></TR>'."\n";
	//if(getval('email_inst') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Special instructions<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">This will be included in the customer email confirmation.</SPAN></TD><TD CLASS="viewresr">'.nl2br(getval('email_inst')).'</TD></TR>'."\n"; }
	if(getval('email_conf') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Email confirmation was sent</TD><TD CLASS="viewresr">'.date("g:ia n/d/Y",getval('email_conf')).'</TD></TR>'."\n"; }
	echo "\n";

//ASSOCIATED ROUTES
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Routes Reserved</TD>';
		echo '</TR>'."\n\n";
	echo '<TR><TD COLSPAN="2" STYLE="padding:0px;">';
		if(count($assoc) > 0){
		echo '<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" WIDTH="100%">';
		foreach($assoc as $row){
			if($row['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }
			if(is_numeric($row['dep_loc'])){ $row['dep_loc'] = $stops['s'.$row['dep_loc']]['name']; }
			if(is_numeric($row['arr_loc'])){ $row['arr_loc'] = $stops['s'.$row['arr_loc']]['name']; }
			$row['legname'] = date("g:ia",$row['dep_time']).' '.$row['dep_loc'].' <I>-to-</I> '.date("g:ia",$row['arr_time']).' '.$row['arr_loc'];
			$row['legname'] = str_replace("\n",' ',$row['legname']);
			$row['legname'] = str_replace('"','',$row['legname']);
			echo '<TR STYLE="background:#'.bgcolor('').'">';
			echo '<TD CLASS="viewsched" STYLE="font-family:Arial; font-size:9pt; padding-left:5px; padding-right:8px;"><A HREF="3_reservations.php?view='.$row['reservation'].'">'.$row['reservation'].'</A></TD>';
			echo '<TD CLASS="viewsched" STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;"><A HREF="3_schedule.php?start='.$row['date'].'" STYLE="color:#000000;">'.date("M j, Y",$row['date']).'</A></TD>';
			echo '<TD CLASS="viewsched" STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$row['legname'].'</TD>';
			echo '<TD ALIGN="center" CLASS="viewsched" STYLE="font-family:Arial; font-size:8pt; padding-left:4px; color:#666666;">';
				if($row['canceled'] > 0){ echo '<I>Canceled</I>'; }
				echo '</TD>';
			echo '</TR>';
			}
		echo '</TABLE>';
		} else {
		echo '<DIV STYLE="padding:3px; font-family:Arial; font-size:10pt; text-align:center;">None</DIV>';
		} //End count if statement
		echo '</TD></TR>'."\n";

//PAYMENTS
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Total / Payments</TD>';
		echo '</TR>'."\n\n";
	if(getval('amount') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Total</TD><TD CLASS="viewresr">$'.getval('amount').'</TD></TR>'."\n"; }
	if(getval('cc_name') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Credit card name</TD><TD CLASS="viewresr">'.getval('cc_name').'</TD></TR>'."\n"; }
	if(getval('cc_num') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Credit card number</TD><TD CLASS="viewresr">'.substr(getval('cc_num'),0,4).'**'.substr(getval('cc_num'),-2).'</TD></TR>'."\n"; }
	if(getval('cc_expdate') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Expiration date</TD><TD CLASS="viewresr">'.getval('cc_expdate').'</TD></TR>'."\n"; }
	if(getval('cc_scode') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Security code</TD><TD CLASS="viewresr">'.getval('cc_scode').'</TD></TR>'."\n"; }
	if(getval('pay_method') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Method of payment</TD><TD CLASS="viewresr">'.getval('pay_method').'</TD></TR>'."\n"; }
	if(getval('alt_name') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Alternate name</TD><TD CLASS="viewresr">'.getval('alt_name').'</TD></TR>'."\n"; }
	if(getval('card_run') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Card was run</TD><TD CLASS="viewresr">'.date("g:ia n/j/Y",getval('card_run')).'</TD></TR>'."\n"; }
	if(getval('pnp_orderid') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">PlugnPay transaction ID</TD><TD CLASS="viewresr">'.getval('pnp_orderid').'</TD></TR>'."\n"; }
	if(getval('conf_details') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">PlugnPay confirmation details</TD><TD CLASS="viewresr" STYLE="font-weight:normal;">'.nl2br(getval('conf_details')).'</TD></TR>'."\n"; }
	echo "\n";

//NOTES/ETC.
	bgcolor('reset');
	echo '<TR STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x;">';
		echo '<TD COLSPAN="2" ALIGN="left" STYLE="border-bottom:solid 1px #000000; font-family:Arial; font-size:10pt; font-weight:bold; padding-left:10px;">Notes / Etc.</TD>';
		echo '</TR>'."\n\n";
	if(getval('comments') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">User comments<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Entered online while making reservation.</SPAN></TD><TD CLASS="viewresr">'.nl2br(getval('comments')).'</TD></TR>'."\n"; }
	if(getval('booker') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Booker</TD><TD CLASS="viewresr">'.getval('booker').'</TD></TR>'."\n"; }
	if(getval('date_booked') > 0){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Date booked</TD><TD CLASS="viewresr">'.date("n/j/Y",getval('date_booked')).'</TD></TR>'."\n"; }
	if(getval('notes') != ""){ echo '<TR STYLE="background:#'.bgcolor('').'"><TD CLASS="viewresl">Notes</TD><TD CLASS="viewresr" STYLE="font-weight:normal;">'.nl2br(getval('notes')).'</TD></TR>'."\n"; }
	echo "\n";

	echo '</TABLE><BR>'."\n\n";

if(isset($_REQUEST['print']) && $_REQUEST['print'] == "y"){
	echo '</CENTER>'."\n\n";
	echo '</BODY>'."\n\n";
	echo '</HTML>'."\n\n";
	}


?>