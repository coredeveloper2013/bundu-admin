<?

if($_SERVER["HTTP_HOST"] != "www.bundubashers.com"){
	header("Location: http://www.bundubashers.com".$_SERVER['REQUEST_URI']);
	exit;
	}

if(!isset($_SESSION)){
	@ini_set("session.gc_maxlifetime","10800");
	@ini_set("session.cookie_domain",'.bundubashers.com');
	@session_set_cookie_params(10800,'/','.bundubashers.com',false);
	session_start();
	}

if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == "agent"){
	unset($_SESSION['agent']);
	}

include($_SERVER['DOCUMENT_ROOT'].'/header.php');

echo '<center>'."\n\n";

//echo '<PRE>'; print_r($_POST); echo '</PRE>';

function printmsgs($successmsg,$errormsg){
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<BR>'."\n"; endif;
	if(isset($successmsg) && count($successmsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Success</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$successmsg).'</I><BR></FONT></TD></TR>
	</TABLE>';
	if(isset($errormsg) && count($errormsg) > 0): echo '<BR>'; endif;
	echo "\n\n";
	}
	if(isset($errormsg) && count($errormsg) > 0){
	echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>'.implode("<BR>",$errormsg).'</I></FONT></TD></TR>
	</TABLE>'."\n\n";
	}
	if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<BR>'."\n"; endif;
}

$successmsg = array();
$errormsg = array();
if(isset($_POST['utaction']) && $_POST['utaction'] == "agentl"){
//GET AGENT AUTHORIZATION
$passcr = md5($_POST['agentp']);
$query = 'SELECT * FROM `agents` WHERE `user` = "'.$_POST['agentu'].'" AND `pass` = "'.$passcr.'" LIMIT 1';
@$result = mysql_query($query);
@$num_results = mysql_num_rows($result);
	if($num_results == 1){
	$_SESSION['agent'] = mysql_fetch_assoc($result);
	array_push($successmsg,'Welcome '.$_SESSION['agent']['name'].', you have been logged in.');
	} else {
	array_push($errormsg,'Your username and/or password is incorrect.');
	}
} //END $_POST['utaction'] IF STATMENT


printmsgs($successmsg,$errormsg);

//echo '<PRE>'; print_r($_SESSION['agent']); echo '</PRE>';

echo '<BR><BR>'."\n\n";

echo '<FORM METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" STYLE="margin:0px; padding:0px;">
<INPUT TYPE="hidden" NAME="utaction" VALUE="agentl">'."\n\n";

if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != "" && $_SESSION['agent']['name'] != ""){
	echo '<SPAN STYLE="font-family:Arial; font-size:10pt;"><B><I>'.gettrans('You are logged in as:').'</I><BR>'.$_SESSION['agent']['name'].'</B><BR><BR>';
		if($_SESSION['lang'] != 1){
			echo gettrans('You may now use the site to order tours. You may also order Bundu Bus routes from www.bundubus.com.  For Bundu Bus, your discount will be applied in the shopping cart.');
		} else {
			echo 'You may now use the site to order tours.<BR>You may also order Bundu Bus routes from <A HREF="http://www.bundubus.com">www.bundubus.com</A>.  For Bundu Bus, your discount will be applied in the shopping cart.';
		}
	echo '</SPAN><BR><BR>';
	echo '<SPAN STYLE="font-family:Arial; font-size:8pt;">[ <A HREF="'.$_SERVER['PHP_SELF'].'?logout=agent">'.gettrans('Log Out').'</A> ]</SPAN>'."\n";
} else {
	echo '<TABLE BORDER="0" WIDTH="300" HEIGHT="100" CELLSPACING="0" CELLPADDING="3" STYLE="border: 1px solid #666666;">
	<TR><TD ALIGN="center" COLSPAN="2" CLASS="calhead" HEIGHT="26"><FONT FACE="Arial" SIZE="3" COLOR="#FFFFFF"><B>'.gettrans('Agent Log In').'</B></FONT></TD></TR>
	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Username/Email:').'</B></FONT></TD><TD><INPUT TYPE="text" NAME="agentu" SIZE="20"></TD></TR>
	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Password:').'</B></FONT></TD><TD><INPUT TYPE="password" NAME="agentp" SIZE="20"></TD></TR>
	<TR><TD></TD><TD><INPUT TYPE="submit" VALUE="Submit"></TD></TR>
	</TABLE>'."\n";
}

echo '</FORM>'."\n\n";

include($_SERVER['DOCUMENT_ROOT'].'/footer.php');

?>