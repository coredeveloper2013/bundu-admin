<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once '../common.inc.php';

//require_once 'auth.inc.php';
$start =  strtotime($_REQUEST['start_date']);
$startTT =  strtotime($_REQUEST['start_date'].' 00:00:00');
$startEE =  strtotime($_REQUEST['start_date'].' 11:59:59');
$end =  strtotime($_REQUEST['end_date']);
$adultPerson = $_REQUEST['adult'];
if ($end <= $start){
    echo json_encode(['status'=>5000,'error'=>'Please enter proper  start and end date']);
    exit();
}

$diff = abs($start - $end);
$days = floor($diff/ (60*60*24));

if (empty($_REQUEST['start_date']) || empty($_REQUEST['end_date']) || empty($_REQUEST['adult'])) {
    echo json_encode(['status'=>5000,'error'=>'Date is not selected.']);
    exit;
}

//    $query = 'SELECT lodging.* , lodging_pricing.price, lodging_pricing.startdate, lodging_pricing.enddate,lodging_pricing.min_nights, lodging_pricing.max_nights ';
//    $query .= ' FROM `lodging_pricing` join lodging on lodging_pricing.lodgeid = lodging.id where `startdate` <= '.$start.' and `enddate` >='.$end.' and lodging.type = "y" ';
//
//    $SqlQuery = $query;
//    $result = mysql_query($SqlQuery);
//    $number_of_lodge = mysql_num_rows($result);
//    if ($number_of_lodge>0){
//            $query .= ' and (lodging.available*lodging.capacity) >= '.$adultPerson.'  group by lodgeid';
//            $query .= ' group by lodgeid';



$query .= 'SELECT * FROM lodging where lodging.type = "y" ORDER by `sort` ASC ';

$result = mysql_query($query);
$number_of_ldg = mysql_num_rows($result);
if ($number_of_ldg>0){
    $data = [];
    while ($row = mysql_fetch_assoc($result)) {
        $data[] = $row;
    }
    foreach ($data as $key => $datum) {
        $id = $datum['id'];
        $qry = 'SELECT * FROM `lodging_pricing_new` where `date` = '.$start.' AND `lodgeid` ='.$id ;
        $result = mysql_query($qry);
        $count = mysql_num_rows($result);
        if ($count>0){
            while ($row = mysql_fetch_assoc($result)) {
                $data[$key]['price'] = $row['price'];
                $data[$key]['booked'] = $row['unit'];
            }
        }else{
            $data[$key]['price'] = 0;
        }
        $qry1 = 'SELECT sum(rooms) as rooms  FROM `reservations_assoc` where (`date` >= '.$startTT.' and `date` <='.$startEE.' ) AND `lodgeid` ='.$id;
        $result1 = mysql_query($qry1);
        $count1 = mysql_num_rows($result1);
//                    echo $count1;
        if ($count1>0){

            while ($row1 = mysql_fetch_assoc($result1)) {
                $data[$key]['rooms']            = ($row1['rooms'] == null) ? 0 : $row1['rooms'];

            }
        }

        if($data[$key]['booked'] > $data[$key]['rooms'] && $data[$key]['booked'] <= $data[$key]['available']){
            $data[$key]['rooms'] = $data[$key]['booked'];
        }



    }

//                echo json_encode(['status' => 2000, 'data'=>$qry]);
    echo json_encode(['status' => 2000, 'data'=>$data]);
    exit();
}else{
    echo json_encode(['status' => 5000, 'error'=>'There have no available lodge or apartment for '.$adultPerson.' person! ']);
}






//    } else {
//        $query = 'SELECT MIN(`startdate`) as `earliest`, MAX(`enddate`) as `latest` FROM `lodging_pricing` ';
//        //$query .= ' WHERE `date` >= "' . strtotime('today') . '"'; // for testing
//
//        $result = mysql_query($query);
//        $number_of_row = mysql_num_rows($result);
//        while ($availDates = mysql_fetch_assoc($result)){
//            $availDate1 = date('M. j, Y',$availDates['earliest']);
//            $availDate2 = date('M. j, Y',$availDates['latest']);
//        }
//        $data = 'Please choose dates between ' .$availDate1. ' and ' . $availDate2 . ' Thank you.';
//        echo json_encode(['status' => 5000, 'error'=>$data]);
//
//    }
