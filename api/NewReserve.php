<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once '../common.inc.php';

if (!isset($_POST['cc_num'])){
    echo 'Authentication Failed';
    exit();
}
$successmsg = array();
$errormsg = array();
$adminsuccessmsg = array();
$adminerrormsg = array();

$als_checkin = mktime(0, 0, 0, $_POST['start_month'], $_POST['start_day'], $_POST['start_year']);
$_POST['total'] = number_format($_POST['total'], 2, '.', '');
$_POST['cc_num'] = trim($_POST['cc_num']);
$_POST['cc_num'] = str_replace(' ', '', $_POST['cc_num']);
$_POST['cc_num'] = str_replace('-', '', $_POST['cc_num']);
$_POST['cc_expdate'] = $_POST['cc_expdate_month'] . '/' . $_POST['cc_expdate_year'];

$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`pay_method`,`comments`,`http_referer`,`booker`,`date_booked`)';
$query .= ' VALUES("' . $_POST['name'] . '","' . $_POST['phone_homebus'] . '","' . $_POST['phone_cell'] . '","' . $_POST['email'] . '","' . $_POST['total'] . '","' . $_POST['name'] . '","' . $_POST['cc_num'] . '","' . $_POST['cc_expdate'] . '","' . $_POST['cc_scode'] . '","C/C","' . $_POST['comments'] . '","' . $_SESSION['http_referer'] . '","Website","' . $time . '")';
@mysql_query($query);
$confnum = mysql_insert_id();
$thiserror = mysql_error();
if($thiserror == ""){

    //GET LODGING TYPES
    $lodging = array();
    $query = 'SELECT * FROM `lodging` WHERE `type` = "y" ORDER BY `name` ASC';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    for($i=0; $i<$num_results; $i++){
        $row = mysql_fetch_assoc($result);
        $lodging[$row['id']] = $row;
    }


    @array_push($adminsuccessmsg,'Saved new lodging reservation.<BR>Order #: <B>'.$confnum.'</B>');
    @array_push($successmsg,'Your order number: <B>'.$confnum.'</B>');

    $query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`date`,`end_date`,`onlydateavl`,`lodgeid`,`nights`,`rooms`,`amount`)';
    $query .= ' VALUES("' . $confnum . '","' . $lodging[$_REQUEST['type']]['name'] . '","l","' . $_POST['name'] . '","' . $_POST['guests'] . '","' . $als_checkin . '","' . $_POST['end_date'] . '",1,"' . $_POST['type'] . '","' . $_POST['nights'] . '","' . $_POST['total_room'] . '","' . $_POST['total'] . '")';
    @mysql_query($query);
    $thiserror = mysql_error();
    if ($thiserror != "") {
        array_push($adminerrormsg, $thiserror);
    }

    $heading = 'New Lodging Reservation #' . $confnum;
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
//$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
    $headers .= "From: " . $_POST['name'] . " <" . $_POST['email'] . ">\r\n";
    $message = "A customer has ordered a new lodging reservation.<BR><BR>\n\n" . $confnum;
    if (!mail('test@gmail.com', $heading, $message, $headers)) {
        array_push($errormsg, 'Unable to send merchant notification.');
    }


    if (isset($adminsuccessmsg) && count($adminsuccessmsg) > 0 || isset($adminerrormsg) && count($adminerrormsg) > 0) {
        if (isset($adminsuccessmsg) && count($adminsuccessmsg) > 0) {
            $logentry = implode("\n", $adminsuccessmsg);
            $logentry = addslashes($logentry);
            @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("' . $time . '","Web User","' . $pageid . '","' . $logentry . '")');
        }
        if (isset($adminerrormsg) && count($adminerrormsg) > 0) {
            $logentry = implode("\n", $adminerrormsg);
            $logentry = addslashes($logentry);
            @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("' . $time . '","Web User","' . $pageid . '","Error: ' . $logentry . '")');
        }
    }

    echo json_encode($confnum);

}
