<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once '../common.inc.php';

if (empty($_REQUEST['start_date']) || empty($_REQUEST['nights'])) {
    echo json_encode(['status' => 5000, 'error' => 'Date is not selected.']);
    exit;
}

$start = strtotime($_REQUEST['start_date']);
$nights = $_REQUEST['nights'];
$data = [];

$query = 'SELECT * FROM `lodging` WHERE `type` = "y" order by `sort` asc ';
$result = mysql_query($query);
$number_of_lodge = mysql_num_rows($result);
if ($number_of_lodge > 0) {
    while ($row = mysql_fetch_assoc($result)) {
        $data[] = $row;
    }
}
//for ($i = $start; $i < strtotime('+'.$nights.' day', $start);$i = strtotime('+1 day', $i)){
//    $query = 'INSERT INTO `lodging_pricing_new` (`lodgeid`, `price`, `unit`, `min_nights`, `max_nights`, `available`, `date`)  ';
//    $query .= ' VALUES (6,200,0,1,1,3,' . $i . ')';
//    $result = mysql_query($query);
//}

foreach ($data as $key => $datum) {
//    for ($i = $start; $i < strtotime('+'.$nights.' day', $start);$i = strtotime('+1 day', $i)){
    for ($i = 0; $i < $nights;$i++){
        $ssday = strtotime('+'.$i.' day', strtotime($_REQUEST['start_date']));
        $tday = strtotime('+'.$i.' day', strtotime($_REQUEST['start_date'].' 00:00:00'));
        $eday = strtotime('+'.$i.' day', strtotime($_REQUEST['start_date'].' 11:59:59'));
        $qu = 'SELECT * FROM `lodging_pricing_new` WHERE `date` = '.$ssday.' and `lodgeid` = '.$datum["id"];
        $re = mysql_query($qu);
        $date_data = mysql_num_rows($re);
        if ($date_data>0){
            while ($row = mysql_fetch_assoc($re)) {
                $qry1 = 'SELECT sum(rooms) as rooms  FROM `reservations_assoc` where `date` >= '.$tday.' and `date` <='.$eday.'  AND `lodgeid` ='.$datum["id"];
                $result1 = mysql_query($qry1);
                $count1 = mysql_num_rows($result1);
                if ($count1>0){
                    while ($row1 = mysql_fetch_assoc($result1)) {
                        if ($row1['rooms'] == null){
                            $row1['rooms'] = 0;
                        }
                        if($row1['rooms'] > $row['unit']){
                            $row['unit'] = $row1['rooms'];
                        }
                    }
                }
                $data[$key]['data'][] = $row;
            }
        }else{
            $data[$key]['data'][]= [
                'id'=>0,
                'lodgeid'=>$datum["id"],
                'price'=>0,
                'unit'=>0,
                'date'=>$ssday,
                'min_nights'=>0,
                'max_nights'=>0,
                'available'=>0,
            ];
        }
    }
}


if (count($data)) {
    echo json_encode(['status' => 2000, 'data' => $data]);
    exit();
} else {
    echo json_encode(['status' => 5000, 'error' => 'No results found!']);
}
