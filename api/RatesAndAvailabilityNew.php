<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once '../common.inc.php';

$data = [];

$query = 'SELECT * FROM `lodging` WHERE `type` = "y"';
$result = mysql_query($query);

$number_of_row = mysql_num_rows($result);
if ($number_of_row > 0) {
    while ($row = mysql_fetch_assoc($result)) {
        $pricing = [];
        $query = 'SELECT * FROM `lodging_pricing` WHERE lodgeid = ' . $row['id'] . ' ORDER BY `startdate` ASC';
        $new_result = mysql_query($query);
        $number_of_row = mysql_num_rows($new_result);
        if ($number_of_row > 0) {
            while ($new_row = mysql_fetch_assoc($new_result)) {
                $new_row['start_date_formatted'] = date('d M, Y', $new_row['startdate']);
                $new_row['end_date_formatted'] = date('d M, Y', $new_row['enddate']);

                $qry1 = 'SELECT sum(rooms) as rooms,date_v2,date  FROM `reservations_assoc` where (`date` <= '.$new_row['startdate'].' and `end_date` >='.$new_row['startdate'].' ) OR (`end_date` >='.$new_row['enddate'].' and `date` <= '.$new_row['enddate'].' ) AND `lodgeid` ='.$id;
                $result1 = mysql_query($qry1);
                $count1 = mysql_num_rows($result1);
                if ($count1>0){
                    while ($row1 = mysql_fetch_assoc($result1)) {
                        $new_row['rooms'] = $row1['rooms'];
                        $new_row['reserve_end'] = $row1['end_date'];
                        $new_row['reserve_start'] = $row1['date'];

                    }
                }else{
                    $new_row['rooms'] = 0;
                    $new_row['reserve_end'] = '';
                    $new_row['reserve_start'] = '';
                }
                $pricing[] = $new_row;

            }
        }



        $row['pricing'] = $pricing;
        $data[] = $row;
    }
}


if (count($data)) {
    echo json_encode(['status' => 2000, 'data' => $data]);
    exit();
} else {
    echo json_encode(['status' => 5000, 'error' => 'No results found!']);
}
