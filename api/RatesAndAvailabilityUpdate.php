<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

require_once '../common.inc.php';

$findDateData = 'SELECT * FROM `lodging_pricing_new` where `lodgeid` = '.$_REQUEST['lodge_id'].' and `date` = '.$_REQUEST['date'];
$result1 = mysql_query($findDateData);
$findDateDataCount = mysql_num_rows($result1);


$_red_date = date('Y-m-d', $_REQUEST['date']);
// Check request data
if (isset($_REQUEST['booked']) && isset($_REQUEST['lodge_id']) && $_REQUEST['booked'] != '' && $_REQUEST['lodge_id'] != 0) {
    $tday = strtotime($_red_date.' 00:00:00');
    $eday = strtotime($_red_date.' 11:59:59');

    $qry1 = 'SELECT sum(rooms) as rooms  FROM `reservations_assoc` where `date` >= '.$tday.' and `date` <='.$eday.'  AND `lodgeid` ='.$_REQUEST['lodge_id'];
    $result1 = mysql_query($qry1);
    $count1 = mysql_num_rows($result1);
    if ($count1>0){
        while ($row1 = mysql_fetch_assoc($result1)) {
            if ($row1['rooms'] == null){
                $booked_room  = 0;
            }else{
                $booked_room = $row1['rooms'];
            }
        }
    }
    if ($booked_room > $_REQUEST['booked']){
        echo json_encode(['status' => 5000,'booked' => $booked_room, 'error' => 'Sorry !! For this day '.$booked_room.' lodge already reserved and confirmed by guests !']);
        exit();
    }
    if ($findDateDataCount > 0) {
        $query = 'UPDATE `lodging_pricing_new` SET `unit` = ' . $_REQUEST['booked'] . ' WHERE `lodgeid` = ' . $_REQUEST['lodge_id'].' and `date` = '.$_REQUEST['date'].' and `available` >='.$_REQUEST['booked'];
        $result = mysql_query($query);
    } else {
        $query = 'INSERT INTO `lodging_pricing_new` (`lodgeid`, `price`, `unit`, `min_nights`, `max_nights`, `available`, `date`)  ';
        $query .= ' VALUES (' . $_REQUEST['lodge_id'] . ',0,' . $_REQUEST['booked'] . ',0,0,0,' . $_REQUEST['date'] . ')';
        $result = mysql_query($query);
    }

} elseif (isset($_REQUEST['available']) && isset($_REQUEST['lodge_id']) && $_REQUEST['available'] != '' && $_REQUEST['lodge_id'] != '') {

    $query = 'UPDATE `lodging` SET `available` = ' . $_REQUEST['available'] . ' WHERE `id` = ' . $_REQUEST['lodge_id'];
    $result = mysql_query($query);

} elseif (isset($_REQUEST['price']) && isset($_REQUEST['lodge_id']) && $_REQUEST['price'] != '' && $_REQUEST['lodge_id'] != '') {
    if ($findDateDataCount > 0) {
        $query = 'UPDATE `lodging_pricing_new` SET `price` = ' . $_REQUEST['price'] . ' WHERE `lodgeid` = ' . $_REQUEST['lodge_id'].' and `date` = '.$_REQUEST['date'];
        $result = mysql_query($query);
    } else {
        $query = 'INSERT INTO `lodging_pricing_new` (`lodgeid`, `price`, `unit`, `min_nights`, `max_nights`, `available`, `date`)  ';
        $query .= ' VALUES (' . $_REQUEST['lodge_id'] . ',' . $_REQUEST['price'] . ',0,0,0,0,' . $_REQUEST['date'] . ')';
        $result = mysql_query($query);
    }
} elseif (isset($_REQUEST['min_nights']) && isset($_REQUEST['lodge_id']) && $_REQUEST['min_nights'] != '' && $_REQUEST['lodge_id'] != '') {
    if ($findDateDataCount > 0) {
        $query = 'UPDATE `lodging_pricing_new` SET `min_nights` = ' . $_REQUEST['min_nights'] . ' WHERE `lodgeid` = ' . $_REQUEST['lodge_id'].' and `date` = '.$_REQUEST['date'];
        $result = mysql_query($query);
    } else {
        $query = 'INSERT INTO `lodging_pricing_new` (`lodgeid`, `price`, `unit`, `min_nights`, `max_nights`, `available`, `date`)  ';
        $query .= ' VALUES (' . $_REQUEST['lodge_id'] . ',0,0,'.$_REQUEST['min_nights'].',0,0,' . $_REQUEST['date'] . ')';
        $result = mysql_query($query);
    }
} elseif (isset($_REQUEST['max_nights']) && isset($_REQUEST['lodge_id']) && $_REQUEST['max_nights'] != '' && $_REQUEST['lodge_id'] != '') {
    if ($findDateDataCount > 0) {
        $query = 'UPDATE `lodging_pricing_new` SET `max_nights` = ' . $_REQUEST['max_nights'] . ' WHERE `lodgeid` = ' . $_REQUEST['lodge_id'].' and `date` = '.$_REQUEST['date'];
        $result = mysql_query($query);
    } else {
        $query = 'INSERT INTO `lodging_pricing_new` (`lodgeid`, `price`, `unit`, `min_nights`, `max_nights`, `available`, `date`)  ';
        $query .= ' VALUES (' . $_REQUEST['lodge_id'] . ',0,0,0,'.$_REQUEST['max_nights'].',0,' . $_REQUEST['date'] . ')';
        $result = mysql_query($query);
    }
} else {
    echo json_encode(['status' => 5000, 'error' => 'Please provide appropriate data!']);
    exit;
}


// Send json response
if ($result) {
    echo json_encode(['status' => 2000, 'success' => 'Data updated successfully. ']);
    exit();
} else {
    echo json_encode(['status' => 5000, 'error' => 'Could not update data! ']);
}