<?php

if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
	echo 'Error: Please use secured version of this API as https://bundubashers.com/api/';
	exit;
}


//Find posted XML
$post_xml = false;
if(isset($_POST['xml'])) {
	//In post?
	$post_xml = $_POST['xml'];

} else {
	//Standard input?
	$f = fopen('php://stdin', 'r');
	while($line = fgets($f)) {
		$post_xml .= $line;
	}
	fclose($f);
}
$xmlArry = array();
if($post_xml != "") {
	$postXmlObj = simplexml_load_string($post_xml);
	$xmlArry = json_decode(json_encode($postXmlObj), true);
}


//Find credentials
$username = false;
$password = false;
if(isset($_POST['username'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];

} elseif(isset($postXmlObj)) {
	$username = $postXmlObj->auth->username;
	$password = $postXmlObj->auth->password;
}


$no_auth = false;
if(isDev() && in_array($_SERVER['REMOTE_ADDR'], $stagingIPs)) {
	//$no_auth = true;
}

$auth = array();
if($username != "" && $password != "") {
	$pass_enc = md5($password.'bash');
	$query = 'SELECT id, auth_type
				FROM api_auth
				WHERE username = "'.mysql_real_escape_string($username).'" AND password = "'.mysql_real_escape_string($pass_enc).'"';
	$result = mysqlQuery($query);
	$auth = @mysql_fetch_assoc($result);
}
if($no_auth) {
	$auth = array('id'=>1, 'auth_type'=>'all');
}
if(@$auth['id'] == "") {
	echo 'Error: Missing or invalid credentials.';
	exit;
}


?>