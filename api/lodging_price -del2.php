<?php

require_once '../common.inc.php';
//require_once 'auth.inc.php';

$successmsg = array();
$errormsg = array();
$adminsuccessmsg = array();
$adminerrormsg = array();

$fullwidth = "780";
$tablewidth = "720";
$leftcol = "300";
$rightcol = ($tablewidth-$leftcol);

$_REQUEST['start_month'] = 5;
$_REQUEST['start_day'] = 3;
$_REQUEST['start_year'] = 2017;
$_REQUEST['nights'] = 2;

$start = mktime(0,0,0,$_REQUEST['start_month'],$_REQUEST['start_day'],$_REQUEST['start_year']);
$end = strtotime("+".$_REQUEST['nights']." days",$start);
$_REQUEST['nights'] = number_format($_REQUEST['nights'],0,'','');
               
//GET LODGING TYPES
$lodging = array();
$query = 'SELECT * FROM `lodging` WHERE `type` = "y" ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$lodging[$row['id']] = $row;
}

//GET AVAILABLE DATES
$query = 'SELECT MIN(`date`) as `earliest`, MAX(`date`) as `latest` FROM `lodging_pricing_new`';
//$query .= ' WHERE `date` >= "'.strtotime('today').'"'; // for testing  
$query .= ' WHERE 1 ';   
if(!empty($_REQUEST['type'])): $query .= ' AND `lodgeid` = "'.$_REQUEST['type'].'"'; endif;
$result = mysql_query($query);
$availdates = mysql_fetch_assoc($result);
//echo $query . '<br />';
//echo mysql_error();
//echo '<PRE>'; print_r($availdates); echo '</PRE>';


//VALIDATE DATES
if(!isset($availdates['earliest']) || $availdates['earliest'] == 0){
        array_push($errormsg,'No dates are currently available for this type of lodging.  Please contact us for details.');
} elseif($start < $availdates['earliest'] || $start > strtotime("+1 day",$availdates['latest']) || $end < $availdates['earliest'] || $end > strtotime("+1 day",$availdates['latest'])){
        array_push($errormsg,'One or more dates chosen are not available.<BR>Please choose dates between '.date("M. j, Y",$availdates['earliest']).' and '.date("M. j, Y",$availdates['latest']).'.  Thank you.');
} 

$pricing = array();
for($i=$start; $i<$end; $i=strtotime("+1 day",$i)){
        $pricing['p'.$i] = 0.00; //date("n/j/Y",$i);
}

//FIND PRICING
if (!empty($_REQUEST['type'])) {
    $query = 'SELECT * FROM `lodging_pricing_new` WHERE `lodgeid` = "'.$_REQUEST['type'].'" AND `date` >= "'.$start.'" AND `date` <= "'.strtotime("+".($_REQUEST['nights']-1)." day",$start).'" ORDER BY `price` DESC';
} else {
    $query = 'SELECT * FROM `lodging_pricing_new` WHERE `date` >= "'.$start.'" AND `date` <= "'.strtotime("+".($_REQUEST['nights']-1)." day",$start).'" ORDER BY `price` DESC';
}
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
if($num_results > 0){
        while($row = mysql_fetch_assoc($result)){
                //echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
                foreach($pricing as $key => $p){
                        $d = str_replace('p','',$key);
                        if($d == $row['date']){ $pricing[$key] = $row['price']; }
                }
        }
        foreach($pricing as $key => $p){
                if($p == 0){
                        array_push($errormsg,'One or more dates chosen are not available.  Please contact us for details.');
                        $step2 = "n";
                        break;
                }
        }
        $total = array_sum($pricing);
} 

echo '<pre>';
print_r($pricing);
echo '</pre>';

//PRINT SUCCESS/ERROR MESSAGES
printmsgs($successmsg,$errormsg);

