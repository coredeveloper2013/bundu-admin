<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/reserve.php


/*
$_POST['xml'] = '<?xml version="1.0" encoding="UTF-8"?>
	<post>
		<auth>
			<username>dominick</username>
			<password>@6}E38TJ6pvNRAf</password>
		</auth>

		<action>reserve</action>

		<tour>
			<id>1110</id>
			<date>2014-07-07</date>
			<pickup_location>The Tropicana</pickup_location>
			<dropoff_location>The Tropicana</dropoff_location>
			<adults>3</adults>
			<seniors/>
			<children/>
			<guest>
				<name>Dominick Bernal</name>
				<weight>160</weight>
				<lunch>Turkey</lunch>
			</guest>
			<guest>
				<name>Testy McGee</name>
				<weight>180</weight>
				<lunch>Chicken</lunch>
			</guest>
			<guest>
				<name>Jane Doe</name>
				<weight>120</weight>
				<lunch>Vegie</lunch>
			</guest>
			<room>
				<num_guests>2</num_guests>
			</room>
			<room>
				<num_guests>1</num_guests>
			</room>
			<extension>
				<date>2014-07-08</date>
				<num_nights>2</num_nights>
				<id_location>23</id_location>
			</extension>
			<activity>
				<id>5</id>
				<date>2014-07-08</date>
				<num_guests>2</num_guests>
				<preferred_time>Morning</preferred_time>
			</activity>
		</tour>

		<tour>
			<id>1234</id>
			<date>2014-06-09</date>
			<pickup_location/>
			<adults>2</adults>
			<seniors/>
			<children/>
			<guest>
				<name/>
				<weight/>
				<lunch/>
			</guest>
			<room>
				<num_guests>2</num_guests>
			</room>
		</tour>

		<route>
			<id>14</id>
			<date>2014-06-10</date>
			<adults>2</adults>
			<seniors/>
			<children/>
		</route>

		<contact>
			<name>Dominick TEST</name>
			<phone_main>111-111-1111</phone_main>
			<phone_cell>222-222-2222</phone_cell>
			<cell_country>USA</cell_country>
			<email>dominick@bernalwebservices.com</email>
			<cc_name>Dominick Bernal</cc_name>
			<cc_num>4111111111111111</cc_num>
			<cc_expdate_month>2</cc_expdate_month>
			<cc_expdate_year>2016</cc_expdate_year>
			<cc_scode>123</cc_scode>
			<cc_zip>12345</cc_zip>
			<alt_name>Dom</alt_name>
			<comments>Here are my brilliant comments.</comments>
		</contact>
	</post>';
*/


require_once '../common.inc.php';
require_once 'auth.inc.php';


//Check credentials for access to create reservations
if($auth['auth_type'] != "all") {
	echo 'Access denied.';
	exit;
}
if(!isset($postXmlObj)) {
	echo 'Error: Missing or invalid XML.';
	exit;
}


/*

Commands:
	- Calculate cost
	- Create reservation

Post:
	<post>
		//-- ALL COMMANDS --
		<auth>
			<username>something</username>
			<password>somepass</password>
		</auth>

		<action>reserve</action>

		<tour>
			<id>1100</id>
			<date>2014-06-07</date>
			<pickup_location/>
			<adults>2</adults>
			<seniors/>
			<children/>
			<guest>
				<name/>
				<weight/>
				<lunch/>
			</guest>
			<room>
				<num_guests>2</num_guests>
			</room>
			<extension>
				<date>2014-06-08</date>
				<num_nights>2</num_nights>
				<id_location>23</id_location>
			</extension>
			<activity>
				<id>56</id>
				<date>2014-06-08</date>
				<num_guests>2</num_guests>
				<preferred_time/>
			</activity>
		</tour>

		<route>
			<id>14</id>
			<date>2014-06-10</date>
			<adults>2</adults>
			<seniors/>
			<children/>
		</route>

		<agent>
			<id>09238423098</id>
			<id_subagent>82347</id_subagent>
			<name>Some Agent</name>
			<phone>234-567-8901</phone>
			<email>someemail@agent.com</email>
		</agent>

		//-- RESERVE ONLY --
		<contact>
			<name/>
			<phone_main/>
			<phone_cell/>
			<cell_country/>
			<email/>
			<cc_name/>
			<cc_num/>
			<cc_expdate_month/>
			<cc_expdate_year/>
			<cc_scode/>
			<cc_zip/>
			<alt_name/>
			<comments/>
		</contact>
	</post>
*/

$action = @$_POST['action'];
if($action == "") {
	$action = @$xmlArry['action'];
}



if($action == "calculate_cost" || $action == "reserve") {
	$total = 0;

	$xmlObj = new DOMDocument();
	$xmlObj->formatOutput = true;
	$xmlObj->encoding = 'UTF-8';
	
	$resultTag = $xmlObj->createElement('result');
	$xmlObj->appendChild($resultTag);
	$email_restypes = array();
	//$email_restypes[] = site_name;

	if($action == "reserve") {
		$errors = array();

		if(@$xmlArry['contact']['name'] == "" && @$xmlArry['agent']['name'] != "") {
			$xmlArry['contact']['name'] = $xmlArry['agent']['name'];
		}
		if(@$xmlArry['contact']['phone_main'] == "" && @$xmlArry['agent']['phone'] != "") {
			$xmlArry['contact']['phone_main'] = $xmlArry['agent']['phone'];
		}
		if(@$xmlArry['contact']['email'] == "" && @$xmlArry['agent']['email'] != "") {
			$xmlArry['contact']['email'] = $xmlArry['agent']['email'];
		}

		$required_fields = array(
			'name' => 'contact name',
			'phone_main' => 'main phone number',
			'phone_cell' => 'cell phone number',
			'cell_country' => 'cell phone country',
			'email' => 'email address',
			'cc_name' => 'billing name',
			'cc_num' => 'credit card number',
			'cc_expdate_month' => 'credit card expiration month',
			'cc_expdate_year' => 'credit card expiration year',
			'cc_scode' => 'credit card security code',
			'cc_zip' => 'credit card zip code'
		);
		foreach($required_fields as $field_name => $desc) {
			if(@$xmlArry['contact'][$field_name] == "") {
				$errors[] = 'Missing '.$desc.'.';
			}
		}
		if(count($errors) > 0) {
			$errorsTag = $xmlObj->createElement('errors');
			$resultTag->appendChild($errorsTag);
			foreach($errors as $error) {
				$tag = $xmlObj->createElement('error');
				$tag->appendChild($xmlObj->createTextNode($error));
				$errorsTag->appendChild($tag);
			}
			header ("Content-Type:text/xml");
			echo $xmlObj->saveXML();
			exit;
		}

		$exp_date = mysqlDate($xmlArry['contact']['cc_expdate_year'].'-'.$xmlArry['contact']['cc_expdate_month'].'-01');
		$exp_date = dateFormat($exp_date, 'n/y');

		$resObj = new reservation();
		$resObj->setName($xmlArry['contact']['name']);
		$resObj->setPhoneHomeBusiness($xmlArry['contact']['phone_main']);
		$resObj->setPhoneCell($xmlArry['contact']['phone_cell']);
		$resObj->setPhoneCellCountry($xmlArry['contact']['cell_country']);
		$resObj->setEmail($xmlArry['contact']['email']);
		$resObj->setCCname($xmlArry['contact']['cc_name']);
		$resObj->setCCnumber($xmlArry['contact']['cc_num']);
		$resObj->setCCexpDate($exp_date);
		$resObj->setCCsecurityCode($xmlArry['contact']['cc_scode']);
		$resObj->setCCzip($xmlArry['contact']['cc_zip']);
		$resObj->setPayMethod('C/C');
		$resObj->setAlternateName($xmlArry['contact']['alt_name']);
		$resObj->setComments($xmlArry['contact']['comments']);
		if(isset($xmlArry['agent'])) {
			$resObj->setIDagent($xmlArry['agent']['id']);
			$resObj->setIDsubAgent($xmlArry['agent']['id_subagent']);
		}
		$resObj->setBooker('API/XML');
		$resObj->setDateTimeBooked('now');
		$resObj->save();
	}

	if(isset($xmlArry['tour'])) {
		if(isset($xmlArry['tour']['id'])) {
			$xmlArry['tour'] = array($xmlArry['tour']);
		}
		foreach($xmlArry['tour'] as $tour) {
			
			if(isset($tour['room']['num_guests'])) {
				$tour['room'] = array($tour['room']);
			}
			if(isset($tour['activity']['id'])) {
				$tour['activity'] = array($tour['activity']);
			}

			$resAssocObj = new reservation_assoc();
			$resAssocObj->setType('t');
			$resAssocObj->setIDtour($tour['id']);
			$resAssocObj->setDate($tour['date']);
			$resAssocObj->setNumAdults((int)$tour['adults']);
			$resAssocObj->setNumSeniors((int)$tour['seniors']);
			$resAssocObj->setNumChildren((int)$tour['children']);

			$amount = $resAssocObj->calcCost(array(
				'id_agent' => @$xmlArry['agent']['id'],
				'rooms' => @$tour['room'],
				'activities' => @$tour['activity']
				));

			$tourTag = $xmlObj->createElement('tour');

			$tag = $xmlObj->createElement('id_cart_item');
			$tag->appendChild($xmlObj->createTextNode($tour['id_cart_item']));
			$tourTag->appendChild($tag);

			$tag = $xmlObj->createElement('id');
			$tag->appendChild($xmlObj->createTextNode($resAssocObj->getIDtour()));
			$tourTag->appendChild($tag);

			$tag = $xmlObj->createElement('date');
			$tag->appendChild($xmlObj->createTextNode($resAssocObj->getDate()));
			$tourTag->appendChild($tag);

			$tag = $xmlObj->createElement('base');
			$tag->appendChild($xmlObj->createTextNode($amount['base']));
			$tourTag->appendChild($tag);

			foreach($amount as $key => $val) {
				if(strpos($key, 'activity') === 0) {
					$tag = $xmlObj->createElement($key);
					$tag->appendChild($xmlObj->createTextNode($val));
					$tourTag->appendChild($tag);
				}
			}

			$tag = $xmlObj->createElement('fuel_surcharge');
			$tag->appendChild($xmlObj->createTextNode($amount['fuel_surcharge']));
			$tourTag->appendChild($tag);

			$tag = $xmlObj->createElement('amount');
			$tag->appendChild($xmlObj->createTextNode($amount['amount']));
			$tourTag->appendChild($tag);

			$resultTag->appendChild($tourTag);

			$total += $amount['amount'];

			if($action == "reserve") {
				if(@$tour['dropoff_location'] == "") {
					$tour['dropoff_location'] = @$tour['pickup_location'];
				}
				$resAssocObj->setIDreservation($resObj->id);
				$resAssocObj->setName($resObj->getName());
				$resAssocObj->setDepLocation(@$tour['pickup_location']);
				$resAssocObj->setArrLocation($tour['dropoff_location']);
				$resAssocObj->setAmount($amount['base']);
				$resAssocObj->save();

				//Guests
				$guests = array();
				if(isset($tour['guest'])) {
					$guests = make_array($tour['guest']);
					if(isset($guests['name'])) {
						$guests = array($guests);
					}
					foreach($guests as $guest) {
						if(is_array($guest['name'])) {
							continue;
						}
						$resGuestObj = new reservation_guest();
						$resGuestObj->setIDreservationAssoc($resAssocObj->id);
						$resGuestObj->setName(@$guest['name']);
						$resGuestObj->setWeight(@$guest['weight']);
						$resGuestObj->setLunch(@$guest['lunch']);
						$resGuestObj->save();
					}
				}

				//Rooms
				if(isset($tour['room'])) {
					$rooms = make_array($tour['room']);
					if(isset($rooms['num_guests'])) {
						$rooms = array($rooms);
					}
					$room_num = 0;
					foreach($rooms as $room) {
						if(is_array($room['num_guests'])) {
							continue;
						}
						$room_num++;
						$adjust = @$amount['room'.$room_num];
						if($adjust == "") {
							$adjust = 0;
						}
						$resRoomObj = new reservation_room();
						$resRoomObj->setIDreservationAssoc($resAssocObj->id);
						$resRoomObj->setRoomNum($room_num);
						$resRoomObj->setNumGuests(@$room['num_guests']);
						$resRoomObj->setNumBeds(@$room['num_beds']);
						$resRoomObj->setAdjust($adjust);
						$resRoomObj->save();
					}
				}

				//Activities
				if(isset($tour['activity'])) {
					$activities = make_array($tour['activity']);
					if(isset($activities['id'])) {
						$activities = array($activities);
					}
					foreach($activities as $key => $activity) {
						if(is_array($activity['id'])) {
							continue;
						}
						$act_num = ($key + 1);
						$actObj = new activity(@$activity['id']);
						$actResAssocObj = new reservation_assoc();
						$actResAssocObj->setIDreservation($resObj->id);
						$actResAssocObj->setType('a');
						$actResAssocObj->setName($resObj->getName());
						$actResAssocObj->setNumAdults((int)$activity['num_guests']);
						$actResAssocObj->setDate(@$activity['date']);
						$actResAssocObj->setPrefTime(@$activity['preferred_time']);
						$actResAssocObj->setIDactivity($actObj->id);
						$actResAssocObj->setAmount(@$amount['activity'.$act_num]);
						$actResAssocObj->save();

						//Guests for activity
						foreach($guests as $guest) {
							if(is_array($guest['name'])) {
								continue;
							}
							$resGuestObj = new reservation_guest();
							$resGuestObj->setIDreservationAssoc($actResAssocObj->id);
							$resGuestObj->setName(@$guest['name']);
							$resGuestObj->setWeight(@$guest['weight']);
							$resGuestObj->setLunch(@$guest['lunch']);
							$resGuestObj->save();
						}
					}
				}
			}

			unset($resAssocObj);
		}
	}

	if(isset($xmlArry['route'])) {
		if(isset($xmlArry['route']['id'])) {
			$xmlArry['route'] = array($xmlArry['route']);
		}
		foreach($xmlArry['route'] as $route) {
			$resAssocObj = new reservation_assoc();
			$resAssocObj->setType('r');
			$resAssocObj->setIDroute($route['id']);
			$resAssocObj->setDate($route['date']);
			$resAssocObj->setNumAdults((int)$route['adults']);
			$resAssocObj->setNumSeniors((int)$route['seniors']);
			$resAssocObj->setNumChildren((int)$route['children']);

			$amount = $resAssocObj->calcCost(array(
				'id_agent' => @$xmlArry['agent']['id'],
				'returnKind' => 'amount'
				));

			$routeTag = $xmlObj->createElement('route');

			$tag = $xmlObj->createElement('id');
			$tag->appendChild($xmlObj->createTextNode($resAssocObj->getIDtour()));
			$routeTag->appendChild($tag);

			$tag = $xmlObj->createElement('date');
			$tag->appendChild($xmlObj->createTextNode($resAssocObj->getDate()));
			$routeTag->appendChild($tag);

			$tag = $xmlObj->createElement('amount');
			$tag->appendChild($xmlObj->createTextNode($amount));
			$routeTag->appendChild($tag);

			$resultTag->appendChild($routeTag);
		
			$total += $amount;

			if($action == "reserve") {
				$resAssocObj->setIDreservation($resObj->id);
				$resAssocObj->setName($resObj->getName());
				$resAssocObj->setAmount($amount);
				$resAssocObj->save();
			}

			unset($resAssocObj);
		}
	}

	$total = moneyFormat($total);
	$tag = $xmlObj->createElement('total');
	$tag->appendChild($xmlObj->createTextNode($total));
	$resultTag->appendChild($tag);

	if($action == "reserve") {
		$resObj->setAmount($total);
		$resObj->save();

		$tag = $xmlObj->createElement('reservation_number');
		$tag->appendChild($xmlObj->createTextNode($resObj->id));
		$resultTag->appendChild($tag);

		//Send Merchant Notification
		// $resObj->sendMerchantNotification();
		$mail_result = $resObj->sendMerchantNotification(array('email_restypes'=>$email_restypes,
																'cart_summary'=>$summary,
																));
		if($mail_result['success']===false) {
			array_push($errormsg,$mail_result['message']);
		}


	}

	header ("Content-Type:text/xml");
	
	echo $xmlObj->saveXML();
}


?>