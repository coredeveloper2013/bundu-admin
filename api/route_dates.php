<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/route_dates.php

require_once '../common.inc.php';
require_once 'auth.inc.php';


$start_date = mysqlDate($_REQUEST['start_date']);
	if(!is_date($start_date)) {
		$start_date = mysqlDate('today');
	}
$end_date = mysqlDate($_REQUEST['end_date']);
	if(!is_date($end_date)) {
		$end_date = mysqlDate('today + 1 week');
	}


$routeDateIDs = array();
$query = 'SELECT routes_dates.id
			FROM routes_dates
			JOIN routes AS r ON r.id = routes_dates.routeid
			WHERE (
					(date_v2 >= "'.encodeSQL($start_date).'" AND date_v2 <= "'.encodeSQL($end_date).'")
					OR (date >= "'.encodeSQL(strtotime($start_date)).'" AND date <= "'.encodeSQL(strtotime($end_date)).'")
				)';
				$routeIDs = make_array(@$_REQUEST['routeIDs']);
				if(@$_REQUEST['id_route'] != "") {
					$routeIDs[] = $_REQUEST['id_route'];
				}
				$routeIDs = arrayPrepForSQL($routeIDs);
				if(count($routeIDs) > 0) {
					$query .= ' AND routeid IN ("'.implode('", "', $routeIDs).'")';
				}
				$query .= ' AND r.bbincl != "n"';
$result = mysqlQuery($query);
while($row = @mysql_fetch_assoc($result)) {
	$routeDateIDs[] = $row['id'];
}

$summary = array(
	'found' => 0,
	'earliest' => false,
	'latest' => false
	);

$xmlObj = new DOMDocument();
$xmlObj->formatOutput = true;
$xmlObj->encoding = 'UTF-8';

$resultTag = $xmlObj->createElement('result');
$xmlObj->appendChild($resultTag);


foreach($routeDateIDs as $id) {
	$routeDateObj = new route_date($id);
	$routeDateObj->setupRouteObj();
	$rdcObj = new route_date_confirmed(array(
		'id_route' => $routeDateObj->getIDroute(),
		'date' => $routeDateObj->getDate()
		));

	//Hide date?
	if($routeDateObj->routeObj->getAPIinclude() == "n") {
		//When set to never
		continue;
	}
	if($routeDateObj->routeObj->getAPIinclude() == "c" && $rdcObj->id == "") {
		//When set to only include with confirmed tours
		continue;
	}

	$seats_total = $routeDateObj->calcNumSeats();
	$query = 'SELECT SUM(seats) FROM blocks WHERE id_route = "'.encodeSQL($routeDateObj->getIDroute()).'" AND date = "'.encodeSQL($routeDateObj->getDate()).'"';
	$blocked = getMysqlValue($query);
	$seats_avail = ($seats_total - $blocked);

	if($seats_avail < 1 && @$_REQUEST['show'] != "all") {
		continue;
	}

	$routeDateTag = $xmlObj->createElement('route_date');

	$tag = $xmlObj->createElement('id');
	$tag->appendChild($xmlObj->createTextNode($id));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('id_route');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->getIDroute()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('date');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->getDate()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('departure_time');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->calcDepTime()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('departure_timezone');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->routeObj->getDepTimezone()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('arrival_time');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->calcArrTime()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('arrival_timezone');
	$tag->appendChild($xmlObj->createTextNode($routeDateObj->routeObj->getArrTimezone()));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('seats_available');
	$tag->appendChild($xmlObj->createTextNode($seats_avail));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('seats_total');
	$tag->appendChild($xmlObj->createTextNode($seats_total));
	$routeDateTag->appendChild($tag);

	$confirmed = 0;
	if($rdcObj->id > 0) {
		$confirmed = 1;
	}
	$tag = $xmlObj->createElement('confirmed');
	$tag->appendChild($xmlObj->createTextNode($confirmed));
	$routeDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('id_tour_date');
	$tag->appendChild($xmlObj->createTextNode($rdcObj->getIDtourDate()));
	$routeDateTag->appendChild($tag);

	$resultTag->appendChild($routeDateTag);

	$summary['found']++;
	if($summary['earliest'] === false || $summary['earliest'] > $routeDateObj->getDate()) {
		$summary['earliest'] = $routeDateObj->getDate();
	}
	if($summary['latest'] === false || $summary['latest'] < $routeDateObj->getDate()) {
		$summary['latest'] = $routeDateObj->getDate();
	}
}

$summaryTag = $xmlObj->createElement('summary');
foreach($summary as $key => $val) {
	$tag = $xmlObj->createElement($key);
	$tag->appendChild($xmlObj->createTextNode($val));
	$summaryTag->appendChild($tag);
}
$resultTag->appendChild($summaryTag);


header ("Content-Type:text/xml");

echo $xmlObj->saveXML();

?>