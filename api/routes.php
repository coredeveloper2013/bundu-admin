<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/routes.php

require_once '../common.inc.php';
require_once 'auth.inc.php';


$summary = array(
	'found' => 0
	);

$xmlObj = new DOMDocument();
$xmlObj->formatOutput = true;
$xmlObj->encoding = 'UTF-8';

$resultTag = $xmlObj->createElement('result');
$xmlObj->appendChild($resultTag);

$query = 'SELECT id
			FROM routes
			WHERE hidden = 0 AND bbincl != "n"
			ORDER BY routes.id ASC';
$result = mysqlQuery($query);
while($row = @mysql_fetch_assoc($result)) {
	$routeObj = new route($row['id']);

	$routeTag = $xmlObj->createElement('route');

	$tag = $xmlObj->createElement('id');
	$tag->appendChild($xmlObj->createTextNode($row['id']));
	$routeTag->appendChild($tag);

	$tag = $xmlObj->createElement('name');
	$tag->appendChild($xmlObj->createTextNode($routeObj->getName()));
	$routeTag->appendChild($tag);

	$tag = $xmlObj->createElement('depart');
	$tag->appendChild($xmlObj->createTextNode($routeObj->getDepLocationName()));
	$routeTag->appendChild($tag);

	$tag = $xmlObj->createElement('arrive');
	$tag->appendChild($xmlObj->createTextNode($routeObj->getArrLocationName()));
	$routeTag->appendChild($tag);

	$tag = $xmlObj->createElement('details');
	$tag->appendChild($xmlObj->createTextNode($routeObj->getDetails()));
	$routeTag->appendChild($tag);

	$tag = $xmlObj->createElement('miles');
	$tag->appendChild($xmlObj->createTextNode($routeObj->getMiles()));
	$routeTag->appendChild($tag);

	$resultTag->appendChild($routeTag);

	$summary['found']++;
}

$summaryTag = $xmlObj->createElement('summary');
foreach($summary as $key => $val) {
	$tag = $xmlObj->createElement($key);
	$tag->appendChild($xmlObj->createTextNode($val));
	$summaryTag->appendChild($tag);
}
$resultTag->appendChild($summaryTag);


header ("Content-Type:text/xml");

echo $xmlObj->saveXML();

?>