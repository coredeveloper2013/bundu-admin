<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/tour_dates.php

require_once '../common.inc.php';
require_once 'auth.inc.php';
//require_once '../getblocked.php';


$start_date = mysqlDate($_REQUEST['start_date']);
	if(!is_date($start_date)) {
		$start_date = mysqlDate('today');
	}
$end_date = mysqlDate($_REQUEST['end_date']);
	if(!is_date($end_date)) {
		$end_date = mysqlDate('today + 1 week');
	}


$tourDateIDs = array();
$query = 'SELECT id
			FROM tours_dates
			WHERE (
					(date_v2 >= "'.encodeSQL($start_date).'" AND date_v2 <= "'.encodeSQL($end_date).'")
					OR (date >= "'.encodeSQL(strtotime($start_date)).'" AND date <= "'.encodeSQL(strtotime($end_date)).'")
				)';
				$tourIDs = make_array(@$_REQUEST['tourIDs']);
				if(@$_REQUEST['id_tour'] != "") {
					$tourIDs[] = $_REQUEST['id_tour'];
				}
				$tourIDs = arrayPrepForSQL($tourIDs);
				if(count($tourIDs) > 0) {
					$query .= ' AND tourid IN ("'.implode('", "', $tourIDs).'")';
				}
$result = mysqlQuery($query);
while($row = @mysql_fetch_assoc($result)) {
	$tourDateIDs[] = $row['id'];
}


/*
	For each tour date
		- Collect how many seats total - MIN() of available seats for tour routes?
		- Subtract seats blocked
	Only display tours with available seats
	Optionally (through param), display all dates
*/


//$blocked = getblocked_tours('*', strtotime($start_date), strtotime($end_date));
//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blocked,true)).'</PRE>';


$summary = array(
	'found' => 0,
	'earliest' => false,
	'latest' => false
	);

$xmlObj = new DOMDocument();
$xmlObj->formatOutput = true;
$xmlObj->encoding = 'UTF-8';

$resultTag = $xmlObj->createElement('result');
$xmlObj->appendChild($resultTag);


foreach($tourDateIDs as $id) {
	$tourDateObj = new tour_date($id);
	$seats_total = $tourDateObj->getSeatsCache();
	$seats_avail = ($seats_total - $tourDateObj->getBlockedCache());

	if($seats_avail < 1 && @$_REQUEST['show'] != "all") {
		continue;
	}

	$tourDateTag = $xmlObj->createElement('tour_date');

	$tag = $xmlObj->createElement('id');
	$tag->appendChild($xmlObj->createTextNode($id));
	$tourDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('id_tour');
	$tag->appendChild($xmlObj->createTextNode($tourDateObj->getIDtour()));
	$tourDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('date');
	$tag->appendChild($xmlObj->createTextNode($tourDateObj->getDate()));
	$tourDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('seats_available');
	$tag->appendChild($xmlObj->createTextNode($seats_avail));
	$tourDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('seats_total');
	$tag->appendChild($xmlObj->createTextNode($seats_total));
	$tourDateTag->appendChild($tag);

	$tag = $xmlObj->createElement('confirmed');
	$tag->appendChild($xmlObj->createTextNode($tourDateObj->getConfirmed()));
	$tourDateTag->appendChild($tag);

	$resultTag->appendChild($tourDateTag);

	$summary['found']++;
	if($summary['earliest'] === false || $summary['earliest'] > $tourDateObj->getDate()) {
		$summary['earliest'] = $tourDateObj->getDate();
	}
	if($summary['latest'] === false || $summary['latest'] < $tourDateObj->getDate()) {
		$summary['latest'] = $tourDateObj->getDate();
	}
}

$summaryTag = $xmlObj->createElement('summary');
foreach($summary as $key => $val) {
	$tag = $xmlObj->createElement($key);
	$tag->appendChild($xmlObj->createTextNode($val));
	$summaryTag->appendChild($tag);
}
$resultTag->appendChild($summaryTag);


header ("Content-Type:text/xml");

echo $xmlObj->saveXML();

?>