<?php

// Author: Dominick Bernal - bernalwebservices.com
// http://bundubashers.com/staging/api/tours.php

require_once '../common.inc.php';
require_once 'auth.inc.php';


$summary = array(
	'found' => 0
	);

$xmlObj = new DOMDocument();
$xmlObj->formatOutput = true;
$xmlObj->encoding = 'UTF-8';

$resultTag = $xmlObj->createElement('result');
$xmlObj->appendChild($resultTag);


$query = 'SELECT id, alias, title, nickname AS short_title, numdays AS num_days, short_desc AS short_description,
				scity AS starting_city, pricingdesc AS pricing_description, highlights, details, pleasenote AS please_note,
				similar, getweights AS weights_needed, getlunch AS lunch_pref_needed
			FROM tours
			WHERE hidden = 0 AND archived = 0'."\n";
			$tourIDs = make_array(@$_REQUEST['tourIDs']);
			if(@$_REQUEST['id_tour'] != "") {
				$tourIDs[] = $_REQUEST['id_tour'];
			}
			$tourIDs = arrayPrepForSQL($tourIDs);
			if(count($tourIDs) > 0) {
				$query .= ' AND id IN ("'.implode('", "', $tourIDs).'")';
			}
			$query .= 'ORDER BY tours.id ASC';
$result = mysqlQuery($query);
while($row = @mysql_fetch_assoc($result)) {
	$tourObj = new tour($row['id']);

	$tourTag = $xmlObj->createElement('tour');

	$tag = $xmlObj->createElement('id');
	$tag->appendChild($xmlObj->createTextNode($row['id']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('url');
	$tag->appendChild($xmlObj->createTextNode('http://bundubashers.com/tour.php?id='.$row['alias']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('title');
	$tag->appendChild($xmlObj->createTextNode($row['title']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('short_title');
	$tag->appendChild($xmlObj->createTextNode($row['short_title']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('num_days');
	$tag->appendChild($xmlObj->createTextNode($row['num_days']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('short_description');
	$tag->appendChild($xmlObj->createTextNode($row['short_description']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('starting_city');
	$tag->appendChild($xmlObj->createTextNode($row['starting_city']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('pricing_description');
	$tag->appendChild($xmlObj->createTextNode($row['pricing_description']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('highlights');
	$tag->appendChild($xmlObj->createTextNode($row['highlights']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('details');
	$tag->appendChild($xmlObj->createTextNode($row['details']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('please_note');
	$tag->appendChild($xmlObj->createTextNode($row['please_note']));
	$tourTag->appendChild($tag);

	$similarTag = $xmlObj->createElement('similar');
	$simIDs = explode('|', $row['similar']);
	$simIDs = array_filter($simIDs);
	$simIDs = array_unique($simIDs);
	foreach($simIDs as $id) {
		$tag = $xmlObj->createElement('id');
		$tag->appendChild($xmlObj->createTextNode($id));
		$similarTag->appendChild($tag);
	}
	$tourTag->appendChild($similarTag);

	$tag = $xmlObj->createElement('weights_needed');
	$tag->appendChild($xmlObj->createTextNode($row['weights_needed']));
	$tourTag->appendChild($tag);

	$tag = $xmlObj->createElement('lunch_pref_needed');
	$tag->appendChild($xmlObj->createTextNode($row['lunch_pref_needed']));
	$tourTag->appendChild($tag);

	$activitiesTag = $xmlObj->createElement('activities');
	$acts = $tourObj->get_steps(array('type'=>'a'));
	foreach($acts['f'] as $act) {
		$actObj = new activity($act['typeid']);

		$activityTag = $xmlObj->createElement('activity');

		$tag = $xmlObj->createElement('id');
		$tag->appendChild($xmlObj->createTextNode($act['typeid']));
		$activityTag->appendChild($tag);

		$tag = $xmlObj->createElement('name');
		$tag->appendChild($xmlObj->createTextNode($actObj->getName()));
		$activityTag->appendChild($tag);

		$tag = $xmlObj->createElement('tour_day');
		$tag->appendChild($xmlObj->createTextNode($act['day']));
		$activityTag->appendChild($tag);

		$tag = $xmlObj->createElement('amount');
		$tag->appendChild($xmlObj->createTextNode($actObj->getPrice()));
		$activityTag->appendChild($tag);

		$activitiesTag->appendChild($activityTag);

		$actObj = null;
	}
	$tourTag->appendChild($activitiesTag);

	$resultTag->appendChild($tourTag);

	$summary['found']++;
}

$summaryTag = $xmlObj->createElement('summary');
foreach($summary as $key => $val) {
	$tag = $xmlObj->createElement($key);
	$tag->appendChild($xmlObj->createTextNode($val));
	$summaryTag->appendChild($tag);
}
$resultTag->appendChild($summaryTag);


header ("Content-Type:text/xml");

echo $xmlObj->saveXML();

?>