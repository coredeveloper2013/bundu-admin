<?  // This custom utility created by Dominick Bernal - www.bernalwebservices.com

require_once('../common.inc.php');

//error_reporting('E_ALL'); ini_set('display_errors','1');

if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	$redirect = 'https://'.$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];
	if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != ""){ $redirect .= '?'.$_SERVER['QUERY_STRING']; }
	//header('Location: '.$redirect);
	//exit;
}

session_set_cookie_params(36000);
session_start();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();


if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == "y"){
	unset($_SESSION['bb_member']);
	if(isset($_SESSION['cart_legs'])): unset($_SESSION['cart_legs']); endif;
	array_push($pubsuccessmsg,'You have been logged out.');
}

if(isset($_POST['submit']) && $_POST['submit'] == "Log in"){
	$_SESSION['bb_member']['username'] = trim($_POST['username']);
	$_SESSION['bb_member']['password'] = trim($_POST['password']);
}


//!Locations & Timezones
$locs = array();
$loc_tzs = array();
	$query = 'SELECT `id`,`name`,`timezone_id` FROM `locations`';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$locs['l'.$row['id']] = $row['name'];
		$loc_tzs['l'.$row['id']] = $row['timezone_id'];
		}
	//echo '<PRE STYLE="text-align:left;">'; print_r($loc_tzs); echo '</PRE>';

function tmz_offset($dep_loc=1,$arr_loc=1,$datetime="now"){
	global $loc_tzs;

	if(!isset($loc_tzs['l'.$dep_loc])){	$loc_tzs['l'.$dep_loc] = date_default_timezone_get(); }
	if(!isset($loc_tzs['l'.$arr_loc])){ $loc_tzs['l'.$arr_loc] = date_default_timezone_get(); }

    $dep_tz = new DateTimeZone($loc_tzs['l'.$dep_loc]);
    $arr_tz = new DateTimeZone($loc_tzs['l'.$arr_loc]);

    $dep_dt = new DateTime($datetime,$dep_tz);
    $arr_dt = new DateTime($datetime,$arr_tz);

    $offset = ($arr_tz->getOffset($arr_dt) - $dep_tz->getOffset($dep_dt));
    return $offset;
	}

//GET DEFAULT SETTINGS
$defaults = array();
$query = 'SELECT `setting`,`var` FROM `bundubus_settings`';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$defaults[$row['setting']] = $row['var'];
	}
if(!isset($defaults['chgcutoff']) || !is_numeric($defaults['chgcutoff'])){ $defaults['chgcutoff'] = 48; }
$defaults['chgcutoff_secs'] = ($defaults['chgcutoff']*3600);


//! HOW DO I DEAL WITH PLAIN TEXT PASSWORD?  LEAVE IT?  ENCRYPT IT?  WHAT?
if(isset($_SESSION['bb_member'])){
	$query = 'SELECT reservations_assoc.`username`, reservations_assoc.`password`, reservations_assoc.`name`, reservations.`phone_homebus`, reservations.`phone_cell`, reservations.`cell_country`, reservations.`email` FROM `reservations`,`reservations_assoc` WHERE reservations.`id` = reservations_assoc.`reservation`';
		$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
		$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
		$query .= ' ORDER BY reservations_assoc.`date` DESC LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		if($num_results < 1){
		array_push($puberrormsg,"Your user name and/or password is incorrect.  Please try again.");
		unset($_SESSION['bb_member']);
		} elseif($num_results == 1){
		$_SESSION['bb_member'] = mysql_fetch_assoc($result);
		if(isset($_POST['remuser']) && $_POST['remuser'] == "y"){
			setcookie("BBHOLogin",$_SESSION['bb_member']['username'],(time()+31536000),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"]);
			} elseif(isset($_POST['submit']) && $_POST['submit'] == "Log in" && isset($_COOKIE['BBHOLogin'])) {
			setcookie("BBHOLogin",'',time(),dirname($_SERVER["PHP_SELF"]),$_SERVER["HTTP_HOST"]);
			} //End Set Cookie
		}
	}

//echo '<PRE STYLE="text-align:left;">'; print_r($_SESSION['bb_member']); echo '</PRE>';


$pagetitle = 'Bundu Bus Login';
include_once('../header_reserve.php');

$today = strtotime("today");

echo '<CENTER><BR>'."\n\n";


printmsgs($pubsuccessmsg,$puberrormsg);


//$_SESSION['bb_member']


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

if(!isset($_SESSION['bb_member'])){  //!LOGIN *************************************************************************

echo '<BODY onLoad="javascript:document.loginform.';
	if(isset($_COOKIE['BBHOLogin']) && $_COOKIE['BBHOLogin'] != ""): echo 'password'; else: echo 'username'; endif;
	echo '.focus();">'."\n\n";

echo '<FORM NAME="loginform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	if(isset($_REQUEST['hoho']) && $_REQUEST['hoho'] != ""){ echo '<INPUT TYPE="hidden" NAME="hoho" VALUE="'.$_REQUEST['hoho'].'">'."\n"; }
	if(isset($_REQUEST['miniroute']) && $_REQUEST['miniroute'] != ""){ echo '<INPUT TYPE="hidden" NAME="miniroute" VALUE="'.$_REQUEST['miniroute'].'">'."\n"; }

	echo '	<TABLE BORDER="0"CELLPADDING="0" CELLSPACING="2">'."\n";
	echo '	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Email address').' / '.gettrans('User name').'</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:200px;" NAME="username" ID="username" VALUE="';
		if(isset($_COOKIE['BBHOLogin']) && $_COOKIE['BBHOLogin'] != ""): echo $_COOKIE['BBHOLogin']; endif;
		echo '"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('Password').'</B>&nbsp;</FONT></TD><TD><INPUT TYPE="password" SIZE="30" STYLE="width:200px;" NAME="password" ID="password" VALUE=""></TD></TR>'."\n";
	echo '	<TR><TD></TD><TD><A HREF="forgotpwd.php" STYLE="font-family:Arial; font-size:9pt; font-style:italic;">Forgot password?</A></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="remuser" VALUE="y"';
		if(isset($_COOKIE['BBHOLogin']) && $_COOKIE['BBHOLogin'] != ""): echo ' CHECKED'; endif;
		echo ' ID="u"></TD><TD><FONT FACE="Arial" SIZE="2"><label for="u">Remember my user name</label></FONT></TD></TR>'."\n";
	echo '	<TR><TD></TD><TD><INPUT TYPE="submit" NAME="submit" VALUE="Log in" STYLE="width:100px;"></TD></TR>'."\n";
	echo '	</TABLE><BR>'."\n\n";

//echo '<SPAN STYLE="font-family:Arial; font-size:10pt;"><A HREF="forgotpwd.php">I forgot my password.</A></SPAN><BR><BR>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:12pt;"><A HREF="hopon.php" STYLE="color:#CC0000;"><I><B>Not registered?</B> Click here to register.</I></A></SPAN>'."\n\n";

echo '</FORM>'."\n";


} else { //!LOGGED IN **************************************************************************************************


//GET BUNDU BUS STOPS
$stops = array();
$query = 'SELECT * FROM `locations` ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$stops['s'.$row['id']] = $row;
	}

//GET TICKET TYPES
$ticket_types = array();
$dtype = 0;
$query = 'SELECT * FROM `bundubus_hopon` WHERE `flexible` = 0 ORDER BY `days` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
		$row['id'] = 'h:'.$row['id'];
		$row['title'] = 'Hop On, Hop Off Pass: '.$row['title'];
	$ticket_types[$row['id']] = $row;
	if($i==0){ $dtype = $row['id']; }
	}
$query = 'SELECT * FROM `miniroutes` ORDER BY `title` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
		$row['id'] = 'm:'.$row['id'];
		$row['title'] = 'Mini Route Pass: '.$row['title'];
		$row['days'] = 10;
	$ticket_types[$row['id']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($ticket_types); echo '</PRE>';

//TICKET LIST
$ticketlist = array();
$query = 'SELECT reservations_assoc.* FROM `reservations`,`reservations_assoc` WHERE reservations.`id` = reservations_assoc.`reservation`';
	$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
	$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
	$query .= ' ORDER BY `id` DESC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($ticketlist,$row['reservation']);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($ticketlist); echo '</PRE>';


if(isset($_POST['utaction']) && $_POST['utaction'] == "*newticket*"){

if(!isset($_POST['acceptterms']) || $_POST['acceptterms'] != "I agree"){
	array_push($puberrormsg,'You must accept the terms and conditions by checking the appropriate box near the end of the form to complete your reservation.');
	}

$checkfields = array(
	"type" => "choice of pass",
	"cc_num" => "credit card number",
	"cc_expdate_month" => "credit card expiration month",
	"cc_expdate_year" => "credit card expiration year",
	"cc_scode" => "credit card security code"
	);

	foreach($checkfields as $key => $desc){
	if(!isset($_POST[$key]) || trim($_POST[$key]) == ""){
		array_push($puberrormsg,'Please enter your '.$desc.' in the form below.');
		}
	}

	if(isset($_POST['cc_num']) && $_POST['cc_num'] != "" && substr($_POST['cc_num'],0,1) == "6"){
		array_push($puberrormsg,'Credit card number is invalid.  Please enter a valid Visa, MasterCard or American Express card number.');
		}

	if(count($puberrormsg) > 0){
		$_REQUEST['ticket'] = "*new*";
		$_SESSION['nodup'] = $time;
		$_POST['utaction'] = "none";
		$fillform = $_POST;
		}

	printmsgs($pubsuccessmsg,$puberrormsg);
}



//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo $_SESSION['ticket_nodup'].'<BR>'.$_POST['nodup'].'<BR><BR>';

if(isset($_POST['utaction']) && $_POST['utaction'] == "*newhoho*" && isset($_POST['nodup']) && $_POST['nodup'] == $_SESSION['nodup']){  //! PROCESS NEW HOHO PASS **********************************************************

	//$_POST['name'] = trim($_POST['firstname']).' '.trim($_POST['lastname']);
	$_POST['cc_num'] = trim($_POST['cc_num']);
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];
	if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $paymethod = 'Alternate'; else: $paymethod = 'C/C'; endif;

	//NEW RESERVATION
	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_zip`,`pay_method`,`comments`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.$_SESSION['bb_member']['name'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['cell_country'].'","'.$_SESSION['bb_member']['email'].'","'.$ticket_types[$_POST['type']]['price'].'","'.$_SESSION['bb_member']['name'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['cc_zip'].'","'.$paymethod.'","'.$_POST['comments'].'","Website","'.$time.'")';
		@mysql_query($query);
	$newid = mysql_insert_id();
	$thiserror = mysql_error();
	if($thiserror != ""){

		array_push($puberrormsg,'There was an error creating your new pass.  Please contact us to complete your purchase.');
		array_push($errormsg,$thiserror);

	} else {

		$t = explode(':',$_POST['type']);
		if($t[0] == "m"){
			$thoho = '';
			$tminiroute = $t[1];
			} else {
			$thoho = $t[1];
			$tminiroute = '';
			}

		$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_time`,`arr_time`,`amount`,`bbticket`,`miniroute`,`username`,`password`,`days`)';
			$query .= ' VALUES("'.$newid.'","'.$ticket_types[$_POST['type']]['title'].'","'.$t[0].'","'.$_SESSION['bb_member']['name'].'",1,0,0,'.time().','.time().','.time().',"'.$ticket_types[$_POST['type']]['price'].'","'.$thoho.'","'.$tminiroute.'","'.trim($_SESSION['bb_member']['email']).'","'.trim($_SESSION['bb_member']['password']).'","'.$ticket_types[$_POST['type']]['days'].'")';
			//echo $query.'<BR>';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){ array_push($errormsg,$thiserror); }

		array_push($pubsuccessmsg,'You have created a Hop On, Hop Off Pass! Your new pass number is <B>'.$newid.'</B>.');
		array_push($successmsg,'Saved new Bundu Bus Hop On/Off Pass: <B>'.$newid.'</B> for '.$_SESSION['bb_member']['name']);

		$message = '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
		$message .= $_SESSION['bb_member']['name'].' has purchased a new HOHO Pass:<BR><BR>'."\n\n";
		$message .= 'Pass Id: <B>'.$newid.'</B><BR>'."\n";
		$message .= 'Pass Title: '.$ticket_types[$_POST['type']]['title'].'<BR>'."\n";
		$message .= 'Days: '.$ticket_types[$_POST['type']]['days'].'<BR>'."\n";
		$message .= 'Price: '.$ticket_types[$_POST['type']]['price'].'<BR>'."\n";
		$message .= 'Comments: '.$_POST['comments'].'<BR>'."\n";
		$message .= '</SPAN>'."\n";

		if(count($errormsg) > 0){ $message .= '<BR>The following errors occurred: '.implode("<BR>\n",$errormsg); }

		//SEND MERCHANT NOTIFICATION
		$heading = 'New HOHO Pass #'.$newid;
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "To: Bundu Bus <yellowstonetours@gmail.com>\r\n"; //".$defaults['notaddress']."
		$headers .= "From: ".$_SESSION['bb_member']['name']." <".$_SESSION['bb_member']['email'].">\r\n";
		@mail("", $heading, $message, $headers);

		$_SESSION['nodup'] = '';

		//Temp - run this so blocks get created
		$resObj = new reservation($newid);
		if($resObj->id > 0) {
			$resObj->save();
		}
	}

	$_POST['utaction'] = 'complete';

	printmsgs($pubsuccessmsg,$puberrormsg);

//echo '<PRE>'; print_r($successmsg); echo '</PRE>';
//echo '<PRE>'; print_r($errormsg); echo '</PRE>';

} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "addroutes" && isset($_POST['nodup']) && $_POST['nodup'] == $_SESSION['nodup']){  //! MAKE CHANGES *****************************************************************


//Ticket info
$rticket = array();
$query = 'SELECT reservations_assoc.* FROM `reservations`,`reservations_assoc` WHERE reservations.`id` = reservations_assoc.`reservation`';
	$query .= ' AND reservations_assoc.`id` = "'.$_POST['bbticket'].'"';
	$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
	$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
	$query .= ' ORDER BY `id` DESC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	$rticket = mysql_fetch_assoc($result);
	//echo '<PRE STYLE="text-align:left;">'; print_r($rticket); echo '</PRE>';

$message = '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
	$message .= 'These changes were made by '.$_SESSION['bb_member']['name'].' in the Bundu Bus Login.<BR><BR>'."\n\n";
	if(isset($rticket) && count($rticket) > 0){
		if($rticket['type'] == "h"){ $message .= 'Hop On, Hop Off Pass '; } elseif($rticket['type'] == "m"){ $message .= 'Mini Route Pass '; }
		$message .= '#'.$rticket['reservation'].'<BR><BR>'."\n\n";
		}

//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';

$numcanceled = 0; $amtcanceled = 0;
$numadded = 0; $amtadded = 0;

//CANCEL TRIPS
if(isset($_POST['remove']) && count($_POST['remove']) > 0){
	//Gather info
	$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations`,`reservations_assoc`,`routes` WHERE';
		$query .= ' reservations_assoc.`routeid` = routes.`id` AND reservations_assoc.`reservation` = reservations.`id`';
		$query .= ' AND (reservations_assoc.`id` = "'.implode('" OR reservations_assoc.`id` = "',$_POST['remove']).'")';
		//$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
		$query .= ' AND (reservations_assoc.`reservation` = "'.implode('" OR reservations_assoc.`reservation` = "',$ticketlist).'")';
		$query .= ' ORDER BY `id` DESC';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);

	$remove = array();
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		array_push($remove,$row);
		}

	} //End $_POST['remove'] if statement

if(isset($remove) && count($remove) > 0){
	$message .= 'These routes have been canceled:<BR>'."\n";
	$message .= '<TABLE BORDER="0">'."\n";
	foreach($remove as $row){
		$query = 'UPDATE `reservations_assoc` SET `canceled` = "'.$time.'" WHERE `id` = "'.$row['tripid'].'" LIMIT 1';
		@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror != ""){
			array_push($errormsg,$thiserror);
			array_push($puberrormsg,'There was an error canceling your trip.  Please contact us to complete your cancellation.');
			} else {
			$numcanceled++;
			$amtcanceled = ($amtcanceled+$row['amount']);
			array_push($successmsg,'Canceled subreservation '.$row['tripid'].' for reservation '.$row['reservation'].'.');
			if(is_numeric($row['dep_loc'])){ $row['dep_loc'] = $stops['s'.$row['dep_loc']]['name']; }
			if(is_numeric($row['arr_loc'])){ $row['arr_loc'] = $stops['s'.$row['arr_loc']]['name']; }
			$message .= '<TR>';
			$message .= '<TD ALIGN="left" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$row['name'].'</TD>';
			$message .= '<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$row['reservation'].'</TD>';
			$message .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px; white-space:nowrap;">'.date("D M j, Y",$row['date']).'</TD>';
			$message .= '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:6px; padding-right:6px;">'.date("g:ia",$row['dep_time']).' '.$row['dep_loc'].' to '.date("g:ia",$row['arr_time']).' '.$row['arr_loc'].'</TD>';
			if($row['amount'] > 0){ $message .= '<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:6px;">- $'.$row['amount'].'</TD>'; }
			$message .= '</TR>'."\n";
			}
		} //End Foreach
	$message .= '</TABLE><BR>'."\n\n";

	if($numcanceled > 0){ array_push($pubsuccessmsg,$numcanceled.' routes were canceled.'); }

	} //End $remove if statement

//ADD TRIPS
if(isset($_POST['routeid']) && count($_POST['routeid']) > 0){

	$amounts = array();
	foreach($_POST['routeid'] as $key => $routeid){
		//Get charge amount
		if(isset($_POST['bbticket']) && $_POST['bbticket'] != ""){
		$amount = 0;
		} else {
		$query = 'SELECT * FROM `routes` WHERE `id` = "'.$routeid.'" LIMIT 1';
			$result = @mysql_query($query);
			$num_results = @mysql_num_rows($result);
			$routeinfo = mysql_fetch_assoc($result);
			if($routeinfo['bbprice'] > 0){
				$amount = ($routeinfo['bbprice']*$_POST['pax'][$key]);
				} else {
				$amount = (($defaults['cpm']*$routeinfo['miles'])*$_POST['pax'][$key]);
				}
			} //End Ticket If
		$amtadded = ($amtadded+$amount);
		$amounts[$key] = $amount;
		} //End ForEach
		//echo '<PRE>'; print_r($amounts); echo '</PRE>';

	$total = ($amtadded - $amtcanceled);

	if(!isset($rticket['reservation']) || $rticket['reservation'] == "" || $rticket['reservation'] == 0){
		array_push($puberrormsg,'There was an error reserving your routes.  Please contact us to complete your reservation.');
		array_push($errormsg,'Error reserving routes.  Requested pass could not be found.');
		}

	if(count($errormsg) == 0 && count($puberrormsg) == 0){
	$message .= 'These routes have been reserved:<BR>'."\n";
	$message .= '<TABLE BORDER="0">'."\n";

	foreach($_POST['routeid'] as $key => $routeid){
		//Get route info
		$query = 'SELECT routes_dates.`id`, routes_dates.`routeid`, routes_dates.`date`, routes.`dep_loc`, IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) AS `dep_time`, routes.`arr_loc`, IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) AS `arr_time`, routes.`travel_time`, routes.`anchor`, routes.`vendor`';
			$query .= ' FROM `routes`,`routes_dates`';
			$query .= ' WHERE routes.`id` = routes_dates.`routeid`';
			$query .= ' AND routes.`id` = "'.$routeid.'"';
			$query .= ' AND routes_dates.`date` >= ('.$_POST['date'][$key].' - 3600)';
			$query .= ' AND routes_dates.`date` <= ('.$_POST['date'][$key].' + 3600)';
			$result = mysql_query($query);
			$routeinfo = mysql_fetch_assoc($result);
			//echo '<PRE STYLE="text-align:left;">'; print_r($routeinfo); echo '</PRE>';

		$datestr = date("F j, Y",$routeinfo['date']);
		if($routeinfo['anchor'] == 'arr'){
			$routeinfo['arr_time'] = strtotime($datestr.' '.date("H:i",(1262329200+$routeinfo['arr_time'])));
			$routeinfo['dep_time'] = ($routeinfo['arr_time']-$routeinfo['travel_time']+tmz_offset($routeinfo['arr_loc'],$routeinfo['dep_loc'],($datestr.' '.date("H:i",$routeinfo['arr_time']-$routeinfo['travel_time']))));
			} else {
			$routeinfo['dep_time'] = strtotime($datestr.' '.date("H:i",(1262329200+$routeinfo['dep_time'])));
			$routeinfo['arr_time'] = ($routeinfo['dep_time']+$routeinfo['travel_time']+tmz_offset($routeinfo['dep_loc'],$routeinfo['arr_loc'],($datestr.' '.date("H:i",$routeinfo['dep_time']+$routeinfo['travel_time']))));
			}

		//Insert into assoc table
		$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`amount`,`routeid`,`bbticket`,`vendor`)';
			$query .= ' VALUES("'.$rticket['reservation'].'","'.$_POST['title'][$key].'","r","'.$_SESSION['bb_member']['name'].'","'.$_POST['pax'][$key].'",0,0,"'.$_POST['date'][$key].'"';
			$query .= ','.$routeinfo['dep_loc'].','.$routeinfo['dep_time'].','.$routeinfo['arr_loc'].','.$routeinfo['arr_time'].',"'.$amounts[$key].'","'.$_POST['routeid'][$key].'","'.$_POST['bbticket'].'",'.$routeinfo['vendor'].')';
			@mysql_query($query);
			//echo $query.'<BR>';
		$thiserror = mysql_error();
		if($thiserror != ""){
			array_push($errormsg,$thiserror);
			array_push($puberrormsg,'There was an error reserving your routes.  Please contact us to complete your reservation.');
			} else {
			$numadded++;
			array_push($successmsg,'Saved subreservation '.mysql_insert_id().' for reservation '.$rticket['reservation'].'.');
			$message .= '<TR>';
			$message .= '<TD ALIGN="left" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$_SESSION['bb_member']['name'].'</TD>';
			$message .= '<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$rticket['reservation'].'</TD>';
			$message .= '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px; white-space:nowrap;">'.date("D M j, Y",$_POST['date'][$key]).'</TD>';
			$message .= '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:6px; padding-right:6px;">'.$_POST['title'][$key].'</TD>';
			if($amounts[$key] > 0){ $message .= '<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:6px;">+ $'.number_format($amounts[$key],2,'.','').'</TD>'; }
			$message .= '</TR>'."\n";
			}
		} //End Foreach
	$message .= '</TABLE><BR>'."\n\n";

	if($numadded > 0){ array_push($pubsuccessmsg,$numadded.' routes were reserved.'); }

	} //End error if statement

	} //End $_POST['routeid'] if statement

	$costdif = ($amtadded - $amtcanceled);
	if($costdif < 0){
		$costdif = ($costdif*-1);
		array_push($pubsuccessmsg,'Your credit card will be refunded for the amount of $'.number_format($costdif,2,'.','').'.');
		$message .= 'The user\'s credit card needs to be <B>refunded</B> for the amount of $'.number_format($costdif,2,'.','').'.';
		} elseif($costdif > 0){
		array_push($pubsuccessmsg,'Your credit card will charged an additional $'.number_format($costdif,2,'.','').'.');
		$message .= 'The user\'s credit card needs to be <B>charged</B> an additional $'.number_format($costdif,2,'.','').'.';
		}

	if(count($errormsg) > 0){ $message .= '<BR>The following errors occurred: '.implode("<BR>\n",$errormsg); }

	//SEND MERCHANT NOTIFICATION
	$heading = 'Bundu Bus HOHO Pass Change';
	$headers  = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	$headers .= "To: Bundu Bus <yellowstonetours@gmail.com>\r\n"; //".$defaults['notaddress']."
	$from_addy = $_SESSION['bb_member']['name']." <".$_SESSION['bb_member']['email'].">";
	$from_addy = str_replace(array(',',';'), '', $from_addy);
	$headers .= "From: ".$from_addy."\r\n";
	@mail("", $heading, $message, $headers);

	$_SESSION['nodup'] = '';

	//printmsgs($successmsg,$errormsg);
	printmsgs($pubsuccessmsg,$puberrormsg);

	if(isset($_POST['bbticket']) && $_POST['bbticket'] != ""){
		$_REQUEST['ticket'] = $_POST['bbticket'];
		$_REQUEST['trip'] = '*new*';
		}

}



if(isset($_POST['batch']) && $_POST['batch'] != "" && isset($_POST['tripids']) && is_array($_POST['tripids'])){
	$_REQUEST[$_POST['batch']] = $_POST['tripids'];
	}
if(isset($_REQUEST['trip']) && !is_array($_REQUEST['trip'])){ $_REQUEST['trip'] = explode('|',$_REQUEST['trip']); }
if(isset($_REQUEST['cancel']) && !is_array($_REQUEST['cancel'])){ $_REQUEST['cancel'] = explode('|',$_REQUEST['cancel']); }


if(isset($_REQUEST['hoho']) && $_REQUEST['hoho'] == "*new*"){ //! NEW HOHO TICKET ********************************************************************


$fillform = array("type"=>$dtype);


?><SCRIPT><!--

function checkform(){
	var go = true;
	var msg = new Array();
	if(document.getElementById("acceptterms").checked == false){
		msg.push('You must accept the terms and conditions by checking the appropriate box near the end of the form to complete your reservation.');
		go = false;
		}
	if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
		msg.push('Please choose an expiration date for your credit card.');
		go = false;
		}
	if(document.getElementById("cc_zip").value == ""){
		msg.push('Please enter the zip or postal code associated with your credit card.');
		go = false;
		}
	if(go){
		var tmpyear;
		if(document.getElementById("cc_expdate_year").value > 96){
			tmpyear = "19" + document.getElementById("cc_expdate_year").value;
			} else if(document.getElementById("cc_expdate_year").value < 21){
			tmpyear = "20" + document.getElementById("cc_expdate_year").value;
			} else {
			msg.push('The card expiration year is not valid.');
			go = false;
			}
		tmpmonth = document.getElementById("cc_expdate_month").value;
		if(!(new CardType()).isExpiryDate(tmpyear, tmpmonth)){
			msg.push('Your credit card has already expired.');
			go = false;
			}
		card = false;
		for(var n = 0; n < Cards.length; n++){
			if(Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)){
				card = true;
				break;
				}
			}
		if(!card){
			msg.push('Your credit card number does not appear to be valid.');
			go = false;
			}
		}
	if(msg.length > 0){ alert(msg.join("\n")); }
	return go;
}

//CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
var Cards = new Array();
Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
var MasterCard = Cards[0];
Cards[1] = new CardType("VisaCard", "4", "13,16");
var VisaCard = Cards[1];
Cards[2] = new CardType("AmExCard", "34,37", "15");
var AmExCard = Cards[2];
//Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
//var DinersClubCard = Cards[3];
//Cards[4] = new CardType("DiscoverCard", "6011", "16");
//var DiscoverCard = Cards[4];
//Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
//var enRouteCard = Cards[5];
//Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
//var JCBCard = Cards[6];
var LuhnCheckSum = Cards[3] = new CardType();

/*************************************************************************\
Object CardType([String cardtype, String rules, String len, int year, 
                                        int month])
cardtype    : type of card, eg: MasterCard, Visa, etc.
rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
len         : valid length of cardnumber, eg: "16,19", "13,16".
year        : year of expiry date.
month       : month of expiry date.
eg:
var VisaCard = new CardType("Visa", "4", "16");
var AmExCard = new CardType("AmEx", "34,37", "15");
\*************************************************************************/
function CardType() {
var n;
var argv = CardType.arguments;
var argc = CardType.arguments.length;

this.objname = "object CardType";

var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

this.setCardNumber = setCardNumber;  // set CardNumber method.
this.setCardType = setCardType;  // setCardType method.
this.setLen = setLen;  // setLen method.
this.setRules = setRules;  // setRules method.
this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

this.setCardType(tmpcardtype);
this.setLen(tmplen);
this.setRules(tmprules);
if (argc > 4)
this.setExpiryDate(argv[3], argv[4]);

this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
this.getCardType = getCardType;  // getCardType method.
this.isCardNumber = isCardNumber;  // isCardNumber method.
this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
this.luhnCheck = luhnCheck;// luhnCheck method.
return this;
}

/*************************************************************************\
boolean checkCardNumber([String cardnumber, int year, int month])
return true if cardnumber pass the luhncheck and the expiry date is
valid, else return false.
\*************************************************************************/
function checkCardNumber() {
var argv = checkCardNumber.arguments;
var argc = checkCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
var year = (argc > 1) ? argv[1] : this.year;
var month = (argc > 2) ? argv[2] : this.month;

this.setCardNumber(cardnumber);
this.setExpiryDate(year, month);

if (!this.isCardNumber())
return false;
if (!this.isExpiryDate())
return false;

return true;
}
/*************************************************************************\
String getCardType()
return the cardtype.
\*************************************************************************/
function getCardType() {
return this.cardtype;
}
/*************************************************************************\
String getExpiryDate()
return the expiry date.
\*************************************************************************/
function getExpiryDate() {
return this.month + "/" + this.year;
}
/*************************************************************************\
boolean isCardNumber([String cardnumber])
return true if cardnumber pass the luhncheck and the rules, else return
false.
\*************************************************************************/
function isCardNumber() {
var argv = isCardNumber.arguments;
var argc = isCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
if (!this.luhnCheck())
return false;

for (var n = 0; n < this.len.size; n++)
if (cardnumber.toString().length == this.len[n]) {
for (var m = 0; m < this.rules.size; m++) {
var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
if (headdigit == this.rules[m])
return true;
}
return false;
}
return false;
}

/*************************************************************************\
boolean isExpiryDate([int year, int month])
return true if the date is a valid expiry date,
else return false.
\*************************************************************************/
function isExpiryDate() {
var argv = isExpiryDate.arguments;
var argc = isExpiryDate.arguments.length;

year = argc > 0 ? argv[0] : this.year;
month = argc > 1 ? argv[1] : this.month;

if (!isNum(year+""))
return false;
if (!isNum(month+""))
return false;
today = new Date();
expiry = new Date(year, month);
if (today.getTime() > expiry.getTime())
return false;
else
return true;
}

/*************************************************************************\
boolean isNum(String argvalue)
return true if argvalue contains only numeric characters,
else return false.
\*************************************************************************/
function isNum(argvalue) {
argvalue = argvalue.toString();

if (argvalue.length == 0)
return false;

for (var n = 0; n < argvalue.length; n++)
if (argvalue.substring(n, n+1) < "0" || argvalue.substring(n, n+1) > "9")
return false;

return true;
}

/*************************************************************************\
boolean luhnCheck([String CardNumber])
return true if CardNumber pass the luhn check else return false.
Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
\*************************************************************************/
function luhnCheck() {
var argv = luhnCheck.arguments;
var argc = luhnCheck.arguments.length;

var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

if (! isNum(CardNumber)) {
return false;
  }

var no_digit = CardNumber.length;
var oddoeven = no_digit & 1;
var sum = 0;

for (var count = 0; count < no_digit; count++) {
var digit = parseInt(CardNumber.charAt(count));
if (!((count & 1) ^ oddoeven)) {
digit *= 2;
if (digit > 9)
digit -= 9;
}
sum += digit;
}
if (sum % 10 == 0)
return true;
else
return false;
}

/*************************************************************************\
ArrayObject makeArray(int size)
return the array object in the size specified.
\*************************************************************************/
function makeArray(size) {
this.size = size;
return this;
}

/*************************************************************************\
CardType setCardNumber(cardnumber)
return the CardType object.
\*************************************************************************/
function setCardNumber(cardnumber) {
this.cardnumber = cardnumber;
return this;
}

/*************************************************************************\
CardType setCardType(cardtype)
return the CardType object.
\*************************************************************************/
function setCardType(cardtype) {
this.cardtype = cardtype;
return this;
}

/*************************************************************************\
CardType setExpiryDate(year, month)
return the CardType object.
\*************************************************************************/
function setExpiryDate(year, month) {
this.year = year;
this.month = month;
return this;
}

/*************************************************************************\
CardType setLen(len)
return the CardType object.
\*************************************************************************/
function setLen(len) {
// Create the len array.
if (len.length == 0 || len == null)
len = "13,14,15,16,19";

var tmplen = len;
n = 1;
while (tmplen.indexOf(",") != -1) {
tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
n++;
}
this.len = new makeArray(n);
n = 0;
while (len.indexOf(",") != -1) {
var tmpstr = len.substring(0, len.indexOf(","));
this.len[n] = tmpstr;
len = len.substring(len.indexOf(",") + 1, len.length);
n++;
}
this.len[n] = len;
return this;
}

/*************************************************************************\
CardType setRules()
return the CardType object.
\*************************************************************************/
function setRules(rules) {
// Create the rules array.
if (rules.length == 0 || rules == null)
rules = "0,1,2,3,4,5,6,7,8,9";
  
var tmprules = rules;
n = 1;
while (tmprules.indexOf(",") != -1) {
tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
n++;
}
this.rules = new makeArray(n);
n = 0;
while (rules.indexOf(",") != -1) {
var tmpstr = rules.substring(0, rules.indexOf(","));
this.rules[n] = tmpstr;
rules = rules.substring(rules.indexOf(",") + 1, rules.length);
n++;
}
this.rules[n] = rules;
return this;
}

// --></SCRIPT><? echo "\n\n";


echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="javascript:return checkform();">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="*newhoho*">'."\n";
$_SESSION['nodup'] = $time; echo '<INPUT TYPE="hidden" NAME="nodup" VALUE="'.$time.'">'."\n";

	echo '	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2">'."\n";

	//echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Name').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:280px;" NAME="name" VALUE="'.getval('name').'"></TD></TR>'."\n";

	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Home/Business Phone').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="phone_homebus" VALUE="'.$_SESSION['bb_member']['phone_homebus'].'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Cell Phone').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="phone_cell" VALUE="'.$_SESSION['bb_member']['phone_cell'].'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Cell Phone Country').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" STYLE="width:180px;" NAME="cell_country" VALUE="'.$_SESSION['bb_member']['cell_country'].'"></TD></TR>'."\n";

	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo '	<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:14pt;">Please choose your pass:</TD></TR>'."\n\n";
	foreach($ticket_types as $thistype){
	echo '<TR><TD ALIGN="right"><INPUT TYPE="radio" NAME="type" ID="type'.$thistype['id'].'" VALUE="'.$thistype['id'].'"';
		if(getval('type') == $thistype['id']): echo ' CHECKED'; endif;
		echo '></TD><TD STYLE="font-family:Arial; font-size:10pt;"><LABEL FOR="type'.$thistype['id'].'">'.$thistype['title'].' - $'.$thistype['price'].'</LABEL></TD></TR>'."\n";
	}
	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo '	<TR><TD ALIGN="right"></TD><TD><FONT FACE="Arial" SIZE="1"><I>'.gettrans('We accept Visa, MasterCard or American Express credit cards.').'</I></FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Credit card number').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" ID="cc_num" VALUE="'.getval('cc_num').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Expiration date').'</B>&nbsp;</TD><TD><SELECT NAME="cc_expdate_month" ID="cc_expdate_month"><OPTION VALUE="">Choose...</OPTION>';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if(getval('cc_expdate_month') == $i): echo " SELECTED"; endif;
			echo '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year"><OPTION VALUE="">Choose...</OPTION>';
		for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
			echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
			if(getval('cc_expdate_year') == date("y",mktime("0","0","0","1","1",$i))): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
		}
		echo '</SELECT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Security code').'</B>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'"></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; width:200px; padding-right:5px;">'.gettrans('Zip/Postal code').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Please provide the zip or postal code associated with your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="'.getval('cc_zip').'" STYLE="width:80px;"></TD></TR>'."\n";
	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt;"><B>'.gettrans('Comments').'</B>&nbsp;</TD><TD ALIGN="left"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:280px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";
	echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";
	echo '	<TR><TD ALIGN="right"><INPUT TYPE="checkbox" NAME="acceptterms" ID="acceptterms" VALUE="I agree"></TD><TD ALIGN="left" STYLE="font-family:Arial; font-size:10pt; padding-left:4px;"><LABEL FOR="acceptterms">I agree to the terms and conditions as outlined on the<BR><A HREF="http://www.bundubus.com/hop_on_hop_off_grand_canyon_transportation.php" TARGET="_blank">Bundu Bus website</A>.</LABEL></TD></TR>'."\n";
	echo '	</TABLE><BR>'."\n\n";

	echo '<INPUT TYPE="submit" NAME="submit" VALUE="Purchase Pass">'."\n\n";

echo '</FORM>'."\n";

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Return to Reservation Index</A><BR><BR>'."\n";


} elseif(isset($_REQUEST['trip']) && count($_REQUEST['trip']) > 0){  //! NEW TRIP**********************************************************************************

$_REQUEST['act'] = $_SERVER['PHP_SELF'];
$_SESSION['nodup'] = $time; $_REQUEST['nodup'] = $time;

if($_REQUEST['trip'][0] != "*new*"){
$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`dep_loc` as `route_dep_loc`, routes.`arr_loc` as `route_arr_loc`, routes.`details` FROM `reservations_assoc`,`routes`,`reservations` WHERE';
	$query .= ' reservations_assoc.`routeid` = routes.`id` AND reservations_assoc.`reservation` = reservations.`id`';
	$query .= ' AND (reservations_assoc.`id` = "'.implode('" OR reservations_assoc.`id` = "',$_REQUEST['trip']).'")';
	$query .= ' AND (reservations_assoc.`reservation` = "'.implode('" OR reservations_assoc.`reservation` = "',$ticketlist).'")';
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
if($num_results > 0){
	//echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">The following trip(s) will be changed.';
		//echo '  <A HREF="'.$_SERVER['PHP_SELF'].'?cancel='.implode('|',$_REQUEST['trip']).'">Click here</A> to cancel.';
	//	echo '</SPAN><BR>'."\n\n";
//echo '<TABLE BORDER="0">'."\n";
	for($i=0; $i<$num_results; $i++){
	$changetrip = mysql_fetch_assoc($result);
		if(is_numeric($changetrip['dep_loc'])){ $changetrip['dep_loc'] = $stops['s'.$changetrip['dep_loc']]['name']; }
		if(is_numeric($changetrip['arr_loc'])){ $changetrip['arr_loc'] = $stops['s'.$changetrip['arr_loc']]['name']; }
		/*echo '<TR>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$changetrip['name'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$changetrip['reservation'].'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px; white-space:nowrap;">'.date("D M j, Y",$changetrip['date']).'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.date("g:ia",$changetrip['dep_time']).'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:6px; padding-right:6px;">'.$changetrip['dep_loc'].'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;"><I>-to-</I></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.date("g:ia",$changetrip['arr_time']).'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.$changetrip['arr_loc'].'</TD>';
		echo '</TR>'."\n";*/
	if(!isset($_REQUEST['smonth']) && $i==0){
		$_REQUEST['smonth'] = date("n",$changetrip['dep_time']);
		$_REQUEST['sday'] = date("j",$changetrip['dep_time']);
		$_REQUEST['syear'] = date("Y",$changetrip['dep_time']);
		$_REQUEST['pax'] = ($changetrip['adults']+$changetrip['seniors']+$changetrip['children']);
		$_REQUEST['start'] = $changetrip['route_dep_loc'];
		$_REQUEST['end'] = $changetrip['route_arr_loc'];
		}
	} //End ForLoop
	//echo '</TABLE><BR>'."\n\n";
	}
}


if(!isset($_REQUEST['safe']) || $_REQUEST['safe'] != "y"){


?><script language="javascript"><!--

var legnotes = new Array();

function jfeed(){
	feedcode = window.getroutes.feedit();
	document.getElementById('routes').innerHTML = feedcode;
	legnotes = window.getroutes.legnotes;
	}

//--></script><? echo "\n\n";


$_REQUEST['return'] = 'f';
$_REQUEST['rettype'] = 'html';
$_REQUEST['disablejs'] = 'y';
include('findroute.php');

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;"><TR>'."\n\n";

//!List miniroute needed/booked routes
if(isset($_REQUEST['ticket']) && $_REQUEST['ticket'] != "*new*"){

	$rticket = false;
	$query = 'SELECT reservations_assoc.* FROM `reservations`,`reservations_assoc` WHERE reservations.`id` = reservations_assoc.`reservation`';
		$query .= ' AND reservations_assoc.`id` = "'.$_REQUEST['ticket'].'"';
		$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
		$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
		$query .= ' ORDER BY `id` DESC';
	$result = mysql_query($query);
	$rticket = mysql_fetch_assoc($result);
		//echo '<PRE STYLE="text-align:left;">'; print_r($rticket); echo '</PRE>';
		if($rticket){
			$query = 'SELECT * FROM `reservations_assoc` WHERE `id` = "'.$_REQUEST['ticket'].'" AND `username` = "'.$_SESSION['bb_member']['username'].'" AND `password` = "'.$_SESSION['bb_member']['password'].'" LIMIT 1';
			$result = mysql_query($query);
			$num_results = mysql_num_rows($result);
			if($num_results == 1){
				$rticket = mysql_fetch_assoc($result);
				$query = 'SELECT reservations_assoc.`date`, reservations_assoc.`dep_time` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$rticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id` AND reservations_assoc.`canceled` = 0 ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC LIMIT 1';
				$result = mysql_query($query);
				$num_results = mysql_num_rows($result);
				if($num_results == 1){
					$row = mysql_fetch_assoc($result);
					$rticket['start'] = $row['dep_time'];
					if($startdate < $rticket['start']){
						$startdate = $rticket['start'];
						if($finddate < $startdate){ $finddate = $startdate; }
						$enddate = mktime(date("G",$finddate),date("i",$finddate),0,date("n",$finddate),(date("j",$finddate)+($_REQUEST['maxdays']+1)),date("Y",$finddate));
						}
					$rticket['expires'] = mktime(date("G",$rticket['start']),date("i",$rticket['start']),0,date("n",$rticket['start']),(date("j",$rticket['start'])+$rticket['days']),date("Y",$rticket['start']));
					if($enddate > $rticket['expires']){ $enddate = $rticket['expires']; }
					}
				}
			}

	$routes = array();
	if(isset($rticket['type']) && $rticket['type'] == "m" && isset($rticket['miniroute']) && $rticket['miniroute'] > 0){
		//Get needed routes outline
		$query = 'SELECT miniroutes_assoc.*, routes.`dep_loc`, routes.`arr_loc` FROM `miniroutes_assoc`,`routes` WHERE';
			$query .= ' `minirouteid` = "'.$rticket['miniroute'].'" AND `minirouteid` != 0';
			$query .= ' AND miniroutes_assoc.`type` = "r" AND miniroutes_assoc.`typeid` = routes.`id`';
			$query .= ' ORDER BY `order` ASC';
		$result = @mysql_query($query);
		$num_results = @mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
				$row = mysql_fetch_assoc($result);
				array_push($routes,$row);
				}
			//echo '<PRE STYLE="text-align:left;">'; print_r($routes); echo '</PRE>';
		}

	//Get booked
	$usedlegs = array();
	$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes` WHERE';
		$query .= ' reservations_assoc.`canceled` = 0';
		$query .= ' AND reservations_assoc.`reservation` = "'.$rticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
		$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
	//echo $query.'<BR>';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($usedlegs,$row);
			}
		//echo '<PRE STYLE="text-align:left;">'; print_r($usedlegs); echo '</PRE>';

	if(isset($rticket['type']) && $rticket['type'] == "m" && isset($rticket['miniroute']) && $rticket['miniroute'] > 0){
		//Load booked into route outline
		foreach($routes as $key => $row){
			$rmv = false;
			foreach($usedlegs as $lkey => $thisleg){
				if($row['type'] == "r" && $thisleg['routeid'] == $row['typeid']){
					$routes[$key] = $thisleg;
					$rmv = $lkey;
					}
				}
			if($rmv !== false){ array_splice($usedlegs,$rmv,1); }
			}
		}
		$routes = array_merge($routes,$usedlegs);

	echo '<TD VALIGN="top" STYLE="font-family:Helvetica; font-size:10pt; text-align:left;">';

		if(isset($rticket)){
			echo '<DIV STYLE="text-align:center; border-bottom:1px solid grey; padding-bottom:8px; margin-bottom:12px;">';
			echo '<SPAN STYLE="font-family:Helvetica; font-size:12pt;"><B>Your current trips for<BR>';
				if($rticket['type'] == "h"){ echo ' Hop On, Hop Off Pass'; } elseif($rticket['type'] == "m"){ echo ' Mini Route Pass'; }
				echo ' #'.$rticket['reservation'].'</B></SPAN><BR>';
			if(isset($rticket['start']) && $rticket['start'] > 0){
				echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Valid dates:<BR>'.date("n/j/y g:ia",$rticket['start']).' <I>-through-</I> '.date("n/j/y g:ia",$rticket['expires']).'</SPAN>'."\n\n";
				} else {
				echo '<DIV STYLE="border:#B00000 1px solid; background-color:#F3D9D9; font-family:Arial; font-size:10pt; margin-right:8px;"><B>IMPORTANT: Please book your earliest trip first!!! The start date can be changed later, but there is an extra fee.</B><BR>Please see our <A HREF="http://www.bundubus.com/faq_grand_canyon_transportation.php" TARGET="_blank">FAQ</A> page for details.</DIV>'."\n\n";
				}

				echo '</DIV>';
			}

		//echo '<PRE STYLE="text-align:left;">'; print_r($_REQUEST); echo '</PRE>';

		$hl = array();
		if(isset($_REQUEST['trip']) && !is_array($_REQUEST['trip'])){
			$hl = explode('|',$_REQUEST['trip']);
			} elseif(isset($_REQUEST['trip']) && is_array($_REQUEST['trip'])){
			$hl = $_REQUEST['trip'];
			}
		$ch = array();
		if(isset($_REQUEST['hl']) && !is_array($_REQUEST['hl'])){
			$ch = explode('|',$_REQUEST['hl']);
			} elseif(isset($_REQUEST['hl']) && is_array($_REQUEST['hl'])){
			$ch = $_REQUEST['hl'];
			}

		$prevdate = time();
		$tripcount = 1;
		foreach($routes as $row){
			$newdate = mktime(0,0,0,date("n",$prevdate),(1+date("j",$prevdate)),date("Y",$prevdate));
			$chlink = $_SERVER['PHP_SELF'].'?ticket='.@$rticket['id'].'&trip=*new*&smonth='.date("n",$newdate).'&sday='.date("j",$newdate).'&syear='.date("Y",$newdate).'&pax=1&maxdays=5&start='.$row['dep_loc'].'&end='.$row['arr_loc'].'&hl='.@$row['typeid'].'&btn=Choose';
			//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';

			if(is_numeric($row['dep_loc'])){ $row['dep_loc'] = $stops['s'.$row['dep_loc']]['name']; }
			if(is_numeric($row['arr_loc'])){ $row['arr_loc'] = $stops['s'.$row['arr_loc']]['name']; }
			echo '<DIV STYLE="margin-bottom:18px; padding:2px;';
				if(in_array(@$row['tripid'],$hl) || in_array(@$row['typeid'],$ch)){ echo ' background-color:#CCCCCC;'; }
				echo '">';
				echo '<SPAN STYLE="font-size:11pt;">';
				if(isset($row['date']) && $row['date'] > 0){
					echo date("D n/j/Y",$row['dep_time']);
					} else {
					echo '<A HREF="'.$chlink.'" STYLE="color:#0000FF; font-style:italic;">Choose date...</A>';
					}
					echo '</SPAN><BR>';
				if(isset($row['dep_time']) && $row['dep_time'] > 0){
					echo date("g:ia",$row['dep_time']).' ';
					}
				echo $row['dep_loc'].' <I>-to-</I>';
				if(isset($row['arr_time']) && $row['arr_time'] > 0){
					echo ' '.date("g:ia",$row['arr_time']);
					}
				echo ' '.$row['arr_loc'];
				if(isset($row['tripid']) && in_array($row['tripid'],$hl)){
					echo '<BR><I>This trip will be replaced with whatever route you choose.</I>';
					} elseif(isset($row['typeid']) && in_array($row['typeid'],$ch)){
					echo '<BR><I>Find a date that works for you and click "Choose".</I>';
					} elseif($tripcount == 1 && isset($row['date']) && $row['date'] > 0 && isset($row['dep_time']) && $row['dep_time'] > 0 && $row['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
					echo '<BR><I>You must call or email to change the first trip on your pass.</I>';
					} elseif($tripcount > 1 && isset($row['date']) && $row['date'] > 0 && isset($row['dep_time']) && $row['dep_time'] > 0 && $row['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
					echo '<BR><A HREF="'.$_SERVER['PHP_SELF'].'?ticket='.$rticket['id'].'&trip='.$row['tripid'].'" STYLE="color:#0000FF; font-style:italic;">Change this trip</A>';
					}
				echo '</DIV>'."\n\n";
			if(isset($thisleg['date']) && $thisleg['date'] > 0){ $prevdate = $thisleg['date']; }
			$tripcount++;
			}
		echo '</TD>'."\n\n";

	}

echo '<TD VALIGN="top" ALIGN="center"';
	if(isset($_REQUEST['ticket']) && $_REQUEST['ticket'] != "*new*"){ echo ' STYLE="padding-left:10px; width:620px; border-left:2px solid grey;"'; }
	echo '>'."\n";

if(isset($_REQUEST['smonth'])){

	echo '<DIV STYLE="text-align:center; font-family:Helvetica; font-size:12pt; font-weight:bold; padding-bottom:20px;">Routes available to reserve</DIV>';
	echo '<DIV ID="routes">';

	$_REQUEST['return'] = 'b';
	$_REQUEST['rettype'] = 'html';
	$_REQUEST['disablejs'] = 'y';
	if(isset($_REQUEST['trip'])){ $_REQUEST['trip'] = implode('|',$_REQUEST['trip']); }
	$safeurl = $_SERVER['PHP_SELF'].'?n=1';
		foreach($_REQUEST as $key => $val){ $safeurl .= '&'.$key.'='.$val; }
		$safeurl .= '&safe=y';
	
	echo '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="200" height="150">
		<param name="movie" value="images/findroutes.swf">
		<param name="quality" value="high">
		<param name="bgcolor" value="#FFFFFF">
		<embed name="findroutes" src="images/findroutes.swf" width="200" height="150" quality="high" bgcolor="#FFFFFF" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
	</object><BR>
	<SPAN STYLE="font-family:Arial; font-size:10pt; color:#333333;"><I><A HREF="'.$safeurl.'">Click here</A> if results do not appear within 2 minutes.</I></SPAN><BR><BR>
	</DIV>'."\n\n";
	
	$_REQUEST['return'] = 'r';
	$_REQUEST['rettype'] = 'jfeed';
	$_REQUEST['disablejs'] = 'y';
	//if(isset($_REQUEST['trip'])){ $_REQUEST['trip'] = implode('|',$_REQUEST['trip']); }
	$url = 'findroute.php?n=1';
		foreach($_REQUEST as $key => $val){ $url .= '&'.$key.'='.$val; }
	
	echo '<IFRAME SRC="'.$url.'" NAME="getroutes" onLoad="jfeed()" WIDTH="1" HEIGHT="1" FRAMEBORDER="0" MARGINHEIGHT="0" MARGINWIDTH="0" SCROLLING="no"></IFRAME>'."\n\n";
	
	} else {

	echo '<SPAN STYLE="font-family:Helvetica; font-style:italic;">Choose your date and departure/arrival locations above,<BR>then click "Find Routes" to continue.</SPAN>';

	} //End Start Search If

echo '	</TD>'."\n\n";

echo '</TR></TABLE><BR>'."\n\n";


} else {

$_REQUEST['return'] = 'b';
$_REQUEST['rettype'] = 'html';
$_REQUEST['disablejs'] = 'n';
include('findroute.php');

}

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Return to Reservation Index</A><BR><BR>'."\n";


} elseif(isset($_REQUEST['cancel']) && count($_REQUEST['cancel']) > 0){ //! CANCEL TRIP? ****************************************************************


$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes`,`reservations` WHERE';
	$query .= ' reservations_assoc.`routeid` = routes.`id` AND reservations_assoc.`reservation` = reservations.`id`';
	//$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
	$query .= ' AND (reservations_assoc.`id` = "'.implode('" OR reservations_assoc.`id` = "',$_REQUEST['cancel']).'")';
	$query .= ' AND (reservations_assoc.`reservation` = "'.implode('" OR reservations_assoc.`reservation` = "',$ticketlist).'")';
	//echo $query.'<BR>';
$result = mysql_query($query); echo mysql_error();
$num_results = mysql_num_rows($result);
if($num_results > 0){

	echo '<FORM METHOD="post"  ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
	echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="addroutes">'."\n";
	$_SESSION['nodup'] = $time; echo '<INPUT TYPE="hidden" NAME="nodup" VALUE="'.$time.'">'."\n";

	echo '<SPAN STYLE="font-family:Arial; font-size:14pt;">Are you sure you want to cancel ';
		if($num_results == 1){ echo 'this trip'; } else { echo 'these trips'; }
		echo '?</SPAN><BR><BR>'."\n\n";

	echo '<TABLE BORDER="0">'."\n";
	for($i=0; $i<$num_results; $i++){
	$canceltrip = mysql_fetch_assoc($result);
		if(is_numeric($canceltrip['dep_loc'])){ $canceltrip['dep_loc'] = $stops['s'.$canceltrip['dep_loc']]['name']; }
		if(is_numeric($canceltrip['arr_loc'])){ $canceltrip['arr_loc'] = $stops['s'.$canceltrip['arr_loc']]['name']; }
		echo '<TR>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">';
			echo '<INPUT TYPE="hidden" NAME="remove[]" VALUE="'.$canceltrip['id'].'">';
			echo $canceltrip['name'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:3px; padding-right:6px; white-space:nowrap;">'.$canceltrip['reservation'].'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px; white-space:nowrap;">'.date("D M j, Y",$canceltrip['date']).'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.date("g:ia",$canceltrip['dep_time']).'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-left:6px; padding-right:6px;">'.$canceltrip['dep_loc'].'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;"><I>-to-</I></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.date("g:ia",$canceltrip['arr_time']).'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-right:6px;">'.$canceltrip['arr_loc'].'</TD>';
		echo '</TR>'."\n";
		} //End ForLoop
	echo '</TABLE><BR>'."\n\n";

	echo '<INPUT TYPE="submit" VALUE="Yes" STYLE="font-size:12pt; width:100px; height:30px;">&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="button" VALUE="No" STYLE="font-size:12pt; width:100px; height:30px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'\'">'."\n\n";

	echo '</FORM>'."\n\n";

	} else {

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">There has been an error. Please contact us to complete your cancellation.</SPAN><BR><BR>'."\n\n";

	}

echo '<A HREF="'.$_SERVER['PHP_SELF'].'" STYLE="font-family:Arial; font-size:12pt;">Return to Reservation Index</A><BR><BR>'."\n";


} else {  //! TRIP/TICKET INDEX ****************************************************************************************************************************

$_SESSION['nodup'] = '';

echo '<DIV ID="showbox" STYLE="display:none; position:absolute; z-index:200;"></DIV>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">Welcome '.$_SESSION['bb_member']['name'].'!</SPAN><BR><BR>'."\n\n";

echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">Your Bundu Bus Reservations are listed below.<BR>You can reserve your spot on a trip and make changes to that reservation online <B>'.$defaults['chgcutoff'].' hours or more</B> before the scheduled departure time.</SPAN><BR><BR>'."\n\n";


//GET TICKETS
$tickets = array();
$query = 'SELECT reservations_assoc.* FROM `reservations`,`reservations_assoc` WHERE reservations.`id` = reservations_assoc.`reservation`';
	$query .= ' AND reservations_assoc.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
	$query .= ' AND reservations_assoc.`username` = "'.$_SESSION['bb_member']['username'].'" AND reservations_assoc.`password` = "'.$_SESSION['bb_member']['password'].'"';
	$query .= ' ORDER BY `id` DESC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tickets,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($tickets); echo '</PRE>';


if(count($tickets) > 0){

echo '<SPAN STYLE="font-family:Arial; font-size:16pt;">Your Bundu Bus Passes</SPAN><BR>'."\n\n";

echo '<TABLE BORDER="0" WIDTH="90%" CELLPADDING="2" CELLSPACING="0" ID="tickettable">'."\n\n";
echo '<TR BGCOLOR="#000066">';
	//echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10px; color:#FFFFFF;">Del</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Reservation</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Type</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Start</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Days</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Expires</TD>';
	echo '</TR>'."\n";

bgcolor('reset');
foreach($tickets as $ticket){

	//!Hoho
	if($ticket['type'] == "h"){
		$ticket['start'] = 0;
		$usedlegs = array();
		$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$ticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
			$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
		//echo $query.'<BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$row = mysql_fetch_assoc($result);
			array_push($usedlegs,$row);
			}
	
		//Find start date/expiration
		$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.`dep_time` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$ticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
			$query .= ' AND reservations_assoc.`canceled` = 0';
			$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$getstart = mysql_fetch_assoc($result);
			$ticket['start'] = $getstart['dep_time'];
			$ticket['expires'] = mktime(date("G",$ticket['start']),date("i",$ticket['start']),0,date("n",$ticket['start']),(date("j",$ticket['start'])+$ticket['days']),date("Y",$ticket['start']));
			$dstart = date("n/j/y g:ia",$ticket['start']);
			$dexpires = date("n/j/y g:ia",$ticket['expires']);
			} else {
			$ticket['start'] = 0;
			$ticket['expires'] = 0;
			$dstart = 'On first use';
			$dexpires = 'n/a';
			}
	
		$bgcolor = bgcolor('');
		echo '<TR BGCOLOR="#'.$bgcolor.'">';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><B>'.$ticket['reservation'].'</B></TD>';
		echo '<TD STYLE="font-family:Arial; font-size:12pt;">';
			if($ticket['type'] == "h"){
				echo 'Hop On, Hop Off';
				} elseif($ticket['type'] == "m"){
				echo 'Mini Route';
				}
			echo '</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:12pt;">'.$ticket['name'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;">'.$dstart.'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;">'.$ticket['days'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;';
			if($ticket['expires'] > 0 && $ticket['expires'] < $time): echo ' color:#FF0000;'; endif;
			echo '">'.$dexpires.'</TD>';
		echo '</TR>'."\n";
	
		echo '<TR BGCOLOR="#'.$bgcolor.'">';
		echo '<TD COLSPAN="6" VALIGN="top" STYLE="padding:0px; padding-left:50px;">'."\n"; //padding-bottom:4px;
		echo '<FORM METHOD="post" ID="hoho_form" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n"; // onSubmit="return pickact(\'hoho\');"
		echo '<INPUT TYPE="hidden" NAME="ticket" VALUE="'.$ticket['id'].'">'."\n";
			echo '<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0">';
			$tripcount = 1; $forbatch = 0;
			foreach($usedlegs as $thisleg){
			if($thisleg['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }
			if(is_numeric($thisleg['dep_loc'])){ $thisleg['dep_loc'] = $stops['s'.$thisleg['dep_loc']]['name']; }
			if(is_numeric($thisleg['arr_loc'])){ $thisleg['arr_loc'] = $stops['s'.$thisleg['arr_loc']]['name']; }
	
			echo '<TR>';
			echo '<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:6px;">&#149;</TD>';
			//echo '<TD STYLE="padding-right:3px;"><INPUT TYPE="checkbox"></TD>';
			//echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$thisleg['reservation'].'</TD>';
			echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px; width:120px;">'.date("M j, Y",$thisleg['date']).'</TD>';
			//echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$thisleg['name'].'</TD>';
				$thisleg['legname'] = date("g:ia",$thisleg['dep_time']).' '.$thisleg['dep_loc'].' <I>-to-</I> '.date("g:ia",$thisleg['arr_time']).' '.$thisleg['arr_loc'];
				$thisleg['legname'] = str_replace("\n",' ',$thisleg['legname']);
				$thisleg['legname'] = str_replace('"','',$thisleg['legname']);
			echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' width:350px; padding-right:8px;">'.$thisleg['legname'].'</TD>';
			if($thisleg['canceled'] > 0){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:8pt; padding-left:4px; color:#666666;"><I>Canceled</I></TD>';
				} elseif($tripcount == 1 && $thisleg['dep_time'] > $time){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chg1st)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
				} elseif($thisleg['dep_time'] > $time && $thisleg['dep_time'] <= ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chgcutoff)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
				} elseif($thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?ticket='.$ticket['id'].'&trip='.$thisleg['tripid'].'">Change</A></TD>';
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:10px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?cancel='.$thisleg['tripid'].'">Cancel</A></TD>';
				//echo '<INPUT TYPE="button" VALUE="Cancel" STYLE="font-size:8pt;">';
				if($tripcount != 1 && $thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){ echo '<TD STYLE="padding-left:8px;"><INPUT TYPE="checkbox" NAME="tripids[]" VALUE="'.$thisleg['tripid'].'"></TD>'; $forbatch++; }
				}
			echo '</TR>';
			$tripcount++;
			}
			echo '<TR><TD COLSPAN="3" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; padding-bottom:6px;">';
				if($ticket['expires'] == 0 || $ticket['expires'] > $time): echo '+ <A HREF="'.$_SERVER['PHP_SELF'].'?ticket='.$ticket['id'].'&trip=*new*" STYLE="color:#0000FF;">Add trips to this pass</A> +'; endif;
				echo '</TD>';
				if($forbatch > 1){ echo '<TD COLSPAN="2"></TD><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; padding-bottom:4px; padding-left:8px;"><SELECT NAME="batch" ID="hoho_batch" STYLE="font-size:9pt;"><OPTION VALUE="trip">Change</OPTION><OPTION VALUE="cancel">Cancel</OPTION></SELECT>&nbsp;<INPUT TYPE="submit" VALUE="Go" STYLE="font-size:9pt;"></TD>'; }
				echo '</TR>';
			echo '</TABLE>'."\n";
		echo '</FORM>'."\n";
		echo '</TD></TR>'."\n";
	} else {

	//!Mini Route
		//echo '<PRE STYLE="text-align:left;">'; print_r($ticket); echo '</PRE>';

		//Get needed routes outline
		$routes = array();
		$query = 'SELECT miniroutes_assoc.*, routes.`dep_loc`, routes.`arr_loc` FROM `miniroutes_assoc`,`routes` WHERE';
			$query .= ' `minirouteid` = "'.$ticket['miniroute'].'" AND `minirouteid` != 0';
			$query .= ' AND miniroutes_assoc.`type` = "r" AND miniroutes_assoc.`typeid` = routes.`id`';
			$query .= ' ORDER BY `order` ASC';
		$result = @mysql_query($query);
		$num_results = @mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
				$row = mysql_fetch_assoc($result);
				array_push($routes,$row);
				}
			//echo '<PRE STYLE="text-align:left;">'; print_r($routes); echo '</PRE>';

		//Get booked
		$usedlegs = array();
		$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$ticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
			$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC';
		//echo $query.'<BR>';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
				$row = mysql_fetch_assoc($result);
				array_push($usedlegs,$row);
				}
			//echo '<PRE STYLE="text-align:left;">'; print_r($usedlegs); echo '</PRE>';

		//Load booked into route outline
		foreach($routes as $key => $row){
			$rmv = false;
			foreach($usedlegs as $lkey => $thisleg){
				if($row['type'] == "r" && $thisleg['routeid'] == $row['typeid'] && $thisleg['canceled'] == 0){
					$routes[$key] = $thisleg;
					//$routes[$key]['dep_loc_id'] = $row['dep_loc'];
					//$routes[$key]['arr_loc_id'] = $row['arr_loc'];
					$rmv = $lkey;
					}
				}
			if($rmv !== false){ array_splice($usedlegs,$rmv,1); }
			}

		//Find start date/expiration
		$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.`dep_time` FROM `reservations_assoc`,`routes` WHERE reservations_assoc.`reservation` = "'.$ticket['reservation'].'" AND reservations_assoc.`routeid` = routes.`id`';
			$query .= ' AND reservations_assoc.`canceled` = 0';
			$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$getstart = mysql_fetch_assoc($result);
			$ticket['start'] = $getstart['dep_time'];
			$ticket['expires'] = mktime(date("G",$ticket['start']),date("i",$ticket['start']),0,date("n",$ticket['start']),(date("j",$ticket['start'])+$ticket['days']),date("Y",$ticket['start']));
			$dstart = date("n/j/y g:ia",$ticket['start']);
			$dexpires = date("n/j/y g:ia",$ticket['expires']);
			} else {
			$ticket['start'] = 0;
			$ticket['expires'] = 0;
			$dstart = 'On first use';
			$dexpires = 'n/a';
			}
			//$ticket['start'] = $ticket['date'];
			//$ticket['expires'] = mktime(date("G",$ticket['start']),date("i",$ticket['start']),0,date("n",$ticket['start']),(date("j",$ticket['start'])+$ticket['days']),date("Y",$ticket['start']));
			//$dstart = date("n/j/y g:ia",$ticket['start']);
			//$dexpires = date("n/j/y g:ia",$ticket['expires']);

		$bgcolor = bgcolor('');
		echo '<TR BGCOLOR="#'.$bgcolor.'">';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;"><B>'.$ticket['reservation'].'</B></TD>';
		echo '<TD STYLE="font-family:Arial; font-size:12pt;">Mini Route</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:12pt;">'.$ticket['name'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;">'.$dstart.'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;">'.$ticket['days'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:12pt;';
			if($ticket['expires'] > 0 && $ticket['expires'] < $time): echo ' color:#FF0000;'; endif;
			echo '">'.$dexpires.'</TD>';
		echo '</TR>'."\n";
	
		echo '<TR BGCOLOR="#'.$bgcolor.'">';
		echo '<TD COLSPAN="6" VALIGN="top" STYLE="padding:0px; padding-left:50px;">'."\n"; //padding-bottom:4px;
		echo '<FORM METHOD="post" ID="hoho_form" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n"; // onSubmit="return pickact(\'hoho\');"
		echo '<INPUT TYPE="hidden" NAME="ticket" VALUE="'.$ticket['id'].'">'."\n";
			echo '<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0">';
			$tripcount = 1; $forbatch = 0;

			$prevdate = time();
			foreach($routes as $thisleg){
			//echo '<PRE STYLE="text-align:left;">'; print_r($thisleg); echo '</PRE>';
			if(isset($thisleg['canceled']) && $thisleg['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }

			$newdate = mktime(0,0,0,date("n",$prevdate),(1+date("j",$prevdate)),date("Y",$prevdate));
			$ch = $_SERVER['PHP_SELF'].'?ticket='.$ticket['id'].'&trip=*new*&smonth='.date("n",$newdate).'&sday='.date("j",$newdate).'&syear='.date("Y",$newdate).'&pax=1&maxdays=5&start='.$thisleg['dep_loc'].'&end='.$thisleg['arr_loc'].'&hl='.@$thisleg['typeid'].'&btn=Choose';

			echo '<TR>';
			echo '<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:6px;">&#149;</TD>';
			echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px; width:120px;">';
				if(isset($thisleg['date']) && $thisleg['date'] > 0){
					echo date("M j, Y",$thisleg['date']);
					} else {
					echo '<A STYLE="color:#0000FF; text-decoration:none; font-style:italic;" HREF="'.$ch.'">Choose date</A>';
					}
				echo '</TD>';

			if(is_numeric($thisleg['dep_loc'])){ $thisleg['dep_loc'] = $stops['s'.$thisleg['dep_loc']]['name']; }
			if(is_numeric($thisleg['arr_loc'])){ $thisleg['arr_loc'] = $stops['s'.$thisleg['arr_loc']]['name']; }

			$thisleg['legname'] = '';
				if(isset($thisleg['dep_time']) && $thisleg['dep_time'] > 0){
					$thisleg['legname'] .= date("g:ia",$thisleg['dep_time']).' ';
					}
				$thisleg['legname'] .= $thisleg['dep_loc'].' <I>-to-</I>';
				if(isset($thisleg['arr_time']) && $thisleg['arr_time'] > 0){
					$thisleg['legname'] .= ' '.date("g:ia",$thisleg['arr_time']);
					}
				$thisleg['legname'] .= ' '.$thisleg['arr_loc'];
				$thisleg['legname'] = str_replace("\n",' ',$thisleg['legname']);
				$thisleg['legname'] = str_replace('"','',$thisleg['legname']);
				echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' width:350px; padding-right:8px;">'.$thisleg['legname'].'</TD>';
			if(!isset($thisleg['date']) || $thisleg['date'] == 0){
				echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$ch.'">Choose</A></TD>';
				} elseif(isset($thisleg['canceled']) && $thisleg['canceled'] > 0){
				echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:8pt; padding-left:4px; color:#666666;"><I>Canceled</I></TD>';
				} elseif($tripcount == 1 && isset($thisleg['dep_time']) && $thisleg['dep_time'] > 0 && $thisleg['dep_time'] > $time){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chg1st)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
				} elseif(isset($thisleg['dep_time']) && $thisleg['dep_time'] > 0 && $thisleg['dep_time'] > $time && $thisleg['dep_time'] <= ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chgcutoff)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
				} elseif(isset($thisleg['dep_time']) && $thisleg['dep_time'] > 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?ticket='.$ticket['id'].'&trip='.$thisleg['tripid'].'">Change</A></TD>';
				//echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?cancel='.$thisleg['tripid'].'">Cancel</A></TD>';
				//echo '<INPUT TYPE="button" VALUE="Cancel" STYLE="font-size:8pt;">';
				//if($tripcount != 1 && $thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){ echo '<TD><INPUT TYPE="checkbox" NAME="tripids[]" VALUE="'.$thisleg['tripid'].'"></TD>'; $forbatch++; }
				}
			echo '</TR>';
			$tripcount++;
			if(isset($thisleg['date']) && $thisleg['date'] > 0){ $prevdate = $thisleg['date']; }
			}

			foreach($usedlegs as $thisleg){
				if($thisleg['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }
				if(is_numeric($thisleg['dep_loc'])){ $thisleg['dep_loc'] = $stops['s'.$thisleg['dep_loc']]['name']; }
				if(is_numeric($thisleg['arr_loc'])){ $thisleg['arr_loc'] = $stops['s'.$thisleg['arr_loc']]['name']; }
	
				echo '<TR>';
				echo '<TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-right:6px;">&#149;</TD>';
				//echo '<TD STYLE="padding-right:3px;"><INPUT TYPE="checkbox"></TD>';
				//echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$thisleg['reservation'].'</TD>';
				echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' width:120px;">'.date("M j, Y",$thisleg['date']).'</TD>';
				//echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-right:8px;">'.$thisleg['name'].'</TD>';
					$thisleg['legname'] = date("g:ia",$thisleg['dep_time']).' '.$thisleg['dep_loc'].' <I>-to-</I> '.date("g:ia",$thisleg['arr_time']).' '.$thisleg['arr_loc'];
					$thisleg['legname'] = str_replace("\n",' ',$thisleg['legname']);
					$thisleg['legname'] = str_replace('"','',$thisleg['legname']);
				echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' width:350px; padding-right:8px;">'.$thisleg['legname'].'</TD>';
				if($thisleg['canceled'] > 0){
					echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:8pt; padding-left:4px; color:#666666;"><I>Canceled</I></TD>';
					} elseif($tripcount == 1 && $thisleg['dep_time'] > $time){
					echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chg1st)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
					} elseif($thisleg['dep_time'] > $time && $thisleg['dep_time'] <= ($time+$defaults['chgcutoff_secs'])){
					echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'ticket\',chgcutoff)" onMouseOut="hideLyr()">Change?</SPAN></TD>';
					} elseif($thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
					echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?ticket='.$ticket['id'].'&trip='.$thisleg['tripid'].'">Change</A></TD>';
					//echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:4px;"><A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?cancel='.$thisleg['tripid'].'">Cancel</A></TD>';
					//echo '<INPUT TYPE="button" VALUE="Cancel" STYLE="font-size:8pt;">';
					//if($tripcount != 1 && $thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){ echo '<TD><INPUT TYPE="checkbox" NAME="tripids[]" VALUE="'.$thisleg['tripid'].'"></TD>'; $forbatch++; }
					}
				echo '</TR>';
				$tripcount++;
				}
			echo '</TABLE>'."\n";
		echo '</FORM>'."\n";
		echo '</TD></TR>'."\n";

	}
}
/*	echo '<TR><TD ALIGN="right" COLSPAN="8" STYLE="font-family:Arial; font-size:9pt; padding-bottom:4px;">Checked:&nbsp;<SELECT NAME="batch" ID="hoho_batch" STYLE="font-size:9pt;">';
		echo '<OPTION VALUE="trip">Change</OPTION>';
		echo '<OPTION VALUE="cancel">Cancel</OPTION>';
		echo '</SELECT>&nbsp;<INPUT TYPE="submit" VALUE="Go" STYLE="font-size:9pt;">';
		//echo '&nbsp;<INPUT TYPE="button" VALUE="Test" STYLE="font-size:9pt;" onClick="pickact();">';
		echo '</TD></TR>'."\n";*/


echo '</TABLE></FORM>'."\n\n";

//echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">- You currently have no hop on, hop off passes. -</SPAN><BR>'."\n\n";

} //END $tickets COUNT IF STATEMENT

//echo '<INPUT TYPE="button" VALUE="New Pass" STYLE="width:140px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?ticket=*new*\'"><BR><BR>'."\n\n";



/*
//GET TRIPS
$trips = array();
$nochange = array();
$query = 'SELECT reservations_assoc.`id` as `tripid`, reservations_assoc.*, routes.`details` FROM `reservations_assoc`,`reservations`,`routes` WHERE `bbticket` = 0';
	//$query .= ' AND reservations_assoc.`canceled` = 0';
	$query .= ' AND reservations_assoc.`date` >= '.$today;
	$query .= ' AND reservations.`email` = "'.$_SESSION['bb_member']['email'].'"';
	$query .= ' AND reservations.`id` = reservations_assoc.`reservation`';
	$query .= ' AND reservations_assoc.`routeid` = routes.`id`';
	$query .= ' ORDER BY reservations_assoc.`date` ASC, reservations_assoc.`dep_time` ASC, routes.`dep_time` ASC, reservations_assoc.`reservation` ASC'; //trips.`canceled` ASC,
	//echo $query.'<BR>';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($trips,$row);
	//if($row['canceled'] > 0){ array_push($nochange,$row['reservation']); }
	}


if(count($trips) > 0){

echo '<FORM METHOD="post" ID="reg_form" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n"; // onSubmit="return pickact(\'reg\');"
echo '<SPAN STYLE="font-family:Arial; font-size:16pt;">Your Bundu Bus Reservations</SPAN><BR>'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="90%" ID="routetable">'."\n\n";
echo '<TR BGCOLOR="#000066">';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold; padding-left:3px;">Conf#</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Name</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Date</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; color:#FFFFFF; font-weight:bold;">Trip</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; color:#FFFFFF; font-weight:bold; padding-left:3px; padding-right:3px;">Details</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:8pt; color:#FFFFFF; font-weight:bold; padding-left:3px; padding-right:3px;">Change</TD>';
	echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:8pt; color:#FFFFFF; font-weight:bold; padding-left:3px; padding-right:3px;">Cancel</TD>';
	echo '</TR>'."\n";

bgcolor('reset');
foreach($trips as $thisleg){
	//$datebase = mktime(0,0,0,date("n",$thisleg['date']),date("j",$thisleg['date']),date("Y",$thisleg['date']));
	//$thisleg['dep_time'] = ($datebase+$thisleg['dep_time']);
	//$thisleg['arr_time'] = ($datebase+$thisleg['arr_time']);
	if(is_numeric($thisleg['dep_loc'])){ $thisleg['dep_loc'] = $stops['s'.$thisleg['dep_loc']]['name']; }
	if(is_numeric($thisleg['arr_loc'])){ $thisleg['arr_loc'] = $stops['s'.$thisleg['arr_loc']]['name']; }
	if($thisleg['canceled'] > 0){ $line = ' color:#666666; text-decoration: line-through;'; } else { $line = ''; }
	echo '<TR BGCOLOR="#'.bgcolor('').'">';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-left:3px; padding-right:3px; white-space:nowrap;">'.$thisleg['reservation'].'</TD>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-left:3px; padding-right:3px; white-space:nowrap;">'.$thisleg['name'].'</TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-left:3px; padding-right:3px; white-space:nowrap;">'.date("D n/j/y",$thisleg['date']).'</TD>';
		echo '<TD STYLE="font-family:Arial; font-size:9pt;'.$line.' padding-left:10px; padding-right:6px;">'.date("g:ia",$thisleg['dep_time']).' '.$thisleg['dep_loc'].' <I>-to-</I> '.date("g:ia",$thisleg['arr_time']).' '.$thisleg['arr_loc'].'</TD>';
		echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:3px; padding-right:3px;">';
			if($thisleg['details'] != ""): echo '<A STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'route\',legnotes[\'l'.$thisleg['id'].'\'])" onMouseOut="hideLyr()">Details</A>'; endif;
			echo '</TD>';
		if($thisleg['canceled'] > 0){
				echo '<TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:8pt; padding-left:3px; padding-right:3px; color:#666666;"><I>Canceled</I>';
			} elseif(in_array($thisleg['reservation'],$nochange)){
				echo '<TD COLSPAN="2" ALIGN="center">&nbsp;';
				//echo date("n/j/y g:ia",$thisleg['dep_time']).'<BR>';
				//echo date("n/j/y g:ia",($time+$defaults['chgcutoff_secs'])).'<BR>';
			} elseif($thisleg['canceled'] == 0 && $thisleg['dep_time'] > $time && $thisleg['dep_time'] <= ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:3px; padding-right:3px;"><SPAN STYLE="color:#0000FF; cursor:pointer;" onMouseOver="showanote(this,\'route\',chgcutoff)" onMouseOut="hideLyr()">Change?</SPAN>';
			} elseif($thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
				echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:3px; padding-right:3px;">';
				echo '<A STYLE="color:#0000FF; text-decoration:none;" HREF="'.$_SERVER['PHP_SELF'].'?trip='.$thisleg['tripid'].'">Change</A>';
			}
			echo '</TD>';
		if(!in_array($thisleg['reservation'],$nochange) && $thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){
			echo '<TD ALIGN="center" STYLE="font-family:Arial; font-size:9pt; padding-left:3px; padding-right:3px;">';
				echo '<A HREF="'.$_SERVER['PHP_SELF'].'?cancel='.$thisleg['tripid'].'" STYLE="color:#0000FF; text-decoration:none;">Cancel</A>';
				echo '</TD>';
			}
		if(!in_array($thisleg['reservation'],$nochange) && $thisleg['canceled'] == 0 && $thisleg['dep_time'] > ($time+$defaults['chgcutoff_secs'])){ echo '<TD STYLE="background:#FFFFFF;"><INPUT TYPE="checkbox" NAME="tripids[]" VALUE="'.$thisleg['tripid'].'"></TD>'; }
		echo '</TR>'."\n";
	}
	echo '<TR><TD ALIGN="right" COLSPAN="8" STYLE="font-family:Arial; font-size:9pt; padding-bottom:4px;">Checked:&nbsp;<SELECT NAME="batch" ID="reg_batch" STYLE="font-size:9pt;">';
		echo '<OPTION VALUE="trip">Change</OPTION>';
		echo '<OPTION VALUE="cancel">Cancel</OPTION>';
		echo '</SELECT>&nbsp;<INPUT TYPE="submit" VALUE="Go" STYLE="font-size:9pt;">';
		//echo '&nbsp;<INPUT TYPE="button" VALUE="Test" STYLE="font-size:9pt;" onClick="pickact();">';
		echo '</TD></TR>'."\n";

echo '</TABLE>'."\n\n";


echo '<script language="javascript"><!--'."\n";
	echo 'var legnotes = new Array();'."\n";
		foreach($trips as $thisleg){
		if($thisleg['details'] != ""){
			$thisleg['details'] = str_replace("\r",'',$thisleg['details']);
			$thisleg['details'] = str_replace("\n",'<BR>',$thisleg['details']);
			$thisleg['details'] = str_replace("'",'\\\'',$thisleg['details']);
			echo "\t".'legnotes[\'l'.$thisleg['id'].'\'] = \''.$thisleg['details'].'\';'."\n";
			}
		}
echo '//--></script>'."\n\n";

//echo '<SPAN STYLE="font-family:Arial; font-size:10pt;">- You currently have no reservations not associated with a hop on, hop off pass. -</SPAN><BR>'."\n\n";

} //END $tickets COUNT IF STATEMENT
*/

echo '<BR><BR>';
//echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">+ <A HREF="'.$_SERVER['PHP_SELF'].'?trip=*new*" STYLE="color:#0000FF;">Routes not covered by the Hop On, Hop Off Pass can be purchased here</A> +</SPAN><BR><BR>'."\n\n";
echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">+ <A HREF="'.$_SERVER['PHP_SELF'].'?hoho=*new*" STYLE="color:#0000FF;">Do you need to purchase another Hop On, Hop Off Pass or Mini Route Pass?  You can do so here.</A> +</SPAN><BR><BR>'."\n\n";


echo '<DIV STYLE="margin:20px;"><A HREF="'.$_SERVER['PHP_SELF'].'?logout=y" STYLE="font-family:Arial; font-size:12pt;">Log Out</A></DIV>'."\n\n";


} //END TICKET IF STATEMENT


}  //END VALID USER IF STATEMENT



?><script language="javascript"><!--

var chg1st = '<B>Change your pass starting date?</B><BR>If you book your start date and then want to change it, you can do so. There is a $20 change fee if you call us to make the change, and $10 if you email us. <B><I>You cannot change it online.</I></B> If you want to change a specific trip once a pass has started to run its course, please see our <A HREF="http://www.bundubus.com/faq_grand_canyon_transportation.php" TARGET="_blank">FAQ\'s</A>.';
var chgcutoff = '<B>Change within <? echo $defaults['chgcutoff']; ?> hours before travel?</B><BR>If you need to change a booking within <? echo $defaults['chgcutoff']; ?> hours of travel, you can call us to make the change.  There is a $10 charge for the call.  Please see our <A HREF="http://www.bundubus.com/faq_grand_canyon_transportation.php" TARGET="_blank">FAQ</A> for details.';

descback = new Image();
descback.src = 'images/descback.jpg';

function hideLyr(){
	document.getElementById("showbox").style.display = "none";
	}

function showLyr(){
	document.getElementById("showbox").style.display = "";
	}

function pickact(f){
	if(document.getElementById(f+"_batch").value == "cancel"){
		document.getElementById(f+"_form").action = "/reserve.php";
		} else {
		document.getElementById(f+"_form").action = "<? echo $_SERVER['PHP_SELF']; ?>";
		}
	return true;
}

function shownotes(obj,legid){
	var thisnote = legnotes['l'+legid];

	if(thisnote != ""){
	var x = document.getElementById('showbox');

	var newhtml = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:260px;" onMouseOver="showLyr()" onMouseOut="hideLyr()"><TR><TD ALIGN="left" ID="fordesc" STYLE="background:#CCCCFF url(\'images/descback.jpg\') right top repeat-y;  border:1px solid #000000; font-family:Arial; font-size:10pt; padding:6px;">'+thisnote+'</TD><TD WIDTH="8">&nbsp;</TD></TR></TABLE>';
	//alert(newhtml);
	x.innerHTML = newhtml;
	x.style.display = "";

	var boxpos = findPos(obj);
	var w = obj.offsetWidth;
	boxpos[0] = eval(boxpos[0] - 260 + 2);
	boxpos[1] = eval(boxpos[1]);
	var windems = getwindems();
	var scrolloff = getScrollXY();
	//alert('WinDems:'+windems[0]+'x'+windems[1]+' ScrollOff:'+scrolloff[0]+'x'+scrolloff[1]);

	var st = document.getElementById('routetable');
	var tabpos = findPos(st);
	var table_bot = eval(tabpos[1] + st.offsetHeight);
	var win_bot = eval(windems[1] + scrolloff[1]);
	var boxb = eval(boxpos[1] + x.offsetHeight);
	//alert(x.offsetHeight+' : '+boxb+' : '+table_bot+' : '+(boxb-table_bot));
	if(table_bot < win_bot && boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		} else if(boxb > win_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-win_bot));
		} else if(boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		}
	boxpos[1] = eval(boxpos[1]); //-20

	x.style.top = boxpos[1] + 'px';
	x.style.left = boxpos[0] + 'px';
	}
	}

function showanote(obj,tbl,thisnote){
	if(thisnote != ""){
	var x = document.getElementById('showbox');

	var newhtml = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:260px;" onMouseOver="showLyr()" onMouseOut="hideLyr()"><TR><TD ALIGN="left" ID="fordesc" STYLE="background:#CCCCFF url(\'images/descback.jpg\') right top repeat-y;  border:1px solid #000000; font-family:Arial; font-size:10pt; padding:6px;">'+thisnote+'</TD><TD WIDTH="8">&nbsp;</TD></TR></TABLE>';
	x.innerHTML = newhtml;
	x.style.display = "";

	var boxpos = findPos(obj);
	var w = obj.offsetWidth;
	boxpos[0] = eval(boxpos[0] - 260 + 2);
	boxpos[1] = eval(boxpos[1]);
	var windems = getwindems();
	var scrolloff = getScrollXY();
	//alert('WinDems:'+windems[0]+'x'+windems[1]+' ScrollOff:'+scrolloff[0]+'x'+scrolloff[1]);

	var st = document.getElementById(tbl+'table');
	var tabpos = findPos(st);
	var table_bot = eval(tabpos[1] + st.offsetHeight);
	var win_bot = eval(windems[1] + scrolloff[1]);
	var boxb = eval(boxpos[1] + x.offsetHeight);
	//alert(x.offsetHeight+' : '+boxb+' : '+table_bot+' : '+(boxb-table_bot));
	if(table_bot < win_bot && boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		} else if(boxb > win_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-win_bot));
		} else if(boxb > table_bot){
		boxpos[1] = eval(boxpos[1]-(boxb-table_bot));
		}
	boxpos[1] = eval(boxpos[1]); //-20

	x.style.top = boxpos[1] + 'px';
	x.style.left = boxpos[0] + 'px';
	}
	}

function findPos(obj){
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
	}

function getScrollXY(){
	var scrOfX = 0, scrOfY = 0;
	if( typeof( window.pageYOffset ) == 'number' ) {
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ){
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return [ scrOfX, scrOfY ];
	}

function getwindems(){
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	return [ myWidth, myHeight ];
	}

//--></script><?


echo '</FORM><BR>'."\n\n";


//echo '<PRE>'; print_r($_SESSION['bb_member']); echo '</PRE>';


echo '<DIV STYLE="font-family:Arial; font-size:8pt;">'.gettrans('Bundu Bus').' '.gettrans('will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";


include_once('../footer.php');

?>