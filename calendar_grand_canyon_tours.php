<?

$pagetitle = 'Bundu Bashers tour calendar';
$metadesc = 'Grand Canyon tours, as well as Monument Valley tours and Bryce Canyon tour service. Yellowstone tours from Salt Lake City, by bus to Grand Canyon, Monument Valley, tours, Yellowstone, Bryce Canyon tour';
include_once('header.php');



?>

The calendar is unavailable right now.  Please check back soon.

<?

if(false){


if(isset($_REQUEST['getmonth']) && $_REQUEST['getmonth'] != ""){
	$_REQUEST['m'] = date("n",$_REQUEST['getmonth']);
	$_REQUEST['y'] = date("Y",$_REQUEST['getmonth']);
	}
if(!isset($_REQUEST['m'])): $_REQUEST['m'] = date("n",$time); endif;
if(!isset($_REQUEST['y'])): $_REQUEST['y'] = date("Y",$time); endif;

$calfirst = mktime(0,0,0,$_REQUEST['m'],1,$_REQUEST['y']);
$calendar = array(
	"start" => date("w",$calfirst),
	"days" => date("t",$calfirst)
	);
$callast = mktime(0,0,0,$_REQUEST['m'],$calendar['days'],$_REQUEST['y']);

if($_REQUEST['m'] != 1 && $_REQUEST['m'] != 12){
	$prevm = ($_REQUEST['m']-1);
	$nextm = ($_REQUEST['m']+1);
	$prevy = $_REQUEST['y'];
	$nexty = $_REQUEST['y'];
} elseif($_REQUEST['m'] == 1){
	$prevm = 12;
	$nextm = 2;
	$prevy = ($_REQUEST['y']-1);
	$nexty = $_REQUEST['y'];
} elseif($_REQUEST['m'] == 12){
	$prevm = 11;
	$nextm = 1;
	$prevy = $_REQUEST['y'];
	$nexty = ($_REQUEST['y']+1);
}

echo '<CENTER><BR>

<FONT FACE="Arial" SIZE="5" COLOR="#000080"><B>Bundu Bashers '.gettrans('Tour Calendar').'</B></FONT><BR><BR>'."\n\n";

echo '<FORM METHOD="get" NAME="cmonth" ID="cmonth" ACTION="'.$_SERVER['PHP_SELF'].'">
	<TABLE BORDER="0"><TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="2"><B>'.gettrans('In which month will you be traveling?').'</B>&nbsp;</FONT></TD><TD><SELECT NAME="getmonth" onChange="javascript:document.cmonth.submit()">';
	$smonth = date("n",$time);
	$syear = date("Y",$time);
	for($i=0; $i<18; $i++){
	$thisvarmonth = mktime(0,0,0,$smonth,1,$syear);
	echo '<OPTION VALUE="'.$thisvarmonth.'"';
		if(date("n",$thisvarmonth) == $_REQUEST['m'] && date("Y",$thisvarmonth) == $_REQUEST['y']): echo ' SELECTED'; endif;
		echo '>'.gettrans( date("F",$thisvarmonth) ).' '.date("Y",$thisvarmonth).'</OPTION>';
	$smonth++;
	}
	echo '</SELECT></TD><TD><INPUT TYPE="submit" VALUE="Find!"></TD></TR></TABLE>
	</FORM>'."\n\n";



//GET TOUR LISTS
$tourdays = array();
$toursord1 = array(); $toursord2 = array(); $toursord3 = array();
$query = 'SELECT tours.`id`, tours.`title`, tours.`eztitle`, tours.`numdays`, tours.`perguest`, tours_dates.`date`';
	$query .= ' FROM `tours`,`tours_dates`';
	$query .= ' WHERE (tours.`hidden` != "1") AND (tours.`id` = tours_dates.`tourid`)';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);

	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(isset($row['eztitle']) && trim($row['eztitle']) != ""): $row['title'] = $row['eztitle']; endif;

	for($day=1; $day<=$calendar['days']; $day++){
		if($row['date'] == mktime(0,0,0,$_REQUEST['m'],$day,$_REQUEST['y'])){
		if(!isset($tourdays[$day]) || !is_array($tourdays[$day])): $tourdays[$day] = array(); $toursord1[$day] = array(); $toursord2[$day] = array(); $toursord3[$day] = array(); endif;
		array_push($tourdays[$day],$row['id']);
		array_push($toursord1[$day],$row['numdays']);
		array_push($toursord2[$day],$row['perguest']);
		array_push($toursord3[$day],$row['title']);
		} //End date if statement
		} //End For Loop
	}
	//echo '<PRE>'; print_r($tourdays); echo '</PRE>';

foreach($tourdays as $id => $thisvar){
	array_multisort($toursord1[$id], SORT_DESC, $toursord2[$id], SORT_DESC, $toursord3[$id], SORT_ASC, $thisvar);
	$tourdays[$id] = $thisvar;
	//echo '<PRE>'; print_r($thisvar); echo '</PRE>';
	}



echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" CLASS="cal" ID="tcalendar">
<TR CLASS="calhead">
	<TD ALIGN="left" STYLE="padding-left:10px;"><A HREF="'.$_SERVER['PHP_SELF'].'?m='.$prevm.'&y='.$prevy.'"><FONT FACE="Arial" SIZE="3" COLOR="#FFFFFF"><B>&lt;&lt;</B></FONT></A></TD>
	<TD ALIGN="center" COLSPAN="5" HEIGHT="32"><FONT FACE="Arial" SIZE="3" COLOR="#FFFFFF"><B>'.gettrans( date("F",$calfirst) ).' '.date("Y",$calfirst).'</B></FONT></TD>
	<TD ALIGN="right" STYLE="padding-right:10px;"><A HREF="'.$_SERVER['PHP_SELF'].'?m='.$nextm.'&y='.$nexty.'"><FONT FACE="Arial" SIZE="3" COLOR="#FFFFFF"><B>&gt;&gt;</B></FONT></A></TD>
	</TR>

<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Sunday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Monday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Tuesday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Wednesday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Thursday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Friday').'</FONT></TD>
	<TD ALIGN="center"><FONT FACE="Arial" SIZE="2">'.gettrans('Saturday').'</FONT></TD>
	</TR>

<TR>';

$day = 0;
$col = $calendar['start'];

if($calendar['start'] > 0): echo '<TD CLASS="cal" COLSPAN="'.$calendar['start'].'">&nbsp;</TD>'; endif;

for($i=0; $day<$calendar['days']; $i++){
$col++;

echo '	<TD ALIGN="left" VALIGN="top" CLASS="cal';
	if($col == 7): echo 'r'; endif;
	echo '">';
	echo '<FONT FACE="Arial" SIZE="3">'.++$day.'</FONT>';
	echo '<BR><FONT FACE="Arial" SIZE="1" COLOR="#666666">';
	if(isset($tourdays[$day]) && count($tourdays[$day]) >0){
		for($i=0; $i<count($tourdays[$day]); $i++){
		$thisvar = $tourdays[$day][$i];
		echo '<A HREF="'.$tourdescs['t'.$thisvar][1].'" onMouseOver="calLyr(this,'."'".$thisvar."'".')" onMouseOut="hideLyr()">'.$tourdescs['t'.$thisvar][0].'</A><BR><BR>';
		}
	} //End If Statement
	//echo '<I>'.gettrans('All one day air, bus and helicopter tours that are not specifically mentioned in this calendar run every day').'</I>';
	echo '</FONT>';
	echo '</TD>'."\n";

if($col == 7){
	$rowbg = bgcolor('');
	echo '</TR>'."\n".'<TR BGCOLOR="'.$rowbg.'">';
	$col = 0;
	}

}

if($col > 0 && $col < 7): echo '<TD CLASS="calr" BGCOLOR="#FFFFFF" COLSPAN="'.(7 - $col).'">&nbsp;</TD>'."\n"; endif;

echo '</TR>
</TABLE><BR></CENTER>

<font face="Arial" size="2" color="#000080">'.gettrans('If, at any time, you would rather speak to a live person, please call us at 1 800 724 7767, or (USA) 435 658 2227, and someone will help you immediately.').'&nbsp;'.gettrans('You can also').' <a href="mailto:info@bundubashers.com?subject=Tour query">'.gettrans('email us').'</a>.</font><BR><BR>'."\n";

}

include('footer.php'); ?>