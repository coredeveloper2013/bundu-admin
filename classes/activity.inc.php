<?php

class activity {
	var $id;
	var $tableName = 'activities';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getName() {
		return $this->data['name'];
	}

	function getPrice() {
		return $this->data['price'];
	}

	function getIDvendor() {
		return $this->data['vendor'];
	}


	function setName($input) {
		$this->data['name'] = $input;
	}

	function setPrice($input) {
		$this->data['price'] = $input;
	}

	function setIDvendor($input) {
		$this->data['vendor'] = $input;
	}

	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>