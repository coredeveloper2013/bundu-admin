<?php

class agent {
	var $id;
	var $tableName = 'agents';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getName() {
		return $this->data['name'];
	}

	function getDiscount() {
		return $this->data['discount'];
	}

	function getDiscountPercent() {
		$discount = $this->getDiscount();
		if($discount == "") {
			$discount = 100;
		}
		return ($discount / 100);
	}

	function getTier() {
		return $this->data['tier'];
	}


	function setName($input) {
		$this->data['name'] = $input;
	}

	function setDiscount($input) {
		$this->data['discount'] = $input;
	}

	function setTier($input) {
		$this->data['tier'] = $input;
	}


	function applyDiscount($amount=0) {
		$perc = $this->getDiscountPercent();
		return ($amount * $perc);
	}

	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>