<?php

class closeout_spreadsheet {
	var $id;
	var $tableName = 'closeout_spreadsheets';
	var $data = array();


	function __construct($params=array()){
		if(!is_array($params)){
			$params = array('id'=>$params);
		}
		if($params['id'] != ""){
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)){
			$result = mysqlQuery($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getData() {
		return json_decode($this->data['data'], true);
	}

	function getDate() {
		return $this->data['date'];
	}

	function getDateTimeCreated() {
		return $this->data['datetime_created'];
	}


	function setData($input) {
		$this->data['data'] = json_encode($input);
	}


	function dataChecks(){
		if($this->data['datetime_created'] == "") {
			$this->data['datetime_created'] = mysqlDateTime();
		}
		if($this->data['date'] == "") {
			$this->data['date'] = mysqlDate($this->data['datetime_created']);
		}
	}

	function save(){
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete(){
		object_delete($this->tableName, $this->id);
	}
}

?>