<?php

class email {
	var $id;
	var $tableName = 'emails';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)){
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservation() {
		return $this->data['id_reservation'];
	}

	function getFrom() {
		return $this->data['from'];
	}

	function getTo() {
		return $this->data['to'];
	}

	function getCC() {
		return $this->data['cc'];
	}

	function getBCC() {
		return $this->data['bcc'];
	}

	function getReplyTo() {
		return $this->data['reply_to'];
	}

	function getSubject() {
		return $this->data['subject'];
	}

	function getBodyText() {
		return $this->data['body_text'];
	}

	function getBodyHTML() {
		return $this->data['body_html'];
	}

	function getSMTP() {
		return $this->data['smtp'];
	}

	function getDateTimeSent() {
		return $this->data['datetime_sent'];
	}

	function getDateTimeCreated() {
		return $this->data['datetime_created'];
	}


	function setIDreservation($input) {
		$this->data['id_reservation'] = $input;
	}

	function setFrom($input) {
		$this->data['from'] = $input;
	}

	function setTo($input) {
		$this->data['to'] = $input;
	}

	function setCC($input) {
		$this->data['cc'] = $input;
	}

	function setBCC($input) {
		$this->data['bcc'] = $input;
	}

	function setReplyTo($input) {
		$this->data['reply_to'] = $input;
	}

	function setSubject($input) {
		$this->data['subject'] = $input;
	}

	function setBodyText($input) {
		$this->data['body_text'] = $input;
	}

	function setBodyHTML($input) {
		$this->data['body_html'] = $input;
	}

	function setSMTP($input) {
		$this->data['smtp'] = $input;
	}

	function appendError($input) {
		$errors = $this->data['errors'];
		if($errors != "") {
			$errors .= "\n";
		}
		$errors .= $input;
		$this->data['errors'] = $errors;
	}

	function setDateTimeSent($input) {
		$this->data['datetime_sent'] = mysqlDateTime($input);
	}


	function dataChecks() {
		if($this->id == "" && $this->data['datetime_created'] == ""){
			$this->data['datetime_created'] = mysqlDateTime();
		}
		if($this->getFrom() == "") {
			$this->setFrom('info@bundubashers.com');
		}
		if($this->getBodyHTML() != "" && $this->getBodyText() == "") {
			$body_text = str_ireplace('<br>', "\n", $body_text);
			$body_text = str_ireplace('<br />', "\n", $body_text);
			$body_text = strip_tags($this->getBodyHTML());
			$this->setBodyText($body_text);
		}
		if($this->getBodyText() != "" && $this->getBodyHTML() == "") {
			$body_html = nl2br($this->getBodyText());
			$this->setBodyHTML($body_html);
		}
	}

	function send() {
		$this->dataChecks();

		$mail = new PHPMailer;

		/*
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'jswan';                            // SMTP username
		$mail->Password = 'secret';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
		*/

		$fromArry = make_array(imap_rfc822_parse_adrlist($this->getFrom(), $_SERVER['HTTP_HOST']));
		$fromObj = reset($fromArry);
		if(is_object($fromObj) && $fromObj->host != ".SYNTAX-ERROR.") {
			$mail->From = $fromObj->mailbox.'@'.$fromObj->host;
			if(isset($fromObj->personal)) {
				$mail->FromName = $fromObj->personal;
			}
		} else {
			$mail->From = 'info@bundubashers.com';
			$mail->FromName = 'Bundu Bashers';
			$this->appendError('From address "'.$this->getFrom().'" is invalid. Bundu Bashers <info@bundubashers.com> used instead.');
		}

		if($mail->From == "admin@bundubashers.com") {
			$this->setSMTP('hostgator');
			$mail->isSMTP();
			//$mail->SMTPDebug = 2;
			$mail->Debugoutput = 'html';
			$mail->Host = 'mail.bundubashers.com';
			$mail->Port = 25;
			$mail->SMTPAuth = true;
			$mail->Username = $mail->From;
			$mail->Password = "0Gt5Z}mbrMTs";

		} elseif($mail->From == "bundubashers@gmail.com") {
			$this->setSMTP('gmail');
			$mail->isSMTP();
			//$mail->SMTPDebug = 2;
			$mail->Debugoutput = 'html';
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 587;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPAuth = true;
			$mail->Username = $mail->From;
			$mail->Password = "yourpassword";
			
		} else {
			$this->setSMTP('local');
		}

		$toArry = make_array(imap_rfc822_parse_adrlist($this->getTo(), $_SERVER['HTTP_HOST']));
		foreach($toArry as $key => $addressObj) {
			if(is_object($addressObj) && $addressObj->host != ".SYNTAX-ERROR.") {
				$address = $addressObj->mailbox.'@'.$addressObj->host;
				$name = null;
				if(isset($addressObj->personal)) {
					$name = $addressObj->personal;
				}
				$mail->addAddress($address, $name);
			} else {
				$this->appendError('To address '.$key.' is invalid and was not included.');
			}
		}

		$ccArry = make_array(imap_rfc822_parse_adrlist($this->getCC(), $_SERVER['HTTP_HOST']));
		foreach($ccArry as $addressObj) {
			if(is_object($addressObj) && $addressObj->host != ".SYNTAX-ERROR.") {
				$address = $addressObj->mailbox.'@'.$addressObj->host;
				$name = null;
				if(isset($addressObj->personal)) {
					$name = $addressObj->personal;
				}
				$mail->addCC($address, $name);
			} else {
				$this->appendError('CC address '.$key.' is invalid and was not included.');
			}
		}

		$bccArry = make_array(imap_rfc822_parse_adrlist($this->getBCC(), $_SERVER['HTTP_HOST']));
		foreach($bccArry as $addressObj) {
			if(is_object($addressObj) && $addressObj->host != ".SYNTAX-ERROR.") {
				$address = $addressObj->mailbox.'@'.$addressObj->host;
				$name = null;
				if(isset($addressObj->personal)) {
					$name = $addressObj->personal;
				}
				$mail->addBCC($address, $name);
			} else {
				$this->appendError('BCC address '.$key.' is invalid and was not included.');
			}
		}

		if($this->getReplyTo() != "") {
			$replyToArry = make_array(imap_rfc822_parse_adrlist($this->getReplyTo(), $_SERVER['HTTP_HOST']));
			$addressObj = reset($replyToArry);
			if(is_object($addressObj) && $addressObj->host != ".SYNTAX-ERROR.") {
				$address = $addressObj->mailbox.'@'.$addressObj->host;
				$name = null;
				if(isset($addressObj->personal)) {
					$name = $addressObj->personal;
				}
				$mail->addReplyTo($address, $name);
			} else {
				$this->appendError('ReplyTo address "'.$this->getReplyTo().'" is invalid.');
			}
		}

		//$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		$mail->Subject = $this->getSubject();

		$mail->isHTML(true);
		$mail->Body = $this->getBodyHTML();
		$mail->AltBody = $this->getBodyText();

		$result = $mail->send();
		//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($mail,true)).'</PRE>';

		if($result) {
			$this->setDateTimeSent("now");
		} else {
		   $this->appendError(mysqlDateTime().' Message could not be sent: '.$mail->ErrorInfo);
		}
		$this->save();

		return $result;
	}

	function save($params=array()) {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>