<?php

class itinerary {
	var $id;
	var $tableName = 'itinerary';
	var $data = array();


	function __construct($params=array()){
		if(!is_array($params)){
			$params = array('id'=>$params);
		}
		if($params['id'] != ""){
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)){
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getNickname(){
		return $this->data['nickname'];
	}

	function getIDroute(){
		return $this->data['routeid'];
	}

	function getTitle(){
		return $this->data['title'];
	}

	function getIDmapImage(){
		return $this->data['map'];
	}

	function getMiles(){
		return $this->data['ml'];
	}

	function getTimeDescription(){
		return $this->data['time'];
	}

	function getComments(){
		return $this->data['comments'];
	}

	function getYouTubeTitle(){
		return $this->data['youtube_title'];
	}

	function getYouTubeEmbed(){
		return $this->data['youtube'];
	}

	function getStep(){
		return $this->data['step'];
	}


	function setNickname($input){
		$this->data['nickname'] = $input;
	}

	function setIDroute($input){
		$this->data['routeid'] = $input;
	}

	function setTitle($input){
		$prevValue = $this->data['title'];
		$this->data['title'] = $input;
		
		if($prevValue != $input){
			$this->setDateTimeTranslationNeeded("now");
		}
	}

	function setIDmapImage($input){
		$this->data['map'] = $input;
	}

	function setMiles($input){
		$this->data['ml'] = $input;
	}

	function setTimeDescription($input){
		$prevValue = $this->data['time'];
		$this->data['time'] = $input;

		if($prevValue != $input){
			$this->setDateTimeTranslationNeeded("now");
		}
	}

	function setComments($input){
		$prevValue = $this->data['comments'];
		$this->data['comments'] = $input;

		if($prevValue != $input){
			$this->setDateTimeTranslationNeeded("now");
		}
	}

	function setYouTubeTitle($input){
		$prevValue = $this->data['youtube_title'];
		$this->data['youtube_title'] = $input;

		if($prevValue != $input){
			$this->setDateTimeTranslationNeeded("now");
		}
	}

	function setYouTubeEmbed($input){
		$this->data['youtube'] = $input;
	}

	function setStep($input){
		$this->data['step'] = $input;
	}

	function setDateTimeTranslationNeeded($input){
		$this->data['datetime_translation_needed'] = mysqlDateTime($input);
	}


	function calcTitle(){
		if($this->getTitle() == "" && $this->getIDroute() > 0 && $this->getStep() == 1){
			$routeObj = new route($this->getIDroute());
			return $routeObj->getName();
		}
		return $this->getTitle();
	}


	function dataChecks(){
		if($this->data['id_author'] == ""){
			$this->data['id_author'] = @$_SESSION['valid_user']['id'];
		}
		if($this->data['datetime_created'] == ""){
			$this->data['datetime_created'] = mysqlDateTime();
		}
		$this->data['datetime_modified'] = mysqlDateTime();
	}

	function save(){
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete(){
		object_delete($this->tableName, $this->id);
	}
}

?>