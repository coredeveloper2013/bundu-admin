<?php

class location {
	var $id;
	var $tableName = 'locations';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getName() {
		return $this->data['name'];
	}

	function getDescription() {
		return $this->data['desc'];
	}

	function getAddress() {
		return $this->data['address'];
	}

	function getTimezone() {
		return $this->data['timezone_id'];
	}


	function setName($input) {
		$this->data['name'] = $input;
	}

	function setDescription($input) {
		$this->data['desc'] = $input;
	}

	function setAddress($input) {
		$this->data['address'] = $input;
	}

	function setTimezone($input) {
		$this->data['timezone_id'] = $input;
	}


	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>