<?php

class reservation_assoc {
	var $id;
	var $tableName = 'reservations_assoc';
	var $data = array();

	var $tourObj = false;
	var $routeObj = false;
	var $activityObj = false;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysqlQuery($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservation() {
		return $this->data['reservation'];
	}

	function getReference() {
		return $this->data['reference'];
	}

	function getType() {
		return $this->data['type'];
	}

	function getName() {
		return $this->data['name'];
	}

	function getNumAdults() {
		return $this->data['adults'];
	}

	function getNumSeniors() {
		return $this->data['seniors'];
	}

	function getNumChildren() {
		return $this->data['children'];
	}

	function getNumPax() {
		return ($this->getNumAdults() + $this->getNumSeniors() + $this->getNumChildren());
	}

	function getDate() {
		$this->syncDates();
		return $this->data['date_v2'];
	}

	function getDepLocation() {
		return $this->data['dep_loc'];
	}

	function getDepTime() {
		$this->syncDates();
		return $this->data['dep_time_v2'];
	}

	function getArrLocation() {
		return $this->data['arr_loc'];
	}

	function getArrTime() {
		$this->syncDates();
		return $this->data['arr_time_v2'];
	}

	function getPrefTime() {
		return $this->data['preftime'];
	}

	function getIDactivity() {
		return $this->data['activityid'];
	}

	function getIDmarket() {
		return $this->data['market'];
	}

	function getIDtransType() {
		return $this->data['trans_type'];
	}

	function getNotes() {
		return $this->data['notes'];
	}

	function getDriverInfo() {
		return $this->data['driver_info'];
	}

	function getFlagOnlyDateAvailable() {
		return $this->data['onlydateavl'];
	}

	function getAlternateDates() {
		return $this->data['altdates'];
	}

	function getIDlodge() {
		return $this->data['lodgeid'];
	}

	function getNumNights() {
		return $this->data['nights'];
	}

	function getFlagExtLodging() {
		return $this->data['extloding'];
	}

	function getAmount() {
		return $this->data['amount'];
	}

	function getCosts() {
		return $this->data['costs'];
	}

	function getIDtour() {
		return $this->data['tourid'];
	}

	function getIDroute() {
		return $this->data['routeid'];
	}

	function getIDbbTicket() {
		return $this->data['bbticket'];
	}

	function getIDminiRoute() {
		return $this->data['miniroute'];
	}

	function getIDassoc() {
		return $this->data['assoc'];
	}

	function getUsername() {
		return $this->data['username'];
	}

	function getPassword() {
		return $this->data['password'];
	}

	function getNumDays() {
		return $this->data['days'];
	}

	function getIDvendor() {
		return $this->data['vendor'];
	}

	function getVendorConfNum() {
		return $this->data['vendorconf'];
	}

	function getDateVendorPaid() {
		$this->syncDates();
		return $this->data['date_vendor_paid'];
	}

	function getVendorPayAmount() {
		return $this->data['vendorpayamt'];
	}

	function getFlagManualBlocking() {
		return $this->data['flag_manual_blocking'];
	}

	function getDateTimeCanceled() {
		$this->syncDates();
		return $this->data['datetime_canceled'];
	}

	function getTourObj() {
		if($this->tourObj === false) {
			$this->tourObj = new tour($this->getIDtour());
		}
		return $this->tourObj;
	}

	function getRouteObj() {
		if($this->routeObj === false) {
			$this->routeObj = new route($this->getIDroute());
		}
		return $this->routeObj;
	}

	function getActivityObj() {
		if($this->activityObj === false) {
			$this->activityObj = new activity($this->getIDactivity());
		}
		return $this->activityObj;
	}


	function setIDreservation($input) {
		$this->data['reservation'] = $input;
	}

	function setReference($input) {
		$this->data['reference'] = $input;
	}

	function setType($input) {
		$this->data['type'] = $input;
	}

	function setName($input) {
		$this->data['name'] = $input;
	}

	function setNumAdults($input) {
		$this->data['adults'] = $input;
	}

	function setNumSeniors($input) {
		$this->data['seniors'] = $input;
	}

	function setNumChildren($input) {
		$this->data['children'] = $input;
	}

	function setDate($input) {
		$this->data['date_v2'] = mysqlDate($input);
		$this->data['date'] = null;
	}

	function setDepLocation($input) {
		$this->data['dep_loc'] = $input;
	}

	function setDepTime($input) {
		$this->data['dep_time_v2'] = mysqlTime($input);
		$this->data['dep_time'] = null;
	}

	function setArrLocation($input) {
		$this->data['arr_loc'] = $input;
	}

	function setArrTime($input) {
		$this->data['arr_time_v2'] = mysqlTime($input);
		$this->data['arr_time'] = null;
	}

	function setPrefTime($input) {
		$this->data['preftime'] = $input;
	}

	function setIDactivity($input) {
		$this->data['activityid'] = $input;
	}

	function setIDmarket($input) {
		$this->data['market'] = $input;
	}

	function setIDtransType($input) {
		$this->data['trans_type'] = $input;
	}

	function setNotes($input) {
		$this->data['notes'] = $input;
	}

	function setDriverInfo($input) {
		$this->data['driver_info'] = $input;
	}

	function setFlagOnlyDateAvailable($input) {
		if($input) {
			$this->data['onlydateavl'] = 1;
		} else {
			$this->data['onlydateavl'] = 0;
		}
	}

	function setAlternateDates($input) {
		$this->data['altdates'] = $input;
	}

	function setIDlodge($input) {
		$this->data['lodgeid'] = $input;
	}

	function setNumNights($input) {
		$this->data['nights'] = $input;
	}

	function setFlagExtLodging($input) {
		if($input) {
			$this->data['extloding'] = 1;
		} else {
			$this->data['extloding'] = 0;
		}
	}

	function setAmount($input) {
		$this->data['amount'] = $input;
	}

	function setCosts($input) {
		$this->data['costs'] = $input;
	}

	function setIDtour($input) {
		$this->data['tourid'] = $input;
	}

	function setIDroute($input) {
		$this->data['routeid'] = $input;
	}

	function setIDbbTicket($input) {
		$this->data['bbticket'] = $input;
	}

	function setIDminiRoute($input) {
		$this->data['miniroute'] = $input;
	}

	function setIDassoc($input) {
		$this->data['assoc'] = $input;
	}

	function setUsername($input) {
		$this->data['username'] = $input;
	}

	function setPassword($input) {
		$this->data['password'] = $input;
	}

	function setNumDays($input) {
		$this->data['days'] = $input;
	}

	function setIDvendor($input) {
		$this->data['vendor'] = $input;
	}

	function setVendorConfNum($input) {
		$this->data['vendorconf'] = $input;
	}

	function setDateVendorPaid($input, $soft=false) {
		if($this->data['date_vendor_paid'] != "" && $soft) {
			return false;
		}
		$this->data['date_vendor_paid'] = mysqlDate($input);
		$this->data['vendorpaid'] = null;
	}

	function setVendorPayAmount($input) {
		$this->data['vendorpayamt'] = $input;
	}

	function setDateTimeCanceled($input, $soft=false) {
		if($this->data['datetime_canceled'] != "" && $soft) {
			return false;
		}
		$this->data['datetime_canceled'] = mysqlDateTime($input);
		$this->data['canceled'] = null;
	}


	function isCanceled() {
		if($this->getDateTimeCanceled() != "") {
			return true;
		} else {
			return false;
		}
	}

	function calcCost($params=array()) {
		$amount = array(
			'amount' => 0
			);

		if($this->getType() == "t") {
			$tourObj = new tour($this->getIDtour());
			if($tourObj->id > 0) {
				$fuel_surcharge = $tourObj->getFuelSurcharge();

				//Extended pricing is for single day tours only
				$ext = array();
				if($tourObj->getNumDays() == 1 && $tourObj->getExtPricing() != "") {
					$ext = $tourObj->getExtPricingArry();
				}
				if(isset($ext[$this->getNumPax()]) && $ext[$this->getNumPax()] > 0) {
					$amount['amount'] = $ext[$this->getNumPax()];
				} else {
					$amount['amount'] = ($this->getNumPax() * $tourObj->getPricePerGuest());
				}

				$perc = 1;
				if(@$params['id_agent'] != "") {
					$agentObj = new agent($params['id_agent']);
					$perc = $agentObj->getDiscountPercent();
					if($tourObj->getAgentTierPercent($agentObj->getTier()) > 0) {
						$perc = ($tourObj->getAgentTierPercent($agentObj->getTier()) / 100);
					}

				} elseif(@$params['updc'] != "" && $params['updc'] >= 0 && $params['updc'] <= 100) {
					$perc = ($params['updc'] / 100);
					if($params['updc'] == 0) {
						$fuel_surcharge = 0;
					}
				}

				//Set base (and apply discount)
				$amount['base'] = ($amount['amount'] * $perc);

				//Lodging
				$rooms = array();
				if(isset($params['rooms'])) {
					$rooms = make_array($params['rooms']);
					if(isset($rooms['num_guests'])) {
						$rooms = array($rooms);
					}

				} elseif($this->id > 0) {
					$query = 'SELECT numguests AS num_guests FROM reservations_lodging WHERE associd = "'.encodeSQL($this->id).'"';
					$result = mysql_query($query);
					while($row = @mysql_fetch_assoc($result)) {
						$rooms[] = $row;
					}
				}
				if(count($rooms) > 0) {
					$room_num = 0;
					foreach($rooms as $room) {
						if(is_array($room['num_guests'])) {
							continue;
						}
						$room_num++;
						if($room['num_guests'] == 1) {
							$amount['amount'] += $tourObj->getPriceSingle();
							$amount['room'.$room_num] = ($tourObj->getPriceSingle() * $perc);
						}
						if($room['num_guests'] == 3) {
							$amount['amount'] = ($amount['amount'] - $tourObj->getPriceTriple());
							$amount['room'.$room_num] = ($tourObj->getPriceTriple() * $perc);
						}
						if($room['num_guests'] == 4) {
							$amount['amount'] = ($amount['amount'] - $tourObj->getPriceQuad());
							$amount['room'.$room_num] = ($tourObj->getPriceQuad() * $perc);
						}
					}
				} else {
					//If no rooms are defined, we can assume only 1 room is needed
					if($this->getNumPax() == 1) {
						$amount['base'] += $tourObj->getPriceSingle();
						$amount['amount'] += $tourObj->getPriceSingle();
					}
				}

				//Apply discount to whole amount
				$amount['amount'] = ($amount['amount'] * $perc);

				/*
				//Lodging extensions
				if(isset($item['extlodgeid']) && is_array($item['extlodgeid'])){
				foreach($item['extlodgeid'] as $key => $val){ if(isset($item['addextl'.$val]) && $item['addextl'.$val] == "y" && $item['extlodgenights'][$key] > 0){
					$elt = ($item['extlodgeperguest'][$key]*$item['numguests']);
					if($item['numguests'] == 1){
						$elt = ($elt + $item['extlodgesingle'][$key]);
						} elseif($item['numrooms'] == 1 && $item['numguests'] == 3){
						$elt = ($elt - $item['extlodgetriple'][$key]);
						} elseif($item['numrooms'] == 1 && $item['numguests'] == 4){
						$elt = ($elt - $item['extlodgequad'][$key]);
						} elseif($item['numrooms'] > 1){
							for($rm=1; $rm<=$item['numrooms']; $rm++){
								if(@$item['room'.$rm] == 1){
									$elt = ($elt + $item['extlodgesingle'][$key]);
									} elseif(@$item['room'.$rm] == 3){
									$elt = ($elt - $item['extlodgetriple'][$key]);
									} elseif(@$item['room'.$rm] == 4){
									$elt = ($elt - $item['extlodgequad'][$key]);
									}
								}
						}
					$amount['amount'] = ($amount['amount'] + ($elt*$item['extlodgenights'][$key]));
					}}
					}
				*/

				//Add Fuel surchage (per guest)
				$amount['fuel_surcharge'] = ($fuel_surcharge * $this->getNumPax());
				$amount['amount'] += ($fuel_surcharge * $this->getNumPax());
	
				//Activities
				$activities = array();
				if(isset($params['activities'])) {
					$activities = make_array($params['activities']);
					if(isset($activities['id'])) {
						$activities = array($activities);
					}

				} elseif($this->id > 0) {
					$query = 'SELECT activityid AS `id`, (adults + seniors + children) AS num_guests FROM reservations_assoc WHERE reservation = "'.encodeSQL($this->getIDreservation()).'" AND type = "a"';
					$result = mysql_query($query);
					while($row = @mysql_fetch_assoc($result)) {
						$activities[] = $row;
					}
				}
				foreach($activities as $key => $activity) {
					if(is_array($activity['id'])) {
						continue;
					}
					$actObj = new activity($activity['id']);
					$amount['amount'] += ($actObj->getPrice() * $activity['num_guests']);
					$amount['activity'.($key+1)] = ($actObj->getPrice() * $activity['num_guests']);
				}
			}
		}

		if($this->getType() == "r") {
			$routeObj = new route($this->getIDroute());
			if($routeObj->id > 0) {
				$cpm = 0.40;
				$query = 'SELECT `var` FROM `bundubus_settings` WHERE `setting` = "cpm" LIMIT 1';
				$default_cpm = getMysqlValue($query);
				if($default_cpm != "") {
					$cpm = $default_cpm;
				}

				if($this->getIDbbTicket() > 0) {
					$amount['amount'] = 0;

				} elseif($routeObj->getBBprice() > 0) {
					$amount['amount'] = ($routeObj->getBBprice() * $this->getNumPax());

				} else {
					$amount['amount'] = (($cpm * $routeObj->getMiles()) * $this->getNumPax());
				}

				if(@$params['id_agent'] != "") {
					$agentObj = new agent($params['id_agent']);
					$amount['amount'] = $agentObj->applyDiscount($amount['amount']);
				}
			}
		}

		foreach($amount as $key => $val) {
			$amount[$key] = moneyFormat($val);
		}

		if(@$params['returnKind'] == "base") {
			return @$amount['base'];
		}
		if(@$params['returnKind'] == "fuel_surcharge") {
			return @$amount['fuel_surcharge'];
		}
		if(@$params['returnKind'] == "amount") {
			return $amount['amount'];
		}

		return $amount;
	}

	function syncDates() {
		//Make sure both old and new date fields are accurate
		if($this->data['date'] > 0 && $this->data['date_v2'] == "") {
			$this->data['date_v2'] = mysqlDate($this->data['date']);
		}
		if($this->data['date'] <= 0 && $this->data['date_v2'] != "") {
			$this->data['date'] = strtotime($this->data['date_v2']);
		}
		if($this->data['date'] == "" && $this->data['date_v2'] == "") {
			$this->data['date'] = 0;
		}

		if($this->data['dep_time'] > -1 && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time_v2'] = mysqlTime($this->data['dep_time']);
		}
		if($this->data['dep_time'] <= 0 && $this->data['dep_time_v2'] != "") {
			$this->data['dep_time'] = strtotime($this->data['dep_time_v2'], $this->data['date']);
		}
		if($this->data['dep_time'] == "" && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time'] = 0;
		}

		if($this->data['arr_time'] > -1 && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time_v2'] = mysqlTime($this->data['arr_time']);
		}
		if($this->data['arr_time'] <= 0 && $this->data['arr_time_v2'] != "") {
			$this->data['arr_time'] = strtotime($this->data['arr_time_v2'], $this->data['date']);
		}
		if($this->data['arr_time'] == "" && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time'] = 0;
		}

		if($this->data['vendorpaid'] > 0 && $this->data['date_vendor_paid'] == "") {
			$this->data['date_vendor_paid'] = mysqlDate($this->data['vendorpaid']);
		}
		if($this->data['vendorpaid'] <= 0 && $this->data['date_vendor_paid'] != "") {
			$this->data['vendorpaid'] = strtotime($this->data['date_vendor_paid']);
		}
		if($this->data['vendorpaid'] == "" && $this->data['date_vendor_paid'] == "") {
			$this->data['vendorpaid'] = 0;
		}

		if($this->data['canceled'] > 0 && $this->data['datetime_canceled'] == "") {
			$this->data['datetime_canceled'] = mysqlDateTime($this->data['canceled']);
		}
		if($this->data['canceled'] <= 0 && $this->data['datetime_canceled'] != "") {
			$this->data['canceled'] = strtotime($this->data['datetime_canceled']);
		}
		if($this->data['canceled'] == "" && $this->data['datetime_canceled'] == "") {
			$this->data['canceled'] = 0;
		}
	}

	function setupBlocks() {
		if($this->id == "") {
			return false;
		}
		if($this->isCanceled()) {
			return false;
		}
		if($this->getFlagManualBlocking() == "1") {
			return false;
		}

		if($this->getIDtour() > 0) {
			$tourDateObj = new tour_date(array(
				'id_tour' => $this->getIDtour(),
				'date' => $this->getDate()
				));
			if($tourDateObj->id > 0) {
				$tourObj = new tour($tourDateObj->getIDtour());
				$routes = $tourObj->get_routes(array(
					'direction' => $tourDateObj->getDirection()
					));

				$extLodging = array();
				//$query = 'SELECT id FROM reservations_assoc WHERE assoc = "'.$this->id.'" AND extlodging = 1 ORDER BY date ASC, date_v2 ASC';

				//We have to assume for the moment that the assoc field does not have the right data.  We'll use this as a fix for now:
				$query = 'SELECT id FROM reservations_assoc
							WHERE reservation = "'.$this->getIDreservation().'" AND extlodging = 1
								AND (canceled IS NULL OR canceled = 0) AND datetime_canceled IS NULL
							ORDER BY date ASC, date_v2 ASC';
				$result = @mysqlQuery($query);
				while($row = @mysql_fetch_assoc($result)) {
					$extAssocObj = new reservation_assoc($row['id']);
					$subQuery = 'SELECT id FROM tours_extlodging WHERE lodgeid = "'.mysql_real_escape_string($extAssocObj->getIDlodge()).'" LIMIT 1';
					$subResult = @mysqlQuery($subQuery);
					$data = @mysql_fetch_assoc($subResult);

					if($data['id'] > 0) {
						$subQuery = 'SELECT `order` FROM tours_assoc
							WHERE tourid = "'.mysql_real_escape_string($this->getIDtour()).'"
								AND dir = "'.mysql_real_escape_string($tourDateObj->getDirection()).'"
								AND type = "e" AND typeid = "'.mysql_real_escape_string($data['id']).'"
							LIMIT 1';
						$subResult = @mysqlQuery($subQuery);
						$data = @mysql_fetch_assoc($subResult);
						$order = @$data['order'];
					}

					$extLodging[] = array(
						'order' => @$order,
						'date' => $extAssocObj->getDate(),
						'nights' => $extAssocObj->getNumNights()
						);
					unset($extAssocObj);
				}
				//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($extLodging,true)).'</PRE>';

				foreach($routes[$tourDateObj->getDirection()] as $route) {
					//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($route,true)).'</PRE>';
					$routeObj = new route($route['typeid']);

					$blockObj = new block();
					$blockObj->setIDreservationAssoc($this->id);
					$blockObj->setIDtour($this->getIDtour());
					$blockObj->setIDroute($routeObj->id);
					$blockDate = mysqlDate($tourDateObj->getDate().' + '.($route['day']-1).' days');
						foreach($extLodging as $ext) {
							//echo $route['order'].' : '.$ext['order'].', '.mysqlDate($ext['date']).' : '.mysqlDate($blockDate).' - '.$routeObj->getName().'<br>';
							if($route['order'] > $ext['order'] || mysqlDate($ext['date']) < mysqlDate($blockDate)) {
								$blockDate = mysqlDate($blockDate.' + '.$ext['nights'].' days');
								//echo 'Adjusting date by '.$ext['nights'].' for extended lodging.<br>';
							}
						}
					$blockObj->setDate($blockDate);
					$blockObj->setTime($routeObj->getDepTime());
					$blockObj->setSeats($this->getNumPax());
					$blockObj->save();
					//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($blockObj,true)).'</PRE>';

					unset($routeObj, $blockObj);
				}
				unset($tourObj);
			}
		}

		if($this->getIDroute() > 0) {
			$blockObj = new block();
			$blockObj->setIDreservationAssoc($this->id);
			$blockObj->setIDroute($this->getIDroute());
			$blockObj->setDate($this->getDate());
			$blockObj->setTime($this->getDepTime());
			$blockObj->setSeats($this->getNumPax());
			$blockObj->save();
			unset($blockObj);
		}

		log_audit(array(
			'primary_id' => $this->id,
			'table' => $this->tableName,
			'comment' => 'Blocks were set up.'
			));
	}

	function clearBlocks() {
		if($this->id == "") {
			return false;
		}
		if($this->getFlagManualBlocking() == "1") {
			return false;
		}

		$query = 'SELECT id FROM blocks WHERE id_reservation_assoc = "'.$this->id.'"';
		$result = @mysqlQuery($query);
		while($row = @mysql_fetch_assoc($result)) {
			$blockObj = new block($row['id']);
			$blockObj->delete();
			unset($blockObj);
		}

		log_audit(array(
			'primary_id' => $this->id,
			'table' => $this->tableName,
			'comment' => 'Blocks were cleared.'
			));
	}

	function clearDateConfirmation() {
		// If reservation is for a confirmed tour date, make sure there is at least 1 reservation still using the date.
		// If not, remove the confirmation.
		if($this->getIDtour() == "") {
			return false;
		}

		$tourDateObj = new tour_date(array(
			'id_tour' => $this->getIDtour(),
			'date' => $this->getDate()
			));
		if($tourDateObj->getConfirmed() != "1") {
			return false;
		}

		$query = 'SELECT id
					FROM reservations_assoc
					WHERE tourid = "'.mysql_real_escape_string($this->getIDtour()).'"
						AND (date = "'.strtotime($this->getDate()).'" OR date_v2 = "'.$this->getDate().'")
						AND canceled < 1 AND datetime_canceled IS NULL
					LIMIT 1';
		$id_res_assoc = getMysqlValue($query);
		if($id_res_assoc == "") {
			$tourDateObj->setConfirmed(false);
			$tourDateObj->save();
			log_audit(array(
				'primary_id' => $tourDateObj->id,
				'table' => $tourDateObj->tableName,
				'comment' => 'clearDateConfirmation(): Confirmation was set to 0 by res assoc "'.$this->id.'".'
				));
		}
	}

	function dataChecks() {
		$this->syncDates();

		if($this->getType() == "t") {
			if($this->getReference() == "") {
				$tourObj = $this->getTourObj();
				$this->setReference('[Bundu Tour '.$this->getIDtour().'] '.$tourObj->getTitle());
			}
			if($this->getDepTime() == "") {
				$tourDateObj = new tour_date(array(
					'id_tour' => $this->getIDtour(),
					'date' => $this->getDate()
					));
				$routes = $tourDateObj->getRoutes();
				$first_route = reset($routes);
				$routeDateObj = new route_date(array(
					'id_route' => $first_route['id'],
					'date' => $first_route['date']
					));
				$this->setDepTime($routeDateObj->calcDepTime());
			}
			if($this->getIDvendor() == "") {
				$tourObj = $this->getTourObj();
				$this->setIDvendor($tourObj->getIDvendor());
			}
		}

		if($this->getType() == "r") {
			if($this->getReference() == "") {
				$routeObj = $this->getRouteObj();
				$this->setReference($routeObj->getName());
			}
			if($this->getDepLocation() == "") {
				$routeObj = $this->getRouteObj();
				$this->setDepLocation($routeObj->getIDdepLocation());
			}
			if($this->getDepTime() == "") {
				$routeDateObj = new route_date(array(
					'id_route' => $this->getIDroute(),
					'date' => $this->getDate()
					));
				$this->setDepTime($routeDateObj->calcDepTime());
			}
			if($this->getArrLocation() == "") {
				$routeObj = $this->getRouteObj();
				$this->setArrLocation($routeObj->getIDarrLocation());
			}
			if($this->getArrTime() == "") {
				$routeDateObj = new route_date(array(
					'id_route' => $this->getIDroute(),
					'date' => $this->getDate()
					));
				$this->setArrTime($routeDateObj->calcArrTime());
			}
			if($this->getIDvendor() == "") {
				$routeObj = $this->getRouteObj();
				$this->setIDvendor($routeObj->getIDvendor());
			}
		}

		if($this->getType() == "a") {
			if($this->getReference() == "") {
				$actObj = $this->getActivityObj();
				$this->setReference($actObj->getName());
			}
			if($this->getIDvendor() == "") {
				$actObj = $this->getActivityObj();
				$this->setIDvendor($actObj->getIDvendor());
			}
		}

		if($this->data['datetime_created'] == "" && $this->id < 45000) {
			$resObj = new reservation($this->getIDreservation());
			$this->data['datetime_created'] = mysqlDateTime($resObj->getDateTimeBooked());
			unset($resObj);
		}
		if($this->data['datetime_created'] == "") {
			$this->data['datetime_created'] = mysqlDateTime();
		}
	}

	function save($params=array()) {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;

		if(@$params['setup_blocks'] !== false) {
			$this->clearBlocks();
			$this->setupBlocks();
		}
		if($this->isCanceled()) {
			$this->clearDateConfirmation();
		}
	}

	function delete() {
		$this->clearBlocks();
		object_delete($this->tableName, $this->id);
	}
}

?>