<?php

class reservation_guest {
	var $id;
	var $tableName = 'reservations_guests';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservationAssoc() {
		return $this->data['associd'];
	}

	function getName() {
		return $this->data['name'];
	}

	function getWeight() {
		return $this->data['weight'];
	}

	function getLunch() {
		return $this->data['lunch'];
	}


	function setIDreservationAssoc($input) {
		$this->data['associd'] = $input;
	}

	function setName($input) {
		$this->data['name'] = $input;
	}

	function setWeight($input) {
		$this->data['weight'] = $input;
	}

	function setLunch($input) {
		$this->data['lunch'] = $input;
	}


	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>