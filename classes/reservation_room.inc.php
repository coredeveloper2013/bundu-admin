<?php

class reservation_room {
	var $id;
	var $tableName = 'reservations_lodging';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getIDreservationAssoc() {
		return $this->data['associd'];
	}

	function getRoomNum() {
		return $this->data['room'];
	}

	function getNumGuests() {
		return $this->data['numguests'];
	}

	function getNumBeds() {
		return $this->data['numbeds'];
	}

	function getAdjust() {
		return $this->data['adjust'];
	}


	function setIDreservationAssoc($input) {
		$this->data['associd'] = $input;
	}

	function setRoomNum($input) {
		$this->data['room'] = $input;
	}

	function setNumGuests($input) {
		$this->data['numguests'] = $input;
	}

	function setNumBeds($input) {
		$this->data['numbeds'] = $input;
	}

	function setAdjust($input) {
		$this->data['adjust'] = $input;
	}


	function dataChecks() {
		//None... yet
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		object_delete($this->tableName, $this->id);
	}
}

?>