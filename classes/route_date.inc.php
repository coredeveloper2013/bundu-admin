<?php

// route_date.inc.php

class route_date {
	var $id;
	var $data = array();
	var $tableName = 'routes_dates';

	var $routeObj = false;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id_route'] != "" && $params['date'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE routeid = "'.mysql_real_escape_string($params['id_route']).'"';
				if(is_numeric($params['date'])) {
					$timestamp = $params['date'];
				} else {
					$timestamp = strtotime($params['date']);
				}
				$query .= ' AND (date = "'.mysql_real_escape_string($timestamp).'" OR date_v2 = "'.mysql_real_escape_string($params['date']).'")';
				$query .= ' LIMIT 1';
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
		if($this->data['routeid'] == "") {
			$this->data['routeid'] = $params['id_route'];
		}
		if($this->getDate() == "") {
			$this->setDate($params['date']);
		}
	}


	function getIDroute() {
		return $this->data['routeid'];
	}

	function getDate() {
		$this->syncDates();
		return $this->data['date_v2'];
	}

	function getDepTime() {
		$this->syncDates();
		return $this->data['dep_time_v2'];
	}

	function getArrTime() {
		$this->syncDates();
		return $this->data['arr_time_v2'];
	}

	function getNumSeats() {
		return $this->data['seats'];
	}

	function getMinimum() {
		return $this->data['minimum'];
	}

	function getNotes() {
		return $this->data['notes'];
	}

	function calcDepTime() {
		if($this->getDepTime() != "") {
			return $this->getDepTime();
		} else {
			$this->setupRouteObj();
			return $this->routeObj->getDepTime();
		}
	}

	function calcArrTime() {
		if($this->getArrTime() != "") {
			return $this->getArrTime();
		} else {
			$this->setupRouteObj();
			return $this->routeObj->getArrTime();
		}
	}

	function calcNumSeats() {
		if($this->getNumSeats() > -1) {
			return $this->getNumSeats();
		} else {
			$this->setupRouteObj();
			return $this->routeObj->getNumSeats();
		}
	}


	function setIDroute($input) {
		$this->data['routeid'] = $input;
	}

	function setDate($input) {
		$this->data['date_v2'] = $input;
		$this->data['date'] = null;
	}

	function setDepTime($input) {
		$this->data['dep_time_v2'] = $input;
		$this->data['dep_time'] = null;
	}

	function setArrTime($input) {
		$this->data['arr_time_v2'] = $input;
		$this->data['arr_time'] = null;
	}

	function setSeats($input) {
		$this->data['seats'] = $input;
	}

	function setMinimum($input) {
		$this->data['minimum'] = $input;
	}

	function setNotes($input) {
		$this->data['notes'] = $input;
	}


	function setupRouteObj() {
		if($this->routeObj === false) {
			$this->routeObj = new route($this->getIDroute());
		}
	}

	function getBlocked($params=array()) {
		$blocked = array(
			'seats' => 0,
			'route_seats' => 0,
			'tour_seats' => 0,
			'reservations' => array(),
			'reservation_ids' => array(),
			'reservation_assoc_ids' => array()
			);
		$this->setupRouteObj();

		if(@$this->routeObj->id > 0) {
			//Gather route reservations
			$query = 'SELECT reservations.id AS reservation_id, ra.id AS reservation_assoc_id, (ra.adults + ra.seniors + ra.children) AS pax
				FROM reservations_assoc AS ra
				JOIN reservations ON ra.reservation = reservations.id
				WHERE (ra.type = "r" OR ra.type = "o") AND ra.routeid = "'.$this->routeObj->id.'"
					AND (ra.date = "'.$this->data['date'].'" OR ra.date_v2 = "'.$this->data['date_v2'].'")
					AND reservations.canceled < 1 AND ra.canceled < 1';
			$result = @mysql_query($query);
			while($row = @mysql_fetch_assoc($result)) {
				array_push($blocked['reservations'], $row);
				array_push($blocked['reservation_ids'], $row['reservation_id']);
				array_push($blocked['reservation_assoc_ids'], $row['reservation_assoc_id']);
				$blocked['route_seats'] += $row['pax'];
				$blocked['seats'] += $row['pax'];
			}

			//Gather tour reservations
			$tourIDs = $this->routeObj->usedby_tours();
			if(count($tourIDs) > 0) {
				$longest_tour = longest_tour($tourIDs); //days
				$longest_ext = longest_tour_extension(); //nights
	
				$query = 'SELECT reservations.id AS reservation_id, ra.id AS reservation_assoc_id, (ra.adults + ra.seniors + ra.children) AS pax
					FROM reservations_assoc AS ra
					JOIN reservations ON ra.reservation = reservations.id
					WHERE (ra.type = "t" OR ra.type = "o") AND ra.tourid IN ('.implode(',', $tourIDs).'")
						AND (
							#ra.date = "'.$this->data['date'].'" OR ra.date_v2 = "'.$this->data['date_v2'].'"
							#find tour date based on route date
							)
						AND reservations.canceled < 1 AND ra.canceled < 1';
			}

		}

		return $blocked;
	}



/*
	function usedby_reservations($params=array()) {
		$resIDs = array();
		$tourIDs_f = $this->usedby_tours(array('direction'=>'f'));
		$tourIDs_r = $this->usedby_tours(array('direction'=>'r'));
		
		$query = 'SELECT ra.reservation
			FROM reservations_assoc AS ra
			JOIN reservations ON ra.reservation = reservations.id
			WHERE (ra.type = "r" OR ra.type = "o") AND ra.routeid = "'.$this->id.'"
				AND reservations.canceled < 1 AND ra.canceled < 1';
		$result = @mysql_query($query);
		while($row = @mysql_fetch_assoc($result)) {
			array_push($resIDs, $row['reservation']);
		}
		return $resIDs;
	}
*/



	function syncDates() {
		//Make sure both old and new date fields are accurate
		if($this->data['date'] > 0 && $this->data['date_v2'] == "") {
			$this->data['date_v2'] = mysqlDate($this->data['date']);
		}
		if($this->data['date'] <= 0 && $this->data['date_v2'] != "") {
			$this->data['date'] = strtotime($this->data['date_v2']);
		}
		if($this->data['date'] == "" && $this->data['date_v2'] == "") {
			$this->data['date'] = 0;
		}

		if($this->data['dep_time'] > -1 && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time_v2'] = mysqlTime(strtotime('2010-1-1') + $this->data['dep_time']);
		}
		if($this->data['dep_time'] <= 0 && $this->data['dep_time_v2'] != "") {
			$this->data['dep_time'] = strtotime($this->data['dep_time_v2']);
		}
		if($this->data['dep_time'] == "" && $this->data['dep_time_v2'] == "") {
			$this->data['dep_time'] = 0;
		}

		if($this->data['arr_time'] > -1 && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time_v2'] = mysqlTime(strtotime('2010-1-1') + $this->data['arr_time']);
		}
		if($this->data['arr_time'] <= 0 && $this->data['arr_time_v2'] != "") {
			$this->data['arr_time'] = strtotime($this->data['arr_time_v2']);
		}
		if($this->data['arr_time'] == "" && $this->data['arr_time_v2'] == "") {
			$this->data['arr_time'] = 0;
		}
	}


	function dataChecks() {
		$this->syncDates();
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;
	}

	function delete() {
		if($this->id > 0) {
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
		}
	}
}

?>