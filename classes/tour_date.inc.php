<?php

// tour_date.inc.php

class tour_date {
	var $id;
	var $data = array();
	var $tableName = 'tours_dates';
	var $tourObj = false;

	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id_tour'] != "" && $params['date'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE tourid = "'.mysql_real_escape_string($params['id_tour']).'"';
				if(is_numeric($params['date'])) {
					$timestamp = $params['date'];
				} else {
					$timestamp = strtotime($params['date']);
				}
				$query .= ' AND (date = "'.mysql_real_escape_string($timestamp).'" OR date_v2 = "'.mysql_real_escape_string($params['date']).'")';
				$query .= ' LIMIT 1';
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
		if($this->getIDtour() == "") {
			$this->setIDtour($params['id_tour']);
		}
		if($this->getDate() == "") {
			$this->setDate($params['date']);
		}
	}


	function getIDtour() {
		return $this->data['tourid'];
	}

	function getDate() {
		$this->syncDates();
		return $this->data['date_v2'];
	}

	function getDirection() {
		return $this->data['dir'];
	}

	function getConfirmed() {
		return $this->data['confirmed'];
	}

	function getSeatsCache() {
		if(is_null($this->data['seats_cache'])) {
			$this->cacheSeatsAndBlocked();
		}
		return $this->data['seats_cache'];
	}

	function getBlockedCache() {
		if(is_null($this->data['blocked_cache'])) {
			$this->cacheSeatsAndBlocked();
		}
		return $this->data['blocked_cache'];
	}


	function setIDtour($input) {
		$this->data['tourid'] = $input;
	}

	function setDate($input) {
		$this->data['date_v2'] = mysqlDate($input);
		$this->data['date'] = null;
	}

	function setDirection($input) {
		if(strtolower($input) == "r") {
			$this->data['dir'] = 'r';
		} else {
			$this->data['dir'] = 'f';
		}
	}

	function setConfirmed($input) {
		if($input || $input == "1") {
			$this->data['confirmed'] = 1;
		} else {
			$this->data['confirmed'] = 0;
		}
	}

	function setSeatsCache($input) {
		$this->data['seats_cache'] = $input;
	}

	function setBlockedCache($input) {
		$this->data['blocked_cache'] = $input;
	}


	function getTourObj() {
		if($this->tourObj === false) {
			$this->tourObj = new tour($this->getIDtour());
		}
		return $this->tourObj;
	}

	function getRoutes() {
		$routes = array();

		$tourObj = $this->getTourObj();
		$routes_assoc = $tourObj->get_routes(array('dir'=>$this->getDirection()));
		foreach($routes_assoc as $route) {
			$routes[] = array(
				'id' => $route['typeid'],
				'date' => mysqlDate($this->getDate().' + '.($route['day']-1).' days')
				);
		}

		return $routes;
	}

	function cacheSeatsAndBlocked() {
		$num_seats = null;
		$num_blocked = 0;

		$routes = $this->getRoutes();
		foreach($routes as $route) {
			//Find the lowest number of seats from routes during the tour
			$query = 'SELECT r.seats, d.seats AS `seats_override`
						FROM routes AS r
						JOIN routes_dates AS d ON r.id = d.routeid
						WHERE r.id = "'.encodeSQL($route['id']).'"
							AND (d.date_v2 = "'.encodeSQL($route['date']).'" OR d.date = "'.encodeSQL(strtotime($route['date'])).'")
						LIMIT 1';
			$result = mysqlQuery($query);
			$data = @mysql_fetch_assoc($result);
			if($data !== false) {
				$seats = ($data['seats_override']>-1?$data['seats_override']:$data['seats']);
				if(is_null($num_seats) || $seats < $num_seats) {
					$num_seats = $seats;
				}
			}
			//Find the highest number blocked from routes during the tour
			$query = 'SELECT SUM(seats)
						FROM blocks
						WHERE id_route = "'.encodeSQL($route['id']).'" AND date = "'.encodeSQL($route['date']).'"';
			$blocked = getMysqlValue($query);
			if($blocked > $num_blocked) {
				$num_blocked = $blocked;
			}
		}

		$this->setSeatsCache($num_seats);
		$this->setBlockedCache($num_blocked);
	}


	function setupRouteDatesConfirmed() {
		if($this->id == "") {
			return false;
		}
		if($this->getIDtour() == "") {
			return false;
		}
		if($this->getConfirmed() != "1") {
			return false;
		}

		$tourObj = new tour($this->getIDtour());
		$routes = $tourObj->get_routes(array(
			'direction' => $this->getDirection()
			));

		foreach($routes[$this->getDirection()] as $route) {
			//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($route,true)).'</PRE>';
			$routeObj = new route($route['typeid']);

			$rdcObj = new route_date_confirmed();
			$rdcObj->setIDroute($routeObj->id);
			$date = mysqlDate($this->getDate().' + '.($route['day']-1).' days');
			$rdcObj->setDate($date);
			$rdcObj->setIDtourDate($this->id);
			$rdcObj->save();
			//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($rdcObj,true)).'</PRE>';

			unset($routeObj, $rdcObj);
		}
		unset($tourObj);
	}

	function clearRouteDatesConfirmed() {
		if($this->id == "") {
			return false;
		}

		$query = 'SELECT id FROM route_dates_confirmed WHERE id_tour_date = "'.$this->id.'"';
		$result = @mysql_query($query);
		while($row = @mysql_fetch_assoc($result)) {
			$rdcObj = new route_date_confirmed($row['id']);
			$rdcObj->delete();
			unset($rdcObj);
		}
	}

	function syncDates() {
		//Make sure both old and new date fields are accurate
		if($this->data['date'] > 0 && !is_date($this->data['date_v2'])) {
			$this->data['date_v2'] = mysqlDate($this->data['date']);
		}
		if($this->data['date'] <= 0 && is_date($this->data['date_v2'])) {
			$this->data['date'] = strtotime($this->data['date_v2']);
		}
		if($this->data['date'] == "" && $this->data['date_v2'] == "") {
			$this->data['date'] = 0;
		}
	}

	function dataChecks() {
		$this->syncDates();
		$this->cacheSeatsAndBlocked();
	}

	function save() {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;

		$this->clearRouteDatesConfirmed();
		$this->setupRouteDatesConfirmed();
	}

	function delete() {
		if($this->id > 0) {
			$delete = 'DELETE FROM '.$this->tableName.' WHERE id = "'.$this->id.'" LIMIT 1';
			$result = mysql_query($delete);
			$this->clearRouteDatesConfirmed();
		}
	}
}

?>