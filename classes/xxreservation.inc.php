<?php

class reservation {
	var $id;
	var $tableName = 'reservations';
	var $data = array();


	function __construct($params=array()) {
		if(!is_array($params)) {
			$params = array('id'=>$params);
		}
		if($params['id'] != "") {
			$query = 'SELECT * FROM '.$this->tableName.' WHERE id = "'.mysql_real_escape_string($params['id']).'" LIMIT 1';
		}
		if(isset($query)) {
			$result = mysql_query($query);
			$this->data = mysql_fetch_assoc($result);
			$this->id = $this->data['id'];
		}
	}


	function getName() {
		return $this->data['name'];
	}

	function getPhoneHomeBusiness() {
		return $this->data['phone_homebus'];
	}

	function getPhoneCell() {
		return $this->data['phone_cell'];
	}

	function getPhoneCellCountry() {
		return $this->data['cell_country'];
	}

	function getEmail() {
		return $this->data['email'];
	}

	function getEmailInstructions() {
		return $this->data['email_inst'];
	}

	function getDateTimeConfSent() {
		$this->syncDates();
		return $this->data['datetime_conf_sent'];
	}

	function getAmount() {
		return $this->data['amount'];
	}

	function getCCname() {
		return $this->data['cc_name'];
	}

	function getCCnumber() {
		$cc_num = enc_decrypt($this->data['cc_num_enc'], '4f74831f8e47e');
		if($cc_num == "") {
			$cc_num = $this->data['cc_num'];
		}
		return $cc_num;
	}

	function getCCexpDate() {
		return $this->data['cc_expdate'];
	}

	function getCCsecurityCode() {
		return $this->data['cc_scode'];
	}

	function getCCzip() {
		return $this->data['cc_zip'];
	}

	function getPayMethod() {
		return $this->data['pay_method'];
	}

	function getAlternateName() {
		return $this->data['alt_name'];
	}

	function getComments() {
		return $this->data['comments'];
	}

	function getDateTimeCardRun() {
		$this->syncDates();
		return $this->data['datetime_card_run'];
	}

	function getPNPorderID() {
		return $this->data['pnp_orderid'];
	}

	function getConfirmationDetails() {
		return $this->data['conf_details'];
	}

	function getIDagent() {
		return $this->data['agent'];
	}

	function getAgentConfirmation() {
		return $this->data['agent_conf'];
	}

	function getReferral() {
		return $this->data['referral'];
	}

	function getNotes() {
		return $this->data['notes'];
	}

	function getHTTPreferer() {
		return $this->data['http_referer'];
	}

	function getBooker() {
		return $this->data['booker'];
	}

	function getDateTimeBooked() {
		$this->syncDates();
		return $this->data['datetime_booked'];
	}

	function getDateTimeCanceled() {
		$this->syncDates();
		return $this->data['datetime_canceled'];
	}

	function getDateTimeCreated() {
		return $this->data['datetime_created'];
	}

	function getAssocIDs() {
		$query = 'SELECT id FROM reservations_assoc WHERE reservation = "'.$this->id.'"';
		return getMysqlValues($query);
	}

	function getAssocObjs() {
		$assocIDs = make_array($this->getAssocIDs());
		$outArry = array();
		foreach($assocIDs as $id_assoc) {
			$outArry[] = new reservation_assoc($id_assoc);
		}
		return $outArry;
	}


	function setName($input) {
		$this->data['name'] = $input;
	}

	function setPhoneHomeBusiness($input) {
		$this->data['phone_homebus'] = $input;
	}

	function setPhoneCell($input) {
		$this->data['phone_cell'] = $input;
	}

	function setPhoneCellCountry($input) {
		$this->data['cell_country'] = $input;
	}

	function setEmail($input) {
		$this->data['email'] = trim($input);
	}

	function setEmailInstructions($input) {
		$this->data['email_inst'] = $input;
	}


	function setDateTimeConfSent($input) {
		$this->data['datetime_conf_sent'] = mysqlDateTime($input);
		$this->data['email_conf'] = null;
	}

	function setAmount($input) {
		$this->data['amount'] = $input;
	}

	function setCCname($input) {
		$this->data['cc_name'] = $input;
	}

	function setCCnumber($input) {
		$ccnum = preg_replace('/\D/', '', $input);
		if(is_numeric($input) && strpos($input, '*') === false
			&& strlen($ccnum) >= 15 && strlen($ccnum) <= 16
			) {
			$this->data['cc_num_enc'] = enc_encrypt($ccnum, '4f74831f8e47e');
			$input = substr($ccnum, 0, 4).'**'.substr($ccnum, -2);
		} else {
			$this->data['cc_num_enc'] = null;
		}
		$this->data['cc_num'] = $input;
	}

	function setCCexpDate($input) {
		$this->data['cc_expdate'] = $input;
	}

	function setCCsecurityCode($input) {
		$this->data['cc_scode'] = $input;
	}

	function setCCzip($input) {
		$this->data['cc_zip'] = $input;
	}

	function setPayMethod($input) {
		$this->data['pay_method'] = $input;
	}

	function setAlternateName($input) {
		$this->data['alt_name'] = $input;
	}

	function setComments($input) {
		$this->data['comments'] = $input;
	}

	function setDateTimeCardRun($input) {
		$this->data['datetime_card_run'] = mysqlDateTime($input);
		$this->data['card_run'] = null;
	}

	function setPNPorderID($input) {
		$this->data['pnp_orderid'] = $input;
	}

	function setConfirmationDetails($input) {
		$this->data['conf_details'] = $input;
	}

	function setIDagent($input) {
		$this->data['agent'] = $input;
	}

	function setIDsubAgent($input) {
		$this->data['id_subagent'] = $input;
	}

	function setAgentConfirmation($input) {
		$this->data['agent_conf'] = $input;
	}

	function setReferral($input) {
		$this->data['referral'] = $input;
	}

	function setNotes($input) {
		$this->data['notes'] = $input;
	}

	function setHTTPreferer($input) {
		$this->data['http_referer'] = $input;
	}

	function setBooker($input) {
		$this->data['booker'] = $input;
	}

	function setDateTimeBooked($input) {
		$this->data['datetime_booked'] = mysqlDateTime($input);
		$this->data['date_booked'] = null;
	}

	function setDateTimeCanceled($input, $soft=false) {
		if($this->data['datetime_canceled'] != "" && $soft) {
			return false;
		}
		$this->data['datetime_canceled'] = mysqlDateTime($input);
		$this->data['canceled'] = null;
	}


	function isCanceled() {
		if($this->getDateTimeCanceled() != "") {
			return true;
		} else {
			return false;
		}
	}

	function summary() {
		/*
		$email_subject = 'New '.implode('/',$email_restypes).' Reservation #'.$confnum;
		$email_to = 'Bundu Bashers Tours <bundubashers@gmail.com>';
		if(isDev()) {
			$email_to = 'BWS Testing <dominick@bernalwebservices.com>';
		}
		$email_from = 'Bundu Admin <admin@bundubashers.com>';
		$email_replyto = $_POST['name']." <".$_POST['email'].">";
		$email_body .= 'Reservation number: <B>'.$confnum.'</B><BR>'."\n";
		$email_body .= 'Name: '.$_POST['name'].'<BR>'."\n";
		$email_body .= 'Home/Bus. Phone: '.$_POST['phone_homebus'].'<BR>'."\n";
		$email_body .= 'Cell Phone: '.$_POST['phone_cell'].'<BR>'."\n";
		$email_body .= 'Cell Country: '.$_POST['cell_country'].'<BR>'."\n";
		$email_body .= 'Email: '.$_POST['email'].'<BR><BR>'."\n\n";
		$email_body .= 'Comments: '.$_POST['comments'].'<BR><BR>'."\n\n";
		$email_body .= $summary;

		$emailObj = new email();
		$emailObj->setIDreservation($confnum);
		$emailObj->setTo($email_to);
		$emailObj->setFrom($email_from);
		$emailObj->setReplyTo($email_replyto);
		$emailObj->setSubject($email_subject);
		$emailObj->setBodyHTML($email_body);
		$result = $emailObj->send();
		if($result) {
			log_audit(array(
				'primary_id' => $confnum,
				'table' => 'reservations',
				'data' => null,
				'comment' => 'Successfully sent merchant email notification.'
				));
		} else {
			array_push($errormsg,'Unable to send merchant notification.');
		}
		*/
	}
	
	function sendMerchantNotification($params=array()) {
		$email_restypes = $params['email_restypes'];
		$cart_summary = $params['cart_summary'];
		
		
		$email_subject = 'New ';
		if(is_array($email_restypes)) {
			$email_subject .= implode('/',$email_restypes);
		}
		$email_subject .= ' Reservation #'.$this->id;
		$email_to = 'Bundu Bashers Tours <bundubashers@gmail.com>';
		if(isDev()) {
			$email_to = 'Michael Berding <mberding@gmail.com>';
		}
		$email_from = 'Bundu Admin <admin@bundubashers.com>';
		$email_replyto = $this->getName()." <".$this->getEmail().">";
		$email_body = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$email_body .= 'Reservation number: <B>'.$this->id.'</B><BR>'."\n";
			$email_body .= 'Name: '.$this->getName().'<BR>'."\n";
			$email_body .= 'Home/Bus. Phone: '.$this->getPhoneHomeBusiness().'<BR>'."\n";
			$email_body .= 'Cell Phone: '.$this->getPhoneCell().'<BR>'."\n";
			$email_body .= 'Cell Country: '.$this->getPhoneCellCountry().'<BR>'."\n";
			$email_body .= 'Email: '.$this->getEmail().'<BR><BR>'."\n\n";
			$email_body .= 'Comments: '.$this->getComments().'<BR><BR>'."\n\n";
			$email_body .= $cart_summary; // was 'summary'

		$emailObj = new email();
		$emailObj->setIDreservation($this->id);
		$emailObj->setTo($email_to);
		$emailObj->setFrom($email_from);
		$emailObj->setReplyTo($email_replyto);
		$emailObj->setSubject($email_subject);
		$emailObj->setBodyHTML($email_body);
		$result = $emailObj->send();
		
		$return_array = array();
		if($result) {
			$return_array['success'] = true;
			$return_array['message'] = 'Successfully sent merchant email notification';
			log_audit(array(
				'primary_id' => $this->id,
				'table' => 'reservations',
				'data' => null,
				'comment' => 'Successfully sent merchant email notification.'
				));
		} else {
			$return_array['success'] = false;
			$return_array['message'] = 'Unable to send merchant notification';
			// array_push($errormsg,'Unable to send merchant notification.');
		}
		return $return_array;
	}


	function syncDates() {
		//Make sure both old and new date fields are accurate
		if($this->data['email_conf'] > 0 && $this->data['datetime_conf_sent'] == "") {
			$this->data['datetime_conf_sent'] = mysqlDateTime($this->data['email_conf']);
		}
		if($this->data['email_conf'] <= 0 && $this->data['datetime_conf_sent'] != "") {
			$this->data['email_conf'] = strtotime($this->data['datetime_conf_sent']);
		}
		if($this->data['email_conf'] == "" && $this->data['datetime_conf_sent'] == "") {
			$this->data['email_conf'] = 0;
		}

		if($this->data['card_run'] > 0 && $this->data['datetime_card_run'] == "") {
			$this->data['datetime_card_run'] = mysqlDateTime($this->data['card_run']);
		}
		if($this->data['card_run'] <= 0 && $this->data['datetime_card_run'] != "") {
			$this->data['card_run'] = strtotime($this->data['datetime_card_run']);
		}
		if($this->data['card_run'] == "" && $this->data['datetime_card_run'] == "") {
			$this->data['card_run'] = 0;
		}

		if($this->data['date_booked'] > 0 && $this->data['datetime_booked'] == "") {
			$this->data['datetime_booked'] = mysqlDateTime($this->data['date_booked']);
		}
		if($this->data['date_booked'] <= 0 && $this->data['datetime_booked'] != "") {
			$this->data['date_booked'] = strtotime($this->data['datetime_booked']);
		}
		if($this->data['date_booked'] == "" && $this->data['datetime_booked'] == "") {
			$this->data['date_booked'] = 0;
		}

		if($this->data['canceled'] > 0 && $this->data['datetime_canceled'] == "") {
			$this->data['datetime_canceled'] = mysqlDateTime($this->data['canceled']);
		}
		if($this->data['canceled'] <= 0 && $this->data['datetime_canceled'] != "") {
			$this->data['canceled'] = strtotime($this->data['datetime_canceled']);
		}
		if($this->data['canceled'] == "" && $this->data['datetime_canceled'] == "") {
			$this->data['canceled'] = 0;
		}
	}

	function setupBlocks() {
		if($this->id > 0 && !$this->isCanceled()) {
			$assocIDs = $this->getAssocIDs();
			foreach($assocIDs as $id_assoc) {
				$resAssocObj = new reservation_assoc($id_assoc);
				$resAssocObj->setupBlocks();
				unset($resAssocObj);
			}
		}
	}

	function clearBlocks() {
		if($this->id > 0) {
			$assocIDs = $this->getAssocIDs();
			foreach($assocIDs as $id_assoc) {
				$resAssocObj = new reservation_assoc($id_assoc);
				$resAssocObj->clearBlocks();
				unset($resAssocObj);
			}
		}
	}

	function clearCCnum() {
		$this->data['cc_num_enc'] = null; //Masked number will remain
	}

	function fullCancel() {
		if($this->id == "") {
			return false;
		}

		$assocIDs = $this->getAssocIDs();
		foreach($assocIDs as $id_assoc) {
			$resAssocObj = new reservation_assoc($id_assoc);
			if($resAssocObj->id > 0) {
				$resAssocObj->setDateTimeCanceled("now", true);
				$resAssocObj->save();
			}
			unset($resAssocObj);
		}

		$this->clearCCnum();
		$this->setDateTimeCanceled("now", true);
		$this->save();

		return true;
	}

	function dataChecks() {
		$this->syncDates();
		if($this->data['datetime_created'] == "") {
			$this->data['datetime_created'] = mysqlDateTime();
		}
	}

	function save($params=array()) {
		$this->dataChecks();
		$this->id = object_save($this->tableName, $this->id, $this->data);
		$this->data['id'] = $this->id;

		if(@$params['setup_blocks'] !== false) {
			$this->clearBlocks();
			$this->setupBlocks();
		}
	}

	function delete() {
		$this->clearBlocks();
		object_delete($this->tableName, $this->id);
	}
}

?>