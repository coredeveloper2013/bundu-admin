<?php

@date_default_timezone_set("America/Denver");


//Var Setup / Staging
function isDev() {
    return true;
	if(strpos($_SERVER['SCRIPT_NAME'], '/staging/') !== false) {
		return true;
	}
	return false;
}

$doc_root = $_SERVER['DOCUMENT_ROOT'];
$dev_root = '';
	if(isDev()) {
		$doc_root .= '/bundu-admin';
		$dev_root .= '/bundu-admin';
	}
define('doc_root', $doc_root);
define('dev_root', $dev_root);




//Restrict who can use staging area
/*$stagingIPs = array('98.238.138.115');
if(isDev() && !in_array($_SERVER['REMOTE_ADDR'], $stagingIPs)) {
	echo 'Access denied.';
	exit;
}*/
//unset($stagingIPs);


//Set Error Reporting
error_reporting('E_ALL');
if(isDev()) {
	ini_set('display_errors', '1');
} else {
	ini_set('display_errors', '0');
}


//MySQL
$db_name = 'sessel_bundubashers';
if(isDev()) {
	$db_name = 'bund';
}
define('db_name', $db_name);

$db = mysql_connect('localhost', 'root', '');
mysql_select_db(db_name);
@mysql_query("SET NAMES utf8");
@mysql_query('SET time_zone = "'.date("P").'"');


//Functions
function getCurPath() {
	$out = $_SERVER['SCRIPT_NAME'];
	if($_SERVER['QUERY_STRING'] != "") {
		$out .= '?'.$_SERVER['QUERY_STRING'];
	}
	return $out;
}

function mysqlQuery($query='') {
	if($query == "") {
		return false;
	}
	$result = @mysql_query($query);
	$GLOBALS['mysql_error'] = @mysql_error();
	if($GLOBALS['mysql_error'] != "") {
		log_bad_query($query, $GLOBALS['mysql_error']);
	}
	return $result;
}

function getMysqlValue($query='') {
	$result = mysqlQuery($query);
	$data = @mysql_fetch_array($result);
	if(!$data) {
		return false;
	}
	return @$data[0];
}

function getMysqlValues($query='') {
	$result = mysqlQuery($query);
	$outArry = array();
	while($row = @mysql_fetch_array($result)) {
		$outArry[] = @$row[0];
	}
	return $outArry;
}

function encodeSQL($input='') {
	return mysql_real_escape_string($input);
}

function log_audit($params=array()){
	$query = 'INSERT INTO sessel_logs.log_audit(`path`, `id_user`, `primary_id`, `db`, `table`, `data`, `comment`, `datetime_logged`)
		VALUES("'.encodeSQL($_SERVER['SCRIPT_NAME']).'", "'.encodeSQL(@$_SESSION['valid_user']['id']).'",
			"'.encodeSQL($params['primary_id']).'", "'.encodeSQL(db_name).'", "'.encodeSQL($params['table']).'",
			"'.encodeSQL(serialize($params['data'])).'", "'.encodeSQL($params['comment']).'", NOW())';
	@mysqlQuery($query);
}

function log_bad_query($query='', $error=''){
	$query = 'INSERT INTO sessel_logs.log_bad_queries(path, id_user, db, query, error, datetime_logged)
		VALUES("'.encodeSQL($_SERVER['SCRIPT_NAME']).'", "'.encodeSQL(@$_SESSION['valid_user']['id']).'", "'.encodeSQL(db_name).'", "'.encodeSQL($query).'", "'.encodeSQL($error).'", NOW())';
	@mysql_query($query);
	$GLOBALS['mysql_error'] = $error;
}

function log_fatal_error() {
	$error = error_get_last();

	if(is_null($error)) {
		return false;
	}

	if($error["type"] == 8 && strpos($error["message"], 'Undefined index') === 0) {
		return false;
	}

	$request = serialize($_REQUEST);

	$query = 'INSERT INTO sessel_logs.log_fatal_errors(type, path, file, line, message, id_user, request, datetime_logged)
		VALUES("'.encodeSQL($error["type"]).'", "'.encodeSQL(getCurPath()).'", "'.encodeSQL($error["file"]).'", "'.encodeSQL($error["line"]).'", "'.encodeSQL($error["message"]).'", "'.encodeSQL(@$_SESSION['valid_user']['id']).'", "'.encodeSQL($request).'", NOW())';
	@mysqlQuery($query);
}

function safe_unserialize($input=''){
	$out = unserialize($input);
	if($out === false){
		$out = $input;
		$out = str_replace("\r", "", $out);
		$out = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $out);
		$out = unserialize($out);
	}
	return $out;
}

register_shutdown_function('log_fatal_error');


//!Function files
$function_files = array(
	'/functions/array.inc.php',
	'/functions/datetime.inc.php',
	'/functions/object.inc.php',
	'/functions/tours.inc.php',
	'/functions/strings.inc.php',
	'/functions/ui.inc.php'
	);
foreach($function_files as $file){
	include_once doc_root.$file;
}


//!Classes
function autoloadclass($class_name) {
	$file = doc_root.'/classes/'.strtolower($class_name).'.inc.php';
	if(file_exists($file)) {
		include_once($file);
	}
}

spl_autoload_register('autoloadclass');

?>