<?

//echo '<PRE STYLE="text-align:left;">'; print_r($_REQUEST); echo '</PRE>';

if(isset($_REQUEST['old']) && $_REQUEST['old'] != "" && file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$_REQUEST['old'])){
	$html = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$_REQUEST['old']);
	} else {
	$html = '<DIV STYLE="margin-top:20px; text-align:center; font-family:Helvetica;">The page you were looking for is not available.<BR><BR>However, you can likely find the page you need by using the menu on the top or to the left.</DIV>';
	}

//Find meta title
$pattern = '/<[tT][iI][tT][lL][eE]>(.*)<\/[tT][iI][tT][lL][eE]>/';
$matches = array();
$found = preg_match($pattern,$html,$matches);
if($found){
	$pagetitle = $matches[1];
	$html = str_replace($matches[0],'',$html);
	}
	//echo '<PRE STYLE="text-align:left;">'; echo htmlspecialchars(print_r($matches,true)); echo '</PRE>';

//Find meta description
$pattern = '/<[mM][eE][tT][aA]\s[^<]*[nN][aA][mM][eE]="[dD]escription"\s[^<]*[cC][oO][nN][tT][eE][nN][tT]="([^<"]*)"[^<]*>/';
$matches = array();
$found = preg_match($pattern,$html,$matches);
if(!$found){
	$pattern = '/<[mM][eE][tT][aA]\s[^<]*[cC][oO][nN][tT][eE][nN][tT]="([^<"]*)"\s[^<]*[nN][aA][mM][eE]="[dD]escription"[^<]*>/';
	$found = preg_match($pattern,$html,$matches);
	}
if($found){
	$metadesc = $matches[1];
	}
	//echo '<PRE STYLE="text-align:left;">'; echo htmlspecialchars(print_r($matches,true)); echo '</PRE>';


$allowed_tags = '<p><br><hr><pre><b><i><u><a><strong><em><strike><small><font><ul><ol><sup><sub><center><table><tr><th><td><form><input><select><option><textarea><div><span><img><!--><object><param><embed><ul><ol><li>';
$html = strip_tags($html,$allowed_tags);


include_once('header.php');

echo '<!-- Start page -->'."\n\n";

echo $html;

echo '<!-- End page -->'."\n\n";

include_once('footer.php');

?>