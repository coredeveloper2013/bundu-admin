<?php

function object_save($table='', $id='', $data=array(), $params=array()){
	if($table != "" && is_array($data) && count($data) > 0){
		//Setup
		$writeAudit = false;
		if(@$params['write_audit'] !== false){
			$writeAudit = true;
		}
		$audit_data = array(
			'changed_from' => array(),
			'changed_to' => array()
			);
		$comment = 'object_save';

		//Update
		if(trim($id) != ""){
			$query = 'UPDATE '.mysql_real_escape_string($table).' SET';
				$sep = '';
				foreach($data as $field => $value){
					$query .= $sep.' `'.mysql_real_escape_string($field).'` = ';
					if(is_null($value)){
						$query .= 'NULL';
					} else {
						$value = stripslashes($value);
						$query .= '"'.mysql_real_escape_string($value).'"';
					}
					$sep = ',';
				}
			$query .= ' WHERE id = "'.$id.'" LIMIT 1';
			
			if($writeAudit){
				$audit_query = 'SELECT * FROM '.mysql_real_escape_string($table).' WHERE id = "'.$id.'" LIMIT 1';
				$audit_result = mysqlQuery($audit_query);
				$oldData = @mysql_fetch_assoc($audit_result);
				$comment = 'update - no change';
				foreach($oldData as $field => $oldValue){
					if($data[$field] != $oldValue){
						$audit_data['changed_from'][$field] = $oldValue;
						$audit_data['changed_to'][$field] = $data[$field];
						$comment = 'update';
					}
				}
			}

		//Insert
		} else {
			unset($data['id']);
			$keys = array_keys($data);
			$values = array_values($data);
			$query = 'INSERT INTO '.mysql_real_escape_string($table).'(`'.implode('`,`', $keys).'`) VALUES(';
				//"'.implode('","', $values).'"
				foreach($values as $key => $val){
					//$values[$key] = mysql_real_escape_string($val);
					if(is_null($val)){
						$query .= 'NULL';
					} else {
						$val = stripslashes($val);
						$query .= '"'.mysql_real_escape_string($val).'"';
					}
					if($key < (count($values)-1)){
						$query .= ',';
					}
				}
				$query .= ')';
			unset($keys);
			unset($values);

			if($writeAudit){
				$comment = 'new record';
				$audit_data['changed_to'] = $data;
			}
		}
		$result = mysqlQuery($query);
		if($id == "") {
			$id = mysql_insert_id();
		}
		if($writeAudit && $GLOBALS['mysql_error'] == ""){
			log_audit(array(
				'primary_id' => $id,
				'table' => $table,
				'data' => $audit_data,
				'comment' => $comment
				));
		}
		return $id;
	}
	return false;
}


function object_delete($table='', $id=''){
	if($table != "" && $id > 0){
		$delete = 'DELETE FROM '.mysql_real_escape_string($table).' WHERE id = "'.mysql_real_escape_string($id).'" LIMIT 1';
		$result = mysqlQuery($delete);
		if($GLOBALS['mysql_error'] == ""){
			log_audit(array(
				'primary_id' => $id,
				'table' => $table,
				'comment' => 'deleted'
				));
		}
		return $result;
	}
	return false;
}


?>