<?php

function moneyFormat($amount=0, $commaSepThousands=false) {
	$sepThousands = '';
		if($commaSepThousands) {
			$sepThousands = ',';
		}
	return number_format($amount, 2, '.', $sepThousands);
}

function quick_select($params=array()) {
	$out = '';

	$out .= '<select name="'.@$params['name'].'"';
	if(@$params['id'] != "") {
		$out .= ' id="'.$params['id'].'"';
	}
	if(@$params['class'] != "") {
		$out .= ' class="'.$params['class'].'"';
	}
	if(@$params['style'] != "") {
		$out .= ' style="'.$params['style'].'"';
	}
	if(@$params['onchange'] != "") {
		$out .= ' onchange="'.$params['onchange'].'"';
	}
	$out .= '>';
	if(@$params['prepend_blank']) {
		$out .= '<option>';
		if($params['prepend_blank'] != "") {
			$out .= $params['prepend_blank'];
		}
		$out .= '</option>';
	}
	$options = make_array(@$params['options']);
	if(@$params['value_is_key']) {
		$newOpts = array();
		foreach($options as $value){
			$newOpts[$value] = $value;
		}
		$options = $newOpts;
	}
	foreach($options as $value => $display) {
		$out .= '<option value="'.$value.'"';
		if((string)$params['selected'] == (string)$value) {
			$out .= ' selected';
		}
		$out .= '>'.$display.'</option>';
	}
	$out .= '</select>';

	return $out;
}

function quick_checkbox($params=array()) {
	$options = make_array(@$params['options']);
	if(@$params['value_is_key']) {
		$newOpts = array();
		foreach($options as $value){
			$newOpts[$value] = $value;
		}
		$options = $newOpts;
	}

	$type = 'checkbox';
		if(@$params['type'] != "") {
			$type = $params['type'];
		}

	$items = array();
	foreach($options as $value => $display) {
		$item = '';
		$item .= '<label><input';
			if(@$params['name'] != "") {
				$item .= ' name="'.$params['name'].'"';
			}
			$item .= ' type="'.$type.'" value="'.$value.'"';
			if(@$params['class'] != "") {
				$item .= ' class="'.$params['class'].'"';
			}
			if(@$params['style'] != "") {
				$item .= ' style="'.$params['style'].'"';
			}
			if(@$params['onchange'] != "") {
				$item .= ' onchange="'.$params['onchange'].'"';
			}
		if((string)$params['selected'] == (string)$value) {
			$item .= ' checked';
		}
		$item .= '>'.$display.'</label>';
		$items[] = $item;
	}

	$glue = '<br>';
		if(@$params['glue'] != "") {
			$glue = $params['glue'];
		}

	$out = implode($glue, $items);

	return $out;
}

function quick_radio($params=array()) {
	$params['type'] = 'radio';
	return quick_checkbox($params);
}

?>