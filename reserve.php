<?  // Developed by Dominick Bernal - www.bernalwebservices.com

include_once('header_reserve.php');
require_once('common.inc.php');

//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($_POST,true)).'</PRE>';

if(!isset($_SESSION['cart'])){ $_SESSION['cart'] = array(); }
if(!isset($_SESSION['continue'])){ $_SESSION['continue'] = 'http://www.bundubashers.com'; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_PATH) != parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH)){
	$_SESSION['continue'] = $_SERVER['HTTP_REFERER'];
	}

$fillform = array();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();

function incart($tour){
	global $_SESSION;
	foreach($_SESSION['cart'] as $row){
		if(@$row['type'] == "t" && @$row['typeid'] == $tour){
			return true;
			}
		}
	return false;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($_SESSION['cart']); echo '</PRE>';

function getamount($item){
	global $SESSION;
	$amount = array('base'=>0,'amount'=>0);

	if($item['type'] == "t"){
	$query = 'SELECT * FROM `tours` WHERE `id` = "'.$item['typeid'].'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
		if(isset($num_results) && $num_results == 1){
			$tourinfo = mysql_fetch_assoc($result);

			//Extended pricing is for single day tours only
			$ext = array();
			if($tourinfo['numdays'] == 1 && $tourinfo['ext_pricing'] != ""){
				$tourinfo['ext_pricing'] = explode('|',$tourinfo['ext_pricing']);
				foreach($tourinfo['ext_pricing'] as $val){
					$val = explode(':',$val);
					$ext[$val[0]] = $val[1];
				}
			}
			if(isset($ext[$item['numguests']]) && $ext[$item['numguests']] > 0){
				$amount['amount'] = $ext[$item['numguests']];
				//} elseif($tourinfo['volstart'] > 0 && $tourinfo['volrate'] > 0 && $item['numguests'] >= $tourinfo['volstart']){
				//$amount['amount'] = ($item['numguests']*$tourinfo['volrate']);
				} else {
				$amount['amount'] = ($item['numguests']*$tourinfo['perguest']);
				}
			//if($tourinfo['mincharge'] > 0 && $amount['amount'] < $tourinfo['mincharge']): $amount['amount'] = $tourinfo['mincharge']; endif;
			//if($tourinfo['maxcharge'] > 0 && $amount['amount'] > $tourinfo['maxcharge']): $amount['amount'] = $tourinfo['maxcharge']; endif;
			//$amount['amount'] = ($item['numguests']*$tourinfo['perguest']);

			$per = 1;
			if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
				//if(isset($tourinfo['agent_perguest']) && $tourinfo['agent_perguest'] > 0){
				//	$amount['amount'] = ($item['numguests']*$tourinfo['agent_perguest']);
				$per = ($_SESSION['agent']['discount'] / 100);
				if(isset($tourinfo['agent'.$_SESSION['agent']['tier'].'_per']) && $tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] > 0){
					$per = ($tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] / 100);
					}
				//$amount['amount'] = ($amount['amount']*$per);
			} elseif(@$item['updc'] != "" && $item['updc'] >= 0 && $item['updc'] <= 100){
				$per = ($item['updc'] / 100);
				if($item['updc'] == 0) {
					$tourinfo['fuelsurcharge'] = 0;
				}
			}

			$amount['base'] = ($amount['amount']*$per);

			//Lodging
			if(isset($item['numrooms'])){
				$rm = 1;
				while($rm <= $item['numrooms'] || @$item['room'.$rm] != ""){
					if($item['room'.$rm] == 1 && $item['room'.$rm]){
						$amount['amount'] = ($amount['amount'] + $tourinfo['single']);
						$amount['room'.$rm] = $tourinfo['single']*$per;
						} elseif($item['room'.$rm] == 3 && $item['room'.$rm]){
						$amount['amount'] = ($amount['amount'] - $tourinfo['triple']);
						$amount['room'.$rm] = $tourinfo['triple']*$per;
						} elseif($item['room'.$rm] == 4 && $item['room'.$rm]){
						$amount['amount'] = ($amount['amount'] - $tourinfo['quad']);
						$amount['room'.$rm] = $tourinfo['quad']*$per;
						}
					$rm++;
					}
				} elseif($item['numguests'] == 1){
				$amount['base'] = ($amount['base'] + $tourinfo['single']);
				$amount['amount'] = ($amount['amount'] + $tourinfo['single']);
				}
			$amount['amount'] = ($amount['amount']*$per);

			//Lodging extensions
			if(isset($item['extlodgeid']) && is_array($item['extlodgeid'])){
			foreach($item['extlodgeid'] as $key => $val){ if(isset($item['addextl'.$val]) && $item['addextl'.$val] == "y" && $item['extlodgenights'][$key] > 0){
				$elt = ($item['extlodgeperguest'][$key]*$item['numguests']);
				if($item['numguests'] == 1){
					$elt = ($elt + $item['extlodgesingle'][$key]);
					} elseif($item['numrooms'] == 1 && $item['numguests'] == 3){
					$elt = ($elt - $item['extlodgetriple'][$key]);
					} elseif($item['numrooms'] == 1 && $item['numguests'] == 4){
					$elt = ($elt - $item['extlodgequad'][$key]);
					} elseif($item['numrooms'] > 1){
						for($rm=1; $rm<=$item['numrooms']; $rm++){
							if(@$item['room'.$rm] == 1){
								$elt = ($elt + $item['extlodgesingle'][$key]);
								} elseif(@$item['room'.$rm] == 3){
								$elt = ($elt - $item['extlodgetriple'][$key]);
								} elseif(@$item['room'.$rm] == 4){
								$elt = ($elt - $item['extlodgequad'][$key]);
								}
							}
					}
				$amount['amount'] = ($amount['amount'] + ($elt*$item['extlodgenights'][$key]));
				}}
				}

			$amount['amount'] += ($item['numguests']*$tourinfo['fuelsurcharge']);

			//Options
			if(isset($item['optionid']) && is_array($item['optionid'])){
			foreach($item['optionid'] as $key => $val){ if(isset($item['addopt'.$val]) && $item['addopt'.$val] == "y"){
				$amount['amount'] = ($amount['amount'] + ($item['optionprice'][$key]*$item['optionpax'][$key]));
				}}
				}

			} //End Tour Num Results if statement
		} elseif($item['type'] == "r"){
		$cpm = 0.40;
		$query = 'SELECT `setting`,`var` FROM `bundubus_settings` WHERE `setting` = "cpm" LIMIT 1';
			$result = @mysqlQuery($query);
			$num_results = @mysql_num_rows($result);
			if(isset($num_results) && $num_results == 1){
				$default = mysql_fetch_assoc($result);
				$cpm = $default['var'];
				}

		$query = 'SELECT * FROM `routes` WHERE `id` = "'.$item['routeid'].'" AND `bbincl` != "n" AND `hidden` != "1" LIMIT 1';
			$result = @mysqlQuery($query);
			$num_results = @mysql_num_rows($result);
			if(isset($num_results) && $num_results == 1){
			$routeinfo = mysql_fetch_assoc($result);

			if(isset($item['bbticket']) && $item['bbticket'] != ""){
				$amount['amount'] = 0;
				} elseif($routeinfo['bbprice'] > 0){
				$amount['amount'] = ($routeinfo['bbprice']*$item['numguests']);
				} else {
				$amount['amount'] = (($cpm*$routeinfo['miles'])*$item['numguests']);
				}

			if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
				$per = ($_SESSION['agent']['discount'] / 100);
				$amount['amount'] = ($amount['amount']*$per);
				}

			} //End Route Num Results if statement
		} //End Type If Statement

	return $amount;
	}

echo '<CENTER>';

if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
	echo '<FONT FACE="Arial" SIZE="2" COLOR="#FF6600"><B>You are logged in as agent: '.$_SESSION['agent']['name'].'</B></FONT><BR><BR>';
	}

//CHECK TOUR DATE
//echo '<PRE>'; print_r($_REQUEST); echo '</PRE>';
if(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && $_POST['type'] == "t"){
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$_POST['tourid'].'" AND `date` > "'.$time.'" AND `date` = "'.mktime(0,0,0,$_POST['date_m'],$_POST['date_d'],$_POST['date_y']).'" LIMIT 1';
	$result = mysqlQuery($query);
	$num_results = @mysql_num_rows($result);
	if($num_results != 1){
		$_POST['utaction'] = '';
		$_REQUEST['t'] = $_POST['tourid'];
		if(isset($_POST['cartid']) && $_POST['cartid'] != '*new*'){ $_REQUEST['edit'] = $_POST['cartid']; }
		array_push($puberrormsg,'This tour is not running on the date you chose.  <A HREF="http://www.bundubashers.com/tour.php?id='.$_POST['tourid'].'">Please click here to check the tour calendar.</A><BR>Click on the date you want to order.  Thank you.');
		printmsgs($pubsuccessmsg,$puberrormsg);
		}
	}
//CHECK FOR PAYMENT DETAILS BEFORE PROCESSING
$isAgent = false;
	if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
		$isAgent = true;
	}
if(@$_REQUEST['form'] == "process" && count($_SESSION['cart']) > 0 && !$isAgent){
	if(trim(@$_POST['cc_num']) == ""){
		array_push($puberrormsg,'Please enter your credit card number.');
		$_REQUEST['form'] = '';
	}
	if(trim(@$_POST['cc_expdate_month']) == "" || trim(@$_POST['cc_expdate_year']) == ""){
		array_push($puberrormsg,'Please choose an expiration date for your credit card.');
		$_REQUEST['form'] = '';
	} elseif(trim(@$_POST['cc_expdate_month']) != "" && trim(@$_POST['cc_expdate_year']) != ""){
		if($_POST['cc_expdate_year'] < 96 && $_POST['cc_expdate_year'] > 90){
			array_push($puberrormsg,'The card expiration year is not valid.');
			$_REQUEST['form'] = '';
		}	
	}
	if(trim(@$_POST['cc_zip']) == ""){
		array_push($puberrormsg,'Please enter the zip or postal code associated with your credit card.');
		$_REQUEST['form'] = '';
	}
	if(trim(@$_POST['email']) != trim(@$_POST['email2'])){
		array_push($puberrormsg,'Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.');
		$_REQUEST['form'] = '';
	}
	printmsgs($pubsuccessmsg,$puberrormsg);
}


if(isset($_REQUEST['form']) && $_REQUEST['form'] == "process" && count($_SESSION['cart']) > 0){  // !PROCESS RESERVATION **********************************************************

//echo '</CENTER>';
//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['cart']); echo '</PRE>';

	foreach($_POST as $key => $val){
		if(!is_array($val)){
			$_POST[$key] = strip_tags($_POST[$key]);
			$_POST[$key] = trim($_POST[$key]);
			//$_POST[$key] = str_replace('"','\"',$_POST[$key]);
			}
		}

	//RECORD RESERVATION
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);

	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];

	//Agent fields
	if(isset($_SESSION['agent']['id']) && $_SESSION['agent']['id'] != ""): $agent = $_SESSION['agent']['id']; else: $agent = ''; endif;
	if(isset($_SESSION['agent']['name']) && $_POST['name'] == ""){ $_POST['name'] = $_SESSION['agent']['name']; }
	if(isset($_SESSION['agent']['phone_homebus']) && $_POST['phone_homebus'] == ""){ $_POST['phone_homebus'] = $_SESSION['agent']['phone_homebus']; }
	if(isset($_SESSION['agent']['email']) && $_POST['email'] == ""){ $_POST['email'] = $_SESSION['agent']['email']; }
	$id_subagent = 'NULL';
	if($_SESSION['agent']['id_subagent'] > 0) {
		$id_subagent = '"'.encodeSQL($_SESSION['agent']['id_subagent']).'"';
	}

	if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $_POST['pay_method'] = 'Alternate'; else: $_POST['pay_method'] = 'C/C'; endif;
	$_POST['amount'] = 0;
		foreach($_SESSION['cart'] as $row){
			$_POST['amount'] = ($_POST['amount']+$row['amount']);
			}

	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_zip`,`pay_method`,`alt_name`,`comments`,`agent`,`id_subagent`,`http_referer`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.encodeSQL($_POST['name']).'","'.encodeSQL($_POST['phone_homebus']).'","'.encodeSQL($_POST['phone_cell']).'","'.encodeSQL($_POST['cell_country']).'","'.encodeSQL($_POST['email']).'","'.encodeSQL($_POST['amount']).'","'.encodeSQL($_POST['name']).'","'.encodeSQL($_POST['cc_num']).'","'.encodeSQL($_POST['cc_expdate']).'","'.encodeSQL($_POST['cc_scode']).'","'.encodeSQL($_POST['cc_zip']).'","'.encodeSQL($_POST['pay_method']).'","'.encodeSQL($_POST['alt_name']).'","'.encodeSQL($_POST['comments']).'","'.encodeSQL($agent).'",'.$id_subagent.',"'.encodeSQL($_SESSION['http_referer']).'","Website","'.$time.'")';
		@mysqlQuery($query);
	$thiserror = $GLOBALS['mysql_error'];
	if($thiserror != ""){
		array_push($errormsg, $thiserror);

	} else {

	//START ON SUBRESERVATIONS
		$confnum = mysql_insert_id();
		$email_restypes = array();
		array_push($successmsg,'Saved new reservation '.$confnum.' for '.$_POST['name'].'.');

		foreach($_SESSION['cart'] as $row){
			foreach($row as $key => $val){
				if(!is_array($val)){
					$row[$key] = str_replace('"','\"',$row[$key]);
					}
				}
			$fillform = $row;

			if(getval('type') == "t"){
				array_push($email_restypes,'Tour');
				$fillform['date'] = mktime(0,0,0,getval('date_m'),getval('date_d'),getval('date_y'));
				//$fillform['pretitle'] = '[Bundu Tour '.getval('tourid').'] ';
				$fillform['title'] = 'CONCAT("[Bundu Tour '.getval('tourid').'] ",IFNULL((SELECT tours.`title` FROM `tours` WHERE tours.`id` = "'.getval('tourid').'" LIMIT 1),""))';
				$fillform['dep_loc'] = '"'.getval('dep_loc').'"';
				$fillform['dep_time'] = '('.getval('date').' + IFNULL((SELECT IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) FROM `routes`,`routes_dates`,`tours_assoc` WHERE tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`id` = routes_dates.`routeid` AND routes_dates.`date` = "'.getval('date').'" AND tours_assoc.`tourid` = "'.getval('tourid').'" ORDER BY `day` ASC, `order` ASC LIMIT 1),0))';
				$fillform['arr_loc'] = '"'.getval('arr_loc').'"';
				$fillform['arr_time'] = '"'.getval('arr_time').'"';
				$fillform['vendor'] = 'IFNULL((SELECT tours.`vendor` FROM `tours` WHERE tours.`id` = "'.getval('tourid').'" LIMIT 1),0)';
				$rm = 1;
				while($rm <= $row['numrooms'] || @$row['rmamt'.$rm] != ""){
					if($row['room'.$rm] > 2){ $fillform['base'] = ($fillform['base']-$row['rmamt'.$rm]); } else { $fillform['base'] = ($fillform['base']+$row['rmamt'.$rm]); }
					$rm++;
					}
				}
			elseif(getval('type') == "r") {
				array_push($email_restypes,'Bundu Bus');
				$fillform['base'] = getval('amount');
				$fillform['title'] = '"'.getval('title').'"';
				$fillform['dep_loc'] = 'IFNULL((SELECT `dep_loc` FROM `routes` WHERE `id` = "'.getval('routeid').'" LIMIT 1),0)';
				$fillform['dep_time'] = '('.getval('date').' + IFNULL((SELECT IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) FROM `routes`,`routes_dates` WHERE routes.`id` = "'.getval('routeid').'" AND routes.`id` = routes_dates.`routeid` AND routes_dates.`date` = "'.getval('date').'" LIMIT 1),0))';
				$fillform['arr_loc'] = 'IFNULL((SELECT `arr_loc` FROM `routes` WHERE `id` = "'.getval('routeid').'" LIMIT 1),0)';
				$fillform['arr_time'] = '('.getval('date').' + IFNULL((SELECT IF(routes_dates.`arr_time`=-1,routes.`arr_time`,routes_dates.`arr_time`) FROM `routes`,`routes_dates` WHERE routes.`id` = "'.getval('routeid').'" AND routes.`id` = routes_dates.`routeid` AND routes_dates.`date` = "'.getval('date').'" LIMIT 1),0))';
				$fillform['vendor'] = 'IFNULL((SELECT `vendor` FROM `routes` WHERE `id` = "'.getval('routeid').'" LIMIT 1),0)';
				}
			//if(getval('pretitle') != ""){ $fillform['title'] = getval('pretitle').' '.getval('title'); }

			//Insert into assoc table
			$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`preftime`,`activityid`,`market`,`trans_type`,`onlydateavl`,`altdates`,`lodgeid`,`nights`,`amount`,`tourid`,`routeid`,`bbticket`,`vendor`)';
				$query .= ' VALUES("'.$confnum.'",'.getval('title').',"'.getval('type').'","'.$_POST['name'].'","'.getval('numguests').'",0,0,'.getval('date').','.getval('dep_loc').','.getval('dep_time').','.getval('arr_loc').','.getval('arr_time').',"'.getval('preftime').'","'.getval('activityid').'","'.getval('market').'","'.getval('trans_type').'","'.getval('onlydateavl').'","'.getval('altdates').'","'.getval('lodgeid').'","'.getval('nights').'"';
				$query .= ',"'.getval('base').'","'.getval('tourid').'","'.getval('routeid').'","'.getval('bbticket').'",'.getval('vendor').')';
				//echo $query.'<BR>';
				@mysqlQuery($query);
			$thiserror = $GLOBALS['mysql_error'];
			if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

			//Add guests
			if(isset($row['guests_name']) && count($row['guests_name']) > 0){
			foreach($row['guests_name'] as $guestid => $guest){ if($guest != ""){
				if(!isset($row['guests_weight'][$guestid])){ $row['guests_weight'][$guestid] = ''; }
				if(!isset($row['guests_lunch'][$guestid])){ $row['guests_lunch'][$guestid] = ''; }
				$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$associd.'","'.encodeSQL($row['guests_name'][$guestid]).'","'.encodeSQL($row['guests_weight'][$guestid]).'","'.encodeSQL($row['guests_lunch'][$guestid]).'")';
				@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror != ""){ array_push($errormsg,$thiserror); }
				}} //End foreach
				} //End Guests if

			//Add lodging preferences
			$rm = 1;
				while($rm <= $row['numrooms'] || @$row['rmamt'.$rm] != ""){
					if($row['room'.$rm] > 2){ $row['rmamt'.$rm] = '-'.$row['rmamt'.$rm]; }
					$query = 'INSERT INTO `reservations_lodging`(`associd`,`room`,`numguests`,`adjust`) VALUES("'.$associd.'","'.$rm.'","'.$row['room'.$rm].'","'.$row['rmamt'.$rm].'")';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if($thiserror != ""){ array_push($errormsg,$thiserror); }
					$rm++;
					}

			//Add lodging and shuttle for tours
			$ext_days = array();
			//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
			if(getval('type') == "t"){
				//Lodging extensions
				$ext_info = array();
				if(isset($row['extlodgeid']) && is_array($row['extlodgeid'])){
				//$query = 'SELECT * FROM `tours_assoc` WHERE `tourid` = "'.getval('tourid').'" AND `type` = "e" AND ';
				$query = 'SELECT tours_assoc.`day`, tours_assoc.`typeid`, tours_extlodging.`lodgeid`, tours_assoc.`order` FROM `tours_assoc`,`tours_dates`,`tours_extlodging` WHERE tours_assoc.`type` = "e" AND tours_assoc.`tourid` = "'.getval('tourid').'" AND tours_assoc.`tourid` = tours_dates.`tourid` AND tours_assoc.`dir` = tours_dates.`dir` AND tours_dates.`date` = "'.getval('date').'" AND (tours_assoc.`typeid` = "'.implode('" OR tours_assoc.`typeid` = "',$row['extlodgeid']).'") AND tours_extlodging.`id` = tours_assoc.`typeid`';
					$result = @mysqlQuery($query);
					$num_results = @mysql_num_rows($result);
						for($i=0; $i<$num_results; $i++){
						$r = mysql_fetch_assoc($result);
						$ext_info['e'.$r['typeid']] = $r;
						}
					}
				//echo '<PRE STYLE="text-align:left;">'; print_r($ext_info); echo '</PRE>';
				if(isset($row['extlodgeid']) && is_array($row['extlodgeid'])){
				foreach($row['extlodgeid'] as $key => $val){ if(isset($row['addextl'.$val]) && $row['addextl'.$val] == "y" && $row['extlodgenights'][$key] > 0){
					$elt = ($row['extlodgeperguest'][$key]*$row['numguests']);
					if($row['numguests'] == 1){
						$elt = ($elt + $row['extlodgesingle'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 3){
						$elt = ($elt - $row['extlodgetriple'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 4){
						$elt = ($elt - $row['extlodgequad'][$key]);
						} elseif($row['numrooms'] > 1){
							for($rm=1; $rm<=$row['numrooms']; $rm++){
								if(@$row['room'.$rm] == 1){
									$elt = ($elt + $row['extlodgesingle'][$key]);
									} elseif(@$row['room'.$rm] == 3){
									$elt = ($elt - $row['extlodgetriple'][$key]);
									} elseif(@$row['room'.$rm] == 4){
									$elt = ($elt - $row['extlodgequad'][$key]);
									}
								}
						}
					$r = array( 'assoc'=>false , 'day'=>$ext_info['e'.$val]['day'] , 'order'=>$ext_info['e'.$val]['order'] , 'lodgeid'=>$ext_info['e'.$val]['lodgeid'] , 'add'=>$row['extlodgenights'][$key] , 'amount' => number_format(($elt*$row['extlodgenights'][$key]),2,'.',''));
					array_push($ext_days,$r);
					}}
					}
					//echo '<PRE STYLE="text-align:left;">'; print_r($ext_days); echo '</PRE>';
				//Tour lodging
				$addassoc = array();
				$query = 'SELECT tours_assoc.`order`, tours_assoc.`day`, lodging.`id` AS `lodgeid`, lodging.`name` AS `reference`, lodging.`vendor` FROM `tours_assoc`,`lodging`,`tours_dates` WHERE tours_assoc.`type` = "l" AND tours_assoc.`tourid` = "'.getval('tourid').'" AND tours_assoc.`tourid` = tours_dates.`tourid` AND tours_assoc.`dir` = tours_dates.`dir` AND tours_dates.`date` = "'.getval('date').'" AND tours_assoc.`typeid` = lodging.`id` AND lodging.`type` = "t" ORDER BY tours_assoc.`order` ASC';
					$result = @mysqlQuery($query);
					$num_results = @mysql_num_rows($result);
						for($i=0; $i<$num_results; $i++){
						$r = mysql_fetch_assoc($result);
						array_push($addassoc,$r);
						}
				foreach($addassoc as $r){
					$r['nights'] = 1;
					$r['ext'] = 0;
					$r['assoc'] = 0;
					$r['ext_skip'] = -1;
					foreach($ext_days as $i => $ext_this){
						if($r['day'] == $ext_this['day'] && $r['lodgeid'] == $ext_this['lodgeid']){
							$ext_days[$i]['assoc'] = true;
							$r['nights'] = ($r['nights'] + $ext_this['add']);
							$r['ext'] = 1;
							$r['assoc'] = $associd;
							$r['ext_skip'] = $i;
							break;
							}
						}
					$r['ext_offset'] = 0; foreach($ext_days as $i => $ext_this){ if($i != $r['ext_skip'] && $r['order'] > $ext_this['order']){ $r['ext_offset']=($r['ext_offset']+$ext_this['add']); } } $r['day'] = ($r['day']+$r['ext_offset']);
					$r['date'] = mktime(0,0,0,date("n",getval('date')),(date("j",getval('date'))+($r['day']-1)),date("Y",getval('date')));
					$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`lodgeid`,`nights`,`extlodging`,`assoc`,`vendor`)';
						$query .= ' VALUES("'.$confnum.'","[Bundu Tour '.getval('tourid').'] '.$r['reference'].'","l","'.encodeSQL($_POST['name']).'","'.getval('numguests').'",0,0,"'.$r['date'].'","'.$r['lodgeid'].'","'.$r['nights'].'","'.$r['ext'].'","'.$r['assoc'].'","'.$r['vendor'].'")';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if($thiserror != ""){ array_push($errormsg,$thiserror); }
					}
				//Extended lodging - Add unassociated extensions
				//echo '<PRE STYLE="text-align:left;">'; print_r($ext_days); echo '</PRE>';
				foreach($ext_days as $i => $r){ if(!$r['assoc']){
					$r['ext_offset'] = 0; foreach($ext_days as $ii => $ext_this){ if($i != $ii && $r['order'] > $ext_this['order']){ $r['ext_offset']=($r['ext_offset']+$ext_this['add']); } } $r['day'] = ($r['day']+$r['ext_offset']);
					$r['date'] = mktime(0,0,0,date("n",getval('date')),(date("j",getval('date'))+($r['day']-1)),date("Y",getval('date')));
					$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`lodgeid`,`nights`,`extlodging`,`amount`,`assoc`,`vendor`)';
						$query .= ' SELECT "'.$confnum.'",CONCAT("[Bundu Tour '.getval('tourid').'] ",lodging.`name`),"l","'.encodeSQL($_POST['name']).'","'.getval('numguests').'",0,0,"'.$r['date'].'","'.$r['lodgeid'].'","'.$r['add'].'","1","'.$r['amount'].'","0",lodging.`vendor` FROM `lodging` WHERE lodging.`id` = "'.$r['lodgeid'].'"';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if($thiserror != ""){ array_push($errormsg,$thiserror); }
					}}
				//Tour transport
				$addassoc = array();
				$query = 'SELECT tours_assoc.`order`, tours_assoc.`day`, tours_transport.`reference`, tours_transport.`vendor` FROM `tours_assoc`,`tours_transport`,`tours_dates` WHERE tours_assoc.`type` = "p" AND tours_assoc.`tourid` = "'.getval('tourid').'" AND tours_assoc.`tourid` = tours_dates.`tourid` AND tours_assoc.`dir` = tours_dates.`dir` AND tours_transport.`id` = tours_assoc.`typeid` AND tours_dates.`date` = "'.getval('date').'"';
					$result = @mysqlQuery($query);
					$num_results = @mysql_num_rows($result);
						for($i=0; $i<$num_results; $i++){
						$r = mysql_fetch_assoc($result);
						array_push($addassoc,$r);
						}
				foreach($addassoc as $r){
					$r['ext_offset'] = 0; foreach($ext_days as $ext_this){ if($r['order'] > $ext_this['order']){ $r['ext_offset']=($r['ext_offset']+$ext_this['add']); } } $r['day'] = ($r['day']+$r['ext_offset']);
						//if($r['day'] > $ext_start){ $r['day'] = ($r['day']+$ext_num); }
					$r['date'] = mktime(0,0,0,date("n",getval('date')),(date("j",getval('date'))+($r['day']-1)),date("Y",getval('date')));
					$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_time`,`vendor`)';
						$query .= ' VALUES("'.$confnum.'","[Bundu Tour '.getval('tourid').'] '.$r['reference'].'","p","'.encodeSQL($_POST['name']).'","'.getval('numguests').'",0,0,"'.$r['date'].'","'.$r['date'].'","'.$r['vendor'].'")';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if($thiserror != ""){ array_push($errormsg,$thiserror); }
					}
				}

			//Add activities
			if(!isset($row['optionid'])){ $row['optionid'] = array(); }
			foreach($row['optionid'] as $optionid => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y" && $row['optionpax'][$optionid] > 0){
				$query = 'SELECT tours_assoc.`order`, tours_assoc.`day` FROM `tours_assoc`,`tours_dates` WHERE tours_assoc.`tourid` = "'.getval('tourid').'" AND tours_assoc.`tourid` = tours_dates.`tourid` AND tours_assoc.`type` = "a" AND tours_assoc.`dir` = tours_dates.`dir` AND tours_dates.`date` = "'.getval('date').'" AND tours_assoc.`typeid` = "'.$row['optionid'][$optionid].'" LIMIT 1';
				$result = mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror != ""): array_push($errormsg,$thiserror); endif;
				$num_results = @mysql_num_rows($result);
				if($num_results == 1){
					$datefound = mysql_fetch_assoc($result);
					$row['optionorder'][$optionid] = $datefound['order'];
					$row['optionday'][$optionid] = $datefound['day'];
					}
				if(!isset($row['optionday'][$optionid]) || $row['optionday'][$optionid] == ""){ $row['optionday'][$optionid] = 1; }
				$ext_offset = 0; foreach($ext_days as $ext_this){ if($row['optionorder'][$optionid] > $ext_this['order']){ $ext_offset=($ext_offset+$ext_this['add']); } } $row['optionday'][$optionid] = ($row['optionday'][$optionid]+$ext_offset);
					//if($row['optionday'][$optionid] > $ext_start){ $row['optionday'][$optionid] = ($row['optionday'][$optionid]+$ext_num); }
				$optiondate = mktime(0,0,0,$row['date_m'],($row['date_d']+($row['optionday'][$optionid]-1)),$row['date_y']);
				if(getval('pretitle') != ""){ $row['optionname'][$optionid] = getval('pretitle').' '.$row['optionname'][$optionid]; }

				//Insert into assoc table
				$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`date`,`dep_time`,`preftime`,`activityid`,`amount`,`vendor`)';
					$query .= ' VALUES("'.$confnum.'","'.$row['optionname'][$optionid].'","a","'.encodeSQL($_POST['name']).'","'.$row['optionpax'][$optionid].'","'.$optiondate.'","'.$optiondate.'","'.encodeSQL(getval('preftime')).'","'.$row['optionid'][$optionid].'","'.($row['optionprice'][$optionid]*$row['optionpax'][$optionid]).'",(SELECT activities.`vendor` FROM `activities` WHERE activities.`id` = "'.$row['optionid'][$optionid].'" LIMIT 1))';
					@mysqlQuery($query);
				$thiserror = $GLOBALS['mysql_error'];
				if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

				//Add guests
				if(isset($row['guests_name']) && count($row['guests_name']) > 0){
				foreach($row['guests_name'] as $guestid => $guest){ if($guest != ""){
					if(!isset($row['guests_weight'][$guestid])){ $row['guests_weight'][$guestid] = ''; }
					if(!isset($row['guests_lunch'][$guestid])){ $row['guests_lunch'][$guestid] = ''; }
					$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$associd.'","'.encodeSQL($row['guests_name'][$guestid]).'","'.encodeSQL($row['guests_weight'][$guestid]).'","'.encodeSQL($row['guests_lunch'][$guestid]).'")';
					@mysqlQuery($query);
					$thiserror = $GLOBALS['mysql_error'];
					if($thiserror != ""): array_push($errormsg,$thiserror); endif;
					}} //End foreach
					} //End Guests if

				}} //End Activities foreach

			} //End Foreach

	} //End Confnum/Error If Statement


//printmsgs($successmsg,$errormsg);

	$email_restypes = array_unique($email_restypes);

	//BUILD MERCHANT NOTIFICATION
	$summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";
		bgcolor('reset');
		$summary .= "\t".'<TR>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Details</TD>';
			$summary .= '<TD STYLE="padding:4px; padding-left:0px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Options/Add Ons</TD>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Totals</TD>';
			$summary .= '</TR>'."\n";
		$TOTALCOST = 0;
		foreach($_SESSION['cart'] as $id => $row){
			if($row['type'] == "r"){
			if(!isset($row['amount'])){
				$getamount = getamount($row); $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
				$row['amount'] = $getamount['amount'];
				}
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" COLSPAN="2" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$row['date']).'<BR>';
					$summary .= 'Guests: '.$row['numguests'].'<BR>';
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
				$summary .= '</TR>'."\n";
			} else {
			$date = mktime(0,0,0,$row['date_m'],$row['date_d'],$row['date_y']);
			if($row['onlydateavl'] > 0){ $onyldateavl = 'Yes'; } else { $onyldateavl = 'No'; }
			if(!isset($row['amount'])){
				$getamount = getamount($row); $_SESSION['cart'][$id]['base'] = $getamount['base']; $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
				$row['amount'] = $getamount['amount'];
				}
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$date).'<BR>';
					if(isset($row['altdates']) && trim($row['altdates']) != ""): $summary .= 'Alternate dates: '.trim($row['altdates']).'<BR>'; endif;
					$summary .= 'Guests: '.$row['numguests'].'<BR>';
					$summary .= 'Base price: $'.number_format($row['base'],2,'.','').'<BR>';
					$summary .= 'Pick Up: '.$row['dep_loc'].'<BR>Drop Off: '.$row['arr_loc'].'<BR>';
					if(isset($row['preftime'])): $summary .= 'Preferred time of day: '.$row['preftime'].'<BR>'; endif;
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
				//Lodging adjustments
				$rm = 1;
				while($rm <= $row['numrooms'] || @$row['rmamt'.$rm] != ""){
					if($row['room'.$rm] == 1 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' single occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						} elseif($row['room'.$rm] == 3 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' triple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						} elseif($row['room'.$rm] == 4 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' quadruple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						}
					$rm++;
					}
				//Lodging extensions
				if(isset($row['extlodgeid']) && is_array($row['extlodgeid'])){
				foreach($row['extlodgeid'] as $key => $val){ if(isset($row['addextl'.$val]) && $row['addextl'.$val] == "y" && $row['extlodgenights'][$key] > 0){
					$elt = ($row['extlodgeperguest'][$key]*$row['numguests']);
					if($row['numguests'] == 1){
						$elt = ($elt + $row['extlodgesingle'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 3){
						$elt = ($elt - $row['extlodgetriple'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 4){
						$elt = ($elt - $row['extlodgequad'][$key]);
						} elseif($row['numrooms'] > 1){
							for($rm=1; $rm<=$row['numrooms']; $rm++){
								if(@$row['room'.$rm] == 1){
									$elt = ($elt + $row['extlodgesingle'][$key]);
									} elseif(@$row['room'.$rm] == 3){
									$elt = ($elt - $row['extlodgetriple'][$key]);
									} elseif(@$row['room'.$rm] == 4){
									$elt = ($elt - $row['extlodgequad'][$key]);
									}
								}
						}
					$summary .= '<DIV>Extended stay at '.$row['extlodgename'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.number_format($elt,2,'.','').' x '.$row['extlodgenights'][$key].' '.gettrans('night(s)').' = <B>$'.number_format( ($elt*$row['extlodgenights'][$key]),2,'.','').'</B></DIV>';
					}}
					}
				//Activities
				if(!isset($row['optionid'])){ $row['optionid'] = array(); }
				foreach($row['optionid'] as $key => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y" && $row['optionpax'][$key] > 0){
					$summary .= '<DIV>'.$row['optionname'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.$row['optionprice'][$key].' x '.$row['optionpax'][$key].' guest(s) = <B>$'.number_format( ($row['optionprice'][$key]*$row['optionpax'][$key]),2,'.','').'</B></DIV>';
					}}
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
				$summary .= '</TR>'."\n";
			} //End Type If Statement
			$TOTALCOST = ($TOTALCOST+$row['amount']);
			} //End ForEach Loop
			$summary .= "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="3" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">Total:&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD></TR>'."\n";
			$summary .= '</TABLE><BR>'."\n\n";

	//SEND MERCHANT NOTIFICATION
		$resObj = new reservation($confnum);
		$mail_result = $resObj->sendMerchantNotification(array('email_restypes'=>$email_restypes,
																'cart_summary'=>$summary,
																));
		if($mail_result['success']===false) {
			array_push($errormsg,$mail_result['message']);
		}
		/*
		$email_subject = 'New '.implode('/',$email_restypes).' Reservation #'.$confnum;
		$email_to = 'Bundu Bashers Tours <bundubashers@gmail.com>';
		if(isDev()) {
			$email_to = 'BWS Testing <dominick@bernalwebservices.com>';
		}
		$email_from = 'Bundu Admin <admin@bundubashers.com>';
		$email_replyto = $_POST['name']." <".$_POST['email'].">";
		$email_body = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$email_body .= 'Reservation number: <B>'.$confnum.'</B><BR>'."\n";
			$email_body .= 'Name: '.$_POST['name'].'<BR>'."\n";
			$email_body .= 'Home/Bus. Phone: '.$_POST['phone_homebus'].'<BR>'."\n";
			$email_body .= 'Cell Phone: '.$_POST['phone_cell'].'<BR>'."\n";
			$email_body .= 'Cell Country: '.$_POST['cell_country'].'<BR>'."\n";
			$email_body .= 'Email: '.$_POST['email'].'<BR><BR>'."\n\n";
			$email_body .= 'Comments: '.$_POST['comments'].'<BR><BR>'."\n\n";
			$email_body .= $summary;

		$emailObj = new email();
		$emailObj->setIDreservation($confnum);
		$emailObj->setTo($email_to);
		$emailObj->setFrom($email_from);
		$emailObj->setReplyTo($email_replyto);
		$emailObj->setSubject($email_subject);
		$emailObj->setBodyHTML($email_body);
		$result = $emailObj->send();
		if($result) {
			log_audit(array(
				'primary_id' => $confnum,
				'table' => 'reservations',
				'data' => null,
				'comment' => 'Successfully sent merchant email notification.'
				));
		} else {
			array_push($errormsg,'Unable to send merchant notification.');
		}
		*/

	//BUILD CUSTOMER NOTIFICATION
	$summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";
		bgcolor('reset');
		$summary .= "\t".'<TR>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">'.gettrans('Details').'</TD>';
			$summary .= '<TD STYLE="padding:4px; padding-left:0px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">'.gettrans('Options').'/'.gettrans('Add Ons').'</TD>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">'.gettrans('Totals').'</TD>';
			$summary .= '</TR>'."\n";
		$TOTALCOST = 0;
		foreach($_SESSION['cart'] as $id => $row){
			if($row['type'] == "r"){
			if(!isset($row['amount'])){
				$getamount = getamount($row); $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
				$row['amount'] = $getamount['amount'];
				}
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" COLSPAN="2" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$row['date']).'<BR>';
					$summary .= gettrans('Guests').': '.$row['numguests'].'<BR>';
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
				$summary .= '</TR>'."\n";
			} else {
			$date = mktime(0,0,0,$row['date_m'],$row['date_d'],$row['date_y']);
			if($row['onlydateavl'] > 0){ $onyldateavl = gettrans('Yes'); } else { $onyldateavl = gettrans('No'); }
			if(!isset($row['amount'])){
				$getamount = getamount($row); $_SESSION['cart'][$id]['base'] = $getamount['base']; $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
				$row['amount'] = $getamount['amount'];
				}
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$date).'<BR>';
					if(isset($row['altdates']) && trim($row['altdates']) != ""): $summary .= gettrans('Alternate dates').': '.trim($row['altdates']).'<BR>'; endif;
					$summary .= gettrans('Guests').': '.$row['numguests'].'<BR>';
					$summary .= gettrans('Base price').': $'.number_format($row['base'],2,'.','').'<BR>';
					$summary .= gettrans('Pick Up').': '.$row['dep_loc'].'<BR>'.gettrans('Drop Off').': '.$row['arr_loc'].'<BR>';
					if(isset($row['preftime'])): $summary .= gettrans('Preferred time of day').': '.$row['preftime'].'<BR>'; endif;
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
				//Lodging adjustments
				$rm = 1;
				while($rm <= $row['numrooms'] || @$row['rmamt'.$rm] != ""){
					if($row['room'.$rm] == 1 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' single occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						} elseif($row['room'.$rm] == 3 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' triple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						} elseif($row['room'.$rm] == 4 && $row['rmamt'.$rm] > 0){
						$summary .= '<DIV>Room '.$rm.' quadruple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
						}
					$rm++;
					}
				//Lodging extensions
				if(isset($row['extlodgeid']) && is_array($row['extlodgeid'])){
				foreach($row['extlodgeid'] as $key => $val){ if(isset($row['addextl'.$val]) && $row['addextl'.$val] == "y" && $row['extlodgenights'][$key] > 0){
					$elt = ($row['extlodgeperguest'][$key]*$row['numguests']);
					if($row['numguests'] == 1){
						$elt = ($elt + $row['extlodgesingle'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 3){
						$elt = ($elt - $row['extlodgetriple'][$key]);
						} elseif($row['numrooms'] == 1 && $row['numguests'] == 4){
						$elt = ($elt - $row['extlodgequad'][$key]);
						} elseif($row['numrooms'] > 1){
							for($rm=1; $rm<=$row['numrooms']; $rm++){
								if(@$row['room'.$rm] == 1){
									$elt = ($elt + $row['extlodgesingle'][$key]);
									} elseif(@$row['room'.$rm] == 3){
									$elt = ($elt - $row['extlodgetriple'][$key]);
									} elseif(@$row['room'.$rm] == 4){
									$elt = ($elt - $row['extlodgequad'][$key]);
									}
								}
						}
					$summary .= '<DIV>Extended stay at '.$row['extlodgename'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.number_format($elt,2,'.','').' x '.$row['extlodgenights'][$key].' '.gettrans('night(s)').' = <B>$'.number_format( ($elt*$row['extlodgenights'][$key]),2,'.','').'</B></DIV>';
					}}
					}
				//Activities
				if(!isset($row['optionid'])){ $row['optionid'] = array(); }
				foreach($row['optionid'] as $key => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y" && $row['optionpax'][$key] > 0){
					$summary .= '<DIV>'.$row['optionname'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.$row['optionprice'][$key].' x '.$row['optionpax'][$key].' '.gettrans('guest(s)').' = <B>$'.number_format( ($row['optionprice'][$key]*$row['optionpax'][$key]),2,'.','').'</B></DIV>';
					}}
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
				$summary .= '</TR>'."\n";
			} //End Type If Statement
			$TOTALCOST = ($TOTALCOST+$row['amount']);
			} //End ForEach Loop
			$summary .= "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="3" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD></TR>'."\n";
			$summary .= '</TABLE><BR>'."\n\n";
			
	//SEND CUSTOMER NOTIFICATION
		$email_subject = 'Your '.implode('/',$email_restypes).' Reservation #'.$confnum;
		$email_to = $_POST['name']." <".$_POST['email'].">";
		if(isDev()) {
			$email_to = 'BWS Testing <dominick@bernalwebservices.com>';
		}
		$email_from = 'Bundu Bashers Tours <info@bundubashers.com>';
		$email_body = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$email_body .= 'Reservation number: <B>'.$confnum.'</B><BR><BR>'."\n";
			$email_body .= 'Thank you! This email acknowledges your order.<BR><BR>'."\n";
			$email_body .= 'Please note that your reservation is not guaranteed until you get a confirmation email from us. If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours. If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.<BR><BR>'."\n";
			$email_body .= 'We strongly advise you against making any plans that are dependent on this reservation, until you receive the confirmation email.<BR><BR>'."\n\n";
			//$email_body .= 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.<BR><BR>'."\n\n";
			$email_body .= $summary."<BR>\n\n";
			$email_body .= '1800 724 7767 extension 12 or (USA) 801 467 8687 x 12 info@bundubashers.com<BR><BR>'."\n\n";
			$email_body .= '<A HREF="http://www.bundubashers.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bashers.jpg" BORDER="0" ALT="Bundu Bashers"></A>&nbsp;&nbsp;<A HREF="http://www.bundubus.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bus.jpg" BORDER="0" ALT="Bundu Bus"></A><BR>'."\n";

		$emailObj = new email();
		$emailObj->setIDreservation($confnum);
		$emailObj->setTo($email_to);
		$emailObj->setFrom($email_from);
		$emailObj->setSubject($email_subject);
		$emailObj->setBodyHTML($email_body);
		$result = $emailObj->send();
		if($result) {
			log_audit(array(
				'primary_id' => $confnum,
				'table' => 'reservations',
				'data' => null,
				'comment' => 'Successfully sent customer email notification.'
				));
		} else {
			array_push($errormsg,'Unable to send customer notification.');
		}

	echo '<BR><SPAN STYLE="font-family:Arial; font-size:20pt;">'.gettrans('Your order number').': <B>'.$confnum.'</B></SPAN><BR><BR>'."\n\n";

	echo $summary.'<BR>'."\n\n";

	echo '<DIV STYLE="font-family:Arial; font-size:10pt; text-align:left; color:#FF0000; width:80%;">';
	echo gettrans('Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.  If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours.  If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.').'<BR><BR>'."\n\n";
	echo gettrans('We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.').'<BR><BR>';
	//echo 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV>';

	//Temp - run this so blocks get created
	$resObj = new reservation($confnum);
	if($resObj->id > 0) {
		$resObj->save();
	}

	unset($_SESSION['cart']);


echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";


//!TOUR RESERVATION ***************************************************************************************************
} elseif(isset($_REQUEST['t']) && $_REQUEST['t'] != ""){


if(isset($_REQUEST['edit']) && isset($_SESSION['cart'][$_REQUEST['edit']])){
	$fillform = $_SESSION['cart'][$_REQUEST['edit']];
	$fillform['cartid'] = $_REQUEST['edit'];
	} else {
	$fillform = $_REQUEST;
	$fillform['cartid'] = '*new*';
	$fillform['typeid'] = $_REQUEST['t'];
	$fillform['tourid'] = $_REQUEST['t'];
	}

$query = 'SELECT * FROM `tours` WHERE `id` = "'.$_REQUEST['t'].'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysqlQuery($query);
	$num_results = @mysql_num_rows($result);

if(isset($num_results) && $num_results > 0){
$tourinfo = mysql_fetch_assoc($result);
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($tourinfo,true)).'</PRE>';

	$tourinfo['safetitle'] = $tourinfo['title']; //.' ('.$tourinfo['id'].')';

	//CHECK FOR PREFERRED LANGUAGE
	if($_SESSION['lang'] != "1"){
	$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$tourinfo['id'].'" AND `lang` = "'.$_SESSION['lang'].'" ORDER BY `id` DESC LIMIT 1';
	$result = @mysqlQuery($query);
	$num_results = @mysql_num_rows($result);
		if($num_results > 0){
		$langinfo = mysql_fetch_assoc($result);
		if(trim($langinfo['title']) != ""): $tourinfo['title'] = $langinfo['title']; endif;
		if(trim($langinfo['short_desc']) != ""): $tourinfo['short_desc'] = $langinfo['short_desc']; endif;
		if(trim($langinfo['pricingdesc']) != ""): $tourinfo['pricingdesc'] = $langinfo['pricingdesc']; endif;
		if(trim($langinfo['highlights']) != ""): $tourinfo['highlights'] = $langinfo['highlights']; endif;
		if(trim($langinfo['details']) != ""): $tourinfo['details'] = $langinfo['details']; endif;
		if(trim($langinfo['pleasenote']) != ""): $tourinfo['pleasenote'] = $langinfo['pleasenote']; endif;
		}
	}

//echo '<PRE STYLE="text-align:left;">'; print_r($_SESSION['cart']); echo '</PRE>';

$touridlist = array();
$cartTourArry = array();
foreach(@$_SESSION['cart'] as $row){
	if(@$row['type'] == "t" && @$row['typeid'] != "" && @$row['typeid'] > 0 && !isset($row['updc'])) {
		$touridlist[] = $row['typeid'];
	}
	if(@$row['type'] == "t" && @$row['typeid'] != "" && @$row['typeid'] > 0) {
		$cartTourArry[$row['typeid']] = (@$cartTourArry[$row['typeid']] + 1);
	}
}

//GET OFFER
$tourinfo['offer'] = array();
if(count($touridlist) > 0) {
	$query = 'SELECT * FROM `tours_upsell`
			WHERE `upsellid` = "'.$tourinfo['id'].'" AND `tourid` > 0 AND `tourid` IN ("'.implode('", "',$touridlist).'")
			ORDER BY `discount` DESC';
	//echo $query.'<BR>';
	$result = mysqlQuery($query);
	$tourinfo['offer'] = @mysql_fetch_assoc($result);
	/*
	while($row = @mysql_fetch_assoc($result)) {
		if(@$cartTourArry[$row['tourid']] > @$cartTourArry[$row['upsellid']]
			|| getval('updc') != "" //Already discounted
			) {
			$tourinfo['offer'] = $row;
			break;	
		}
	}
	*/
	//echo '<PRE STYLE="text-align:left;">'; print_r($tourinfo); echo '</PRE>';
}

if(FALSE) {
	echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($touridlist,true)).'</PRE>';
	echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($cartTourArry,true)).'</PRE>';
	echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
}

//GET LODGING EXTENSTIONS
$tourinfo['extlodging'] = array();
$query = 'SELECT DISTINCT tours_assoc.`tourid`, tours_extlodging.`id` AS `extlodgeid`, tours_extlodging.`name`, tours_extlodging.`perguest`, tours_extlodging.`single`, tours_extlodging.`triple`, tours_extlodging.`quad`, tours_extlodging.`vendor`, tours_assoc.`order`';
	$query .= ' FROM `tours_assoc`,`tours_extlodging`';
	$query .= ' WHERE tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "f" AND tours_assoc.`type` = "e" AND tours_assoc.`typeid` = tours_extlodging.`id`';
	$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC';
	//echo $query.'<BR>';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		array_push($tourinfo['extlodging'],$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($tourinfo['extlodging']); echo '</PRE>';

//GET ACTIVITIES
$tourinfo['options'] = array();
$query = 'SELECT DISTINCT tours_assoc.`tourid`, activities.`id` AS `optionid`, activities.`name`, activities.`price`, activities.`vendor`, tours_assoc.`order`';
	$query .= ' FROM `tours_assoc`,`activities`';
	$query .= ' WHERE tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "f" AND tours_assoc.`type` = "a" AND tours_assoc.`typeid` = activities.`id`';
	$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC';
	//echo $query.'<BR>';
$result = @mysqlQuery($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tourinfo['options'],$row);
	}

//GET TOUR DATES
$dates = array();
$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" ORDER BY `date` ASC';
$result = mysqlQuery($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($dates[date("Y",$row['date'])])){ $dates[date("Y",$row['date'])] = array(); }
	if(!isset($dates[date("Y",$row['date'])][date("n",$row['date'])])){ $dates[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
	$dates[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = 1;
	//array_push($dates,$row);
	}

if(getval('date_m') == "" || getval('date_d') == "" || getval('date_y') == ""){
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" AND `date` > "'.$time.'" ORDER BY `date` ASC LIMIT 1';
	$result = mysqlQuery($query);
	$num_results = @mysql_num_rows($result);
	if($num_results == 1){
		$date = mysql_fetch_assoc($result);
		$fillform['date_m'] = date("n",$date['date']);
		$fillform['date_d'] = date("j",$date['date']);
		$fillform['date_y'] = date("Y",$date['date']);
		}
	}

$preftimes = array("Morning","Mid Day","Afternoon","Evening");

$lunchtypes = array("Turkey","Beef","Ham","Vegetarian");

//Factor in discounts for upsold tours
if(@$tourinfo['offer']['discount'] != "" && $tourinfo['offer']['discount'] >= 0 && $tourinfo['offer']['discount'] <= 100){
	$tourinfo['perguest'] = number_format($tourinfo['perguest']*($tourinfo['offer']['discount']/100),2,'.','');
	$tourinfo['single'] = number_format($tourinfo['single']*($tourinfo['offer']['discount']/100),2,'.','');
	$tourinfo['triple'] = number_format($tourinfo['triple']*($tourinfo['offer']['discount']/100),2,'.','');
	$tourinfo['quad'] = number_format($tourinfo['quad']*($tourinfo['offer']['discount']/100),2,'.','');
	if($tourinfo['offer']['discount'] == 0) {
		$tourinfo['fuelsurcharge'] = 0;
	}
}

//Setup agent discount for 
$agent_discount = 1;
if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
	$agent_discount = ($_SESSION['agent']['discount'] / 100);
	if(isset($tourinfo['agent'.$_SESSION['agent']['tier'].'_per']) && $tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] > 0){
		$agent_discount = ($tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] / 100);
		}
	}

?><SCRIPT><!--

window.onload = tour_total;

var perguest = <?=($tourinfo['perguest']>0?$tourinfo['perguest']:'0')?>;
var fuelsur = <?=($tourinfo['fuelsurcharge']>0?$tourinfo['fuelsurcharge']:'0')?>;
var twobedsur = <?=($tourinfo['twobed_surcharge']>0?$tourinfo['twobed_surcharge']:'0')?>;
var single = <?=($tourinfo['single']>0?$tourinfo['single']:'0')?>;
var triple = <?=($tourinfo['triple']>0?$tourinfo['triple']:'0')?>;
var quad = <?=($tourinfo['quad']>0?$tourinfo['quad']:'0')?>;
var agper = <? echo $agent_discount; ?>;

var numdays = <?=$tourinfo['numdays']?>;
var ext_pricing = new Array();
<?
	//Extended pricing is for single day tours only
	$ext = array();
	if($tourinfo['numdays'] == 1 && $tourinfo['ext_pricing'] != ""){
		$ext_max_guests = 0;
		$tourinfo['ext_pricing'] = explode('|',$tourinfo['ext_pricing']);
		foreach($tourinfo['ext_pricing'] as $val){
			$val = explode(':',$val);
			echo '	ext_pricing['.$val[0].'] = '.$val[1].';'."\n";
			$ext[$val[0]] = $val[1];
			if($val[0] > $ext_max_guests) {
				$ext_max_guests = $val[0];
			}
		}
	}
?>

var runs = new Array();
<? foreach($dates as $y => $years){
	echo "\t".'runs['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'runs['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' runs['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

function copypickup(){
	if(document.getElementById("sameasp").checked == true){
	document.getElementById("dropoff").value = document.getElementById("pickup").value;
	}
}

function taltdates(){
	if(document.getElementById("onlydateavl").value == 0){ var d = ""; } else { var d = "none"; }
	document.getElementById("showalt1").style.display = d;
	document.getElementById("showalt2").style.display = d;
}

function opt_check(optid){
	if(document.getElementById('opt'+optid).checked){
		var num = document.getElementById('numguests').value;
		if(document.getElementById('optpax'+optid).value == 0){ document.getElementById('optpax'+optid).value = num; }
		} else {
		document.getElementById('optpax'+optid).value = 0;
		}
	tour_total();
	}

function opt_choose(id){
	if(document.getElementById('optpax'+id).value > 0){
		document.getElementById('opt'+id).checked = true;
		} else {
		document.getElementById('opt'+id).checked = false;
		}
	tour_total();
	}

function extl_check(id){
	if(document.getElementById('extl'+id).checked){
		if(document.getElementById('extlnight'+id).value == 0){ document.getElementById('extlnight'+id).value = 1; }
		} else {
		document.getElementById('extlnight'+id).value = 0;
		}
	tour_total();
	}

function extl_choose(id){
	if(document.getElementById('extlnight'+id).value > 0){
		document.getElementById('extl'+id).checked = true;
		} else {
		document.getElementById('extl'+id).checked = false;
		}
	tour_total();
	}

function addguest(){
	var t = document.getElementById('gueststable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.verticalAlign = 'middle';
		d.style.whiteSpace = 'nowrap';
		d.style.paddingRight = '8px';
		d.innerHTML = '<? echo gettrans('Name'); ?>: <INPUT TYPE="text" NAME="guests_name[]" VALUE="" STYLE="width:200px;"><?
			if($tourinfo['getweights'] == "y"){ echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="" STYLE="font-size:9pt; width:100px;">'; }
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'">'.str_replace("'",'\\\'',$val).'</OPTION>';
					}
				echo '</SELECT>';
				}
				?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex); adjustgnum();">';
	}

function adjustguests(){
	var num = document.getElementById('numguests').value;
	if(document.getElementById('gueststable') != undefined){
		var t = document.getElementById('gueststable');
		while(t.rows.length > num){ t.deleteRow(t.rows.length-1); }
		while(t.rows.length < num){ addguest(); }
		}
	tour_total();
	adjustrooms(1);
}

function adjustgnum(){
	if(document.getElementById('gueststable') != undefined && document.getElementById('numguests').value != document.getElementById('gueststable').rows.length){
		document.getElementById('numguests').value = document.getElementById('gueststable').rows.length;
		}
	tour_total();
	adjustrooms(1);
	}

function addroom() {
	var numguests = document.getElementById('numguests').value;
	var numrooms = document.getElementById('numrooms').value;
	var t = document.getElementById('roomstable');
		var r = t.insertRow(t.rows.length);
		var rm = (parseInt(r.rowIndex)+1);

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.verticalAlign = 'middle';
		d.style.whiteSpace = 'nowrap';
		d.innerHTML = 'Room '+rm+':&nbsp;';

		var d = r.insertCell(1);
		var newcode = '<SELECT NAME="room'+rm+'" ID="room'+rm+'" onChange="adjustrooms(0);">';
		for(var i=1; i<5; i++){
			newcode += '<OPTION VALUE="'+i+'">'+i+'</OPTION>';
			}
			newcode += '</SELECT>'+"\n";

		newcode += 'guests, ';
		newcode += '<span class="num_beds_per_room">'+"\n";
			newcode += '<label class="bed_opt_1"><input type="radio" name="num_beds_'+rm+'" value="1" checked> 1 bed</label>'+"\n";
			newcode += '<label class="bed_opt_2"><input type="radio" name="num_beds_'+rm+'" value="2"> 2 beds</label>'+"\n";
		newcode += '</span>'+"\n";

		d.innerHTML = newcode;
}

function adjustrooms(dist){
	if($('#numrooms').length == 0) {
		return false;
	}

	var numguests = parseInt(document.getElementById('numguests').value);
	var numrooms = parseInt(document.getElementById('numrooms').value);
	var msg = new Array();

	if(numrooms > numguests){
		numrooms = numguests;
		msg.push('The number of rooms can\'t be<BR>greater than the number of guests.');
	}
	if((numguests/numrooms) > 4){
		numrooms = Math.ceil(numguests/4);
		//msg.push('There can\'t be more than 4 guests per room.');
	}

	if(numguests == 2 && numrooms == 1) {
		$('.num_beds_main_td').show();
	} else {
		$('.num_beds_main_td').hide();
	}

	if(numrooms > 1) {
		$('.gpr_td').show();
	} else {
		$('.gpr_td').hide();
	}
	document.getElementById('numrooms').value = numrooms;

	var t = document.getElementById('roomstable');
	if(dist || t.rows.length != numrooms) {
		while(t.rows.length < numrooms){ addroom(); }
		while(t.rows.length > numrooms){ t.deleteRow(t.rows.length - 1); }
		if(numrooms == numguests) {
			for(var i=1; i<=numrooms; i++) {
				document.getElementById('room'+i).value = 1;
			}
		} else {
			var g = 1;
			if((numguests/numrooms) == Math.floor(numguests/numrooms)) {
				g = (numguests/numrooms);
			} else {
				g = Math.ceil(numguests/numrooms);
			}
			var gleft = numguests;
			var rleft = numrooms;
			for(var i=1; i<=numrooms; i++) {
				if(gleft == rleft) {
					g = 1;
				} else if(g > gleft) {
					g = gleft;
				}
				document.getElementById('room'+i).value = g;
				gleft = (gleft - g);
				rleft--;
			}
		}
	} else {
		var totalboard = 0;
		for(var i=1; i<=numrooms; i++){
			totalboard = (totalboard + parseInt(document.getElementById('room'+i).value));
			}
		if(totalboard != numguests){ msg.push('Please check your numbers.<BR>Total guests for all rooms<BR>is different from number<BR>of guests for the tour.'); }
	}

	document.getElementById('gpr_msg').innerHTML = msg.join('<BR>');
	tour_total();
}

//!left off here
function arrayfield(formid,field,ignore_empty){
	var x = document.getElementById(formid).elements;
	var info = new Array();
	for(i in x){ if(x[i] != undefined && x[i]['name'] == field+'[]'){
		if(!ignore_empty || x[i]['value'] != ""){ info.push(x[i]['value']); }
		}}
	return info;
	}

function tour_total() {
	var numguests = parseInt(document.getElementById('numguests').value);

	//alert('numdays:'+numdays+' numguests:'+numguests+' price:'+ext_pricing[numguests]);
	if(numdays != undefined && numdays == 1 && ext_pricing != undefined && ext_pricing[numguests] != undefined && ext_pricing[numguests] > 0){
		var total = parseFloat(ext_pricing[numguests]);
	} else {
		var total = (perguest * numguests);
	}

	//Adjust for lodging
	if(document.getElementById('numrooms') != undefined) {
		var numrooms = parseInt(document.getElementById('numrooms').value);
		if(numguests == 1){
			total = (total + single);
		} else if(numrooms == 1 && numguests == 3){
			total = (total - triple);
		} else if(numrooms == 1 && numguests == 4){
			total = (total - quad);
		} else if(numrooms > 1){
				for(var r=1; r<=numrooms; r++){
					var g = document.getElementById('room'+r).value;
					if(g == 1){
						total = (total + single);
					} else if(g == 3){
						total = (total - triple);
					} else if(g == 4){
						total = (total - quad);
					}
				}
		}
		total = (total * agper);

		//Add lodging extensions
		//$('.debug').html('');
		$('.check_extl').each(function(index){
			//$('.debug').append($(this).attr('id')+'<br>');
			//if($(this).attr('checked') == "checked"){
				var elt = (parseInt($('.extlodgeperguest').eq(index).val()) * numguests);
				if(numguests == 1){
					elt = (elt + parseFloat($('.extlodgesingle').eq(index).val()));
				} else if(numrooms == 1 && numguests == 3){
					elt = (elt - parseFloat($('.extlodgetriple').eq(index).val()));
				} else if(numrooms == 1 && numguests == 4){
					elt = (elt - parseFloat($('.extlodgequad').eq(index).val()));
				} else if(numrooms > 1){
					for(var r=1; r<=numrooms; r++){
						var g = document.getElementById('room'+r).value;
						if(g == 1){
							elt = (elt + parseFloat($('.extlodgesingle').eq(index).val()));
						} else if(g == 3){
							elt = (elt - parseFloat($('.extlodgetriple').eq(index).val()));
						} else if(g == 4){
							elt = (elt - parseFloat($('.extlodgequad').eq(index).val()));
						}
					}
				}
				var extl_id = $('.extlodgeid').eq(index).val();
				$('#extlpernight'+extl_id).html(elt.toFixed(2));
				if(parseInt($('.extlnight').eq(index).val()) > 0){
					total = (total + (parseInt($('.extlnight').eq(index).val()) * elt));
				}				
			//}
		});

		/*
		var extlids = arrayfield('resform','extlodgeid',0);
		var extlperguest = arrayfield('resform','extlodgeperguest',0);
		var extlsingle = arrayfield('resform','extlodgesingle',0);
		var extltriple = arrayfield('resform','extlodgetriple',0);
		var extlquad = arrayfield('resform','extlodgequad',0);
		var extlnights = arrayfield('resform','extlodgenights',0);
		for(i in extlids){
			var elt = (parseFloat(extlperguest[i])*numguests);
			if(numguests == 1){
				elt = (elt + parseFloat(extlsingle[i]));
			} else if(numrooms == 1 && numguests == 3){
				elt = (elt - parseFloat(extltriple[i]));
			} else if(numrooms == 1 && numguests == 4){
				elt = (elt - parseFloat(extlquad[i]));
			} else if(numrooms > 1){
				for(var r=1; r<=numrooms; r++){
					var g = document.getElementById('room'+r).value;
					if(g == 1){
						elt = (elt + parseFloat(extlsingle[i]));
					} else if(g == 3){
						elt = (elt - parseFloat(extltriple[i]));
					} else if(g == 4){
						elt = (elt - parseFloat(extlquad[i]));
					}
				}
			}
			document.getElementById('extlpernight'+extlids[i]).innerHTML = elt.toFixed(2);
			if(parseInt(extlnights[i]) > 0){
				total = (total + (parseInt(extlnights[i]) * elt));
			}
		}
		*/
	} else if(numguests == 1){
		total = (total + single);
		total = (total * agper);
	} else {
		total = (total * agper);		
	}

	//Add fuel surcharge
	total += (fuelsur * numguests);

	//Add activities
	$('.check_opt').each(function(index){
		if($(this).attr('checked') == "checked"){
			var opt_price = parseFloat($('.optionprice').eq(index).val());
			var opt_pax = parseInt($('.optionpax').eq(index).val());
			total = (total + (opt_price*opt_pax));
		}
	});

	/*
	var optids = arrayfield('resform','optionid',0);
	var optprices = arrayfield('resform','optionprice',0);
	var optpax = arrayfield('resform','optionpax',0);
	for(i in optids){
		if(document.getElementById('opt'+optids[i]) != undefined && document.getElementById('opt'+optids[i]).checked){
			total = (total + (parseInt(optpax[i]) * parseFloat(optprices[i])));
		}
	}
	*/

	document.getElementById('tourcost').innerHTML = '$'+total.toFixed(2);
}

// -->
</SCRIPT>

<style>
	.total_box {
		background-color: #e6e6e6;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		border-radius: 10px;
		-moz-box-shadow: 0px 0px 6px #666666;
		-webkit-box-shadow: 0px 0px 6px #666666;
		box-shadow: 0px 0px 6px #666666;
		filter: progid: DXImageTransform.Microsoft.gradient(startColorstr = '#eeeeee', endColorstr = '#d6d6d6');
		-ms-filter: "progid: DXImageTransform.Microsoft.gradient(startColorstr = '#eeeeee', endColorstr = '#d6d6d6')";
		background-image: -moz-linear-gradient(top, #eeeeee, #d6d6d6);
		background-image: -ms-linear-gradient(top, #eeeeee, #d6d6d6);
		background-image: -o-linear-gradient(top, #eeeeee, #d6d6d6);
		background-image: -webkit-gradient(linear, center top, center bottom, from(#eeeeee), to(#d6d6d6));
		background-image: -webkit-linear-gradient(top, #eeeeee, #d6d6d6);
		background-image: linear-gradient(top, #eeeeee, #d6d6d6);
		-moz-background-clip: padding;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		margin-top: 10px;
		margin-left: 20px;
		padding: 10px;
		position: relative;
	}

	.debug {
		position: fixed;
		right: 0px;
		bottom: 0px;
	}
</style>

<script>
	$(window).scroll(function(){
		var margin = 150;
		var win_top = $(window).scrollTop();
		var win_bot = ($(window).scrollTop() + $(window).height() - margin);
		var td_pos = $('#total_box_top').offset();
		var td_bot = (td_pos.top + $('#total_box_td').height() - margin);
		var total_box_top_pos = $('#total_box_top').offset();
		var total_box_pos = $('#total_box_div').offset();
		var total_box_top = total_box_top_pos.top;
		var total_box_bot = (total_box_pos.top + $('#total_box_div').height());
		var diff_top = (win_top - total_box_top + margin);
		var diff_bot = (win_bot - total_box_bot);
		var debug_act = '';

		if(diff_top < 0){
			$('#total_box_div').css('top', '0px');
			debug_act = 'Set to boundary top';

		} else if((total_box_top + diff_top + $('#total_box_div').height()) > td_bot){
			var new_top = (td_bot - $('#total_box_div').height() - total_box_top);
			$('#total_box_div').css('top', new_top+'px');
			debug_act = 'Set to boundary bottom';

		} else {
			$('#total_box_div').css('top', diff_top+'px');
			debug_act = 'Set to window top';
		}
		
		//$('.debug').html('total_box Top: '+total_box_top+', total_box Bottom: '+total_box_bot+'<br>Win Top: '+win_top+', Win Bottom: '+win_bot+'<br>Diff Top: '+diff_top+', Diff Bottom: '+diff_bot+'<br>Debug Act: '+debug_act);
	});
</script>

<?

echo '<DIV STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:8px;">'.$tourinfo['title'].'</DIV>'."\n";
	echo '<BR>'."\n\n";

//!TOUR FORM START
echo '<FORM NAME="resform" ID="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="addtocart">'."\n";
echo '<INPUT TYPE="hidden" NAME="cartid" VALUE="'.getval('cartid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="type" VALUE="t">'."\n";
echo '<INPUT TYPE="hidden" NAME="typeid" VALUE="'.getval('typeid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="tourid" VALUE="'.getval('typeid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="title" VALUE="'.$tourinfo['title'].'">'."\n";
if(getval('updc') != "" && getval('updc') >= 0 && getval('updc') <= 100) {
	echo '<INPUT TYPE="hidden" NAME="updc" VALUE="'.getval('updc').'">'."\n";
}
echo "\n";

?> 
<style>
	.field_label_td {
		font-family: Arial, Helvetica;
		font-size: 10pt;
		font-weight: bold;
		text-align: right;
		padding-right: 10px;
	}
	.field_desc {
		font-size: 8pt;
		font-weight: normal;
		color: #666666;
	}
</style>
<?

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:310px;">'.gettrans('Tour date').'</TD><TD ALIGN="left">';
	//echo '<INPUT TYPE="hidden" NAME="date_m" VALUE="'.getval('date_m').'">
	echo '<SELECT NAME="date_m">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_m') == $i ){ echo " SELECTED"; }
			echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_d">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_d') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_y">';
		for($i=date("Y",$time); $i<(date("Y",$time)+6); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_y') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD>'."\n";

?>
	<td id="total_box_td" rowspan="20" valign="top" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">
		<div id="total_box_top"></div>
		<div id="total_box_div" class="total_box">
			<?
			echo '<span>'.gettrans('Tour Cost').'</span><BR>'."\n";
			echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">';
			echo wordwrap(gettrans('Based on the options chosen so far, your total cost for this tour will be:'), 38, '<br>');
			echo '</SPAN>'."\n";
			$initial_cost = 0;
				if($tourinfo['numdays'] == 1 && isset($ext[2]) && $ext[2] > 0){
					$initial_cost = $ext[2];
				} else {
					$initial_cost = ($tourinfo['perguest']*2);
				}
				$initial_cost = ($initial_cost*$agent_discount);
				$initial_cost += ($tourinfo['fuelsurcharge']*2);
			echo '<DIV ID="tourcost" STYLE="font-size:14pt;">$'.number_format($initial_cost,2,'.','').'</DIV>'."\n";
			echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#333333;">';
			echo gettrans('Any applicable surcharge has been included.');
			echo '</SPAN>'."\n";
			if(@$tourinfo['offer']['discount'] != "" && $tourinfo['offer']['discount'] >= 0 && $tourinfo['offer']['discount'] <= 100){
				$per = (100-$tourinfo['offer']['discount']);
				$per = number_format($per,0);
				echo '<DIV STYLE="font-family:Arial; font-size:10pt; font-weight:bold; color:#FF6600; padding-top:2px;">';
				echo gettrans('This tour has been discounted!');
				echo '</DIV>'."\n\n";
				}
			?> 
		</div>
		<div class="debug"></div>
	</td>
</tr>
<?

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Is this the only date you are available?').'</TD><TD ALIGN="left"><SELECT NAME="onlydateavl" ID="onlydateavl" onChange="taltdates()"><OPTION VALUE="0"';
	if(getval('onlydateavl') == 0): echo ' SELECTED'; endif;
	echo '>'.gettrans('No').'</OPTION><OPTION VALUE="1"';
	if(getval('onlydateavl') == 1): echo ' SELECTED'; endif;
	echo '>'.gettrans('Yes').'</OPTION></SELECT></TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;"><DIV ID="showalt1"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '>'.gettrans('If not, what alternate dates are you available?').'</DIV></TD><TD ALIGN="left"><DIV ID="showalt2"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '><TEXTAREA NAME="altdates" ID="altdates" COLS="30" ROWS="2">'.getval('altdates').'</TEXTAREA></DIV></TD></TR>'."\n";

if($tourinfo['getpreftime'] == "y"){
	echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Preferred time of day').'</TD><TD ALIGN="left"><SELECT NAME="preftime">'; foreach($preftimes as $thistime){ echo '<OPTION VALUE="'.$thistime.'"'; if(getval('preftime') == $thistime): echo ' SELECTED'; endif; echo '>'.$thistime.'</OPTION>'; } echo '</SELECT></TD></TR>'."\n";
	}
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for pick up').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'</SPAN></TD><TD ALIGN="left"><TEXTAREA NAME="dep_loc" ID="pickup" COLS="30" ROWS="2" onChange="copypickup()">'.getval('dep_loc').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for drop off').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'<BR><label for="sameasp">'.gettrans('Check if this is the same as pick up').'</label></SPAN><INPUT TYPE="checkbox" ID="sameasp" onClick="copypickup()"></TD><TD ALIGN="left"><TEXTAREA NAME="arr_loc" ID="dropoff" COLS="30" ROWS="2">'.getval('arr_loc').'</TEXTAREA></TD></TR>'."\n";

$max_guests = 10;
	if($tourinfo['perguest'] > 0) {
		$max_guests = 55;
	}
	if(isset($ext_max_guests) && $ext_max_guests > 0) {
		$max_guests = $ext_max_guests;
	}

if(getval('numguests') == ""){ $fillform['numguests'] = 2; }
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Number of guests').'<BR>';
	//echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.wordwrap(gettrans('Please contact us for a group rate if there are more than eight in your party.'),50,'<BR>').'</SPAN>';
	echo '</TD><TD ALIGN="left"><SELECT NAME="numguests" ID="numguests" STYLE="font-size:14pt;" onChange="adjustguests();">';
	for($i=1; $i<=$max_guests; $i++){
		echo'<OPTION VALUE="'.$i.'"';
			if($i == getval('numguests')){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";

if($tourinfo['numdays'] > 1 && ($tourinfo['single']+$tourinfo['triple']+$tourinfo['quad']) != 0){
	if(getval('numrooms') == ""){ $fillform['numrooms'] = 1; }
	echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Number of rooms').'</TD><TD ALIGN="left"><SELECT NAME="numrooms" ID="numrooms" onChange="adjustrooms(1);">';
		for($i=1; $i<=$max_guests; $i++){
			echo'<OPTION VALUE="'.$i.'"';
				if($i == getval('numrooms')){ echo ' SELECTED'; }
				echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n";

	?> 
	<tr>
		<td class="num_beds_main_td field_label_td">
			<?=gettrans('Number of beds')?> 
		</td>
		<td class="num_beds_main_td">
			<?=quick_select(array(
				'id' => 'num_beds_main',
				'name' => 'num_beds_main',
				'options' => array(1,2),
				'selected' => getval('num_beds_main'),
				'value_is_key' => true,
				'onchange' => 'adjustrooms(0);'
				))?> 
		</td>
	</tr>

	<tr>
		<td class="gpr_td field_label_td" style="<?=(getval('numrooms')==1?'display:none':'')?>">
			<?=gettrans('Number of guests per room')?><br>
			<span class="field_desc">
				<?=gettrans('Rooms with more than one bed are subject to availability.')?><br>
				<i><?=wordwrap(gettrans('Please be sure the total guests for all rooms is the same as the number of guests for the tour.'),50,'<br>')?></i>
			</span>
		</td>
		<td class="gpr_td" style="<?=(getval('numrooms')==1?'display:none':'')?>">
			<div id="gpr_msg" style="font-family:Arial; font-size:9pt; color:#FF0000;"></div>
			<table border="0" cellpadding="0" cellspacing="0" id="roomstable" style="font-family:Arial; font-size:9pt;">
				<?
				if(getval('room1') == "") {
					$fillform['room1'] = 2;
				}
				$rm = 1;
				while($rm <= getval('numrooms') || getval('room'.$rm) != "") {
					?> 
					<tr>
						<td align="right" style="white-space:nowrap; padding-right:5px;">
							Room <?=$rm?>:
						</td>
						<td>
							<?=quick_select(array(
								'id' => 'room'.$rm,
								'name' => 'room'.$rm,
								'options' => range(1, 4),
								'selected' => getval('room'.$rm),
								'value_is_key' => true,
								'onchange' => 'adjustrooms(0);'
								))?> 
							guests,
							<span class="num_beds_per_room">
								<label class="bed_opt_1"><input type="radio" name="num_beds_<?=$rm?>" value="1" <?=(getval('num_beds_'.$rm)<2?'checked':'')?>> 1 bed</label>
								<label class="bed_opt_2"><input type="radio" name="num_beds_<?=$rm?>" value="2" <?=(getval('num_beds_'.$rm)>1?'checked':'')?>> 2 beds</label>
							</span>
						</td>
					</tr>
					<?
					$rm++;
				}
				?> 
			</table>
		</td>
	</tr>
	<?
}


if(isset($tourinfo['extlodging']) && count($tourinfo['extlodging']) > 0){
	//echo '<PRE STYLE="text-align:left;">'; print_r($tourinfo['extlodging']); echo '</PRE>';
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Lodging Extension').'<BR>'."\n";
		echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">You have the option of extending your stay at certain locations during the tour.</SPAN>'."\n";

		echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">';
		foreach($tourinfo['extlodging'] as $row){
			echo '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="extl'.$row['extlodgeid'].'" onClick="extl_check(\''.$row['extlodgeid'].'\');">'.gettrans('Extend your stay in').' '.gettrans($row['name']).':</LABEL></TD>';
			echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="extl'.$row['extlodgeid'].'" onClick="extl_check(\''.$row['extlodgeid'].'\');">+ $<SPAN ID="extlpernight'.$row['extlodgeid'].'">'.number_format(($row['perguest'] * 2),2,'.','').'</SPAN> '.gettrans('per night').'</LABEL></TD>';
			echo '<TD ALIGN="center"><INPUT TYPE="hidden" NAME="extlodgeid[]" class="extlodgeid" VALUE="'.$row['extlodgeid'].'"><INPUT TYPE="hidden" NAME="extlodgename[]" class="extlodgename" VALUE="'.$row['name'].'"><INPUT TYPE="hidden" NAME="extlodgeperguest[]" class="extlodgeperguest" VALUE="'.$row['perguest'].'"><INPUT TYPE="hidden" NAME="extlodgesingle[]" class="extlodgesingle" VALUE="'.$row['single'].'"><INPUT TYPE="hidden" NAME="extlodgetriple[]" class="extlodgetriple" VALUE="'.$row['triple'].'"><INPUT TYPE="hidden" NAME="extlodgequad[]" class="extlodgequad" VALUE="'.$row['quad'].'"><INPUT TYPE="checkbox" NAME="addextl'.$row['extlodgeid'].'" ID="extl'.$row['extlodgeid'].'" class="check_extl" VALUE="y"';
				if(getval('addextl'.$row['extlodgeid']) == "y"): echo ' CHECKED'; endif;
				echo ' onClick="extl_check(\''.$row['extlodgeid'].'\');"></TD>';
			echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('For').'</TD>';
			echo '<TD>';
			echo '<SELECT NAME="extlodgenights[]" ID="extlnight'.$row['extlodgeid'].'" class="extlnight" onChange="extl_choose(\''.$row['extlodgeid'].'\');">';
				for($i=0; $i<11; $i++){
					echo'<OPTION VALUE="'.$i.'"';
						if(isset($fillform['extlodgeid']) && is_array($fillform['extlodgeid'])){
							$key = array_search($row['extlodgeid'],$fillform['extlodgeid']);
							if($key !== FALSE && isset($fillform['extlodgenights'][$key]) && $fillform['extlodgenights'][$key] == $i){ echo ' SELECTED'; }
							}
						echo '>'.$i.'</OPTION>';
					}
				echo '</SELECT>';
				echo '</TD>';
			echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('nights').'.</TD>';
			echo '</TR>'."\n";
			}
			echo '</TABLE>'."\n";

		echo '</TD></TR>'."\n";
	}

if(isset($tourinfo['options']) && count($tourinfo['options']) > 0){
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Tour Options').'<BR>'."\n";
	echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">';
	foreach($tourinfo['options'] as $row){
		echo '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="opt'.$row['optionid'].'" onClick="opt_check(\''.$row['optionid'].'\')">'.gettrans($row['name']).':</LABEL></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="opt'.$row['optionid'].'" onClick="opt_check(\''.$row['optionid'].'\')">+ $'.$row['price'].' '.gettrans('per guest').'</LABEL></TD>';
		echo '<TD ALIGN="center"><INPUT TYPE="hidden" NAME="optionid[]" class="optionid" VALUE="'.$row['optionid'].'"><INPUT TYPE="hidden" NAME="optionname[]" class="optionname" VALUE="'.$row['name'].'"><INPUT TYPE="hidden" NAME="optionprice[]" class="optionprice" VALUE="'.$row['price'].'"><INPUT TYPE="checkbox" NAME="addopt'.$row['optionid'].'" class="check_opt" ID="opt'.$row['optionid'].'" VALUE="y"';
			if(getval('addopt'.$row['optionid']) == "y"): echo ' CHECKED'; endif;
			echo ' onClick="opt_check(\''.$row['optionid'].'\')"></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('For').'</TD>';
		echo '<TD>';
		echo '<SELECT NAME="optionpax[]" ID="optpax'.$row['optionid'].'" class="optionpax" onChange="opt_choose(\''.$row['optionid'].'\');">';
			for($i=0; $i<11; $i++){
				echo'<OPTION VALUE="'.$i.'"';
					if(isset($fillform['optionid']) && is_array($fillform['optionid'])){
						$key = array_search($row['optionid'],$fillform['optionid']);
						if($key !== FALSE && isset($fillform['optionpax'][$key]) && $fillform['optionpax'][$key] == $i){ echo ' SELECTED'; }
						}
					echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT>';
		/*echo '<INPUT TYPE="text" NAME="optionpax[]" ID="optpax'.$row['optionid'].'" STYLE="width:32px;" VALUE="';
		if(isset($fillform['optionid']) && is_array($fillform['optionid'])){
			$key = array_search($row['optionid'],$fillform['optionid']);
			if($key !== FALSE && isset($fillform['optionpax'][$key])){ echo $fillform['optionpax'][$key]; }
			}
			echo '">';*/
			echo '</TD>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('guests').'.</TD>';
		echo '</TR>'."\n";
		}
		echo '</TABLE>'."\n";
		echo '</TD></TR>'."\n";
	}

/*
echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Tour Cost').'<BR>'."\n";
	echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Based on the options you chose, your total cost for this tour will be:</SPAN>'."\n";
	$initial_cost = 0;
		if($tourinfo['numdays'] == 1 && isset($ext[2]) && $ext[2] > 0){
			$initial_cost = $ext[2];
		} else {
			$initial_cost = ($tourinfo['perguest']*2);
		}
		$initial_cost = ($initial_cost*$agent_discount);
		$initial_cost += ($tourinfo['fuelsurcharge']*2);
	echo '<DIV ID="tourcost" STYLE="font-size:14pt;">$'.number_format($initial_cost,2,'.','').'</DIV>'."\n";
	echo '<SPAN STYLE="font-size:8pt; font-weight:normal; color:#333333;">Any applicable surcharge has been included.</SPAN>'."\n";
	if(isset($tourinfo['offer']) && isset($tourinfo['offer']['discount']) && $tourinfo['offer']['discount'] > 0 && $tourinfo['offer']['discount'] <= 100){
		$per = (100-$tourinfo['offer']['discount']);
		$per = number_format($per,0);
		echo '<DIV STYLE="font-family:Arial; font-size:10pt; font-weight:bold; color:#FF6600; padding-top:2px;">This tour has been discounted!</DIV>'."\n\n";
		}
	echo '</TD></TR>'."\n";
*/

if($tourinfo['getweights'] == "y" || $tourinfo['getlunch'] == "y"){
	if(!isset($fillform['guests_name'])){ $fillform['guests_name'] = array(); }
	while(count($fillform['guests_name']) < 2){ array_push($fillform['guests_name'],''); }
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Guest information');
		if($tourinfo['getweights'] == "y"){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Weights are only needed if you have ordered an airplane or helicopter tour.').'</SPAN>'."\n"; }
		echo '<DIV STYLE="padding-bottom:4px;"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" ID="gueststable">'."\n";
		foreach($fillform['guests_name'] as $key => $row){
			echo "\t".'<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap; padding-right:8px;">'.gettrans('Name').': <INPUT TYPE="text" NAME="guests_name[]" VALUE="'.$fillform['guests_name'][$key].'" STYLE="font-size:9pt; width:200px;">';
			if($tourinfo['getweights'] == "y"){
				echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="';
				if(isset($fillform['guests_weight'][$key])){ echo $fillform['guests_weight'][$key]; }
				echo '" STYLE="font-size:9pt; width:100px;">';
				}
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.$val.'"';
					if($fillform['guests_lunch'][$key] == $val){ echo ' SELECTED'; }
					echo '>'.$val.'</OPTION>';
					}
				echo '</SELECT>';
				}
			echo '</TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this guest" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex); adjustgnum();"></TD></TR>'."\n";
			}
		echo "\t".'</TABLE></DIV>'."\n";
		echo "\t".'<INPUT TYPE="button" VALUE="Add another guest" STYLE="font-size:8pt;" onClick="addguest(); adjustgnum();">'."\n";
		echo "\t".'</TD></TR>'."\n\n";
	}

echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="'.gettrans('Continue').' -&gt;" STYLE="width:180px;">';
	
echo '</FORM><BR>'."\n\n";

echo '<DIV STYLE="width:80%; font-family:Arial; font-size:10pt; text-align:left; color:#FF0000;">Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.<BR>We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.';
	//echo '<BR>We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV><BR><BR>';

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubashers.com';
	if(isset($tourinfo['alias']) && $tourinfo['alias'] != ""){ echo '/tour.php?id='.urlencode( $tourinfo['alias'] ); }
	echo '">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">'.gettrans('Bundu Bus').'</A></B></FONT>'."\n\n";


} else {
	echo '<DIV STYLE="font-family:Arial; font-size:11pt; padding:20px;">The tour you have selected is not available to order at this time.<BR><BR>Please <A HREF="http://www.bundubashers.com/grand_canyon_tours_contact.php">Contact Us</A> so we can direct you to current tours that are similar.</DIV>';
} //End Num results If statement


//!SHOPPING CART ***************************************************************************************************
} else {


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

if(isset($_POST) && count($_POST) > 0){
	foreach($_POST as $key => $val){
		if(!is_array($val)){
		$_POST[$key] = strip_tags($_POST[$key]);
		$_POST[$key] = stripslashes($_POST[$key]);
		}
		}
	}

//!ADD ITEMS TO SHOPPING CART *********************************************

$offers = array();
//echo '<PRE STYLE="text-align:left;">'; print_r($_POST); echo '</PRE>';

if(isset($_POST['numrooms']) && $_POST['numrooms'] == 1){ $_POST['room1'] = $_POST['numguests']; }
if(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid']) && $_POST['cartid'] == '*new*'){
		//Check for offers
		if($_POST['type'] == "t"){
			$touridlist = array();
			foreach(@$_SESSION['cart'] as $row){
				if(@$row['type'] == "t" && @$row['typeid'] != "" && @$row['typeid'] > 0){
					array_push($touridlist,$row['typeid']);
					}
				}

			$discount = array();
			$query = 'SELECT * FROM `tours_upsell`';
				$query .= ' WHERE `upsellid` = "'.$_POST['typeid'].'" AND `tourid` > 0 AND (`tourid` = "'.implode('" OR `tourid` = "',$touridlist).'")';
				$query .= ' ORDER BY `discount` DESC LIMIT 1';
				//echo $query.'<BR>';
				$result = @mysqlQuery($query);
				$num_results = @mysql_num_rows($result);
			if($num_results > 0){
				$discount = @mysql_fetch_assoc($result);
				$_POST['updc'] = $discount['discount'];
			}

			$query = 'SELECT tours.`id`, tours.`alias`, tours.`title`, tours.`short_desc`, tours.`upsell_headline`, tours.`upsell_desc`, tours.`youtube_title`, tours.`youtube`, tours.`perguest`, tours_upsell.`discount`';
				if($_SESSION['lang'] != "1") {
					$query .= ', tours_translations.`title` AS `trans_title`';
				}
				$query .= ' FROM `tours`
					JOIN `tours_upsell` ON tours.`id` = tours_upsell.`upsellid`';
				if($_SESSION['lang'] != "1") {
					$query .= ' LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid` AND tours_translations.`lang` = "'.$_SESSION['lang'].'"';
				}
				$query .= ' WHERE tours_upsell.`tourid` = "'.$_POST['typeid'].'" AND tours.`hidden` != "1"
					GROUP BY tours.`id`
					ORDER BY tours_upsell.`order` ASC, tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
					//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
				$result = @mysqlQuery($query);
				$num_results = @mysql_num_rows($result);
			if($num_results > 0){
				for($i=0; $i<$num_results; $i++){
					$row = mysql_fetch_assoc($result);
					if(isset($row['trans_title']) && $row['trans_title'] != ""){ $row['title'] = $row['trans_title']; }
					if(!incart($row['id'])){ array_push($offers,$row); }
					}
				}
			}

	$getamount = getamount($_POST); $_POST['base'] = $getamount['base']; $_POST['amount'] = $getamount['amount'];

	//$rm = 1; while($rm <= $_POST['numrooms'] || $_POST['room'.$rm] != ""){ $_POST['rmamt'.$rm] = $getamount['room'.$rm]; $rm++; }
	for($rm=1; $rm <= @$_POST['numrooms']; $rm++){ $_POST['rmamt'.$rm] = @$getamount['room'.$rm]; }

	array_push($_SESSION['cart'],$_POST);

	} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid'])){
	$getamount = getamount($_POST); $_POST['base'] = $getamount['base']; $_POST['amount'] = $getamount['amount'];
	for($rm=1; $rm <= @$_POST['numrooms']; $rm++){ $_POST['rmamt'.$rm] = @$getamount['room'.$rm]; }
	$_SESSION['cart'][$_POST['cartid']] = $_POST;
	} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "addroutes"){
		foreach($_POST['routeid'] as $key => $routeid){
			$add = array(
				'type' => 'r',
				'title' => $_POST['title'][$key],
				'numguests' => $_POST['pax'][$key],
				'date' => $_POST['date'][$key],
				'routeid' => $_POST['routeid'][$key],
				'bbticket' => $_POST['bbticket']
				);
			$getamount = getamount($add); $add['amount'] = $getamount['amount'];
			array_push($_SESSION['cart'],$add);
			}
	} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "remove" && $_REQUEST['cartid'] != ""){
	array_splice($_SESSION['cart'],$_REQUEST['cartid'],1);
	}
	//echo '<PRE>'; print_r($_SESSION['cart']); echo '</PRE>';


//!Upsell tours - This section should run once a tour has been added if any offers are available.
if(isset($offers) && count($offers) > 0){
	//echo '<PRE STYLE="text-align:left;">'; print_r($offers); echo '</PRE>';

	foreach($offers as $offer){
		//Get images
		$images = array();
		$query = 'SELECT images.`filename`, images.`caption` FROM `images`,`images_assoc_tours` WHERE images_assoc_tours.`imgid` = images.`id` AND images_assoc_tours.`tourid` = "'.$offer['id'].'" ORDER BY images_assoc_tours.`order` ASC LIMIT 4';
		$result = @mysqlQuery($query);
		$num_results = @mysql_num_rows($result);
			for($i=0; $i<$num_results; $i++){
			$img = mysql_fetch_assoc($result);
			array_push($images,$img);
			}
			//echo '<PRE STYLE="text-align:left;">'; print_r($images); echo '</PRE>';
		if(trim($offer['upsell_headline']) == ""){ $offer['upsell_headline'] = $offer['title']; }
		if(trim($offer['upsell_desc']) == ""){ $offer['upsell_desc'] = $offer['short_desc']; }
		echo '<DIV STYLE="font-family:Helvetica; font-size:26pt; padding-top:8px;">'.$offer['upsell_headline'].'</DIV>'."\n";
		if(count($images) > 0){
			echo '<DIV STYLE="width:80%; text-align:center; padding-top:4px;">';
			foreach($images as $row){
				$checksmll = explode(".",$row['filename']);
				if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.reset($checksmll).'_small.'.end($checksmll))){
					$row['filename'] = reset($checksmll).'_small.'.end($checksmll);
					}
				$row['size'] = imgform($row['filename'],200,120);
				echo '<IMG SRC="/images/'.$row['filename'].'" BORDER="0" CLASS="imgbord" ALT="'.$row['caption'].'" STYLE="width:'.$row['size']['w'].'px; height:'.$row['size']['h'].'px; margin:0px 2px 0px;">'."\n";
				}
			echo '</DIV>'."\n";
			} //End Count Images If Statement
		echo '<DIV STYLE="width:80%; font-family:Helvetica; font-size:12pt; text-align:left; padding-top:4px;">'.$offer['upsell_desc'].'</DIV>'."\n";
		if(isset($offer['youtube']) && $offer['youtube'] != ""){
			if(isset($offer['youtube_title']) && $offer['youtube_title'] != ""){ echo '<DIV STYLE="text-align:center; margin-bottom:4px; font-family:Helvetica; font-size:10pt; font-weight:bold;">'.$offer['youtube_title'].'</DIV>'."\n"; }
			echo '<DIV STYLE="text-align:center; margin-bottom:4px;">'.$offer['youtube'].'</DIV>';
			}

		echo '<DIV STYLE="width:80%; font-family:Helvetica; font-size:16pt; text-align:center; color:#000080; padding-bottom:4px;">';
			echo '<STRIKE>$'.$offer['perguest'].'</STRIKE>, $'.number_format((($offer['discount']/100)*$offer['perguest']),2,'.',',').' per person';
			echo '</DIV>'."\n";

		echo '<TABLE BORDER="0" STYLE="margin:4px 0px 8px;"><TR>';
		echo '<TD><INPUT TYPE="button" VALUE="'.gettrans('Order now').'" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?t='.$offer['id'].'\';"></TD>';
		echo '<TD STYLE="padding:0px 10px; 0px;"><INPUT TYPE="button" VALUE="'.gettrans('See this tour in more detail').'" onClick="window.location=\'http://www.bundubashers.com/tour.php?id='.$offer['id'].'\'"></TD>';
		echo '<TD><INPUT TYPE="button" VALUE="'.gettrans('No, thanks').' -&gt;" STYLE="width:180px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'\';"></TD>';
		echo '</TR></TABLE>'."\n\n";
		}

	echo '</CENTER>'."\n\n";

	include('footer.php');

	exit;
	}


?><SCRIPT><!--

//CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
var Cards = new Array();
Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
var MasterCard = Cards[0];
Cards[1] = new CardType("VisaCard", "4", "13,16");
var VisaCard = Cards[1];
Cards[2] = new CardType("AmExCard", "34,37", "15");
var AmExCard = Cards[2];
//Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
//var DinersClubCard = Cards[3];
//Cards[4] = new CardType("DiscoverCard", "6011", "16");
//var DiscoverCard = Cards[4];
//Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
//var enRouteCard = Cards[5];
//Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
//var JCBCard = Cards[6];
var LuhnCheckSum = Cards[3] = new CardType();

function checkexp(){
	var r = true;
	var msg = '';
	if(document.getElementById("cc_num").value == ""){
		msg += '<? echo gettrans('Please enter your credit card number.'); ?>'+"\n";
		r =  false;
		}
	if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
		msg += '<? echo gettrans('Please choose an expiration date for your credit card.'); ?>'+"\n";
		r =  false;
		}
	if(r){
		var tmpyear;
		if(document.getElementById("cc_expdate_year").value > 96){
			tmpyear = "19" + document.getElementById("cc_expdate_year").value;
			} else if(document.getElementById("cc_expdate_year").value < 90){
			tmpyear = "20" + document.getElementById("cc_expdate_year").value;
			} else {
			msg += '<? echo gettrans('The card expiration year is not valid.'); ?>'+"\n";
			r =  false;
			}
		tmpmonth = document.getElementById("cc_expdate_month").value;
		if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
			msg += '<? echo gettrans('Your credit card has already expired.'); ?>'+"\n";
			r =  false;
			}
		card = false;
		for(var n = 0; n < Cards.length; n++){
			if(Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)){
				card = true;
				break;
				}
			}
		if(!card){
			msg += '<? echo gettrans('Your credit card number does not appear to be valid.'); ?>'+"\n";
			r =  false;
			}
		}
	if(document.getElementById("cc_zip").value == ""){
		msg += '<? echo gettrans('Please enter the zip or postal code associated with your credit card.'); ?>'+"\n";
		r =  false;
		}
	if(document.getElementById("email").value != document.getElementById("email2").value){
		msg += '<? echo gettrans('Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.'); ?>'+"\n";
		r =  false;
		}
	if(msg != ""){ alert(msg); }
	return r;
	}

function rmvitem(x){
	if( confirm('<? echo gettrans('Remove this item from your shopping cart?'); ?>') ){
		window.location = '<? echo $_SERVER['PHP_SELF']; ?>?utaction=remove&cartid='+x;
		}
	}




/*************************************************************************\
Object CardType([String cardtype, String rules, String len, int year, 
                                        int month])
cardtype    : type of card, eg: MasterCard, Visa, etc.
rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
len         : valid length of cardnumber, eg: "16,19", "13,16".
year        : year of expiry date.
month       : month of expiry date.
eg:
var VisaCard = new CardType("Visa", "4", "16");
var AmExCard = new CardType("AmEx", "34,37", "15");
\*************************************************************************/
function CardType() {
var n;
var argv = CardType.arguments;
var argc = CardType.arguments.length;

this.objname = "object CardType";

var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

this.setCardNumber = setCardNumber;  // set CardNumber method.
this.setCardType = setCardType;  // setCardType method.
this.setLen = setLen;  // setLen method.
this.setRules = setRules;  // setRules method.
this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

this.setCardType(tmpcardtype);
this.setLen(tmplen);
this.setRules(tmprules);
if (argc > 4)
this.setExpiryDate(argv[3], argv[4]);

this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
this.getCardType = getCardType;  // getCardType method.
this.isCardNumber = isCardNumber;  // isCardNumber method.
this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
this.luhnCheck = luhnCheck;// luhnCheck method.
return this;
}

/*************************************************************************\
boolean checkCardNumber([String cardnumber, int year, int month])
return true if cardnumber pass the luhncheck and the expiry date is
valid, else return false.
\*************************************************************************/
function checkCardNumber() {
var argv = checkCardNumber.arguments;
var argc = checkCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
var year = (argc > 1) ? argv[1] : this.year;
var month = (argc > 2) ? argv[2] : this.month;

this.setCardNumber(cardnumber);
this.setExpiryDate(year, month);

if (!this.isCardNumber())
return false;
if (!this.isExpiryDate())
return false;

return true;
}
/*************************************************************************\
String getCardType()
return the cardtype.
\*************************************************************************/
function getCardType() {
return this.cardtype;
}
/*************************************************************************\
String getExpiryDate()
return the expiry date.
\*************************************************************************/
function getExpiryDate() {
return this.month + "/" + this.year;
}
/*************************************************************************\
boolean isCardNumber([String cardnumber])
return true if cardnumber pass the luhncheck and the rules, else return
false.
\*************************************************************************/
function isCardNumber() {
var argv = isCardNumber.arguments;
var argc = isCardNumber.arguments.length;
var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
if (!this.luhnCheck())
return false;

for (var n = 0; n < this.len.size; n++)
if (cardnumber.toString().length == this.len[n]) {
for (var m = 0; m < this.rules.size; m++) {
var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
if (headdigit == this.rules[m])
return true;
}
return false;
}
return false;
}

/*************************************************************************\
boolean isExpiryDate([int year, int month])
return true if the date is a valid expiry date,
else return false.
\*************************************************************************/
function isExpiryDate() {
var argv = isExpiryDate.arguments;
var argc = isExpiryDate.arguments.length;

year = argc > 0 ? argv[0] : this.year;
month = argc > 1 ? argv[1] : this.month;

if (!isNum(year+""))
return false;
if (!isNum(month+""))
return false;
today = new Date();
expiry = new Date(year, month);
if (today.getTime() > expiry.getTime())
return false;
else
return true;
}

/*************************************************************************\
boolean isNum(String argvalue)
return true if argvalue contains only numeric characters,
else return false.
\*************************************************************************/
function isNum(argvalue) {
argvalue = argvalue.toString();

if (argvalue.length == 0)
return false;

for (var n = 0; n < argvalue.length; n++)
if (argvalue.substring(n, n+1) < "0" || argvalue.substring(n, n+1) > "9")
return false;

return true;
}

/*************************************************************************\
boolean luhnCheck([String CardNumber])
return true if CardNumber pass the luhn check else return false.
Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
\*************************************************************************/
function luhnCheck() {
var argv = luhnCheck.arguments;
var argc = luhnCheck.arguments.length;

var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

if (! isNum(CardNumber)) {
return false;
  }

var no_digit = CardNumber.length;
var oddoeven = no_digit & 1;
var sum = 0;

for (var count = 0; count < no_digit; count++) {
var digit = parseInt(CardNumber.charAt(count));
if (!((count & 1) ^ oddoeven)) {
digit *= 2;
if (digit > 9)
digit -= 9;
}
sum += digit;
}
if (sum % 10 == 0)
return true;
else
return false;
}

/*************************************************************************\
ArrayObject makeArray(int size)
return the array object in the size specified.
\*************************************************************************/
function makeArray(size) {
this.size = size;
return this;
}

/*************************************************************************\
CardType setCardNumber(cardnumber)
return the CardType object.
\*************************************************************************/
function setCardNumber(cardnumber) {
this.cardnumber = cardnumber;
return this;
}

/*************************************************************************\
CardType setCardType(cardtype)
return the CardType object.
\*************************************************************************/
function setCardType(cardtype) {
this.cardtype = cardtype;
return this;
}

/*************************************************************************\
CardType setExpiryDate(year, month)
return the CardType object.
\*************************************************************************/
function setExpiryDate(year, month) {
this.year = year;
this.month = month;
return this;
}

/*************************************************************************\
CardType setLen(len)
return the CardType object.
\*************************************************************************/
function setLen(len) {
// Create the len array.
if (len.length == 0 || len == null)
len = "13,14,15,16,19";

var tmplen = len;
n = 1;
while (tmplen.indexOf(",") != -1) {
tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
n++;
}
this.len = new makeArray(n);
n = 0;
while (len.indexOf(",") != -1) {
var tmpstr = len.substring(0, len.indexOf(","));
this.len[n] = tmpstr;
len = len.substring(len.indexOf(",") + 1, len.length);
n++;
}
this.len[n] = len;
return this;
}

/*************************************************************************\
CardType setRules()
return the CardType object.
\*************************************************************************/
function setRules(rules) {
// Create the rules array.
if (rules.length == 0 || rules == null)
rules = "0,1,2,3,4,5,6,7,8,9";
  
var tmprules = rules;
n = 1;
while (tmprules.indexOf(",") != -1) {
tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
n++;
}
this.rules = new makeArray(n);
n = 0;
while (rules.indexOf(",") != -1) {
var tmpstr = rules.substring(0, rules.indexOf(","));
this.rules[n] = tmpstr;
rules = rules.substring(rules.indexOf(",") + 1, rules.length);
n++;
}
this.rules[n] = rules;
return this;
}
	
// --></SCRIPT><? echo "\n\n";

echo '<DIV STYLE="font-family:Arial; font-size:20pt; padding-top:8px; padding-bottom:6px;">'.gettrans('Shopping Cart').'</DIV>'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";

	bgcolor('reset');
	if(count($_SESSION['cart']) > 0){

	echo "\t".'<TR>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Details</TD>';
		echo '<TD STYLE="padding:4px; padding-left:0px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Options/Add Ons</TD>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Totals</TD>';
		echo '<TD STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333;">&nbsp;</TD>';
		echo '</TR>'."\n";

	$TOTALCOST = 0;
	foreach($_SESSION['cart'] as $id => $row){
		if($row['type'] == "r"){

		if(!isset($row['amount']) || $row['amount'] == "" || $row['amount'] == 0){
			$getamount = getamount($row); $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
			$row['amount'] = $getamount['amount'];
			}

		$rowbg = bgcolor('');
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="4" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'">';
			echo '<TD ALIGN="left" COLSPAN="2" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
				echo gettrans('Date').': '.gettrans(date("l",$row['date'])).', '.date("n/j/Y",$row['date']).'<BR>';
				echo gettrans('Guests').': '.$row['numguests'].'<BR>';
				echo '</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; padding-right:4px;">';
				//echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
			echo '</TR>'."\n";

		} else {

		$date = mktime(0,0,0,$row['date_m'],$row['date_d'],$row['date_y']);
		if($row['onlydateavl'] > 0): $onyldateavl = gettrans('Yes'); else: $onyldateavl = gettrans('No'); endif;

		if(!isset($row['amount']) || $row['amount'] == "" || $row['amount'] == 0){
			$getamount = getamount($row); $_SESSION['cart'][$id]['base'] = $getamount['base']; $_SESSION['cart'][$id]['amount'] = $getamount['amount'];
			$row['amount'] = $getamount['amount'];
			}

		$rowbg = bgcolor('');
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="4" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'">';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
				echo gettrans('Date').': '.gettrans(date("l",$date)).', '.date("n/j/Y",$date).'<BR>';
				if(isset($row['altdates']) && trim($row['altdates']) != ""): echo gettrans('Alternate dates').': '.trim($row['altdates']).'<BR>'; endif;
				echo gettrans('Guests').': '.$row['numguests'].'<BR>';
				echo gettrans('Base price').': $'.number_format($row['base'],2,'.','').'<BR>';
				echo gettrans('Pick Up').': '.$row['dep_loc'].'<BR>'.gettrans('Drop Off').': '.$row['arr_loc'].'<BR>';
				if(isset($row['preftime'])): echo gettrans('Preferred time of day').': '.$row['preftime'].'<BR>'; endif;
				echo '</TD>';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
			//Lodging adjustments
			$rm = 1;
			while($rm <= @$row['numrooms'] || @$row['rmamt'.$rm] != ""){
				if($row['room'.$rm] == 1 && $row['rmamt'.$rm] > 0){
					echo '<DIV>Room '.$rm.' single occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
					} elseif($row['room'.$rm] == 3 && $row['rmamt'.$rm] > 0){
					echo '<DIV>Room '.$rm.' triple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
					} elseif($row['room'.$rm] == 4 && $row['rmamt'.$rm] > 0){
					echo '<DIV>Room '.$rm.' quadruple occupancy:</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">- <B>$'.number_format($row['rmamt'.$rm],2,'.','').'</B></DIV>';
					}
				$rm++;
				}
			//Lodging extensions
			if(isset($row['extlodgeid']) && is_array($row['extlodgeid'])){
			foreach($row['extlodgeid'] as $key => $val){ if(isset($row['addextl'.$val]) && $row['addextl'.$val] == "y" && $row['extlodgenights'][$key] > 0){
				$elt = ($row['extlodgeperguest'][$key]*$row['numguests']);
				if($row['numguests'] == 1){
					$elt = ($elt + $row['extlodgesingle'][$key]);
					} elseif($row['numrooms'] == 1 && $row['numguests'] == 3){
					$elt = ($elt - $row['extlodgetriple'][$key]);
					} elseif($row['numrooms'] == 1 && $row['numguests'] == 4){
					$elt = ($elt - $row['extlodgequad'][$key]);
					} elseif($row['numrooms'] > 1){
						for($rm=1; $rm<=$row['numrooms']; $rm++){
							if(@$row['room'.$rm] == 1){
								$elt = ($elt + $row['extlodgesingle'][$key]);
								} elseif(@$row['room'.$rm] == 3){
								$elt = ($elt - $row['extlodgetriple'][$key]);
								} elseif(@$row['room'.$rm] == 4){
								$elt = ($elt - $row['extlodgequad'][$key]);
								}
							}
					}
				echo '<DIV>Extended stay at '.$row['extlodgename'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.number_format($elt,2,'.','').' x '.$row['extlodgenights'][$key].' '.gettrans('night(s)').' = <B>$'.number_format( ($elt*$row['extlodgenights'][$key]),2,'.','').'</B></DIV>';
				}}
				}
			//Activities
			if(isset($row['optionid']) && is_array($row['optionid'])){
			foreach($row['optionid'] as $key => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y" && $row['optionpax'][$key] > 0){
				echo '<DIV>'.$row['optionname'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.$row['optionprice'][$key].' x '.$row['optionpax'][$key].' '.gettrans('guest(s)').' = <B>$'.number_format( ($row['optionprice'][$key]*$row['optionpax'][$key]),2,'.','').'</B></DIV>';
				}}
				}
				echo '</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; padding-right:4px;">';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
			echo '</TR>'."\n";

		} //End $row type if statement

		$TOTALCOST = ($TOTALCOST+$row['amount']);
		} //End ForEach Loop

		echo "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="3" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD><TD>&nbsp;</TD></TR>'."\n";

		echo "\t".'<TR><TD ALIGN="center" COLSPAN="4"><FONT FACE="Arial" SIZE="2"><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><I>-'.gettrans('or').'-</I><BR>'.gettrans('Submit payment below to order listed reservations.').'</FONT></TD></TR>'."\n";

		echo '</TABLE><BR>'."\n\n";


if(isset($puberrormsg) && count($puberrormsg) > 0){
	$fillform = $_POST;
}

if(isset($_SESSION['bb_member']) && count($_SESSION['bb_member']) > 0){
	$fillform = $_SESSION['bb_member'];
	$fillform['name'] = $_SESSION['bb_member']['firstname'].' '.$_SESSION['bb_member']['lastname'];
	}

//echo '<!-- ';
//	echo '<PRE STYLE="text-align:left;">'; print_r($_SESSION['agent']); echo '</PRE>';
//	echo ' -->'."\n\n";

echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'"';
	if(!isset($_SESSION['agent']['id']) || $_SESSION['agent']['id'] == ""){ echo ' onSubmit="javascript:return checkexp();"'; }
	echo '>'."\n";
echo '<INPUT TYPE="hidden" NAME="form" ID="form" VALUE="process">'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" STYLE="width:80%;">'."\n";

	echo "\t".'<TR><TD COLSPAN="2" STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:center;">'.gettrans('Contact').' / '.gettrans('Payment Information').'</TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Name').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('As it appears on your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="'.getval('name').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Home/Business Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="'.getval('phone_homebus').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="'.getval('phone_cell').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Country').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cell_country" VALUE="'.getval('cell_country').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" ID="email" VALUE="'.getval('email').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Re-type Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email2" ID="email2" VALUE="'.getval('email2').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	if(isset($_SESSION['agent']['id']) && $_SESSION['agent']['id'] != ""){
		echo "\t".'<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><INPUT TYPE="checkbox" NAME="altpaymethod" ID="altpaymethod" VALUE="Alternate"></TD><TD><FONT FACE="Arial" SIZE="2"><LABEL FOR="altpaymethod">'.gettrans('I will submit payment in the form of wire transfer or alternate (skip credit card fields below).').'</LABEL></FONT></TD></TR>'."\n";
		}

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Credit card number').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('We accept Visa, MasterCard and American Express.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" ID="cc_num" VALUE="'.getval('cc_num').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Expiration date').'</TD><TD>';
		echo '<SELECT NAME="cc_expdate_month" ID="cc_expdate_month">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if( (isset($_SESSION['reginfo']['cc_expdate']) && date("n",$_SESSION['reginfo']['cc_expdate']) == $i) || getval('cc_expdate_month') == $i ){ echo " SELECTED"; }
				echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
				if( (isset($_SESSION['reginfo']['cc_expdate']) && date("Y",$_SESSION['reginfo']['cc_expdate']) == $i) || getval('cc_expdate_year') == date("y",mktime("0","0","0","1","1",$i)) ){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Security code').'</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'" STYLE="width:50px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Zip/Postal code').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Please provide the zip or postal code associated with your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="'.getval('cc_zip').'" STYLE="width:80px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.wordwrap(gettrans('If the card holder will not be there, provide the name of one of the guests'),40,'<BR>').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="alt_name" VALUE="'.getval('alt_name').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Comments').'</TD><TD ALIGN="left"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:300px; height:80px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="center" COLSPAN="2"><INPUT TYPE="submit" NAME="submit" VALUE="'.gettrans('Reserve').'" STYLE="width:180px;"></FORM></TD></TR>'."\n";

	} else {
		echo "\t\t".'<TR><TD ALIGN="center" COLSPAN="5"><FONT FACE="Arial" SIZE="2"><BR>'.gettrans('There are currently no reservations in your shopping cart.').'<BR><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><BR></FONT></TD></TR>'."\n";
	}

echo '</TABLE><BR><BR>'."\n\n";

echo '<DIV STYLE="width:80%; font-family:Arial; font-size:10pt; text-align:left; color:#FF0000;">Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.<BR>We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.';
	//echo '<BR>We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV><BR><BR>';

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";


} //End Tour/Shopping Cart If Statement

echo '<DIV STYLE="font-family:Arial; font-size:8pt;">'.gettrans('Bundu Bashers Tours will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";

include('footer.php');

?>