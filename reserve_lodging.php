<? // This script written by Dominick Bernal - www.bernalwebservices.com

/*if($_SERVER["HTTP_HOST"] != "www.bundubashers.com" || !isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
	header("Location: https://www.bundubashers.com/reserve_lodging.php?".$_SERVER["QUERY_STRING"]);
	exit;
	}*/

@date_default_timezone_set('America/Denver');
$time = time(); //mktime();

@ini_set("session.gc_maxlifetime", "10800");
if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['http_referer'])) {
    $_SESSION['http_referer'] = '';
}
if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER["HTTP_HOST"]) {
    $_SESSION['http_referer'] = $_SERVER['HTTP_REFERER'];
}

$db = mysql_connect('localhost', 'root', '');
mysql_select_db('bund');
@mysql_query("SET NAMES utf8");
@mysql_query('SET time_zone = "'.date("P").'"');

$fullwidth = "780";
$tablewidth = "720";
$leftcol = "300";
$rightcol = ($tablewidth - $leftcol);

function printmsgs($successmsg, $errormsg)
{
    //if(isset($successmsg) && count($successmsg) > 0 || isset($errormsg) && count($errormsg) > 0): echo '<IMG SRC="spacer.gif" BORDER="0"><BR>'."\n"; endif;
    if (isset($successmsg) && count($successmsg) > 0) {
        echo '<TABLE BORDER="0" BGCOLOR="#000C7F" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Update Successful</FONT></TD></TR>
	<TR BGCOLOR="#D9DBEC"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>' . implode("<BR>", $successmsg) . '</I><BR></FONT></TD></TR>
	</TABLE>';
        if (isset($errormsg) && count($errormsg) > 0): echo '<BR>'; endif;
        echo "\n\n";
    }
    if (isset($errormsg) && count($errormsg) > 0) {
        echo '<TABLE BORDER="0" BGCOLOR="#B00000" CELLSPACING="1" CELLPADDING="2" WIDTH="600">
	<TR BGCOLOR="#FFFFFF"><TD ALIGN="center"><FONT FACE="Arial" SIZE="3">Error:</FONT></TD></TR>
	<TR BGCOLOR="#F3D9D9"><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><I>' . implode("<BR>", $errormsg) . '</I></FONT></TD></TR>
	</TABLE>' . "\n\n";
    }
}

function bgcolor($i)
{
    if (!isset($GLOBALS['bgcolor']) || $i == "reset"): $GLOBALS['bgcolor'] = "DDDDDD"; endif;
    if ($i != "reset" && $i != ""): $GLOBALS['bgcolor'] = $i; endif;
    if ($GLOBALS['bgcolor'] == "FFFFFF"): $GLOBALS['bgcolor'] = "DDDDDD";
    else: $GLOBALS['bgcolor'] = "FFFFFF"; endif;
    return $GLOBALS['bgcolor'];
}

$successmsg = array();
$errormsg = array();
$adminsuccessmsg = array();
$adminerrormsg = array();

if (isset($_REQUEST['start_year']) && $_REQUEST['start_year'] != ""): $syear = $_REQUEST['start_year'];
else: $syear = date("Y", $time); endif;

//GET LODGING TYPES
$lodging = array();
$query = 'SELECT * FROM `lodging` WHERE `type` = "y" ORDER BY `name` ASC';
$result = @mysql_query($query);
$num_results = mysql_num_rows($result);
for ($i = 0; $i < $num_results; $i++) {
    $row = mysql_fetch_assoc($result);
    $lodging[$row['id']] = $row;
}

//GET AVAILABLE DATES
$query = 'SELECT MIN(`startdate`) as `earliest`, MAX(`enddate`) as `latest`, MIN(`min_nights`) as `min_nights`, MAX(`max_nights`) as `max_nights` FROM `lodging_pricing`';
$query .= ' WHERE `enddate` >= "' . strtotime('today') . '"';
if (isset($_REQUEST['type']) && $_REQUEST['type'] != ""): $query .= ' AND `lodgeid` = "' . $_REQUEST['type'] . '"'; endif;
$result = mysql_query($query);
$availdates = mysql_fetch_assoc($result);

//echo mysql_error();


echo '<HTML>

<HEAD>
	<TITLE>Yellowstone Lodging Reservations</TITLE>
	<style><!--
	.res1 { font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000; }
	--></style>
</HEAD>

<BODY BGCOLOR="#E1E1E1" TEXT="#000000" TOPMARGIN="0">

<CENTER>' . "\n\n";


//echo '<PRE>'; print_r($availdates); echo '</PRE>';


echo '<TABLE BORDER="0" WIDTH="' . $fullwidth . '" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF">
<TR><TD ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>
<TR><TD ALIGN="center">
	<TABLE BORDER="0" WIDTH="' . $tablewidth . '" CELLPADDING="0" CELLSPACING="2"><TR><TD ALIGN="center" STYLE="font-family:Arial; font-size:40pt; color:#FFA500;">Yellowstone Lodging</TD></TR></TABLE>
	<HR SIZE="1" WIDTH="' . $tablewidth . '">' . "\n";

//VALIDATE INPUT
if (isset($_POST['utaction']) && $_POST['utaction'] == "process") {
    if (!isset($_REQUEST['name']) || trim($_REQUEST['name']) == "" || !isset($_REQUEST['email']) || trim($_REQUEST['email']) == "" || !isset($_REQUEST['cc_num']) || trim($_REQUEST['cc_num']) == "" || !isset($_REQUEST['cc_scode']) || trim($_REQUEST['cc_scode']) == "") {
        @array_push($errormsg, 'One or more required fields were not filled in.  Please fill in all required fields.');
        $_POST['utaction'] = "validateres";
        $_REQUEST['utaction'] = "validateres";
        $step2 = "y";
    }
}

//VALIDATE DATES
if (isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "validateres") {
    $start = mktime(0, 0, 0, $_REQUEST['start_month'], $_REQUEST['start_day'], $_REQUEST['start_year']);
    if (!is_numeric($_REQUEST['nights'])): $_REQUEST['nights'] = $availdates['min_nights'];
    else: $_REQUEST['nights'] = number_format($_REQUEST['nights'], 0, '', ''); endif;
    $end = strtotime("+" . $_REQUEST['nights'] . " days", $start);
    //echo $start.' : '.$seasons[0].'<BR>'.$end.' : '.end($seasons);
    $dates = array();
    for ($i = $start; $i < $end; $i = strtotime("+1 day", $i)) {
        $dates['d' . $i] = 0; //date("n/j/Y",$i);
    }
    //echo '<PRE STYLE="text-align:left;">'; print_r($dates); echo '</PRE>';

    if (!isset($availdates['earliest']) || $availdates['earliest'] == 0) {
        array_push($errormsg, 'No dates are currently available for this type of lodging.  Please contact us for details.');
    } elseif ($start < $availdates['earliest'] || $start > strtotime("+1 day", $availdates['latest']) || $end < $availdates['earliest'] || $end > strtotime("+1 day", $availdates['latest'])) {
        array_push($errormsg, 'One or more dates chosen are not available.<BR>Please choose dates between ' . date("M. j, Y", $availdates['earliest']) . ' and ' . date("M. j, Y", $availdates['latest']) . '.  Thank you.');
    } else {
        $step2 = "y";
    }

    $query = 'SELECT `id`, `name`, (IFNULL((SELECT SUM(`available`) FROM `lodging` WHERE `subunit` = ' . $_REQUEST['type'] . '),1)*`available`) AS `totalavail`, IFNULL((SELECT SUM(`available`) FROM `lodging` WHERE `subunit` = ' . $_REQUEST['type'] . '),1) AS `single` FROM `lodging` WHERE `id` = ' . $_REQUEST['type'] . ' LIMIT 1';
    $result = @mysql_query($query);
    $lodge = mysql_fetch_assoc($result);
    //echo '<PRE STYLE="text-align:left;">'; print_r($lodge); echo '</PRE>';

    $reservations = array();
    $query = 'SELECT reservations_assoc.`reservation`, reservations_assoc.`id` AS `assoc`, reservations_assoc.`date`, FROM_UNIXTIME(reservations_assoc.`date`+3600) AS `date2`, (reservations_assoc.`adults` + reservations_assoc.`seniors` + reservations_assoc.`children`) AS `numguests`, IF(reservations_assoc.`name`!="",reservations_assoc.`name`,reservations.`name`) AS `name`, reservations_assoc.`lodgeid`, lodging.`subunit`, reservations_assoc.`nights`, IFNULL((SELECT SUM(lodging.`available`) FROM `lodging` WHERE lodging.`subunit` = reservations_assoc.`lodgeid`),1) AS `add`';
    $query .= ' FROM `reservations`, `reservations_assoc`, `lodging`';
    $query .= ' WHERE reservations.`id` = reservations_assoc.`reservation`';
    $query .= ' AND reservations_assoc.`type` = "l" AND reservations_assoc.`lodgeid` > 0';
    $query .= ' AND reservations.`canceled` = 0 AND reservations_assoc.`canceled` = 0';
    $query .= ' AND (';
    $query .= '(reservations_assoc.`lodgeid` = lodging.`id` AND lodging.`id` = "' . $_REQUEST['type'] . '")';
    $query .= ' OR ';
    $query .= '(reservations_assoc.`lodgeid` = lodging.`id` AND lodging.`subunit` = "' . $_REQUEST['type'] . '")';
    $query .= ' OR ';
    $query .= '(reservations_assoc.`lodgeid` = lodging.`subunit` AND lodging.`id` = "' . $_REQUEST['type'] . '")';
    $query .= ')';
    //$query .= ' AND (lodging.`id` = "'.$_REQUEST['type'].'" OR lodging.`subunit` = "'.$_REQUEST['type'].'")';
    $query .= ' AND ((reservations_assoc.`date`+((reservations_assoc.`nights`-1)*86400)) >= ' . $start . ' AND reservations_assoc.`date` < ' . $end . ')';
    $query .= ' ORDER BY `date` ASC, reservations_assoc.`reservation` ASC';
    //echo $query.'<BR>';
    $result = @mysql_query($query);
    $num_results = @mysql_num_rows($result);
    for ($i = 0; $i < $num_results; $i++) {
        $row = mysql_fetch_assoc($result);
        array_push($reservations, $row);
    }
    //echo '<PRE STYLE="text-align:left;">'; print_r($reservations); echo '</PRE>';
    //array_push($errormsg,'Checking between '.date("n/j/Y g:ia",$start).' and '.date("n/j/Y g:ia",$end));
    //$step2 = "n";

    //Add up points
    foreach ($reservations as $row) {
        for ($i = 0; $i < $row['nights']; $i++) {
            $d = strtotime("+" . $i . " day", $row['date']);
            //echo $d.' '.date("n/j/Y",$d);
            if (isset($dates['d' . $d])) {
                $dates['d' . $d] += $row['add'];
                //echo ' +'.$row['add'];
            }
            //echo '<BR>';
        }
    }
    //echo '<PRE STYLE="text-align:left;">'; print_r($dates); echo '</PRE>';

    //Determine availability
    $full = array();
    foreach ($dates as $d => $points) {
        if (($lodge['totalavail'] - $points) < $lodge['single']) {
            array_push($full, date("M. j, Y", str_replace('d', '', $d)));
        }
    }
    //echo '<PRE STYLE="text-align:left;">'; print_r($full); echo '</PRE>';
    if (count($full) > 0) {
        array_push($errormsg, 'All available "' . $lodge['name'] . '" have been fully booked on the following dates:<BR>' . implode(", ", $full));
        $step2 = "n";
    }

}

//FIND PRICING
if (isset($step2) && $step2 == "y") {
    $query = 'SELECT * FROM `lodging_pricing` WHERE `lodgeid` = "' . $_REQUEST['type'] . '" AND `startdate` <= "' . $start . '" AND `enddate` >= "' . strtotime("+" . ($_REQUEST['nights'] - 1) . " day", $start) . '" AND `min_nights` <= "' . $_REQUEST['nights'] . '" AND `max_nights` >= "' . $_REQUEST['nights'] . '" ORDER BY `price` DESC LIMIT 1';
    $result = mysql_query($query);
    $num_results = mysql_num_rows($result);
    if ($num_results == 1) {
        $pricing = mysql_fetch_assoc($result);
        if ($pricing['calc'] == "*") {
            $total = ($_REQUEST['nights'] * $pricing['price']);
        } else {
            $total = $pricing['price'];
        }
    } else {
        $pricing = array();
        for ($i = $start; $i < $end; $i = strtotime("+1 day", $i)) {
            $pricing['p' . $i] = 0.00; //date("n/j/Y",$i);
        }
        $query = 'SELECT * FROM `lodging_pricing` WHERE `lodgeid` = "' . $_REQUEST['type'] . '" AND `min_nights` = "1" AND `calc` = "*"';
        $query .= ' AND (';
        $query .= ' (`startdate` <= "' . $start . '" AND `enddate` >= "' . $start . '")';
        $query .= ' OR';
        $query .= ' (`startdate` <= "' . strtotime("+" . ($_REQUEST['nights'] - 1) . " day", $start) . '" AND `enddate` >= "' . strtotime("+" . ($_REQUEST['nights'] - 1) . " day", $start) . '")';
        $query .= ' )';
        $query .= ' ORDER BY `startdate` ASC';
        //echo date("n/j/y g:ia",$start).' '.date("n/j/y g:ia",strtotime("+".($_REQUEST['nights']-1)." day",$start)).'<BR><BR>';
        //echo $query;
        $result = mysql_query($query);
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                //echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
                foreach ($pricing as $key => $p) {
                    $d = str_replace('p', '', $key);
                    if ($d >= $row['startdate'] && $d <= $row['enddate']) {
                        $pricing[$key] = $row['price'];
                    }
                }
            }
            foreach ($pricing as $key => $p) {
                if ($p == 0) {
                    array_push($errormsg, 'One or more dates chosen are not available.  Please contact us for details.');
                    $step2 = "n";
                    break;
                }
            }
            $total = array_sum($pricing);
        } else {
            array_push($errormsg, $availdates['min_nights'] . ' night minimum.  ' . $availdates['max_nights'] . ' night maximum.');
            $step2 = "n";
        }
        //echo '<PRE STYLE="text-align:left;">'; print_r($pricing); echo '</PRE>';
    }
}

//PRINT SUCCESS/ERROR MESSAGES
printmsgs($successmsg, $errormsg);

//PROCESS
if (isset($_POST['utaction']) && $_POST['utaction'] == "process") {
    $als_checkin = mktime(0, 0, 0, $_POST['start_month'], $_POST['start_day'], $_POST['start_year']);
    $_POST['total'] = number_format($_POST['total'], 2, '.', '');
    $_POST['cc_num'] = trim($_POST['cc_num']);
    $_POST['cc_num'] = str_replace(' ', '', $_POST['cc_num']);
    $_POST['cc_num'] = str_replace('-', '', $_POST['cc_num']);
    $_POST['cc_expdate'] = $_POST['cc_expdate_month'] . '/' . $_POST['cc_expdate_year'];

    $query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`pay_method`,`comments`,`http_referer`,`booker`,`date_booked`)';
    $query .= ' VALUES("' . $_POST['name'] . '","' . $_POST['phone_homebus'] . '","' . $_POST['phone_cell'] . '","' . $_POST['email'] . '","' . $_POST['total'] . '","' . $_POST['name'] . '","' . $_POST['cc_num'] . '","' . $_POST['cc_expdate'] . '","' . $_POST['cc_scode'] . '","C/C","' . $_POST['comments'] . '","' . $_SESSION['http_referer'] . '","Website","' . $time . '")';
    @mysql_query($query);
    $confnum = mysql_insert_id();
    $thiserror = mysql_error();
    if ($thiserror == "") {

        echo '<FONT FACE="Arial" SIZE="3"><BR>Your order number: <B>' . $confnum . '</B><BR><BR>Thank you for your order.  If you placed it during regular business hours in the USA you can expect an email response from us within two hours.    If the order were placed outside regular business hours, we will be in touch when we open in the morning.<BR><BR>Please note that this order is not confirmed until you receive an email confirmation from us.  <SPAN STYLE="color:#cc0000;">Please do not make any arrangements that are dependent on this lodging until we send you a confirmation.</SPAN><BR><BR></FONT>' . "\n\n";
        @array_push($adminsuccessmsg, 'Saved new lodging reservation.<BR>Order #: <B>' . $confnum . '</B>');

        $query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`date`,`onlydateavl`,`lodgeid`,`nights`,`amount`)';
        $query .= ' VALUES("' . $confnum . '","' . $lodging[$_REQUEST['type']]['name'] . '","l","' . $_POST['name'] . '","' . $_POST['guests'] . '","' . $als_checkin . '",1,"' . $_POST['type'] . '","' . $_POST['nights'] . '","' . $_POST['total'] . '")';
        @mysql_query($query);
        $thiserror = mysql_error();
        if ($thiserror != "") {
            array_push($adminerrormsg, $thiserror);
        }

        $heading = 'New Lodging Reservation #' . $confnum;
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
        //$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
        $headers .= "From: " . $_POST['name'] . " <" . $_POST['email'] . ">\r\n";
        $message = "A customer has ordered a new lodging reservation.<BR><BR>\n\n" . $confnum;
        if (!mail('', $heading, $message, $headers)) {
            array_push($errormsg, 'Unable to send merchant notification.');
        }

    } else {
        echo '<FONT FACE="Arial" SIZE="3"><BR>The system encountered an error while processing your order.<BR>We apologize for the inconvenience.<BR>Please contact us to complete your reservation.<BR><B>Toll Free: (866) 646-1118</B><BR>Your order number: <B>' . $confnum . '</B><BR><BR></FONT>' . "\n\n";
        @array_push($adminerrormsg, $thiserror);
        //echo $thiserror."<BR>";
    }

    //echo '<FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">Yellowstone Lodging</A></B><BR><BR></FONT>'."\n\n";

    //PRINT FORMS
} elseif (isset($step2) && $step2 == "y") {

    //echo '<PRE>'; print_r($prices); echo '</PRE>';
    //$total = array_sum($prices);
    $total = number_format($total, 2, '.', '');

    echo '<BR><TABLE BORDER="0" WIDTH="550" CELLPADDING="2" CELLSPACING="1">' . "\n";
    echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="center" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Your Reservation:</B></FONT></TD></TR>' . "\n";

    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Room:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">' . $lodging[$_REQUEST['type']]['name'] . '</FONT></TD></TR>' . "\n";
    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Dates:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">' . date("M j, Y", $start);
    echo ' - ' . date("M j, Y", $end) . ' (' . $_REQUEST['nights'] . ' Night';
    if ($_REQUEST['nights'] > 1) {
        echo 's';
    }
    echo ')';
    echo '</FONT></TD></TR>' . "\n";
    echo '<TR><TD ALIGN="right"><FONT FACE="Arial" SIZE="3"><B>Guests:</B></FONT></TD><TD ALIGN="left"><FONT FACE="Arial" SIZE="3">' . $_REQUEST['guests'] . '</FONT></TD></TR>' . "\n";

    echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Subtotal:&nbsp;&nbsp;$' . $total . '&nbsp;&nbsp;</B></FONT></TD></TR>' . "\n";

    $tax = ($total * .10);
    echo '<TR BGCOLOR="#E1E1E1"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Tax:&nbsp;&nbsp;&nbsp;$' . number_format($tax, 2, '.', '') . '&nbsp;&nbsp;</B></FONT></TD></TR>' . "\n";

    $total = $total + $tax;
    $total = number_format($total, 2, '.', '');
    echo '<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="2"><FONT FACE="Arial" SIZE="2"><B>Total:&nbsp;&nbsp;$' . $total . '&nbsp;&nbsp;</B></FONT></TD></TR>' . "\n";

    echo '</TABLE>' . "\n\n";
    echo '<FORM NAME="resform" METHOD="post" ACTION="' . $_SERVER['PHP_SELF'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="type" VALUE="' . $_REQUEST['type'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_month" VALUE="' . $_REQUEST['start_month'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_day" VALUE="' . $_REQUEST['start_day'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_year" VALUE="' . $_REQUEST['start_year'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="nights" VALUE="' . $_REQUEST['nights'] . '">' . "\n";
    echo '<INPUT TYPE="submit" VALUE="&lt;&lt; Change">' . "\n";
    echo '</FORM>' . "\n\n";

    echo '<FORM NAME="resform" METHOD="post" ACTION="' . $_SERVER['PHP_SELF'] . '" onSubmit="javascript:return checkexp();">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="process">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="type" VALUE="' . $_REQUEST['type'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_month" VALUE="' . $_REQUEST['start_month'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_day" VALUE="' . $_REQUEST['start_day'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="start_year" VALUE="' . $_REQUEST['start_year'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="nights" VALUE="' . $_REQUEST['nights'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="guests" VALUE="' . $_REQUEST['guests'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="total" VALUE="' . $total . '">' . "\n";
    echo '	<TABLE BORDER="0" WIDTH="' . $tablewidth . '" CELLPADDING="0" CELLSPACING="2">' . "\n";
    //echo '	<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="center"><FONT FACE="Arial" SIZE="2"><B>Contact / Payment Information</B></FONT></TD></TR>'."\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Name</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="';
    if (isset($_REQUEST['name'])): echo $_REQUEST['name']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Home/Business Phone</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="';
    if (isset($_REQUEST['phone_homebus'])): echo $_REQUEST['phone_homebus']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Cell Phone</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="';
    if (isset($_REQUEST['phone_cell'])): echo $_REQUEST['phone_cell']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Email Address</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" ID="email" VALUE="';
    if (isset($_REQUEST['email'])): echo $_REQUEST['email']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Re-type Email Address</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="email2" ID="email2" VALUE="';
    if (isset($_REQUEST['email'])): echo $_REQUEST['email']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Credit card number</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" ID="cc_num" VALUE="';
    if (isset($_REQUEST['cc_num'])): echo $_REQUEST['cc_num']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Expiration date</B>&nbsp;</FONT></TD><TD><SELECT NAME="cc_expdate_month" ID="cc_expdate_month">';
    for ($i = 1; $i < 13; $i++) {
        echo '<OPTION VALUE="' . date("m", mktime("0", "0", "0", $i, "1", "2005")) . '"';
        if (isset($_REQUEST['cc_expdate_month']) && $_REQUEST['cc_expdate_month'] == $i): echo " SELECTED"; endif;
        echo '>' . $i . ' (' . date("M", mktime("0", "0", "0", $i, "1", "2005")) . ')</OPTION>';
    }
    echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year">';
    for ($i = date("Y", $time); $i < (date("Y", $time) + 11); $i++) {
        echo '<OPTION VALUE="' . date("y", mktime("0", "0", "0", "1", "1", $i)) . '"';
        if (isset($_REQUEST['cc_expdate_year']) && $_REQUEST['cc_expdate_year'] == date("y", mktime("0", "0", "0", "1", "1", $i))): echo " SELECTED"; endif;
        echo '>' . $i . '</OPTION>';
    }
    echo '</SELECT></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Security code</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" ID="cc_scode" VALUE="';
    if (isset($_REQUEST['cc_scode'])): echo $_REQUEST['cc_scode']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Zip/Postal code&nbsp;<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Please provide the zip or postal code&nbsp;<BR>associated with your credit card.</SPAN>&nbsp;</TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="" STYLE="width:80px;"></TD></TR>' . "\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Comments</B>&nbsp;</FONT></TD><TD ALIGN="left" WIDTH="' . $rightcol . '"><TEXTAREA NAME="comments" COLS="30" ROWS="3">';
    if (isset($_REQUEST['comments'])): echo $_REQUEST['comments']; endif;
    echo '</TEXTAREA></TD></TR>' . "\n";
    echo '	<TR><TD COLSPAN="2" ALIGN="center"><INPUT TYPE="submit" VALUE="Submit Reservation"></TD></TR>' . "\n";
    echo '	</TABLE>' . "\n";
    echo '</FORM>' . "\n";

} else {

    echo '<FORM NAME="resform" METHOD="post" ACTION="' . $_SERVER['PHP_SELF'] . '">' . "\n";
    echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="validateres">' . "\n";
    echo '	<TABLE BORDER="0" WIDTH="' . $tablewidth . '" CELLPADDING="0" CELLSPACING="2">' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Lodging Type</B>&nbsp;</FONT></TD><TD STYLE="font-family:Arial; font-size:14pt;">';
    if (isset($_REQUEST['type']) && $_REQUEST['type'] != "") {
        echo '<INPUT TYPE="hidden" NAME="type" VALUE="' . $_REQUEST['type'] . '">' . $lodging[$_REQUEST['type']]['name'];
    } else {
        echo '<SELECT NAME="type">';
        foreach ($lodging as $type) {
            echo '<OPTION VALUE="' . $type['id'] . '"';
            if (isset($_REQUEST['type']) && $_REQUEST['type'] == $type['id']): echo " SELECTED"; endif;
            echo '>' . $type['name'] . '</OPTION>';
        }
        echo '</SELECT>';
    }
    echo '</TD></TR>' . "\n";
    echo '	<TR><TD></TD><TD><FONT FACE="Arial" SIZE="1">';
    if (!isset($availdates['earliest']) || $availdates['earliest'] == 0) {
        echo 'No dates are currently available for this type of lodging.  Please contact us for details.</FONT>';
    } else {
        echo 'Lodging may be available from ' . date("M. j, Y", $availdates['earliest']) . ' through ' . date("M. j, Y", $availdates['latest']) . '.<BR>' . $availdates['min_nights'] . ' night minimum.  ' . $availdates['max_nights'] . ' night maximum.</FONT>';
    }
    echo '</TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Check In Date</B>&nbsp;</FONT></TD><TD><SELECT NAME="start_month">';

    if (!isset($availdates['earliest']) || $availdates['earliest'] == 0) {
        $availdates['earliest'] = time();
    }
    if (!isset($availdates['latest']) || $availdates['latest'] == 0 || $availdates['latest'] < $availdates['earliest']) {
        $availdates['latest'] = $availdates['earliest'];
    }

    if (date("Y", $availdates['earliest']) == date("Y", $availdates['latest'])) {
        $smonth = date("n", $availdates['earliest']);
        $emonth = date("n", $availdates['latest']);
    } else {
        $smonth = 1;
        $emonth = 12;
    }

    if (!isset($_REQUEST['start_month']) || isset($_REQUEST['start_month']) == "") {
        $_REQUEST['start_month'] = date("n", strtotime("+1 day"));
        $_REQUEST['start_day'] = date("j", strtotime("+1 day"));
        $_REQUEST['start_year'] = date("Y", strtotime("+1 day"));
    }

    for ($i = $smonth; $i <= $emonth; $i++) {
        echo '<OPTION VALUE="' . $i . '"';
        if ($_REQUEST['start_month'] == $i): echo " SELECTED"; endif;
        echo '>' . date("F", mktime("0", "0", "0", $i, "1", "2005")) . '</OPTION>';
    }
    echo '</SELECT> / <SELECT NAME="start_day">';
    for ($i = 1; $i < 32; $i++) {
        echo '<OPTION VALUE="' . $i . '"';
        if (isset($_REQUEST['start_day']) && $_REQUEST['start_day'] == $i): echo " SELECTED"; endif;
        echo '>' . $i . '</OPTION>';
    }
    echo '</SELECT> / <SELECT NAME="start_year">';
    for ($i = date("Y", $availdates['earliest']); $i <= date("Y", $availdates['latest']); $i++) {
        echo '<OPTION VALUE="' . date("Y", mktime(0, 0, 0, 1, 1, $i)) . '"';
        if (isset($_REQUEST['start_year']) && $_REQUEST['start_year'] == date("Y", mktime(0, 0, 0, 1, 1, $i))): echo " SELECTED"; endif;
        echo '>' . $i . '</OPTION>';
    }
    echo '</SELECT></TD></TR>' . "\n";


    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Number of Nights</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="5" NAME="nights" VALUE="';
    if (isset($_REQUEST['nights'])): echo $_REQUEST['nights'];
    else: echo $availdates['min_nights']; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD ALIGN="right" WIDTH="' . $leftcol . '"><FONT FACE="Arial" SIZE="2"><B>Number of Guests</B>&nbsp;</FONT></TD><TD><INPUT TYPE="text" SIZE="5" NAME="guests" VALUE="';
    if (isset($_REQUEST['guests'])): echo $_REQUEST['guests'];
    else: echo '2'; endif;
    echo '"></TD></TR>' . "\n";
    echo '	<TR><TD></TD><TD ALIGN="left"><INPUT TYPE="submit" VALUE="Continue &gt;&gt;"></TD></TR>' . "\n";
    echo '	</TABLE>' . "\n";
    echo '</FORM>' . "\n";

}


?>
    <SCRIPT><!--

        function openhelp() {
            open("alswestwardhelp.html", "helpwin", "width=450,height=150,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
        }

        //CC Validation Script Author: Simon Tneoh (tneohcb@pc.jaring.my)
        var Cards = new Array();
        Cards[0] = new CardType("MasterCard", "51,52,53,54,55", "16");
        var MasterCard = Cards[0];
        Cards[1] = new CardType("VisaCard", "4", "13,16");
        var VisaCard = Cards[1];
        Cards[2] = new CardType("AmExCard", "34,37", "15");
        var AmExCard = Cards[2];
        //Cards[3] = new CardType("DinersClubCard", "30,36,38", "14");
        //var DinersClubCard = Cards[3];
        //Cards[4] = new CardType("DiscoverCard", "6011", "16");
        //var DiscoverCard = Cards[4];
        //Cards[5] = new CardType("enRouteCard", "2014,2149", "15");
        //var enRouteCard = Cards[5];
        //Cards[6] = new CardType("JCBCard", "3088,3096,3112,3158,3337,3528", "16");
        //var JCBCard = Cards[6];
        var LuhnCheckSum = Cards[3] = new CardType();

        function checkexp() {
            var r = true;
            var msg = '';
            if (document.getElementById("cc_num").value == "") {
                msg += 'Please enter your credit card number.' + "\n";
                r = false;
            }
            if (document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == "") {
                msg += 'Please choose an expiration date for your credit card.' + "\n";
                r = false;
            }
            if (r) {
                var tmpyear;
                if (document.getElementById("cc_expdate_year").value > 96) {
                    tmpyear = "19" + document.getElementById("cc_expdate_year").value;
                } else if (document.getElementById("cc_expdate_year").value < 21) {
                    tmpyear = "20" + document.getElementById("cc_expdate_year").value;
                } else {
                    msg += 'The card expiration year is not valid.' + "\n";
                    r = false;
                }
                tmpmonth = document.getElementById("cc_expdate_month").value;
                if (!(new CardType()).isExpiryDate(tmpyear, tmpmonth)) {
                    msg += 'Your credit card has already expired.' + "\n";
                    r = false;
                }
                card = false;
                for (var n = 0; n < Cards.length; n++) {
                    if (Cards[n].checkCardNumber(document.getElementById("cc_num").value, tmpyear, tmpmonth)) {
                        card = true;
                        break;
                    }
                }
                if (!card) {
                    msg += 'Your credit card number does not appear to be valid.' + "\n";
                    r = false;
                }
            }
            if (document.getElementById("cc_zip").value == "") {
                msg += 'Please enter the zip or postal code associated with your credit card.' + "\n";
                r = false;
            }
            if (document.getElementById("email").value != document.getElementById("email2").value) {
                msg += 'Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.' + "\n";
                r = false;
            }
            if (msg != "") {
                alert(msg);
            }
            return r;
        }


        /*************************************************************************\
         Object CardType([String cardtype, String rules, String len, int year,
         int month])
         cardtype    : type of card, eg: MasterCard, Visa, etc.
         rules       : rules of the cardnumber, eg: "4", "6011", "34,37".
         len         : valid length of cardnumber, eg: "16,19", "13,16".
         year        : year of expiry date.
         month       : month of expiry date.
         eg:
         var VisaCard = new CardType("Visa", "4", "16");
         var AmExCard = new CardType("AmEx", "34,37", "15");
         \*************************************************************************/
        function CardType() {
            var n;
            var argv = CardType.arguments;
            var argc = CardType.arguments.length;

            this.objname = "object CardType";

            var tmpcardtype = (argc > 0) ? argv[0] : "CardObject";
            var tmprules = (argc > 1) ? argv[1] : "0,1,2,3,4,5,6,7,8,9";
            var tmplen = (argc > 2) ? argv[2] : "13,14,15,16,19";

            this.setCardNumber = setCardNumber;  // set CardNumber method.
            this.setCardType = setCardType;  // setCardType method.
            this.setLen = setLen;  // setLen method.
            this.setRules = setRules;  // setRules method.
            this.setExpiryDate = setExpiryDate;  // setExpiryDate method.

            this.setCardType(tmpcardtype);
            this.setLen(tmplen);
            this.setRules(tmprules);
            if (argc > 4)
                this.setExpiryDate(argv[3], argv[4]);

            this.checkCardNumber = checkCardNumber;  // checkCardNumber method.
            this.getExpiryDate = getExpiryDate;  // getExpiryDate method.
            this.getCardType = getCardType;  // getCardType method.
            this.isCardNumber = isCardNumber;  // isCardNumber method.
            this.isExpiryDate = isExpiryDate;  // isExpiryDate method.
            this.luhnCheck = luhnCheck;// luhnCheck method.
            return this;
        }

        /*************************************************************************\
         boolean checkCardNumber([String cardnumber, int year, int month])
         return true if cardnumber pass the luhncheck and the expiry date is
         valid, else return false.
         \*************************************************************************/
        function checkCardNumber() {
            var argv = checkCardNumber.arguments;
            var argc = checkCardNumber.arguments.length;
            var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
            var year = (argc > 1) ? argv[1] : this.year;
            var month = (argc > 2) ? argv[2] : this.month;

            this.setCardNumber(cardnumber);
            this.setExpiryDate(year, month);

            if (!this.isCardNumber())
                return false;
            if (!this.isExpiryDate())
                return false;

            return true;
        }

        /*************************************************************************\
         String getCardType()
         return the cardtype.
         \*************************************************************************/
        function getCardType() {
            return this.cardtype;
        }

        /*************************************************************************\
         String getExpiryDate()
         return the expiry date.
         \*************************************************************************/
        function getExpiryDate() {
            return this.month + "/" + this.year;
        }

        /*************************************************************************\
         boolean isCardNumber([String cardnumber])
         return true if cardnumber pass the luhncheck and the rules, else return
         false.
         \*************************************************************************/
        function isCardNumber() {
            var argv = isCardNumber.arguments;
            var argc = isCardNumber.arguments.length;
            var cardnumber = (argc > 0) ? argv[0] : this.cardnumber;
            if (!this.luhnCheck())
                return false;

            for (var n = 0; n < this.len.size; n++)
                if (cardnumber.toString().length == this.len[n]) {
                    for (var m = 0; m < this.rules.size; m++) {
                        var headdigit = cardnumber.substring(0, this.rules[m].toString().length);
                        if (headdigit == this.rules[m])
                            return true;
                    }
                    return false;
                }
            return false;
        }

        /*************************************************************************\
         boolean isExpiryDate([int year, int month])
         return true if the date is a valid expiry date,
         else return false.
         \*************************************************************************/
        function isExpiryDate() {
            var argv = isExpiryDate.arguments;
            var argc = isExpiryDate.arguments.length;

            year = argc > 0 ? argv[0] : this.year;
            month = argc > 1 ? argv[1] : this.month;

            if (!isNum(year + ""))
                return false;
            if (!isNum(month + ""))
                return false;
            today = new Date();
            expiry = new Date(year, month);
            if (today.getTime() > expiry.getTime())
                return false;
            else
                return true;
        }

        /*************************************************************************\
         boolean isNum(String argvalue)
         return true if argvalue contains only numeric characters,
         else return false.
         \*************************************************************************/
        function isNum(argvalue) {
            argvalue = argvalue.toString();

            if (argvalue.length == 0)
                return false;

            for (var n = 0; n < argvalue.length; n++)
                if (argvalue.substring(n, n + 1) < "0" || argvalue.substring(n, n + 1) > "9")
                    return false;

            return true;
        }

        /*************************************************************************\
         boolean luhnCheck([String CardNumber])
         return true if CardNumber pass the luhn check else return false.
         Reference: http://www.ling.nwu.edu/~sburke/pub/luhn_lib.pl
         \*************************************************************************/
        function luhnCheck() {
            var argv = luhnCheck.arguments;
            var argc = luhnCheck.arguments.length;

            var CardNumber = argc > 0 ? argv[0] : this.cardnumber;

            if (!isNum(CardNumber)) {
                return false;
            }

            var no_digit = CardNumber.length;
            var oddoeven = no_digit & 1;
            var sum = 0;

            for (var count = 0; count < no_digit; count++) {
                var digit = parseInt(CardNumber.charAt(count));
                if (!((count & 1) ^ oddoeven)) {
                    digit *= 2;
                    if (digit > 9)
                        digit -= 9;
                }
                sum += digit;
            }
            if (sum % 10 == 0)
                return true;
            else
                return false;
        }

        /*************************************************************************\
         ArrayObject makeArray(int size)
         return the array object in the size specified.
         \*************************************************************************/
        function makeArray(size) {
            this.size = size;
            return this;
        }

        /*************************************************************************\
         CardType setCardNumber(cardnumber)
         return the CardType object.
         \*************************************************************************/
        function setCardNumber(cardnumber) {
            this.cardnumber = cardnumber;
            return this;
        }

        /*************************************************************************\
         CardType setCardType(cardtype)
         return the CardType object.
         \*************************************************************************/
        function setCardType(cardtype) {
            this.cardtype = cardtype;
            return this;
        }

        /*************************************************************************\
         CardType setExpiryDate(year, month)
         return the CardType object.
         \*************************************************************************/
        function setExpiryDate(year, month) {
            this.year = year;
            this.month = month;
            return this;
        }

        /*************************************************************************\
         CardType setLen(len)
         return the CardType object.
         \*************************************************************************/
        function setLen(len) {
// Create the len array.
            if (len.length == 0 || len == null)
                len = "13,14,15,16,19";

            var tmplen = len;
            n = 1;
            while (tmplen.indexOf(",") != -1) {
                tmplen = tmplen.substring(tmplen.indexOf(",") + 1, tmplen.length);
                n++;
            }
            this.len = new makeArray(n);
            n = 0;
            while (len.indexOf(",") != -1) {
                var tmpstr = len.substring(0, len.indexOf(","));
                this.len[n] = tmpstr;
                len = len.substring(len.indexOf(",") + 1, len.length);
                n++;
            }
            this.len[n] = len;
            return this;
        }

        /*************************************************************************\
         CardType setRules()
         return the CardType object.
         \*************************************************************************/
        function setRules(rules) {
// Create the rules array.
            if (rules.length == 0 || rules == null)
                rules = "0,1,2,3,4,5,6,7,8,9";

            var tmprules = rules;
            n = 1;
            while (tmprules.indexOf(",") != -1) {
                tmprules = tmprules.substring(tmprules.indexOf(",") + 1, tmprules.length);
                n++;
            }
            this.rules = new makeArray(n);
            n = 0;
            while (rules.indexOf(",") != -1) {
                var tmpstr = rules.substring(0, rules.indexOf(","));
                this.rules[n] = tmpstr;
                rules = rules.substring(rules.indexOf(",") + 1, rules.length);
                n++;
            }
            this.rules[n] = rules;
            return this;
        }

        // --></SCRIPT><? echo "\n\n";


echo '<FONT FACE="Arial" SIZE="3">Need <A HREF="null" onClick="openhelp(); return false">help</A>?<BR>' . "\n";

echo '	</TD></TR>' . "\n\n";

echo '<TR><TD ALIGN="center"><HR SIZE="1" WIDTH="' . $tablewidth . '">
<FONT FACE="Arial" SIZE="2"><B>Return to:&nbsp;&nbsp;<A HREF="http://www.yellowstonelodging.biz">Yellowstone Lodging</A></B><BR></FONT>
<FONT FACE="Arial" SIZE="1"><I>West Yellowstone Lodging will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.</I></FONT><BR><BR>
</TD></TR>' . "\n";

echo '</TABLE>

</FORM>


</CENTER>

</BODY>

</HTML>' . "\n\n";


if (isset($adminsuccessmsg) && count($adminsuccessmsg) > 0 || isset($adminerrormsg) && count($adminerrormsg) > 0) {
    if (isset($adminsuccessmsg) && count($adminsuccessmsg) > 0) {
        $logentry = implode("\n", $adminsuccessmsg);
        $logentry = addslashes($logentry);
        @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("' . $time . '","Web User","' . $pageid . '","' . $logentry . '")');
    }
    if (isset($adminerrormsg) && count($adminerrormsg) > 0) {
        $logentry = implode("\n", $adminerrormsg);
        $logentry = addslashes($logentry);
        @mysql_query('insert into `logs`(`time`,`user`,`page`,`log`) values("' . $time . '","Web User","' . $pageid . '","Error: ' . $logentry . '")');
    }
}

?>