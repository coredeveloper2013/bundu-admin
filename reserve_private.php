<?  // Developed by Dominick Bernal - www.bernalwebservices.com

include_once('header_reserve.php');

if(!isset($_SESSION['continue'])){ $_SESSION['continue'] = 'http://www.bundubashers.com'; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_PATH) != parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH)){
	$_SESSION['continue'] = $_SERVER['HTTP_REFERER'];
	}

$fillform = array();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();


echo '<CENTER>';

if($_REQUEST['form'] == "process"){  // !PROCESS RESERVATION **********************************************************

//echo '</CENTER>';
//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['cart']); echo '</PRE>';

	foreach($_POST as $key => $val){
		if(!is_array($val)){
			$_POST[$key] = strip_tags($_POST[$key]);
			$_POST[$key] = trim($_POST[$key]);
			//$_POST[$key] = str_replace('"','\"',$_POST[$key]);
			}
		}

	//RECORD RESERVATION
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];
	if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $_POST['pay_method'] = 'Alternate'; else: $_POST['pay_method'] = 'C/C'; endif;

	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_zip`,`pay_method`,`alt_name`,`comments`,`agent`,`http_referer`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['cell_country'].'","'.$_POST['email'].'","'.$_POST['amount'].'","'.$_POST['name'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['cc_zip'].'","'.$_POST['pay_method'].'","'.$_POST['alt_name'].'","'.$_POST['comments'].'","'.$agent.'","'.$_SESSION['http_referer'].'","Website","'.$time.'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror != ""){

		array_push($errormsg,$thiserror);

	} else {

	//ADD SUBRESERVATION
		$confnum = mysql_insert_id();
		array_push($successmsg,'Saved new reservation '.$confnum.', private tour for '.$_POST['name'].'.');

		foreach($row as $key => $val){
			if(!is_array($val)){
				$row[$key] = str_replace('"','\"',$row[$key]);
				}
			}
		$fillform = $_POST;

		$fillform['date'] = mktime(0,0,0,getval('date_m'),getval('date_d'),getval('date_y'));
		$fillform['pretitle'] = '[Bundu Private Tour '.getval('tourid').'] ';
		$fillform['vendor'] = 'IFNULL((SELECT tours_prv.`vendor` FROM `tours_prv` WHERE tours_prv.`id` = "'.getval('tourid').'" LIMIT 1),0)';
		if(getval('pretitle') != ""){ $fillform['title'] = getval('pretitle').' '.getval('title'); }

		//Insert into assoc table
		$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`preftime`,`onlydateavl`,`altdates`,`amount`,`tourid`,`vendor`)';
			$query .= ' VALUES("'.$confnum.'","'.getval('title').'","t","'.getval('name').'","'.getval('numguests').'",0,0,'.getval('date').',"'.getval('preftime').'","'.getval('onlydateavl').'","'.getval('altdates').'","'.getval('amount').'","1",'.getval('vendor').')';
			@mysql_query($query);
		$thiserror = mysql_error();
		if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

		//Add guests
		if(isset($_POST['guests_name']) && count($_POST['guests_name']) > 0){
		foreach($_POST['guests_name'] as $guestid => $guest){ if($guest != ""){
			if(!isset($_POST['guests_weight'][$guestid])){ $_POST['guests_weight'][$guestid] = ''; }
			if(!isset($_POST['guests_lunch'][$guestid])){ $_POST['guests_lunch'][$guestid] = ''; }
			$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$associd.'","'.$_POST['guests_name'][$guestid].'","'.$_POST['guests_weight'][$guestid].'","'.$_POST['guests_lunch'][$guestid].'")';
			@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror != ""){ array_push($errormsg,$thiserror); }
			}} //End foreach
			} //End Guests if

	} //End Confnum/Error If Statement


//printmsgs($successmsg,$errormsg);

$query = 'SELECT * FROM `tours_prv` WHERE `id` = "'.getval('tourid').'" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
	$tourinfo = mysql_fetch_assoc($result);

	$summary = '';
		$summary .= 'Title: <SPAN STYLE="font-weight:bold; color:#000080;">'.$tourinfo['title']."</SPAN><BR><BR>\n";
		$summary .= 'Description: '.nl2br(trim($tourinfo['desc']))."<BR><BR>\n";
		$summary .= 'Details: '.nl2br(trim($tourinfo['details']))."<BR><BR>\n";
		$summary .= 'Cost: $'.getval('amount')."<BR>\n";
		$summary .= '<BR>'."\n\n";

	//SEND CUSTOMER NOTIFICATION
		$heading = 'Your Private Tour Reservation #'.$confnum;
		$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "To: ".$_POST['name']." <".$_POST['email'].">\r\n";
			$headers .= "From: Bundu Bashers Tours <info@bundubashers.com>\r\n";
		$message = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$message .= 'Reservation number: <B>'.$confnum.'</B><BR><BR>'."\n";
			$message .= 'Thank you! This email acknowledges your order.<BR><BR>'."\n";
			$message .= 'Please note that your reservation is not guaranteed until you get a confirmation email from us. If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours. If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.<BR><BR>'."\n";
			$message .= 'We strongly advise you against making any plans that are dependent on this reservation, until you receive the confirmation email.<BR><BR>'."\n\n";
			//$message .= 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.<BR><BR>'."\n\n";
			$message .= $summary."<BR>\n\n";
			$message .= '1800 724 7767 extension 12 or (USA) 801 467 8687 x 12 info@bundubashers.com<BR><BR>'."\n\n";
			$message .= '<A HREF="http://www.bundubashers.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bashers.jpg" BORDER="0" ALT="Bundu Bashers"></A>&nbsp;&nbsp;<A HREF="http://www.bundubus.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bus.jpg" BORDER="0" ALT="Bundu Bus"></A><BR>'."\n";
		if(!mail('',$heading,$message,$headers)){
			array_push($errormsg,'Unable to send customer notification.');
			}

	//SEND MERCHANT NOTIFICATION
		$heading = 'New Private Tour Reservation #'.$confnum;
		$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
			//$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
			$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
		$message = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$message .= 'Reservation number: <B>'.$confnum.'</B><BR>'."\n";
			$message .= 'Name: '.$_POST['name'].'<BR>'."\n";
			$message .= 'Home/Bus. Phone: '.$_POST['phone_homebus'].'<BR>'."\n";
			$message .= 'Cell Phone: '.$_POST['phone_cell'].'<BR>'."\n";
			$message .= 'Cell Country: '.$_POST['cell_country'].'<BR>'."\n";
			$message .= 'Email: '.$_POST['email'].'<BR><BR>'."\n\n";
			$message .= 'Comments: '.$_POST['comments'].'<BR><BR>'."\n\n";
			$message .= $summary;
		if(!mail('',$heading,$message,$headers)){
			array_push($errormsg,'Unable to send merchant notification.');
			}


	echo '<BR><SPAN STYLE="font-family:Arial; font-size:20pt;">'.gettrans('Your order number').': <B>'.$confnum.'</B></SPAN><BR><BR>'."\n\n";

	echo '<SPAN STYLE="font-family:Arial; font-size:12pt;">';
	echo $summary;
	echo '</SPAN><BR>'."\n\n";

	echo '<DIV STYLE="font-family:Arial; font-size:10pt; text-align:left; color:#FF0000; width:80%;">';
	echo 'Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.  If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours.  If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.<BR><BR>'."\n\n";
	echo 'We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.<BR><BR>';
	//echo 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV>';


echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";


//!TOUR RESERVATION ***************************************************************************************************
} elseif(isset($_REQUEST['t']) && $_REQUEST['t'] != ""){


$fillform = $_REQUEST;
	$fillform['cartid'] = '*new*';
	$fillform['tourid'] = $_REQUEST['t'];

$query = 'SELECT * FROM `tours_prv` WHERE `id` = "'.$_REQUEST['t'].'" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);


if(isset($num_results) && $num_results > 0){

$tourinfo = mysql_fetch_assoc($result);

$preftimes = array("Morning","Mid Day","Afternoon","Evening");

$lunchtypes = array("Turkey","Beef","Ham","Vegetarian");

?><SCRIPT><!--

function copypickup(){
	if(document.getElementById("sameasp").checked == true){
	document.getElementById("dropoff").value = document.getElementById("pickup").value;
	}
}

function taltdates(){
	if(document.getElementById("onlydateavl").value == 0){ var d = ""; } else { var d = "none"; }
	document.getElementById("showalt1").style.display = d;
	document.getElementById("showalt2").style.display = d;
}

function addguest(){
	var t = document.getElementById('gueststable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.verticalAlign = 'middle';
		d.style.whiteSpace = 'nowrap';
		d.style.paddingRight = '8px';
		d.innerHTML = '<? echo gettrans('Name'); ?>: <INPUT TYPE="text" NAME="guests_name[]" VALUE="" STYLE="width:200px;"><?
			if($tourinfo['getweights'] == "y"){ echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="" STYLE="font-size:9pt; width:100px;">'; }
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'">'.str_replace("'",'\\\'',$val).'</OPTION>';
					}
				echo '</SELECT>';
				}
				?>';

		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';

	}

function adjustguests(){
	var num = document.getElementById('numguests').value;
	if(document.getElementById('gueststable')){
		var t = document.getElementById('gueststable');
		while(t.rows.length < num){ addguest(); }
		}
	}

function checkexp(){
	var r = true;
	var msg = '';
	//if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
	//	msg += '<? //echo gettrans('Please choose an expiration date for your credit card.'); ?>'+"\n";
	//	r =  false;
	//	}
	if(document.getElementById("email").value != document.getElementById("email2").value){
		msg += '<? echo gettrans('Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.'); ?>'+"\n";
		r =  false;
		}
	if(msg != ""){ alert(msg); }
	return r;
	}

// --></SCRIPT><? echo "\n\n";


echo '<DIV STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:8px;">'.$tourinfo['title'].'</DIV>'."\n\n";

if(isset($tourinfo['desc']) && $tourinfo['desc'] != ""){ echo '<DIV STYLE="width:90%; font-family:Arial; font-size:12pt; font-weight:bold; color:#000080; padding-top:4px;">'.nl2br($tourinfo['desc']).'</DIV>'."\n\n"; }
if(isset($tourinfo['details']) && $tourinfo['details'] != ""){ echo '<TABLE BORDER="0" STYLE="margin-top:4px;"><TR><TD STYLE="text-align:left; font-family:Arial; font-size:10pt;">'.nl2br($tourinfo['details']).'</TD></TR></TABLE>'."\n\n"; }
if(isset($tourinfo['amount']) && $tourinfo['amount'] > 0){ echo '<DIV STYLE="width:90%; font-family:Arial; font-size:12pt; font-weight:bold; color:#000080; padding-top:4px; margin-bottom:10px;">Total: $'.$tourinfo['amount'].'</DIV>'."\n\n"; }


echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="javascript:return checkexp();">'."\n";
echo '<INPUT TYPE="hidden" NAME="form" ID="form" VALUE="process">'."\n";
echo '<INPUT TYPE="hidden" NAME="type" VALUE="t">'."\n";
echo '<INPUT TYPE="hidden" NAME="tourid" VALUE="'.$tourinfo['id'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="title" VALUE="'.addslashes($tourinfo['title']).'">'."\n";
echo '<INPUT TYPE="hidden" NAME="amount" VALUE="'.$tourinfo['amount'].'">'."\n";
	echo "\n";


echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">'."\n";

echo "\t".'<TR><TD COLSPAN="2" STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:center;">'.gettrans('Tour Information').'</TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:310px;">'.gettrans('Tour date').'</TD><TD ALIGN="left">';
	echo '<SELECT NAME="date_m">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_m') == $i ){ echo " SELECTED"; }
			echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_d">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_d') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_y">';
		for($i=date("Y",$time); $i<(date("Y",$time)+6); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_y') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Is this the only date you are available?').'</TD><TD ALIGN="left"><SELECT NAME="onlydateavl" ID="onlydateavl" onChange="taltdates()"><OPTION VALUE="0"';
	if(getval('onlydateavl') == 0): echo ' SELECTED'; endif;
	echo '>'.gettrans('No').'</OPTION><OPTION VALUE="1"';
	if(getval('onlydateavl') == 1): echo ' SELECTED'; endif;
	echo '>'.gettrans('Yes').'</OPTION></SELECT></TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;"><DIV ID="showalt1"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '>'.gettrans('If not, what alternate dates are you available?').'</DIV></TD><TD ALIGN="left"><DIV ID="showalt2"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '><TEXTAREA NAME="altdates" ID="altdates" COLS="30" ROWS="2">'.getval('altdates').'</TEXTAREA></DIV></TD></TR>'."\n";

if($tourinfo['getpreftime'] == "y"){
	echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Preferred time of day').'</TD><TD ALIGN="left"><SELECT NAME="preftime">'; foreach($preftimes as $thistime){ echo '<OPTION VALUE="'.$thistime.'"'; if(getval('preftime') == $thistime): echo ' SELECTED'; endif; echo '>'.$thistime.'</OPTION>'; } echo '</SELECT></TD></TR>'."\n";
	}
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for pick up').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'</SPAN></TD><TD ALIGN="left"><TEXTAREA NAME="dep_loc" ID="pickup" COLS="30" ROWS="2" onChange="copypickup()">'.getval('dep_loc').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for drop off').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'<BR><label for="sameasp">'.gettrans('Check if this is the same as pick up').'</label></SPAN><INPUT TYPE="checkbox" ID="sameasp" onClick="copypickup()"></TD><TD ALIGN="left"><TEXTAREA NAME="arr_loc" ID="dropoff" COLS="30" ROWS="2">'.getval('arr_loc').'</TEXTAREA></TD></TR>'."\n";

/*if(getval('numguests') == ""){ $fillform['numguests'] = 2; }
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Number of guests').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.wordwrap(gettrans('Please contact us for a group rate if there are more than eight in your party.'),50,'<BR>').'</SPAN></TD><TD ALIGN="left"><SELECT NAME="numguests" ID="numguests" STYLE="font-size:14pt;" onChange="adjustguests();">';
	for($i=1; $i<9; $i++){
		echo'<OPTION VALUE="'.$i.'"';
			if($i == getval('numguests')){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";*/

if($tourinfo['getweights'] == "y" || $tourinfo['getlunch'] == "y"){
	if(!isset($fillform['guests_name'])){ $fillform['guests_name'] = array(); }
	while(count($fillform['guests_name']) < 4){ array_push($fillform['guests_name'],''); }
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Guest information');
		if($tourinfo['getweights'] == "y"){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Weights are only needed if you have ordered an airplane or helicopter tour.').'</SPAN>'."\n"; }
		echo '<DIV STYLE="padding-bottom:4px;"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" ID="gueststable">';
		foreach($fillform['guests_name'] as $key => $row){
			echo "\t".'<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap; padding-right:8px;">'.gettrans('Name').': <INPUT TYPE="text" NAME="guests_name[]" VALUE="'.$fillform['guests_name'][$key].'" STYLE="font-size:9pt; width:200px;">';
			if($tourinfo['getweights'] == "y"){ echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="'.$fillform['guests_weight'][$key].'" STYLE="font-size:9pt; width:100px;">'; }
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.$val.'"';
					if($fillform['guests_lunch'][$key] == $val){ echo ' SELECTED'; }
					echo '>'.$val.'</OPTION>';
					}
				echo '</SELECT>';
				}
			echo '</TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this guest" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>'."\n";
			}
		echo '</TABLE></DIV>';
		echo '<INPUT TYPE="button" VALUE="Add another guest" STYLE="font-size:8pt;" onClick="addguest();">';
		echo '</TD></TR>'."\n\n";
	}

//echo '</TABLE><BR>'."\n\n";
//echo '<TABLE BORDER="1" CELLPADDING="0" CELLSPACING="4" STYLE="width:80%;">'."\n";

	echo "\t".'<TR><TD COLSPAN="2" STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:center;">'.gettrans('Contact').' / '.gettrans('Payment Information').'</TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Name').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('As it appears on your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="'.getval('name').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Home/Business Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="'.getval('phone_homebus').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="'.getval('phone_cell').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Country').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cell_country" VALUE="'.getval('cell_country').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" ID="email" VALUE="'.getval('email').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Re-type Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email2" ID="email2" VALUE="'.getval('email2').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Credit card number').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('We accept Visa, MasterCard and American Express.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" VALUE="'.getval('cc_num').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Expiration date').'</TD><TD>';
		echo '<SELECT NAME="cc_expdate_month" ID="cc_expdate_month">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("n",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("Y",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Security code').'</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'" STYLE="width:50px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Zip/Postal code').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Please provide the zip or postal code associated with your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="'.getval('cc_zip').'" STYLE="width:80px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.wordwrap(gettrans('If the card holder will not be there, provide the name of one of the guests'),40,'<BR>').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="alt_name" VALUE="'.getval('alt_name').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Comments').'</TD><TD ALIGN="left" WIDTH="'.$rightcol.'"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:300px; height:80px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="center" COLSPAN="2"><INPUT TYPE="submit" NAME="submit" VALUE="'.gettrans('Reserve').'" STYLE="width:180px;"></TD></TR>'."\n";

	echo '</TABLE>'."\n\n";

	echo '</FORM>'."\n\n";

echo '<DIV STYLE="width:80%; font-family:Arial; font-size:10pt; text-align:left; color:#FF0000;">Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.<BR>We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.';
	//echo '<BR>We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV><BR><BR>';

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";



} else {
	echo '<DIV STYLE="font-family:Arial; font-size:11pt; padding:20px;">The tour you have selected is not available to order at this time.<BR><BR>Please <A HREF="http://www.bundubashers.com/grand_canyon_tours_contact.php">Contact Us</A> so we can direct you to current tours that are similar.</DIV>';
} //End Num results If statement

}



echo '<DIV STYLE="font-family:Arial; font-size:8pt;">'.gettrans('Bundu Bashers Tours will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";

include('footer.php');

?>