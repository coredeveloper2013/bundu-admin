<?  // Developed by Dominick Bernal - www.bernalwebservices.com

include_once('header_reserve.php');

if(!isset($_SESSION['shuttle_cart'])){ $_SESSION['shuttle_cart'] = array(); }
if(!isset($_SESSION['continue'])){ $_SESSION['continue'] = 'http://www.parkcityshuttle.com'; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_PATH) != parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH)){
	$_SESSION['continue'] = $_SERVER['HTTP_REFERER'];
	}

$fillform = array();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();


//GET MARKETS
$markets = array();
$query = 'SELECT * FROM `markets` WHERE (`id` != 3 AND `id` != 9) ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$markets['m'.$row['id']] = $row;
	}

//GET TRANS TYPES
$transtypes = array();
$query = 'SELECT * FROM `shuttle_transtypes` WHERE (`id` != 12 AND `id` != 16) ORDER BY `name` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$transtypes['t'.$row['id']] = $row;
	}

$triptypes = array(
	"round" => "Round Trip",
	"arrival" => "One Way: Arrival",
	"departure" => "One Way: Departure"
	);

if(isset($_POST) && count($_POST) > 0){
	foreach($_POST as $key => $val){
		if(!is_array($val)){
		$_POST[$key] = strip_tags($_POST[$key]);
		$_POST[$key] = stripslashes($_POST[$key]);
		}
		}
	}

echo '<CENTER>';

if(isset($_REQUEST['setmarket'])){ $_REQUEST['market'] = $_REQUEST['setmarket']; }
if(isset($_REQUEST['settranstype'])){ $_REQUEST['s'] = $_REQUEST['settranstype']; }

//DIRECT TO CORRECT FORM
$form = 'cart';
if(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "addtocart"){
	$form = 'cart';
	$limits = array('min'=>0,'max'=>0);
	$query = 'SELECT MIN(`pax1`) AS `min`, MAX(`pax2`) AS `max` FROM `shuttle_pricing` WHERE `market` = "'.$_REQUEST['market'].'" AND `trans_type` = "'.$_REQUEST['typeid'].'" AND (`adults`+`seniors`+`children`) > 0 GROUP BY `market`';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){ $limits = mysql_fetch_assoc($result); }
	if($_REQUEST['triptype'] != "departure"){
	if(($_REQUEST['arr_adults']+$_REQUEST['arr_seniors']+$_REQUEST['arr_children']) < $limits['min'] || ($_REQUEST['arr_adults']+$_REQUEST['arr_seniors']+$_REQUEST['arr_children']) > $limits['max']){
		$form = 'shuttle';
		array_push($puberrormsg,'Number of passengers for arrival is not available for '.$transtypes['t'.$_REQUEST['typeid']]['name'].'.<BR>Please choose between '.$limits['min'].' and '.$limits['max'].' passengers total, or choose a different form of transporation.');
		}
		}
	if($_REQUEST['triptype'] != "arrival"){
	if(($_REQUEST['dep_adults']+$_REQUEST['dep_seniors']+$_REQUEST['dep_children']) < $limits['min'] || ($_REQUEST['dep_adults']+$_REQUEST['dep_seniors']+$_REQUEST['dep_children']) > $limits['max']){
		$form = 'shuttle';
		array_push($puberrormsg,'Number of passengers for departure is not available for '.$transtypes['t'.$_REQUEST['typeid']]['name'].'.<BR>Please choose between '.$limits['min'].' and '.$limits['max'].' passengers total, or choose a different form of transporation.');
		}
		}
	} elseif(isset($_REQUEST['s']) || isset($_REQUEST['market'])){
		if(!isset($transtypes['t'.$_REQUEST['s']]) || !isset($markets['m'.$_REQUEST['market']])){
			$form = 'market';
			} else {
			$query = 'SELECT `market` FROM `shuttle_pricing` WHERE `market` = "'.$_REQUEST['market'].'" AND `trans_type` = "'.$_REQUEST['s'].'" AND (`adults`+`seniors`+`children`) > 0 GROUP BY `market`';
			$result = mysql_query($query);
			$num_results = mysql_num_rows($result);
			if($num_results > 0){
				$form = 'shuttle';
				} else {
				$form = 'market';
				array_push($puberrormsg,$transtypes['t'.$_REQUEST['s']]['name'].' is not available in '.$markets['m'.$_REQUEST['market']]['name'].'.<BR>Please choose a different form of transportation.');				
				}
			}
	} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "process" && count($_SESSION['shuttle_cart']) > 0){
		$form = 'process';
	}


if($form == "process" && count($_SESSION['shuttle_cart']) > 0){  // PROCESS RESERVATION **********************************************************

//echo '</CENTER>';
//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['shuttle_cart']); echo '</PRE>';

	foreach($_POST as $key => $val){
		if(!is_array($val)){
			$_POST[$key] = strip_tags($_POST[$key]);
			$_POST[$key] = trim($_POST[$key]);
			//$_POST[$key] = str_replace('"','\"',$_POST[$key]);
			}
		}

	//RECORD RESERVATION
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];
	$agent = ''; //if(isset($_SESSION['agent']['id']) && $_SESSION['agent']['id'] != ""): $agent = $_SESSION['agent']['id']; else: $agent = ''; endif;
	$_POST['pay_method'] = 'C/C'; //if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $_POST['pay_method'] = 'Alternate'; else: $_POST['pay_method'] = 'C/C'; endif;
	$_POST['amount'] = 0;
		foreach($_SESSION['shuttle_cart'] as $row){
			if($row['triptype'] != "departure"){ $_POST['amount'] = ($_POST['amount']+$row['arr_amount']); }
			if($row['triptype'] != "arrival"){ $_POST['amount'] = ($_POST['amount']+$row['dep_amount']); }			
			}

	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`pay_method`,`alt_name`,`comments`,`agent`,`http_referer`,`booker`,`date_booked`)
				VALUES("'.mysql_real_escape_string($_POST['name']).'","'.mysql_real_escape_string($_POST['phone_homebus']).'","'.mysql_real_escape_string($_POST['phone_cell']).'","'.mysql_real_escape_string($_POST['cell_country']).'","'.mysql_real_escape_string($_POST['email']).'","'.mysql_real_escape_string($_POST['amount']).'",
						"'.mysql_real_escape_string($_POST['name']).'","'.mysql_real_escape_string($_POST['cc_num']).'","'.mysql_real_escape_string($_POST['cc_expdate']).'","'.mysql_real_escape_string($_POST['cc_scode']).'","'.mysql_real_escape_string($_POST['pay_method']).'","'.mysql_real_escape_string($_POST['alt_name']).'",
						"'.mysql_real_escape_string($_POST['comments']).'","'.mysql_real_escape_string($agent).'","'.mysql_real_escape_string($_SESSION['http_referer']).'","Website","'.$time.'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror != ""){

		array_push($errormsg,$thiserror);

	} else {

	//START ON SUBRESERVATIONS
		$confnum = mysql_insert_id();
		array_push($successmsg,'Saved new reservation '.$confnum.' for '.$_POST['name'].'.');

		foreach($_SESSION['shuttle_cart'] as $row){
			foreach($row as $key => $val){
				if(!is_array($val)){
					$row[$key] = str_replace('"','\"',$row[$key]);
					}
				}
			$fillform = $row;

			//ARRIVAL
			if(getval('triptype') != "departure"){
				$arr_date = mktime(0,0,0,getval('arr_m'),getval('arr_d'),getval('arr_y'));
				$arr_time = mktime(getval('arr_hour'),getval('arr_mins'),0,getval('arr_m'),getval('arr_d'),getval('arr_y'));
				$query = 'INSERT INTO `reservations_assoc`(`reservation`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`market`,`trans_type`,`notes`,`onlydateavl`,`amount`)
							VALUES("'.mysql_real_escape_string($confnum).'","s","'.mysql_real_escape_string($_POST['name']).'","'.mysql_real_escape_string(getval('arr_adults')).'","'.mysql_real_escape_string(getval('arr_seniors')).'",
									"'.mysql_real_escape_string(getval('arr_children')).'",'.mysql_real_escape_string($arr_date).',"SLC Airport",'.mysql_real_escape_string($arr_time).',"'.mysql_real_escape_string(getval('going_to')).'",0,
									"'.mysql_real_escape_string(getval('market')).'","'.mysql_real_escape_string(getval('typeid')).'","Last flight: '.mysql_real_escape_string(getval('last_airline')).' '.mysql_real_escape_string(getval('last_flightnum')).' '.mysql_real_escape_string(getval('last_depcity')).'",
									1,"'.mysql_real_escape_string(getval('arr_amount')).'")';
					@mysql_query($query);
				$thiserror = mysql_error();
				if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }
				}

			//DEPARTURE
			if(getval('triptype') != "arrival"){
				$dep_date = mktime(0,0,0,getval('dep_m'),getval('dep_d'),getval('dep_y'));
				$dep_time = mktime(getval('dep_hour'),getval('dep_mins'),0,getval('dep_m'),getval('dep_d'),getval('dep_y'));
				$dep_flighttime = mktime(getval('dep_flighttime_hour'),getval('dep_flighttime_mins'),0,getval('dep_m'),getval('dep_d'),getval('dep_y'));
				$query = 'INSERT INTO `reservations_assoc`(`reservation`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`market`,`trans_type`,`notes`,`onlydateavl`,`amount`)';
					$query .= ' VALUES("'.mysql_real_escape_string($confnum).'","s","'.mysql_real_escape_string($_POST['name']).'","'.mysql_real_escape_string(getval('dep_adults')).'","'.mysql_real_escape_string(getval('dep_seniors')).'",
					"'.mysql_real_escape_string(getval('dep_children')).'",'.mysql_real_escape_string($dep_date).',"'.mysql_real_escape_string(getval('coming_from')).'",'.mysql_real_escape_string($dep_time).',"SLC Airport",0,
					"'.mysql_real_escape_string(getval('market')).'","'.mysql_real_escape_string(getval('typeid')).'","Flight time: '.date("g:ia",$dep_flighttime).'",1,"'.mysql_real_escape_string(getval('dep_amount')).'")';
					@mysql_query($query);
				$thiserror = mysql_error();
				if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }
				}

			} //End Foreach

	} //End Confnum/Error If Statement


//printmsgs($successmsg,$errormsg);

	bgcolor('reset');
	$summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";
	$summary .= "\t".'<TR>';
		$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Date/Flight Info.</TD>';
		$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Type/Passengers</TD>';
		$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Trip</TD>';
		$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Price</TD>';
		$summary .= '</TR>'."\n";
		$TOTALCOST = 0;
		foreach($_SESSION['shuttle_cart'] as $id => $row){
		//PRINT ARRIVAL
		if($row['triptype'] != "departure"){
		if($row['arr_mins'] == ""){ $row['arr_mins'] = 0; }
		$arr_date = mktime($row['arr_hour'],$row['arr_mins'],0,$row['arr_m'],$row['arr_d'],$row['arr_y']);
		$summary .= "\t\t".'<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>Arrival:</I> '.date("n/j/Y g:ia",$arr_date).'<BR>';
			$summary .= '<I>Flight:</I> '.$row['last_airline'].' '.$row['last_flightnum'].' '.$row['last_depcity'];
			$summary .= '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$transtypes['t'.$row['typeid']]['name'].'<BR>';
			if($row['arr_adults'] > 0): $summary .= '<I>Adults:</I> '.$row['arr_adults']; endif;
			if($row['arr_adults'] > 0 && $row['arr_seniors'] > 0): $summary .= ', '; endif;
			if($row['arr_seniors'] > 0): $summary .= '<I>Seniors:</I> '.$row['arr_seniors']; endif;
			if($row['arr_adults'] > 0 && $row['arr_children'] > 0 || $row['arr_seniors'] > 0 && $row['arr_children'] > 0): $summary .= ', '; endif;
			if($row['arr_children'] > 0): $summary .= '<I>Children:</I> '.$row['arr_children']; endif;
			$summary .= '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>SLC Airport to-</I><BR>'.$markets["m".$row['market']]['name'].': '.nl2br($row['going_to']).'</TD>';
			$summary .= '<TD ALIGN="right" VALIGN="middle" STYLE="font-family:Arial; font-size:10pt; padding:4px; padding-right:8px;"><B>$'.number_format($row['arr_amount'],2,'.','').'</B></NOBR></TD>';
			$summary .= '</TR>'."\n";
		$TOTALCOST = ($TOTALCOST+$row['arr_amount']);
		} //End Arrival If Statement
		//PRINT DEPARTURE
		if($row['triptype'] != "arrival"){
		if($row['dep_mins'] == ""){ $row['dep_mins'] = 0; }
		$dep_date = mktime($row['dep_hour'],$row['dep_mins'],0,$row['dep_m'],$row['dep_d'],$row['dep_y']);
		if($row['dep_flighttime_mins'] == ""){ $row['dep_flighttime_mins'] = 0; }
		$dep_flighttime = mktime($row['dep_flighttime_hour'],$row['dep_flighttime_mins'],0,$row['dep_m'],$row['dep_d'],$row['dep_y']);
		$summary .= "\t\t".'<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>Departure:</I> '.date("n/j/Y g:ia",$dep_date).'<BR>';
			$summary .= '<I>Flight:</I> '.date("g:ia",$dep_flighttime);
			$summary .= '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$transtypes['t'.$row['typeid']]['name'].'<BR>';
			if($row['dep_adults'] > 0): $summary .= '<I>Adults:</I> '.$row['dep_adults']; endif;
			if($row['dep_adults'] > 0 && $row['dep_seniors'] > 0): $summary .= ', '; endif;
			if($row['dep_seniors'] > 0): $summary .= '<I>Seniors:</I> '.$row['dep_seniors']; endif;
			if($row['dep_adults'] > 0 && $row['dep_children'] > 0 || $row['dep_seniors'] > 0 && $row['dep_children'] > 0): $summary .= ', '; endif;
			if($row['dep_children'] > 0): $summary .= '<I>Children:</I> '.$row['dep_children']; endif;
			$summary .= '</SPAN></TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$markets["m".$row['market']]['name'].': '.nl2br($row['coming_from']).'<BR><I>-to SLC Airport</I></TD>';
			$summary .= '<TD ALIGN="right" VALIGN="middle" STYLE="font-family:Arial; font-size:10pt; padding:4px; padding-right:8px;"><NOBR><B>$'.number_format($row['dep_amount'],2,'.','').'</B></NOBR></TD>';
			$summary .= '</TR>'."\n";
		$TOTALCOST = ($TOTALCOST+$row['dep_amount']);
		} //End Departure If Statement
		} //End ForEach Loop
		$summary .= "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="4" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD></TR>'."\n";
		$summary .= '</TABLE><BR>'."\n\n";

	//SEND MERCHANT NOTIFICATION
		$heading = 'New Shuttle Reservation #'.$confnum;
		$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
			//$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
			$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
		$message = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$message .= 'Reservation number: <B>'.$confnum.'</B><BR>'."\n";
			$message .= 'Name: '.$_POST['name'].'<BR>'."\n";
			$message .= 'Home/Bus. Phone: '.$_POST['phone_homebus'].'<BR>'."\n";
			$message .= 'Cell Phone: '.$_POST['phone_cell'].'<BR>'."\n";
			$message .= 'Cell Country: '.$_POST['cell_country'].'<BR>'."\n";
			$message .= 'Email: '.$_POST['email'].'<BR><BR>'."\n\n";
			$message .= 'Comments: '.$_POST['comments'].'<BR><BR>'."\n\n";
			$message .= $summary;
		if(!mail('',$heading,$message,$headers)){
			array_push($errormsg,'Unable to send merchant notification.');
			}

	echo '<BR><SPAN STYLE="font-family:Arial; font-size:20pt;">'.gettrans('Your order number').': <B>'.$confnum.'</B></SPAN><BR><BR>'."\n\n";

	echo $summary;

	unset($_SESSION['shuttle_cart']);

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcityshuttle.com/">Park City Shuttle</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcitylimousines.com/">Park City Limousines</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.saltlakecarservice.com/">Salt Lake Car Service</A>';
	echo '</SPAN>'."\n\n";
	

//SHUTTLE RESERVATION ***************************************************************************************************
} elseif($form == 'market'){


echo '<FORM METHOD="get" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n\n";

echo '<DIV STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:8px;">Transportation Reservation</DIV><BR>'."\n\n";

printmsgs($pubsuccessmsg,$puberrormsg);

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="width:80%; margin-bottom:12px;">'."\n";

echo '<TR><TD CLASS="cart_head" STYLE="font-size:14pt;">To which area are you traveling?</TD><TD CLASS="cart_entry">';
	echo '<SELECT NAME="market" STYLE="width:400px; font-size:14pt;">';
		foreach($markets as $thismarket){
			echo '<OPTION VALUE="'.$thismarket['id'].'"';
			if($_REQUEST['market'] == $thismarket['id']){ echo " SELECTED"; }
			echo '>'.$thismarket['name'].'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD></TR>'."\n";

echo '<TR><TD CLASS="cart_head" STYLE="font-size:14pt;">Type of transportation</TD><TD CLASS="cart_entry">';
	echo '<SELECT NAME="s" STYLE="width:400px; font-size:14pt;">';
		foreach($transtypes as $thistype){
			echo '<OPTION VALUE="'.$thistype['id'].'"';
			if($_REQUEST['s'] == $thistype['id']){ echo ' SELECTED'; }
			echo '>'.$thistype['name'].'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD></TR>'."\n";

echo '</TABLE>'."\n";

echo '<INPUT TYPE="submit" STYLE="font-size:14pt; width:200px;" VALUE="Continue -&gt;">'."\n\n";

echo '</FORM>'."\n\n";

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcityshuttle.com/">Park City Shuttle</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcitylimousines.com/">Park City Limousines</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.saltlakecarservice.com/">Salt Lake Car Service</A>';
	echo '</SPAN>'."\n\n";


} elseif($form == 'shuttle'){


if(isset($puberrormsg) && count($puberrormsg) > 0){
	$fillform = $_REQUEST;
	} elseif(isset($_REQUEST['edit']) && isset($_SESSION['shuttle_cart'][$_REQUEST['edit']])){
	$fillform = $_SESSION['shuttle_cart'][$_REQUEST['edit']];
	$fillform['cartid'] = $_REQUEST['edit'];
	} else {
	$fillform = $_REQUEST;
	$fillform['cartid'] = '*new*';
	$fillform['triptype'] = 'round';
	$fillform['typeid'] = $_REQUEST['s'];
	}

if(getval('dep_m') == "" || getval('dep_d') == "" || getval('dep_y') == ""){
	$fillform['dep_m'] = date("n",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	$fillform['dep_d'] = date("j",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	$fillform['dep_y'] = date("Y",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	}

if(getval('arr_m') == "" || getval('arr_d') == "" || getval('arr_y') == ""){
	$fillform['arr_m'] = date("n",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	$fillform['arr_d'] = date("j",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	$fillform['arr_y'] = date("Y",mktime(0,0,0,date("n",$time),(date("j",$time)+1),date("Y",$time)));
	}

//COLLECT WHAT DATA?
$query = 'SELECT MAX(`adults`) AS `adults`, MAX(`seniors`) AS `seniors`, MAX(`children`) AS `children` FROM `shuttle_pricing` WHERE `market` = '.getval('market').' AND `trans_type` = '.getval('typeid').' GROUP BY `trans_type`';
	$result = mysql_query($query);
	$groups = mysql_fetch_assoc($result);

?><SCRIPT><!--

function toggleform(){
	var d = document.getElementById("triptype").value;
		if(d == "round"){
		document.getElementById("sec_arrival").style.display = "";
		document.getElementById("sec_departure").style.display = "";
		document.getElementById("dep_r").style.display = "none";
		} else if(d == "arrival"){
		document.getElementById("sec_arrival").style.display = "";
		document.getElementById("dep_r").style.display = "none";
		document.getElementById("sec_departure").style.display = "none";
		} else if(d == "departure"){
		document.getElementById("sec_arrival").style.display = "none";
		document.getElementById("sec_departure").style.display = "";
		document.getElementById("dep_r").style.display = "";
		}
	}

// --></SCRIPT><? echo "\n\n";

printmsgs($pubsuccessmsg,$puberrormsg);

echo '<DIV STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:8px;">';
	if(getval('market') != ""){ echo $markets['m'.getval('market')]['name']."<BR>\n"; }
	if(getval('typeid') != ""){ echo $transtypes['t'.getval('typeid')]['name']."<BR>\n"; }
	echo '</DIV><BR>'."\n\n";

echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="addtocart">'."\n";
echo '<INPUT TYPE="hidden" NAME="cartid" VALUE="'.getval('cartid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="type" VALUE="s">'."\n";
echo '<INPUT TYPE="hidden" NAME="typeid" VALUE="'.getval('typeid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="market" VALUE="'.getval('market').'">'."\n";
	echo "\n";


echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="margin-bottom:12px;">'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:12pt; font-weight:bold; text-align:right; padding-right:10px;">Type of trip</TD>';
	echo '<TD ALIGN="left"><SELECT NAME="triptype" ID="triptype" STYLE="font-size:12pt;" onChange="toggleform()">';
		foreach($triptypes as $key => $name){
			echo '<OPTION VALUE="'.$key.'"';
			if(getval('triptype') == $key){ echo " SELECTED"; }
			echo '>'.$name.'</OPTION>';
			}
		echo '</SELECT></TD></TR>'."\n\n";
	echo '</TABLE>'."\n\n";


echo '<DIV ID="sec_arrival"';
	if(getval('triptype') == "departure"){ echo ' STYLE="display:none;"'; }
	echo'>';
echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="width:80%; margin-bottom:12px;">'."\n";
echo '<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:14pt; font-weight:bold;">Arrival</TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Arrival date</TD><TD CLASS="cart_entry"><SELECT NAME="arr_m">';
	for($i=1; $i<13; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('arr_m') == $i){ echo " SELECTED"; }
		echo '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
		}
	echo '</SELECT> / <SELECT NAME="arr_d">';
	for($i=1; $i<32; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('arr_d') == $i){ echo " SELECTED"; }
		echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT> / <SELECT NAME="arr_y">';
	for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('arr_y') == $i){ echo " SELECTED"; }
		echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Arrival time</TD><TD CLASS="cart_entry"><SELECT NAME="arr_hour">';
	for($i=0; $i<24; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('arr_hour') == $i ){ echo " SELECTED"; }
		echo '>'.date("ga",mktime($i,"1","0","1","1","2005")).'</OPTION>';
		}
	echo '</SELECT>:<INPUT TYPE="text" SIZE="3" NAME="arr_mins" VALUE="'.getval('arr_mins').'"></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Last airline<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Please include info for all passegers</SPAN></TD><TD CLASS="cart_entry"><TEXTAREA NAME="last_airline" COLS="30" ROWS="2">'.getval('last_airline').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Last flight number<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Please include info for all passegers</SPAN></TD><TD CLASS="cart_entry"><TEXTAREA NAME="last_flightnum" COLS="30" ROWS="2">'.getval('last_flightnum').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Last departure city<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">Please include info for all passegers</SPAN></TD><TD CLASS="cart_entry"><TEXTAREA NAME="last_depcity" COLS="30" ROWS="2">'.getval('last_depcity').'</TEXTAREA></TD></TR>'."\n";
//echo '<TR><TD CLASS="cart_head">To which area are you traveling?</TD><TD CLASS="cart_entry"><INPUT TYPE="hidden" NAME="arrival_market" VALUE="'.getval('market').'">'.$markets['m'.getval('market')]['name'].'</TD></TR>'."\n";
//echo '<TR><TD CLASS="cart_head">Type of transportation</TD><TD CLASS="cart_entry"><INPUT TYPE="hidden" NAME="arrival_trans_type" VALUE="'.$_REQUEST['settranstype'].'"><FONT FACE="Arial" SIZE="2">'.$trans_types['t'.$_REQUEST['settranstype']]['name'].'</FONT></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Drop Off Hotel/Street Address</TD><TD CLASS="cart_entry"><TEXTAREA NAME="going_to" COLS="30" ROWS="2">'.getval('going_to').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Number of ';
	if($groups['seniors'] > 0 || $groups['children'] > 0){ echo 'adults'; } else { echo 'passengers'; }
	echo '</TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="arr_adults" VALUE="'.getval('arr_adults').'"></TD></TR>'."\n";
if($groups['seniors'] > 0){
	echo '<TR><TD CLASS="cart_head">Number of seniors<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">65 or older</SPAN></TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="arr_seniors" VALUE="'.getval('arr_seniors').'"></TD></TR>'."\n";
	}
if($groups['children'] > 0){
	echo '<TR><TD CLASS="cart_head">Number of chilren<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">12 years old or younger</SPAN></TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="arr_children" VALUE="'.getval('arr_children').'"></TD></TR>'."\n";
	}
echo '</TABLE>'."\n";
echo '</DIV>'."\n\n";

echo '<DIV ID="sec_departure"';
	if(getval('triptype') == "arrival"){ echo ' STYLE="display:none;"'; }
	echo'>';

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="width:80%;">'."\n";
echo '<TR BGCOLOR="#CCCCCC"><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:14pt; font-weight:bold;">Departure</TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Departure date</TD><TD CLASS="cart_entry"><SELECT NAME="dep_m">';
	for($i=1; $i<13; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('dep_m') == $i){ echo " SELECTED"; }
		echo '>'.date("F",mktime("0","0","0",$i,"1","2005")).'</OPTION>';
		}
	echo '</SELECT> / <SELECT NAME="dep_d">';
	for($i=1; $i<32; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('dep_d') == $i ){ echo " SELECTED"; }
		echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT> / <SELECT NAME="dep_y">';
	for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('dep_y') == $i){ echo " SELECTED"; }
		echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";
echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt;"><I>Private vehicles can leave at any time.  Shuttles only depart on the hour.<BR>Depending on various factors, and assuming no snow, allow 45 minutes to an hour to get to the airport.</I></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Pick up time</TD><TD CLASS="cart_entry"><SELECT NAME="dep_hour">';
	for($i=0; $i<24; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('dep_hour') == $i ){ echo " SELECTED"; }
		echo '>'.date("ga",mktime($i,"1","0","1","1","2005")).'</OPTION>';
		}
	echo '</SELECT>:<INPUT TYPE="text" SIZE="3" NAME="dep_mins" VALUE="'.getval('dep_mins').'"></TD></TR>'."\n";
echo '<TR><TD CLASS="cart_head">Departing flight time</TD><TD CLASS="cart_entry"><SELECT NAME="dep_flighttime_hour">';
	for($i=0; $i<24; $i++){
		echo '<OPTION VALUE="'.$i.'"';
		if(getval('dep_flighttime_hour') == $i ){ echo " SELECTED"; }
		echo '>'.date("ga",mktime($i,"1","0","1","1","2005")).'</OPTION>';
		}
	echo '</SELECT>:<INPUT TYPE="text" SIZE="3" NAME="dep_flighttime_mins" VALUE="'.getval('dep_flighttime_mins').'"></TD></TR>'."\n";
echo '</TABLE>'."\n";

echo '<DIV ID="dep_r"';
	if(getval('triptype') == "round"){ echo ' style="display: none;"'; }
	echo'>'."\n";
echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="width:80%;">'."\n";
echo '<TR><TD CLASS="cart_head">Pick Up Hotel/Street Address</TD><TD CLASS="cart_entry"><TEXTAREA NAME="coming_from" COLS="30" ROWS="2">'.getval('coming_from').'</TEXTAREA></TD></TR>'."\n";
echo '</TABLE>'."\n";
echo '</DIV>'."\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="2" BGCOLOR="#FFFFFF" STYLE="width:80%; margin-bottom:12px;">'."\n";
echo '<TR><TD CLASS="cart_head">Number of ';
	if($groups['seniors'] > 0 || $groups['children'] > 0){ echo 'adults'; } else { echo 'passengers'; }
	echo '</TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="dep_adults" VALUE="'.getval('dep_adults').'"></TD></TR>'."\n";
if($groups['seniors'] > 0){
	echo '<TR><TD CLASS="cart_head">Number of seniors<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">65 or older</SPAN></TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="dep_seniors" VALUE="'.getval('dep_seniors').'"></TD></TR>'."\n";
	}
if($groups['children'] > 0){
	echo '<TR><TD CLASS="cart_head">Number of chilren<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">12 years old or younger</SPAN></TD><TD CLASS="cart_entry"><INPUT TYPE="text" SIZE="5" NAME="dep_children" VALUE="'.getval('dep_children').'"></TD></TR>'."\n";
	}
echo '</TABLE>'."\n";

echo '</DIV>'."\n\n";

echo '<INPUT TYPE="submit" STYLE="font-size:14pt; width:200px;" VALUE="Continue -&gt;">'."\n\n";

echo '</FORM><BR>'."\n\n";


echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcityshuttle.com/">Park City Shuttle</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcitylimousines.com/">Park City Limousines</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.saltlakecarservice.com/">Salt Lake Car Service</A>';
	echo '</SPAN>'."\n\n";


//SHOPPING CART ***************************************************************************************************
} else {


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

function getamount($item){
	global $SESSION;
	$amount = array('arr_amount'=>0, 'dep_amount'=>0);

	$arr_adults = $item['arr_adults'];
	$dep_adults = $item['dep_adults'];

	//ARRIVAL SENIORS
	if($item['arr_seniors'] > 0){
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$item['arr_seniors'].'" AND `pax2` >= "'.$item['arr_seniors'].'" AND `seniors` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$thisprice = mysql_fetch_assoc($result);
			$amount['arr_amount'] = ($amount['arr_amount']+$thisprice['seniors']);
			} else {
			$arr_adults = ($arr_adults+$item['arr_seniors']);
			}
		}

	//ARRIVAL CHILDREN
	if($item['arr_children'] > 0){
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$item['arr_children'].'" AND `pax2` >= "'.$item['arr_children'].'" AND `children` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$thisprice = mysql_fetch_assoc($result);
			$amount['arr_amount'] = ($amount['arr_amount']+$thisprice['children']);
			} else {
			$arr_adults = ($arr_adults+$item['arr_children']);
			}
		}

	//ARRIVAL ADULTS
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$arr_adults.'" AND `pax2` >= "'.$arr_adults.'" AND `adults` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		$thisprice = mysql_fetch_assoc($result);
		$amount['arr_amount'] = ($amount['arr_amount']+$thisprice['adults']);

	//DEPATURE SENIORS
	if($item['dep_seniors'] > 0){
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$item['dep_seniors'].'" AND `pax2` >= "'.$item['dep_seniors'].'" AND `seniors` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$thisprice = mysql_fetch_assoc($result);
			$amount['dep_amount'] = ($amount['dep_amount']+$thisprice['seniors']);
			} else {
			$dep_adults = ($dep_adults+$item['dep_seniors']);
			}
		}

	//DEPATURE CHILDREN
	if($item['dep_children'] > 0){
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$item['dep_children'].'" AND `pax2` >= "'.$item['dep_children'].'" AND `children` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		if($num_results == 1){
			$thisprice = mysql_fetch_assoc($result);
			$amount['dep_amount'] = ($amount['dep_amount']+$thisprice['children']);
			} else {
			$dep_adults = ($dep_adults+$item['dep_children']);
			}
		}

	//DEPATURE ADULTS
	$query = 'SELECT * FROM `shuttle_pricing` WHERE `market` = "'.$item['market'].'" AND `trans_type` = "'.$item['typeid'].'" AND `pax1` <= "'.$dep_adults.'" AND `pax2` >= "'.$dep_adults.'" AND `adults` > 0 ORDER BY `market` ASC, `pax1` ASC, `pax2` ASC, `adults` ASC LIMIT 1';
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
		$thisprice = mysql_fetch_assoc($result);
		$amount['dep_amount'] = ($amount['dep_amount']+$thisprice['adults']);
		
	return $amount;
	}


//********************************************** ADD ITEMS TO SHOPPING CART *********************************************
if(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid']) && $_POST['cartid'] == '*new*'){
	$getamount = getamount($_POST); $_POST['arr_amount'] = $getamount['arr_amount']; $_POST['dep_amount'] = $getamount['dep_amount'];
	if($_POST['triptype'] == 'round'){ $_POST['coming_from'] = $_POST['going_to']; }
	array_push($_SESSION['shuttle_cart'],$_POST);
	} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid'])){
	$getamount = getamount($_POST); $_POST['arr_amount'] = $getamount['arr_amount']; $_POST['dep_amount'] = $getamount['dep_amount'];
	if($_POST['triptype'] == 'round'){ $_POST['coming_from'] = $_POST['going_to']; }
	$_SESSION['shuttle_cart'][$_POST['cartid']] = $_POST;
	} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "remove" && $_REQUEST['cartid'] != ""){
	array_splice($_SESSION['shuttle_cart'],$_REQUEST['cartid'],1);
	}
	//echo '<PRE>'; print_r($_SESSION['shuttle_cart']); echo '</PRE>';


?><SCRIPT><!--

function checkexp(){
	var r = true;
	var msg = '';
	if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
		msg += '<? echo gettrans('Please choose an expiration date for your credit card.'); ?>'+"\n";
		r =  false;
		}
	if(document.getElementById("email").value != document.getElementById("email2").value){
		msg += '<? echo gettrans('Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.'); ?>'+"\n";
		r =  false;
		}
	if(msg != ""){ alert(msg); }
	return r;
	}

function rmvitem(x){
	if( confirm('<? echo gettrans('Remove this item from your shopping cart?'); ?>') ){
		window.location = '<? echo $_SERVER['PHP_SELF']; ?>?utaction=remove&cartid='+x;
		}
	}

// --></SCRIPT><? echo "\n\n";


echo '<DIV STYLE="font-family:Arial; font-size:20pt; padding-top:8px; padding-bottom:6px;">'.gettrans('Shopping Cart').'</DIV>'."\n\n";

printmsgs($pubsuccessmsg,$puberrormsg);

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";

	bgcolor('reset');
	if(count($_SESSION['shuttle_cart']) > 0){

	echo "\t".'<TR>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Date/Flight Info.</TD>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Type/Passengers</TD>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Trip</TD>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Price</TD>';
		echo '<TD STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333;">&nbsp;</TD>';
		echo '</TR>'."\n";

	$TOTALCOST = 0;
	foreach($_SESSION['shuttle_cart'] as $id => $row){

		if(!isset($row['arr_amount']) || $row['arr_amount'] == "" || $row['arr_amount'] == 0 || !isset($row['dep_amount']) || $row['dep_amount'] == "" || $row['dep_amount'] == 0){
				$getamount = getamount($row); $_SESSION['shuttle_cart'][$id]['arr_amount'] = $getamount['arr_amount']; $_SESSION['shuttle_cart'][$id]['dep_amount'] = $getamount['dep_amount'];
				 }

		//PRINT ARRIVAL
		if($row['triptype'] != "departure"){
		if($row['arr_mins'] == ""){ $row['arr_mins'] = 0; }
		$arr_date = mktime($row['arr_hour'],$row['arr_mins'],0,$row['arr_m'],$row['arr_d'],$row['arr_y']);
		echo "\t\t".'<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>Arrival:</I> '.date("n/j/Y g:ia",$arr_date).'<BR>';
			echo '<I>Flight:</I> '.$row['last_airline'].' '.$row['last_flightnum'].' '.$row['last_depcity'];
			echo '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$transtypes['t'.$row['typeid']]['name'].'<BR>';
			if($row['arr_adults'] > 0): echo '<I>Adults:</I> '.$row['arr_adults']; endif;
			if($row['arr_adults'] > 0 && $row['arr_seniors'] > 0): echo ', '; endif;
			if($row['arr_seniors'] > 0): echo '<I>Seniors:</I> '.$row['arr_seniors']; endif;
			if($row['arr_adults'] > 0 && $row['arr_children'] > 0 || $row['arr_seniors'] > 0 && $row['arr_children'] > 0): echo ', '; endif;
			if($row['arr_children'] > 0): echo '<I>Children:</I> '.$row['arr_children']; endif;
			echo '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>SLC Airport to-</I><BR>'.$markets["m".$row['market']]['name'].': '.nl2br($row['going_to']).'</TD>';
			echo '<TD ALIGN="right" VALIGN="middle" STYLE="font-family:Arial; font-size:10pt; padding:4px; padding-right:8px;"><B>$'.number_format($row['arr_amount'],2,'.','').'</B></NOBR></TD>';
			echo '<TD ALIGN="right" VALIGN="middle" STYLE="padding:4px; padding-top:2px; padding-bottom:2px;"';
				if($row['triptype'] == "round"){ echo ' ROWSPAN="2"'; }
				echo '>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&market='.$row['market'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
			echo '</TR>'."\n";
		$TOTALCOST = ($TOTALCOST+$row['arr_amount']);
		} //End Arrival If Statement

		//PRINT DEPARTURE
		if($row['triptype'] != "arrival"){
		if($row['dep_mins'] == ""){ $row['dep_mins'] = 0; }
		$dep_date = mktime($row['dep_hour'],$row['dep_mins'],0,$row['dep_m'],$row['dep_d'],$row['dep_y']);
		if($row['dep_flighttime_mins'] == ""){ $row['dep_flighttime_mins'] = 0; }
		$dep_flighttime = mktime($row['dep_flighttime_hour'],$row['dep_flighttime_mins'],0,$row['dep_m'],$row['dep_d'],$row['dep_y']);
		echo "\t\t".'<TR BGCOLOR="#'.bgcolor('').'"><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;"><I>Departure:</I> '.date("n/j/Y g:ia",$dep_date).'<BR>';
			echo '<I>Flight:</I> '.date("g:ia",$dep_flighttime);
			echo '</TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$transtypes['t'.$row['typeid']]['name'].'<BR>';
			if($row['dep_adults'] > 0): echo '<I>Adults:</I> '.$row['dep_adults']; endif;
			if($row['dep_adults'] > 0 && $row['dep_seniors'] > 0): echo ', '; endif;
			if($row['dep_seniors'] > 0): echo '<I>Seniors:</I> '.$row['dep_seniors']; endif;
			if($row['dep_adults'] > 0 && $row['dep_children'] > 0 || $row['dep_seniors'] > 0 && $row['dep_children'] > 0): echo ', '; endif;
			if($row['dep_children'] > 0): echo '<I>Children:</I> '.$row['dep_children']; endif;
			echo '</SPAN></TD><TD ALIGN="left" VALIGN="top" STYLE="font-family:Arial; font-size:10pt; padding:4px;">'.$markets["m".$row['market']]['name'].': '.nl2br($row['coming_from']).'<BR><I>-to SLC Airport</I></TD>';
			echo '<TD ALIGN="right" VALIGN="middle" STYLE="font-family:Arial; font-size:10pt; padding:4px; padding-right:8px;"><NOBR><B>$'.number_format($row['dep_amount'],2,'.','').'</B></NOBR></TD>';
			if($row['triptype'] != "round"){
			echo '<TD ALIGN="right" VALIGN="middle" STYLE="padding:4px; padding-top:2px; padding-bottom:2px;">';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&market='.$row['market'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
				}
			echo '</TR>'."\n";
		$TOTALCOST = ($TOTALCOST+$row['dep_amount']);
		} //End Departure If Statement

		} //End ForEach Loop

		echo "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="4" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD><TD>&nbsp;</TD></TR>'."\n";

		echo "\t".'<TR><TD ALIGN="center" COLSPAN="5"><FONT FACE="Arial" SIZE="2"><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><I>-'.gettrans('or').'-</I><BR>'.gettrans('Submit payment below to order listed reservations.').'</FONT></TD></TR>'."\n";

		echo '</TABLE><BR>'."\n\n";

echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'" onSubmit="javascript:return checkexp();">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="process">'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" STYLE="width:80%;">'."\n";

	echo "\t".'<TR><TD COLSPAN="2" STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:center;">'.gettrans('Contact').' / '.gettrans('Payment Information').'</TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Name').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('As it appears on your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="'.getval('name').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Home/Business Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="'.getval('phone_homebus').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Cell Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="'.getval('phone_cell').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Cell Country').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cell_country" VALUE="'.getval('cell_country').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" ID="email" VALUE="'.getval('email').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Re-type Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email2" ID="email2" VALUE="'.getval('email2').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Credit card number').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('We accept Visa, MasterCard and American Express.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" VALUE="'.getval('cc_num').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Expiration date').'</TD><TD>';
		echo '<SELECT NAME="cc_expdate_month" ID="cc_expdate_month">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("n",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("Y",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Security code').'</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'" STYLE="width:50px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.wordwrap(gettrans('If the card holder will not be there, provide the name of one of the guests'),40,'<BR>').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="alt_name" VALUE="'.getval('alt_name').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" CLASS="cart_head">'.gettrans('Comments').'</TD><TD CLASS="cart_entry"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:300px; height:80px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="center" COLSPAN="2"><INPUT TYPE="submit" NAME="submit" VALUE="'.gettrans('Reserve').'" STYLE="width:180px;"></FORM></TD></TR>'."\n";

	} else {
		echo "\t\t".'<TR><TD ALIGN="center" COLSPAN="5"><FONT FACE="Arial" SIZE="2"><BR>'.gettrans('There are currently no reservations in your shopping cart.').'<BR><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><BR></FONT></TD></TR>'."\n";
	}

echo '</TABLE><BR>'."\n\n";

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold;">'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcityshuttle.com/">Park City Shuttle</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.parkcitylimousines.com/">Park City Limousines</A>';
	echo '&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.saltlakecarservice.com/">Salt Lake Car Service</A>';
	echo '</SPAN>'."\n\n";


} //End Tour/Shopping Cart If Statement

echo '<DIV STYLE="font-family:Arial; font-size:8pt; margin-top:20px;">'.gettrans('Utah Transportation will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";

//echo '<PRE>'; print_r($_SESSION['shuttle_cart']); echo '</PRE>';

include('footer.php');

?>