<?  // Developed by Dominick Bernal - www.bernalwebservices.com

include_once('header_reserve.php');

if(!isset($_SESSION['cart_special'])){ $_SESSION['cart_special'] = array(); }
if(!isset($_SESSION['continue'])){ $_SESSION['continue'] = 'http://www.bundubashers.com'; }
if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "" && parse_url($_SERVER['HTTP_REFERER'],PHP_URL_PATH) != parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH)){
	$_SESSION['continue'] = $_SERVER['HTTP_REFERER'];
	}

$fillform = array();

$successmsg = array();
$errormsg = array();
$pubsuccessmsg = array();
$puberrormsg = array();

$anchortourid = '1156';
$anchorprice = '149';
if(isset($_REQUEST['t'])){ $_REQUEST['t'] = $anchortourid; }
$specialtourid = '1153';
$specialprice = '19.95';

echo '<CENTER>';

if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
	echo '<FONT FACE="Arial" SIZE="2" COLOR="#FF6600"><B>You are logged in as agent: '.$_SESSION['agent']['name'].'</B></FONT><BR><BR>';
	}

//CHECK TOUR DATE
//echo '<PRE>'; print_r($_REQUEST); echo '</PRE>';
if(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart"){
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$anchortourid.'" AND `date` > "'.$time.'" AND `date` = "'.mktime(0,0,0,$_POST['date_m'],$_POST['date_d'],$_POST['date_y']).'" LIMIT 1';
	$result = mysql_query($query);
	$num_results = @mysql_num_rows($result);
	if($num_results != 1){
		$_POST['utaction'] = '';
		$_REQUEST['t'] = $anchortourid;
		if(isset($_POST['cartid']) && $_POST['cartid'] != '*new*'){ $_REQUEST['edit'] = $_POST['cartid']; }
		array_push($puberrormsg,'This tour is not running on the date you chose.  <A HREF="http://www.bundubashers.com/tour.php?id='.$anchortourid.'">Please click here to check the tour calendar.</A>');
		printmsgs($pubsuccessmsg,$puberrormsg);
		}
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$specialtourid.'" AND `date` > "'.$time.'" AND `date` = "'.mktime(0,0,0,$_POST['spec_date_m'],$_POST['spec_date_d'],$_POST['spec_date_y']).'" LIMIT 1';
	$result = mysql_query($query);
	$num_results = @mysql_num_rows($result);
	if($num_results != 1){
		$_POST['utaction'] = '';
		$_REQUEST['t'] = $anchortourid;
		if(isset($_POST['cartid']) && $_POST['cartid'] != '*new*'){ $_REQUEST['edit'] = $_POST['cartid']; }
		array_push($puberrormsg,'The special tour is not running on the date you chose.  <A HREF="http://www.bundubashers.com/tour.php?id='.$specialtourid.'">Please click here to check the tour calendar.</A>');
		printmsgs($pubsuccessmsg,$puberrormsg);
		}
	}


if($_REQUEST['form'] == "process" && count($_SESSION['cart_special']) > 0){  // PROCESS RESERVATION **********************************************************

//echo '</CENTER>';
//echo '<PRE>'; print_r($_POST); echo '</PRE>';
//echo '<PRE>'; print_r($_SESSION['cart_special']); echo '</PRE>';

	foreach($_POST as $key => $val){
		if(!is_array($val)){
			$_POST[$key] = strip_tags($_POST[$key]);
			$_POST[$key] = trim($_POST[$key]);
			//$_POST[$key] = str_replace('"','\"',$_POST[$key]);
			}
		}

	//RECORD RESERVATION
	$_POST['cc_num'] = str_replace(' ','',$_POST['cc_num']);
	$_POST['cc_num'] = str_replace('-','',$_POST['cc_num']);
	$_POST['cc_expdate'] = $_POST['cc_expdate_month'].'/'.$_POST['cc_expdate_year'];
	if(isset($_SESSION['agent']['id']) && $_SESSION['agent']['id'] != ""): $agent = $_SESSION['agent']['id']; else: $agent = ''; endif;
	if(isset($_POST['altpaymethod']) && $_POST['altpaymethod'] != ""): $_POST['pay_method'] = 'Alternate'; else: $_POST['pay_method'] = 'C/C'; endif;
	$_POST['amount'] = 0;
		foreach($_SESSION['cart_special'] as $row){
			$spec_price = ($specialprice * $row['numguests']);
			$_POST['amount'] = ($_POST['amount']+$row['amount']+$spec_price);
			}

	$query = 'INSERT INTO `reservations`(`name`,`phone_homebus`,`phone_cell`,`cell_country`,`email`,`amount`,`cc_name`,`cc_num`,`cc_expdate`,`cc_scode`,`cc_zip`,`pay_method`,`alt_name`,`comments`,`agent`,`http_referer`,`booker`,`date_booked`)';
		$query .= ' VALUES("'.$_POST['name'].'","'.$_POST['phone_homebus'].'","'.$_POST['phone_cell'].'","'.$_POST['cell_country'].'","'.$_POST['email'].'","'.$_POST['amount'].'","'.$_POST['name'].'","'.$_POST['cc_num'].'","'.$_POST['cc_expdate'].'","'.$_POST['cc_scode'].'","'.$_POST['cc_zip'].'","'.$_POST['pay_method'].'","'.$_POST['alt_name'].'","'.$_POST['comments'].'","'.$agent.'","'.$_SESSION['http_referer'].'","Website","'.$time.'")';
		@mysql_query($query);
	$thiserror = mysql_error();
	if($thiserror != ""){

		array_push($errormsg,$thiserror);

	} else {

	//START ON SUBRESERVATIONS
		$confnum = mysql_insert_id();
		$email_restypes = array();
		array_push($successmsg,'Saved new reservation '.$confnum.' for '.$_POST['name'].'.');

		foreach($_SESSION['cart_special'] as $row){
			foreach($row as $key => $val){
				if(!is_array($val)){
					$row[$key] = str_replace('"','\"',$row[$key]);
					}
				}
			$fillform = $row;

			array_push($email_restypes,'Special Tour');
			$fillform['date'] = mktime(0,0,0,getval('date_m'),getval('date_d'),getval('date_y'));
			$fillform['pretitle'] = '[Bundu Tour '.getval('tourid').'] ';
			$fillform['dep_loc'] = '"'.getval('dep_loc').'"';
			$fillform['dep_time'] = '('.getval('date').' + IFNULL((SELECT IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) FROM `routes`,`routes_dates`,`tours_assoc` WHERE tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`id` = routes_dates.`routeid` AND routes_dates.`date` = "'.getval('date').'" AND tours_assoc.`tourid` = "'.getval('tourid').'" ORDER BY `day` ASC, `order` ASC LIMIT 1),0))';
			$fillform['arr_loc'] = '"'.getval('arr_loc').'"';
			$fillform['arr_time'] = '"'.getval('arr_time').'"';
			$fillform['vendor'] = 'IFNULL((SELECT tours.`vendor` FROM `tours` WHERE tours.`id` = "'.getval('tourid').'" LIMIT 1),0)';
			if(getval('pretitle') != ""){ $fillform['title'] = getval('pretitle').' '.getval('title'); }

			$fillform['spec_date'] = mktime(0,0,0,getval('spec_date_m'),getval('spec_date_d'),getval('spec_date_y'));
			$fillform['spec_pretitle'] = '[Bundu Tour '.getval('specialtourid').'] ';
			$fillform['spec_dep_loc'] = getval('dep_loc');
			$fillform['spec_dep_time'] = '('.getval('spec_date').' + IFNULL((SELECT IF(routes_dates.`dep_time`=-1,routes.`dep_time`,routes_dates.`dep_time`) FROM `routes`,`routes_dates`,`tours_assoc` WHERE tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id` AND routes.`id` = routes_dates.`routeid` AND routes_dates.`date` = "'.getval('spec_date').'" AND tours_assoc.`tourid` = "'.getval('specialtourid').'" ORDER BY `day` ASC, `order` ASC LIMIT 1),0))';
			$fillform['spec_arr_loc'] = getval('arr_loc');
			$fillform['spec_arr_time'] = getval('arr_time');
			$fillform['spec_vendor'] = 'IFNULL((SELECT tours.`vendor` FROM `tours` WHERE tours.`id` = "'.getval('specialtourid').'" LIMIT 1),0)';
			if(getval('spec_pretitle') != ""){ $fillform['specialtitle'] = getval('spec_pretitle').' '.getval('specialtitle'); }

			//Insert into assoc table
			$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`preftime`,`activityid`,`market`,`trans_type`,`onlydateavl`,`altdates`,`lodgeid`,`nights`,`amount`,`tourid`,`routeid`,`bbticket`,`vendor`)';
				$query .= ' VALUES("'.$confnum.'","'.getval('title').'","'.getval('type').'","'.$_POST['name'].'","'.getval('numguests').'",0,0,'.getval('date').','.getval('dep_loc').','.getval('dep_time').','.getval('arr_loc').','.getval('arr_time').',"'.getval('preftime').'","'.getval('activityid').'","'.getval('market').'","'.getval('trans_type').'","'.getval('onlydateavl').'","'.getval('altdates').'","'.getval('lodgeid').'","'.getval('nights').'"';
				$query .= ',"'.getval('base').'","'.getval('tourid').'","'.getval('routeid').'","'.getval('bbticket').'",'.getval('vendor').')';
				@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

			//Add guests
			if(isset($row['guests_name']) && count($row['guests_name']) > 0){
			foreach($row['guests_name'] as $guestid => $guest){ if($guest != ""){
				if(!isset($row['guests_weight'][$guestid])){ $row['guests_weight'][$guestid] = ''; }
				if(!isset($row['guests_lunch'][$guestid])){ $row['guests_lunch'][$guestid] = ''; }
				$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$associd.'","'.$row['guests_name'][$guestid].'","'.$row['guests_weight'][$guestid].'","'.$row['guests_lunch'][$guestid].'")';
				@mysql_query($query);
				$thiserror = mysql_error();
				if($thiserror != ""): array_push($errormsg,$thiserror); endif;
				}} //End foreach
				} //End Guests if

			//Add activities
			foreach($row['optionid'] as $optionid => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y"){
				if(!isset($row['optionday'][$optionid]) || $row['optionday'][$optionid] == ""){ $row['optionday'][$optionid] = 1; }
				$optiondate = mktime(0,0,0,$row['date_m'],($row['date_d']+($row['optionday'][$optionid]-1)),$row['date_y']);
				if(getval('pretitle') != ""){ $row['optionname'][$optionid] = getval('pretitle').' '.$row['optionname'][$optionid]; }

				//Insert into assoc table
				$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`date`,`dep_time`,`preftime`,`activityid`,`amount`,`vendor`)';
					$query .= ' VALUES("'.$confnum.'","'.$row['optionname'][$optionid].'","a","'.$_POST['name'].'","'.$row['optionpax'][$optionid].'","'.$optiondate.'","'.$optiondate.'","'.getval('preftime').'","'.$row['optionid'][$optionid].'","'.($row['optionprice'][$optionid]*$row['optionpax'][$optionid]).'",(SELECT activities.`vendor` FROM `activities` WHERE activities.`id` = "'.$row['optionid'][$optionid].'" LIMIT 1))';
					@mysql_query($query);
				$thiserror = mysql_error();
				if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

				//Add guests
				if(isset($row['guests_name']) && count($row['guests_name']) > 0){
				foreach($row['guests_name'] as $guestid => $guest){ if($guest != ""){
					if(!isset($row['guests_weight'][$guestid])){ $row['guests_weight'][$guestid] = ''; }
					if(!isset($row['guests_lunch'][$guestid])){ $row['guests_lunch'][$guestid] = ''; }
					$query = 'INSERT INTO `reservations_guests`(`associd`,`name`,`weight`,`lunch`) VALUES("'.$associd.'","'.$row['guests_name'][$guestid].'","'.$row['guests_weight'][$guestid].'","'.$row['guests_lunch'][$guestid].'")';
					@mysql_query($query);
					$thiserror = mysql_error();
					if($thiserror != ""): array_push($errormsg,$thiserror); endif;
					}} //End foreach
					} //End Guests if

				}} //End Activities foreach

			//Insert into assoc table
			$query = 'INSERT INTO `reservations_assoc`(`reservation`,`reference`,`type`,`name`,`adults`,`seniors`,`children`,`date`,`dep_loc`,`dep_time`,`arr_loc`,`arr_time`,`preftime`,`activityid`,`market`,`trans_type`,`onlydateavl`,`altdates`,`lodgeid`,`nights`,`amount`,`tourid`,`routeid`,`bbticket`,`vendor`)';
				$query .= ' VALUES("'.$confnum.'","'.getval('specialtitle').'","'.getval('type').'","'.$_POST['name'].'","'.getval('numguests').'",0,0,'.getval('spec_date').','.getval('spec_dep_loc').','.getval('spec_dep_time').','.getval('spec_arr_loc').','.getval('spec_arr_time').',"'.getval('preftime').'","'.getval('activityid').'","'.getval('market').'","'.getval('trans_type').'","'.getval('spec_onlydateavl').'","'.getval('spec_altdates').'","'.getval('lodgeid').'","'.getval('nights').'"';
				$query .= ',"'.($specialprice * getval('numguests')).'","'.getval('specialtourid').'","'.getval('routeid').'","'.getval('bbticket').'",'.getval('spec_vendor').')';
				@mysql_query($query);
			$thiserror = mysql_error();
			if($thiserror == ""){ $associd = mysql_insert_id(); } else { array_push($errormsg,$thiserror); }

			} //End Foreach

	} //End Confnum/Error If Statement


//printmsgs($successmsg,$errormsg);


	$summary = '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";
		bgcolor('reset');
		$summary .= "\t".'<TR>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">'.gettrans('Details').'</TD>';
			$summary .= '<TD STYLE="padding:4px; padding-left:0px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">'.gettrans('Options').'/'.gettrans('Add Ons').'</TD>';
			$summary .= '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">'.gettrans('Totals').'</TD>';
			$summary .= '</TR>'."\n";
		$TOTALCOST = 0;
		foreach($_SESSION['cart_special'] as $id => $row){

			$date = mktime(0,0,0,$row['date_m'],$row['date_d'],$row['date_y']);
			if($row['onlydateavl'] > 0){ $onyldateavl = gettrans('Yes'); } else { $onyldateavl = gettrans('No'); }
			if(!isset($row['amount']) || $row['amount'] == "" || $row['amount'] == 0){
				$getamount = getamount($row); $_SESSION['cart_special'][$id]['base'] = $getamount['base']; $_SESSION['cart_special'][$id]['amount'] = $getamount['amount'];
				$row['amount'] = $getamount['amount'];
				}
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$date).'<BR>';
					if(isset($row['altdates']) && trim($row['altdates']) != ""): $summary .= gettrans('Alternate dates').': '.trim($row['altdates']).'<BR>'; endif;
					$summary .= gettrans('Guests').': '.$row['numguests'].'<BR>';
					$summary .= gettrans('Base price').': $'.number_format($row['base'],2,'.','').'<BR>';
					$summary .= gettrans('Pick Up').': '.$row['dep_loc'].'<BR>'.gettrans('Drop Off').': '.$row['arr_loc'].'<BR>';
					if(isset($row['preftime'])): $summary .= gettrans('Preferred time of day').': '.$row['preftime'].'<BR>'; endif;
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
				foreach($row['optionid'] as $key => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y"){
					$summary .= '<DIV>'.$row['optionname'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.$row['optionprice'][$key].' x '.$row['optionpax'][$key].' '.gettrans('guest(s)').' = <B>$'.number_format( ($row['optionprice'][$key]*$row['optionpax'][$key]),2,'.','').'</B></DIV>';
					}}
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
				$summary .= '</TR>'."\n";

			$spec_price = ($specialprice * $row['numguests']);
			$spec_date = mktime(0,0,0,$row['spec_date_m'],$row['spec_date_d'],$row['spec_date_y']);
			if($row['spec_onlydateavl'] > 0){ $onyldateavl = gettrans('Yes'); } else { $onyldateavl = gettrans('No'); }
			$rowbg = bgcolor('');
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="3" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['specialtitle'].'</TD></TR>'."\n";
			$summary .= "\t".'<TR BGCOLOR="#'.$rowbg.'">';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
					$summary .= 'Date: '.date("l, n/j/Y",$spec_date).'<BR>';
					if(isset($row['spec_altdates']) && trim($row['spec_altdates']) != ""): $summary .= gettrans('Alternate dates').': '.trim($row['spec_altdates']).'<BR>'; endif;
					$summary .= gettrans('Guests').': '.$row['numguests'].'<BR>';
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
					$summary .= '</TD>';
				$summary .= '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($spec_price,2,'.','').'</TD>';
				$summary .= '</TR>'."\n";
				
			$TOTALCOST = ($TOTALCOST+$row['amount']+$spec_price);
			} //End ForEach Loop
			$summary .= "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="3" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD></TR>'."\n";
			$summary .= '</TABLE><BR>'."\n\n";

	$email_restypes = array_unique($email_restypes);
			
	//SEND CUSTOMER NOTIFICATION
		$heading = 'Your '.implode('/',$email_restypes).' Reservation #'.$confnum;
		$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "To: ".$_POST['name']." <".$_POST['email'].">\r\n";
			$headers .= "From: Bundu Bashers Tours <info@bundubashers.com>\r\n";
		$message = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$message .= 'Reservation number: <B>'.$confnum.'</B><BR><BR>'."\n";
			$message .= 'Thank you! This email acknowledges your order.<BR><BR>'."\n";
			$message .= 'Please note that your reservation is not guaranteed until you get a confirmation email from us. If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours. If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.<BR><BR>'."\n";
			$message .= 'We strongly advise you against making any plans that are dependent on this reservation, until you receive the confirmation email.<BR><BR>'."\n\n";
			$message .= 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.<BR><BR>'."\n\n";
			$message .= $summary."<BR>\n\n";
			$message .= '1800 724 7767 extension 12 or (USA) 801 467 8687 x 12 info@bundubashers.com<BR><BR>'."\n\n";
			$message .= '<A HREF="http://www.bundubashers.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bashers.jpg" BORDER="0" ALT="Bundu Bashers"></A>&nbsp;&nbsp;<A HREF="http://www.bundubus.com"><IMG SRC="http://www.bundubashers.com/img/logo_mdm_bus.jpg" BORDER="0" ALT="Bundu Bus"></A><BR>'."\n";
		if(!mail('',$heading,$message,$headers)){
			array_push($errormsg,'Unable to send customer notification.');
			}

	//SEND MERCHANT NOTIFICATION
		$heading = 'New '.implode('/',$email_restypes).' Reservation #'.$confnum;
		$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";
			$headers .= "To: Bundu Bashers Tours <bundubashers@gmail.com>\r\n";
			$headers .= "Bcc: BWS Testing <dominick@bernalwebservices.com>\r\n";
			$headers .= "From: ".$_POST['name']." <".$_POST['email'].">\r\n";
		$message = '<FONT FACE="Arial" SIZE="3">'."\n\n";
			$message .= 'Reservation number: <B>'.$confnum.'</B><BR>'."\n";
			$message .= 'Name: '.$_POST['name'].'<BR>'."\n";
			$message .= 'Home/Bus. Phone: '.$_POST['phone_homebus'].'<BR>'."\n";
			$message .= 'Cell Phone: '.$_POST['phone_cell'].'<BR>'."\n";
			$message .= 'Cell Country: '.$_POST['cell_country'].'<BR>'."\n";
			$message .= 'Email: '.$_POST['email'].'<BR><BR>'."\n\n";
			$message .= 'Comments: '.$_POST['comments'].'<BR><BR>'."\n\n";
			$message .= $summary;
		if(!mail('',$heading,$message,$headers)){
			array_push($errormsg,'Unable to send merchant notification.');
			}

	echo '<BR><SPAN STYLE="font-family:Arial; font-size:20pt;">'.gettrans('Your order number').': <B>'.$confnum.'</B></SPAN><BR><BR>'."\n\n";

	echo $summary.'<BR>'."\n\n";

	echo '<DIV STYLE="font-family:Arial; font-size:10pt; text-align:left; color:#FF0000; width:80%;">';
	echo 'Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.  If you placed the order while one of our offices in the USA is open, you should receive an email in the next couple of hours.  If the order were placed outside regular business hours you can expect an email shortly after we open in the morning.<BR><BR>'."\n\n";
	echo 'We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.<BR><BR>';
	echo 'We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.';
	echo '</DIV>';

	unset($_SESSION['cart_special']);


echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";


//TOUR RESERVATION ***************************************************************************************************
} elseif(isset($_REQUEST['t']) && $_REQUEST['t'] != ""){


if(isset($_REQUEST['edit']) && isset($_SESSION['cart_special'][$_REQUEST['edit']])){
	$fillform = $_SESSION['cart_special'][$_REQUEST['edit']];
	$fillform['cartid'] = $_REQUEST['edit'];
	} else {
	$fillform = $_REQUEST;
	$fillform['cartid'] = '*new*';
	$fillform['typeid'] = $_REQUEST['t'];
	$fillform['tourid'] = $_REQUEST['t'];
	}

//ANCHOR TOUR
$query = 'SELECT * FROM `tours` WHERE `id` = "'.$_REQUEST['t'].'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);

if(isset($num_results) && $num_results > 0){
$tourinfo = mysql_fetch_assoc($result);

	$tourinfo['perguest'] = $anchorprice;
	$tourinfo['ext_pricing'] = '';
	$tourinfo['volstart'] = 0;
	$tourinfo['volrate'] = 0;
	$tourinfo['safetitle'] = $tourinfo['title']; //.' ('.$tourinfo['id'].')';

	//CHECK FOR PREFERRED LANGUAGE
	if($_SESSION['lang'] != "1"){
	$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$tourinfo['id'].'" AND `lang` = "'.$_SESSION['lang'].'" ORDER BY `id` DESC LIMIT 1';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		if($num_results > 0){
		$langinfo = mysql_fetch_assoc($result);
		if(trim($langinfo['title']) != ""): $tourinfo['title'] = $langinfo['title']; endif;
		if(trim($langinfo['short_desc']) != ""): $tourinfo['short_desc'] = $langinfo['short_desc']; endif;
		if(trim($langinfo['pricingdesc']) != ""): $tourinfo['pricingdesc'] = $langinfo['pricingdesc']; endif;
		if(trim($langinfo['highlights']) != ""): $tourinfo['highlights'] = $langinfo['highlights']; endif;
		if(trim($langinfo['details']) != ""): $tourinfo['details'] = $langinfo['details']; endif;
		if(trim($langinfo['pleasenote']) != ""): $tourinfo['pleasenote'] = $langinfo['pleasenote']; endif;
		}
	}

//GET ACTIVITIES
$tourinfo['options'] = array();
$query = 'SELECT DISTINCT tours_assoc.`tourid`, activities.`id` AS `optionid`, activities.`name`, activities.`price`, activities.`vendor`, tours_assoc.`day`, tours_assoc.`order`';
	$query .= ' FROM `tours_assoc`,`activities`';
	$query .= ' WHERE tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "f" AND tours_assoc.`type` = "a" AND tours_assoc.`typeid` = activities.`id`';
	$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC';
	//echo $query.'<BR>';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tourinfo['options'],$row);
	}

//GET TOUR DATES
$dates = array();
$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" ORDER BY `date` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($dates[date("Y",$row['date'])])){ $dates[date("Y",$row['date'])] = array(); }
	if(!isset($dates[date("Y",$row['date'])][date("n",$row['date'])])){ $dates[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
	$dates[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = 1;
	//array_push($dates,$row);
	}

if(getval('date_m') == "" || getval('date_d') == "" || getval('date_y') == ""){
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" AND `date` > "'.$time.'" ORDER BY `date` ASC LIMIT 1';
	$result = mysql_query($query);
	$num_results = @mysql_num_rows($result);
	if($num_results == 1){
		$date = mysql_fetch_assoc($result);
		$fillform['date_m'] = date("n",$date['date']);
		$fillform['date_d'] = date("j",$date['date']);
		$fillform['date_y'] = date("Y",$date['date']);
		}
	}

//SPECIAL TOUR
$query = 'SELECT * FROM `tours` WHERE `id` = "'.$specialtourid.'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);

if(isset($num_results) && $num_results > 0){
$specialinfo = mysql_fetch_assoc($result);

	$specialinfo['safetitle'] = $specialinfo['title']; //.' ('.$specialinfo['id'].')';

	//CHECK FOR PREFERRED LANGUAGE
	if($_SESSION['lang'] != "1"){
	$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$specialinfo['id'].'" AND `lang` = "'.$_SESSION['lang'].'" ORDER BY `id` DESC LIMIT 1';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		if($num_results > 0){
		$langinfo = mysql_fetch_assoc($result);
		if(trim($langinfo['title']) != ""): $specialinfo['title'] = $langinfo['title']; endif;
		if(trim($langinfo['short_desc']) != ""): $specialinfo['short_desc'] = $langinfo['short_desc']; endif;
		if(trim($langinfo['pricingdesc']) != ""): $specialinfo['pricingdesc'] = $langinfo['pricingdesc']; endif;
		if(trim($langinfo['highlights']) != ""): $specialinfo['highlights'] = $langinfo['highlights']; endif;
		if(trim($langinfo['details']) != ""): $specialinfo['details'] = $langinfo['details']; endif;
		if(trim($langinfo['pleasenote']) != ""): $specialinfo['pleasenote'] = $langinfo['pleasenote']; endif;
		}
	}

if(getval('spec_date_m') == "" || getval('spec_date_d') == "" || getval('spec_date_y') == ""){
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$specialinfo['id'].'" AND `date` > "'.$time.'" ORDER BY `date` ASC LIMIT 1';
	$result = mysql_query($query);
	$num_results = @mysql_num_rows($result);
	if($num_results == 1){
		$date = mysql_fetch_assoc($result);
		$fillform['spec_date_m'] = date("n",$date['date']);
		$fillform['spec_date_d'] = date("j",$date['date']);
		$fillform['spec_date_y'] = date("Y",$date['date']);
		}
	}
}

$preftimes = array("Morning","Mid Day","Afternoon","Evening");

$lunchtypes = array("Turkey","Beef","Ham","Vegetarian");

?><SCRIPT><!--

var runs = new Array();
<? foreach($dates as $y => $years){
	echo "\t".'runs['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'runs['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' runs['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

function copypickup(){
	if(document.getElementById("sameasp").checked == true){
	document.getElementById("dropoff").value = document.getElementById("pickup").value;
	}
}

function taltdates(){
	if(document.getElementById("onlydateavl").value == 0){ var d = ""; } else { var d = "none"; }
	document.getElementById("showalt1").style.display = d;
	document.getElementById("showalt2").style.display = d;

	if(document.getElementById("spec_onlydateavl").value == 0){ var d = ""; } else { var d = "none"; }
	document.getElementById("spec_showalt1").style.display = d;
	document.getElementById("spec_showalt2").style.display = d;
}

function optcopyguest(optid){
	if(document.getElementById('opt'+optid).checked){
		var num = document.getElementById('numguests').value;
		if(document.getElementById('optpax'+optid).value == ""){ document.getElementById('optpax'+optid).value = num; }
		}
	}

function addguest(){
	var t = document.getElementById('gueststable');
		var r = t.insertRow(t.rows.length);

		var d = r.insertCell(0);
		d.style.fontFamily = 'Arial';
		d.style.fontSize = '9pt';
		d.style.verticalAlign = 'middle';
		d.style.whiteSpace = 'nowrap';
		d.style.paddingRight = '8px';
		d.innerHTML = '<? echo gettrans('Name'); ?>: <INPUT TYPE="text" NAME="guests_name[]" VALUE="" STYLE="width:200px;"><?
			if($tourinfo['getweights'] == "y"){ echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="" STYLE="font-size:9pt; width:100px;">'; }
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.str_replace("'",'\\\'',$val).'">'.str_replace("'",'\\\'',$val).'</OPTION>';
					}
				echo '</SELECT>';
				}
				?>';
		var d = r.insertCell(1);
		d.style.verticalAlign = 'middle';
		d.style.paddingLeft = '3px';
		d.innerHTML = '<INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this line" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);">';
	}

function adjustguests(){
	var num = document.getElementById('numguests').value;
	var t = document.getElementById('gueststable');
	while(t.rows.length < num){ addguest(); }

	/*
	if(t.rows.length < num){
		while(t.rows.length < num){ addguest(); }
		} else if(t.rows.length > num){
		for(i=t.rows.length; i==0; i--){
			if(t.rows.length > num && ){
				}
			}
		}
	*/
	}

// --></SCRIPT><? echo "\n\n";


echo '<DIV STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:8px;">'.$tourinfo['title'].'</DIV><BR>'."\n\n";

echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="utaction" VALUE="addtocart">'."\n";
echo '<INPUT TYPE="hidden" NAME="cartid" VALUE="'.getval('cartid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="type" VALUE="t">'."\n";
echo '<INPUT TYPE="hidden" NAME="typeid" VALUE="'.getval('typeid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="tourid" VALUE="'.getval('typeid').'">'."\n";
echo '<INPUT TYPE="hidden" NAME="title" VALUE="'.$tourinfo['title'].'">'."\n";
echo '<INPUT TYPE="hidden" NAME="specialtourid" VALUE="'.$specialtourid.'">'."\n";
echo '<INPUT TYPE="hidden" NAME="specialtitle" VALUE="'.$specialinfo['title'].'">'."\n";
	echo "\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:310px;">'.gettrans('Tour date').'</TD><TD ALIGN="left">';
	//echo '<INPUT TYPE="hidden" NAME="date_m" VALUE="'.getval('date_m').'">
	echo '<SELECT NAME="date_m">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_m') == $i ){ echo " SELECTED"; }
			echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_d">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_d') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="date_y">';
		for($i=date("Y",$time); $i<(date("Y",$time)+6); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('date_y') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Is this the only date you are available?').'</TD><TD ALIGN="left"><SELECT NAME="onlydateavl" ID="onlydateavl" onChange="taltdates()"><OPTION VALUE="0"';
	if(getval('onlydateavl') == 0): echo ' SELECTED'; endif;
	echo '>'.gettrans('No').'</OPTION><OPTION VALUE="1"';
	if(getval('onlydateavl') == 1): echo ' SELECTED'; endif;
	echo '>'.gettrans('Yes').'</OPTION></SELECT></TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;"><DIV ID="showalt1"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '>'.gettrans('If not, what alternate dates are you available?').'</DIV></TD><TD ALIGN="left"><DIV ID="showalt2"'; if(getval('onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '><TEXTAREA NAME="altdates" ID="altdates" COLS="30" ROWS="2">'.getval('altdates').'</TEXTAREA></DIV></TD></TR>'."\n";

if($tourinfo['getpreftime'] == "y"){
	echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Preferred time of day').'</TD><TD ALIGN="left"><SELECT NAME="preftime">'; foreach($preftimes as $thistime){ echo '<OPTION VALUE="'.$thistime.'"'; if(getval('preftime') == $thistime): echo ' SELECTED'; endif; echo '>'.$thistime.'</OPTION>'; } echo '</SELECT></TD></TR>'."\n";
	}
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for pick up').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'</SPAN></TD><TD ALIGN="left"><TEXTAREA NAME="dep_loc" ID="pickup" COLS="30" ROWS="2" onChange="copypickup()">'.getval('dep_loc').'</TEXTAREA></TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Hotel or property for drop off').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('If applicable.').'<BR><label for="sameasp">'.gettrans('Check if this is the same as pick up').'</label></SPAN><INPUT TYPE="checkbox" ID="sameasp" onClick="copypickup()"></TD><TD ALIGN="left"><TEXTAREA NAME="arr_loc" ID="dropoff" COLS="30" ROWS="2">'.getval('arr_loc').'</TEXTAREA></TD></TR>'."\n";

if(getval('numguests') == ""){ $fillform['numguests'] = 2; }
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Number of guests').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.wordwrap(gettrans('Please contact us for a group rate if there are more than eight in your party.'),50,'<BR>').'</SPAN></TD><TD ALIGN="left"><SELECT NAME="numguests" ID="numguests" STYLE="font-size:14pt;" onChange="adjustguests();">';
	for($i=1; $i<11; $i++){
		echo'<OPTION VALUE="'.$i.'"';
			if($i == getval('numguests')){ echo ' SELECTED'; }
			echo '>'.$i.'</OPTION>';
		}
	echo '</SELECT></TD></TR>'."\n";

if(isset($tourinfo['options']) && count($tourinfo['options']) > 0){
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Tour Options').'<BR>'."\n";
	echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4">';
	foreach($tourinfo['options'] as $row){
		echo '<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="opt'.$row['optionid'].'" onClick="optcopyguest(\''.$row['optionid'].'\')">'.gettrans($row['name']).':</LABEL></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;"><LABEL FOR="opt'.$row['optionid'].'" onClick="optcopyguest(\''.$row['optionid'].'\')">+ $'.$row['price'].' '.gettrans('per guest').'</LABEL></TD>';
		echo '<TD ALIGN="center"><INPUT TYPE="hidden" NAME="optionid[]" VALUE="'.$row['optionid'].'"><INPUT TYPE="hidden" NAME="optionname[]" VALUE="'.$row['name'].'"><INPUT TYPE="hidden" NAME="optionprice[]" VALUE="'.$row['price'].'"><INPUT TYPE="hidden" NAME="optionday[]" VALUE="'.$row['day'].'"><INPUT TYPE="checkbox" NAME="addopt'.$row['optionid'].'" ID="opt'.$row['optionid'].'" VALUE="y"';
			if(getval('addopt'.$row['optionid']) == "y"): echo ' CHECKED'; endif;
			echo ' onClick="optcopyguest(\''.$row['optionid'].'\')"></TD>';
		echo '<TD ALIGN="right" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('For').'</TD>';
		echo '<TD><INPUT TYPE="text" NAME="optionpax[]" ID="optpax'.$row['optionid'].'" STYLE="width:32px;" VALUE="';
		if(isset($fillform['optionid']) && is_array($fillform['optionid'])){
			$key = array_search($row['optionid'],$fillform['optionid']);
			if($key !== FALSE && isset($fillform['optionpax'][$key])){ echo $fillform['optionpax'][$key]; }
			}
			echo '"></TD>';
		echo '<TD ALIGN="left" STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap;">'.gettrans('guests').'.</TD>';
		echo '</TR>'."\n";
		}
		echo '</TABLE>'."\n";
		echo '</TD></TR>'."\n";
	}

if($tourinfo['getweights'] == "y" || $tourinfo['getlunch'] == "y"){
	if(!isset($fillform['guests_name'])){ $fillform['guests_name'] = array(); }
	while(count($fillform['guests_name']) < 4){ array_push($fillform['guests_name'],''); }
	echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; padding-top:10px;">'.gettrans('Guest information');
		if($tourinfo['getweights'] == "y"){ echo '<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Weights are only needed if you have ordered an airplane or helicopter tour.').'</SPAN>'."\n"; }
		echo '<DIV STYLE="padding-bottom:4px;"><TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" ID="gueststable">';
		foreach($fillform['guests_name'] as $key => $row){
			echo "\t".'<TR><TD STYLE="font-family:Arial; font-size:9pt; vertical-align:middle; white-space:nowrap; padding-right:8px;">'.gettrans('Name').': <INPUT TYPE="text" NAME="guests_name[]" VALUE="'.$fillform['guests_name'][$key].'" STYLE="font-size:9pt; width:200px;">';
			if($tourinfo['getweights'] == "y"){ echo '&nbsp;&nbsp;'.gettrans('Weight').': <INPUT TYPE="text" NAME="guests_weight[]" VALUE="'.$fillform['guests_weight'][$key].'" STYLE="font-size:9pt; width:100px;">'; }
			if($tourinfo['getlunch'] == "y"){
			echo '&nbsp;&nbsp;'.gettrans('Lunch preference').': <SELECT NAME="guests_lunch[]" STYLE="font-size:9pt;">';
				foreach($lunchtypes as $val){
					echo '<OPTION VALUE="'.$val.'"';
					if($fillform['guests_lunch'][$key] == $val){ echo ' SELECTED'; }
					echo '>'.$val.'</OPTION>';
					}
				echo '</SELECT>';
				}
			echo '</TD><TD STYLE="vertical-align:middle; padding-left:3px;"><INPUT TYPE="button" VALUE="X" STYLE="font-size:10px; color:#FF0000; width:20px; text-align:center;" TITLE="Remove this guest" onClick="this.parentNode.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);"></TD></TR>'."\n";
			}
		echo '</TABLE></DIV>';
		echo '<INPUT TYPE="button" VALUE="Add another guest" STYLE="font-size:8pt;" onClick="addguest();">';
		echo '</TD></TR>'."\n\n";
	}


echo '<TR><TD COLSPAN="2" ALIGN="center" STYLE="font-family:Arial; font-size:18pt; font-weight:bold; color:#000080; padding-top:20px; padding-bottom:8px;">'.$specialinfo['title'].'<BR><SPAN STYLE="font-size:12pt;">$'.number_format($specialprice,2,'.','').' per person when you order '.$tourinfo['title'].'!</SPAN></TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px; width:310px;">'.gettrans('Tour date').'</TD><TD ALIGN="left">';
	//echo '<INPUT TYPE="hidden" NAME="date_m" VALUE="'.getval('date_m').'">
	echo '<SELECT NAME="spec_date_m">';
		for($i=1; $i<13; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('spec_date_m') == $i ){ echo " SELECTED"; }
			echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="spec_date_d">';
		for($i=1; $i<32; $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('spec_date_d') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT> / <SELECT NAME="spec_date_y">';
		for($i=date("Y",$time); $i<(date("Y",$time)+6); $i++){
			echo '<OPTION VALUE="'.$i.'"';
			if( getval('spec_date_y') == $i ): echo " SELECTED"; endif;
			echo '>'.$i.'</OPTION>';
			}
		echo '</SELECT>';
	echo '</TD></TR>'."\n";
echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Is this the only date you are available?').'</TD><TD ALIGN="left"><SELECT NAME="spec_onlydateavl" ID="spec_onlydateavl" onChange="taltdates()"><OPTION VALUE="0"';
	if(getval('spec_onlydateavl') == 0): echo ' SELECTED'; endif;
	echo '>'.gettrans('No').'</OPTION><OPTION VALUE="1"';
	if(getval('spec_onlydateavl') == 1): echo ' SELECTED'; endif;
	echo '>'.gettrans('Yes').'</OPTION></SELECT></TD></TR>'."\n";

echo '<TR><TD STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;"><DIV ID="spec_showalt1"'; if(getval('spec_onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '>'.gettrans('If not, what alternate dates are you available?').'</DIV></TD><TD ALIGN="left"><DIV ID="spec_showalt2"'; if(getval('spec_onlydateavl') == 1): echo ' style="display:none;"'; endif; echo '><TEXTAREA NAME="spec_altdates" ID="spec_altdates" COLS="30" ROWS="2">'.getval('spec_altdates').'</TEXTAREA></DIV></TD></TR>'."\n";
	
echo '</TABLE><BR>'."\n\n";

echo '<INPUT TYPE="submit" VALUE="'.gettrans('Continue').' -&gt;" STYLE="width:180px;">';
	
echo '</FORM><BR>'."\n\n";

echo '<DIV STYLE="width:80%; font-family:Arial; font-size:10pt; text-align:left; color:#FF0000;">Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.<BR>We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.<BR>We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.</DIV><BR><BR>';

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="'.$_SERVER['PHP_SELF'].'">'.gettrans('Shopping Cart').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubashers.com';
	if(isset($tourinfo['alias']) && $tourinfo['alias'] != ""): echo '/tour.php?id='.urlencode( $tourinfo['alias'] ); endif;
	echo '">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">'.gettrans('Bundu Bus').'</A></B></FONT>'."\n\n";


} else {
	echo '<DIV STYLE="font-family:Arial; font-size:11pt; padding:20px;">The tour you have selected is not available to order at this time.<BR><BR>Please <A HREF="http://www.bundubashers.com/grand_canyon_tours_contact.php">Contact Us</A> so we can direct you to current tours that are similar.</DIV>';
} //End Num results If statement


//SHOPPING CART ***************************************************************************************************
} else {


//echo '<PRE>'; print_r($_POST); echo '</PRE>';

function getamount($item){
	global $SESSION;
	global $anchorprice;
	$amount = array('base'=>0,'amount'=>0);

	$query = 'SELECT * FROM `tours` WHERE `id` = "'.$item['typeid'].'" AND `archived` = 0 AND `hidden` != "1" LIMIT 1';
		$result = @mysql_query($query);
		$num_results = @mysql_num_rows($result);
		if(isset($num_results) && $num_results == 1){
			$tourinfo = mysql_fetch_assoc($result);

			$tourinfo['perguest'] = $anchorprice;
			$tourinfo['ext_pricing'] = '';
			$tourinfo['volstart'] = 0;
			$tourinfo['volrate'] = 0;

			$ext = array();
			$tourinfo['ext_pricing'] = explode('|',$tourinfo['ext_pricing']);
			foreach($tourinfo['ext_pricing'] as $val){
				$val = explode(':',$val);
				$ext[$val[0]] = $val[1];
				}

			if(isset($ext[$item['numguests']]) && $ext[$item['numguests']] > 0){
				$amount['amount'] = $ext[$item['numguests']];
				} elseif($tourinfo['volstart'] > 0 && $tourinfo['volrate'] > 0 && $item['numguests'] >= $tourinfo['volstart']){
				$amount['amount'] = ($item['numguests']*$tourinfo['volrate']);
				} else {
				$amount['amount'] = ($item['numguests']*$tourinfo['perguest']);
				}

			if($tourinfo['mincharge'] > 0 && $amount['amount'] < $tourinfo['mincharge']): $amount['amount'] = $tourinfo['mincharge']; endif;
			if($tourinfo['maxcharge'] > 0 && $amount['amount'] > $tourinfo['maxcharge']): $amount['amount'] = $tourinfo['maxcharge']; endif;

			if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
				if(isset($tourinfo['agent_perguest']) && $tourinfo['agent_perguest'] > 0){
					$amount['amount'] = ($item['numguests']*$tourinfo['agent_perguest']);
					} else {
					$per = ($_SESSION['agent']['discount'] / 100);
					$amount['amount'] = ($amount['amount']*$per);
					}
				}
			$amount['base'] = $amount['amount'];

			if(isset($item['optionid']) && is_array($item['optionid'])){
			foreach($item['optionid'] as $key => $val){ if(isset($item['addopt'.$val]) && $item['addopt'.$val] == "y"){
				$amount['amount'] = ($amount['amount'] + ($item['optionprice'][$key]*$item['optionpax'][$key]));
				}}
				}

			} //End Tour Num Results if statement

	return $amount;
	}

if(isset($_POST) && count($_POST) > 0){
	foreach($_POST as $key => $val){
		if(!is_array($val)){
		$_POST[$key] = strip_tags($_POST[$key]);
		$_POST[$key] = stripslashes($_POST[$key]);
		}
		}
	}

//********************************************** ADD ITEMS TO SHOPPING CART *********************************************
if(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid']) && $_POST['cartid'] == '*new*'){
	$getamount = getamount($_POST); $_POST['base'] = $getamount['base']; $_POST['amount'] = $getamount['amount'];
	array_push($_SESSION['cart_special'],$_POST);
	} elseif(isset($_POST['utaction']) && $_POST['utaction'] == "addtocart" && isset($_POST['cartid'])){
	$getamount = getamount($_POST); $_POST['base'] = $getamount['base']; $_POST['amount'] = $getamount['amount'];
	$_SESSION['cart_special'][$_POST['cartid']] = $_POST;
	} elseif(isset($_REQUEST['utaction']) && $_REQUEST['utaction'] == "remove" && $_REQUEST['cartid'] != ""){
	array_splice($_SESSION['cart_special'],$_REQUEST['cartid'],1);
	}
	//echo '<PRE>'; print_r($_SESSION['cart_special']); echo '</PRE>';


?><SCRIPT><!--

function checkexp(){
	var r = true;
	var msg = '';
	if(document.getElementById("cc_expdate_month").value == "" || document.getElementById("cc_expdate_year").value == ""){
		msg += '<? echo gettrans('Please choose an expiration date for your credit card.'); ?>'+"\n";
		r =  false;
		}
	if(document.getElementById("cc_zip").value == ""){
		msg += '<? echo gettrans('Please enter the zip or postal code associated with your credit card.'); ?>'+"\n";
		r =  false;
		}
	if(document.getElementById("email").value != document.getElementById("email2").value){
		msg += '<? echo gettrans('Email Address and Re-type Email Address do not match.  Please check carefully to be sure you have entered the correct email address in both places.'); ?>'+"\n";
		r =  false;
		}
	if(msg != ""){ alert(msg); }
	return r;
	}

function rmvitem(x){
	if( confirm('<? echo gettrans('Remove this item from your shopping cart?'); ?>') ){
		window.location = '<? echo $_SERVER['PHP_SELF']; ?>?utaction=remove&cartid='+x;
		}
	}

// --></SCRIPT><? echo "\n\n";


echo '<DIV STYLE="font-family:Arial; font-size:20pt; padding-top:8px; padding-bottom:6px;">'.gettrans('Shopping Cart').'</DIV>'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:80%;">'."\n";

	bgcolor('reset');
	if(count($_SESSION['cart_special']) > 0){

	echo "\t".'<TR>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Details</TD>';
		echo '<TD STYLE="padding:4px; padding-left:0px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left;">Options/Add Ons</TD>';
		echo '<TD STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:right;">Totals</TD>';
		echo '<TD STYLE="background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333;">&nbsp;</TD>';
		echo '</TR>'."\n";

	$TOTALCOST = 0;
	foreach($_SESSION['cart_special'] as $id => $row){

		$date = mktime(0,0,0,$row['date_m'],$row['date_d'],$row['date_y']);
		if($row['onlydateavl'] > 0): $onyldateavl = gettrans('Yes'); else: $onyldateavl = gettrans('No'); endif;

		if(!isset($row['amount']) || $row['amount'] == "" || $row['amount'] == 0){
			$getamount = getamount($row); $_SESSION['cart_special'][$id]['base'] = $getamount['base']; $_SESSION['cart_special'][$id]['amount'] = $getamount['amount'];
			$row['amount'] = $getamount['amount'];
			}

		$rowbg = bgcolor('');
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="4" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['title'].'</TD></TR>'."\n";
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'">';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
				echo gettrans('Date').': '.gettrans(date("l",$date)).', '.date("n/j/Y",$date).'<BR>';
				if(isset($row['altdates']) && trim($row['altdates']) != ""): echo gettrans('Alternate dates').': '.trim($row['altdates']).'<BR>'; endif;
				echo gettrans('Guests').': '.$row['numguests'].'<BR>';
				echo gettrans('Base price').': $'.number_format($row['base'],2,'.','').'<BR>';
				echo gettrans('Pick Up').': '.$row['dep_loc'].'<BR>'.gettrans('Drop Off').': '.$row['arr_loc'].'<BR>';
				if(isset($row['preftime'])): echo gettrans('Preferred time of day').': '.$row['preftime'].'<BR>'; endif;
				echo '</TD>';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
			if(isset($row['optionid']) && is_array($row['optionid'])){
			foreach($row['optionid'] as $key => $val){ if(isset($row['addopt'.$val]) && $row['addopt'.$val] == "y"){
				echo '<DIV>'.$row['optionname'][$key].':</DIV><DIV STYLE="padding-bottom:4px; text-align:right;">+ $'.$row['optionprice'][$key].' x '.$row['optionpax'][$key].' '.gettrans('guest(s)').' = <B>$'.number_format( ($row['optionprice'][$key]*$row['optionpax'][$key]),2,'.','').'</B></DIV>';
				}}
				}
				echo '</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($row['amount'],2,'.','').'</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; padding-right:4px;">';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
			echo '</TR>'."\n";

		$spec_price = ($specialprice * $row['numguests']);
		$date = mktime(0,0,0,$row['spec_date_m'],$row['spec_date_d'],$row['spec_date_y']);
		if($row['spec_onlydateavl'] > 0): $onyldateavl = gettrans('Yes'); else: $onyldateavl = gettrans('No'); endif;

		$rowbg = bgcolor('');
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'"><TD ALIGN="left" VALIGN="top" COLSPAN="4" STYLE="padding:8px; padding-bottom:0px; font-family:Arial; font-size:10pt; font-weight:bold;">'.$row['specialtitle'].'</TD></TR>'."\n";
		echo "\t".'<TR BGCOLOR="#'.$rowbg.'">';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-top:2px; font-family:Arial; font-size:9pt;">';
				echo gettrans('Date').': '.gettrans(date("l",$date)).', '.date("n/j/Y",$date).'<BR>';
				if(isset($row['spec_altdates']) && trim($row['spec_altdates']) != ""): echo gettrans('Alternate dates').': '.trim($row['spec_altdates']).'<BR>'; endif;
				echo gettrans('Guests').': '.$row['numguests'].'<BR>';
				echo '</TD>';
			echo '<TD ALIGN="left" VALIGN="top" STYLE="padding:2px; padding-bottom:4px; padding-right:10px; font-family:Arial; font-size:9pt;">';
				echo '</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; font-family:Arial; font-size:11pt; font-weight:bold;">$'.number_format($spec_price,2,'.','').'</TD>';
			echo '<TD ALIGN="right" VALIGN="bottom" STYLE="padding:8px; padding-left:2px; padding-top:2px; padding-right:4px;">';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Change').'" STYLE="font-size:11px; width:60px;" onClick="window.location=\''.$_SERVER['PHP_SELF'].'?'.$row['type'].'='.$row['typeid'].'&edit='.$id.'\'"><BR>';
				echo '<INPUT TYPE="button" VALUE="'.gettrans('Remove').'" STYLE="font-size:11px; width:60px;" onClick="rmvitem(\''.$id.'\');">';
				echo '</TD>';
			echo '</TR>'."\n";

		$TOTALCOST = ($TOTALCOST+$row['amount']+$spec_price);
		} //End ForEach Loop

		echo "\t".'<TR BGCOLOR="#CCCCCC"><TD ALIGN="right" COLSPAN="3" STYLE="padding:4px; padding-right:8px; font-family:Arial; font-size:11pt; font-weight:bold;">'.gettrans('Total').':&nbsp;&nbsp;$'.number_format($TOTALCOST,2,'.','').'</TD><TD>&nbsp;</TD></TR>'."\n";

		echo "\t".'<TR><TD ALIGN="center" COLSPAN="4"><FONT FACE="Arial" SIZE="2"><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><I>-'.gettrans('or').'-</I><BR>'.gettrans('Submit payment below to order listed reservations.').'</FONT></TD></TR>'."\n";

		echo '</TABLE><BR>'."\n\n";


if(isset($_SESSION['bb_member']) && count($_SESSION['bb_member']) > 0){
	$fillform = $_SESSION['bb_member'];
	$fillform['name'] = $_SESSION['bb_member']['firstname'].' '.$_SESSION['bb_member']['lastname'];
	}

echo '<FORM NAME="resform" METHOD="post" ACTION="'.$_SERVER['PHP_SELF'].'"';
	if(!isset($_SESSION['agent']['id']) || $_SESSION['agent']['id'] == ""){ echo ' onSubmit="javascript:return checkexp();"'; }
	echo '>'."\n";
echo '<INPUT TYPE="hidden" NAME="form" ID="form" VALUE="process">'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="4" STYLE="width:80%;">'."\n";

	echo "\t".'<TR><TD COLSPAN="2" STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:center;">'.gettrans('Contact').' / '.gettrans('Payment Information').'</TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Name').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('As it appears on your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="name" VALUE="'.getval('name').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Home/Business Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_homebus" VALUE="'.getval('phone_homebus').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Phone').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="phone_cell" VALUE="'.getval('phone_cell').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Cell Country').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="cell_country" VALUE="'.getval('cell_country').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email" ID="email" VALUE="'.getval('email').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Re-type Email Address').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="email2" ID="email2" VALUE="'.getval('email2').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	if(isset($_SESSION['agent']['id']) && $_SESSION['agent']['id'] != ""){
		echo "\t".'<TR><TD ALIGN="right" WIDTH="'.$leftcol.'"><INPUT TYPE="checkbox" NAME="altpaymethod" ID="altpaymethod" VALUE="Alternate"></TD><TD><FONT FACE="Arial" SIZE="2"><LABEL FOR="altpaymethod">'.gettrans('I will submit payment in the form of wire transfer or alternate (skip credit card fields below).').'</LABEL></FONT></TD></TR>'."\n";
		}

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Credit card number').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('We accept Visa, MasterCard and American Express.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="30" NAME="cc_num" VALUE="'.getval('cc_num').'" STYLE="width:300px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Expiration date').'</TD><TD>';
		echo '<SELECT NAME="cc_expdate_month" ID="cc_expdate_month">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=1; $i<13; $i++){
				echo '<OPTION VALUE="'.$i.'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("n",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.gettrans(date("F",mktime("0","0","0",$i,"1","2005"))).'</OPTION>';
				}
			echo '</SELECT> / <SELECT NAME="cc_expdate_year" ID="cc_expdate_year">';
			echo '<OPTION VALUE="">Choose...</OPTION>';
			for($i=date("Y",$time); $i<(date("Y",$time)+11); $i++){
				echo '<OPTION VALUE="'.date("y",mktime("0","0","0","1","1",$i)).'"';
				if( isset($_SESSION['reginfo']['cc_expdate']) && date("Y",$_SESSION['reginfo']['cc_expdate']) == $i ){ echo " SELECTED"; }
				echo '>'.$i.'</OPTION>';
				}
			echo '</SELECT></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Security code').'</TD><TD><INPUT TYPE="text" SIZE="5" NAME="cc_scode" VALUE="'.getval('cc_scode').'" STYLE="width:50px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Zip/Postal code').'<BR><SPAN STYLE="font-size:8pt; font-weight:normal; color:#666666;">'.gettrans('Please provide the zip or postal code associated with your credit card.').'</SPAN></TD><TD><INPUT TYPE="text" SIZE="8" NAME="cc_zip" ID="cc_zip" VALUE="'.getval('cc_zip').'" STYLE="width:80px;"></TD></TR>'."\n";
	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.wordwrap(gettrans('If the card holder will not be there, provide the name of one of the guests'),40,'<BR>').'</TD><TD><INPUT TYPE="text" SIZE="30" NAME="alt_name" VALUE="'.getval('alt_name').'" STYLE="width:300px;"></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="right" STYLE="font-family:Arial; font-size:10pt; font-weight:bold; text-align:right; padding-right:10px;">'.gettrans('Comments').'</TD><TD ALIGN="left" WIDTH="'.$rightcol.'"><TEXTAREA NAME="comments" COLS="30" ROWS="3" STYLE="width:300px; height:80px;">'.getval('comments').'</TEXTAREA></TD></TR>'."\n";

	echo "\t".'<TR><TD COLSPAN="2" ALIGN="center"><FONT SIZE="1">&nbsp;</FONT></TD></TR>'."\n";

	echo "\t".'<TR><TD ALIGN="center" COLSPAN="2"><INPUT TYPE="submit" NAME="submit" VALUE="'.gettrans('Reserve').'" STYLE="width:180px;"></FORM></TD></TR>'."\n";

	} else {
		echo "\t\t".'<TR><TD ALIGN="center" COLSPAN="5"><FONT FACE="Arial" SIZE="2"><BR>'.gettrans('There are currently no reservations in your shopping cart.').'<BR><BR><B><A HREF="'.$_SESSION['continue'].'">'.gettrans('Continue Shopping').'</A></B><BR><BR></FONT></TD></TR>'."\n";
	}

echo '</TABLE><BR><BR>'."\n\n";

echo '<DIV STYLE="width:80%; font-family:Arial; font-size:10pt; text-align:left; color:#FF0000;">Please note that your reservation is not guaranteed until you have received a detailed confirmation email from us.<BR>We strongly advise you against making any plans that are dependent on this reservation until you receive a confirmation email.<BR>We reserve the right to switch the dates of your tours. This order is subject to availability.  If there is no availability on either of your dates this order will be canceled and your credit card will not be charged.</DIV><BR><BR>';

echo '<HR SIZE="1" COLOR="#999999">'."\n\n";
echo '<FONT FACE="Arial" SIZE="2"><B>'.gettrans('Return to:').'&nbsp;&nbsp;<A HREF="http://www.bundubashers.com">'.gettrans('Bundu Bashers Tours').'</A>&nbsp;&nbsp;|&nbsp;&nbsp;<A HREF="http://www.bundubus.com">Bundu Bus</A></B></FONT>'."\n\n";


} //End Tour/Shopping Cart If Statement

echo '<DIV STYLE="font-family:Arial; font-size:8pt;">'.gettrans('Bundu Bashers Tours will not be bound by any prices that are generated maliciously or in error, and that are not its regular prices as detailed on its web sites.').'</DIV>'."\n\n";

echo '</CENTER>'."\n\n";


include('footer.php');

?>