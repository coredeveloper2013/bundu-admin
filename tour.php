<?

require_once('common.inc.php');

//$_REQUEST['id'] = 1205; echo 'REMOVE THIS LINE!!!';

@ini_set("session.gc_maxlifetime","10800");
if(!isset($_SESSION)){ session_start();	}


if(!isset($_SESSION['lang']) || $_SESSION['lang'] == ""){ @include('findlang.php'); }
if(!isset($_SESSION['lang']) || $_SESSION['lang'] == ""){ $_SESSION['lang'] = 1; }
if(isset($_REQUEST['lang']) && $_REQUEST['lang'] != ""){ $_SESSION['lang'] = $_REQUEST['lang']; }

if(isset($_REQUEST['id']) && $_REQUEST['id'] != ""){
	$query = 'SELECT * FROM `tours` WHERE (`alias` = "'.$_REQUEST['id'].'" OR `id` = "'.$_REQUEST['id'].'") AND `hidden` != "1" LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
	}

if(isset($num_results) && $num_results > 0){
$tourinfo = mysql_fetch_assoc($result);

	$tourinfo['safetitle'] = $tourinfo['title']; //.' ('.$tourinfo['id'].')';

	//CHECK FOR PREFERRED LANGUAGE
	if($_SESSION['lang'] != "1"){
	$query = 'SELECT * FROM `tours_translations` WHERE `tourid` = "'.$tourinfo['id'].'" AND `lang` = "'.$_SESSION['lang'].'" AND CONCAT(tours_translations.`title`,tours_translations.`eztitle`,tours_translations.`short_desc`,tours_translations.`highlights`,tours_translations.`details`,tours_translations.`youtube_title`,tours_translations.`pleasenote`,tours_translations.`toptitle`,tours_translations.`topdesc`) != "" ORDER BY `id` DESC LIMIT 1';
	//echo $query."<BR>\n";
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		if($num_results > 0){
		$langinfo = mysql_fetch_assoc($result);
		if(trim($langinfo['title']) != ""){ $tourinfo['title'] = $langinfo['title']; }
		if(trim($langinfo['short_desc']) != ""){ $tourinfo['short_desc'] = $langinfo['short_desc']; }
		if(trim($langinfo['pricingdesc']) != ""){ $tourinfo['pricingdesc'] = $langinfo['pricingdesc']; }
		if(trim($langinfo['highlights']) != ""){ $tourinfo['highlights'] = $langinfo['highlights']; }
		if(trim($langinfo['details']) != ""){ $tourinfo['details'] = $langinfo['details']; }
		if(trim($langinfo['youtube_title']) != ""){ $tourinfo['youtube_title'] = $langinfo['youtube_title']; }
		if(trim($langinfo['pleasenote']) != ""){ $tourinfo['pleasenote'] = $langinfo['pleasenote']; }
		} else {
		$langinfo = 'A translation in your preferred language is not available for this tour.';
		}
	}

if(isset($tourinfo['meta_title']) && trim($tourinfo['meta_title']) != ""){
	$pagetitle = $tourinfo['meta_title'];
	} else {
	$pagetitle = $tourinfo['title'];
	}
if(isset($tourinfo['meta_desc']) && trim($tourinfo['meta_desc']) != ""){
	$metadesc = $tourinfo['meta_desc'];
	} else {
	$metadesc = $tourinfo['short_desc'];
	}
include_once('header.php');

echo '<SCRIPT><!--

obhl = "url(img/btn_orderbackhl.jpg)";
ob = "url(img/btn_orderback.jpg)";

function btnhl(obj,s){
	if(s == 1){
		obj.style.backgroundImage = obhl;
		} else {
		obj.style.backgroundImage = ob;
		}
	}

// --></SCRIPT>'."\n\n";

//GET IMAGES
$images = array();
$query = 'SELECT images.`filename`, images.`caption` FROM `images`,`images_assoc_tours` WHERE images_assoc_tours.`imgid` = images.`id` AND images_assoc_tours.`tourid` = "'.$tourinfo['id'].'" ORDER BY images_assoc_tours.`order` ASC';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($images,$row);
	}

//GET ACTIVITIES
$tourinfo['options'] = array();
$query = 'SELECT DISTINCT tours_assoc.`tourid`, activities.`id` AS `optionid`, activities.`name`, activities.`price`, tours_assoc.`day`, tours_assoc.`order`';
	$query .= ' FROM `tours_assoc`,`activities`';
	$query .= ' WHERE tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "f" AND tours_assoc.`type` = "a" AND tours_assoc.`typeid` = activities.`id`';
	$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC';
	//echo $query.'<BR>';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	array_push($tourinfo['options'],$row);
	}

//GET LODGING EXTENSTIONS
$tourinfo['extlodging'] = array();
$query = 'SELECT DISTINCT tours_assoc.`tourid`, tours_extlodging.`id` AS `extlodgeid`, tours_extlodging.`name`, tours_extlodging.`perguest`, tours_extlodging.`single`, tours_extlodging.`triple`, tours_extlodging.`quad`, tours_extlodging.`vendor`, tours_assoc.`order`';
	$query .= ' FROM `tours_assoc`,`tours_extlodging`';
	$query .= ' WHERE tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "f" AND tours_assoc.`type` = "e" AND tours_assoc.`typeid` = tours_extlodging.`id`';
	$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC';
	//echo $query.'<BR>';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	//array_push($tourinfo['extlodging'],$row);
	$tourinfo['extlodging']['e'.$row['extlodgeid']] = $row;
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($tourinfo['extlodging']); echo '</PRE>';

//GET SIMILAR TOURS
$tourinfo['simtours'] = array();
$query = 'SELECT tours.`id`,tours.`alias`,tours.`title`';
	if($_SESSION['lang'] != "1"): $query .= ', tours_translations.`title` AS `trans_title`'; endif;
	$query .= ' FROM (`tours` LEFT JOIN `tours_similar` ON tours.`id` = tours_similar.`simid`)';
	if($_SESSION['lang'] != "1"): $query .= ' LEFT JOIN `tours_translations` ON tours.`id` = tours_translations.`tourid` AND tours_translations.`lang` = "'.$_SESSION['lang'].'"'; endif;
	$query .= ' WHERE tours_similar.`tourid` = "'.$tourinfo['id'].'" AND tours.`hidden` != "1"';
	$query .= ' ORDER BY tours_similar.`order` ASC, tours.`order_weight` ASC, tours.`numdays` ASC, tours.`perguest` ASC, tours.`title` ASC';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
if($num_results > 0){
	for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		if(isset($row['trans_title']) && $row['trans_title'] != ""): $row['title'] = $row['trans_title']; endif;
		array_push($tourinfo['simtours'],$row);
		}
	}

function fromsecs($secs=0){
	$out = array('h'=>0,'m'=>0,'s'=>0);
	$out['h'] = intval($secs / 3600);
		$secs_left = ($secs - ($out['h'] * 3600));
	$out['m'] = intval($secs_left / 60);
	$out['s'] = ($secs_left - ($out['m'] * 60));
	return $out;
	}

function gen_ithead($row,$curroute,$curday){
	//echo '<PRE STYLE="text-align:left;">'; print_r($row); echo '</PRE>';
	$headline = array();
	if(!isset($row['routeid'])){ $row['routeid'] = ''; }
	if(trim($row['title']) != ""){
		array_push($headline,$row['title']);
		} elseif($curroute != $row['routeid'] && trim($row['route_name']) != ""){
		array_push($headline,$row['route_name']);
		}
	$dist = "";
	if($curroute != $row['routeid'] && $row['ml'] == 0){
		$row['ml'] = $row['route_miles'];
		}
	if($row['ml'] != "" && $row['ml'] > 0){
		$dist .= round($row['ml'],2).' mi';
		if($row['ml'] != "" && $row['ml'] > 0): $dist .= ' / '; endif;
		}
	if($row['ml'] != "" && $row['ml'] > 0): $dist .= round(($row['ml']*1.609),2).' km'; endif;
	if(trim($dist) != ""): array_push($headline,trim($dist)); endif;
	if($row['type'] == "r" && $curroute != $row['routeid'] && $row['time'] === ""){
		//$offset = gettimediff($row['dep_loc'],$row['arr_loc']);
		//$traveltime = ($row['arr_time'] - $row['dep_time'] + $offset);
		$timedesc = fromsecs($row['travel_time']);
			$row['time'] = '';
			if($timedesc['h'] > 0 || $timedesc['m'] > 0){ $row['time'] .= gettrans('About').' '; }
			if($timedesc['h'] > 0){ $row['time'] .= $timedesc['h'].' '.gettrans('hours'); }
			if($timedesc['h'] > 0 && $timedesc['m'] > 0){ $row['time'] .= ', '; }
			if($timedesc['m'] > 0){ $row['time'] .= $timedesc['m'].' '.gettrans('minutes'); }
		}
	if(trim($row['time']) != ""){
		if(is_int($row['time'])){
			$addtotime = 'hour';
			if($row['time'] > 1){ $addtotime .= 's'; }
			$row['time'] .= ' '.gettrans($addtotime);
			}
		array_push($headline,$row['time']);
		}
	if(count($headline) > 0 && is_numeric($row['day']) && $row['day'] > 0){
		array_unshift($headline,gettrans('Day').' '.$row['day']);
		} elseif(count($headline) == 0 && $curday != $row['day']){
		array_push($headline,gettrans('Day').' '.$row['day']);
		}
	//echo '<!-- headline '.implode(' &nbsp;-&nbsp; ',$headline).' -->'."\n";
	return implode(' &nbsp;-&nbsp; ',$headline);
	}



if(!isset($_REQUEST['direction'])): $_REQUEST['direction'] = 'f'; endif;

//GET ITINERARY
$itinerary = array();
$it_ids = array();
if($tourinfo['archived'] > 0){
	$query = 'SELECT itinerary_archive.*, "" as `type`';
		if($_SESSION['lang'] != "1" && is_array($langinfo)): $query .= ', itinerary_archive_translations.`title` AS `trans_title`, itinerary_archive_translations.`time` AS `trans_time`, itinerary_archive_translations.`comments` AS `trans_comments`, itinerary_archive_translations.`youtube_title` AS `trans_youtube_title`'; endif;
		$query .= ' FROM `itinerary_archive`';
		if($_SESSION['lang'] != "1" && is_array($langinfo)): $query .= ' LEFT JOIN `itinerary_archive_translations` ON itinerary_archive_translations.`itid` = itinerary_archive.`id` AND itinerary_archive_translations.`lang` = "'.$_SESSION['lang'].'"'; endif;
		$query .= ' WHERE `tourid` = "'.$tourinfo['id'].'"';
		$query .= 'ORDER BY itinerary_archive.`step` ASC';
} else {
	/*$query = 'SELECT tours_assoc.`id` AS `associd`, tours_assoc.`tourid`, tours_assoc.`type`, tours_assoc.`day`, tours_assoc.`order` AS `step`, tours_assoc.`content`, routes.`id` AS `routeid`, routes.`name` AS `route_name`, routes.`miles` AS `route_miles`, routes.`dep_loc`, routes.`dep_time`, routes.`arr_loc`, routes.`arr_time`, tours_assoc.`adjust`, tours_assoc.`day`, tours_assoc.`order`, itinerary.`id`, itinerary.`title`, itinerary.`map`, itinerary.`ml`, itinerary.`time`, itinerary.`comments`';
		$query .= ' FROM (`tours_assoc` LEFT JOIN `routes` ON tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id`)';
		$query .= ' LEFT JOIN `itinerary` ON itinerary.`routeid` = routes.`id`';
		$query .= ' WHERE (tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "'.$_REQUEST['direction'].'")';
		$query .= ' AND ((tours_assoc.`type` = "r" AND itinerary.`id` > 0) OR (tours_assoc.`type` = "i" AND tours_assoc.`content` != ""))';
		$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC, itinerary.`step` ASC';*/
	$query = 'SELECT';
		$query .= ' tours_assoc.`id` AS `associd`, tours_assoc.`tourid`, tours_assoc.`day`, tours_assoc.`order` AS `step`, tours_assoc.`type`, tours_assoc.`typeid`, tours_assoc.`adjust`';
		$query .= ' , routes_assoc.`order` AS `itorder`';
		$query .= ' , itinerary.`id` AS `id`, itinerary.`title`, itinerary.`map`, itinerary.`ml`, itinerary.`time`, itinerary.`comments`, itinerary.`youtube_title`, itinerary.`youtube`';
		$query .= ' , routes.`id` AS `routeid`, routes.`name` AS `route_name`, routes.`miles` AS `route_miles`, routes.`dep_loc`, routes.`dep_time`, routes.`arr_loc`, routes.`arr_time`, routes.`travel_time`';
		$query .= ' FROM `tours_assoc`';
		$query .= ' LEFT JOIN `routes_assoc` ON tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes_assoc.`routeid`';
		$query .= ' LEFT JOIN `itinerary` ON (tours_assoc.`type` = "r" AND routes_assoc.`itid` = itinerary.`id`)';
			$query .= ' OR (tours_assoc.`type` = "i" AND tours_assoc.`typeid` = itinerary.`id`)';
		$query .= ' LEFT JOIN `routes` ON tours_assoc.`type` = "r" AND tours_assoc.`typeid` = routes.`id`';
		$query .= ' WHERE tours_assoc.`dir` = "f"';
		$query .= ' AND tours_assoc.`tourid` = "'.$tourinfo['id'].'"';
		$query .= ' AND (tours_assoc.`type` = "r" OR tours_assoc.`type` = "i" OR tours_assoc.`type` = "e")';
		$query .= ' ORDER BY tours_assoc.`day` ASC, tours_assoc.`order` ASC, routes_assoc.`order` ASC';
}
if($_SERVER['REMOTE_ADDR'] == "76.103.138.139"){
	//echo $query.'<BR>';
}
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	$curroute = 0; $curday = 1;
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(isset($row['trans_title']) && $row['trans_title'] != ""){ $row['title'] = $row['trans_title']; }
	if(isset($row['trans_time']) && $row['trans_time'] != ""){ $row['time'] = $row['trans_time']; }
	if(isset($row['trans_comments']) && $row['trans_comments'] != ""){ $row['comments'] = $row['trans_comments']; }
	if(isset($row['trans_youtube_title']) && $row['trans_youtube_title'] != ""){ $row['youtube_title'] = $row['trans_youtube_title']; }
	if(!isset($row['routeid'])){ $row['routeid'] = ''; }
	$row['headline'] = gen_ithead($row,$curroute,$curday);
	$curroute = $row['routeid'];
	$curday = $row['day'];
	if($row['id'] != NULL){ array_push($it_ids,$row['id']); }
	array_push($itinerary,$row);
	}
	//echo '<PRE STYLE="text-align:left;">'; print_r($itinerary); echo '</PRE>';

//GET ITIMAGES


//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($it_ids,true)).'</PRE>';


$images_asoc = array();
if($tourinfo['archived'] > 0){
	$query = 'SELECT images_assoc_itinerary_archive.* FROM `images_assoc_itinerary_archive`,`itinerary_archive`
				WHERE images_assoc_itinerary_archive.`imgid` > 0
					AND images_assoc_itinerary_archive.`itid` = itinerary_archive.`id`
						AND (images_assoc_itinerary_archive.`itid` = "'.implode('" OR images_assoc_itinerary_archive.`itid` = "',$it_ids).'")
					ORDER BY itinerary_archive.`id` ASC, images_assoc_itinerary_archive.`order` ASC';
} else {
	$query = 'SELECT images_assoc_itinerary.* FROM images_assoc_itinerary, itinerary
				WHERE images_assoc_itinerary.`imgid` > 0 AND images_assoc_itinerary.`itid` = itinerary.`id`'."\n";
				if(count($it_ids) > 0){
					$query .= '	AND images_assoc_itinerary.`itid` IN ("'.implode('", "', $it_ids).'")'."\n";
				}
				$query .= 'ORDER BY itinerary.`id` ASC, images_assoc_itinerary.`order` ASC';
}

$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($images_asoc['i'.$row['itid']])){ $images_asoc['i'.$row['itid']] = array(); }
	array_push($images_asoc['i'.$row['itid']],$row['imgid']);
	}
$itimages = array();

if($_SERVER['REMOTE_ADDR'] == "76.103.138.139"){
	//echo $query.'<BR>';
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($images_asoc,true)).'</PRE>';
}

//Collect itinerary maps
$map_ids = array();
if($tourinfo['archived'] > 0){
	$query = 'SELECT DISTINCT map
		FROM itinerary_archive
		WHERE id IN ("'.implode('", "', $it_ids).'")
			AND map <> ""';
} else {
	$query = 'SELECT DISTINCT map
		FROM itinerary
		WHERE id IN ("'.implode('", "', $it_ids).'")
			AND map <> ""';
}
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$map_ids[] = $row['map'];
	}
	//echo '<PRE STYLE="text-align:left;">'.htmlentities(print_r($map_ids,true)).'</PRE>';

//Collect other itinerary images
if($tourinfo['archived'] > 0){
	$query = 'SELECT DISTINCT images.*
				FROM `images`
				LEFT JOIN `images_assoc_itinerary_archive` ON images.`id` = images_assoc_itinerary_archive.`imgid`
				#LEFT JOIN `itinerary_archive` ON images_assoc_itinerary_archive.`itid` = itinerary_archive.`id`
				WHERE images_assoc_itinerary_archive.`itid` IN ("'.implode('", "', $it_ids).'")';
					if(count($map_ids) > 0){
						$query .= '	OR images.id IN ("'.implode('", "', $map_ids).'")';
					}
					/*(
						images.`id` = images_assoc_itinerary_archive.`imgid` AND images_assoc_itinerary_archive.`itid` = itinerary_archive.`id`
						AND itinerary_archive.`id` IN ("'.implode('", "', $it_ids).'")
						)
					OR
						(
						images.`id` = itinerary_archive.`map`
						AND itinerary_archive.`id` IN ("'.implode('", "', $it_ids).'")
						)';*/
	if($_SERVER['REMOTE_ADDR'] == "76.103.138.139"){
		//echo $query.'<BR>';
	}

} else {
	$query = 'SELECT DISTINCT images.*
				FROM images
				LEFT JOIN images_assoc_itinerary ON images.`id` = images_assoc_itinerary.`imgid`
				#JOIN itinerary ON images_assoc_itinerary.`itid` = itinerary.`id`
		WHERE images_assoc_itinerary.`itid` IN ("'.implode('", "', $it_ids).'")'."\n";
			if(count($map_ids) > 0){
				$query .= '	OR images.id IN ("'.implode('", "', $map_ids).'")';
			}
			//(itinerary.`id` = "'.implode('" OR itinerary.`id` = "',$it_ids).'"))
			//OR (images.`id` = itinerary.`map` AND (itinerary.`id` = "'.implode('" OR itinerary.`id` = "',$it_ids).'")';
}
//echo '<PRE STYLE="text-align:left;">'.$query.'</PRE>';
$result = @mysql_query($query);
$num_results = @mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	$itimages['i'.$row['id']] = $row;
	$itimages['i'.$row['id']]['data'] = imgform($row['filename'],180,90);
	}
	//echo '<PRE>'; print_r($itimages); echo '</PRE>';
	//echo count($itimages).'<BR>';

//GET ITTRANSLATIONS
if($_SESSION['lang'] != "1" && $tourinfo['archived'] == 0){
$ittranslations = array();
	$query = 'SELECT DISTINCT * FROM `itinerary_translations` WHERE `lang` = "'.$_SESSION['lang'].'" AND (`itid` = "'.implode('" OR `itid` = "',$it_ids).'")';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$ittranslations['r'.$row['itid']] = $row;
		}

	/*$query = 'SELECT tours_assoc.`id` AS `associd`, tours_assoc.`content`, trans_translations.`translation` AS `trans_content` FROM `tours_assoc`,`trans_static`,`trans_translations` WHERE trans_translations.`language` = "'.$_SESSION['lang'].'" AND tours_assoc.`tourid` = "'.$tourinfo['id'].'" AND tours_assoc.`dir` = "'.$_REQUEST['direction'].'" AND tours_assoc.`type` = "i" AND tours_assoc.`content` != "" AND tours_assoc.`content` = trans_static.`english` AND trans_static.`id` = trans_translations.`staticid` AND trans_translations.`translation` != "" ORDER BY tours_assoc.`id` ASC';
	$result = @mysql_query($query);
	$num_results = @mysql_num_rows($result);
		for($i=0; $i<$num_results; $i++){
		$row = mysql_fetch_assoc($result);
		$ittranslations['i'.$row['associd']] = $row;
		}*/
	}


if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
	$per = ($_SESSION['agent']['discount'] / 100);
		if(isset($tourinfo['agent'.$_SESSION['agent']['tier'].'_per']) && $tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] > 0){
			$per = ($tourinfo['agent'.$_SESSION['agent']['tier'].'_per'] / 100);
			}
	$tourinfo['perguest'] = ($tourinfo['perguest']*$per);
	}
	
$orderlink = 'https://www.bundubashers.com'.dev_root.'/reserve.php?t='.$tourinfo['id'];
	$orderlink .= '&lang='.$_SESSION['lang'];
	if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
		$orderlink .= '&agent='.urlencode($_SESSION['agent']['id']);
		//$orderlink .= '&'.session_name().'='.session_id();
		}

echo '<BR>'."\n\n";

if($tourinfo['archived'] > 0 && $tourinfo['cur_tour'] > 0){
	$query = 'SELECT `alias`,`title` FROM `tours` WHERE `id` = "'.$tourinfo['cur_tour'].'" LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results == 1){
		$cur_tour = mysql_fetch_assoc($result);

		echo '<TABLE BORDER="0" WIDTH="100%" CELLSPACING="2" CELLPADDING="4" BGCOLOR="#CCCCFF">';
		echo '<TR><TD ALIGN="center"><FONT FACE="Arial" SIZE="3"><B><I>';
			echo '<A HREF="tour.php?id='.$cur_tour['alias'].'">';
				if(isset($tourinfo['cur_link']) && trim($tourinfo['cur_link']) != ""){
					echo gettrans(trim($tourinfo['cur_link']));
					} else {
					echo gettrans('There is a new and much better version of this tour!');
					}
				echo '</A>';
			echo '</I></B></FONT></TD></TR>';
		echo '</TABLE><BR>'."\n\n";
		}
	}

if(isset($langinfo) && !is_array($langinfo) && $langinfo != ""): echo '<TABLE BORDER="0" WIDTH="100%" CELLSPACING="2" CELLPADDING="4" BGCOLOR="#CCCCFF"><TR><TD ALIGN="center"><FONT FACE="Arial" SIZE="2"><B><I>'.gettrans($langinfo).'</I></B></FONT></TD></TR></TABLE><BR>'."\n\n"; endif;


//GET TOUR DATES
$dates = array();
if($tourinfo['archived'] == "0"){
$query = 'SELECT * FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" AND `date` > '.$time.' ORDER BY `date` ASC';
$result = mysql_query($query);
$num_results = mysql_num_rows($result);
	for($i=0; $i<$num_results; $i++){
	$row = mysql_fetch_assoc($result);
	if(!isset($dates[date("Y",$row['date'])])){ $dates[date("Y",$row['date'])] = array(); }
	if(!isset($dates[date("Y",$row['date'])][date("n",$row['date'])])){ $dates[date("Y",$row['date'])][date("n",$row['date'])] = array(); }
	$dates[date("Y",$row['date'])][date("n",$row['date'])][date("j",$row['date'])] = 1;
	//echo date("n/j/Y g:ia",$row['date']).'<BR>';
	//array_push($dates,$row);
	}
}

//CALENDAR FUNCTIONS
	//Find soonest upcoming month
	$query = 'SELECT `date` FROM `tours_dates` WHERE `tourid` = "'.$tourinfo['id'].'" AND `date` > '.$time.' ORDER BY `date` ASC LIMIT 1';
	$result = mysql_query($query);
	$num_results = mysql_num_rows($result);
	if($num_results > 0){
	$soonest = mysql_fetch_assoc($result);
	$soonest = $soonest['date'];
	$calfirst = mktime(0,0,0,date("n",$soonest),1,date("Y",$soonest));
	} else {
	$calfirst = mktime(0,0,0,date("n",$time),1,date("Y",$time));
	}
	$calendar = array(
		"first" => $calfirst,
		"start" => date("w",$calfirst),
		"days" => date("t",$calfirst),
		"m" => date("n",$calfirst),
		"y" => date("Y",$calfirst)
		);
$calw = (7*28);

?><SCRIPT><!--

var calw = <? echo $calw; ?>;

var bg = 'DDDDDD';
function bgcolor(){
	if(bg == "FFFFFF"){ bg = "DDDDDD"; } else { bg = "FFFFFF"; }
	return bg;
	}

var cal = new Array();
<? for($y=date("Y",$time); $y<(date("Y",$time)+3); $y++){
	echo "\t".'cal['.$y.'] = new Array();';
		for($m=1; $m<13; $m++){
		$first = mktime(0,0,0,$m,1,$y);
		echo ' cal['.$y.']['.$m.'] = new Array('.date("w",$first).','.date("t",$first).');';
		}
		echo "\n";
	} ?>

var runs = new Array();
<? foreach($dates as $y => $years){
	echo "\t".'runs['.$y.'] = new Array();'."\n";
	foreach($years as $m => $months){
		echo "\t".'runs['.$y.']['.$m.'] = new Array();';
		foreach($months as $d => $day){
			echo ' runs['.$y.']['.$m.']['.$d.'] = \''.$day.'\';';
			}
		echo "\n";
		}
	echo "\n";
	} ?>

function movecal(i){
	var newmonth = new Date();
	newmonth.setFullYear(document.getElementById("choose_year").value,eval(document.getElementById("choose_month").value - 1 + i),1);
	document.getElementById('choose_month').value = eval(newmonth.getMonth()+1);
	document.getElementById('choose_year').value = newmonth.getFullYear();
	buildcal(document.getElementById('choose_year').value,document.getElementById('choose_month').value);
	}

function buildcal(y,m){
	while(document.getElementById('calendar').rows.length > 2){ document.getElementById('calendar').deleteRow( eval(document.getElementById('calendar').rows.length-1) ) }

	var col = cal[y][m][0];

	//bg = 'DDDDDD';
	var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
	r.style.backgroundColor = 'FFFFFF'; //bgcolor();

	if(cal[y][m][0] > 0){
		var c = r.insertCell(r.cells.length);
		c.colSpan = cal[y][m][0];
		c.style.backgroundColor = 'DDDDDD';
		c.style.height = eval(Math.ceil(calw/7)-3)+'px';
		c.style.fontSize = '2px';
		c.style.borderTop = '1px solid #999999';
		c.style.borderRight = '1px solid #999999';
		c.innerHTML = '&nbsp;';
		}

	for(day=1; day<=cal[y][m][1]; day++){
		col++;

		var c = r.insertCell(r.cells.length);
		c.id = 'd'+day;
		c.style.textAlign = 'left';
		c.style.verticalAlign = 'top';
		c.style.padding = '2px';
		c.style.width = eval(Math.ceil(calw/7)-3)+'px';
		c.style.height = eval(Math.ceil(calw/7)-3)+'px';
		c.style.fontFamily = 'Arial';
		c.style.fontSize = '10px';
		c.style.borderTop = '1px solid #999999';
		if(col < 7){
			c.style.borderRight = '1px solid #999999';
			}
		c.innerHTML = day;

	if(col == 7){
		var r = document.getElementById('calendar').insertRow(document.getElementById('calendar').rows.length);
		r.style.backgroundColor = 'FFFFFF'; //bgcolor();
		col = 0;
		}

	} //End For Loop

	if(col > 0 && col < 7){
		var c = r.insertCell(r.cells.length);
		c.colSpan = eval(7-col);
		c.style.backgroundColor = 'DDDDDD';
		c.style.height = eval(Math.ceil(calw/7)-3)+'px';
		c.style.fontSize = '2px';
		c.style.borderTop = '1px solid #999999';
		c.innerHTML = '&nbsp;';
		}

	hl_runs(y,m);
	}

function hl_runs(y,m){
var numruns = 0;
for(d=1; d<=cal[y][m][1]; d++){
	var td = document.getElementById('d'+d);
		td.innerHTML = d;

	if(runs[y] != undefined && runs[y][m] != undefined && runs[y][m][d] != undefined && runs[y][m][d] == 1){
		td.style.backgroundColor = '#9999FF';
		td.style.backgroundImage = 'url(\'img/btn_orderback.jpg\')';
		td.style.backgroundPosition = 'top center';
		td.style.backgroundRepeat = 'repeat-x';
		td.style.cursor = 'pointer';
		td.onclick = function(){ window.location = '<? echo $orderlink; ?>&date_m='+m+'&date_d='+this.innerHTML+'&date_y='+y; }
		td.onmouseover = function(){ btnhl(this,1); }
		td.onmouseout = function(){ btnhl(this,0); }
		td.style.color = '#000000';
		numruns++;
		} else {
		td.style.backgroundColor = td.parentNode.style.backgroundColor;
		td.style.color = '#000000';
		}

	} //End for loop

	var x = document.getElementById('datesum');
	if(numruns == cal[y][m][1]){
		x.innerHTML = '<?
	 		if(isset($_SESSION['lang']) && $_SESSION['lang'] != 1){
				echo str_replace("'",'\\\'',gettrans('Tour runs on all dates this month.'));
				} else {
				echo 'Tour <U STYLE="color:blue; cursor:help;" onMouseOver="showschednote(this);" onMouseOut="hideLyr();">scheduled</U> to run on all dates this month.';
				}
			?>';
		} else if(numruns == 0){
		x.innerHTML = '<? echo str_replace("'",'\\\'',gettrans('Tour doesn\'t run this month.')); ?>';
		} else {
		x.innerHTML = '<?
	 		if(isset($_SESSION['lang']) && $_SESSION['lang'] != 1){
				echo str_replace("'",'\\\'',gettrans('Tour scheduled to run on blue dates.'));
				} else {
				echo 'Tour <U STYLE="color:blue; cursor:help;" onMouseOver="showschednote(this);" onMouseOut="hideLyr();">scheduled</U> to run on blue dates.';
				}
			?>';
		}
	}

function showschednote(obj){
	var newcode = '<TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0" CLASS="ezdesc" WIDTH="200"><TR><TD CLASS="eztour" ALIGN="center">Please note that "scheduled" does not mean that the tour is guaranteed to depart.  Most tours run but some may be sold out. In addition, for any tour to depart the first two people have to book at least four weeks from the tour date.  If this has not happened, the tour may not operate.</TD></TR></TABLE>';
	document.getElementById("desc").innerHTML = newcode;
	document.getElementById("desc").style.display = "";

	var coors = findPos(obj);
	coors[0] = parseInt(coors[0])-100;
	coors[1] = parseInt(coors[1])+16;
	var x = document.getElementById('desc');
	x.style.top = coors[1]+'px';
	x.style.left = coors[0]+'px';
	}

// -->
</SCRIPT><? echo "\n\n";

$primaryw = 238;

echo '<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="7" ID="img_tbl" STYLE="float:right;"><TR><TD CLASS="imgtblmrg" STYLE="background:#FFFFFF;">&nbsp;</TD><TD ALIGN="center" BGCOLOR="#E1E1E1" CLASS="imgback">'."\n";


if(!isset($cur_tour)){

echo '<SPAN STYLE="font-family:Arial; font-size:10pt; font-weight:bold; color:#000000;">'.gettrans('Tour Calendar').'</SPAN>'."\n";

echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" ID="calendar" WIDTH="'.$calw.'" STYLE="border:1px solid #666666;">
<TR><TD COLSPAN="7" ALIGN="center" STYLE="padding-top:4px; padding-bottom:4px; background-color:#FFFFFF;">
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="width:100%;">
	<TD ALIGN="left"><INPUT TYPE="button" STYLE="font-size:8pt; text-align:center; width:28px;" VALUE="&lt;&lt;" onClick="movecal(-1);"></TD>
	<TD ALIGN="center" STYLE="font-size:8pt; white-space:nowrap;"><SELECT ID="choose_month" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=1; $ii<13; $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['m'] == $ii ): echo " SELECTED"; endif;
			echo '>'.gettrans(date("F",mktime("0","0","0",$ii,"1","2005"))).'</OPTION>';
			}
			echo '</SELECT><SELECT ID="choose_year" STYLE="font-size:8pt;" onChange="buildcal(document.getElementById(\'choose_year\').value,document.getElementById(\'choose_month\').value);">';
			for($ii=date("Y",$time); $ii<(date("Y",$time)+3); $ii++){
			echo '<OPTION VALUE="'.$ii.'"';
			if( $calendar['y'] == $ii ): echo " SELECTED"; endif;
			echo '>'.$ii.'</OPTION>';
			}
			echo '</SELECT></TD>
	<TD ALIGN="right"><INPUT TYPE="button" STYLE="font-size:8pt; text-align:center; width:28px;" VALUE="&gt;&gt;" onClick="movecal(1);"></TD>
	</TABLE>
	</TD></TR>

<TR BGCOLOR="#CCCCCC">
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Sunday').'">'.mb_substr(gettrans('Sunday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Monday').'">'.mb_substr(gettrans('Monday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Tuesday').'">'.mb_substr(gettrans('Tuesday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Wednesday').'">'.mb_substr(gettrans('Wednesday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Thursday').'">'.mb_substr(gettrans('Thursday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Friday').'">'.mb_substr(gettrans('Friday'),0,1,'utf-8').'</TD>
	<TD ALIGN="center" STYLE="font-family:Arial; font-size:10pt; border-top:1px solid #999999; cursor:help;" TITLE="'.gettrans('Saturday').'">'.mb_substr(gettrans('Saturday'),0,1,'utf-8').'</TD>
	</TR>

<TR BGCOLOR="#FFFFFF">'; //'.bgcolor('DDDDDD').'

$day = 0;
$col = $calendar['start'];

if($calendar['start'] > 0): echo '<TD BGCOLOR="#DDDDDD" COLSPAN="'.$calendar['start'].'" STYLE="border-top:1px solid #999999; border-right:1px solid #999999; height:'.(ceil($calw/7)-3).'px; font-size:2px;">&nbsp;</TD>'; endif;
for($i=0; $day<$calendar['days']; $i++){
	$col++;
	++$day;
	echo '	<TD ALIGN="left" VALIGN="top" ID="d'.$day.'" STYLE="border-top:1px solid #999999;';
		if($col < 7){ echo ' border-right:1px solid #999999;'; }
		echo ' padding:2px; color:#000000; width:'.ceil($calw/7).'px; height:'.(ceil($calw/7)-3).'px; font-family:Arial; font-size:10px;';
		if(isset($dates[$calendar['y']][$calendar['m']][$day]) && $dates[$calendar['y']][$calendar['m']][$day] == 1){
			echo ' background: #9999FF url(\'img/btn_orderback.jpg\') top center repeat-x; cursor:pointer;" onMouseOver="btnhl(this,1);" onMouseOut="btnhl(this,0);" onClick="window.location=\''.$orderlink.'&date_m='.$calendar['m'].'&date_d='.$day.'&date_y='.$calendar['y'].'\'';
			}
		echo '">'.$day.'</TD>'."\n";
	if($col == 7){
		$rowbg = bgcolor('');
		echo '</TR>'."\n".'<TR BGCOLOR="#FFFFFF">'; //'.$rowbg.'
		$col = 0;
		}
	}
	if($col > 0 && $col < 7): echo '<TD BGCOLOR="#DDDDDD" COLSPAN="'.(7 - $col).'" STYLE="border-top:1px solid #999999; height:'.(ceil($calw/7)-3).'px; font-size:2px;">&nbsp;</TD>'."\n"; endif;
	echo '</TR></TABLE>'."\n\n";

echo '<SPAN ID="datesum" STYLE="font-family:Arial; font-size:8pt; color:#000000; padding-bottom:4px;">';
	if(isset($dates[$calendar['y']][$calendar['m']]) && count($dates[$calendar['y']][$calendar['m']]) == $calendar['days']){
		if(isset($_SESSION['lang']) && $_SESSION['lang'] != 1){
			echo gettrans('Tour runs on all dates this month.');
			} else {
			echo 'Tour <U STYLE="color:blue; cursor:help;" onMouseOver="showschednote(this);" onMouseOut="hideLyr();">scheduled</U> to run on all dates this month.';
			}
	} elseif(!isset($dates[$calendar['y']][$calendar['m']]) || count($dates[$calendar['y']][$calendar['m']]) == 0){
		echo gettrans('Tour doesn\'t run this month.');
	} else {
		if(isset($_SESSION['lang']) && $_SESSION['lang'] != 1){
			echo gettrans('Tour scheduled to run on blue dates.');
			} else {
			echo 'Tour <U STYLE="color:blue; cursor:help;" onMouseOver="showschednote(this);" onMouseOut="hideLyr();">scheduled</U> to run on blue dates.';
			}
	}
	echo '</SPAN><BR>'."\n";

} //End archived if statement

if(count($images) > 0){
	echo '<IMG SRC="img/spacer.gif" BORDER="0" WIDTH="1" HEIGHT="4"><BR>'."\n\n";
	$pimg = array_shift($images);
	$checksmll = explode(".",$pimg['filename']);
	if( file_exists($_SERVER['DOCUMENT_ROOT'].'/images/'.reset($checksmll).'_small.'.end($checksmll)) ){
		$thisvarimage = reset($checksmll).'_small.'.end($checksmll);
		} else {
		$thisvarimage = $pimg['filename'];
		}
	$imgsize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisvarimage);
		if($imgsize[0] < $primaryw){
			$thisvarimage = $pimg['filename'];
			$imgsize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/'.$thisvarimage);
			}
		if($imgsize[0] > $primaryw){
			$newsize[0] = $primaryw;
			$d = ($newsize[0] / $imgsize[0]);
			$newsize[1] = ceil($imgsize[1] * $d);
			} else {
			$newsize = $imgsize;
			}
	echo '<A HREF="lrgimage.php?img='.urlencode($pimg['filename']).'"><IMG SRC="/images/'.$thisvarimage.'" BORDER="0" WIDTH="'.$newsize[0].'" HEIGHT="'.$newsize[1].'" CLASS="imgbord" ALT="'.$pimg['caption'].'"></A><BR>'."\n";
	$maxw = floor(($primaryw - 14)/2);

echo '	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="'.$primaryw.'">
	<TR>'."\n";
	$cols = 0;
	$align = "center";
	for($i=0; $i<count($images); $i++){
	$image = $images[$i];
	$thisvarimage = imgform($image['filename'],$maxw,200);
	if($cols == 0){
		echo "\t".'</tr>'."\n\t".'<TR><TD WIDTH="8" COLSPAN="2"><IMG SRC="img/spacer.gif" BORDER="0" HEIGHT="8" WIDTH="1"></TD></TR>'."\n\t".'<tr>'."\n";
		$oimage = $images[($i+1)];
		} else {
		$oimage = $images[($i-1)];
		}
		if(is_array($oimage)){
		$oimage = imgform($oimage['filename'],$maxw,200);
		if($oimage['h'] < $thisvarimage['h']): $thisvarimage = imgform($image['filename'],$maxw,$oimage['h']); endif;
		}
	//if($align == "right"): $align = "left"; else: $align = "right"; endif;
	echo "\t".'<TD ALIGN="'.$align.'" VALIGN="top"><A HREF="lrgimage.php?img='.urlencode($image['filename']).'"><IMG SRC="/images/'.$thisvarimage['filename'].'" BORDER="0" WIDTH="'.$thisvarimage['w'].'" HEIGHT="'.$thisvarimage['h'].'" CLASS="imgbord" ALT="'.$image['caption'].'"></A></TD>'."\n";
	$cols++; if($cols == 2): $cols = 0; endif;
	} //End For Loop
	echo '	</TR>
	<TR><TD WIDTH="8" COLSPAN="2"><IMG SRC="img/spacer.gif" BORDER="0" HEIGHT="8" WIDTH="1"></TD></TR>
	</TABLE>'."\n\n";

} //End Count Images If Statement

	echo '</TD></TR>';

	echo '<TR><TD CLASS="imgtblmrg" STYLE="background:#FFFFFF;" COLSPAN="2"><IMG SRC="img/spacer.gif" BORDER="0" WIDTH="2" HEIGHT="2"></TD></TR>';

	echo'</TABLE>'."\n\n";



//!TITLE/BASIC INFO
echo '<CENTER><SPAN STYLE="font-family:Helvetica; font-size:12pt; color:#000080;"><B>'.$tourinfo['title'].'</B></SPAN></CENTER><BR>'."\n\n";

echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0"';
	if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE){ echo ' STYLE="float:left; margin-bottom:12px;"'; }
	echo '>'."\n";
	$tourinfo['perguest'] = number_format($tourinfo['perguest'],2,'.','');
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Price').'</B>&nbsp;</FONT></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000"><B>$'.$tourinfo['perguest'].'</B> '.gettrans('per person')."\n";
	if($tourinfo['archived'] == "0"){ echo '<BR><I><A HREF="'.$orderlink.'">'.gettrans('Order here').'</A></I>'; }
	echo '</FONT><BR><FONT FACE="Arial" SIZE="1"><I>'.gettrans('All prices are in US currency.').'</I></FONT>';

	//if(isset($_SESSION['agent']) && $_SESSION['agent']['id'] != ""){
		//$per = (100 - $_SESSION['agent']['discount']);
		//echo '<BR><FONT FACE="Arial" SIZE="2" COLOR="#FF6600"><B>You are logged in as '.$_SESSION['agent']['name'].'.</B></FONT>'; //<BR>A '.$per.'% discount will be applied at checkout.
		//}

	echo '</TD></TR>'."\n";
	//}

//!HIGHLIGHTS
if($tourinfo['highlights'] != "" || count($itinerary) > 0){
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Highlights').'</B>&nbsp;</FONT></NOBR></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000">';
	$tourinfo['highlights'] = explode("\n",$tourinfo['highlights']);
	foreach($tourinfo['highlights'] as $thisvar){
	$thisvar = trim($thisvar);
	if($thisvar != ""): echo '<LI>'.$thisvar.'<BR>'."\n"; endif;
	}
	//if($tourinfo['details'] != ""): echo '<LI><A HREF="#details">'.gettrans('Scroll down for more details, or click here.').'</A><BR>'."\n"; endif;
	if(count($itinerary) > 0): echo '<LI><A HREF="#itinerary">'.gettrans('Scroll down for the tour itinerary, or click here.').'</A></LI>'."\n"; endif;
	//if($tourinfo['pleasenote'] != ""): echo '<LI><A HREF="#pleasenote">'.gettrans('Please note these items.').'</A><BR>'."\n"; endif;
	//echo $tourinfo['highlights'];
	echo '</FONT></TD></TR>'."\n";
	}

//!SIMILAR TOURS
if(isset($tourinfo['simtours']) && count($tourinfo['simtours']) > 0){
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Similar Tours').'</B>&nbsp;</FONT></NOBR></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000"><I>'.gettrans('You may also enjoy the following similar tours...').'</I><BR>';
	foreach($tourinfo['simtours'] as $row){
		echo '<LI STYLE="margin:2px 0px 3px;"><A HREF="tour.php?id='.urlencode($row['alias']).'">'.$row['title'].'</A></LI>'."\n";
		}
	echo '</FONT></TD></TR>'."\n";
	}

echo '</TABLE><BR>'."\n\n";

if($tourinfo['archived'] == "0"){ echo '<CENTER><TABLE BORDER="0" onClick="javascript:window.location='."'".$orderlink."'".'" STYLE="clear:left; background: #000099 url('."'img/btn_orderback.jpg'".') center center repeat-x; border: #666666 1px solid; cursor:pointer;" onMouseOver="btnhl(this,1)" onMouseOut="btnhl(this,0)"><TR><TD ALIGN="center" STYLE="padding:5px; padding-left:10px; padding-right:10px;"><A HREF="'.$orderlink.'" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; text-decoration: none; color:#FFFFFF;">'.gettrans('Order here').'</A></TD></TR></TABLE></CENTER><BR>'."\n\n"; }

//!DETAILS
if($tourinfo['details'] != ""){
	echo '<CENTER><HR SIZE="1" STYLE="clear:left;"></CENTER><BR>'."\n\n";
	echo '<CENTER><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B><A NAME="details">'.gettrans('Details').'</A></B><BR><BR></FONT></CENTER>';
	echo '<FONT FACE="Arial" SIZE="2" COLOR="#000000">'.nl2br($tourinfo['details']);
		//if($tourinfo['fuelsurcharge'] == "1"){ echo '<BR><A HREF="#" onClick=\'window.open( "http://www.bundubashers.com/fuel_surcharge_grand_canyon_tours.html","fuel_surcharge","scrollbars=1,status=1,height=500,width=500,resizable=1" ); return false;\'>'.gettrans('This tour may be subject to a fuel surcharge.').'</A>'."\n"; }
		echo '</FONT><BR><BR>';
	echo '<CENTER><A HREF="#top" STYLE="font-family:Arial; font-size:11px;">'.gettrans('TOP').'</A><BR></CENTER>'."\n\n";
	}

//!YOUTUBE VIDEO
if(isset($tourinfo['youtube']) && $tourinfo['youtube'] != ""){
	echo '<CENTER><HR SIZE="1" STYLE="clear:left;"></CENTER><BR>'."\n\n";
	if(isset($tourinfo['youtube_title']) && $tourinfo['youtube_title'] != ""){
		echo '<DIV STYLE="text-align:center; margin-bottom:6px; font-family:Helvetica; font-size:10pt; font-weight:bold;">'.$tourinfo['youtube_title'].'</DIV>';
		}
	echo '<DIV STYLE="text-align:center; margin-bottom:24px;">'.$tourinfo['youtube'].'</DIV>';
	}

//!ITINERARY
if(count($itinerary) > 0){

echo '<CENTER><HR SIZE="1" STYLE="clear:left;"></CENTER><BR>'."\n\n";

echo '<CENTER><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B><A NAME="itinerary">'.gettrans('Itinerary').'</A></B>';
	if(isset($cur_tour)){
		echo '<BR><BR><SPAN STYLE="font-family:Arial; font-size:11pt;"><I><A HREF="tour.php?id='.$cur_tour['alias'].'">';
		if(isset($tourinfo['cur_link']) && trim($tourinfo['cur_link']) != ""){
			echo gettrans(trim($tourinfo['cur_link']));
			} else {
			echo gettrans('There is a new and much better version of this tour!');
			}
			echo '</A></I></SPAN>';
		}
	echo '</CENTER><BR></FONT>'."\n\n";

	$curroute = 0; $curday = 1; $tblid = 0;
	foreach($itinerary as $itkey => $row){

	if($_SESSION['lang'] != "1" && $tourinfo['archived'] == 0){ //isset($row['type']) && $row['type'] == "r" && 
		if(isset($ittranslations['r'.$row['id']]['title']) && $ittranslations['r'.$row['id']]['title'] != ""){ $row['title'] = $ittranslations['r'.$row['id']]['title']; }
		if(isset($ittranslations['r'.$row['id']]['time']) && $ittranslations['r'.$row['id']]['time'] != ""){ $row['time'] = $ittranslations['r'.$row['id']]['time']; }
		if(isset($ittranslations['r'.$row['id']]['comments']) && $ittranslations['r'.$row['id']]['comments'] != ""){ $row['comments'] = $ittranslations['r'.$row['id']]['comments']; }
		if(isset($ittranslations['r'.$row['id']]['youtube_title']) && $ittranslations['r'.$row['id']]['youtube_title'] != ""){ $row['youtube_title'] = $ittranslations['r'.$row['id']]['youtube_title']; }
		$row['headline'] = gen_ithead($row,$curroute,$curday);
		}

	//Note for extended lodging
	if($row['type'] == "e"){
		$row['extlodge'] = $tourinfo['extlodging']['e'.$row['typeid']];
		//$row['comments'] = '<PRE STYLE="text-align:left;">'.print_r($row,1).'</PRE>';
		//$row['comments'] = '<PRE STYLE="text-align:left;">'.print_r($tourinfo['extlodging']['e'.$row['typeid']],1).'</PRE>';
		//$row['comments'] = '<B>You have the option of extending your stay here at '.$row['extlodge']['name'].' for an additional $'.$row['extlodge']['perguest'].' per guest, per night.</B>';
		}

	if(isset($row['headline']) && $row['headline'] != ""){
		//echo '<TR><TD ALIGN="left" COLSPAN="2" CLASS="ithd"><FONT FACE="Arial" SIZE="2"><B>'.$row['headline'].'</B></FONT></TD></TR>'."\n";
		echo '<DIV STYLE="padding:4px; background:#FFFFFF url(\'img/toplinksback.jpg\') center center repeat-x; border-bottom: 1px solid #333333; font-family:Arial; font-size:10pt; font-weight:bold; text-align:left; padding-bottom:4px; clear:left;">'.$row['headline'].'</DIV>'."\n";
		}
	if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE){ echo '<DIV STYLE="clear:left;">'; }
	echo '<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" STYLE="clear:left;';
	if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE){ echo ' float:left;'; } // width:793px;
	echo '"><TR><TD ALIGN="center" VALIGN="top" STYLE="padding:0px; padding-left:8px; padding-top:8px; width:122px; font-family:Arial; font-size:8pt;">';
		if($row['map'] != "0" && $row['map'] != ""){
			$thisimage = imgform($itimages['i'.$row['map']]['filename'],120,180);
			echo '<A HREF="lrgimage.php?img='.urlencode($itimages['i'.$row['map']]['filename']).'"><IMG SRC="images/'.$thisimage['filename'].'" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" STYLE="border: #666666 solid 1px;" CLASS="imgbord" ALT="Map"><BR>'.gettrans('Map').'</A>';
			} else {
			echo '&nbsp;';
			}
		echo '</TD><TD ALIGN="left" VALIGN="top" STYLE="padding:8px; padding-right:0px; padding-bottom:0px; font-family:Arial,Helvetica,sans-serif; font-size:9pt;" CLASS="ittext">';
		echo nl2br($row['comments']);

		if(isset($images_asoc['i'.$row['id']]) && is_array($images_asoc['i'.$row['id']]) && count($images_asoc['i'.$row['id']]) > 0){
		echo '<DIV STYLE="text-align:center;">'; // width:100%;
			foreach($images_asoc['i'.$row['id']] as $key => $img){
				//echo '<!-- '.$key.' '.$img.' -->';
				$thisimage = imgform($itimages['i'.$img]['filename'],200,110);
				echo '<A HREF="lrgimage.php?img='.urlencode($itimages['i'.$img]['filename']).'" TITLE="'.$itimages['i'.$img]['caption'].'">';
				echo '<IMG SRC="images/'.$thisimage['filename'].'" ID="'.$row['id'].'_'.$key.'" BORDER="0" WIDTH="'.$thisimage['w'].'" HEIGHT="'.$thisimage['h'].'" ALT="'.$itimages['i'.$img]['caption'].'" STYLE="border:4px solid #FFFFFF;">';
				echo '</A> ';
				}
			echo '</DIV>';
			} //End images_assoc if statement

		if(isset($row['youtube']) && $row['youtube'] != ""){
			if(isset($row['youtube_title']) && $row['youtube_title'] != ""){
				echo '<DIV STYLE="text-align:center; margin-top:16px; font-family:Helvetica; font-size:10pt; font-weight:bold;">'.$row['youtube_title'].'</DIV>';
				echo '<DIV STYLE="text-align:center; padding:2px 0px 16px;">';
				} else {
				echo '<DIV STYLE="text-align:center; padding:16px 0px 16px;">';
				}
			echo $row['youtube'].'</DIV>';
			}

		echo '</TD></TR></TABLE>'."\n";
	if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE){ echo '</DIV>'; }

	if(isset($itinerary[($itkey+1)]) && isset($itinerary[($itkey+1)]['headline']) && $itinerary[($itkey+1)]['headline'] != ""){
		echo '<DIV STYLE="font-family:Arial; font-size:9pt; text-align:center; margin-bottom:10px; clear:left;"><A HREF="#top" STYLE="font-family:Arial; font-size:11px;">'.gettrans('TOP').'</A></DIV>'."\n";
		}

	if(isset($row['type']) && $row['type'] == "r" && $row['routeid'] != ""){ $curroute = $row['routeid']; }
	$curday = $row['day'];
	} //End for each

	echo '<DIV STYLE="font-family:Arial; font-size:9pt; text-align:center; margin-bottom:10px; clear:left;"><A HREF="#top" STYLE="font-family:Arial; font-size:11px;">'.gettrans('TOP').'</A></DIV><BR>'."\n\n";

} //End itinerary



echo '<CENTER><HR SIZE="1" STYLE="clear:left;"></CENTER><BR>'."\n\n";


echo '<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0">'."\n";

//!ACTIVITIES
if(isset($tourinfo['options']) && count($tourinfo['options']) > 0){
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Available Options').'</B>&nbsp;</FONT></NOBR></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000">';
	foreach($tourinfo['options'] as $thisvar){
	echo '<LI>'.$thisvar['name'].': +$'.number_format($thisvar['price'], 2, '.', '').'<BR>'."\n";
	}
	echo '</FONT></TD></TR>'."\n";
	}

//!LODGING EXTENSIONS
if(isset($tourinfo['extlodging']) && count($tourinfo['extlodging']) > 0){
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Extend Your Stay').'</B>&nbsp;</FONT></NOBR></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000">';
	echo '<I>'.gettrans('You have the option of extending your stay at certain locations during the tour.').'</I><BR>';
	foreach($tourinfo['extlodging'] as $thisvar){
	echo '<LI>'.$thisvar['name'].': +$'.number_format($thisvar['perguest'], 2, '.', '').' per guest, per night<BR>'."\n";
	}
	echo '</FONT></TD></TR>'."\n";
	}

//!TOUR ID
	echo '<TR><TD VALIGN="top"><NOBR><IMG SRC="img/bullet2.gif" WIDTH="8" HEIGHT="8"><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B>'.gettrans('Tour ID').'</B>&nbsp;</FONT></TD><TD><FONT FACE="Arial" SIZE="2" COLOR="#000000">'.$tourinfo['id'].'</FONT></TD></TR>'."\n";

echo '</TABLE><BR>'."\n\n";

if($tourinfo['pleasenote'] != ""){ // || $tourinfo['zion_warning'] == "1"

echo '<CENTER><FONT FACE="Arial" SIZE="2" COLOR="#000080"><B><A NAME="pleasenote">'.gettrans('Please note the following...').'</A></B></FONT></CENTER>';
echo '<FONT FACE="Arial" SIZE="2" COLOR="#000000"><UL>';
	//if($tourinfo['zion_warning'] == "1"){ echo '<LI><SPAN STYLE="color:#FF0000;">'.gettrans('Please note that as a result of late planned road work in Zion, one of the roads through Zion will be closed on certain days through the summer.  On about 15% of the tours Zion will be bypassed and we will visit Cedar Breaks instead.').'</SPAN></LI>'."\n"; }
	if($tourinfo['fuelsurcharge'] > 0){
		echo '<LI>'; //<A HREF="#" onClick=\'window.open( "http://www.bundubashers.com/fuel_surcharge_grand_canyon_tours.html","fuel_surcharge","scrollbars=1,status=1,height=500,width=500,resizable=1" ); return false;\'>';
 		if(isset($_SESSION['lang']) && $_SESSION['lang'] != 1){
			echo gettrans('This tour is subject to a fuel surcharge.');
			} else {
			echo 'This tour is subject to a $'.$tourinfo['fuelsurcharge'].' per person fuel surcharge.';
			}
		echo '</LI>'."\n"; //</A>
		}
	$tourinfo['pleasenote'] = explode("\n",$tourinfo['pleasenote']);
	foreach($tourinfo['pleasenote'] as $thisvar){
		$thisvar = trim($thisvar);
		if($thisvar != ""){ echo '<LI>'.$thisvar.'</LI>'."\n"; }
		}
echo '</UL></FONT>'."\n\n";
echo '<CENTER><A HREF="#top" STYLE="font-family:Arial; font-size:11px;">'.gettrans('TOP').'</A></CENTER><BR>'."\n\n";

} elseif(count($itinerary) == 0){

echo '<BR>';

}


echo '<CENTER>'."\n\n";

if($tourinfo['archived'] == "0"){
	echo '<TABLE BORDER="0" onClick="javascript:window.location='."'".$orderlink."'".'" STYLE="background: #000099 url('."'img/btn_orderback.jpg'".') center center repeat-x; border: #666666 1px solid; cursor:pointer;" onMouseOver="btnhl(this,1)" onMouseOut="btnhl(this,0)"><TR><TD ALIGN="center" STYLE="padding:5px; padding-left:10px; padding-right:10px;"><A HREF="'.$orderlink.'" STYLE="font-family:Arial; font-size:11pt; font-weight:bold; text-decoration: none; color:#FFFFFF;">'.gettrans('Order here').'</A></TD></TR></TABLE><BR>'."\n\n";
	}

echo '<HR SIZE="1" STYLE="clear:left;"><BR>'."\n\n";

//if($tourinfo['fuelsurcharge'] == "1"){ echo '<font face="Arial" size="2" color="#000080"><A HREF="#" onClick=\'window.open( "http://www.bundubashers.com/fuel_surcharge_grand_canyon_tours.html","fuel_surcharge","scrollbars=1,status=1,height=500,width=500,resizable=1" ); return false;\'>'.gettrans('This tour may be subject to a fuel surcharge.').'</A></FONT><BR><BR>'."\n\n"; }

echo '<font face="Arial" size="2">';
if($tourinfo['archived'] == "0"){
	echo '<a href="'.$orderlink.'">'.gettrans('This tour can be ordered online here.').'</a>&nbsp; ';
	}
	echo gettrans('Please call us at 1 800 724 7767 or (USA) 435 658 2227,').' <a href="mailto:info@bundubashers.com">'.gettrans('or mail us for additional information.').'</a>';
	echo '</FONT><BR><BR>'."\n\n";

echo '</CENTER>';

echo '<!-- '.$tourinfo['id'].' -->'."\n\n";

} else {

include_once('header.php');

echo '<CENTER><BR><BR><FONT FACE="Arial" SIZE="3">'.gettrans('We are sorry.  We were unable to find the tour you requested.').'<BR>'.gettrans('Please choose another from the menu on the top of this page, or the EZ Tour Finder on the left of this page.').'<BR>'.gettrans('Thank you.').'</FONT></CENTER>'."\n\n";

} //END NUM RESULTS IF STATEMENT

echo '</CENTER>'."\n\n";


include('footer.php'); ?>